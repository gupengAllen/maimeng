//
//  AppDelegate.h
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"
#import "CustomAlertView.h"
#import "Config.h"

#import "ASIDownloadCache.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, CustomAlertViewDelegate>
{
    BOOL isPush;
//    ASIDownloadCache *myCache;
}
//@property (nonatomic, retain) ASIDownloadCache *myCache;

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *telephone;

@property (nonatomic, strong) UserInfo *userInfo;

@property (nonatomic, strong) NSDictionary *pushDic;

@property (nonatomic, strong) NSString *adId;
@property (nonatomic, strong) NSString *loginType;

@end

