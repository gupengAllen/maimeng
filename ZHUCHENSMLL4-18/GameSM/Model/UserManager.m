//
//  UserManager.m
//  GameSM
//
//  Created by 王涛 on 15/8/3.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserManager.h"
#import "AppDelegate.h"
#import "Y_X_DataInterface.h"

@implementation UserManager

- (void)saveUserInfo:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        g_App.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userName = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"]];
        g_App.userInfo = [[UserInfo alloc] init];
        g_App.userInfo.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];
        g_App.userInfo.sex = (int)[[[dic objectForKey:@"results"] objectForKey:@"sex"] integerValue];
        g_App.userInfo.name = [[dic objectForKey:@"results"] objectForKey:@"name"];
        g_App.userInfo.telephone = [[dic objectForKey:@"results"] objectForKey:@"username"];
        g_App.userInfo.signature = [[dic objectForKey:@"results"] objectForKey:@"signature"];
        g_App.userInfo.images = [[dic objectForKey:@"results"] objectForKey:@"images"];
        g_App.userInfo.loginTime = [[dic objectForKey:@"results"] objectForKey:@"loginTimeValue"];
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:g_App.userInfo.userID forKey:@"userID"];
        [userDefaults setObject:[NSString stringWithFormat:@"%d", g_App.userInfo.sex] forKey:@"sex"];
        [userDefaults setObject:g_App.userInfo.name forKey:@"name"];
        [userDefaults setObject:g_App.userInfo.telephone forKey:@"telephone"];
        [userDefaults setObject:g_App.userInfo.signature forKey:@"signature"];
        [userDefaults setObject:g_App.userInfo.images forKey:@"images"];
        [userDefaults setObject:g_App.userInfo.loginTime forKey:@"loginTime"];
        [userDefaults setObject:g_App.loginType forKey:@"loginType"];
        
        [userDefaults synchronize];
    }
}

- (void)clearUserInfo {
    g_App.userID = nil;
    g_App.userName = nil;
    g_App.telephone = nil;
    [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
    g_App.userInfo = nil;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"" forKey:@"userID"];
    [userDefaults setObject:@"" forKey:@"sex"];
    [userDefaults setObject:@"" forKey:@"name"];
    [userDefaults setObject:@"" forKey:@"userName"];
    [userDefaults setObject:@"" forKey:@"signature"];
    [userDefaults setObject:@"" forKey:@"images"];
    [userDefaults setObject:@"" forKey:@"loginTime"];
    [userDefaults setObject:@"" forKey:@"loginType"];
    [userDefaults synchronize];
}


@end
