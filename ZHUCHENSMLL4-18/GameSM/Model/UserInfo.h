//
//  UserInfo.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

@property (nonatomic, strong) NSString *userID;
@property (nonatomic, assign) int sex;                  // 0 保密  1 男 2 女
@property (nonatomic, strong) NSString *name;           // 昵称
@property (nonatomic, strong) NSString *signature;
@property (nonatomic, strong) NSString *images;
@property (nonatomic, strong) NSString *telephone;       // 手机号
@property (nonatomic, strong) NSString *loginTime;       // 手机号
@property (nonatomic, strong) NSString *loginType;             // 0 正常登录 1第三方登录
//@property (nonatomic, strong) NSString *signStatus;

@end
