//
//  AFDownloadRequestOperationManager.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/12.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "AFDownloadRequestOperationManager.h"
#import "AFDownloadRequestOperation.h"



@implementation AFDownloadRequestOperationManager
{
//    NSInteger _currentRequestPage;
    float _currentProgress;
}

+ (id)sharedAFDownloadRequestOperationManager
{
    static dispatch_once_t pred;
    static AFDownloadRequestOperationManager * AFDownloadRequestOperationManager= nil;
    
    dispatch_once(&pred, ^{ AFDownloadRequestOperationManager = [[self alloc] init];});
    return AFDownloadRequestOperationManager;
}



//gameDownload.operation = [[AFDownloadRequestOperation alloc] initWithRequest:request targetPath:filePath shouldResume:YES];
//[gameDownload.operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//    __weak DownloadTableViewCell *weakcell = (DownloadTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    gameDownload.state = DOWNLOADED;
//    weakcell.downloadState.image = [UIImage imageNamed:@”success”];
//    [[MyGames sharedManager] addGame:gameDownload.gameView];
//} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//    __weak DownloadTableViewCell *weakcell = (DownloadTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    NSLog(@”Error: %@”, error);
//    gameDownload.state = FAILURE;
//    weakcell.downloadState.image = [UIImage imageNamed:@”failure”];
//}];


- (void)addDownRequest:(NSArray *)requestUrl
            targetPath:(NSString *)filepath
           shouldResum:(BOOL)isResum
       completeBlockRe:(void(^)(AFHTTPRequestOperation *operation,id responseObject))completeRequestBlock
           failBlockRe:(void(^)(AFHTTPRequestOperation *operation,NSError *error))failRequestBlock{
//    _requestUrl = requestUrl;
//    _filepath = filepath;
//    _completeRequestBlock = completeRequestBlock;
//    _failRequestBlock = failRequestBlock;
//    _isResum = isResum;
    [self addMoreRequest:requestUrl targetPath:filepath shouldResum:isResum completeBlockRe:completeRequestBlock failBlockRe:failRequestBlock];
}

static NSInteger _currentRequestPage = 0;
- (void)addMoreRequest:(NSArray *)requestUrl targetPath:(NSString *)filepath
           shouldResum:(BOOL)isResum
       completeBlockRe:(void(^)(AFHTTPRequestOperation *operation,id responseObject))completeRequestBlock
           failBlockRe:(void(^)(AFHTTPRequestOperation *operation,NSError *error))failRequestBlock{
    
    NSString *sandboxPath = NSHomeDirectory();
    NSString *documentPath = [sandboxPath
                              stringByAppendingPathComponent:@"Documents"];
    
//    NSString * FileName=[documentPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",cartoonModel.cartoonId]];//fileName就是保存文件的文件名
//    NSString *lastFileName = [FileName stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",cartoonModel.chapterId]];
//    [[NSFileManager defaultManager] createDirectoryAtPath:FileName withIntermediateDirectories:YES attributes:nil error:nil];
    
    
    AFDownloadRequestOperation *gameDownloadOperation = [[AFDownloadRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://7xkbpd.com2.z0.glb.qiniucdn.com/e2e60e0c8bc4e893680cd2723cec37bc_81181.jpg"]] targetPath:filepath shouldResume:isResum];
    [gameDownloadOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"成功");
        _currentRequestPage ++;
        if (_currentRequestPage < requestUrl.count) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
               BOOL isSave = [responseObject writeToFile:[NSString stringWithFormat:@"%@%@",documentPath,requestUrl[_currentRequestPage]] atomically:YES];
                //            NSLog(@"=================>%@",isSave);
//                NSLog(@"isSave%@",isSave);
            });

            
            [self addMoreRequest:requestUrl targetPath:filepath shouldResum:isResum completeBlockRe:completeRequestBlock failBlockRe:failRequestBlock];
            _currentProgress = _currentRequestPage/requestUrl.count;
        }else if (_currentRequestPage == requestUrl.count){
            completeRequestBlock(operation,responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        failRequestBlock(operation,error);
//        NSLog(@"error %@",error);
    }];
    
}






@end
