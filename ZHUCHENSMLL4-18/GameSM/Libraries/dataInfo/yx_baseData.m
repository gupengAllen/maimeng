//
//  yx_baseData.m
//  YouYou_Work
//
//  Created by lianghuigui on 13-11-19.
//  Copyright (c) 2013年 lianghuigui. All rights reserved.
//

#import "yx_baseData.h"

@implementation yx_baseData
@synthesize m_Dictionary;

- (NSString*)errorCode{
    
    return [m_Dictionary objectForKey:@"errorCode"];
}


-(id)initWithJsonObject:(id)aJson
{
    if (self == [super init]) {
        m_Dictionary = (NSMutableDictionary*)aJson;        
        
    }
	
	return self;
}

-(id)init {
    if ( self == [super init] ) {
        m_Dictionary = [[NSMutableDictionary alloc] init];
    }
	
	return self;
}
@end
