//
//  yx_baseData.h
//  YouYou_Work
//
//  Created by lianghuigui on 13-11-19.
//  Copyright (c) 2013年 lianghuigui. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface yx_baseData : NSObject{
    NSMutableDictionary*   m_Dictionary;
}
@property (nonatomic ,readonly)NSString* errorCode; // 增加的错误信息返回字段
@property (nonatomic,strong)NSMutableDictionary*  m_Dictionary;

-(id)initWithJsonObject:(id)aJson;  // 用json对象初始化

-(id)init;

@end
