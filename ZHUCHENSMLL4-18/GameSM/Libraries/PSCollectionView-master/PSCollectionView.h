//
// PSCollectionView.h
//
// Copyright (c) 2012 Peter Shih (http://petershih.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "PSCollectionViewCell.h"

#define kPSCollectionViewDidRelayoutNotification @"kPSCollectionViewDidRelayoutNotification"

typedef enum {
    kPRPSCStateNormal = 0,
    kPRPSCStatePulling = 1,
    kPRPSCStateLoading = 2,
    kPRPSCStateHitTheEnd = 3
} PRPSCState;

@interface PSCollectLoadingView : UIView
{
    UILabel *_stateLabel;
    UILabel *_dateLabel;
    UIImageView *_arrowView;
    UIActivityIndicatorView *_activityView;
    CALayer *_arrow;
    BOOL _loading;
}
@property (nonatomic,getter = isLoading) BOOL loading;
@property (nonatomic,getter = isAtTop) BOOL atTop;
@property (nonatomic) PRPSCState state;

- (id)initWithFrame:(CGRect)frame atTop:(BOOL)top;

- (void)updateRefreshDate:(NSDate *)date;

@end

@class PullingRefreshPSCollectionView;
@protocol PullingRefreshPSCollectionViewDelegate <NSObject>

@required
- (void)pullingPSCollectionViewDidStartRefreshing:(PullingRefreshPSCollectionView *)tableView;

@optional
//Implement this method if headerOnly is false
- (void)pullingPSCollectionViewDidStartLoading:(PullingRefreshPSCollectionView *)tableView;
//Implement the follows to set date you want,Or Ignore them to use current date
- (NSDate *)pullingPSCollectionViewRefreshingFinishedDate;
- (NSDate *)pullingPSCollectionViewLoadingFinishedDate;

@end

@class PSCollectionViewCell;

@protocol PSCollectionViewDelegate, PSCollectionViewDataSource;

@interface PSCollectionView : UIScrollView
{
    PSCollectLoadingView *_headerView;
    PSCollectLoadingView *_footerView;
    UILabel *_msgLabel;
    BOOL _loading;
    BOOL _isFooterInAction;
    NSInteger _bottomRow;
}



@property (assign,nonatomic) id <PullingRefreshPSCollectionViewDelegate> pullingDelegate;
@property (nonatomic) BOOL autoScrollToNextPage;
@property (nonatomic) BOOL reachedTheEnd;
@property (nonatomic,getter = isHeaderOnly) BOOL headerOnly;

@property (nonatomic, assign) BOOL noPullRefresh;

- (id)initWithFrame:(CGRect)frame pullingDelegate:(id<PullingRefreshPSCollectionViewDelegate>)aPullingDelegate;

- (void)collectionViewDidScroll:(UIScrollView *)scrollView;

- (void)collectionViewDidEndDragging:(UIScrollView *)scrollView;

- (void)collectionViewDidFinishedLoading;

- (void)collectionViewDidFinishedLoadingWithMessage:(NSString *)msg;

- (void)launchRefreshing;

#pragma mark - Public Properties

@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *footerView;

@property (nonatomic, assign, readonly) CGFloat colWidth;
@property (nonatomic, assign, readonly) NSInteger numCols;
@property (nonatomic, assign) NSInteger numColsLandscape;
@property (nonatomic, assign) NSInteger numColsPortrait;
@property (nonatomic, unsafe_unretained) id <PSCollectionViewDelegate> collectionViewDelegate;
@property (nonatomic, unsafe_unretained) id <PSCollectionViewDataSource> collectionViewDataSource;

#pragma mark - Public Methods

/**
 Reloads the collection view
 This is similar to UITableView reloadData)
 */
- (void)reloadData;

/**
 Dequeues a reusable view that was previously initialized
 This is similar to UITableView dequeueReusableCellWithIdentifier
 */
- (PSCollectionViewCell *)dequeueReusableViewForClass:(Class)viewClass;

@end

#pragma mark - Delegate

@protocol PSCollectionViewDelegate <NSObject>

@optional
- (void)collectionView:(PSCollectionView *)collectionView didSelectCell:(PSCollectionViewCell *)cell atIndex:(NSInteger)index;
- (Class)collectionView:(PSCollectionView *)collectionView cellClassForRowAtIndex:(NSInteger)index;

@end

#pragma mark - DataSource

@protocol PSCollectionViewDataSource <NSObject>

@required
- (NSInteger)numberOfRowsInCollectionView:(PSCollectionView *)collectionView;
- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView cellForRowAtIndex:(NSInteger)index;
- (CGFloat)collectionView:(PSCollectionView *)collectionView heightForRowAtIndex:(NSInteger)index;

@end
