//
//  AFHTTPRequestOperation+RequestParams.m
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import "AFHTTPRequestOperation+RequestParams.h"
#import <objc/runtime.h>

static void *GSRequestParamsCartoonId;
static void *GSRequestParamsChapterId;
static void *GSRequestParamsURL;


@implementation AFHTTPRequestOperation (RequestParams)

- (void)setCartoonId:(NSString *)cartoonId
{
    objc_setAssociatedObject(self, &GSRequestParamsCartoonId, cartoonId, OBJC_ASSOCIATION_COPY);
}


- (NSString *)cartoonId
{
    return objc_getAssociatedObject(self, &GSRequestParamsCartoonId);
}


- (void)setChapterId:(NSString *)chapterId
{
    objc_setAssociatedObject(self, &GSRequestParamsChapterId, chapterId, OBJC_ASSOCIATION_COPY);
}


- (NSString *)chapterId
{
    return objc_getAssociatedObject(self, &GSRequestParamsChapterId);
}


- (void)setUrl:(NSString *)url
{
    objc_setAssociatedObject(self, &GSRequestParamsURL, url, OBJC_ASSOCIATION_COPY);
}


- (NSString *)url
{
    return objc_getAssociatedObject(self, &GSRequestParamsURL);
}




@end
