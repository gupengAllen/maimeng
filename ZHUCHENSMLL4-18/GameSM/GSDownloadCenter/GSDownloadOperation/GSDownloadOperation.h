//
//  GSDownloadOperation.h
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFDownloadRequestOperation.h"
@class GSDownloadOperation;
@protocol GSDownloadOperationDelegate <NSObject>
@optional
- (void)downloadOperationSuccess:(GSDownloadOperation *) downloadOperation chapterInfo:(NSDictionary *)info;
- (void)downloadOperationFail:(GSDownloadOperation *) downloadOperation chapterInfo:(NSDictionary *)info;

@end



@interface GSDownloadOperation : NSObject


@property (nonatomic, weak) id<GSDownloadOperationDelegate> delegate;

@property (nonatomic, strong) AFDownloadRequestOperation *downloadOperation;

@property (nonatomic, copy) void(^downOperationSuccess)(GSDownloadOperation * downloadOperation,NSDictionary *info);
- (instancetype)initWithURL:(NSString *)url targetPath:(NSString *)path;



@end
