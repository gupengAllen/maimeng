//
//  GSDownloadManager.m
//  AIFDownload
//
//  Created by xiaerfei on 15/11/15.
//  Copyright © 2015年 geren. All rights reserved.
//

#import "GSDownloadManager.h"
#import "GSDownloadQueue.h"
#import "DataBaseHelper.h"

NSString *const kGSDownloadManagerCartoonChange = @"GSDownloadManagerCartoonChange";




@interface GSDownloadManager ()<GSDownloadQueueDelegate>

@property (nonatomic, strong) NSMutableDictionary *dispatchCartoon;
@property (nonatomic, copy) NSMutableArray *allQueue;
@property (nonatomic, copy) NSMutableArray *allUrlSSS;
@property (nonatomic, copy) NSMutableArray *targetPathArr;
@property (nonatomic, copy) NSMutableArray *allCartoonId;
@property (nonatomic, assign) BOOL isDownLoad;
//@property (nonatomic, copy)NSMutableArray *allQueue
@end

@implementation GSDownloadManager
static int currentQueue = 0;
+ (instancetype)manager
{
    static dispatch_once_t onceToken;
    static GSDownloadManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[GSDownloadManager alloc] init];
    });
    return manager;
}

/*
 开始任务的时候，查询出任务的进度
 
 
 
    处理没有下载完成的
 */
- (void)startDownloadWithCartoonId:(NSString *)cartoonId andDownLoad:(BOOL)isDownLoad
{
    _isDownLoad = isDownLoad;
    //1.根据cartoonId 拿到所有的话
//    NSMutableArray *chapters = [[NSMutableArray alloc] init];
//    NSString *chapterId = nil;
//    //http://img.playsm.com/cartoon/9efdf2d448e57d635e54313ec37e5fec_001_001.jpg
////    http://img.playsm.com/cartoon/9efdf2d448e57d635e54313ec37e5fec_001_002.jpg
//    for (int i = 1; i < 20; i++) {
//        NSString *str = [NSString stringWithFormat:@"http://img.playsm.com/cartoon/9efdf2d448e57d635e54313ec37e5fec_001_%03d.jpg",i];
//        [chapters addObject:str];
//        
//    }
    
    
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",cartoonId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSArray *chapterArr = [[DataBaseHelper shared] fetchpart:cartoonId chapterId:nil];
    NSMutableArray *cartoonChapter = [NSMutableArray array];
    [self.allUrlSSS addObjectsFromArray:chapterArr];
    for (int i = 0;i < chapterArr.count;i ++) {
        NSString *lastpath = [path stringByAppendingPathComponent:[chapterArr[i] allKeys].lastObject];
        if (![fileManager fileExistsAtPath:lastpath]) {
            [fileManager createDirectoryAtPath:lastpath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        [self.targetPathArr addObject:lastpath];
        [self.allCartoonId  addObject:cartoonId];
//        [_allUrlSSS addObject:[chapterArr[i] allKeys].lastObject];
        
//        if (self.dispatchCartoon[cartoonId] == nil) {
//            NSMutableArray *array = [[NSMutableArray alloc] init];
//            
//            [array addObject:downloadQueue];
//            self.dispatchCartoon[cartoonId] = array;
//        } else {
//            NSMutableArray *array = self.dispatchCartoon[cartoonId];
//            [array addObject:downloadQueue];
//        }
        
    }
    
    GSDownloadQueue *downloadQueue = [[GSDownloadQueue alloc] init];
    downloadQueue.delegate = self;
    downloadQueue.chapterId = [self.allUrlSSS[0] allKeys].lastObject;
    [downloadQueue addOperationWithURLs:[self.allUrlSSS[0] allValues].lastObject target:self.targetPathArr[0] cartoonId:self.allCartoonId[0] chapterId:[self.allUrlSSS[0] allKeys].lastObject];
    [cartoonChapter addObject:downloadQueue];
    [_allQueue addObject:downloadQueue];
}


- (void)startLeftDownloadWithCartoonId:(NSString *)cartoonId{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",cartoonId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    NSArray *chapterArr = [[DataBaseHelper shared] fetchpartIsSaveExist:cartoonId chapterId:nil];
    NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
    
    for (int i = 0;i < chapterArr.count;i ++) {
        [indexSet addIndex:i];
        NSString *lastpath = [path stringByAppendingPathComponent:[chapterArr[i] allKeys].lastObject];
        if (![fileManager fileExistsAtPath:lastpath]) {
            [fileManager createDirectoryAtPath:lastpath withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        [self.targetPathArr insertObject:lastpath atIndex:i];
        [self.allCartoonId insertObject:cartoonId atIndex:i];
    }
    

    [self.allUrlSSS insertObjects:chapterArr atIndexes:indexSet];
    
    
    GSDownloadQueue *downloadQueue = [[GSDownloadQueue alloc] init];
    downloadQueue.delegate = self;
    downloadQueue.chapterId = [self.allUrlSSS[0] allKeys].lastObject;
    downloadQueue.cartoonId = cartoonId;
    [downloadQueue addOperationWithURLs:[self.allUrlSSS[0] allValues].lastObject
                                 target:self.targetPathArr[0]
                              cartoonId:self.allCartoonId[0]
                              chapterId:[self.allUrlSSS[0] allKeys].lastObject];
    [_allQueue addObject:downloadQueue];
    
}


- (void)pauseDownloadWithCartoonId:(NSString *)cartoon chapterId:(NSString *)chapterId {
    NSRange range;
    range.location = 0;
    range.length = [[[DataBaseHelper shared] fetchpartIsSaveExist:cartoon chapterId:nil] count];
    [_allUrlSSS removeObjectsInRange:range];
}


- (void)recoverDownloadWithCartoonId:(NSString *)cartoonId chapterId:(NSString *)chapterId
{
    NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"Documents/%@",cartoonId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:path]) {
        [fileManager createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    NSString *lastpath = [path stringByAppendingPathComponent:chapterId];
    
    
    NSDictionary *singleCartoon = [[DataBaseHelper shared] fetchSinglepart:@"" chapterId:chapterId];
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"1"]){
        
        GSDownloadQueue *downloadQueue = [[GSDownloadQueue alloc] init];
        downloadQueue.delegate = self;
        downloadQueue.chapterId = chapterId;
        downloadQueue.cartoonId = cartoonId;
        
        [self.allUrlSSS insertObject:singleCartoon atIndex:0];
        [self.targetPathArr insertObject:lastpath atIndex:0];
        [self.allCartoonId  insertObject:cartoonId atIndex:0];
        
        [downloadQueue addOperationWithURLs:[self.allUrlSSS[0] allValues].lastObject target:self.targetPathArr[0] cartoonId:self.allCartoonId[0] chapterId:[self.allUrlSSS[0] allKeys].lastObject];
        [_allQueue addObject:downloadQueue];
    }

}

#pragma mark - GSDownloadQueueDelegate

- (void)downloadQueueSuccess:(GSDownloadQueue *) downloadQueue chapterInfo:(NSDictionary *)info
{
//    // 数据库的操作运算
//    
//    // 当前进行的任务 计算部的进度  话的进度
//    NSLog(@"chapterId%@",downloadQueue.chapterId);
//    NSLog(@"allUrlSSS%d",[[self.allUrlSSS[0] allValues].lastObject count]);
//    NSLog(@"allUrlSSSQUEUE%d",[[self.allUrlSSS[0] allValues].lastObject count]- downloadQueue.operationQueue.operationCount);
    
//    if([[self.allUrlSSS[0] allValues].lastObject count] >= 0 && downloadQueue.operationQueue.operationCount >=0 && [[self.allUrlSSS[0] allValues].lastObject count] - downloadQueue.operationQueue.operationCount >= 0){
    
//    CURRENTWLAN
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"DOWNLOADBUTTON"] isEqualToString:@"1"]||_isDownLoad||[[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"1"]){
        if (self.allUrlSSS.count > 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changeCellProgress" object:@{@"proprtion": @((float)([[self.allUrlSSS[0] allValues].lastObject count] - downloadQueue.operationQueue.operationCount)/([[self.allUrlSSS[0] allValues].lastObject count])),@"tag":downloadQueue.chapterId} userInfo:nil];}
        
        if(downloadQueue.operationQueue.operationCount == 0 && self.allUrlSSS.count > 0){
            [[DataBaseHelper shared] updateChapter:[self.allUrlSSS[0] allKeys].lastObject];
            [self.allCartoonId removeObjectAtIndex:0];
            [self.targetPathArr removeObjectAtIndex:0];
            [self.allUrlSSS removeObjectAtIndex:0];
            [downloadQueue.operationQueue cancelAllOperations];
            if (self.allUrlSSS.count > 0) {
                GSDownloadQueue *downloadQueueNext = [[GSDownloadQueue alloc] init];
                downloadQueueNext.delegate = self;
                downloadQueueNext.chapterId = [self.allUrlSSS[0] allKeys].lastObject;
                downloadQueueNext.cartoonId = self.allCartoonId[0];
                [downloadQueueNext addOperationWithURLs:[self.allUrlSSS[0] allValues].lastObject target:self.targetPathArr[0]
                                              cartoonId:self.allCartoonId[0]
                                              chapterId:[self.allUrlSSS[0] allKeys].lastObject];
                [_allQueue addObject:downloadQueue];
            }
        }
        
    }
    
}

- (void)downloadQueueFail:(GSDownloadQueue *) downloadQueue chapterInfo:(NSDictionary *)info{
    
}




#pragma mark - getters
- (NSMutableDictionary *)dispatchCartoon
{
    if (_dispatchCartoon == nil) {
        _dispatchCartoon = [[NSMutableDictionary alloc] init];
    }
    return _dispatchCartoon;
}

- (NSMutableArray *)allQueue{
    if (_allQueue == nil) {
        _allQueue = [[NSMutableArray alloc] init];
    }
    return _allQueue;
}

- (NSMutableArray *)allUrlSSS{
    if (_allUrlSSS == nil) {
        _allUrlSSS = [[NSMutableArray alloc] init];
    }
    return _allUrlSSS;
}

- (NSMutableArray *)targetPathArr{
    if (_targetPathArr == nil) {
        _targetPathArr = [[NSMutableArray alloc] init];
    }
    return _targetPathArr;

}

- (NSMutableArray *)allCartoonId{
    if (_allCartoonId == nil) {
        _allCartoonId = [[NSMutableArray alloc] init];
    }
    return _allCartoonId;

}

@end
