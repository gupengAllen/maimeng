//
//  DownListCollectionViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/23.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "DownListCollectionViewCell.h"
#import "cartoonIdModel.h"
#import "UIImageView+AFNetworking.h"
#import "UAProgressView.h"
#import "DataBaseHelper.h"
#import "chapterIdModel.h"

#import "Config.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@implementation DownListCollectionViewCell


//- (instancetype)initWithFrame:(CGRect)frame WithImageUrlName:(cartoonIdModel *)cartoonIdModel WithPotition:(CGFloat)progressPotition {
//    if (self = [super initWithFrame:frame]) {
//        [super awakeFromNib];
//        _cartoonName.text = cartoonIdModel.name;
//        [_imageInfo setImageWithURL:[NSURL URLWithString:cartoonIdModel.image]];
//    }
//    return self;
//}

//- (instancetype)init{
//    if (self = [super init]) {

 
//    }
//    return self;
//}



- (void)awakeFromNib {
// Initialization code
//    self.progressView.tintColor = [UIColor colorWithRed:234/255.0 green:74/255.0 blue:97/255.0 alpha:0.8];
    self.progressView.borderWidth = 3.0;
    self.progressView.lineWidth = 3.0;
    _imageInfo.layer.cornerRadius = 4.0;
    _imageInfo.layer.masksToBounds = YES;
    _progressBottomView.layer.cornerRadius = 4.0;
    

//    _add = [UIButton buttonWithType:UIButtonTypeCustom];
//    _add.frame = CGRectMake(self.imageInfo.width - 30, 0, 30, 30);
//    [_add setImage:[UIImage imageNamed:@"recipe_normal"] forState:UIControlStateNormal];
//    [_add setImage:[UIImage imageNamed:@"recipe_add"] forState:UIControlStateSelected];
//    _add.hidden = YES;
//    
//    [_imageInfo addSubview:_add];

    
    
    
    
    
    _progressBottomView.layer.masksToBounds = YES;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    _add.selected = isSelected;
}


- (void)setCartoonIdModel:(cartoonIdModel *)cartoonIdModel{

    
    
    NSArray *cartoonArr = [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:cartoonIdModel.cartoonId];
    NSMutableArray *carStatusArr = [NSMutableArray array];
    for (chapterIdModel *chapterIdModel in cartoonArr) {
        if ([chapterIdModel.status isEqualToString:@"1"]) {
            [carStatusArr addObject:chapterIdModel];
        }
    }
    if ((float)(carStatusArr.count/cartoonArr.count) == 1) {
        _progressBottomView.hidden = YES;
    }
    [_progressView setProgress:((float)carStatusArr.count/cartoonArr.count)];
    _progressLabel.text = [NSString stringWithFormat:@"%.f%%",((float)carStatusArr.count/cartoonArr.count) * 100];
    
    _cartoonName.text = cartoonIdModel.name;
    [_imageInfo setImageWithURL:[NSURL URLWithString:cartoonIdModel.image]];
    self.imageInfo.frame = CGRectMake(0, 0, (KScreenWidth-24)/3, (KScreenWidth-24)/3*___Scale);
    self.cartoonName.frame = CGRectMake(0, _imageInfo.bottom, self.bounds.size.width, 20);
    
}

- (void)setProgress:(CGFloat)progress{
    _progressLabel.text = [NSString stringWithFormat:@"%.f%%",progress * 100];
    [self.progressView setProgress:progress];
}

- (void)setIsCommicHidden:(BOOL)isCommicHidden{
    
}

@end
