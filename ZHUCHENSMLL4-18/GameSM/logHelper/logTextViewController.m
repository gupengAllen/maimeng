//
//  logTextViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/20.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "logTextViewController.h"

@interface logTextViewController ()

@end

@implementation logTextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addBackBtn];
    _logTextView.text = [[LogHelper shared] indicateAllLog];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)lastLog:(id)sender {
    [_logTextView scrollRectToVisible:CGRectMake(0, _logTextView.contentSize.height-15, _logTextView.contentSize.width, 10) animated:YES];
}
@end
