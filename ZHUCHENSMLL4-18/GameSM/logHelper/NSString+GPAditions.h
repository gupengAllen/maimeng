//
//  NSString+GPAditions.h
//  GameSM
//
//  Created by 顾鹏 on 16/1/5.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GPAditions)
+(NSString *) jsonStringWithString:(NSString *) string;
+(NSString *) jsonStringWithArray:(NSArray *)array;
+(NSString *) jsonStringWithDictionary:(NSDictionary *)dictionary;
+(NSString *) jsonStringWithObject:(id) object;
@end
