//
//  CartoonInfoAll.h
//  GameSM
//
//  Created by 顾鹏 on 15/12/25.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
//"create table if not exists cartoonInfo(id varchar(64) primary key ,name varchar(64),images varchar(128),author varchar(64),introduction varchar(256),priority varchar(64),totalChapterCount varchar(64),updateInfo varchar(64),isRead int,ReadChapterId varchar(64),readAlbumId varchar(64),lastReadTime varchar(64),isCollection int,collectionTime varchar(64),lastSynTime varchar(64)
@interface CartoonInfoAll : NSObject
@property (nonatomic, copy) NSString *id;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *images;
@property (nonatomic, copy) NSString *author;
@property (nonatomic, copy) NSString *introduction;
@property (nonatomic, copy) NSString *priority;
@property (nonatomic, copy) NSString *totalChapterCount;
@property (nonatomic, copy) NSString *updateInfo;
@property (nonatomic, copy) NSString *isRead;
@property (nonatomic, copy) NSString *ReadChapterId;
@property (nonatomic, copy) NSString *currentReadAlbumId;
@property (nonatomic, copy) NSString *lastReadTime;
@property (nonatomic, copy) NSString *collectionStatus;
@property (nonatomic, copy) NSString *collectTime;
@property (nonatomic, copy) NSString *lastSynTime;
@property (nonatomic, copy) NSString *readInfo;
@property (nonatomic, copy) NSString *chapterIndex;
@property (nonatomic, copy) NSString *currentReadChapterId;
@property (nonatomic, copy) NSString *recentUpdateTime;
@property (nonatomic, copy) NSString *updateValueLabel;
@property (nonatomic, copy) NSString *categorys;
@property (nonatomic, copy) NSString *categoryLabel;
+ (NSMutableArray *)paraCartoonInfoAll:(NSDictionary *)dict andReadSaveType:(NSString *)type;
+ (CartoonInfoAll *)paraCartoonInfoAllSingle:(NSDictionary *)cartoon;
+ (NSMutableArray *)paraCartoonInfoAll:(NSDictionary *)dict ;
@end
