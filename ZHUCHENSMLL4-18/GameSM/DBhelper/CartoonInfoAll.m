//
//  CartoonInfoAll.m
//  GameSM
//
//  Created by 顾鹏 on 15/12/25.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CartoonInfoAll.h"
#import "DataBaseHelper.h"
#import "NSDate+OTS.h"
@implementation CartoonInfoAll
+ (NSMutableArray *)paraCartoonInfoAll:(NSDictionary *)dict andReadSaveType:(NSString *)type{
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];//实例化一个NSDateFormatter对象
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];//设定时间格式,这里可以设置成自己需要的格式
    
    NSArray *cartoonInfo = dict[@"results"];
    NSMutableArray *cartoonArr = [NSMutableArray array];
    for (NSDictionary *cartoon in cartoonInfo) {
        CartoonInfoAll *cartoonInfo = [[CartoonInfoAll alloc] init];
        cartoonInfo.id = cartoon[@"id"];
        cartoonInfo.name = cartoon[@"name"];
        cartoonInfo.images = cartoon[@"images"];
        cartoonInfo.author = cartoon[@"author"];
        cartoonInfo.introduction = cartoon[@"introduction"];
        cartoonInfo.chapterIndex = cartoon[@"currentReadChapterIndex"];
        cartoonInfo.currentReadChapterId = cartoon[@"currentReadChapterId"];
        cartoonInfo.recentUpdateTime = cartoon[@"recentUpdateTime"];
        cartoonInfo.currentReadAlbumId = cartoon[@"currentReadAlbumId"];
        cartoonInfo.updateValueLabel = cartoon[@"updateValueLabel"];
        cartoonInfo.categorys = cartoon[@"categorys"];
        if ([type isEqualToString:@"ISREAD"]) {
            cartoonInfo.isRead = @"1";
            cartoonInfo.collectionStatus = @"0";
            cartoonInfo.lastReadTime = [NSString stringWithFormat:@"%f",[[dateFormat dateFromString:cartoon[@"readTime"]] timeIntervalSince1970]];
        }else if([type isEqualToString:@"ISCOLLECTION"]){
            cartoonInfo.isRead = @"0";
            cartoonInfo.collectionStatus = @"1";
            cartoonInfo.collectTime = [NSString stringWithFormat:@"%f",[[dateFormat dateFromString:cartoon[@"collectTime"]] timeIntervalSince1970]];
            
        }
        cartoonInfo.updateInfo = cartoon[@"updateInfo"];
        [[DataBaseHelper shared] insertExistCartoonInfo:cartoonInfo andValue:@{([type isEqualToString:@"ISREAD"]?@"ISREAD":@"ISCOLLECTION"):@"1"}];
        [cartoonArr addObject:cartoonInfo];
    }
    return cartoonArr;
}

+ (NSMutableArray *)paraCartoonInfoAll:(NSDictionary *)dict {
    NSArray *cartoonInfo = dict[@"results"];
    NSMutableArray *cartoonArr = [NSMutableArray array];
    for (NSDictionary *cartoon in cartoonInfo) {
        CartoonInfoAll *cartoonInfo = [[CartoonInfoAll alloc] init];
        cartoonInfo.id = cartoon[@"id"];
        cartoonInfo.name = cartoon[@"name"];
        cartoonInfo.images = cartoon[@"images"];
        cartoonInfo.author = cartoon[@"author"];
        cartoonInfo.introduction = cartoon[@"introduction"];
        cartoonInfo.updateInfo = cartoon[@"updateInfo"];
        [cartoonArr addObject:cartoonInfo];
    }
    return cartoonArr;
    
}





+ (CartoonInfoAll *)paraCartoonInfoAllSingle:(NSDictionary *)cartoon {
        CartoonInfoAll *cartoonInfo = [[CartoonInfoAll alloc] init];
        cartoonInfo.id = cartoon[@"id"];
        cartoonInfo.name = cartoon[@"name"];
        cartoonInfo.images = cartoon[@"images"];
        cartoonInfo.author = cartoon[@"author"];
        cartoonInfo.introduction = cartoon[@"introduction"];
        cartoonInfo.isRead = @"0";
        cartoonInfo.collectionStatus = @"0";
        cartoonInfo.updateInfo = cartoon[@"updateInfo"];
        cartoonInfo.chapterIndex = cartoon[@"currentReadChapterIndex"];
        cartoonInfo.currentReadChapterId = cartoon[@"currentReadChapterId"];
        cartoonInfo.recentUpdateTime = cartoon[@"recentUpdateTime"];
        cartoonInfo.currentReadAlbumId = cartoon[@"currentReadAlbumId"];
        cartoonInfo.updateValueLabel = cartoon[@"updateValueLabel"];
        cartoonInfo.categorys = cartoon[@"categorys"];
        cartoonInfo.categoryLabel = cartoon[@"categoryLabel"];
    return cartoonInfo;

}

@end
