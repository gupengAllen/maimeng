//
//  chapterIdModel.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface chapterIdModel : NSObject
@property (nonatomic,copy)NSString *chapterId;
@property (nonatomic,copy)NSString *chapterName;
@property (nonatomic,copy)NSString *cartoonId;
@property (nonatomic,copy)NSString *status;
@end
