//
//  DataBaseHelper.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/11.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "DataBaseHelper.h"
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabaseQueue.h"
#import <objc/runtime.h>
#import "cartoonDetailModel.h"
#import "cartoonChapterListModel.h"
#import "cartoonIdModel.h"
#import "chapterIdModel.h"
#import "cartoonDetailModel.h"
#import "RecommentModel.h"
#import "BookModel.h"
#import "CartoonInfoAll.h"
#import "cartoonChapterListModel.h"
#import "NSString+GPAditions.h"
#import "YK_API_request.h"
#import "Y_X_DataInterface.h"
static DataBaseHelper *_DataShare = nil;

@implementation DataBaseHelper

{
    FMDatabase *_fmdb;
}

+ (DataBaseHelper *)shared;
{
    @synchronized(self)
    {
        if(!_DataShare){
            _DataShare = [[DataBaseHelper alloc] init];
        }
        return _DataShare;
    }
}
- (id)init
{
    if (self = [super init]) {
        
        NSString *path = [[NSHomeDirectory() stringByAppendingPathComponent:@"Documents"]stringByAppendingPathComponent:@"GAMESM.db"];
        NSLog(@"%@",path);
        _fmdb = [[FMDatabase alloc]initWithPath:path];
        if ( [_fmdb open]) {
            [self createCommicWhole];
            [self createCommicOne];
            [self createAlbumTable];
            [self createDate];
//            [self createContinueWatch];
//            [self createSaveHistory];
            [self createCartoonInfo];
            [self createCartoonDetail];
//            [self createOtherRelationShipTable];
        };
        
    }
    return self;
}


//@interface cartoonChapterListModel : NSObject
//@property(nonatomic,copy)NSString *id;
//@property(nonatomic,copy)NSString *name;
//@property(nonatomic,copy)NSString *priority;
//@property(nonatomic,copy)NSString *status;
//@property(nonatomic,copy)NSString *downloadStatus;
//@property(nonatomic,copy)NSString *readStatus;
//@property(nonatomic,copy)NSString *totalChapterSize;

- (void)createCartoonDetail {
    NSString *sql = @"create table if not exists cartoonDetail(id varchar(64) primary key,cartoonId varchar(64),cartoonName varchar(128),priority varchar(64))";
    BOOL isCreate = [_fmdb executeUpdate:sql];
    
}

- (void)createDate {
    NSString *sql = @"create table if not exists OpenId(dateTime int primary key)";
    BOOL isCreate = [_fmdb executeUpdate:sql];
    
}

- (void)addDateTime:(NSInteger)dateTime{
    NSString *sql = @"insert into OpenId(dateTime) values (?)";
    BOOL isSave= [_fmdb executeUpdate:sql,@(dateTime)];
    if (isSave) {
        //        NSLog(@"================>插入漫画集成功");
    }
    
}

/**
 *  创建漫画集的all info
 */
- (void)createCartoonInfo {
    NSString *sql = @"create table if not exists cartoonAllInfo(id varchar(64) primary key ,name varchar(64),images varchar(128),author varchar(64),introduction varchar(256),priority varchar(64),totalChapterCount varchar(64),updateInfo varchar(64),isRead varchar(64),ReadChapterId varchar(64),currentReadAlbumId varchar(64),lastReadTime varchar(64),collectionStatus varchar(64),collectTime varchar(64),lastSynTime varchar(64),chapterIndex varchar(64),currentReadChapterId varchar(64),recentUpdateTime varchar(64),updateValueLabel varchar(64))";
    BOOL isCreate = [_fmdb executeUpdate:sql];
    if(isCreate){
        
    }
    
//    if ([_fmdb columnExists:@"categorys" inTableWithName:@"cartoonAllInfo"]) {
    if(![_fmdb columnExists:@"categorys" columnName:@"cartoonAllInfo"]){
    NSString *sqlAlert = @"ALTER TABLE cartoonAllInfo ADD  categorys varchar(64)";
        [_fmdb executeUpdate:sqlAlert];
    }
    
    
}

- (void)insertCartoonInfo:(CartoonInfoAll *)cartoonInfoAll {
    NSString *sql = @"insert into cartoonAllInfo(id,name,images,author,introduction,priority,totalChapterCount,updateInfo,isRead,ReadChapterId,currentReadAlbumId,lastReadTime,collectionStatus,collectTime,lastSynTime,chapterIndex,currentReadChapterId,recentUpdateTime,updateValueLabel,categorys) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    BOOL isSave= [_fmdb executeUpdate:sql,cartoonInfoAll.id,cartoonInfoAll.name,cartoonInfoAll.images,cartoonInfoAll.author,cartoonInfoAll.introduction,cartoonInfoAll.priority,cartoonInfoAll.totalChapterCount,cartoonInfoAll.updateInfo,cartoonInfoAll.isRead,cartoonInfoAll.ReadChapterId,cartoonInfoAll.currentReadAlbumId,cartoonInfoAll.lastReadTime,cartoonInfoAll.collectionStatus,cartoonInfoAll.collectTime,cartoonInfoAll.lastSynTime,cartoonInfoAll.chapterIndex,cartoonInfoAll.currentReadChapterId,cartoonInfoAll.recentUpdateTime,cartoonInfoAll.updateValueLabel,cartoonInfoAll.categorys];
    if (isSave) {
        //        NSLog(@"================>插入漫画集成功");
    }
}

- (void)updateCartoonInfoCodition:(NSString *)id andValue:(NSDictionary *)valueD {
    NSString *sql =  [NSString stringWithFormat:@"update cartoonAllInfo set %@ where id=%@",[self lastCommon:valueD],id];
    
    BOOL isUpdate = [_fmdb executeUpdate:sql];
}

- (BOOL)isExistChapList:(NSString *)id {
    NSString *sql = @"select * from cartoonDetail where cartoonId = ?";
    FMResultSet *set = [_fmdb executeQuery:sql,id];
    if ([set next]) {
        return YES;
    }else{
        return NO;
    }

}

- (void)dropTableView {
    NSString *sql = @"delete from cartoonAllInfo";
    BOOL isDeleteCartoonAllInfo = [_fmdb executeUpdate:sql];
    
    NSString *sqlOne = @"delete from cartoonDetail";
    BOOL isDeleteCartoonDetail = [_fmdb executeUpdate:sqlOne];
    
}



- (BOOL)isExistCartoonAllInfo:(NSString *)id {
    NSString *sql = @"select * from cartoonAllInfo where id = ?";
    FMResultSet *set = [_fmdb executeQuery:sql,id];
    if ([set next]) {
        return YES;
    }else{
        return NO;
    }
}

- (void)insertExistCartoonInfo:(CartoonInfoAll *)cartoonInfoAll andValue:(NSDictionary *)valueD{
    if ([self isExistCartoonAllInfo:cartoonInfoAll.id]) {
        [self updateCartoonInfoCodition:cartoonInfoAll.id andValue:valueD ];
    }else {
        [self insertCartoonInfo:cartoonInfoAll];
    }
}

/**
 *  收藏历史
 *
 *  @return
 */
- (void)createSaveHistory {
    NSString *sql = @"create table if not exists saveHistory(id varchar(64) primary key ,name varchar(64),images varchar(128),author varchar(64),introduction varchar(256),priority varchar(64))";
    BOOL isCreate = [_fmdb executeUpdate:sql];
    
}

/**
 *  继续撸
 *
 *  @return
 */

- (void)createContinueWatch {
    NSString *sql = @"create table if not exists continueWatch(id varchar(64) primary key ,name varchar(64),images varchar(128),author varchar(64),introduction varchar(256),priority varchar(64),chapterId varchar(64),chapterName varchar(64),chapterNameLabel varchar(64),chapterIndex varchar(64),albumId varchar(64),updateInfo varchar(64))";
    BOOL isCreate = [_fmdb executeUpdate:sql];
    
}



/**
 *  创建漫画集表
 */
- (void)createCommicWhole {
    NSString *sql = @"create table if not exists commicCartoon(cartoonId varchar(64) primary key ,name varchar(64),image varchar(64),idAllStr varchar(64))";
   BOOL isCreate = [_fmdb executeUpdate:sql];
}
/**
 *  创建漫画话表
 */
- (void)createCommicOne {
    NSString *sql = @"create table if not exists commicChapter(chapterId varchar(64) primary key ,chapterName varchar(64),cartoonId varchar(64),status char)";
    [_fmdb executeUpdate:sql];
}


/**
 *  创建一张张漫画信息表
 */
- (void)createAlbumTable {
    NSString *sql = @"create table if not exists albumInfo(albumId varchar(64) primary key ,images varchar(128),imgWidth int,imgHeight int,chapterName varchar(128),propotion float,chapterId varchar(64),cartoonId varchar(64),status char,lastChapterId varchar(64),nextChapterId varchar(64),countTotal varchar(64),currentAlbumId varchar(64),priority varchar(64),chapterIndex varchar(64))";
   BOOL isCreate = [_fmdb executeUpdate:sql];
    
}

/**
 *  创建关系表
 */
- (void)createOtherRelationShipTable {
    NSString *sql = @"create table if not exists albumRelationShip(albumId varchar(64) primary key ,cartoonId varchar(64),chapterId varchar(64),status char)";
    [_fmdb executeUpdate:sql];
    
}



-(BOOL)isExistsDownLoad:(NSString *)chapterId
{
    NSString *sql = @"select status from myapp where chapterId = ?";
    FMResultSet *set = [_fmdb executeQuery:sql,chapterId];
    if ([set next]) {
        return YES;
    }else{
        return NO;
    }
}

//NSString *sql = @"create table if not exists cartoonDetail(id varchar(64) primary key,cartoonId varchar(64),cartoonName varchar(128),priority varchar(64))";


- (NSMutableArray *)fetchCartoonChapList:(NSDictionary *)valueList{
    NSMutableArray *cartoonChapListArr = [[NSMutableArray alloc] init];
    NSString *cartoonChapListSql = [NSString stringWithFormat:@"select * from cartoonDetail where %@=%@",[valueList allKeys][0],[valueList allValues][0]];
    FMResultSet *set = [_fmdb executeQuery:cartoonChapListSql];
    while ([set next]) {
        cartoonChapterListModel *cartoonModel = [[cartoonChapterListModel alloc] init];
        cartoonModel.id = [set stringForColumn:@"id"];
        cartoonModel.name = [set stringForColumn:@"cartoonName"];
        cartoonModel.cartoonId = [set stringForColumn:@"cartoonId"];
        cartoonModel.priority = [set stringForColumn:@"priority"];
        [cartoonChapListArr addObject:cartoonModel];
    }
    return cartoonChapListArr;

}

//NSString *sql = @"create table if not exists cartoonDetail(id varchar(64) primary key,cartoonId varchar(64),cartoonName varchar(128),priority varchar(64))";

- (void)insertChapList:(cartoonChapterListModel *)chapModel {
    NSString *sql = @"insert into cartoonDetail(id,cartoonId,cartoonName,priority) values (?,?,?,?)";
    BOOL isSave= [_fmdb executeUpdate:sql,chapModel.id,chapModel.cartoonId,chapModel.cartoonName,chapModel.priority];
    if (isSave) {
        //        NSLog(@"================>插入漫画集成功");
    }

}

- (void)insertSaveHistory:(BookModel *)bookModel {
    NSString *sql = @"insert into saveHistory(id,name,images,author,introduction,priority) values (?,?,?,?,?,?)";
    BOOL isSave= [_fmdb executeUpdate:sql,bookModel.id,bookModel.name,bookModel.images,bookModel.author,bookModel.introduction,bookModel.priority];
    if (isSave) {
        //        NSLog(@"================>插入漫画集成功");
    }
    
}

//d varchar(64) primary key ,name varchar(64),images varchar(128),author varchar(64),introduction varchar(256),priority varchar(64),chapterId varchar(64),chapterName varchar(64),chapterNameLabel varchar(64),chapterIndex varchar(64),albumId varchar(64),updateInfo varchar(64
/**
 *  插入继续撸
 *
 *  @param recommentModel <#recommentModel description#>
 */
- (void)insertContinueWatch:(RecommentModel *)recommentModel {
    NSString *sql = @"insert into continueWatch(id,name,images,author,introduction,priority,chapterId,chapterName,chapterNameLabel,chapterIndex,albumId,updateInfo) values (?,?,?,?,?,?,?,?,?,?,?,?)";
    BOOL isSave= [_fmdb executeUpdate:sql,recommentModel.id,recommentModel.name,recommentModel.images,recommentModel.author,recommentModel.introduction,recommentModel.priority,recommentModel.chapterId,recommentModel.chapterName,recommentModel.chapterNameLabel,recommentModel.chapterIndex,recommentModel.albumId,recommentModel.updateInfo];
    if (isSave) {
        //        NSLog(@"================>插入漫画集成功");
    }
    
}




/**
 *  插入漫画集
 */
- (void)insertCartoonId:(NSString *)cartoonId Name:(NSString *)name Image:(NSString *)image IdAllStr:(NSString *)idAllStr{
    NSString *sql = @"insert into commicCartoon(cartoonId,name,image,idAllStr) values (?,?,?,?)";
    BOOL isSave= [_fmdb executeUpdate:sql,cartoonId,name,image,idAllStr];
    if (isSave) {
//        NSLog(@"================>插入漫画集成功");
    }
}


/**
 *  插入漫画话
 *
 *  @param model
 */
- (void)insertCharpterModel:(cartoonDetailModel *)model andStatus:(NSString *)status {
    NSString *sql = @"insert into commicChapter(chapterId,chapterName,cartoonId,status) values (?,?,?,?)";
    BOOL isSave= [_fmdb executeUpdate:sql,model.chapterId,model.chapterName,model.cartoonId,status];
    if (isSave) {
        //        NSLog(@"================>插入漫画话成功");
    }
}

/**
 *  插入图片信息
 *
 *  @param model
 */

-(void)insertAlbumModel:(cartoonDetailModel *)model {
    NSString *sql = @"insert into albumInfo(albumId,images,imgWidth,imgHeight,chapterName,propotion,chapterId,cartoonId,lastChapterId,nextChapterId,currentAlbumId,priority,countTotal,chapterIndex) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    if (model.lastChapterId == [NSNull null]) {
        model.lastChapterId = @"";
    }
    if (model.nextChapterId == [NSNull null]) {
        model.nextChapterId = @"";
    }
    BOOL isSave= [_fmdb executeUpdate:sql,model.id,model.images,@(model.imgWidth),@(model.imgHeight),model.chapterName,@(model.propotion),model.chapterId,model.cartoonId,model.lastChapterId,model.nextChapterId,model.currentAlbumId,model.priority,model.countTotal,model.chapterIndex];
    if (isSave) {
//        NSLog(@"================>插入图片成功");
    }
    
}






/**
 *  图片相关信息
 *
 *  @param model
 *  @param status 下载状态
 */

//- (void)insertAlbumRelationShip:(cartoonDetailModel *)model Status:(BOOL) status {
//    NSString *sql = @"insert into albumRelationShip(albumId,cartoonId,chapterId,status) values (?,?,?,?)";
//    BOOL isSave= [_fmdb executeUpdate:sql,model.id,model.cartoonId,model.chapterId,status];
//    if (isSave) {
//        NSLog(@"================>插入图片相关成功");
//    }
//
//
//}

- (void)updateInfoCondition:(NSString *)id updateInfo:(NSString *)updateInfo{
    NSString *sql =  [NSString stringWithFormat:@"update cartoonAllInfo set updateInfo=%@ where id=%@",updateInfo,id];
    BOOL isUpdate = [_fmdb executeUpdate:sql];

}

- (void)updateChapter:(NSString *)chapterId{
    NSString *sql =  [NSString stringWithFormat:@"update commicChapter set status=%@ where chapterId=%@",@"1",chapterId];
    BOOL isUpdate = [_fmdb executeUpdate:sql];
    
}

- (NSMutableArray *)fetchCartoonAllInfo {
    NSMutableArray *cartoonAllInfoArr = [[NSMutableArray alloc] init];
    NSString *cartoonAllInfoSql = @"select * from cartoonAllInfo";
    FMResultSet *set = [_fmdb executeQuery:cartoonAllInfoSql];
    while ([set next]) {
        [cartoonAllInfoArr addObject:[set stringForColumn:@"id"]];
    }
    return cartoonAllInfoArr;
    
}

- (NSMutableString *)fetchIdMore{
    NSMutableArray *bookCollectionArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"collectionStatus":@"1"} andChooseType:@"collectTime"];
    NSMutableArray *categoryIdArr = [NSMutableArray array];
    for (CartoonInfoAll *cartoonAll in bookCollectionArr) {
        [categoryIdArr addObjectsFromArray: [cartoonAll.categorys componentsSeparatedByString:@","]];
    }
    
    
    NSMutableArray *dateMutablearray = [NSMutableArray array];
    
    for (int i = 0; i < categoryIdArr.count; i ++) {
        
        NSString *string = categoryIdArr[i];
        
        NSMutableArray *tempArray = [@[] mutableCopy];
        
        [tempArray addObject:string];
        
        for (int j = i+1; j < categoryIdArr.count; j ++) {
            
            NSString *jstring = categoryIdArr[j];
            
            
            if([string isEqualToString:jstring]){
                
                
                [tempArray addObject:jstring];
                
                [categoryIdArr removeObjectAtIndex:j];
                
            }
            
        }
        
        [dateMutablearray addObject:tempArray];
        
    }
    
     [dateMutablearray sortUsingComparator:^NSComparisonResult(NSArray *obj1, NSArray *obj2) {
        /*
         NSOrderedAscending = -1L, // 右边的对象排后面
         NSOrderedSame, // 一样
         NSOrderedDescending // 左边的对象排后面
         */
        
        
        if (obj1.count > obj2.count) { // obj1排后面
            return NSOrderedDescending;
        } else { // obj1排前面
            return NSOrderedAscending;
        }
    }];
    
    NSMutableString *cateIdStr = [NSMutableString string];
    if (dateMutablearray.count > 3) {
    for (int i = 0; i < 3; i ++) {
        if (dateMutablearray.count > 1) {
            [cateIdStr appendString:[NSString stringWithFormat:@"%@",dateMutablearray[dateMutablearray.count -i - 1][0]]];
            if (i < 2) {
                [cateIdStr appendString:@","];
            }
        }
    }
    }
    
    
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:@{@"r":API_URL_UPCATEGROY,@"categorys":cateIdStr,@"isNew":@"1"} object:self action:@selector(upCollectionData:) method:POSTDATA];
    
    return cateIdStr;
    
}

- (void)upCollectionData:(NSDictionary *)dict{
    
}


- (NSMutableArray *)fetchCartoonAllInfoWithCondition:(NSDictionary *) conditionDict andChooseType:(NSString *)chooseType{
    NSMutableArray *cartoonAllInfoArr = [[NSMutableArray alloc] init];
    NSString *cartoonAllInfoSql = [NSString stringWithFormat:@"select * from cartoonAllInfo where %@=%@ order by %@ DESC",[conditionDict allKeys][0],[conditionDict allValues][0],chooseType] ;
    FMResultSet *set = [_fmdb executeQuery:cartoonAllInfoSql];
    while ([set next]) {
        CartoonInfoAll *cartoonInfoModel = [[CartoonInfoAll alloc] init];
        cartoonInfoModel.id = [set stringForColumn:@"id"];
        cartoonInfoModel.name = [set stringForColumn:@"name"];
        cartoonInfoModel.images = [set stringForColumn:@"images"];
        cartoonInfoModel.author = [set stringForColumn:@"author"];
        cartoonInfoModel.introduction = [set stringForColumn:@"introduction"];
        cartoonInfoModel.priority = [set stringForColumn:@"priority"];
        cartoonInfoModel.totalChapterCount = [set stringForColumn:@"totalChapterCount"];
        cartoonInfoModel.updateInfo = [set stringForColumn:@"updateInfo"];
        cartoonInfoModel.isRead = [set stringForColumn:@"isRead"];
        cartoonInfoModel.ReadChapterId = [set stringForColumn:@"ReadChapterId"];
        cartoonInfoModel.currentReadAlbumId = [set stringForColumn:@"currentReadAlbumId"];
        cartoonInfoModel.lastReadTime = [set stringForColumn:@"lastReadTime"];
        cartoonInfoModel.collectionStatus = [set stringForColumn:@"collectionStatus"];
        cartoonInfoModel.collectTime = [set stringForColumn:@"collectTime"];
        cartoonInfoModel.lastSynTime = [set stringForColumn:@"lastSynTime"];
        cartoonInfoModel.chapterIndex = [set stringForColumn:@"chapterIndex"];
        cartoonInfoModel.currentReadChapterId = [set stringForColumn:@"currentReadChapterId"];
        cartoonInfoModel.recentUpdateTime = [set stringForColumn:@"recentUpdateTime"];
        cartoonInfoModel.updateValueLabel = [set stringForColumn:@"updateValueLabel"];
        cartoonInfoModel.categorys = [set stringForColumn:@"categorys"];
        [cartoonAllInfoArr addObject:cartoonInfoModel];
    }
    return cartoonAllInfoArr;
}

- (NSString *)fetchCartoonAllCollectionStatus{
    NSMutableString *cartoonStr = [NSMutableString string];
    NSMutableArray *carArr = [NSMutableArray array];
    NSString *cartoonAllInfoSql = [NSString stringWithFormat:@"select * from cartoonAllInfo"] ;
    [cartoonStr appendString:@"["];
    FMResultSet *set = [_fmdb executeQuery:cartoonAllInfoSql];
    while ([set next]) {
        CartoonInfoAll *cartoonInfoModel = [[CartoonInfoAll alloc] init];
        cartoonInfoModel.id = [set stringForColumn:@"id"];
        cartoonInfoModel.collectionStatus = [set stringForColumn:@"collectionStatus"];
        cartoonInfoModel.collectTime = [set stringForColumn:@"collectTime"];
        [carArr addObject:cartoonInfoModel];
    }
    
    for (int i = 0; i < carArr.count; i ++) {
        [cartoonStr appendString:[NSString jsonStringWithDictionary:[self entityToDictionary:carArr[i]]]];
        if (i < carArr.count - 1) {
            [cartoonStr appendString:@","];
        }
    }
    
    [cartoonStr appendString:@"]"];
    NSLog(@"%@",cartoonStr);
    return cartoonStr;
    
    
}


- (NSString *)fetchCartoonAllReadStatus{
    NSMutableString *cartoonStr = [NSMutableString string];
    NSMutableArray *carArr = [NSMutableArray array];
    NSString *cartoonAllInfoSql = [NSString stringWithFormat:@"select * from cartoonAllInfo where isRead=1"] ;
    [cartoonStr appendString:@"["];
    
    FMResultSet *set = [_fmdb executeQuery:cartoonAllInfoSql];
    while ([set next]) {
        CartoonInfoAll *cartoonInfoModel = [[CartoonInfoAll alloc] init];
        cartoonInfoModel.id = [set stringForColumn:@"id"];
        cartoonInfoModel.currentReadChapterId = [set stringForColumn:@"currentReadChapterId"];
        cartoonInfoModel.currentReadAlbumId = [set stringForColumn:@"currentReadAlbumId"];
        cartoonInfoModel.lastReadTime = [set stringForColumn:@"lastReadTime"];
//        [cartoonStr appendString:[NSString jsonStringWithDictionary:[self entityToDictionary:cartoonInfoModel]]];
        [carArr addObject:cartoonInfoModel];
        
    }
    for (int i = 0; i < carArr.count; i ++) {
        [cartoonStr appendString:[NSString jsonStringWithDictionary:[self entityToDictionary:carArr[i]]]];
        if (i < carArr.count - 1) {
            [cartoonStr appendString:@","];
        }
    }
    
    [cartoonStr appendString:@"]"];
    NSLog(@"%@",cartoonStr);
    return cartoonStr;
    
    
}




- (NSString *)fetchCartoonPartCollectionStatus{
    NSMutableString *cartoonStr = [NSMutableString string];
    NSMutableArray *carArr = [NSMutableArray array];
    NSString *cartoonAllInfoSql = [NSString stringWithFormat:@"select * from cartoonAllInfo where collectTime > %@ and collectTime < %.f",[[NSUserDefaults standardUserDefaults] objectForKey:@"SYNTIME"],[[NSDate date] timeIntervalSince1970]] ;
    [cartoonStr appendString:@"["];
    FMResultSet *set = [_fmdb executeQuery:cartoonAllInfoSql];
    while ([set next]) {
        CartoonInfoAll *cartoonInfoModel = [[CartoonInfoAll alloc] init];
        cartoonInfoModel.id = [set stringForColumn:@"id"];
        cartoonInfoModel.collectionStatus = [set stringForColumn:@"collectionStatus"];
        cartoonInfoModel.collectTime = [set stringForColumn:@"collectTime"];
        [carArr addObject:cartoonInfoModel];
    }
    
    for (int i = 0; i < carArr.count; i ++) {
        [cartoonStr appendString:[NSString jsonStringWithDictionary:[self entityToDictionary:carArr[i]]]];
        if (i < carArr.count - 1) {
            [cartoonStr appendString:@","];
        }
    }
    
    [cartoonStr appendString:@"]"];
    NSLog(@"%@",cartoonStr);
    return cartoonStr;
    

}


- (NSString *)fetchCartoonPartReadStatus{
    NSMutableString *cartoonStr = [NSMutableString string];
    NSMutableArray *carArr = [NSMutableArray array];
    NSString *cartoonAllInfoSql = [NSString stringWithFormat:@"select * from cartoonAllInfo where lastReadTime > %@ and lastReadTime < %.f and isRead=1",[[NSUserDefaults standardUserDefaults] objectForKey:@"SYNTIME"],[[NSDate date] timeIntervalSince1970]] ;
    [cartoonStr appendString:@"["];
    
    FMResultSet *set = [_fmdb executeQuery:cartoonAllInfoSql];
    while ([set next]) {
        CartoonInfoAll *cartoonInfoModel = [[CartoonInfoAll alloc] init];
        cartoonInfoModel.id = [set stringForColumn:@"id"];
        cartoonInfoModel.currentReadChapterId = [set stringForColumn:@"currentReadChapterId"];
        cartoonInfoModel.currentReadAlbumId = [set stringForColumn:@"currentReadAlbumId"];
        cartoonInfoModel.lastReadTime = [set stringForColumn:@"lastReadTime"];
        
        [cartoonStr appendString:[NSString jsonStringWithDictionary:[self entityToDictionary:cartoonInfoModel]]];
        [carArr addObject:cartoonInfoModel];

    }
    for (int i = 0; i < carArr.count; i ++) {
        [cartoonStr appendString:[NSString jsonStringWithDictionary:[self entityToDictionary:carArr[i]]]];
        if (i < carArr.count - 1) {
            [cartoonStr appendString:@","];
        }
    }
    
    [cartoonStr appendString:@"]"];
    NSLog(@"%@",cartoonStr);
    return cartoonStr;
    
    
}





- (NSMutableDictionary *) entityToDictionary:(CartoonInfoAll *)entity
{
    
    Class clazz = [entity class];
    u_int count;
    
    objc_property_t* properties = class_copyPropertyList(clazz, &count);
    NSMutableArray* propertyArray = [NSMutableArray arrayWithCapacity:count];
    NSMutableArray* valueArray = [NSMutableArray arrayWithCapacity:count];
    
    for (int i = 0; i < count ; i++)
    {
        objc_property_t prop=properties[i];
        const char* propertyName = property_getName(prop);
        
        [propertyArray addObject:[NSString stringWithCString:propertyName encoding:NSUTF8StringEncoding]];
        
        //        const char* attributeName = property_getAttributes(prop);
        //        NSLog(@"%@",[NSString stringWithUTF8String:propertyName]);
        //        NSLog(@"%@",[NSString stringWithUTF8String:attributeName]);
        
        id value =  [entity performSelector:NSSelectorFromString([NSString stringWithUTF8String:propertyName])];
        if(value ==nil)
            [valueArray addObject:[NSNull null]];
        else {
            [valueArray addObject:value];
        }
        //        NSLog(@"%@",value);
    }
    
    free(properties);
    
    NSMutableDictionary* returnDic = [NSMutableDictionary dictionaryWithObjects:valueArray forKeys:propertyArray];
    NSLog(@"%@", returnDic);
    
    return returnDic;
}


- (NSMutableArray *)fetchDateTime {
    NSString *DateTimeSql = @"select * from OpenId ";
    FMResultSet *set = [_fmdb executeQuery:DateTimeSql];
    NSMutableArray *timeArray = [NSMutableArray array];
    while ([set next]) {
        [timeArray addObject:@([set intForColumn:@"dateTime"])];
    }
    return timeArray;
}


- (NSMutableArray *)fetchSaveHistory {
    NSMutableArray *saveHistoryArr = [[NSMutableArray alloc] init];
    NSString *saveHistorySql = @"select * from saveHistory";
    FMResultSet *set = [_fmdb executeQuery:saveHistorySql];
    while ([set next]) {
        BookModel *bookModel = [[BookModel alloc] init];
        bookModel.id = [set stringForColumn:@"id"];
        bookModel.name = [set stringForColumn:@"name"];
        bookModel.images = [set stringForColumn:@"images"];
        bookModel.author = [set stringForColumn:@"author"];
        bookModel.introduction = [set stringForColumn:@"introduction"];
        bookModel.priority = [set stringForColumn:@"priority"];
        [saveHistoryArr addObject:bookModel];
    }
    return saveHistoryArr;
}


- (NSMutableArray *)fetchAllContinueWatch {
    NSMutableArray *ContinueWatchArr = [[NSMutableArray alloc] init];
    NSString *ContinueWatchSql = @"select * from continueWatch";
    FMResultSet *set = [_fmdb executeQuery:ContinueWatchSql];
    while ([set next]) {
        RecommentModel *recommentModel = [[RecommentModel alloc] init];
        recommentModel.id = [set stringForColumn:@"id"];
        recommentModel.name = [set stringForColumn:@"name"];
        recommentModel.images = [set stringForColumn:@"images"];
        recommentModel.author = [set stringForColumn:@"author"];
        recommentModel.introduction = [set stringForColumn:@"introduction"];
        recommentModel.priority = [set stringForColumn:@"priority"];
        recommentModel.chapterId = [set stringForColumn:@"chapterId"];
        recommentModel.chapterName = [set stringForColumn:@"chapterName"];
        recommentModel.chapterNameLabel = [set stringForColumn:@"chapterNameLabel"];
        recommentModel.chapterIndex = [set stringForColumn:@"chapterIndex"];
        recommentModel.albumId = [set stringForColumn:@"albumId"];
        recommentModel.updateInfo = [set stringForColumn:@"updateInfo"];
        [ContinueWatchArr addObject:recommentModel];
    }
    return ContinueWatchArr;
}


- (NSMutableArray *)fetchAlbumsWithChapterId:(NSString *)chapterId{
    NSMutableArray *AlbumIdArr = [[NSMutableArray alloc] init];
    NSString *AlbumSql = [NSString stringWithFormat:@"select * from albumInfo where chapterId=%@",chapterId];
    FMResultSet *set = [_fmdb executeQuery:AlbumSql];
    while ([set next]) {
        cartoonDetailModel *cartoonDetail = [[cartoonDetailModel alloc] init];
        cartoonDetail.id = [set stringForColumn:@"albumId"];
        cartoonDetail.images = [set stringForColumn:@"images"];
        cartoonDetail.chapterId = [set stringForColumn:@"chapterId"];
        cartoonDetail.imgWidth = [set intForColumn:@"imgWidth"];
        cartoonDetail.imgHeight = [set intForColumn:@"imgHeight"];
        cartoonDetail.chapterName = [set stringForColumn:@"chapterName"];
        cartoonDetail.propotion = [set doubleForColumn:@"propotion"];
        cartoonDetail.cartoonId = [set stringForColumn:@"cartoonId"];
        cartoonDetail.status = [set stringForColumn:@"status"];
        cartoonDetail.lastChapterId = [set stringForColumn:@"lastChapterId"];
        cartoonDetail.nextChapterId = [set stringForColumn:@"nextChapterId"];
        cartoonDetail.currentAlbumId = [set stringForColumn:@"currentAlbumId"];
        cartoonDetail.priority = [set stringForColumn:@"priority"];
        cartoonDetail.countTotal = [set stringForColumn:@"countTotal"];
        cartoonDetail.chapterIndex = [set stringForColumn:@"chapterIndex"];
        [AlbumIdArr addObject:cartoonDetail];
    }
    return AlbumIdArr;
    
}


- (NSMutableArray *)fetchSaveChapterWithCartoonId:(NSString *)cartoonId{
    NSMutableArray *chapterIdArr = [[NSMutableArray alloc] init];
    NSString *charpterSql = [NSString stringWithFormat:@"select * from commicChapter where cartoonId=%@",cartoonId];
    FMResultSet *set = [_fmdb executeQuery:charpterSql];
    while ([set next]) {
        chapterIdModel *chapterModel = [[chapterIdModel alloc] init];
        chapterModel.chapterId = [set stringForColumn:@"chapterId"];
        chapterModel.chapterName = [set stringForColumn:@"chapterName"];
        chapterModel.cartoonId = [set stringForColumn:@"cartoonId"];
        chapterModel.status = [set stringForColumn:@"status"];
        [chapterIdArr addObject:chapterModel];
    }
    return chapterIdArr;
    
}

- (NSMutableArray *)fetchSaveCartoonId{
    NSMutableArray *cartoonArr = [NSMutableArray array];
    NSString *CartoonSql = @"select * from commicCartoon";
    FMResultSet *set = [_fmdb executeQuery:CartoonSql];
    while ([set next]) {
        cartoonIdModel *cartoonModel = [[cartoonIdModel alloc] init];
        cartoonModel.cartoonId = [set stringForColumn:@"cartoonId"];
        cartoonModel.name = [set stringForColumn:@"name"];
        cartoonModel.image = [set stringForColumn:@"image"];
        cartoonModel.idAllStr = [set stringForColumn:@"idAllStr"];
        [cartoonArr addObject:cartoonModel];
    }
    return cartoonArr;
}

- (cartoonIdModel *)fetchSingleCartoonId:(NSString *)cartoonId{
    NSString *CartoonSql = [NSString stringWithFormat:@"select * from commicCartoon where cartoonId=%@",cartoonId] ;
    FMResultSet *set = [_fmdb executeQuery:CartoonSql];
    cartoonIdModel *cartoonModel = [[cartoonIdModel alloc] init];
    while ([set next]) {
        cartoonModel.cartoonId = [set stringForColumn:@"cartoonId"];
        cartoonModel.name = [set stringForColumn:@"name"];
        cartoonModel.image = [set stringForColumn:@"image"];
        cartoonModel.idAllStr = [set stringForColumn:@"idAllStr"];
    }
    return cartoonModel;
}

- (NSMutableDictionary *)fetchSinglepart:(NSString *)cartoonId chapterId:(NSString *)chapterId{

    NSMutableArray *imagesArr = [NSMutableArray array];
    NSMutableDictionary *imagesDict = [NSMutableDictionary dictionary];
    NSString *sql = [NSString stringWithFormat:@"select * from albumInfo where chapterId=%@",chapterId];
      FMResultSet *set = [_fmdb executeQuery:sql];
        while ([set next]) {
            [imagesArr addObject:[set stringForColumn:@"images"]];
        }
        [imagesDict setObject:imagesArr forKey:chapterId];
    
    return imagesDict;

}

- (NSMutableArray *)fetchpartIsSaveExist:(NSString *)cartoonId chapterId:(NSString *)chapterId {
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSMutableArray *chapterIdArr = [[NSMutableArray alloc] init];
    NSString *charpterSql = [NSString stringWithFormat:@"select chapterId from commicChapter where cartoonId=%@ and status is null",cartoonId];
    FMResultSet *set = [_fmdb executeQuery:charpterSql];
    while ([set next]) {
        [chapterIdArr addObject:[set stringForColumn:@"chapterId"]];
    }
    for (NSString *chapterId in chapterIdArr) {
        NSMutableArray *imagesArr = [NSMutableArray array];
        NSMutableDictionary *imagesDict = [NSMutableDictionary dictionary];
        NSString *sql = [NSString stringWithFormat:@"select * from albumInfo where chapterId=%@",chapterId];
        set = [_fmdb executeQuery:sql];
        while ([set next]) {
            [imagesArr addObject:[set stringForColumn:@"images"]];
        }
        [imagesDict setObject:imagesArr forKey:chapterId];
        [arr addObject:imagesDict];
    }
    return arr;
    
    
}

- (BOOL)fetchSingleIsSaveExist:(NSString *)cartoonId {
    NSMutableArray *chapterIdArr = [[NSMutableArray alloc] init];
    NSString *charpterSql = [NSString stringWithFormat:@"select chapterId from commicChapter where cartoonId=%@ and status is 2",cartoonId];
    FMResultSet *set = [_fmdb executeQuery:charpterSql];
    while ([set next]) {
        [chapterIdArr addObject:[set stringForColumn:@"chapterId"]];
    }
    if (chapterIdArr.count == 0) {
        return YES;
    }else{
        return NO;
    }


}

- (BOOL)fetchpartIsSaveExistchapterId:(NSString *)chapterId{
    NSString *sql = [NSString stringWithFormat:@"select * from commicChapter where chapterId=%@",chapterId];
    FMResultSet *set = [_fmdb executeQuery:sql];
    chapterIdModel *chapModel  = [[chapterIdModel alloc] init];
    while ([set next]) {
        chapModel.status = [set stringForColumn:@"status"];
        chapModel.chapterId = [set stringForColumn:@"chapterId"];
        chapModel.chapterName = [set stringForColumn:@"chapterName"];
        chapModel.cartoonId = [set stringForColumn:@"cartoonId"];
    }
    if ([chapModel.status isEqualToString:@"1"]) {
        return YES;
    }else{
        return NO;
    }
}

- (NSMutableArray *)fetchpart:(NSString *)cartoonId chapterId:(NSString *)chapterId
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSMutableArray *chapterIdArr = [[NSMutableArray alloc] init];
    NSString *charpterSql = [NSString stringWithFormat:@"select chapterId from commicChapter where cartoonId=%@",cartoonId];
    FMResultSet *set = [_fmdb executeQuery:charpterSql];
    while ([set next]) {
        [chapterIdArr addObject:[set stringForColumn:@"chapterId"]];
    }
    for (NSString *chapterId in chapterIdArr) {
        NSMutableArray *imagesArr = [NSMutableArray array];
        NSMutableDictionary *imagesDict = [NSMutableDictionary dictionary];
        NSString *sql = [NSString stringWithFormat:@"select * from albumInfo where chapterId=%@",chapterId];
        set = [_fmdb executeQuery:sql];
        while ([set next]) {
            [imagesArr addObject:[set stringForColumn:@"images"]];
        }
        [imagesDict setObject:imagesArr forKey:chapterId];
        [arr addObject:imagesDict];
    }
    return arr;
}


- (void)deleteCartoonId:(NSString *)cartoonId{
    NSString *sql = @"delete from commicCartoon where cartoonId = ?";
    [_fmdb executeUpdate:sql,cartoonId];

}

- (void)deleteChapterId:(NSString *)cartoonId{
    NSMutableArray *chapterIdArr = [[NSMutableArray alloc] init];
    NSString *charpterSql = [NSString stringWithFormat:@"select chapterId from commicChapter where cartoonId=%@",cartoonId];
    FMResultSet *set = [_fmdb executeQuery:charpterSql];
    while ([set next]) {
        [chapterIdArr addObject:[set stringForColumn:@"chapterId"]];
    }
    for (NSString *chapterId in chapterIdArr) {
        NSString *sql = @"delete from commicChapter where chapterId = ?";
        [_fmdb executeUpdate:sql,chapterId];

    }

}

- (void)deleteSingleChapterId:(NSString *)chapterId{
    NSString *sql = @"delete from commicChapter where chapterId = ?";
    [_fmdb executeUpdate:sql,chapterId];

}



- (BOOL)isExistsSaveHistory:(NSString *)cartoonId andIsStatus:(NSDictionary *)statusDict{
    NSString *sql = [NSString stringWithFormat:@"select * from cartoonAllInfo where id = %@ and %@=%@",cartoonId,[statusDict allKeys][0],[statusDict allValues][0]];
    FMResultSet *set = [_fmdb executeQuery:sql];
    if ([set next]) {
        return YES;
    }else{
        return NO;
    }
}


-(BOOL)isExistsCartoonId:(NSString *)cartoonId {
    NSString *sql = @"select status from commicChapter where chapterId = ?";
    FMResultSet *set = [_fmdb executeQuery:sql,cartoonId];
    if ([set next]) {
        return YES;
    }else{
        return NO;
    }
}



//插入数据
- (void) AddJsonObject:(NSDictionary *) tableDict andTable:(NSString *)table
{
    NSString *sql = [self assembleInsertSql:tableDict andTable:table];
    BOOL result = [_fmdb executeUpdate:sql withParameterDictionary:tableDict];
    if(!result){
//        NSLog([[_fmdb lastErrorMessage], nil);
    }
}

//拼接插入sql
- (NSString*) assembleInsertSql:(NSDictionary*) tableDict andTable:(NSString *)table
{
    NSArray *columns = [tableDict allKeys];
    NSString *prefix = [NSString stringWithFormat:@"insert into %@ (", table];
    NSMutableString *middle = [NSMutableString new];
    for(int i=0;i<[columns count];i++){
        NSString *columnName = [columns objectAtIndex:i];// 列名
        [middle appendString:columnName];
        [middle appendString:@","];
        
    }
    NSString * currentMiddle = [self removeLastChar:middle];
    
    NSMutableString *suffix = [NSMutableString new];
    [suffix appendString:@") values ("];
    for(int i=0;i<[columns count];i++){
        NSString *columnName = [columns objectAtIndex:i];// 列名
        [suffix appendString:@":"];
        [suffix appendString:columnName];
        [suffix appendString:@","];
    }
    
    NSString *currentSuffix = [self removeLastChar:suffix];
    
    NSMutableString *sql = [NSMutableString new];
    [sql appendString:prefix];
    [sql appendString:currentMiddle];
    [sql appendString:currentSuffix];
    [sql appendString:@");"];
    return sql;
}

//移除多余字符
- (NSString *)removeLastChar:(NSMutableString *)pastStr
{
    NSString *currentStr = [pastStr substringToIndex:[pastStr length] - 1];
    return currentStr;
}

- (void)deleteSaveHistory:(NSString *)saveId {
    NSString *sql = @"delete from saveHistory where id = ?";
    [_fmdb executeUpdate:sql,saveId];
}


//删除数据
- (void)deleteJsonObject:(NSDictionary *)words andTable:(NSString *)table
{
    NSString *sql = [self deleteSql:words andTable:table];
    [_fmdb executeUpdate:sql];
}
//拼接删除sql
- (NSString *)deleteSql:(NSDictionary *)dict andTable:(NSString *)table
{
    NSString *prefix = [NSString stringWithFormat:@"delete from %@ where %@", table,[self lastCommon:dict]];
    return prefix;
}

- (NSString *)lastCommon:(NSDictionary *)dict
{
    NSArray *columns = [dict allKeys];
    NSArray *values = [dict allValues];
    NSMutableString *suffix = [NSMutableString new];
    for(int i=0;i<[columns count];i++){
        NSString *columnName = [columns objectAtIndex:i];// 列名
        NSString *valueName = [values objectAtIndex:i];
        [suffix appendString:@" "];
        [suffix appendString:columnName];
        [suffix appendString:@"="];
        [suffix appendString:valueName];
        [suffix appendString:@","];
    }
    
    NSMutableString *sql = [NSMutableString new];
    [sql appendString:suffix];
    NSString *currentStr = [sql substringToIndex:[sql length] - 1];
    return currentStr;
}
//获取表的所有数据
- (NSMutableArray *)fetchAll:(NSString *)table andClassName:(NSString *)ClassName
{
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSArray *propertiesArr = [self getAllProperties:ClassName];
    
    NSString *sql = [NSString stringWithFormat:@"select * from %@",table];
    FMResultSet *set = [_fmdb executeQuery:sql];
    while ([set next]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        for (int i = 0; i < propertiesArr.count; i ++) {
            [dict setValue:[set stringForColumn:propertiesArr[i]] forKey:propertiesArr[i]];
        }
        id model = [[NSClassFromString(ClassName) alloc] init];
        [model setValuesForKeysWithDictionary:dict];
        [arr addObject:model];
    }
    return arr;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    
}

//获取对象属性名
- (NSArray *)getAllProperties:(NSString *)ClassName
{
    u_int count;
    objc_property_t *properties  =class_copyPropertyList(NSClassFromString(ClassName), &count);
    NSMutableArray *propertiesArray = [NSMutableArray arrayWithCapacity:count];
    for (int i = 0; i<count; i++)
    {
        const char *propertyName =property_getName(properties[i]);
        [propertiesArray addObject: [NSString stringWithUTF8String: propertyName]];
    }
    free(properties);
    return propertiesArray;
}

//修改数据
- (void)updataTable:(NSString *)table andWordsData:(NSDictionary *)dataDict
         andAimData:(NSDictionary *)AimDict {
    NSString *sql = [self createUpdateSql:table andWordsData:dataDict andAimData:AimDict];
    [_fmdb executeUpdate:sql];
}

//拼接更新sql
- (NSString *)createUpdateSql:(NSString *)table andWordsData:(NSDictionary *)dataDict andAimData:(NSDictionary *)AimDict {
    NSString *sql =  [NSString stringWithFormat:@"update %@ set %@ where %@",table,[self lastCommon:dataDict],[self lastCommon:AimDict]];
    return sql;
}


//获取指定数据
- (NSMutableArray *)fetchPart:(NSString *)table andClassName:(NSString *)ClassName
                   andAimDict:(NSDictionary *)dict {
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSArray *propertiesArr = [self getAllProperties:ClassName];
    NSString *sql = [self getPartFetchSql:dict andTable:table];
    FMResultSet *set = [_fmdb executeQuery:sql];
    while ([set next]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        for (int i = 0; i < propertiesArr.count; i ++) {
            [dict setValue:[set stringForColumn:propertiesArr[i]] forKey:propertiesArr[i]];
        }
        id model = [[NSClassFromString(ClassName) alloc] init];
        [model setValuesForKeysWithDictionary:dict];
        [arr addObject:model];
    }
    return arr;
}

//拼接获取部分数据sql
- (NSString *)getPartFetchSql:(NSDictionary *)dict andTable:(NSString *)table
{
    NSArray *columns = [dict allKeys];
    NSArray *values = [dict allValues];
    NSString *prefix = [NSString stringWithFormat:@"select * from %@ where", table];
    
    NSMutableString *suffix = [NSMutableString new];
    for(int i=0;i<[columns count];i++){
        NSString *columnName = [columns objectAtIndex:i];// 列名
        NSString *valueName = [values objectAtIndex:i];
        [suffix appendString:@" "];
        [suffix appendString:columnName];
        [suffix appendString:@"="];
        [suffix appendString:valueName];
        [suffix appendString:@" and"];
    }
    
    NSMutableString *sql = [NSMutableString new];
    [sql appendString:prefix];
    [sql appendString:suffix];
    NSString *currentStr = [sql substringToIndex:[sql length] - 4];
    return currentStr;
    
    
}


- (BOOL) isTableOK:(NSString *)tableName
{
    FMResultSet *rs = [_fmdb executeQuery:@"select count(*) as 'count' from sqlite_master where type ='table' and name = ?", tableName];
    while ([rs next])
    {
        NSInteger count = [rs intForColumn:@"count"];
        if (0 == count){
            return NO;
        }else {
            return YES;
        }
    }
    
    return NO;
}

/* 资讯缓存test */








@end
