//
//  DataBaseHelper.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/11.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@class cartoonDetailModel;
@class cartoonIdModel;
@class BookModel;
@class RecommentModel;
@class CartoonInfoAll;
@class cartoonChapterListModel;

//2.7
@class sqlTestList;

@interface DataBaseHelper : NSObject

+ (DataBaseHelper *) shared;
/**
 *  创建表
 *
 *  @param allDict 解析出来的数据
 */
- (void)createTable:(NSDictionary *)allDict;

/**
 *  增加表字段
 *
 *  @param words 添加的字段
 *  @param tableName 表名称
 *  @param property  字段类型
 */
- (void)AddColumn:(NSString *)words andTable:(NSString *)tableName andColumnType:(NSString *)property;

/**
 *  一次添加一条数据
 *
 *  @param tableName  需要添加的数据 添加数据{字段:值,字段:值。。。}格式
 *  @param table  表名称
 */
- (void)AddJsonObject:(NSDictionary *) tableName andTable:(NSString *)table;

/**
 *  一次性添加很多数据
 *
 *  @param tableData  需要添加的数据 数组形式 添加数据[{字段:值,字段:值。。。},{字段:值,字段:值。。。}]格式
 *  @param table  表名称
 */
- (void)AddMoreJsonObject:(NSArray *)tableData;

/**
 *  删除数据
 *
 *  @param words   需要删除的数据 添加数据{字段:值,字段:值。。。}格式
 *  @param table   表名称
 */
- (void)deleteJsonObject:(NSDictionary *)words andTable:(NSString *)table;

/**
 *  获取全部数据
 *
 *  @param table   表名称
 *  @param ClassName   Model类名
 
 *  @return 返回包含该类的数组
 */
- (NSMutableArray *)fetchAll:(NSString *)table andClassName:(NSString *)ClassName;

/**
 *  更新数据
 *  
 *  @param table   表名称
 *  @param dataDict  需要更新数据的字段和值 添加数据{字段:值,字段:值。。。}格式
 *  @param AimDict   目标数据 添加数据{字段:值,字段:值。。。}格式
 */
- (void)updataTable:(NSString *)table andWordsData:(NSDictionary *)dataDict
         andAimData:(NSDictionary *)AimDict;

/**
 *  按条件获取数据
 *
 *  @param table   表名称
 *  @param ClassName  model类型名
 *  @param dict 获取部分数据所需条件 where ..  目标数据 添加数据{字段:值,字段:值。。。}格式
 */
- (NSMutableArray *)fetchPart:(NSString *)table andClassName:(NSString *)ClassName
                   andAimDict:(NSDictionary *)dict;




/********************************漫画相关数据库方法******************************************/

- (void)insertSaveHistory:(BookModel *)bookModel;
- (void)insertContinueWatch:(RecommentModel *)recommentModel ;

/**
 *  插入漫画集
 *
 *  @param cartoonId
 *  @param name
 *  @param image
 */
- (void)insertCartoonId:(NSString *)cartoonId Name:(NSString *)name
                                             Image:(NSString *)image IdAllStr:(NSString *)idAllStr;
/**
 *  插入漫画话
 *
 *  @param model
 */
- (void)insertCharpterModel:(cartoonDetailModel *)model andStatus:(NSString *)status;
/**
 *  插入漫画内的图片
 *
 *  @param model
 */
- (void)insertAlbumModel:(cartoonDetailModel *)model;
- (void)insertAlbumRelationShip:(cartoonDetailModel *)model Status:(BOOL) status;
/**
 *  更新漫画话
 *
 *  @param chapterId
 */
- (void)updateChapter:(NSString *)chapterId;
/**
 *  查询漫画话是否存在
 *
 *  @param chapterId
 *
 *  @return
 */
- (BOOL)isExistsDownLoad:(NSString *)chapterId;
/**
 *  查询漫画集是否下载
 *
 *  @param cartoonId
 *
 *  @return
 */
 
 
- (NSMutableArray *)fetchSaveHistory ;
- (NSMutableArray *)fetchAllContinueWatch ;

 
/**
 *  获取单个漫画话的urls
 *
 *  @param cartoonId
 *  @param chapterId
 *
 *  @return
 */
- (NSMutableDictionary *)fetchSinglepart:(NSString *)cartoonId chapterId:(NSString *)chapterId;

/**
 *  获取单个下载漫画话的信息
 *
 *  @param cartoonId
 *
 *  @return
 */
- (cartoonIdModel *)fetchSingleCartoonId:(NSString *)cartoonId;
/**
 *  获取全部需要下载的对应漫画集的漫画话的urls
 *
 *  @param cartoonId
 *  @param chapterId
 *
 *  @return
 */
- (NSMutableArray *)fetchpart:(NSString *)cartoonId chapterId:(NSString *)chapterId;
/**
 *  获取保存的漫画集信息
 *
 *  @return
 */
- (NSMutableArray *)fetchSaveCartoonId;

/**
 *  根据漫画集id获取保存到数据库中的漫画话
 *
 *  @param cartoonId
 *
 *  @return
 */
- (NSMutableArray *)fetchSaveChapterWithCartoonId:(NSString *)cartoonId;
/**
 *  获取漫画话的album的信息
 *
 *  @param chapterId
 *
 *  @return
 */
- (NSMutableArray *)fetchAlbumsWithChapterId:(NSString *)chapterId;
/**
 *  获取已经下载好的漫画话信息
 *
 *  @param cartoonId
 *  @param chapterId
 *
 *  @return
 */
- (NSMutableArray *)fetchpartIsSaveExist:(NSString *)cartoonId chapterId:(NSString *)chapterId;
/**
 *  查询是否已经全部下完
 *
 *  @param cartoonId
 *
 *  @return
 */
- (BOOL)fetchSingleIsSaveExist:(NSString *)cartoonId;
/**
 *  查询已经下载好的漫画话
 *
 *  @param chapterId
 *
 *  @return
 */
- (BOOL)fetchpartIsSaveExistchapterId:(NSString *)chapterId;
- (void)deleteSaveHistory:(NSString *)saveId ;
/**
 *  删除漫画集
 *
 *  @param cartoonId
 */
- (void)deleteCartoonId:(NSString *)cartoonId;
/**
 *  删除漫画话
 *
 *  @param cartoonId
 */
- (void)deleteChapterId:(NSString *)cartoonId;
/**
 *  删除单个漫画话
 *
 *  @param chapterId
 */
- (void)deleteSingleChapterId:(NSString *)chapterId;


- (BOOL)isExistsCartoonId:(NSString *)cartoonId;
- (BOOL)isExistsSaveHistory:(NSString *)cartoonId andIsStatus:(NSDictionary *)statusDict;

- (void)insertCartoonInfo:(CartoonInfoAll *)cartoonInfoAll;
- (void)updateCartoonInfoCodition:(NSString *)id andValue:(NSDictionary *)valueD ;
- (BOOL)isExistCartoonAllInfo:(NSString *)id ;
- (void)insertExistCartoonInfo:(CartoonInfoAll *)cartoonInfoAll andValue:(NSDictionary *)valueD;
- (NSMutableArray *)fetchCartoonAllInfoWithCondition:(NSDictionary *) conditionDict andChooseType:(NSString *)chooseType;
- (void)addDateTime:(NSInteger)dateTime;

- (NSMutableArray *)fetchDateTime;
- (void)insertChapList:(cartoonChapterListModel *)chapModel;
- (NSMutableArray *)fetchCartoonChapList:(NSDictionary *)valueList;
- (BOOL)isExistChapList:(NSString *)id;
- (void)updateInfoCondition:(NSString *)id updateInfo:(NSString *)updateInfo;
- (NSMutableArray *)fetchCartoonAllInfo ;
- (NSString *)fetchCartoonPartCollectionStatus;
- (NSString *)fetchCartoonPartReadStatus;
- (void)dropTableView;
- (NSString *)fetchCartoonAllCollectionStatus;
- (NSString *)fetchCartoonAllReadStatus;
//- (void)fetchIdMore;
- (NSMutableString *)fetchIdMore;

/* 资讯首页缓存test */

@end
