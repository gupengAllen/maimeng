//
//  cartoonIdModel.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface cartoonIdModel : NSObject
@property (nonatomic,copy)NSString *cartoonId;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *image;
@property (nonatomic,copy)NSString *idAllStr;
@end
