//
//  Y_X_DataInterface.m
//  YouYou_Work
//
//  Created by lianghuigui on 13-11-18.
//  Copyright (c) 2013年 lianghuigui. All rights reserved.
//

#import "Y_X_DataInterface.h"

//----
const NSString* INTERFACE_PREFIXD = @"http://apitest.playsm.com/index.php?";
const NSString* INTERFACE_PREFIX = @"http://apitest.playsm.com/index.php?";

/* 主题 */
const NSString *NAVIGATIONBARTITLEFONT;
const NSString *NAVIGATIONBARTITLECOLOR;
const NSString *s;




const NSString* YX_KEY_CLIENTVERSION=@"clientversion";//客户端版本号
const NSString* YX_KEY_DEVICETYPE=@"devicetype";//设备类型 1：浏览器设备 2：pc设备 3：Android设备 4：ios设备 5：windows phone设备
const NSString* YX_KEY_REQUESTTIME=@"requesttime";//请求时的时间戳
const NSString* YX_KEY_DEVICENTOKEN=@"devicetoken";
const NSString* YX_KEY_LOGINTIME=@"logintime";//登录时间
const NSString* YX_KEY_USERID=@"userid";//用户ID
const NSString* YX_KEY_CHECKCODE=@"checkcode";//校验码
const NSString* YX_KEY_DEVICEINFO=@"deviceinfo";//校验码
const NSString *YX_KEY_CLENTID = @"clientid";

const NSString* YX_VALUE_CLIENTVERSION=@"1.0";
const NSString* YX_VALUE_DEVICETYPE=@"4";
const NSString* YX_VALUE_USERID=@"";
const NSString* YX_VALUE_CHECKCODE=@"";
const NSString* YX_VALUE_DEVICENTOKEN=@"";
const NSString* YX_VALUE_LOGINTIME=@"";
const NSString *YX_VALUE_REQUESTTIME=@"10";
const NSString *YX_VALUE_DEVICEINFO=@"";
const NSString *YX_VALUE_CLENTID = @"";

// 系统
const NSString *API_URL_USER_ADD                    = @"user/add";
const NSString *API_URL_USER_LOGIN                  = @"user/login";
const NSString *API_URL_USER_UNION_LOGIN            = @"user/unionLogin";
const NSString *API_URL_USER_UPDATE_PASSWORD        = @"user/updatePassword";
const NSString *API_URL_USER_CHECK_VERIFY_CODE      = @"user/checkVerifycode";

const NSString *API_URL_USER_DETAIL                 = @"user/detail";
const NSString *API_URL_USER_UPDATE                 = @"user/update";
const NSString *API_URL_USER_CENTER                 = @"v3/user/centre";
const NSString *API_URL_USER_LOGOUT                 = @"user/logout";
const NSString *API_URL_USER_RESET_PASSWORD         = @"user/resetPassword";

const NSString *API_URL_SYSTEM_SENDNOTE             = @"system/sendNote";
// 七牛
const NSString *API_URL_SYSTEM_GETUPTOKEN           = @"system/getUpToken";

// 资讯
const NSString *API_URL_MESSAGE_LIST                = @"message/list";
const NSString *API_URL_MESSAGE_DETAIL              = @"message/detail";
const NSString *API_URL_MESSAGE_PRAISE              = @"message/praise";

// 美图
const NSString *API_URL_PRETTYIMAGESLIST            = @"prettyImages/list";
const NSString *API_URL_PRETTYIMAGESDETAIL          = @"prettyImages/detail";
const NSString *API_URL_PRETTYIMAGESPRAISE          = @"prettyImages/praise";
//上传美图
const NSString *API_URL_UPLOADNICEPIC               = @"prettyImages/add";

// 评论
const NSString *API_URL_CONTENT_COMMENT_MESSAGE     = @"content/commentMessage";
const NSString *API_URL_CONTENT_REPLY_MESSAGE       = @"content/replyMessage";
const NSString *API_URL_CONTENT_WITHUSERINFORM      = @"content/withUserInform";

const NSString *API_URL_CONTENT_WITHUSERREPLY       = @"v3/content/withUserReply";
const NSString *API_URL_INFORMMESSAGE_WITHINFORM    = @"informMessage/withInform";

const NSString *API_URL_CONTENT_WITH_USER_COMMENT   = @"content/withUserComment";
const NSString *API_URL_ACTIONTYPE_WITH_USER_PRAISE = @"actionType/withUserPraise";
const NSString *API_URL_CONTENT_DELETE              = @"content/delete";
//萌窝漫画
const NSString *API_URL_CONTENT_WITHUSERCARTOONCOMMENT     = @"content/withUserCartoonComment";//content/withUserCartoonComment
// 礼包
const NSString *API_URL_GIFT_LIST                   = @"gift/list";
const NSString *API_URL_GIFT_DETAIL                 = @"gift/detail";
const NSString *API_URL_GIFT_SWITCH                 = @"quDaoConfig/checkIsEnableGift";
// 礼包码
const NSString *API_URL_GIFTCODE_GETCODE            = @"giftCode/getCode";
const NSString *API_URL_GIFTCODE_WITHUSER           = @"giftCode/withUser";

// 反馈
const NSString *API_URL_FEEDBACK_ADD                = @"feedback/add";
const NSString *API_URL_FEEDBACK_ADD_CONSTANT       = @"feedback/withSystemReply";

// 关于我们
const NSString *API_URL_ABOUT                       = @"app/about.php";

// 轮播图
const NSString *API_URL_ADIMAGE_LIST                = @"adImage/list";
const NSString *API_URL_ADIMAGE_LOG_ADD             = @"adImageLog/add";

//动画-
//漫画集
const NSString *API_URL_CARTOONSET_LIST             = @"cartoonSet/detail";

//漫画话
//const NSString *API_URL_CARTOONCONTENT_LIST         = @"cartoonSet/detail";
const NSString *API_URL_CARTOONCHAPTER_DETAIL       = @"cartoonChapter/detail";
const NSString *API_URL_CARTOONCHAPTERALBUM_DETAIL  = @"cartoonChapter/albumList";

//添加阅读记录
const NSString *API_URL_CARTOONUSERREADHISTORY_DETAIL = @"cartoonReadHistory/getUserReadHistoryList";
const NSString *API_URL_CARTOONREADHISTORY_DETAIL   = @"cartoonReadHistory/list";

//推荐
const NSString *API_URL_NEWRECOMMENTALL_LIST        = @"v4/recommend/getUserRecommendList";

const NSString *API_URL_RECOMMENTALL_LIST           = @"recommend/getUserRecommendList";
const NSString *API_URL_RECOMMENTLU_LIST            = @"cartoonSet/userContinueReadList";
const NSString *API_URL_RECENTUPDATE_LIST           = @"cartoonSet/recentUpdateBillBoard";
const NSString *API_URL_RECOMMENT_LIST              = @"cartoonSet/list";
const NSString *API_URL_RECOMMENTDOWNLOAD_LIST      = @"cartoonBillBoard/downloadBillBoard";
const NSString *API_URL_RECOMMENTCOLLECTION_LIST    = @"cartoonBillBoard/collectionBillBoard";
const NSString *API_URL_RECOMMENTTOP_LIST           = @"cartoonSet/hitBillBoard";
const NSString *API_URL_RECOMMENTCOLLECTIN_LIST     = @"cartoonSet/collectionBillBoard";

//收藏
const NSString *API_URL_CARTOONCOLLECTION_LIST      = @"cartoonCollection/getUserCollectionList";

//搜索
const NSString *API_URL_SEARCH_LIST                 = @"cartoonSearch/keywordList";
const NSString *API_URL_SEARCHLIST_LIST             = @"cartoonSearch/list";
//大家都在看
const NSString *API_URL_EVERYBODYWATCHING_LIST      = @"cartoonBillBoard/hitBillBoard";
//榜单
const NSString *API_URL_TOP_LIST                    = @"cartoonBillBoard/list";
//const NSString *API_URL_TOP_DETAIL                  = @"cartoonBillBoard/detail";
const NSString *API_URL_TOP_DETAIL                  = @"cartoonBillBoard/getCartoonSetListByBillBoard";
//漫画页
const NSString *API_URL_CHAPTERALBUM_LIST           = @"cartoonChapter/albumList";
//类别
const NSString *API_URL_CATEGORY_LIST               = @"cartoonCategory/list";
const NSString *API_URL_CATEGORY_DETAL              = @"cartoonCategory/getCartoonSetListByCategory";
//书架
const NSString *API_URL_BOOKCOLLECTION_LIST         = @"cartoonCollection/updateCollection";
const NSString *API_URL_BOOKCOLLECTIONLIST_LIST     = @"cartoonCollection/getUserCollectionList";
const NSString *API_URL_CONTINUELIST                = @"cartoonSet/userContinueReadList";

const NSString *API_URL_ADIMAGE                     = @"adImage/getBootAds";
const NSString *API_URL_CARTOONSET_CONTENTLIST      = @"cartoonSet/contentList";
const NSString *API_URL_UPDATEINFO                  = @"cartoonSet/getUpdatedCartoonInfos";

const NSString *API_URL_LOGO                        = @"system/log";
//更新
const NSString *API_URL_UPDATE                      = @"system/checkAppNeedUpdate";
const NSString *API_URL_UPCATEGROY                  = @"cartoonSet/getInterestList";
// 专题
const NSString *API_URL_SPECIAL_LIST                = @"special/list";
const NSString *API_URL_SPECIAL_DETAIL              = @"special/getMessageListBySpecial";
const NSString *API_URL_TAG                         = @"messageTags/list";

const NSString *API_URL_PICTUREZAN                  = @"actionType/getPraiseWithUserReply";
const NSString *API_URL_PRAISEPIC                   =@"actionType/withUserPicPraise";
const NSString *API_URL_USERPICCOMMMENT             = @"content/withUserPicComment";
const NSString *API_URL_CARTOONCOMMENT                                   = @"content/withUserCartoonComment";

const NSString *API_URL_SCAN                        = @"ticket/scanQRCode";
//举报
const NSString *API_URL_REPORT                      = @"informAgainst/add";
const NSString *API_URL_MESSAGETAGS_LIST            = @"messageTags/list";
const NSString *API_URL_CONTENT_PRAISELIST          = @"content/praiseList";
const NSString *API_URL_CONTENT_REPLYLIST           = @"content/replyList";
static NSMutableDictionary* commonParams;
@implementation Y_X_DataInterface : NSObject 

+(void) init{
    commonParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                    YX_VALUE_CLIENTVERSION,YX_KEY_CLIENTVERSION,
                    YX_VALUE_DEVICETYPE,YX_KEY_DEVICETYPE,
                    YX_VALUE_USERID,YX_KEY_USERID,
                    YX_VALUE_DEVICENTOKEN,YX_KEY_DEVICENTOKEN,
                    YX_VALUE_CHECKCODE,YX_KEY_CHECKCODE,
                    YX_VALUE_LOGINTIME,YX_KEY_LOGINTIME,
                    YX_VALUE_REQUESTTIME,YX_KEY_REQUESTTIME,
                    YX_VALUE_DEVICEINFO,YX_KEY_DEVICEINFO,
                    YX_VALUE_CLENTID,YX_KEY_CLENTID,
                    nil];
}

// 获取数据字典
+(NSMutableDictionary *)commonParams {
	return commonParams;
}
// 向公用字典中添加字段
+(void) setCommonParam:(id)key value:(id)value{
	[commonParams setValue:value forKey:key];
}
// 查找key对应的一个对象
+(id) commonParam:(id) key{
	return [commonParams objectForKey:key];
}

@end
