//
//  YK_API_request.m
//  Anxin_work
//
//  Created by lianghuigui on 13-12-6.
//  Copyright (c) 2013年 lianghuigui. All rights reserved.
//

#import "YK_API_request.h"
#import "YKHttpRequest.h"
#import "YKHttpRequestHelper.h"
#import "Y_X_DataInterface.h"

@implementation YK_API_request

/*
 返回当前时间
 */
+(NSString *)nowTimestamp {
	NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    NSString* format=@"yyyyMMddHHmmss";
    assert(format!=nil);
	NSDate* nowDate=[NSDate date];
	NSDateFormatter* dateFormater=[[NSDateFormatter alloc] init];
    [dateFormater setDateFormat:format];
	[dateFormater stringFromDate:nowDate];
	NSString* timestamp=[[dateFormater stringFromDate:nowDate] copy];
	[dateFormater release];
	[pool release];
	return [timestamp autorelease];
}

+(void) startLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action method:(NSString *)methodData{
	//assert(object!=nil);
    NSLog(@"requestURL :%@",url);
	NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    //	NSMutableDictionary* fixParams=[YKHttpAPIHelper fixParams:extraParams];
    
	YKHttpRequestHelper* helper=[[YKHttpRequestHelper alloc] initWithObject:object action:action];
	[YKHttpRequest startLoadUrlString:url delegate:helper params:extraParams extraHeaders:[YK_API_request extraHeaders] method:methodData];
    NSLog(@"header%@",[YK_API_request extraHeaders]);
	[helper release];
	[pool release];
}

+(void) startLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action method:(NSString *)methodData andHeaders:(NSDictionary *)heards{
    //assert(object!=nil);
    NSLog(@"requestURL :%@",url);
    NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    //	NSMutableDictionary* fixParams=[YKHttpAPIHelper fixParams:extraParams];
    
    YKHttpRequestHelper* helper=[[YKHttpRequestHelper alloc] initWithObject:object action:action];
    [YKHttpRequest startLoadUrlString:url delegate:helper params:extraParams extraHeaders:heards method:methodData];
    [helper release];
    [pool release];
}


+(void) startGetLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action header:(NSDictionary *)headerDict {
    NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    //	NSMutableDictionary* fixParams=[YKHttpAPIHelper fixParams:extraParams];
    
    YKHttpRequestHelper* helper=[[YKHttpRequestHelper alloc] initWithObject:object action:action];
//    [YKHttpRequest startLoadUrlString:url delegate:helper params:extraParams extraHeaders:[YK_API_request extraHeaders] method:methodData];
    [YKHttpRequest startGetLoadUrlString:url delegate:helper params:extraParams extraHeaders:headerDict];
    [helper release];
    [pool release];

}

+(void) startGetLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action{
	//assert(object!=nil);
    NSLog(@"requestURL :%@",url);
	NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
    //	NSMutableDictionary* fixParams=[YKHttpAPIHelper fixParams:extraParams];
    
	YKHttpRequestHelper* helper=[[YKHttpRequestHelper alloc] initWithObject:object action:action];
	[YKHttpRequest startGetLoadUrlString:url delegate:helper params:extraParams extraHeaders:[YK_API_request extraHeaders]];
	[helper release];
	[pool release];
}
//发送数据 ，上传图片
+(void)startSendPicUrl:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action imageDatas:(NSMutableArray *)arraydatas method:(NSString *)methodData{
    
    NSAutoreleasePool* pool=[[NSAutoreleasePool alloc] init];
	YKHttpRequestHelper* helper=[[YKHttpRequestHelper alloc] initWithObject:object action:action];
	[YKHttpRequest startLoadSendpicUrl:[NSURL URLWithString:url] delegate:helper params:extraParams extraHeaders:[YK_API_request extraHeaders] imagedatas:arraydatas medhod:methodData];
	[helper release];
	[pool release];
    
}

+(NSDictionary*) extraHeaders{
    
    NSString * currentOSVERSION=[[UIDevice currentDevice] systemVersion];
    NSString * deviceType=[[UIDevice currentDevice] model];    
//    [Y_X_DataInterface setCommonParam:YX_KEY_OSVERSION value:currentOSVERSION];
//    [Y_X_DataInterface setCommonParam:YX_KEY_CLIENTTYPE value:deviceType];
    
    NSString * nowTime=[NSString stringWithFormat:@"%.0f",[[NSDate date] timeIntervalSince1970]];
    
//    [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:nowTime];
    [Y_X_DataInterface setCommonParam:YX_KEY_REQUESTTIME value:nowTime];
    
    return [Y_X_DataInterface commonParams];
}

- (void)getCurrentIP
{
    NSURL *url = [NSURL URLWithString:@"http://automation.whatismyip.com/n09230945.asp"];
    __block ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request setCompletionBlock:^{
        NSString *responseString = [request responseString];
        if (responseString) {
            NSString *ip = [NSString stringWithFormat:@"%@", responseString];
            NSLog(@"responseString = %@", ip);
        };
    }]; 
}
@end
