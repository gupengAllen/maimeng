//
//  YK_API_request.h
//  Anxin_work
//
//  Created by lianghuigui on 13-12-6.
//  Copyright (c) 2013年 lianghuigui. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GDataXMLNode.h"
#import "MBProgressHUD.h"

@interface YK_API_request : NSObject
+(void) startLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action method:(NSString *)methodData;
+(void) startGetLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action;
+(void) startGetLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action header:(NSDictionary *)headerDict;
+(NSDictionary*) extraHeaders;
+(void)startSendPicUrl:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action imageDatas:(NSMutableArray *)arraydatas;
+(void) startLoad:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action method:(NSString *)methodData andHeaders:(NSDictionary *)heards;

//发送数据 ，上传图片
+(void)startSendPicUrl:(NSString *)url extraParams:(NSDictionary *)extraParams object:(id)object action:(SEL)action imageDatas:(NSMutableArray *)arraydatas method:(NSString *)methodData;
@end
