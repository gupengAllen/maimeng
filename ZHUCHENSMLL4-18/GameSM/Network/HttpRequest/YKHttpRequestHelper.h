//
//  YKHttpRequestHelper.h
//  YK_B2C
//
//  Created by Guwei.Z on 11-4-28.
//  Modify by YuHui.Liu on 11-7-8
//  Copyright 2011 Yek. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKHttpRequest.h"
#import "CustomAlertView.h"

@interface YKHttpRequestHelper : NSObject<YKHttpRequestDelegate> {
	id object;
	SEL action;
    
    NSInteger   m_ResponseType; // 返回的数据类型 0=json, 1=xml
}

@property(nonatomic,assign)NSInteger    m_ResponseType;
/*
 object:
 网址内容下载完成后调用此对象的action方法，action方法格式同下
 action:
 -(void) ****:(GDataXMLDocument*) xmlDoc params:(NSDictionary*) params ;
 */
-(id) initWithObject:(id)aobj action:(SEL)aaction;

@end
