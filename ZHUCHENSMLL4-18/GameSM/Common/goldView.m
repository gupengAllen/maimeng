//
//  goldView.m
//  goldText
//
//  Created by 顾鹏 on 16/1/15.
//  Copyright © 2016年 顾鹏. All rights reserved.
//

#import "goldView.h"

@implementation goldView
{
    UILabel *commentLabel;
    UILabel *pointLabel;
}

- (instancetype)initWithFrame:(CGRect)frame topText:(NSString *)top bottomText:(NSString *)bottom color:(UIColor *)color{
    if (self = [super initWithFrame:frame]) {
        commentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width/2, self.frame.size.height)];
        commentLabel.font = [UIFont systemFontOfSize:15.0];
        commentLabel.text = top;
        commentLabel.textAlignment = 2;
        commentLabel.textColor = color;
        commentLabel.numberOfLines = 0;
        [self addSubview:commentLabel];
        
        pointLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/2, 0, self.frame.size.width/2, self.frame.size.height)];
        pointLabel.font = [UIFont systemFontOfSize:20.0];
        pointLabel.textAlignment = 0;
        pointLabel.text = bottom;
        pointLabel.textColor = [UIColor redColor];
        pointLabel.textColor = color;
        
        pointLabel.numberOfLines = 0;
        
        [self addSubview:pointLabel];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
