//
//  Config.h
//  AnXin
//
//  Created by wt on 14-2-28.
//  Copyright (c) 2014年 wt. All rights reserved.
//
#import "CustomTool.h"
#define IsIOS7              ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)

#define iPhone5             ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define ScreenSizeWidth     [UIScreen mainScreen].bounds.size.width
#define ScreenSizeHeight    [UIScreen mainScreen].bounds.size.height

#import "UIViewExt.h"
//推荐漫画
#define KrecommentCellHeight 203
#define KrecommentCellImageHeight 150
#define krecommentCellInset 4

//漫画类别
#define kCategoryCellWidth ((ScreenSizeWidth-60)/3.0)

//漫画榜单
#define KTOPCellHeight 172
#define KTOPCellInset 6
#define  KTOPCellImageHeight 150

//书架
#define KBookCellHeight 120+30+26+8
#define KBookCellInset 4
#define KBookCellImageHeight 120

//漫画搜索
#define KSearchHeaderInset 4

//漫画搜索有结果
#define  KSearchresultsitemInset 4
//#define KSearchresultsitemwidth (([UIScreen mainScreen].bounds.size.width-4*3*2)/3.0)
#define KSearchresultimageHeight 120;



//颜色
#define CUSTOMBGCOLOR         [UIColor colorWithRed:240/255.0 green:240/255.0 blue:240/255.0 alpha:1]  //灰色背景
#define CUSTOMREDCOLOR      [UIColor colorWithRed:253/255.0 green:85/255.0 blue:103/255.0 alpha:1]   //主色调
#define CUSTOMTITLECOLOER1    [UIColor colorWithRed:43/255.0 green:43/255.0 blue:43/255.0 alpha:1]  //标题1
#define CUSTOMTITLECOLOER2    [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1]  //标题2
#define CUSTOMTITLECOLOER3    [UIColor colorWithRed:209/255.0 green:208/255.0 blue:208/255.0 alpha:1]  //标题3
#define CUSTOMBORDER    [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1]  //标题3

//字体颜色
#define LIGHTTEXTCOLOR144 [UIColor colorWithRed:144/255.0 green:144/255.0 blue:144/255.0 alpha:1]
#define LIGHTTEXTCOLOR160 [UIColor colorWithRed:160/255.0 green:160/255.0 blue:160/255.0 alpha:1]
#define LIGHTTEXTCOLOR2D1 [UIColor colorWithRed:209/255.0 green:208/255.0 blue:208/255.0 alpha:1]
#define CUSTOMBLUECOLOR        [UIColor colorWithRed:100/255.0 green:192/255.0 blue:210/255.0 alpha:1]   //蓝调
//字体大小
#define CUSTOMTITLESIZE24 [UIFont systemFontOfSize:24]
#define CUSTOMTITLESIZE22 [UIFont systemFontOfSize:22]
#define CUSTOMTITLESIZE20 [UIFont systemFontOfSize:20]
#define CUSTOMTITLESIZE16 [UIFont systemFontOfSize:16]
#define CUSTOMTITLESIZE18 [UIFont systemFontOfSize:18]
#define CUSTOMTITLESIZE14 [UIFont systemFontOfSize:14]
#define CUSTOMTITLESIZE12 [UIFont systemFontOfSize:12]
#define CUSTOMTITLESIZE10 [UIFont systemFontOfSize:10]

#define ProductName         [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleExecutableKey]

#define Version             [[[NSBundle mainBundle] infoDictionary] objectForKey:(NSString *)kCFBundleVersionKey]


#define ShortVersion        [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define APISECRETKEY        @"apites465671yoditc"

#define GAODEMAPKEY         @"b5bae6b5ea356f9c47b28e17b91eaf11"

#define g_App               ((AppDelegate*)[[UIApplication sharedApplication] delegate])
#define NSValueToString(a)  [NSString stringWithFormat:@"%@",a]

#define RGBACOLOR(R,G,B,A)  [UIColor colorWithRed:(R)/255.0f green:(G)/255.0f blue:(B)/255.0f alpha:(A)]

#ifndef __OPTIMIZE__

#define NSLog(fmt, ...) NSLog((@ "%s" ":%d" fmt"\n"), __FUNCTION__, __LINE__, ##__VA_ARGS__)

//#define NSLog(...) {}

#else

#define NSLog(...) {}

#endif

#define _Scale [UIScreen mainScreen].bounds.size.width/320.f
#define _Scale6 [UIScreen mainScreen].bounds.size.width/375.f

#define ___Scale 1.36

#define __Scale [UIScreen mainScreen].bounds.size.height/480.f
#define __Scale6 [UIScreen mainScreen].bounds.size.height/568.f


#define RefreshNotification     @"RefreshNotification"
#define HomeNotification        @"HomeNotification"
#define LoginNotification       @"LoginNotification"

#define WECHATAPPID             @"wx9a603356aa61137e"
#define WECHATAPPSECRET         @"0e9b92693b35eb2ad1e7d25c0701cc93"

#define WEIBOAPPID             @"51252659"
#define WEIBOAPPSECRET         @"508e546aec2d9201cc66fdb13f0aa420"

#define QQAPPID             @"1104747833"
#define QQAPPSECRET         @"JPfivgqrZRO0K5QA"

#define UMENGKEY            @"55df01bae0f55aa65a001531"

