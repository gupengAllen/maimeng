//
//  upVote.h
//  GameSM
//
//  Created by 顾鹏 on 16/1/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface upVote : NSObject
+ (upVote *)shared;
- (void)startAnimitionTop:(NSString *)top bottom:(NSString *)bottom point:(CGPoint)point fatherView:(UIView *)fatherView color:(UIColor *)color;
- (void)startMiddleAnimitionTop:(NSString *)top bottom:(NSString *)bottom point:(CGPoint)point fatherView:(UIView *)fatherView;
@end
