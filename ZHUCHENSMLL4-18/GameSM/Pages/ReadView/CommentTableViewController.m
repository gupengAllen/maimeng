//
//  CommentTableViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CommentTableViewController.h"

@interface CommentTableViewController ()
{
    
}
@end



@implementation CommentTableViewController

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, self.frame.size.width, 400);
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
