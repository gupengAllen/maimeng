//
//  GPSubCollectionCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/1/28.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "GPCollectionCell.h"

@interface GPSubCollectionCell : GPCollectionCell <UIScrollViewDelegate>
@property (nonatomic, strong) UIScrollView *contentScrollView;
@property (nonatomic, strong) id target;
- (void)setImageUrlll:(NSString *)imageUrlll andImage:(UIImage *)image andDelegate:(id)target;
@end
