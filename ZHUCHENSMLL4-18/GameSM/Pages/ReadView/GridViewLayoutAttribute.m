//
//  GridViewLayoutAttribute.m
//  GridViewDemo
//
//

#import "GridViewLayoutAttribute.h"

@implementation GridViewLayoutAttribute

- (id)init {
  self = [super init];
  if (self) {
  }
  
  return self;
}

- (id)copyWithZone:(NSZone *)zone {
  GridViewLayoutAttribute *newAttributes = [super copyWithZone:zone];
  return newAttributes;
}

@end
