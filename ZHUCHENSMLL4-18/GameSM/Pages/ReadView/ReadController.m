//
//  ReadController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/22.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "ReadController.h"
#import <QuartzCore/QuartzCore.h>
#import "LandscapeController.h"
#import "DetailsViewController.h"
#import "AFNetworking.h"
#import "Y_X_DataInterface.h"
#import "KeychainItemWrapper.h"
#import "Masonry.h"
#import "Tool.h"
#import "MBProgressHUD.h"
#import "FeedbackViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define HWIDTH self.view.bounds.size.width
#define HHEIGHT self.view.bounds.size.height
//#define HWIDTH self.view.bounds.size.height
//#define HHEIGHT self.view.bounds.size.width
#define HMARGIN 5

@interface ReadController ()<UIScrollViewDelegate,UIAlertViewDelegate>
{
 
    NSDictionary    *_dataDic;
    NSDictionary    *_allDict;
    NSInteger       _cartoonChapterCount;
    NSInteger       _cartoonId;
    NSMutableArray  *_dataIDArr;
    NSMutableArray  *_subButtons;
    NSMutableArray  *_heightArr;
    NSMutableArray  *_widthArr;
    NSMutableArray  *_chapterNumArr;
    NSMutableArray  *_witchId;
    NSMutableArray  *_heightSum;
    UILabel         *_titleName;
    NSString        *_idd;
    NSString        *_chapterIndex;
    
    
    
    UIView          *_redIv;
    
    UIScrollView    *_selectScrollView;
    UIScrollView    *_scrollView;
    NSMutableArray  *_dataArr;
    NSMutableArray  *_dataAllArr;
    NSArray         *_idNum;
    UIImageView     *_iv;
    UIImageView     *_imageV;
    NSString        *_nextId;
    NSString        *_statusStr;
    
    UIView          *_topView;
    UIView          *_bottonView;
    UIView          *_rightView;
    UIView          *_leftView;
    UIView          *_selectView;
    UIView          *_helpView;
    UIView          *_tagView;
    UIImageView     *_BGView;
    UIImageView     *_imageView;
    UIView          *_bagV;
    
    UISlider        *_pageNumControl;
    UISlider        *_lightSlider;
    UILabel         *_pageNumLabel;
    UILabel         *_helpLabel;
    UILabel         *_nameLabel;
    UILabel         *_waitLabel;
    UILabel         *_statusLabel;
    
    UIButton        *_setBtn;
    UIButton        *_rightBtn;
    UIButton        *_leftBtn;
    UIButton        *_rightViewBtn;
    
    BOOL            _Horizontal;
    BOOL            _RightHand;
    BOOL            _SelectWork;
    NSString        *_Continue;
    NSString        *_urlStr;
    int              _cartoonNum;
    NSString        *_cartoonID;
    NSMutableArray  *_chapterID;
    NSMutableArray  *_chapterArr;
    NSString        *_cartoonNextId;
    NSString        *_cartoonLastId;
    NSString        *_selectUrl;
    NSString        *_chapterReadId;
    NSString        *_strwithUrl;
//    NSString        *_witchOneChapter;
    
    float _a;
    float _sum;
    float _two;
    float _lightNum;
    float _direction;
    
    BOOL _SELECTCHAPTER;
    BOOL _NOZERO;
    BOOL _SUN;
    
    
    
    int _h;
    int _p;
    int _t;
    int _u;
    int _upCount;
    int _tag1;
    int _witchOneChapter;
}
@end

#warning 代码未封装

@implementation ReadController

- (void)viewDidLoad {
   
    _SELECTCHAPTER = YES;
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    
    [self initTagView];
    _NOZERO = YES;
    _SUN = YES;
    _t=1;
    _upCount = 0;
    NSLog(@"%f",HWIDTH);
    _chapterNumArr = [NSMutableArray arrayWithCapacity:0];
    _chapterID = [NSMutableArray arrayWithCapacity:0];
    _heightArr = [NSMutableArray arrayWithCapacity:0];
    _widthArr = [NSMutableArray arrayWithCapacity:0];
    _dataAllArr = [NSMutableArray arrayWithCapacity:0];
    _witchId = [NSMutableArray array];
    _heightSum = [NSMutableArray array];
    
    [self addNoti];
    
    _Horizontal = NO;
    _RightHand = NO;
    _SelectWork = NO;
    _p = 0;
    _u = 0;
    _sum = 0;
    
    self.view.backgroundColor = [UIColor blackColor];
    [self initBGView];
    [self createScrollView];//创建scrollView
    [self createMenuBtn];//菜单按钮
    [self createPageTurnBtn];//翻页
    [self createSetUI];//菜单详情页
    [self createHelpView];//帮助栏
    [self initWaitView];

//    [self createSelectWorkView];
    //提示视图
}
#pragma mark -
-(void)initBGView{
    _BGView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, HWIDTH, HHEIGHT)];
    _BGView.image = [UIImage imageNamed:@"jiazaitu_2@3x.png"];
    _BGView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_BGView];
    _BGView.alpha = 0;
}

-(void)initWaitView{
    _bagV = [[UIView alloc]initWithFrame:CGRectMake(HWIDTH/2-75, HHEIGHT - 20, 150, 20)];
    [self.view addSubview:_bagV];
    _waitLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 150, 20)];
    _waitLabel.textAlignment = 1;
    _waitLabel.text = @"人家正在加载中...";
    _waitLabel.textColor = [UIColor whiteColor];
    _waitLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    [_bagV addSubview:_waitLabel];
    _bagV.alpha = 0;
}

-(void)createScrollView{
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, -1, HWIDTH, HHEIGHT+1);
    _scrollView.contentOffset = CGPointMake(0, 0);
    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _scrollView.bounces = NO;//弹簧效果？？
    NSLog(@"-=----=%f",_scrollView.contentOffset.y);
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
}

-(void)createSelectWorkView{
    if (_t==1) {
        _selectScrollView = [[UIScrollView alloc]init];
        _selectScrollView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"di"]];
        _selectScrollView.frame = CGRectMake(HWIDTH*1.0/4, HHEIGHT*1.0/3, HWIDTH*1.0/2,HHEIGHT*1.0/3);
        [self.view addSubview:_selectScrollView];
        
        
//    _selectView = [[UIView alloc]init];
//    _selectView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"di"]];
//    [self.view addSubview:_selectView];
        
    _selectUrl = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_cartoonID];
    NSLog(@"%@",_selectUrl);
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //    [manager.requestSerializer setValue:g_App.userInfo.userID forHTTPHeaderField:@"userID"];
    [manager GET:_selectUrl parameters:nil
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             id backData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                           options:NSJSONReadingMutableContainers
                                                             error:nil];
             NSDictionary *dic = backData[@"results"];
             NSArray *cartoonChapListArr = dic[@"cartoonChapterList"];//.count
             
                 for (NSDictionary *dict in cartoonChapListArr) {
                     [_chapterID addObject:dict[@"id"]];
                 }

             for (int i = 0 ; i < _chapterID.count ; i ++) {
                 int chapter = [_chapterID[i] intValue];
                 UIButton *btn = [[UIButton alloc]init];
//                 joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 40 * (i/4), joinComicWidth - 10, 35);
                 btn.frame = CGRectMake(5*(i%3+1) + (HWIDTH*1.0/2-4*HMARGIN)/3*(i%3), 5*(i/3+1)+(HHEIGHT*1.0-8*HMARGIN)/3/7*(i/3), (HWIDTH*1.0/2-4*HMARGIN)/3, (HHEIGHT*1.0/3-8*HMARGIN)/7);
                 _selectScrollView.contentSize = CGSizeMake(0, (HHEIGHT*1.0+80)/3/7*(_chapterID.count/3+1));
//                 btn.frame = CGRectMake(HMARGIN+j*((HWIDTH*1.0/2-4*HMARGIN)/3+HMARGIN), HMARGIN+i*((HHEIGHT*1.0/3-8*HMARGIN)/7+HMARGIN),(HWIDTH*1.0/2-4*HMARGIN)/3, (HHEIGHT*1.0/3-8*HMARGIN)/7);
                 [btn setBackgroundImage:[UIImage imageNamed:@"xuanjikuang"] forState:UIControlStateNormal];
                 [btn setTitle:[NSString stringWithFormat:@"%d话",i+1] forState:UIControlStateNormal];
                 btn.titleLabel.textColor = [UIColor whiteColor];
                 btn.titleLabel.font = [UIFont systemFontOfSize:10];
                 [btn.layer setCornerRadius:2.0];
                 [btn.layer setBorderWidth:2.0];
                 btn.backgroundColor = [UIColor blackColor];
                 btn.layer.borderColor = (__bridge CGColorRef)([UIColor redColor]);
                 [btn.layer setMasksToBounds:YES];
                 btn.tag = chapter;
                 [btn addTarget:self action:@selector(SelectWorkBtnClike:) forControlEvents:UIControlEventTouchUpInside];
                 [_selectScrollView addSubview:btn];
//                 k++;
             }
             
//                 }
//             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"=================>error%@",error);
         }];
    }
    _selectScrollView.alpha = 0;
    _t++;
}
-(void)createSetUI{
    //--------上视图
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0, -64, HWIDTH, 64)];
    _topView.backgroundColor = [UIColor blackColor];
    _topView.userInteractionEnabled = YES;
    [self.view addSubview:_topView];
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 30, 30, 30)];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"fanhui@2x"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(misRead) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:backBtn];
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 30, HWIDTH - 100, 30)];
    _nameLabel.textAlignment = 1;//--
    _nameLabel.textColor = [UIColor whiteColor];
    _nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
    [_topView addSubview:_nameLabel];
    UIButton *helpBtn = [[UIButton alloc]initWithFrame:CGRectMake(HWIDTH - 40, 30, 30, 30)];
    [helpBtn setBackgroundImage:[UIImage imageNamed:@"bangzhu@2x"] forState:UIControlStateNormal];
    [helpBtn addTarget:self action:@selector(helpUI:) forControlEvents:UIControlEventTouchUpInside];
//    [_topView addSubview:helpBtn];
    
//--------下视图
    _bottonView = [[UIView alloc]initWithFrame:CGRectMake(0, HHEIGHT, HWIDTH, 64)];//HHEIGHT/6
    _bottonView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_bottonView];
    
    _pageNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(HWIDTH/8, 5, HWIDTH/8, 10)];
    _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value+1,(unsigned long)_dataArr.count];
    _pageNumLabel.textColor = [UIColor whiteColor];
    _pageNumLabel.font = [UIFont systemFontOfSize:12.0*HHEIGHT/6/100];
//    [_bottonView addSubview:_pageNumLabel];
    UISlider *pageNumControl = [[UISlider alloc]initWithFrame:CGRectMake(HWIDTH/4 - 10, 5, HWIDTH - 120, 10)];
    pageNumControl.backgroundColor = [UIColor blackColor];
    pageNumControl.minimumValue = 1;
    pageNumControl.maximumValue = _dataArr.count;
    [pageNumControl setThumbImage:[UIImage imageNamed:@"page_a@2x"] forState:UIControlStateNormal];
    //    [pageNumControl addTarget:self action:@selector(changePage) forControlEvents:UIControlEventValueChanged];
    //    [pageNumControl addTarget:self action:@selector(dragUp) forControlEvents:UIControlEventTouchUpInside];
//    [_bottonView addSubview:pageNumControl];
    _pageNumControl = pageNumControl;
    NSArray *imageBtn = @[@"yejian@2x",@"shangyihua@2x",@"xuanji@2x",@"xiayihua@2x",@"fankui@2x"];//shangyihua@3x   henping@3x,@"baocun_1@3x"
    NSArray *nameBtn = @[@"夜间",@"上一话",@"选集",@"下一话",@"反馈"];//横屏,@"保存"
    for (int i = 0 ; i < 5; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(HWIDTH/5.0 * i, 14, HWIDTH/5.0, 30);//HHEIGHT/6 - 30 - 25
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(HWIDTH/5.0*i, 44, HWIDTH/5.0, 20)];
        label.text = nameBtn[i];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Helvetica" size:14];
        [_bottonView addSubview:label];
        [btn setImage:[UIImage imageNamed:imageBtn[i]] forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(5, HWIDTH/15, 5,HWIDTH/15 );//btn.titleLabel.bounds.size.width
        [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 100 + i;
        [_bottonView addSubview:btn];
    }
    
//--------右视图
    _rightView = [[UIView alloc]initWithFrame:CGRectMake(HWIDTH, HHEIGHT/2-35, HWIDTH/6, 70)];//HHEIGHT/2
    _rightView.backgroundColor = [UIColor blackColor];
//    [self.view addSubview:_rightView];
    NSArray *imageRightBtn = @[@"xuanji@2x",@"",@""];
    NSArray *nameRightBtn = @[@"选集",@"",@""];
//    for (int i = 0 ; i < 3; i ++) {
        _rightViewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _rightViewBtn.frame = CGRectMake(0 , _rightView.bounds.size.height/2-35, HWIDTH/6, 40);//_rightView.bounds.size.height/3.0
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, _rightView.bounds.size.height/2+5, HWIDTH/6, 30)];
        label.text = nameRightBtn[0];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Helvetica" size:14];
        label.textAlignment = NSTextAlignmentCenter;
        [_rightView addSubview:label];
        [_rightViewBtn setImage:[UIImage imageNamed:imageRightBtn[0]] forState:UIControlStateNormal];
        _rightViewBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 8, 5,8);//btn.titleLabel.bounds.size.width
        _rightViewBtn.tag = 100 ;
        [_rightViewBtn addTarget:self action:@selector(rightBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
        [_rightView addSubview:_rightViewBtn];
//    }
    
    //--------左视图
    _leftView = [[UIView alloc]initWithFrame:CGRectMake(-64, HHEIGHT/2/2-64, 64, HHEIGHT/2)];//HHEIGHT-320
    _leftView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_leftView];
    
    _leftView.userInteractionEnabled = YES;
    UILabel *bigLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 15, _leftView.bounds.size.width, 20)];
    bigLabel.textColor = [UIColor whiteColor];
    bigLabel.text = @"大";
    bigLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    bigLabel.textAlignment = NSTextAlignmentCenter;
//    [_leftView addSubview:bigLabel];
    UILabel *leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _leftView.bounds.size.height -45, _leftView.bounds.size.width, 40)];
    leftLabel.text = @"亮度";
    leftLabel.textAlignment = NSTextAlignmentCenter;
    leftLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    leftLabel.textColor = [UIColor whiteColor];
    leftLabel.numberOfLines = 0;
//    leftLabel.font = [UIFont systemFontOfSize:15];
    [_leftView addSubview:leftLabel];
    
    UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(30, 45.5, 2, _leftView.bounds.size.height - 93)];
    iv.backgroundColor = [UIColor whiteColor];
    [_leftView addSubview:iv];
    UIView *iv1 = [[UIView alloc]initWithFrame:CGRectMake(20, 40, 40, _leftView.bounds.size.height - 93+7)];
    iv1.backgroundColor = [UIColor clearColor];
    [_leftView addSubview:iv1];
//    iv1.clipsToBounds = YES;
//    _redIv = [[UIView alloc]initWithFrame:CGRectMake(0, 20, 6, _leftView.bounds.size.height - 93)];
//    _redIv.backgroundColor = [UIColor redColor];
//    [iv addSubview:_redIv];
//    _imageV = [[UIImageView alloc]initWithFrame:CGRectMake(25, 64, 14, 14)];
//    _imageV.image = [UIImage imageNamed:@"btn1@3x"];
//    [_leftView addSubview:_imageV];
//    
    _lightSlider = [[UISlider alloc]init];
    _lightSlider.backgroundColor = [UIColor clearColor];
    UIImage *image = [[UIImage imageNamed:@""] stretchableImageWithLeftCapWidth:100.0 topCapHeight:100.0];
    [_lightSlider setThumbImage:[UIImage imageNamed:@"btn1@3x"] forState:UIControlStateNormal];
//    _lightSlider.frame = CGRectMake(-91.5, 150.5, 245, 30);
    _lightSlider.frame = CGRectMake(-111.5, 110.5, 245, 30);
    if (self.view.frame.size.height<=569 && self.view.frame.size.height>=500) {
        _lightSlider.frame = CGRectMake(-86.5, 86, 195, 30);
    }else if (self.view.frame.size.height <= 481){
        _lightSlider.frame = CGRectMake(-66.5,66, 155, 30);
    }
    _lightSlider.maximumValue = 1;
    _lightSlider.minimumValue = 0.5;
    _lightSlider.value = 0.9;
    _lightNum = _lightSlider.value;
    _lightSlider.tintColor = [UIColor redColor];
    [_lightSlider addTarget:self action:@selector(lightChange:) forControlEvents:UIControlEventValueChanged];
    [_lightSlider setMinimumTrackImage:image forState:UIControlStateNormal];
    [_lightSlider setMaximumTrackImage:image forState:UIControlStateNormal];
    CGAffineTransform trans = CGAffineTransformMakeRotation(M_PI * -0.5);
    _lightSlider.transform = trans;
    [iv1 addSubview:_lightSlider];
    
    
    
//    UIPanGestureRecognizer *topRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(lightChangeUp:)];
//    [topRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
//    [_leftView addGestureRecognizer:topRecognizer];
//    UIPanGestureRecognizer *bottonRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(lightChangeDown:)];
//    [bottonRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
//    [_leftView addGestureRecognizer:bottonRecognizer];
    
}

-(void)initTagView{
    _statusLabel = [[UILabel alloc]initWithFrame:CGRectMake(2, 2, 97, 10)];
    _statusLabel.textColor = [UIColor whiteColor];
    _statusLabel.textAlignment = 0;
    _statusLabel.font = [UIFont systemFontOfSize:7];
    
    double barray =[self batteryMoniter];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(78, 1, 19, 8);
    [button setBackgroundImage:[UIImage imageNamed:@"dianchi"] forState:UIControlStateNormal];
    [_statusLabel addSubview:button];
    [self drawRect:barray andButton:button];
    
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
                _statusStr = @"无网络";
                break;
            case AFNetworkReachabilityStatusNotReachable:
                _statusStr = @"";
                break;
            case AFNetworkReachabilityStatusReachableViaWWAN:
                _statusStr = @"3G";
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
                _statusStr = @"WIFI";
                break;
            default:
                break;
        }
    }];
    [manager startMonitoring];
}
-(void)createTAGView{
    _tagView = [[UIView alloc]initWithFrame:CGRectMake(HWIDTH - 105, 3.5, 101.5, 13.5)];
    _tagView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_tagView];
    

    NSDate * senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"HH:mm"];
    NSString * locationString=[dateformatter stringFromDate:senddate];
    
    //    BOOL b = [self isDisplayedInScreen:imageView];
    int tag1 = 1;
    for (int i = 0; i < _heightSum.count; i ++) {
        if ((_scrollView.contentOffset.y >= [_heightSum[i] intValue])) {
            tag1 = i+2;
        }
    }
    
    _statusLabel.text = [NSString stringWithFormat:@"%@话 %d/%lu %@ %@", _chapterIndex,tag1,(unsigned long)_dataArr.count,locationString,_statusStr];
    
    [_tagView addSubview:_statusLabel];
    //    UIImageView *imageview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 10, 10)];
    //    imageview.image = [UIImage imageNamed:@""];
    //    [tagView addSubview:imageview];
    
}

-(void)lightChange:(id)sender{
    _scrollView.alpha = _lightSlider.value;
    _waitLabel.alpha = _scrollView.alpha;
    _lightNum = _lightSlider.value;
}

-(void)createMenuBtn{
    _setBtn = [[UIButton alloc]init];
    _setBtn.frame = CGRectMake(HWIDTH/2.0 - 50 , HHEIGHT/2.0 - 125, 100, 200);
    _setBtn.backgroundColor = [UIColor whiteColor];
    _setBtn.alpha = 0.011;
    [_setBtn addTarget:self action:@selector(toSetPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_setBtn];
    
    UIPanGestureRecognizer *topRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(setBtnMove:)];
    [_setBtn addGestureRecognizer:topRecognizer];
}

-(void)setBtnMove:(UIPanGestureRecognizer *)gesture{
    CGPoint offset = _scrollView.contentOffset;
    CGRect bounds = _scrollView.bounds;
    CGSize size = _scrollView.contentSize;
    UIEdgeInsets inset = _scrollView.contentInset;
    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
    CGFloat maximumOffset = size.height;
//    if ((_scrollView.contentOffset.y<_direction && _scrollView.contentOffset.y <= maximumOffset-HHEIGHT) || (_scrollView.contentOffset.y > _direction && _scrollView.contentOffset.y >=0)) {
        if (gesture.state == UIGestureRecognizerStateChanged){
            CGPoint point = [gesture translationInView:_scrollView];
            CGFloat y = point.y;//+ [_scrollView center].y
//            if (_scrollView.contentOffset.y - y>=0 && _scrollView.contentOffset.y-y <= maximumOffset - HHEIGHT) {
                _scrollView.contentOffset=CGPointMake(0, _scrollView.contentOffset.y - y);
                [gesture setTranslation:CGPointZero  inView:_scrollView];
            }
            
//        }
//    }
    
//    if (currentOffset >= (maximumOffset-HWIDTH*_a/2)) {
//        if (_p<_dataArr.count) {
//            [self initData];
//        }else{
//            _p=0;
//            _u=0;
//            if (_cartoonNextId) {
//                NSLog(@"--%@",_cartoonNextId);
//                _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
//                [self analysisData];
//                
//            }else {
//                UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//                [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
//                [promptAlert show];
//            }
//        }
  
        if (currentOffset >= (maximumOffset-HWIDTH*_a/2)) {
            if (_p<_dataArr.count) {
                [self initData];
            }else {
                if (_scrollView.contentOffset.y > _direction) {//if (gesture.state == UIGestureRecognizerStateEnded){
                    [self performSelector:@selector(promptAlert) withObject:nil afterDelay:1.0];
                }
            }
            if (_scrollView.contentOffset.y <_direction -15 && _scrollView.contentOffset.y < 0 ){
                NSLog(@"----%f---%f",_direction,_scrollView.contentOffset.y);
                UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这是本话的第一张啦" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
                [promptAlert show];
                [self toSetPage];
            }
        }
       //            else{
//                if (_cartoonNextId != [NSNull null]) {
//                    NSLog(@"--%@",_cartoonNextId);
//                    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
//                    _p=0;
//                    _u=0;
//                    [self analysisData];
//                }else {
//                    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
//                    [promptAlert show];
//                }
//            }
//        static int uuu=1;
//        if (uuu != 1) {
//            if (_scrollView.contentOffset.y <= 0) {
//                if (_cartoonLastId != [NSNull null]) {
//                    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonLastId];
//                    _sum = 0;
//                    [self analysisData1];
//                }else if (_cartoonLastId == [NSNull null]){
//                    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
//                    [promptAlert show];
//                }
//            }
//        }
//        uuu++;
        if (currentOffset >= maximumOffset-1) {
            NSLog(@"===============\n================");
            [self showHudToView:self.view text:@""];
            [self hideHudFromView:self.view];
        }
}
-(void)setBtnMoveBottom{
    _scrollView.contentOffset = CGPointMake(0, _scrollView.contentOffset.y - 20);
}

-(void)createPageTurnBtn{
    _rightBtn = [[UIButton alloc]init];
    _rightBtn.frame = CGRectMake(250, 0, HWIDTH - 250, HHEIGHT);
    _rightBtn.backgroundColor = [UIColor cyanColor];
    _rightBtn.alpha = 0.0;
    [_rightBtn addTarget:self action:@selector(toNextPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_rightBtn];
    
    _leftBtn = [[UIButton alloc]init];
    _leftBtn.frame = CGRectMake(0, 0, 130, HHEIGHT);
    _leftBtn.backgroundColor = [UIColor cyanColor];
    _leftBtn.alpha = 0.0;
    [_leftBtn addTarget:self action:@selector(toPreviousPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_leftBtn];
}
-(void)createHelpView{
    _helpView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, HWIDTH, HHEIGHT)];
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.image = [UIImage imageNamed:@"zhezhao_720"];
    bgView.frame = CGRectMake(0, 0, HWIDTH, HHEIGHT);
    [_helpView addSubview:bgView];
    bgView.userInteractionEnabled = YES;
    [self.view addSubview:_helpView];
    
    UILabel *label1 = [[UILabel alloc]initWithFrame:CGRectMake(0, HHEIGHT/2 - 10, 100, 20)];
    label1.text = @"上一页";
    label1.font = [UIFont fontWithName:@"Helvetica" size:20];
    label1.textColor = [UIColor greenColor];
    label1.textAlignment = 1;
    [_helpView addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(HWIDTH-100, HHEIGHT/2 - 10, 100, 20)];
    label2.text = @"下一页";
    label2.font = [UIFont fontWithName:@"Helvetica" size:20];
    label2.textColor = [UIColor greenColor];
    label2.textAlignment = 1;
    [_helpView addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc]initWithFrame:CGRectMake(HWIDTH/2.0 - 50 , HHEIGHT/2.0 - 75, 100, 150)];
    label3.text = @"菜单";
    label3.font = [UIFont fontWithName:@"Helvetica" size:20];
    label3.textColor = [UIColor greenColor];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.alpha = 1;
    [_helpView addSubview:label3];
    UITapGestureRecognizer *tapMisSet = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(misAll)];
    [bgView addGestureRecognizer:tapMisSet];
    _helpView.alpha = 0;
}

#pragma mark － 点击事件
-(void)misAll{
    _helpView.alpha = 0;
    [self misSet];
}
-(void)helpUI:(UIButton *)btn{
    _helpView.alpha = 1;
    [self misSet];
    [btn addTarget:self action:@selector(helpUI:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)misHelpUI:(UIButton *)btn{
    _helpView.alpha = 0;
    [btn addTarget:self action:@selector(helpUI:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)misRead{
    _scrollView = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:FALSE];
    //    NSString *saveUrl = [NSString stringWithFormat:@"%@r=%@",INTERFACE_PREFIXD,@"cartoonReadHistory/add"];
    //    NSString *saveReadUrl = INTERFACE_PREFIXD;
    NSDictionary *readDict;
    if (_p < 0) {
        readDict = @{@"chapterId":_chapterReadId,@"r":@"cartoonReadHistory/add",@"albumId":_witchId[_p+1]};
    }else if (_p<_dataArr.count && _p >= 0) {
        readDict = @{@"chapterId":_chapterReadId,@"r":@"cartoonReadHistory/add",@"albumId":_witchId[_p]};
    }else if (_p == _dataArr.count){
        readDict = @{@"chapterId":_chapterReadId,@"r":@"cartoonReadHistory/add",@"albumId":_witchId[_p-1]};
    }
    NSDictionary *headers;
    if (g_App.userInfo.userID != nil) {
        headers = @{@"userID":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
    }else {
        headers = @{@"userID":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
    }
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[self getUUID]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[self getUUID]};
//    }
    //    [YK_API_request startLoad:@"" extraParams:readDict object:self action:@selector(saveReadData:) method:@"POST"];
    [YK_API_request startLoad:@"http://api.playsm.com/index.php" extraParams:readDict object:self action:@selector(saveReadData:) method:@"POST" andHeaders:headers];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *chapterId = _chapterReadId;
    [defaults setObject:chapterId forKey:@"chapterId"];
    [defaults synchronize];

    [self dismissViewControllerAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshTag" object:nil];
    }];
}
-(void)saveReadData:(NSDictionary *)saveDict{
    NSLog(@"====%@",saveDict);
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveReadNum" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"upLuLu" object:nil];
}

//-(void)saveReadData:(NSDictionary *)saveDict{
//    NSLog(@"====%@",saveDict);
////    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveReadNum" object:nil];
//}

- (void)updataFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSLog(@"上传成功");
}

-(void)SelectWorkBtnClike:(UIButton *)btn{
    NSLog(@"%ld",(long)btn.tag);
    NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&page=1&size=300",INTERFACE_PREFIXD,@"cartoonChapter/albumList",(long)btn.tag];
    _urlStr = str;
    NSLog(@"%@",str);
    [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _sum = 0;
    _p = 0;
    _u = 0;
    [self misSet];
    [self analysisData];
}

-(void)toSetPage{
    [[UIApplication sharedApplication] setStatusBarHidden:FALSE];
    //添加动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.0f];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationRepeatCount:1];
    [UIView setAnimationDelegate:self];
    
    _topView.frame = CGRectMake(0, 0, HWIDTH, 64);
    _bottonView.frame = CGRectMake(0, HHEIGHT-64, HWIDTH, 64);//HHEIGHT/6
    _rightView.frame = CGRectMake(HWIDTH - 64, HHEIGHT/2-35,64 , 70);//
    _leftView.frame = CGRectMake(0, HHEIGHT/4, 64, HHEIGHT/2);
    
    _tagView.alpha = 0;
    
    [UIView commitAnimations];
    [_setBtn addTarget:self action:@selector(misSet) forControlEvents:UIControlEventTouchUpInside];
}
-(void)misSet{
    
    [[UIApplication sharedApplication] setStatusBarHidden:TRUE];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.0f];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationRepeatCount:1];
    [UIView setAnimationDelegate:self];
    
    _topView.frame = CGRectMake(0, -64, HWIDTH, 64);
    _bottonView.frame = CGRectMake(0, HHEIGHT + 20, HWIDTH, 64);
    _rightView.frame = CGRectMake(HWIDTH, HHEIGHT/2-35, 64, 70);
    _leftView.frame = CGRectMake(-64, 160, 64, HHEIGHT - 320);
    
    _tagView.alpha = 1;
    _selectScrollView.alpha = 0.0;
    [UIView commitAnimations];
    [_setBtn addTarget:self action:@selector(toSetPage) forControlEvents:UIControlEventTouchUpInside];
    _SELECTCHAPTER = YES;
}
-(void)toPreviousPage{
    if (_Horizontal && !_RightHand && _scrollView.contentOffset.x > 0) {
        _scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x - HWIDTH, 0);
    }else if (_RightHand && _scrollView.contentOffset.x < (_dataArr.count - 1) * HWIDTH){
        _scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x + HWIDTH, 0);
    }
}
-(void)toNextPage{
    if (_Horizontal && !_RightHand && _scrollView.contentOffset.x < (_dataArr.count - 1) * HWIDTH) {
        _scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x + HWIDTH, 0);
    }else if (_RightHand && _scrollView.contentOffset.x > 0){
        _scrollView.contentOffset = CGPointMake(_scrollView.contentOffset.x - HWIDTH, 0);
    }
}
-(void)bottonBtnClicked:(UIButton *)btn{
    switch (btn.tag%100) {
        case 0:{
            if (_SUN) {
                _lightSlider.maximumValue = 0.5;
                _lightSlider.minimumValue = 0;
                _lightSlider.value = _lightNum-0.5;
                _lightNum = _lightSlider.value;
                _scrollView.alpha = _lightSlider.value ;
                _waitLabel.alpha = _scrollView.alpha;
                btn.alpha = 0.5;
                _SUN = NO;
            }else{
                btn.alpha = 1.0;
                _lightSlider.maximumValue = 1.0;
                _lightSlider.minimumValue = 0.5;
                _lightSlider.value = _lightNum + 0.5;
                _lightNum = _lightSlider.value;
                _scrollView.alpha = _lightSlider.value ;
                _waitLabel.alpha = _scrollView.alpha;
                _SUN = YES;
                
            }
            break;
//            [self misSet];
//            LandscapeController *landscapeView = [[LandscapeController alloc]init];
//            [self presentViewController:landscapeView animated:NO completion:^{
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"land" object:_dataArr];
//            }];
//            break;
        }
//        case 1:{//保存到照片了。。
////            int f = _pageNumControl.value/1;
////            NSLog(@"%f",_pageNumControl.value);
//            if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusDenied) {
//                ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
//                [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
//                    if (*stop) {
//                        //点击“好”回调方法:
//                        NSLog(@"好");
//                        return;
//                    }
//                    *stop = TRUE;
//                    
//                } failureBlock:^(NSError *error) {
//                    //点击“不允许”回调方法:
//                    NSLog(@"不允许");
//                    UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"提示-请允许打开相册!\n设置方式:手机设置->隐私->照片->允许本App访问相册" message:@"保存失败" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                                [alter show];
////                    [self dismissViewControllerAnimated:YES completion:nil];
//                    
//                }];  
//            }else if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusAuthorized){
//                NSURL *url = [NSURL URLWithString:_dataArr[_p-1]];
//                UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//                UIImageWriteToSavedPhotosAlbum(testImage, self, nil, nil);
//                UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"提示-图片在相册" message:@"保存成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
//                [alter show];
//
//            }
//            break;
//            
//        }
        case 1:{//上一话
            if (_cartoonLastId != [NSNull null]) {
                _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonLastId];
                [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                _sum = 0;
                _p=0;
                _u=0;
                [self misSet];
                [self analysisData];
            }
            else{
                UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这是第一话了" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [promptAlert show];
            }
        }
            break;
        case 2:{
            if (_SELECTCHAPTER) {
                [self createSelectWorkView];//选集
                _selectScrollView.alpha =1;
                _SELECTCHAPTER = NO;
            }else{
                _selectScrollView.alpha = 0;
                _SELECTCHAPTER = YES;
            }
            
//            [btn addTarget:self action:@selector(misSelectView:) forControlEvents:UIControlEventTouchUpInside];
//            break;
//            [self misSet];
//            LandscapeController *landscapeView = [[LandscapeController alloc]init];
//            [self presentViewController:landscapeView animated:NO completion:^{
////                _scrollView = nil;
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"land" object:_urlStr];
//            }];
//            break;
//            _Horizontal = YES;
//            btn.alpha = 0.5;
//            _rightBtn.alpha = 0.02;
//            _leftBtn.alpha = 0.02;
//            [btn addTarget:self action:@selector(cancelHorizontal:) forControlEvents:UIControlEventTouchUpInside];
//            break;
        }
            break;
        case 3:{//下一话
            if (_cartoonNextId != [NSNull null]) {
                //                    NSLog(@"--%@",_cartoonNextId);
                _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
                [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
                _sum = 0;
                _p=0;
                _u=0;
                [self misSet];
                [self analysisData];
            }else{
                UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这是最后一话啦" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
                [promptAlert show];
            }
        }
            break;
        case 4:{
            NSLog(@"我找找在哪");
            
            FeedbackViewController *feedbackView = [[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
            UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
            feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nac animated:YES completion:^{
                [[UIApplication sharedApplication] setStatusBarHidden:FALSE];
            }];
            break;
//            if (_Horizontal) {
//                _RightHand = YES;
//                btn.alpha = 0.5;
//                [btn addTarget:self action:@selector(cancelRightHand:) forControlEvents:UIControlEventTouchUpInside];
//                break;
//            }else {
//                UIImageWriteToSavedPhotosAlbum(_iv.image, self, nil, nil);
//                UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"提示" message:@"该功能只在“横向移动”下有用" delegate:nil cancelButtonTitle:@"快去点“横向移动”吧" otherButtonTitles:nil];
//                [alter show];
//            }
        }
        default:
            break;
    }
}

-(void)rightBtnCliked:(UIButton *)btn{
    
    switch (btn.tag%100) {
        case 0:{
//            [_selectView removeFromSuperview];
////            _selectView = nil;
            [self createSelectWorkView];//选集
            _selectScrollView.alpha =1;
            [btn addTarget:self action:@selector(misSelectView:) forControlEvents:UIControlEventTouchUpInside];
            break;
        }
        case 1:{
//            _scrollView.alpha = 0.5;
//            btn.alpha = 0.5;
//            [btn addTarget:self  action:@selector(sun:) forControlEvents:UIControlEventTouchUpInside];
//            break;
        }case 2:{

        }
        default:
            break;
    }
}

-(void)sun:(UIButton *)btn{
    btn.alpha = 1.0;
    
    NSLog(@"%f",_lightNum);
    if (btn.tag == 104) {
        _lightSlider.maximumValue = 1.0;
        _lightSlider.minimumValue = 0.5;
        _lightSlider.value = _lightNum + 0.5;
        NSLog(@"--%f",_lightSlider.value);
        _lightNum = _lightSlider.value;
        _scrollView.alpha = _lightSlider.value ;
        _waitLabel.alpha = _scrollView.alpha;
        btn.tag = 100;
    }
    
    [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#warning 没实现呢。
-(void)dragUp{
    //    _pageNumControl.value = _h*_scrollView.contentOffset.y/_iv.frame.origin.y + 1;
    _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value,(unsigned long)_dataArr.count];
    _scrollView.contentOffset = CGPointMake(0, (int)_pageNumControl.value*HWIDTH*_a);
}

-(void)knowPage{
    if (!_Horizontal && !_RightHand) {
        _pageNumControl.value = _scrollView.contentOffset.y*_dataArr.count/_iv.frame.origin.y + 1;
//        NSLog(@"%f  %f  %f",_pageNumControl.value,_scrollView.contentOffset.y,_iv.frame.origin.y);
        _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value,(unsigned long)_dataArr.count];
    }else {
        _pageNumControl.value = _p*_scrollView.contentOffset.x/_iv.frame.origin.x + 1;
        _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value,(unsigned long)_dataArr.count];
    }
}
//-(void)lightChangeUp:(UIPanGestureRecognizer *)recognizer{
//    if (_scrollView.alpha<=1) {
//        CGPoint touchPoint = [recognizer locationInView:self.view];
////        float yValue = touchPoint.y
//        if (recognizer.state == UIGestureRecognizerStateChanged){
//            CGPoint point = [recognizer translationInView:_leftView];
//            CGFloat y = point.y;
//            _redIv.center = CGPointMake(1.7, _redIv.center.y + y);
//            _imageV.center = CGPointMake(31.5, _imageV.center.y + y);
//            [recognizer setTranslation:CGPointZero inView:_imageV];
//            [recognizer setTranslation:CGPointZero inView:_redIv];
//            _scrollView.alpha = (_redIv.center.y*1.0 + 50)/(_leftView.bounds.size.height - 93);
//            NSLog(@"%f",(_redIv.center.y - (HHEIGHT/2/2-64 + 45))*1.0/(_leftView.bounds.size.height - 93));
//        }
//    }
//}
-(void)lightChangeDown:(UIPanGestureRecognizer *)recognizer{

    if (_scrollView.alpha >=0 && (_imageV.frame.origin.y <= HHEIGHT/2/2-64+45+_leftView.bounds.size.height - 90) &&(_imageV.frame.origin.y >= HHEIGHT/2/2-64+65 )) {
        if (recognizer.state == UIGestureRecognizerStateChanged){
            CGPoint point = [recognizer translationInView:_leftView];
            CGFloat y = point.y;
            _redIv.center = CGPointMake(2.0, _redIv.center.y + y);
            _imageV.center = CGPointMake(31.5, _imageV.center.y + y);
            [recognizer setTranslation:CGPointZero inView:_imageV];
            [recognizer setTranslation:CGPointZero inView:_redIv];
           
                _scrollView.alpha = (_leftView.bounds.size.height - 93-_redIv.center.y+120)/(_leftView.bounds.size.height - 93);
        _waitLabel.alpha = _scrollView.alpha;
//            _scrollView.alpha = (_leftView.bounds.size.height - 93-_redIv.center.y+120)/(_leftView.bounds.size.height - 93);
            NSLog(@"%f",(_redIv.center.y - (HHEIGHT/2/2-64 + 45))*1.0/(_leftView.bounds.size.height - 93));
            
        }
        
        
//        CGRect frame = _redIv.frame;
//        frame.origin.y += 20;
//        _redIv.frame = frame;
//        
//        CGRect imageframe = _imageV.frame;
//        imageframe.origin.y += 20;
//        _imageV.frame = imageframe;
    }
}
-(void)misSelectView:(UIButton *)btn{
    
//    btn.tag = 10;
    _selectScrollView.alpha = 0;
    [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)cancelHorizontal:(UIButton *)btn{
    _leftBtn.alpha = 0.0;
    _rightBtn.alpha = 0.0;
    btn.alpha = 1.0;
    _Horizontal = NO;
    btn.tag = 102;
    [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)cancelRightHand:(UIButton *)btn{
    _RightHand = NO;
    btn.alpha = 1.0;
    btn.tag = 103;
    [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 代理   加载后续的内容

#pragma mark - 警告⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠⚠
#warning - 测试
#pragma mark 开始拖拽视图
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self misSet];
    _direction = _scrollView.contentOffset.y;
}
//-(void)scrollViewd

#pragma mark - 拖动结束时
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGPoint offset = _scrollView.contentOffset;
    CGRect bounds = _scrollView.bounds;
    CGSize size = _scrollView.contentSize;
    UIEdgeInsets inset = _scrollView.contentInset;
    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
    CGFloat maximumOffset = size.height;
    NSLog(@"----%f,,,%f,,,,%f",_scrollView.contentOffset.y,maximumOffset,currentOffset);
    [self createTAGView];
//    if (!_Horizontal && !_RightHand) {
//        static int v = 0;
//        v++;
    
//    if (_scrollView.contentOffset.y > _direction - 30) {
//        if (currentOffset >= maximumOffset - 3) {
            if (currentOffset >= (maximumOffset-HWIDTH*_a*2/3)) {
                if (_p<_dataArr.count) {
                    [self initData];
                }else {
                    
                    if (currentOffset >= (maximumOffset - 7)) {
                        [self toSetPage];
                        sleep(0.5);
                        [self performSelector:@selector(promptAlert) withObject:nil afterDelay:1.0];
                        
//                        [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
                        
                    }
                }
            }
//                else{
//                    if (_cartoonNextId != [NSNull null]) {
//                        //                    NSLog(@"--%@",_cartoonNextId);
//                        _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
//                        _p=0;
//                        _u=0;
//                        [self analysisData];
//                    }else {
//                        UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
//                        [promptAlert show];
//                    }
//                }
            
//        }
//    }//else if (_scrollView.contentOffset.y <= _direction+1){
//        static int uuu=1;
//        if (uuu != 1) {
//            if (_scrollView.contentOffset.y <= 0 && _scrollView.contentOffset.y <= _direction) {
//                    if (_cartoonLastId != [NSNull null]) {
//                        _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonLastId];
//                        _sum = 0;
//                        [self analysisData1];
//                    }else {//
    if (_scrollView.contentOffset.y <=_direction && _scrollView.contentOffset.y <=0){
        UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这是本话的第一张啦" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                    [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
        
        [promptAlert show];
        
                    }
//            }
//        }
//        uuu++;
//    }
    
        if (currentOffset >= maximumOffset-1) {
    NSLog(@"===============\n================");
    [self showHudToView:self.view text:@""];
    [self hideHudFromView:self.view];
        }
//    }
}
-(void)promptAlert{
    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这是本话的最后一张啦" delegate:self cancelButtonTitle:@"伦家还没看完" otherButtonTitles:@"继续看下一话", nil];
    [promptAlert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
//    NSLog(@"--------------:%ld",(long)buttonIndex);
    if (buttonIndex == 1) {
        if (_cartoonNextId != [NSNull null]) {
            //                    NSLog(@"--%@",_cartoonNextId);
            _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
            [_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            _sum = 0;
            _p=0;
            _u=0;
            [self misSet];
            [self analysisData];
        }else{
            UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"这是最后一话啦" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [promptAlert show];
        }
    }
}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGPoint offset = _scrollView.contentOffset;
//    CGRect bounds = _scrollView.bounds;
//    CGSize size = _scrollView.contentSize;
//    UIEdgeInsets inset = _scrollView.contentInset;
//    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
//    CGFloat maximumOffset = size.height;
//    NSLog(@"===%f--%f",currentOffset,maximumOffset);
//    if (currentOffset >= maximumOffset-1) {
//        NSLog(@"===============\n================");
//        [self showHudToView:self.view text:@""];
//    }
//}

//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGPoint offset = _scrollView.contentOffset;
//    CGRect bounds = _scrollView.bounds;
//    CGSize size = _scrollView.contentSize;
//    UIEdgeInsets inset = _scrollView.contentInset;
//    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
//    CGFloat maximumOffset = size.height;
//    NSLog(@"----%f,,,%f",_scrollView.contentOffset.y,maximumOffset);
//    if (!_Horizontal && !_RightHand) {
//        if (currentOffset > maximumOffset-600) {
//            if (_p<_dataArr.count) {
//                [self initData];
//            }else{
//                for (UIImageView *iv in _scrollView.subviews) {
//                    for (int i = 0; i < _p - 2; i ++) {
//                        if (iv.tag == i) {
//                            [iv removeFromSuperview];
//                        }
//                    }
//                }
//                _p=0;
//                _u=0;
//                if (_cartoonNextId) {
//                    NSLog(@"--%@",_cartoonNextId);
//                    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
//                    [self analysisData];
//                }else {
//                    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//                    [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
//                    [promptAlert show];
//                }
//            }
//        }
//        if (_scrollView.contentOffset.y <= 0) {
//            if (_cartoonLastId) {
//                UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(lastChapterSee)];
//                swipe.direction = UISwipeGestureRecognizerDirectionDown;
//                [_scrollView addGestureRecognizer:swipe];
//            }else {
//                UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//                [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
//                [promptAlert show];
//            }
//        }
//    }
//}
-(void)lastChapterSee{
    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"傻了吧" message:@"上边不给看" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
    [promptAlert show];
    NSLog(@"上边不给你看");
}
#warning - 标记


#pragma mark 滚动时
/*-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGPoint offset = _scrollView.contentOffset;
    CGRect bounds = _scrollView.bounds;
    CGSize size = _scrollView.contentSize;
    UIEdgeInsets inset = _scrollView.contentInset;
    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
    CGFloat maximumOffset = size.height;
    NSLog(@"----%f,,,%f",_scrollView.contentOffset.y,maximumOffset);
    if (!_Horizontal && !_RightHand) {
        
        if ((_scrollView.contentOffset.y > maximumOffset-1000)&&(currentOffset < maximumOffset - 240)) {
            if (_p<_dataArr.count) {
                [self initData];
            }else{
                _p=0;
                if (_cartoonNextId) {
                    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
                    [self analysisData];
                }else {
                    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
                    [promptAlert show];
                }
            }
        }
    }
}
*/
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    
//    CGPoint offset = _scrollView.contentOffset;
//    CGRect bounds = _scrollView.bounds;
//    CGSize size = _scrollView.contentSize;
//    UIEdgeInsets inset = _scrollView.contentInset;
//    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
//    CGFloat maximumOffset = size.height;
//
//    if (!_Horizontal && !_RightHand) {
//            NSLog(@"          %f,,%f",currentOffset,maximumOffset);
//            NSLog(@"              %f,,%f",_scrollView.contentOffset.y,_sum-HHEIGHT-1);
//            if ((currentOffset >= maximumOffset-4000)&&(currentOffset <= maximumOffset-4000+10)) {
//                if (_cartoonNextId) {
//#warning nextId;
//                    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
//                    _cartoonNum++;
//                    [self analysisData];
//                }else if (_cartoonNum == _idNum.count){
//                    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
//                    [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
//                    [promptAlert show];
//                }
//        }
//    }
//}




-(BOOL)shouldAutorotate{
    return NO;
}
-(void)timerFireMethod:(NSTimer *)theTimer{
//    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonLastId];
//    [self analysisData1];
    
    UIAlertView *promptAlert = (UIAlertView *)[theTimer userInfo];
    [promptAlert dismissWithClickedButtonIndex:0 animated:NO];
    promptAlert = NULL;
}

- (void)setContinueReadID:(NSString *)continueReadID{
    _continueReadID = [NSString stringWithFormat:@"%ld",[continueReadID integerValue] - 2];
}
#pragma mark - 数据加载
-(void)initImageData:(NSNotification *)noti{
    _urlStr = noti.object;
    NSLog(@"==%@",_urlStr);
    [self analysisData];
}
-(void)analysisData{
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    _chapterNumArr = [NSMutableArray arrayWithCapacity:0];
    _heightSum = [NSMutableArray arrayWithCapacity:0];
    _scrollView.contentSize = CGSizeMake(0, 0);
    _sum = 0;
//    _heightSum = [NSMutableArray array];
//    _witchId = [NSMutableArray array];
    _BGView.alpha = 1;
    [self showHudToView:self.view text:@"拼命加载漫画......"];
//    _bagV.alpha = 1;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:_urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"=======%@",backData);
        NSArray *arr = backData[@"results"];
        _cartoonNextId = backData[@"extraInfo"][@"nextChapterId"];
        _cartoonLastId = backData[@"extraInfo"][@"lastChapterId"];
        _chapterReadId = backData[@"params"][@"chapterId"];
        _idd = _continueReadID;
//        NSLog(@"--%@",_idd);
        for (NSDictionary *dic in arr) {
            NSString *imagesUrlStr = [NSString stringWithFormat:@"%@",dic[@"images"]];
//            NSString *height = [NSString stringWithFormat:@"%@",dic[@"imgHeight"]];
//            NSString *width = [NSString stringWithFormat:@"%@",dic[@"imgWidth"]];
            _cartoonID = dic[@"cartoonId"];
            _nameLabel.text = [NSString stringWithFormat:@"%@",dic[@"chapterName"]];
//            _nameLabel.text = [NSString stringWithFormat:@"%@",dic[@"cartoonName"]];
            _chapterIndex = [NSString stringWithFormat:@"%@",dic[@"chapterIndex"]];
            [_chapterNumArr addObject:[NSString stringWithFormat:@"%@",dic[@"chapterId"]]];
            [_witchId addObject:[NSString stringWithFormat:@"%@",dic[@"id"]]];
            [_dataArr addObject:imagesUrlStr];
//            [_heightArr addObject:height];
//            [_widthArr addObject:width];
        }
//        NSLog(@"-----%@",_chapterNumArr);
        [self hideHudFromView:self.view];
//        _scrollView.contentOffset = CGPointMake(0, _sum-HWIDTH*_a/2 - 300);
        
//        NSLog(@"---%@",_chapterReadId);
        for (int i = 0 ; i < _witchId.count; i ++) {
//            NSLog(@"_witchId%@----%@",_witchId[i],_idd);
            if ([_idd isEqualToString:_witchId[i]]) {
                _p = i;
                _u = i;
                _upCount = i;
                _witchOneChapter = i;
            }
        }
        _BGView.alpha = 0;
//         [NSTimer scheduledTimerWithTimeInterval:1.5 target:self selector:@selector(misWait) userInfo:nil repeats:NO];
        [self performSelector:@selector(initData) withObject:nil afterDelay:0.05];
//        [self initData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)misWait{
    _bagV.alpha = 0;
}
-(void)initData{
    NSLog(@"===%d,,,,%d",_p , _u);
    
    if (_p>0 && _NOZERO) {
        [self showHudToView:self.view text:@"拼命加载漫画......"];
        _BGView.alpha = 1;
        for (int i = 0; i < _p; i ++) {
            NSURL *url = [NSURL URLWithString:_dataArr[i]];
            UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            _a = testImage.size.height / testImage.size.width * 1.0;
            _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a+20);
            _imageView = [[UIImageView alloc]init];
//            _imageView.image = testImage;
            [_imageView setImageWithURL:url];
            _imageView.frame = CGRectMake(0, _sum, HWIDTH, HWIDTH*_a);
            _imageView.contentMode = UIViewContentModeScaleAspectFill;
            _sum = _sum + HWIDTH*_a;
            NSString *sumStr = [NSString stringWithFormat:@"%f",_sum];
            [_heightSum addObject:sumStr];
            [_scrollView addSubview:_imageView];
        }
        _BGView.alpha = 0;
        [self hideHudFromView:self.view];
        [self createTAGView];
        _scrollView.contentOffset = CGPointMake(0,_sum  - HWIDTH*_a);
        
    }
    _NOZERO = NO;
    if (_p<_dataArr.count) {
#warning 实验代码
//        dispatch_queue_t global_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//        dispatch_group_t group = dispatch_group_create();
//        __block UIImage *image = nil;
//        dispatch_group_async(group, global_queue, ^{
//            
//        });
        
        
        
#warning 原代码
        dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        if (_u==_p) {
            _BGView.alpha = 1;
            dispatch_async(globalQueue, ^{
//                __block UIImage *image = nil;
                NSURL *url = [NSURL URLWithString:_dataArr[_p]];//i
                _p++;
                UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                _a = testImage.size.height / testImage.size.width * 1.0;
                
//                int hh = [_heightArr[_p] intValue];
//                int ww = [_widthArr[_p] intValue];
//                _a = hh*1.0/ww;
                
                _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a+20);//*(i + 1)
                
//                _imageView = [[UIImageView alloc]init];
//                [_imageView setImageWithURL:url];
                dispatch_sync(dispatch_get_main_queue(), ^{
                    _imageView = [[UIImageView alloc]init];
//                    [_imageView setImage:testImage];
                    
                    _imageView.frame = CGRectMake(0, _sum, HWIDTH, HWIDTH*_a);
                    [_imageView setImageWithURL:url];
                    _imageView.contentMode = UIViewContentModeScaleAspectFill;
//                    _imageView.tag = _p;
                    _sum = _sum + HWIDTH*_a;
                    NSString *sumStr = [NSString stringWithFormat:@"%f",_sum];
                    [_heightSum addObject:sumStr];
                    [_scrollView addSubview:_imageView];
                    _u=_p;
                    [self createTAGView];
                });
            });
            _BGView.alpha = 0;
        }
    }
}
//    if (_p+4<_dataArr.count) {
//        for (int i = 0; i < 4; i ++) {
//            dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//            dispatch_async(globalQueue, ^{
//                __block UIImage *image = nil;
//                dispatch_sync(globalQueue, ^{
//                    
//                    NSURL *url = [NSURL URLWithString:_dataArr[_p]];//i
//                    image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//                    _a = image.size.height / image.size.width * 1.0;
//                    NSLog(@"   %f",_a);
//                    _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a);//*(i + 1)
//                });
//                
//                dispatch_sync(dispatch_get_main_queue(), ^{
//                    _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _sum, HWIDTH, HWIDTH*_a)];
//                    _iv.image = image;
//                    _iv.contentMode = UIViewContentModeScaleAspectFill;
//                    _iv.tag = _p;
//                    [_scrollView addSubview:_iv];
//                    _sum = _sum + HWIDTH*_a;
//                    _p++;
//                    u++;
//                });
//            });
//            NSURL *url = [NSURL URLWithString:_dataArr[_p]];//i
//            UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _a = testImage.size.height / testImage.size.width * 1.0;
//            NSLog(@"   %f",_a);
//            _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a);//*(i + 1)
//            NSLog(@"   000--%f",_scrollView.contentOffset.y);
//            _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _sum, HWIDTH, HWIDTH*_a)];
//            _iv.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _iv.contentMode = UIViewContentModeScaleAspectFill;
//            _iv.tag = _p;
//            [_scrollView addSubview:_iv];
//            _sum = _sum + HWIDTH*_a;
//            _p++;
//            u++;
//        }
//    }else{
//        for (int i = _p; i < _dataArr.count; i ++) {
//            NSURL *url = [NSURL URLWithString:_dataArr[_p]];//i
//            UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _a = testImage.size.height / testImage.size.width * 1.0;
//            NSLog(@"   %f",_a);
//            _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a);//*(i + 1)
//            NSLog(@"   000--%f",_scrollView.contentOffset.y);
//            _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _sum, HWIDTH, HWIDTH*_a)];//i*HWIDTH*_a
//            _iv.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _iv.contentMode = UIViewContentModeScaleAspectFill;
//            [_scrollView addSubview:_iv];
//            _sum = _sum + HWIDTH*_a;
//            _p++;
//        }
//    }
//}
//-(void)initData{
//    for (int i = 0 ; i < _dataArr.count ; i ++) {
//        NSURL *url = [NSURL URLWithString:_dataArr[i]];//i
//        UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//        _a = testImage.size.height / testImage.size.width * 1.0;
//        NSLog(@"   %f",_a);
//        _scrollView.contentSize = CGSizeMake(HWIDTH+1, _sum + HWIDTH*_a);//*(i + 1)
//        NSLog(@"   000--%f",_scrollView.contentOffset.y);
//        _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _sum, HWIDTH, HWIDTH*_a)];//i*HWIDTH*_a
//        _iv.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//        _iv.contentMode = UIViewContentModeScaleAspectFill;
//        [_scrollView addSubview:_iv];
//        _sum = _sum + HWIDTH*_a;
//    }
//    NSLog(@"000--%f",_scrollView.contentOffset.y);
//}
-(void)initImageDataRead:(NSNotification *)noti{
    NSString *notiStr = noti.object;
    NSArray *notiArr = [notiStr componentsSeparatedByString:@"="];
    _continueReadID = [NSString stringWithFormat:@"%@",notiArr[5]];
    _urlStr = [NSString stringWithFormat:@"%@=%@=%@=%@=%@",notiArr[0],notiArr[1],notiArr[2],notiArr[3],notiArr[4]];
    NSLog(@"==%@",_urlStr);
    [self analysisData];
}
-(void)initidNum:(NSNotification *)noti{
    _idNum = noti.object;
    NSLog(@"%@",_idNum);
}
-(void)setCartoonNum:(NSNotification *)noti{
    _cartoonNum = [noti.object intValue];
    NSLog(@"%d",_cartoonNum);
}
-(void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"continueLu1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"content" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initidNum:) name:@"idNum" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setCartoonNum:) name:@"cartoonNum" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageDataRead:) name:@"readDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"giveLoad" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initCommicName:) name:@"commicName" object:nil];
    
}
-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"commicName" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"giveLoad" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"content" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"idNum" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cartoonNum" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"continueLu1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"readDetails" object:nil];
}



#pragma makr - 网络请求时的视图
-(void)showHudToView:(UIView *)view text:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
}
-(void)hideHudFromView:(UIView *)view{
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

- (NSString *)getUUID
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc]
                                         
                                         initWithIdentifier:@"UUID"
                                         
                                         accessGroup:@"YOUR_BUNDLE_SEED.com.yourcompany.userinfo"];
    
    
    NSString *strUUID = [keychainItem objectForKey:(id)CFBridgingRelease(kSecValueData)];
    
    //首次执行该方法时，uuid为空
    if ([strUUID isEqualToString:@""])
        
    {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
        
        [keychainItem setObject:strUUID forKey:(id)CFBridgingRelease(kSecValueData)];
        
    }
    return strUUID;
}

- (void)analysisData1{
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    _chapterNumArr = [NSMutableArray arrayWithCapacity:0];
    _witchId = [NSMutableArray arrayWithCapacity:0];
    _heightSum = [NSMutableArray array];
    _BGView.alpha = 1;
    [self showHudToView:self.view text:@"拼命加载新漫画......"];
    //    _bagV.alpha = 1.0;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:_urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"=======%@",backData);
        NSArray *arr = backData[@"results"];
        _cartoonNextId = backData[@"extraInfo"][@"nextChapterId"];
        _cartoonLastId = backData[@"extraInfo"][@"lastChapterId"];
        _chapterReadId = backData[@"params"][@"chapterId"];
        _idd = backData[@"extraInfo"][@"currentAlbumId"];
        NSLog(@"--%@",_idd);
        for (NSDictionary *dic in arr) {
            NSString *imagesUrlStr = [NSString stringWithFormat:@"%@",dic[@"images"]];
            NSString *height = [NSString stringWithFormat:@"%@",dic[@"imgHeight"]];
            NSString *width = [NSString stringWithFormat:@"%@",dic[@"imgWidth"]];
            _chapterIndex = [NSString stringWithFormat:@"%@",dic[@"chapterIndex"]];
            _cartoonID = dic[@"cartoonId"];
            _nameLabel.text = [NSString stringWithFormat:@"%@",dic[@"cartoonName"]];
            _waitLabel.text = [NSString stringWithFormat:@"%@",dic[@"chapterName"]];
            [_chapterNumArr addObject:[NSString stringWithFormat:@"%@",dic[@"chapterId"]]];
            [_witchId addObject:[NSString stringWithFormat:@"%@",dic[@"id"]]];
            [_dataArr addObject:imagesUrlStr];
            [_heightArr addObject:height];
            [_widthArr addObject:width];
        }
        [self hideHudFromView:self.view];
        _BGView.alpha = 0;
        [self initUpData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];}

-(void)initUpData{
    for (int i = 0; i<_dataArr.count; i ++) {
        _BGView.alpha = 1;
        [self showHudToView:self.view text:@"拼命加载新漫画......"];
        NSURL *url = [NSURL URLWithString:_dataArr[i]];//i
        UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
        _a = testImage.size.height / testImage.size.width * 1.0;
//        int hh = [_heightArr[i] intValue];
//        int ww = [_widthArr[i] intValue];
//        _a = hh*1.0/ww;
        _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a+20);
        UIImageView *imageView = [[UIImageView alloc]init];
        [imageView setImageWithURL:url];
        imageView.frame = CGRectMake(0, _sum, HWIDTH, HWIDTH*_a);
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        [_scrollView addSubview:imageView];
        _sum = _sum + HWIDTH*_a;
        [self hideHudFromView:self.view];
    }
    _BGView.alpha = 0;
    _p = (int)_dataArr.count;
    _scrollView.contentOffset = CGPointMake(0,_sum  -HHEIGHT);
}

//-(void)initUpData1{
//    for (int i = 0; i<_upCount; i ++) {
//        [self showHudToView:self.view text:@"拼命加载新漫画......"];
//        NSURL *url = [NSURL URLWithString:_dataArr[i]];//i
//        UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//        _a = testImage.size.height / testImage.size.width * 1.0;
////        int hh = [_heightArr[i] intValue];
////        int ww = [_widthArr[i] intValue];
////        _a = hh*1.0/ww;
//        _scrollView.contentSize = CGSizeMake(HWIDTH, _sum + HWIDTH*_a+20);
//        UIImageView *imageView = [[UIImageView alloc]init];
//        [imageView setImageWithURL:url];
//        imageView.frame = CGRectMake(0, _sum, HWIDTH, HWIDTH*_a);
//        imageView.contentMode = UIViewContentModeScaleAspectFill;
//        [_scrollView addSubview:imageView];
//        _sum = _sum + HWIDTH*_a;
//        [self hideHudFromView:self.view];
//    }
//    _p = _upCount - 1;
//    _u = _upCount - 1;
//    _upCount = 0;
//    _scrollView.contentOffset = CGPointMake(0,_sum - HHEIGHT-2);
//}

- (void)drawRect:(double)barray andButton:(UIButton *)btn
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    UIColor*aColor = [UIColor colorWithRed:1 green:0.0 blue:0 alpha:1];
    
    CGContextStrokeRect(context,CGRectMake(0, 0, 19, 8));
    CGContextFillRect(context,CGRectMake(0, 0, 19, 8));
    CGContextSetLineWidth(context, 2.0);
    aColor = [UIColor whiteColor];
    CGContextSetFillColorWithColor(context, aColor.CGColor);
    aColor = [UIColor whiteColor];
    CGContextSetStrokeColorWithColor(context, aColor.CGColor);
    CGContextAddRect(context,CGRectMake(
                                        0, 0, 19, 8));
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    gradient1.frame = CGRectMake(0, 0, 19 * barray , 8);
    gradient1.colors = [NSArray arrayWithObjects:(id)[UIColor whiteColor].CGColor,
                        (id)[UIColor whiteColor].CGColor,nil];
    [btn.layer insertSublayer:gradient1 atIndex:0];
}


- (double)batteryMoniter {
    UIDevice *device = [UIDevice currentDevice];
    device.batteryMonitoringEnabled = YES;
    if (device.batteryState == UIDeviceBatteryStateUnknown) {
        NSLog(@"UnKnow");
    }else if (device.batteryState == UIDeviceBatteryStateUnplugged){
        NSLog(@"Unplugged");
    }else if (device.batteryState == UIDeviceBatteryStateCharging){
        NSLog(@"Charging");
    }else if (device.batteryState == UIDeviceBatteryStateFull){
        NSLog(@"Full");
    }
    NSLog(@"%f",device.batteryLevel);
    return device.batteryLevel;
}

//-(UIStatusBarStyle) preferredStatusBarStyle{
//    return UIStatusBarStyleLightContent;
//}

//-(BOOL) prefersStatusBarHidden{
//    return YES;
//}



@end
