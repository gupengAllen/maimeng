//
//  GridLayout.h
//  GridViewDemo
//
//

#import <UIKit/UIKit.h>
//#import "GridViewDataSource.h"
@class cartoonDetailModel;
@interface GridLayout : UICollectionViewLayout

/*!
 */
@property (nonatomic) CGSize itemSize;

/*!
 The spacing to use between items in the same row.
 */
@property (nonatomic) CGFloat itemSpacing;

/*!
 The spacing to use between lines of items in the grid.
 */
@property (nonatomic) CGFloat lineSpacing;

/*!
 The number of columns
 */
@property (nonatomic) NSInteger numberOfColumns;


//@property (nonatomic, strong) NSObject<GridDataSource> *source;
@property (nonatomic) BOOL isHeng;
@property (nonatomic) BOOL isHengPin;
@property (nonatomic) CGPoint pinchPoint;
@property (nonatomic) CGFloat scale;

#warning new
@property (nonatomic) CGFloat pointX;
@property (nonatomic) CGFloat pointY;
@property (nonatomic) CGFloat pointChangeX;
@property (nonatomic) CGFloat pointChangeY;
@property (nonatomic) CGFloat pointScale;
@property (nonatomic) CGFloat initScale1;
@property (nonatomic) CGFloat changeScale;
@property (nonatomic) BOOL    scalePoint;


@property (nonatomic) CGFloat maxScale;
@property (nonatomic) CGFloat minScale;

@property (nonatomic) NSMutableArray <cartoonDetailModel *>*cellHeightArr;
@end
