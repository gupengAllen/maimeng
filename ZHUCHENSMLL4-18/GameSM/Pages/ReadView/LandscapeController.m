//
//  LandscapeController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "LandscapeController.h"
#import "ReadController.h"
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "DetailsViewController.h"
#import "Y_X_DataInterface.h"
#import "FeedbackViewController.h"
#import "KeychainItemWrapper.h"
#import "testViewController.h"

#define HLWIDTH self.view.bounds.size.height
#define HLHEIGHT self.view.bounds.size.width
#define HLMARGIN 5

@interface LandscapeController ()<UIScrollViewDelegate>
{
    NSDictionary    *_dataDic;
    NSDictionary    *_allDict;
    NSInteger       _cartoonChapterCount;
    NSInteger       _cartoonId;
    NSMutableArray  *_dataIDArr;
    NSMutableArray  *_subButtons;
    NSMutableArray  *_heightArr;
    NSMutableArray  *_widthArr;
    UILabel         *_titleName;
    UILabel         *_label1;
    NSString        *_idd;
    
    UIView          *_redIv;
    
    UIScrollView    *_scrollView;
    NSMutableArray  *_dataArr;
    NSMutableArray  *_dataAllArr;
    NSArray         *_idNum;
    UIImageView     *_iv;
    NSString        *_nextId;
    
    UIView          *_topView;
    UIView          *_bottonView;
    UIView          *_rightView;
    UIView          *_leftView;
    UIView          *_selectView;
    UIView          *_helpView;
    
    UISlider        *_pageNumControl;
    UILabel         *_pageNumLabel;
    UILabel         *_helpLabel;
    UILabel         *_nameLabel;
    
    UIButton        *_setBtn;
    UIButton        *_rightBtn;
    UIButton        *_leftBtn;
    
    BOOL            _Horizontal;
    BOOL            _RightHand;
    BOOL            _SelectWork;
    NSString        *_Continue;
    NSString        *_urlStr;
    int              _cartoonNum;
    NSString        *_cartoonID;
    NSMutableArray  *_chapterID;
    NSMutableArray  *_chapterArr;
    NSString        *_cartoonNextId;
    NSString        *_selectUrl;
    NSString        *_chapterReadId;
    NSString        *_strwithUrl;
    UILabel         *_nameLabel2;
    
    float _a;
    float _sum;
    
    int _h;
    int _p;
    id _po;
    int _t;
}
@end

@implementation LandscapeController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _chapterID = [NSMutableArray array];
    
    
    [self addNoti];
    
    NSLog(@"%f,,,%f",self.view.bounds.size.width,self.view.bounds.size.height);
    _p = 0;
    static _n = 1;
    
    self.view.backgroundColor = [UIColor blackColor];
    
    [self createUI];//创建scrollView
    [self createSelectWorkView];
    [self createButton];//创建菜单按钮
    [self createSetUI];
    
}
#pragma mark -

-(void)aspectRatio{
    NSURL *url = [NSURL URLWithString:_dataArr[0]];
    UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    NSLog(@"%f,%f",testImage.size.width,testImage.size.height);
    _a = testImage.size.height / testImage.size.width * 1.0;
    _sum = HLWIDTH * _a * 2.0;
}

-(void)createUI{
    _scrollView = [[UIScrollView alloc]init];
    _scrollView.frame = CGRectMake(0, 0, HLWIDTH, HLHEIGHT);
    _scrollView.contentOffset = CGPointMake(0, 0);
    _scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    _scrollView.delegate = self;
    [self.view addSubview:_scrollView];
}

-(void)createSetUI{
//--------上视图
    _topView = [[UIView alloc]initWithFrame:CGRectMake(0, -64, HLHEIGHT, 64)];//HLWIDTH/6
    _topView.backgroundColor = [UIColor blackColor];
    _topView.userInteractionEnabled = YES;
    [self.view addSubview:_topView];
    
    UIButton *backBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 20, 30, 30)];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"fanhui@2x"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(misRead) forControlEvents:UIControlEventTouchUpInside];
    [_topView addSubview:backBtn];
    UILabel *nameLable = [[UILabel alloc]initWithFrame:CGRectMake(50, 20, HLWIDTH - 100, 30)];
    nameLable.textAlignment = 1;//--
//    nameLable.text = @"火影忍者特别版";
    nameLable.textColor = [UIColor whiteColor];
    nameLable.font = [UIFont systemFontOfSize:25.0*(HLHEIGHT/6)/60];
    _nameLabel2 = nameLable;
    [_topView addSubview:nameLable];
    UIButton *helpBtn = [[UIButton alloc]initWithFrame:CGRectMake(HLWIDTH - 40, 20, 30, 30)];
    [helpBtn setBackgroundImage:[UIImage imageNamed:@"bangzhu@2x"] forState:UIControlStateNormal];
    [_topView addSubview:helpBtn];
    
//--------下视图
    _bottonView = [[UIView alloc]initWithFrame:CGRectMake(0, HLWIDTH, HLHEIGHT, 64)];
    _bottonView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_bottonView];
    
    _pageNumLabel = [[UILabel alloc]initWithFrame:CGRectMake(HLHEIGHT/8, 2, HLHEIGHT/8, 10)];
    _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value+1,(unsigned long)_dataArr.count];
    _pageNumLabel.textColor = [UIColor whiteColor];
    _pageNumLabel.font = [UIFont systemFontOfSize:18.0*HLHEIGHT/6/100];
//    [_bottonView addSubview:_pageNumLabel];
    UISlider *pageNumControl = [[UISlider alloc]initWithFrame:CGRectMake(80, 5, HLWIDTH - 120, 3)];
    pageNumControl.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"jingdutiao@3x"]];
    pageNumControl.minimumValue = 1;
    pageNumControl.maximumValue = _dataArr.count;
    [pageNumControl setThumbImage:[UIImage imageNamed:@"page_a@2x"] forState:UIControlStateNormal];
//    [pageNumControl setThumbImage:[UIImage imageNamed:@"btn@3x"] forState:UIControlStateNormal];
    //    [pageNumControl addTarget:self action:@selector(changePage) forControlEvents:UIControlEventValueChanged];
    [pageNumControl addTarget:self action:@selector(dragUp) forControlEvents:UIControlEventTouchUpInside];
//    [_bottonView addSubview:pageNumControl];
    _pageNumControl = pageNumControl;
    NSArray *imageBtn = @[@"henping@3x",@"baocun_1@3x",@"fankui@2x"];
    NSArray *nameBtn = @[@"竖屏",@"保存",@"反馈"];
    for (int i = 0 ; i < 3; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(HLWIDTH/3.0 * i, 20, HLWIDTH/3.0, 30);
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(HLWIDTH/3.0*i, 44, HLWIDTH/3.0, 20)];
        label.text = nameBtn[i];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
//        label.font = [UIFont systemFontOfSize:15];
        [_bottonView addSubview:label];
        [btn setImage:[UIImage imageNamed:imageBtn[i]] forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(5, HLWIDTH/12, 5, HLWIDTH/12);
        [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 100 + i;
        [_bottonView addSubview:btn];
    }
    
//--------右视图
    _rightView = [[UIView alloc]initWithFrame:CGRectMake(HLWIDTH, 80, HLHEIGHT/8, HLWIDTH/2)];//HLWIDTH/4
    _rightView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:_rightView];
    
    NSArray *imageRightBtn = @[@"xuanji@2x",@"yejian@2x"];//,
    NSArray *nameRightBtn = @[@"选集",@"夜间"];//,
    for (int i = 0 ; i < 2; i ++) {
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0 , HLHEIGHT/2/2.0*i, 64, 30);//HLWIDTH/8
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(0, HLHEIGHT/2/2.0*i + 30, 64, 20);
        label.text = nameRightBtn[i];
        label.textColor = [UIColor whiteColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
//        label.font = [UIFont systemFontOfSize:15];
        [_rightView addSubview:label];
        [btn setImage:[UIImage imageNamed:imageRightBtn[i]] forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 5);//btn.titleLabel.bounds.size.width
        btn.tag = 100 + i;
        [btn addTarget:self action:@selector(rightBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
        [_rightView addSubview:btn];
    }
    
//--------左视图
    _leftView = [[UIView alloc]initWithFrame:CGRectMake(-64, HLWIDTH/4, 64, HLWIDTH/2)];//HLHEIGHT/8 //HLWIDTH/5
    _leftView.backgroundColor = [UIColor blackColor];
//    [self.view addSubview:_leftView];
    _leftView.userInteractionEnabled = YES;
    
    UILabel *leftLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _leftView.bounds.size.height - 40, _leftView.bounds.size.width, 40)];
//    leftLabel.text = @"亮度";
    leftLabel.textAlignment = NSTextAlignmentCenter;
    leftLabel.textColor = [UIColor whiteColor];
    leftLabel.numberOfLines = 0;
    leftLabel.font = [UIFont systemFontOfSize:15];
    [_leftView addSubview:leftLabel];
    
//    UIImageView *iv = [[UIImageView alloc]initWithFrame:CGRectMake(30, 30, 4, _leftView.bounds.size.height - 80)];
//    iv.backgroundColor = [UIColor whiteColor];
//    [_leftView addSubview:iv];
    
    
    UISwipeGestureRecognizer *topRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(lightChangeUp)];
    [topRecognizer setDirection:UISwipeGestureRecognizerDirectionUp];
    [_leftView addGestureRecognizer:topRecognizer];
    UISwipeGestureRecognizer *bottonRecognizer = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(lightChangeDown)];
    [bottonRecognizer setDirection:UISwipeGestureRecognizerDirectionDown];
    [_leftView addGestureRecognizer:bottonRecognizer];
}

-(void)createButton{
    _setBtn = [[UIButton alloc]init];
    _setBtn.frame = CGRectMake(HLWIDTH/12*5 , HLHEIGHT/12*5, HLWIDTH/6, HLHEIGHT/6);
    _setBtn.backgroundColor = [UIColor whiteColor];
    _setBtn.alpha = 0.02;
    [_setBtn addTarget:self action:@selector(toSetPage) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_setBtn];
    
}

-(void)createSelectWorkView{
    if (_t==1) {
        _selectView = [[UIView alloc]init];
        _selectView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"di"]];
        [self.view addSubview:_selectView];
        _selectUrl = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_cartoonID];
        NSLog(@"%@",_selectUrl);
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //    [manager.requestSerializer setValue:g_App.userInfo.userID forHTTPHeaderField:@"userID"];
        [manager GET:_selectUrl parameters:nil
             success:^(AFHTTPRequestOperation *operation, id responseObject) {
                 id backData = [NSJSONSerialization JSONObjectWithData:responseObject
                                                               options:NSJSONReadingMutableContainers
                                                                 error:nil];
                 NSDictionary *dic = backData[@"results"];
                 NSArray *cartoonChapListArr = dic[@"cartoonChapterList"];//.count
                 
                 for (NSDictionary *dict in cartoonChapListArr) {
                     [_chapterID addObject:dict[@"id"]];
                 }
                 
                 NSLog(@"%@",_chapterID);
                 
                 
                 int k = 0;
                 for (int i = 0; i < _chapterID.count; i ++) {
                     if (_chapterReadId == _chapterID[i]) {
                         k=i;
                     }
                 }
                 
                 int g = _chapterID.count/7;
                 int a ;
                 if (g > 3) {
                     g=3;
                 }
                 
                 if (_chapterID.count > 21) {
                     a = 21;
                 }else {
                     a = _chapterID.count-k;
                 }
                 _selectView.frame = CGRectMake(HLHEIGHT*1.0/4, HLWIDTH*1.0/3, HLHEIGHT*1.0/2, (g)*(HLWIDTH*1.0/3/3));
//                 _selectView.frame = CGRectMake(HWIDTH*1.0/4, HHEIGHT*1.0/3, HWIDTH*1.0/2, (a+1)*(HHEIGHT*1.0/3-8*HMARGIN)/7+4*HMARGIN);
                 for (int i = 0; i < a; i ++) {
                     int chapter = [_chapterID[k] intValue];
                     UIButton *btn = [[UIButton alloc]init];
                     btn.frame = CGRectMake(5*(i%7+1) + (HLHEIGHT*1.0/2-8*HLMARGIN)/7*(i%7), 5*(i/7+1)+(HLWIDTH*1.0/3-4*HLMARGIN)/3*(i/7), (HLHEIGHT*1.0/2-8*HLMARGIN)/7, (HLWIDTH*1.0/3-4*HLMARGIN)/3);
//                     btn.frame = CGRectMake(HLMARGIN+j*((HLHEIGHT*1.0/2-8*HLMARGIN)/7+HLMARGIN), HLMARGIN+i*((HLWIDTH*1.0/3-4*HLMARGIN)/3+HLMARGIN),(HLHEIGHT*1.0/2-8*HLMARGIN)/7, (HLWIDTH*1.0/3-4*HLMARGIN)/3);
                     [btn setBackgroundImage:[UIImage imageNamed:@"xuanjikuang"] forState:UIControlStateNormal];
                     [btn setTitle:[NSString stringWithFormat:@"%d话",k+1] forState:UIControlStateNormal];
                     btn.titleLabel.textColor = [UIColor whiteColor];
                     btn.titleLabel.font = [UIFont systemFontOfSize:10];
                     [btn.layer setCornerRadius:2.0];
                     [btn.layer setBorderWidth:2.0];
                     btn.backgroundColor = [UIColor blackColor];
                     btn.layer.borderColor = (__bridge CGColorRef)([UIColor redColor]);
                     [btn.layer setMasksToBounds:YES];
                     btn.tag = chapter;
                     [btn addTarget:self action:@selector(SelectWorkBtnClike:) forControlEvents:UIControlEventTouchUpInside];
                     [_selectView addSubview:btn];
                     k++;
                 }
//                 for (int i=0; i <a; i++) {
//                     for (int j = 0; j < 7; j ++) {
//                         int chapter = [_chapterID[k] intValue];
//                         UIButton *btn = [[UIButton alloc]init];
//                         btn.frame = CGRectMake(HLMARGIN+j*((HLHEIGHT*1.0/2-8*HLMARGIN)/7+HLMARGIN), HLMARGIN+i*((HLWIDTH*1.0/3-4*HLMARGIN)/3+HLMARGIN),(HLHEIGHT*1.0/2-8*HLMARGIN)/7, (HLWIDTH*1.0/3-4*HLMARGIN)/3);
//                         [btn setBackgroundImage:[UIImage imageNamed:@"xuanjikuang"] forState:UIControlStateNormal];
//                         [btn setTitle:[NSString stringWithFormat:@"%d话",k+1] forState:UIControlStateNormal];
//                         btn.titleLabel.textColor = [UIColor whiteColor];
//                         btn.titleLabel.font = [UIFont systemFontOfSize:10];
//                         [btn.layer setCornerRadius:2.0];
//                         [btn.layer setBorderWidth:2.0];
//                         btn.backgroundColor = [UIColor blackColor];
//                         btn.layer.borderColor = (__bridge CGColorRef)([UIColor redColor]);
//                         [btn.layer setMasksToBounds:YES];
//                         btn.tag = chapter;
//                         [btn addTarget:self action:@selector(SelectWorkBtnClike:) forControlEvents:UIControlEventTouchUpInside];
//                         [_selectView addSubview:btn];
//                         k++;
//                     }
//                 }
             } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                 NSLog(@"=================>error%@",error);
             }];
    }
    _selectView.alpha = 0;
    _t++;
}
//-(void)createSelectWorkView{
//    _selectView = [[UIView alloc]init];
//    _selectView.frame = CGRectMake(HLWIDTH*1.0/4, HLHEIGHT*1.0/3, HLWIDTH*1.0/2, HLHEIGHT*1.0/3);
//    _selectView.backgroundColor = [UIColor blackColor];
//    _selectView.alpha = 0;
//    [self.view addSubview:_selectView];
//    
//    for (int i = 0 ;i <3 ; i ++){
//        for (int k = 0; k < 7; k ++) {
//            UIButton *btn = [[UIButton alloc]init];
//            btn.frame = CGRectMake(HLMARGIN+k*((HLWIDTH*1.0/2-8*HLMARGIN)/7+HLMARGIN), HLMARGIN+i*((HLHEIGHT*1.0/3-4*HLMARGIN)/3+HLMARGIN),(HLWIDTH*1.0/2-8*HLMARGIN)/7, (HLHEIGHT*1.0/3-4*HLMARGIN)/3);
//            btn.backgroundColor = [UIColor redColor];
//            [btn setTitle:[NSString stringWithFormat:@"%d-%d",i+1,k+1] forState:UIControlStateNormal];
//            [btn addTarget:self action:@selector(SelectWorkBtnClike) forControlEvents:UIControlEventTouchUpInside];
//            [_selectView addSubview:btn];
//        }
//    }
//}


#pragma mark － 点击事件
-(void)misRead{
    _scrollView = nil;
    
//    NSString *saveUrl = [NSString stringWithFormat:@"%@r=%@",INTERFACE_PREFIXD,@"cartoonReadHistory/update"];
//    //    NSString *saveReadUrl = INTERFACE_PREFIXD;
//    NSDictionary *readDict = @{@"chapterId":_chapterReadId,@"r":@"cartoonReadHistory/update",@"albumId":_idd};
//    NSLog(@"---%@--%@",_chapterReadId,_cartoonID);
//    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[self getUUID]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[self getUUID]};
//    }
//    [YK_API_request startLoad:saveUrl extraParams:readDict object:self action:@selector(saveReadData:) method:@"POST"];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *chapterId = _chapterReadId;
//    [defaults setObject:chapterId forKey:@"chapterId"];
//    [defaults synchronize];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    testViewController *textVC = [[testViewController alloc]init];
//    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:textVC];
//    [self presentViewController:nav animated:YES completion:nil];
    
    
    
}

-(void)toSetPage{
    //添加动画
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.0f];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationRepeatCount:1];
    [UIView setAnimationDelegate:self];
    
    _topView.frame = CGRectMake(0, 0, HLHEIGHT, 64);//HLWIDTH/6
    _bottonView.frame = CGRectMake(0, HLWIDTH-64, HLHEIGHT, 64);//0, HLHEIGHT, HLWIDTH, HLHEIGHT/6
    _rightView.frame = CGRectMake(HLHEIGHT-64, HLWIDTH/4,64 , HLWIDTH/2);//HLWIDTH, HLHEIGHT/4, HLWIDTH/6, HLHEIGHT/2
    _leftView.frame = CGRectMake(0, HLWIDTH/4, 64, HLWIDTH/2);
    [UIView commitAnimations];
    [_setBtn addTarget:self action:@selector(misSet) forControlEvents:UIControlEventTouchUpInside];
}

-(void)misSet{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.0f];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationRepeatCount:1];
    [UIView setAnimationDelegate:self];
    
    _topView.frame = CGRectMake(0, -64, HLHEIGHT, 64);
    _bottonView.frame = CGRectMake(0, HLWIDTH + 100, HLHEIGHT, 100);
    _rightView.frame = CGRectMake(HLHEIGHT, HLWIDTH/4, 64, HLWIDTH/2);
    _leftView.frame = CGRectMake(-64, HLWIDTH/4, 64, HLWIDTH/2);//HLHEIGHT, HLHEIGHT, HLHEIGHT/6, HLWIDTH/2
    _selectView.alpha = 0.0;
    
    [UIView commitAnimations];
    
    [_setBtn addTarget:self action:@selector(toSetPage) forControlEvents:UIControlEventTouchUpInside];
}

-(void)bottonBtnClicked:(UIButton *)btn{
    switch (btn.tag%100) {
        case 0:{
            _scrollView = nil;
            [self dismissViewControllerAnimated:NO completion:^{
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"giveLoad" object:_urlStr];
            }];
            break;
        }
        case 1:{//保存到照片了。。
            NSURL *url = [NSURL URLWithString:_dataArr[_p-1]];
            UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
            UIImageWriteToSavedPhotosAlbum(testImage, self, nil, nil);
//            UIImageWriteToSavedPhotosAlbum(_iv.image, self, nil, nil);
            _label1 = [[UILabel alloc]initWithFrame:CGRectMake(HLHEIGHT/2 - 75, 100, 150, 80)];
            _label1.text = @"保存成功";
            _label1.font = [UIFont systemFontOfSize:30];
            _label1.textAlignment = 0;
            _label1.textColor = [UIColor blackColor];
            [self.view addSubview:_label1];
          
            
            [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(misMe) userInfo:nil repeats:YES];
           
            
//            UIAlertView *alter = [[UIAlertView alloc]initWithTitle:@"提示" message:@"保存成功" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
////            alter.frame = CGRectMake(HLHEIGHT/2, HLWIDTH/2, 100, 50);
////            NSLog(@"%f",)
////            alter.title = @"ai";
////            [self.view addSubview:alter];
////            [view1 addSubview:alter];
//            [alter show];
            break;
        }
        case 2:{
            FeedbackViewController *feedbackView = [[FeedbackViewController alloc] initWithNibName:@"FeedbackViewController" bundle:nil];
            UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
            feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [self presentViewController:nac animated:YES completion:nil];
            
        }
            break;
        case 3:
            break;
        default:
            break;
    }
}
-(void)misMe{
    _label1.alpha = 0;
}
-(void)rightBtnCliked:(UIButton *)btn{
    
    switch (btn.tag%100) {
        case 0:{
            [self createSelectWorkView];//选集
            _selectView.alpha =1;
            [btn addTarget:self action:@selector(misSelectView:) forControlEvents:UIControlEventTouchUpInside];
            break;
        }
        case 1:{
            _scrollView.alpha = 0.5;
            btn.alpha = 0.5;
            [btn addTarget:self  action:@selector(sun:) forControlEvents:UIControlEventTouchUpInside];
            break;
        }case 2:{
            
            break;
        }
        default:
            break;
    }
}
-(void)sun:(UIButton *)btn{
    btn.alpha = 1.0;
    _scrollView.alpha = 1.0;
    btn.tag = 100;
    [btn addTarget:self action:@selector(bottonBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)dragUp{
    _pageNumLabel.text = [NSString stringWithFormat:@"%0.0f/%lu",_pageNumControl.value,(unsigned long)_dataArr.count];
    
}
-(void)lightChangeUp{
    if (_scrollView.alpha<=1) {
        _scrollView.alpha += 0.1;
    }
}
-(void)lightChangeDown{
    if (_scrollView.alpha >=0) {
        _scrollView.alpha -= 0.1;
    }
}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
////    static int j = 1;
//    if (_n<_dataArr.count) {
//        if (_scrollView.contentOffset.y > _iv.frame.origin.y) {
//            NSURL *url = [NSURL URLWithString:_dataArr[_n]];
//            UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _a = testImage.size.height / testImage.size.width * 1.0;
//            _sum = _sum +HLHEIGHT*_a;
//            _scrollView.contentSize = CGSizeMake(HLHEIGHT, _sum);//HWIDTH*_a*(j+1)
//            
//            NSLog(@"%f    %f",HLWIDTH,_sum);
//            
//            _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _n*HLHEIGHT*_a, HLHEIGHT, HLHEIGHT*_a)];
//            _iv.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _iv.contentMode = UIViewContentModeScaleAspectFill;
//            [_scrollView addSubview:_iv];
//            _n++;
//        }
//    }
//}
#pragma mark  拖拽停止
//-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
////    static int j = 1;
//    if (_n<_dataArr.count) {
//        if (_scrollView.contentOffset.y > _iv.frame.origin.y) {
//            NSURL *url = [NSURL URLWithString:_dataArr[_n]];
//            UIImage *testImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _a = testImage.size.height / testImage.size.width * 1.0;
//            _sum = _sum +HLHEIGHT*_a;
//            _scrollView.contentSize = CGSizeMake(HLHEIGHT, _sum);//HWIDTH*_a*(j+1)
//            
//            NSLog(@"%f    %f",HLWIDTH,_sum);
//            
//            _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _n*HLHEIGHT*_a, HLHEIGHT, HLHEIGHT*_a)];
//            _iv.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
//            _iv.contentMode = UIViewContentModeScaleAspectFill;
//            [_scrollView addSubview:_iv];
//            _n++;
//        }
//    }
//}

-(void)misSelectView:(UIButton *)btn{
    btn.tag = 100;
    _selectView.alpha = 0;
    [btn addTarget:self action:@selector(rightBtnCliked:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)SelectWorkBtnClike:(UIButton *)btn{
    NSLog(@"%ld",(long)btn.tag);
    NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&page=1&size=30",INTERFACE_PREFIXD,@"cartoonChapter/albumList",(long)btn.tag];
    _urlStr = str;
    NSLog(@"%@",str);
    [self analysisData];
}
-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIDeviceOrientationLandscapeLeft;
}

#pragma mark - 
-(void)initImageData:(NSNotification *)noti{
    _urlStr = noti.object;
    NSLog(@"==%@",_urlStr);
    [self analysisData];
}
-(void)analysisData{
    
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    
    [self showHudToView:self.view text:@"一大波麦萌军正在拼命加载..."];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    //    [manager.requestSerializer setValue:@"4" forHTTPHeaderField:@"userid"];
    //    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //    [manager.requestSerializer setValue:@"application/json; 1.0" forHTTPHeaderField:@"userid"];
    [manager GET:_urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSLog(@"%@",backData);
        NSArray *arr = backData[@"results"];
        _cartoonNextId = backData[@"extraInfo"][@"nextChapterId"];
        _chapterReadId = backData[@"params"][@"chapterId"];
//        _nameLabel2.text = [NSString stringWithFormat:@"%@",backData]
        for (NSDictionary *dic in arr) {
            NSString *imagesUrlStr = [NSString stringWithFormat:@"%@",dic[@"images"]];
            NSString *height = [NSString stringWithFormat:@"%@",dic[@"imgHeight"]];
            NSString *width = [NSString stringWithFormat:@"%@",dic[@"imgWidth"]];
            _cartoonID = dic[@"cartoonId"];
            _idd = dic[@"id"];
            [_dataArr addObject:imagesUrlStr];
            [_heightArr addObject:height];
            [_widthArr addObject:width];
        }
        NSLog(@"%@",_dataArr);
        [self hideHudFromView:self.view];
        _scrollView.contentOffset = CGPointMake(0, _sum);
        [self initData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
-(void)initData{
    static int u = 1;
    
    if (_p<_dataArr.count) {
        dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_async(globalQueue, ^{
            __block UIImage *image = nil;
            dispatch_sync(globalQueue, ^{
                NSURL *url = [NSURL URLWithString:_dataArr[_p]];//i
                _p++;
                image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                _a = image.size.height / image.size.width * 1.0;
                NSLog(@"   %f",_a);
                _scrollView.contentSize = CGSizeMake(HLWIDTH+1, _sum + HLHEIGHT*_a);//(i+1)
                
                _iv.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
                _iv.contentMode = UIViewContentModeScaleAspectFill;
            });
            dispatch_sync(dispatch_get_main_queue(), ^{
                _iv = [[UIImageView alloc] initWithFrame:CGRectMake(0, _sum, HLHEIGHT, HLHEIGHT*_a)];//HLWIDTH
                _iv.image = image;
                _iv.contentMode = UIViewContentModeScaleAspectFill;
                _iv.tag = _p;
                _sum = _sum + HLHEIGHT*_a;
                [_scrollView addSubview:_iv];
            });
        });
    }
    NSLog(@"--%d",_p);
    u++;
}
#pragma mark - scrollVIew delegate
#pragma mark 开始拖拽视图
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self misSet];
}
#pragma mark - 拖动结束时
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGPoint offset = _scrollView.contentOffset;
    CGRect bounds = _scrollView.bounds;
    CGSize size = _scrollView.contentSize;
    UIEdgeInsets inset = _scrollView.contentInset;
    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
    CGFloat maximumOffset = size.height;
    NSLog(@"----%f,,,%f",_scrollView.contentOffset.y,maximumOffset);
        
        if (currentOffset > maximumOffset-1000) {
            if (_p<_dataArr.count) {
                [self initData];
            }else{
                _p=0;
                if (_cartoonNextId) {
                    _urlStr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_cartoonNextId];
                    [self analysisData];
                }else {
                    UIAlertView *promptAlert = [[UIAlertView alloc]initWithTitle:@"提示" message:@"图都被你啃光了" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
                    [NSTimer scheduledTimerWithTimeInterval:1.5f target:self selector:@selector(timerFireMethod:) userInfo:promptAlert repeats:YES];
                    [promptAlert show];
                }
            }
    }
}



- (NSString *)getUUID
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc]
                                         
                                         initWithIdentifier:@"UUID"
                                         
                                         accessGroup:@"YOUR_BUNDLE_SEED.com.yourcompany.userinfo"];
    
    NSString *strUUID = [keychainItem objectForKey:(id)CFBridgingRelease(kSecValueData)];
    
    //首次执行该方法时，uuid为空
    if ([strUUID isEqualToString:@""])
        
    {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
        
        [keychainItem setObject:strUUID forKey:(id)CFBridgingRelease(kSecValueData)];
        
    }
    return strUUID;
}


-(void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initImageData:) name:@"land" object:nil];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"land" object:nil];
}
-(BOOL)shouldAutorotate{
    return NO;
}
#pragma makr - 网络请求时的视图
-(void)showHudToView:(UIView *)view text:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
}
-(void)hideHudFromView:(UIView *)view{
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

@end
