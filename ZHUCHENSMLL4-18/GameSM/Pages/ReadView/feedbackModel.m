//
//  feedbackModel.m
//  GameSM
//
//  Created by 顾鹏 on 15/12/1.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "feedbackModel.h"
#import "Tool.h"
@implementation feedbackModel
+ (NSMutableArray *)paraseFeedBackArr:(NSArray *)feedBackArr{
    NSMutableArray *feedBackModelArr = [NSMutableArray array];
    for (NSDictionary *feedBackDict in feedBackArr) {
        feedbackModel *feedModel = [[feedbackModel alloc] init];
//        [feedModel setValuesForKeysWithDictionary:feedBackDict];
//        [feedBackModelArr addObject:feedModel];
        feedModel.id = feedBackDict[@"id"];
        feedModel.status = feedBackDict[@"status"];
        feedModel.createTime = feedBackDict[@"createTime"];
        feedModel.modifyTime = feedBackDict[@"modifyTime"];
        feedModel.userID = feedBackDict[@"userID"];
        feedModel.deviceToken = feedBackDict[@"deviceToken"];
        feedModel.deviceID = feedBackDict[@"deviceID"];
        feedModel.content = feedBackDict[@"content"];
        feedModel.contact = feedBackDict[@"contact"];
        feedModel.replyUserID = feedBackDict[@"replyUserID"];
        feedModel.feedbackID = feedBackDict[@"feedbackID"];
        feedModel.type = feedBackDict[@"type"];
        [feedBackModelArr insertObject:feedModel atIndex:0];
    }
    
    dispatch_apply(feedBackModelArr.count, dispatch_get_global_queue(0, 0), ^(size_t n) {
        feedbackModel *model = feedBackModelArr[n];

//        CGSize size = [Tool sizeOfStr:model.content andFont:[UIFont systemFontOfSize:12.0] andMaxSize:CGSizeMake(50,99999) andLineBreakMode:NSLineBreakByWordWrapping];
        CGSize sizeToFit = [model.content sizeWithFont:[UIFont systemFontOfSize:12.0] constrainedToSize:CGSizeMake(150, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//
        
        model.cellSize = sizeToFit;
    });

    return feedBackModelArr;
}

//- (void)setValue:(id)value forUndefinedKey:(NSString *)key{
//    
//}
@end
