//
//  NextNoticeViewController.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/14.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface NextNoticeViewController : BaseViewController
@property (nonatomic, copy)NSMutableArray *dataArrayType;
@property (nonatomic, copy)NSMutableArray *dataArray;

@property (weak, nonatomic)  UIView *sheetView;

@property (nonatomic, assign)NSInteger type;
@end
