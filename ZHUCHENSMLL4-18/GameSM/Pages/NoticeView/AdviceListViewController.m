//
//  AdviceListViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/11.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "AdviceListViewController.h"
#import "adviceListTableViewCell.h"
#import "adviceModel.h"
#import "NextNoticeViewController.h"
#import "NoticeViewController.h"
#import "ConstantViewController.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
@interface AdviceListViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation AdviceListViewController
{
    NSMutableArray *_adviceModelArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    UITableView *adviceList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64) style:UITableViewStylePlain];
    adviceList.dataSource = self;
    adviceList.delegate = self;
    adviceList.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:adviceList];
    [self addBackBtn];
    [self initTitleName:@"提醒"];
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 10)];
    adviceList.tableFooterView = footerView;
    

    
}

- (void)setAdviceDict:(NSDictionary *)adviceDict {
    _adviceDict = adviceDict;
    
    _adviceModelArr = [NSMutableArray array];
    NSArray *imageArr = @[@"icon_pinglun",@"icon_dianzan",@"icon_yijian",@"icon_tongzhi"];
    NSArray *nameArr = @[@"收到的评论",@"收到的赞",@"意见反馈",@"系统通知"];
    
    NSString *receivePL;
    NSString *receiveZan;
    NSString *xitongInform;
    if (_adviceDict[@"replyUnreadCount"] == nil||_adviceDict[@"replyUnreadCount"] == [NSNull null]) {
        receivePL = @"0";
    }else{
        receivePL = _adviceDict[@"replyUnreadCount"];

    }
    if (_adviceDict[ @"userPraiseUnreadCount"] == nil||_adviceDict[@"userPraiseUnreadCount"] == [NSNull null]) {
        receiveZan = @"0";
    }else{
        receiveZan = [NSString stringWithFormat:@"%@", _adviceDict[@"userPraiseUnreadCount"]];

    }
    if (_adviceDict[@"userInformUnreadCount"] == nil||_adviceDict[@"userInformUnreadCount"] == [NSNull null]) {
        xitongInform = @"0";
    }else{
        xitongInform =_adviceDict[@"userInformUnreadCount"];
    }
    
    NSArray *numberCountArr = @[receivePL,receiveZan,@"0",xitongInform];
    for(int i = 0; i < imageArr.count;i ++){
        adviceModel *advicemodel = [[adviceModel alloc] init];
        advicemodel.imageTitle = imageArr[i];
        advicemodel.desName = nameArr[i];
        advicemodel.numberCount = numberCountArr[i];
        [_adviceModelArr addObject:advicemodel];
    }
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _adviceModelArr.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 45 ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellIdentity = @"cellid";
    adviceListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentity];
//    cell.selectionStyle=UITableViewCellSelectionStyleDefault;
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"adviceListTableViewCell" owner:self options:nil] lastObject];
    }
    
    [cell setAdvicemodel:_adviceModelArr[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"unReadCount" object:nil];
    if (!g_App.userID&&(indexPath.row == 0||indexPath.row == 1)) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
        return;
        
    }
    
    adviceListTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.receiveNum.hidden = YES;
    if (indexPath.row == 0) {
        [[LogHelper shared] writeToApafrom_page:@"pnc" from_section:@"p" from_step:@"l" to_page:@"pncc" to_section:@"p" to_step:@"l" type:@"" id:@"0"];
        NoticeViewController *noticeVC = [[NoticeViewController alloc] initWithNibName:@"NoticeViewController" bundle:nil];
        noticeVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        [self.navigationController pushViewController:noticeVC animated:YES];
        
    }else if (indexPath.row == 2){
        [[LogHelper shared] writeToApafrom_page:@"pnc" from_section:@"p" from_step:@"l" to_page:@"sf" to_section:@"p" to_step:@"l" type:@"" id:@""];
        ConstantViewController *constantVC = [[ConstantViewController alloc] init];
        constantVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:constantVC animated:YES completion:nil];
        
    }else{
        [[LogHelper shared] writeToApafrom_page:@"pnc" from_section:@"p" from_step:@"l" to_page:@"pncp" to_section:@"p" to_step:@"l" type:@"" id:@"0"];
        
        NextNoticeViewController *nextVC = [[NextNoticeViewController alloc] init];
        nextVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        nextVC.type = indexPath.row;
        [self.navigationController pushViewController:nextVC animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
