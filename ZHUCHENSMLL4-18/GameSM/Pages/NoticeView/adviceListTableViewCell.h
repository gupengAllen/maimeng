//
//  adviceListTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/11.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class adviceModel;
@interface adviceListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *receiveCommentImage;
@property (weak, nonatomic) IBOutlet UILabel *receiveLabel;
@property (weak, nonatomic) IBOutlet UIButton *receiveNum;
@property (nonatomic, copy) adviceModel *advicemodel;
@end
