//
//  adviceModel.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/11.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface adviceModel : NSObject
@property (nonatomic,copy)NSString *imageTitle;
@property (nonatomic,copy)NSString *desName;
@property (nonatomic,copy)NSString *numberCount;
@end
