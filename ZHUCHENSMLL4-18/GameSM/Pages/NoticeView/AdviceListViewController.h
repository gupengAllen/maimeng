//
//  AdviceListViewController.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/11.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface AdviceListViewController : BaseViewController
@property (nonatomic, copy)NSDictionary *adviceDict;

@end
