//
//  userIDInfo.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface userIDInfo : NSObject
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSString *status;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *images;
+(userIDInfo *)paraDict:(NSDictionary *)dict;
@end
