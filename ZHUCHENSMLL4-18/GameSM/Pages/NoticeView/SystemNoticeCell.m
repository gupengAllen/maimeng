//
//  SystemNoticeCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/17.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SystemNoticeCell.h"

@implementation SystemNoticeCell

+ (id)systemNoticeCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"SystemNoticeCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[SystemNoticeCell class]]) {
            return (SystemNoticeCell *)cellObject;
        }
    }
    return nil;
}

- (void)resetType:(int)type {
    if (type == 1) {
        self.systemNoticeImage.hidden = NO;
        self.systemContentBg.hidden = NO;
        self.systemInfoLabel.hidden = YES;
        self.bounds = CGRectMake(0, 0, self.bounds.size.width, 80);
    } else if (type == 0) {
        self.systemNoticeImage.hidden = YES;
        self.systemContentBg.hidden  = YES;
        self.systemInfoLabel.hidden = NO;
        
        self.bounds = CGRectMake(0, 0, self.bounds.size.width, 70);
    }
}
@end
