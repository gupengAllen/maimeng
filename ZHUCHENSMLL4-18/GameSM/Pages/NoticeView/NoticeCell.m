//
//  NoticeCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "NoticeCell.h"

@implementation NoticeCell


+ (id)noticeCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"NoticeCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[NoticeCell class]]) {
            return (NoticeCell *)cellObject;
        }
    }
    
    return nil;
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    [self bringSubviewToFront:self.bgIV];
    self.bgIV.alpha = 0.2;
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesCancelled:touches withEvent:event];
    [UIView animateWithDuration:0.2 animations:^{
        self.bgIV.alpha = 0;
    }];
    
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesEnded:touches withEvent:event];
    [UIView animateWithDuration:0.2 animations:^{
        self.bgIV.alpha = 0;
    }];
}

//- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
//    [super setHighlighted:highlighted animated:animated];
//    if (highlighted) {
//        [(UIButton *)self.accessoryView setHighlighted:NO];
//    }
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
//    [super setSelected:selected animated:animated];
//    
//    if (selected) {
//        [(UIButton *)self.accessoryView setHighlighted:NO];
//    }
//}


@end
