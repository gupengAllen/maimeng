//
//  UserEditViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"

@interface UserEditViewController : BaseTableViewController<UITableViewDataSource, UITableViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UIView *bottomView;

@property (strong, nonatomic) IBOutlet UIView *sexView;

- (IBAction)bottomBtnPress:(id)sender;

- (IBAction)sexChooseBtnPressed:(id)sender;
@end
