//
//  EditPhotoCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface EditPhotoCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *editTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *editImageView;
+ (id)editPhotoCellOwner:(id)owner;

@end
