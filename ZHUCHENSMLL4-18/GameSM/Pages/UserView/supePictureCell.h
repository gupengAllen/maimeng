//
//  supePictureCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/16.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "secondScrollView.h"

@interface supePictureCell : UICollectionViewCell

@property (strong, nonatomic)  UITableView *tableView;
@property (strong, nonatomic)  secondScrollView *scrollView;

@property (strong, nonatomic)  UICollectionView *collectionView;
@property (strong, nonatomic)  UICollectionView *picCollectionView;

@property (strong, nonatomic)  NSMutableDictionary   *dataDic;

@property (strong, nonatomic)  UIImageView *upPicture;
@property (strong, nonatomic)  UIImageView *userImage;
@property (strong, nonatomic)  UILabel *userName;
@property (strong, nonatomic)  UILabel *upTime;
@property (strong, nonatomic)  UIButton *loveImageBtn;
@property (strong, nonatomic)  UILabel *praiseNum;
@property (strong, nonatomic)  UIImageView *lineImage;
@property (strong, nonatomic)  UILabel *otherOne;
@property (strong, nonatomic)  UILabel *picLabel;
@property (strong, nonatomic)  NSString  *type;
@property (strong, nonatomic)  UIView *rightBottomVew;
@property (strong, nonatomic)  UIView *tagBtnView;
@property (strong, nonatomic)  UILabel *tagLabel;
@property (strong, nonatomic)  UILabel *nCommentLabel;
@property (strong, nonatomic)  UIImageView *sanJiaoImage;
@property (strong, nonatomic)  UILabel *tittttlabel;
//数据
@property (strong, nonatomic)  NSMutableArray *dataArray;
@property (strong, nonatomic)  NSMutableArray *collectionArr;
@property (strong, nonatomic)  NSMutableArray *picCollectionArr;
@property (copy, nonatomic)  NSString *boolLoadData;
@end
