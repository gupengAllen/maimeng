//
//  UserEditViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserEditViewController.h"
#import "EditCell.h"
#import "EditPhotoCell.h"
#import "QiniuSDK.h"
#import "UIImageView+AFNetworking.h"
#import "EditViewController.h"
#import "DataBaseHelper.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface UserEditViewController ()

@property (nonatomic, strong) NSString *avatarURL;
@property (nonatomic, strong) NSString *nickName;
@property (nonatomic, assign) int sex;
@property (nonatomic, strong) NSString *sign;

@end

@implementation UserEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"编辑个人资料"];
    [self addBackBtn];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.dataArray = [NSMutableArray arrayWithObjects:@[@"头像"],@[@"昵称", @"性别", @"签名"], nil];
    
    self.tableView.noPullRefresh = YES;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>7.0) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }else{
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20);
    }
    
    self.sexView.frame = CGRectMake(0, self.view.bounds.size.height, ScreenSizeWidth, self.sexView.bounds.size.height);
    [self.view addSubview:self.bottomView];
    [self.view addSubview:self.sexView];
//    [self.view bringSubviewToFront:self.sexView];
    
//    self.bottomView.userInteractionEnabled = YES;
//    self.view.userInteractionEnabled = YES;
    
//    self.tableView.tableFooterView = self.bottomView;
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    
    [self userDetail];
    
    self.sexView.frame = CGRectMake(0, ScreenSizeHeight, ScreenSizeWidth, self.sexView.frame.size.height);
}

#pragma mark - 按钮事件
- (IBAction)bottomBtnPress:(id)sender {
     [self logout];
}
- (void)logout {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self synInfo];
    
}

- (void)logoutFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [[DataBaseHelper shared] dropTableView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
        
        g_App.userID = nil;
        g_App.userName = nil;
        g_App.telephone = nil;
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        g_App.userInfo = nil;
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@"" forKey:@"userID"];
        [userDefaults setObject:@"" forKey:@"sex"];
        [userDefaults setObject:@"" forKey:@"name"];
        [userDefaults setObject:@"" forKey:@"userName"];
        [userDefaults setObject:@"" forKey:@"signature"];
        [userDefaults setObject:@"" forKey:@"images"];
        [userDefaults setObject:@"" forKey:@"loginTime"];
        [userDefaults setObject:@"" forKey:@"loginType"];
        [userDefaults setObject:@"0" forKey:@"HAVENREQUEST"];
//        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [userDefaults synchronize];
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
        [[LogHelper shared] writeToFilefrom_page:@"psi" from_section:@"profile" from_step:@"home" to_page:@"psie" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];
    }
}

- (void)synInfo{
    
    NSDictionary *saveDict = @{@"r":@"cartoonCollection/synUserCollectionInfo",@"postData":[[DataBaseHelper shared] fetchCartoonAllCollectionStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:saveDict object:self action:@selector(upCollectionData:) method:POSTDATA];
    
    
    NSDictionary *readDict = @{@"r":@"cartoonReadHistory/synUserReadInfo",@"postData":    [[DataBaseHelper shared] fetchCartoonAllReadStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(upReadData:) method:POSTDATA];
    
}

- (void)upReadData:(NSDictionary *)readDict {
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_LOGOUT,@"r",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(logoutFinish:) method:POSTDATA];
    
    
}

- (void)upCollectionData:(NSDictionary *)collectionDict {
    
}


- (IBAction)sexChooseBtnPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    switch (btn.tag) {
        case 100:
            self.sex = 1;
            [self userUpdate];
            break;
        case 101:
            self.sex = 2;
            [self userUpdate];
            break;
        case 102:
            
            break;
        default:
            break;
    }
    [self hideSexView];
}

- (void)showSexView {
    [UIView animateWithDuration:0.3 animations:^{
        self.sexView.frame = CGRectMake(0, self.view.bounds.size.height - self.sexView.bounds.size.height, ScreenSizeWidth, self.sexView.bounds.size.height);
    }];
}

- (void)hideSexView {
    [UIView animateWithDuration:0.3 animations:^{
        self.sexView.frame = CGRectMake(0, self.view.bounds.size.height, ScreenSizeWidth, self.sexView.bounds.size.height);
    }];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.dataArray objectAtIndex:section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell*)[self tableView:self.tableView cellForRowAtIndexPath:indexPath];
    return cell.bounds.size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1;//
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, -1, KScreenWidth, 2)];
    lineView.backgroundColor = [UIColor whiteColor];
    [view addSubview:lineView];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 20, 2)];
    lineView.backgroundColor = [UIColor whiteColor];
    [view addSubview:lineView];
    UIImageView *lineView1 = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth-20, 0, 20, 2)];
    lineView1.backgroundColor = [UIColor whiteColor];
    [view addSubview:lineView1];
    return view;
}
- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *cellID = @"editPhotoCell";
        EditPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
        if (!cell) {
            cell = [EditPhotoCell editPhotoCellOwner:self];
            cell.editImageView.layer.cornerRadius = cell.editImageView.bounds.size.width/2;
            cell.editImageView.layer.masksToBounds = YES;
        }
        cell.editTitleLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        [cell.editImageView setImageWithURL:[NSURL URLWithString:self.avatarURL] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jinru"]];

        return cell;
    } else {
        static NSString *cellIdentifier = @"editCell";
        EditCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [EditCell editCellOwner:self];
        }
        cell.editTitleLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

        switch (indexPath.row) {
            case 0:
                cell.editContentLabel.text = self.nickName;
                break;
            case 1: {
                cell.editContentLabel.text = nil;
                if (self.sex == 1) {
                    cell.editSexImageView.image = [UIImage imageNamed:@"nan.png"];
                } else {
                    cell.editSexImageView.image = [UIImage imageNamed:@"nv.png"];
                }
            }
                break;
            case 2: {
                cell.editContentLabel.text = self.sign;
            }
                break;
            default:
                break;
        }
        [cell resetSize];
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jinru"]];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0: {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"选择照片来源" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照", @"相册", nil];
            [actionSheet showInView:self.view];
        }
            break;
        case 1: {
            switch (indexPath.row) {
                case 0: {
                    [[LogHelper shared] writeToFilefrom_page:@"psi" from_section:@"profile" from_step:@"home" to_page:@"psis" to_section:@"profile" to_step:@"r" type:@"" id:@"0"];
                    
                    EditViewController *editView = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
                    editView.titleName = @"昵称";
                    editView.nickName = self.nickName;
                    editView.type = 0;
                    [self.navigationController pushViewController:editView animated:YES];
                }
                    break;
                case 1: {
                    [self showSexView];
                }
                    break;
                case 2: {
                    EditViewController *editView = [[EditViewController alloc] initWithNibName:@"EditViewController" bundle:nil];
                    editView.titleName = @"签名";
                    editView.sign = self.sign;
                    editView.type = 1;
                    [self.navigationController pushViewController:editView animated:YES];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takePhoto];
        }
            break;
        case 1: {
            [self localPhoto];
        }
            break;
            
        default:
            break;
    }
}

- (void)takePhoto {
    UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = sourceType;
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)localPhoto {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"]) {
        UIImage* image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        NSData *data;
        if (UIImagePNGRepresentation(image) == nil) {
            data = UIImageJPEGRepresentation(image, 1.0);
        } else {
            data = UIImagePNGRepresentation(image);
        }
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL isDir;
        if (![fileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/avator", [Tool dataFilePath:nil]] isDirectory:&isDir]) {
            
            [fileManager createDirectoryAtPath:[NSString stringWithFormat:@"%@/avator", [Tool dataFilePath:nil]] withIntermediateDirectories:YES attributes:nil error:nil];
        }
        
        [fileManager createFileAtPath:[Tool dataFilePath:@"/avator/avator.jpg"] contents:data attributes:nil];
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        [self getUploadTokenForQiniu:[Tool dataFilePath:@"/avator/avator.jpg"]];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)saveUserInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:g_App.userInfo.userID forKey:@"userID"];
    [userDefaults setObject:[NSString stringWithFormat:@"%d", g_App.userInfo.sex] forKey:@"sex"];
    [userDefaults setObject:g_App.userInfo.name forKey:@"name"];
    [userDefaults setObject:g_App.userInfo.telephone forKey:@"telephone"];
    [userDefaults setObject:g_App.userInfo.signature forKey:@"signature"];
    [userDefaults setObject:g_App.userInfo.images forKey:@"images"];
    [userDefaults setObject:g_App.userInfo.loginTime forKey:@"loginTime"];
    
    [userDefaults synchronize];
}

#pragma makr - 加载数据
- (void)getUploadTokenForQiniu:(NSString*)imageStr{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_SYSTEM_GETUPTOKEN,@"r",
                                    [Tool computeMD5HashOfFileInPath:imageStr],@"md5",
                                    [NSString stringWithFormat:@"%lld",[Tool fileSizeAtPath:imageStr]],@"filesize",
                                    @"jpg",@"filetype",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(getUploadTokenForQiniuFinish:) method:POSTDATA];
}

- (void)getUploadTokenForQiniuFinish:(NSDictionary*)dic {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (![[dic objectForKey:@"code"] integerValue]) {
        self.avatarURL = [[dic objectForKey:@"results"] objectForKey:@"urlDownload"];
        if (![[[dic objectForKey:@"results"] objectForKey:@"fileExist"] boolValue]) {
            QNUploadManager *upManager = [[QNUploadManager alloc] init];
            NSData *data = [NSData dataWithContentsOfMappedFile:[Tool dataFilePath:@"/avator/avator.jpg"]];
            
            [upManager putData:data
                           key:[[[dic objectForKey:@"results"] objectForKey:@"saveKey"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                         token:[[[dic objectForKey:@"results"] objectForKey:@"uploadToken"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                      complete: ^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                          NSLog(@"%@", info);
                          NSLog(@"%@", resp);
                          self.avatarURL = [[dic objectForKey:@"results"] objectForKey:@"urlDownload"];
                          [self userUpdate];
                          [self.tableView reloadData];
                      } option:nil];
        } else {
            [self userUpdate];
            
            [self.tableView reloadData];
        }
    }
}

- (void)userDetail {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_DETAIL,@"r",
                                    g_App.userID,@"id",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(userDetailFinish:) method:GETDATA];
}

- (void)userDetailFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (![[dic objectForKey:@"code"] integerValue]) {
        self.avatarURL = [[dic objectForKey:@"results"] objectForKey:@"images"];
        self.nickName = [[dic objectForKey:@"results"] objectForKey:@"name"];
        self.sex = [[[dic objectForKey:@"results"] objectForKey:@"sex"] integerValue];
        self.sign = [[dic objectForKey:@"results"] objectForKey:@"signature"];
        g_App.userInfo.images = self.avatarURL;
        g_App.userInfo.name = self.nickName;
        g_App.userInfo.sex = self.sex;
        g_App.userInfo.signature = self.sign;
        [self.tableView reloadData];
        [self saveUserInfo];
    }
}

- (void)userUpdate {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_UPDATE,@"r",
                                    g_App.userID,@"id",nil];
    if (self.avatarURL) {
        [exprame setObject:self.avatarURL forKey:@"images"];
    }
    
    if (self.nickName) {
        [exprame setObject:self.nickName  forKey:@"name"];
    }
    if (self.sex) {
        [exprame setObject:[NSString stringWithFormat:@"%d", self.sex] forKey:@"sex"];
    }
    if (self.sign) {
        [exprame setObject:self.sign forKey:@"signature"];
    }
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(userUpdateFinish:) method:POSTDATA];

}

- (void)userUpdateFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (![[dic objectForKey:@"code"] integerValue]) {
        [self userDetail];
    }
}
@end
