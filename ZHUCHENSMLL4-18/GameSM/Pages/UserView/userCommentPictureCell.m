//
//  userCommentPictureCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "userCommentPictureCell.h"

@implementation userCommentPictureCell

+ (id)userInfoCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"userCommentPictureCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[userCommentPictureCell class]]) {
            return (userCommentPictureCell *)cellObject;
        }
    }
    return nil;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
