//
//  PointsHelpViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "PointsHelpViewController.h"
#import "UIWebView+AFNetworking.h"

#import "GiftViewController.h"

#import "PointsDetailsViewController.h"

@interface PointsHelpViewController ()<UIWebViewDelegate>
{
    UIWebView *_webView;
}
@end

@implementation PointsHelpViewController

-(void)viewWillAppear:(BOOL)animated{
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    [super addBackBtn];
    // Do any additional setup after loading the view from its nib.
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    _webView.delegate = self;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [[Y_X_DataInterface commonParams] enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    [manager GET:INTERFACE_PREFIXD parameters:@{@"r":@"userScore/center"} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"----%@",responseObject);
        NSString *html = operation.responseString;
        [_webView loadHTMLString:html baseURL:nil];
        [_webView setScalesPageToFit:YES];
        [self.view addSubview:_webView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error:%@",error);
    } ];    
}

#pragma mark - webview代理方法

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"开始加载页面");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"页面加载完毕");
    [super initTitleName:[_webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
}

#pragma mark webview每次加载之前都会调用这个方法
// 如果返回NO，代表不允许加载这个请求
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *picName = [[request URL] absoluteString];
    NSLog(@"picName is %@",picName);
    
    if (![picName isEqualToString:@"about:blank"]) {
        [_webView stopLoading];
        if ([picName isEqualToString:@"http://api.playsm.com/help.html"]) {
            PointsDetailsViewController *pointDVC = [[PointsDetailsViewController alloc]initWithNibName:@"PointsDetailsViewController" bundle:nil];
            pointDVC.urlStr = picName;//@"http://m.maimengjun.com";
            [self.navigationController pushViewController:pointDVC animated:YES];
        }else if ([picName isEqualToString:@"http://wwww.maimengjun.com/libao.html"]){
            GiftViewController *giftVC = [[GiftViewController alloc]initWithNibName:@"GiftViewController" bundle:nil];
            [self.navigationController pushViewController:giftVC animated:YES];
            
        }
        
    }

    return YES;
}

#pragma mark - 提供一个接口方法给JS调用
- (void)openCamera {
    NSLog(@"打开了照相机");
}

- (void)call {
    NSLog(@"打电话");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
