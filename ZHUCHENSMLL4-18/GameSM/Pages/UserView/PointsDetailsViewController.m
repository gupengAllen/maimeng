//
//  PointsDetailsViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/19.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "PointsDetailsViewController.h"

@interface PointsDetailsViewController ()
{
    UIWebView *_webView;
}
@end

@implementation PointsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [super addBackBtn];
    
    if (![_urlStr isEqualToString:@"http://api.playsm.com/help.html"]) {
        [self addRightBtn];
    }
    
    
    // Do any additional setup after loading the view from its nib.
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    _webView.delegate = self;
    NSURL *url = [NSURL URLWithString:self.urlStr];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    [_webView setScalesPageToFit:YES];
    [self.view addSubview:_webView];
}

-(void)addRightBtn{
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    [buttonBack setImage:[UIImage imageNamed:@"goForward_.png"] forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(goForwardWebView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *buttonBack1=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack1.frame = CGRectMake(0, 0, 40, 40);
    [buttonBack1 setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
    [buttonBack1 addTarget:self action:@selector(goBackWebView:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    UIBarButtonItem *backItem1 = [[UIBarButtonItem alloc] initWithCustomView:buttonBack1];
    
    NSArray *barArr = [[NSArray alloc]initWithObjects:backItem,backItem1, nil];
    self.navigationItem.rightBarButtonItems = barArr;
}
-(void)setUrlStr:(NSString *)urlStr{
    _urlStr = urlStr;
}
#pragma mark - webview代理方法
-(void)goBackWebView:(UIButton *)btn{
    [_webView goBack];
}
-(void)goForwardWebView:(UIButton *)btn{
    [_webView goForward];
}
- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"开始加载页面");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"页面加载完毕");
    [super initTitleName:[_webView stringByEvaluatingJavaScriptFromString:@"document.title"]];
}

#pragma mark webview每次加载之前都会调用这个方法
// 如果返回NO，代表不允许加载这个请求
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *picName = [[request URL] absoluteString];
    NSLog(@"picName is %@",picName);

    // 说明协议头是ios
    if ([@"ios" isEqualToString:request.URL.scheme]) {
        NSString *url = request.URL.absoluteString;
        NSRange range = [url rangeOfString:@":"];
        NSString *method = [request.URL.absoluteString substringFromIndex:range.location + 1];
        
        SEL selector = NSSelectorFromString(method);
        
        if ([self respondsToSelector:selector]) {
            [self performSelector:selector];
        }
        return NO;
    }
    
    return YES;
}

#pragma mark - 提供一个接口方法给JS调用
- (void)openCamera {
    NSLog(@"打开了照相机");
}

- (void)call {
    NSLog(@"打电话");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
