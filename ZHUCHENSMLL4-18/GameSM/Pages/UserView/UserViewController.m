//
//  UserViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserViewController.h"
#import "LoginViewController.h"
#import "UserEditViewController.h"
#import "UserInfoViewController.h"
#import "UserGiftViewController.h"
#import "SettingViewController.h"
#import "NoticeViewController.h"
#import "CustomNavigationController.h"
#import "LYTabBarController.h"
#import "ConstantViewController.h"

#import "UserPointsViewController.h"
#import "PointsHelpViewController.h"
#import "MBProgressHUD.h"

#import "ASIFormDataRequest.h"
#import "ASIHTTPRequest.h"

#import "upVote.h"
#import "CustomMenuView.h"
#import "UserUpLoadPicViewController.h"
#import "SaveWatchDownViewController.h"

#import "UserPictureWhoPraiseViewController.h"
#import "XLScrollViewer.h"
#import "AdviceListViewController.h"
#import "PictureAboutViewController.h"
#import "CartoonCommentViewController.h"
#import "DataBaseHelper.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface UserViewController ()<UIImagePickerControllerDelegate,UIScrollViewDelegate>
{
    int _giftStatus;
    int _num;
    int _open;
    int _continueMarkDays;
    UIImageView *_navBarHairlineImageView;
    UIView *_bgTopView;
    NSString *_markTimeStr;
    UIImageView *_markImage;
    NSDictionary *_dataDict;
    NSArray *_nameArr;
    SaveWatchDownViewController *_SWDVC;
    
}
@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSMutableArray *dataArray1;
@property (nonatomic, strong) NSMutableArray *dataArray2;
@property (nonatomic, strong) NSMutableArray *dataArray3;
@property (nonatomic, strong) NSMutableArray *dataArray4;


@property (nonatomic, strong) NSMutableArray *imageArray;

@property (nonatomic, strong) NSString *commentCount;
@property (nonatomic, strong) NSString *appraiseCount;
@property (nonatomic, strong) NSString *giftCount;
@property (nonatomic, strong) NSString *notificationCount;
@property (nonatomic, assign) int userInformUnreadCount;

@property (nonatomic, assign) BOOL hasNew;

@end

@implementation UserViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LoginNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"judgeSignStatus" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"unReadCount" object:nil];
}


-(void)preGiftStatus:(int)values{
    _giftStatus = values;
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    g_App.userID = [[NSUserDefaults standardUserDefaults] objectForKey:@"userID"];
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view from its nib.
    _navBarHairlineImageView = [self findHairlineImageViewUnder:self.navigationController.navigationBar];
    
    if (IsIOS7) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    _SWDVC = [[SaveWatchDownViewController alloc] init];
    self.titleView = [[CustomMenuView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 45)];
    self.titleView.backgroundColor = RGBACOLOR(227, 84, 101, 1.0);
    self.titleView.delegate = self;
    self.titleView.type = 1;
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"giftStatus"]  isEqualToString:@"1"]){
        self.titleView.titleArray = @[@"资讯",@"漫画",@"美图",@"礼包"];
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"giftStatus"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        self.titleView.titleArray = @[@"资讯",@"漫画",@"美图"];
    }
    self.titleView.currentIndex = 0;
        self.dataArray1 = [[NSMutableArray alloc] initWithObjects:@"  我评论过的资讯",@"  我赞过的资讯", nil];

        self.dataArray2 = [[NSMutableArray alloc] initWithObjects:@"  我评论过的漫画",@"  我的阅读历史",@"  我的收藏",@"  我的下载", nil];

        self.dataArray3 = [[NSMutableArray alloc] initWithObjects:@"  我评论过的美图",@"  我赞过的美图",@"  我上传的美图", nil];

        self.dataArray4 = [[NSMutableArray alloc] initWithObjects:@"  我已领的礼包", nil];
    
    [self initTopView];
    
    if (IsIOS7) {
        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            [self.tableView setSeparatorInset:UIEdgeInsetsZero];
        }
        if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            [self.tableView setLayoutMargins:UIEdgeInsetsZero];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveRefreshNotification:) name:RefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveLoginNotification:) name:LoginNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDetail) name:@"judgeSignStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDetail) name:@"unReadCount" object:nil];
    CGPoint point = self.headImageView.center;
    self.sexImageView.center = CGPointMake(point.x+30, point.y +25);
    
    [self createScrollView];
}

#pragma mark - 视图
-(void)createScrollView{
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 44*4)];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    [self.scrollView alwaysBounceHorizontal];
    self.scrollView.delegate = self;
    self.tableView.tableFooterView = self.scrollView;
    
    self.scrollView.contentSize = CGSizeMake(KScreenWidth*4, 0);
    self.scrollView.pagingEnabled = YES;
    
    for (int i = 0; i <4; i++) {
        UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(i*ScreenSizeWidth, 0, ScreenSizeWidth, 44*4)];
        tableView.scrollEnabled = NO;
        tableView.tag = 1000+i;
        tableView.delegate = self;
        tableView.dataSource = self;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.scrollView addSubview:tableView];
    }
}

-(void)initTopView{
    
    UIView *bgTopWhiteView = [[UIView alloc]initWithFrame:CGRectMake(0, -KScreenheight, KScreenWidth, KScreenheight)];
    bgTopWhiteView.backgroundColor = RGBACOLOR(255, 93, 112, 1.0);//[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_t"]];
    [self.tableView addSubview:bgTopWhiteView];
    
    UIImageView *imageViewA = [[UIImageView alloc] initWithFrame:self.headBgView.bounds];
    imageViewA.image = [UIImage imageNamed:@"denglutouxiang_"];
    
    self.headImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGe = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(editSelfMessage:)];
    tapGe.numberOfTapsRequired = 1;
    [self.headImageView addGestureRecognizer:tapGe];
    
    self.loginBtn.layer.cornerRadius = 8;
    self.loginBtn.layer.masksToBounds = YES;
    self.loginBtn.backgroundColor = [UIColor whiteColor];

    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 72, 48)];
    imageView.image = [UIImage imageNamed:@"tips_mengwo"];
    [self.markBtn addSubview:imageView];
    self.markBtn.backgroundColor = [UIColor clearColor];
    
    self.dianImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 8, 8)];
    self.dianImage.hidden = YES;
    self.dianImage.backgroundColor = [UIColor whiteColor];
    self.dianImage.layer.masksToBounds = YES;
    self.dianImage.layer.cornerRadius = 4;
    [self.topView addSubview:self.dianImage];
    
    self.bewrite.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width / 2.0;
    self.headImageView.layer.masksToBounds = YES;
    self.tableView.tableHeaderView = self.topView;
    
    self.noticeBtn.frame = CGRectMake(self.headImageView.frame.origin.x - 70, 125, 60, 60);
    self.dianImage.frame = CGRectMake(self.noticeBtn.frame.origin.x+45 * __Scale6, 130, 8, 8);
    self.userPointsBtn.frame = CGRectMake(self.headImageView.frame.origin.x + 10 + 88, 125, 60, 60);
//    g_App.userID = [[dic objectForKey:@"results"] objectForKey:@"id"];

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"userID"] length]>0) {
        [self userDetail];
    }
//    [self.view addSubview:self.topView];
}


//---
- (UIImageView *)findHairlineImageViewUnder:(UIView *)view {
    if ([view isKindOfClass:UIImageView.class] && view.bounds.size.height <= 1.0) {
        return (UIImageView *)view;
    }
    for (UIView *subview in view.subviews) {
        UIImageView *imageView = [self findHairlineImageViewUnder:subview];
        if (imageView) {
            return imageView;
        }
    }
    return nil;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = YES;
    [(LYTabBarController*)self.tabBarController hiddenBar:NO animated:YES];
//初始化部分界面
    NSLog(@"---%@",g_App.userID);
    if (g_App.userID) {
        self.loginBtn.hidden = YES;
        self.markBtn.hidden = NO;
        if ([g_App.userInfo.signature isEqualToString:@""]) {
            self.bewrite.text = @"这家伙很萌，什么都没有留下(￣3￣)!";
        }else{
            self.bewrite.text = g_App.userInfo.signature;
        }
        self.userNameLabel.text = [NSString stringWithFormat:@"%@",g_App.userInfo.name];
//        self.userNameLabel.textColor = RGBACOLOR(251, 69, 96, 1.0);
        [self.headImageView setImageWithURL:[NSURL URLWithString:g_App.userInfo.images] placeholderImage:nil];
    } else {
        self.loginBtn.hidden = NO;
        self.sexImageView.hidden = YES;
        self.markBtn.hidden = YES;
        self.userNameLabel.text = nil;
        self.bewrite.text = @"登录后开始卖萌哦(￣3￣)!";
        for (int i = 0; i < 4; i ++) {
            UIButton *bottomBtn = (UIButton *)[self.titleView viewWithTag:1000+i];
            [bottomBtn setTitle:@"0" forState:UIControlStateNormal];
        }
        [self.headImageView setImageWithURL:nil placeholderImage:[UIImage imageNamed:@"denglutouxiang_"]];
    }
    
    self.hasNew = NO;
//    [self noticeContentList];
    _navBarHairlineImageView.hidden = YES;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}

-(void)viewWillDisappear:(BOOL)animated{
    _navBarHairlineImageView.hidden = NO;
    self.navigationController.navigationBar.hidden = NO;
}
- (void)receiveRefreshNotification:(NSNotification*)notification {
    if (g_App.userID) {
        [self userDetail];
    }
}

- (void)receiveLoginNotification:(NSNotification*)notification {
    [self loginView];
}

#pragma mark - 按钮事件
//- (void)rightBtnPressed:(UIButton*)btn {
//    if (!g_App.userID) {
//        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
//        [self presentViewController:nav animated:YES completion:nil];
//        return;
//    }
//    UserEditViewController *editView = [[UserEditViewController alloc] initWithNibName:@"UserEditViewController" bundle:nil];
//    [self.navigationController pushViewController:editView animated:YES];
//}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    UIImage *image= [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        //        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    }
    NSLog(@"imageSize:%@",NSStringFromCGSize(image.size));
    UIImage *smallI = [self imageWithImageSimple:image scaledToSize:CGSizeMake(image.size.width, image.size.height)];
    UIImage *smallI1 = [self imageWithImageSimple:image scaledToSize:CGSizeMake(image.size.width/4, image.size.height/4)];

    NSData *fdata = UIImageJPEGRepresentation(image, 1.0);
    
    NSLog(@"imageSize:%@---%f",NSStringFromCGSize(smallI1.size),fdata.length);
    
    [self saveImage:smallI WithName:@"image.jpg"];
    [self saveImage:smallI1 WithName:@"image1.jpg"];
    [self dismissViewControllerAnimated:YES completion:nil];
}
//压缩图片--压缩成指定大小代码
- (UIImage*)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

//保存到本地
- (void)saveImage:(UIImage *)tempImage WithName:(NSString *)imageName
{
   NSData* imageData = UIImagePNGRepresentation(tempImage);

   NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);

    NSString* documentsDirectory = [paths objectAtIndex:0];

    NSString* fullPathToFile = [documentsDirectory stringByAppendingPathComponent:imageName];
    [imageData writeToFile:fullPathToFile atomically:NO];
    
}
- (void)upLoadSalesBigImage:(NSString *)bigImage MidImage:(NSString *)midImage SmallImage:(NSString *)smallImage
{
    NSURL *url = [NSURL URLWithString:@"www.baidu.com"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:@"photo" forKey:@"type"];
    [request setFile:bigImage forKey:@"file_pic_big"];
    [request buildPostBody];
    [request setDelegate:self];
    [request setTimeOutSeconds:30];
    [request startAsynchronous];
}
- (IBAction)loginBtnPressed:(id)sender {
    if (!g_App.userID) {
        [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"pl" to_section:@"profile" to_step:@"r" type:@"" id:@"0"];
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
        return;
    }
}

-(void)editSelfMessage:(UIButton *)btn{
    if (!g_App.userID) {
        [self loginView];
        return;
    }
    
    [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"psi" to_section:@"profile" to_step:@"h" type:@"header" id:@"0"];
    
    UserEditViewController *editView = [[UserEditViewController alloc] initWithNibName:@"UserEditViewController" bundle:nil];
    [self.navigationController pushViewController:editView animated:YES];
}

- (IBAction)editBtnClike:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"psh" to_section:@"profile" to_step:@"h" type:@"" id:@"0"];
    SettingViewController *settinView = [[SettingViewController alloc] initWithNibName:@"SettingViewController" bundle:nil];
    [self.navigationController pushViewController:settinView animated:YES];
}

- (IBAction)noticeBtnClike:(id)sender {
//    if (!g_App.userID) {
//        [self loginView];
//        return;
//    }
    [[LogHelper shared] writeToApafrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"pnc" to_section:@"p" to_step:@"l" type:@"" id:@"0"];
    self.dianImage.hidden = YES;
    AdviceListViewController *userP = [[AdviceListViewController alloc]init];
    userP.adviceDict = _dataDict;
//    [self.navigationController pushViewController:userP animated:YES];
//    [self presentViewController:userP animated:YES completion:nil];
    [self.navigationController pushViewController:userP animated:YES];
}

- (IBAction)userPointsBtnClike:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"pi" to_section:@"profile" to_step:@"h" type:@"" id:@"0"];
    
    PointsHelpViewController *pointView = [[PointsHelpViewController alloc] initWithNibName:nil bundle:nil];
    [self.navigationController pushViewController:pointView animated:YES];
}

- (IBAction)mark:(id)sender {
    if (!g_App.userID) {
        [self loginView];
        return;
    }
    UIButton *btn = (UIButton *)sender;
    btn.userInteractionEnabled = NO;
    [self.hud show:YES];
    [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"ps" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"userScore/add",@"r",
                                    @"1",@"type",nil];
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(markFinished:) method:POSTDATA];
}
-(void)markFinished:(NSDictionary *)dict{
    [self.hud hide:YES];
    if (dict) {
        if ([[NSString stringWithFormat:@"%@",dict[@"status"]] isEqualToString:@"0"]) {
//            [[upVote shared] startAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",dict[@"score"]]  point:CGPointMake(KScreenWidth - 40,330) fatherView:self.view color:[UIColor colorWithRed:255/255.0 green:239/255.0 blue:155/255.0 alpha:1.0]];
//            [[upVote shared] startAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",dict[@"score"]]  point:CGPointMake(KScreenWidth - 50,330) fatherView:self.view color:[UIColor colorWithRed:255/255.0 green:239/255.0 blue:155/255.0 alpha:1.0]];
         /*   UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-1, 275, 2, 2)];
            imageView.image = [UIImage imageNamed:@"tips_"];
            [self.view addSubview:imageView];
            [UIView animateWithDuration:0.2 animations:^{
                imageView.frame = CGRectMake(ScreenSizeWidth/2 - 120, 250-15, 240, 80);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.1 animations:^{
                    imageView.frame = CGRectMake(ScreenSizeWidth/2 - 105, 255-15, 210, 70);
                } completion:^(BOOL finished) {
                    UIImageView *gouIV = [[UIImageView alloc]initWithFrame:CGRectMake(80, imageView.frame.size.height/2-1, 2, 2)];
                    gouIV.image = [UIImage imageNamed:@"tips_gou_"];
                    [imageView addSubview:gouIV];
                    
                    [UIView animateWithDuration:0.2 animations:^{
                        gouIV.frame = CGRectMake(65, 10, 50, 50);
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.1 animations:^{
                            gouIV.frame = CGRectMake(69, 14, 42, 42);
                        }completion:^(BOOL finished) {
                            sleep(0.2);
                            [UIView animateWithDuration:2.0 animations:^{
                                imageView.frame = CGRectMake(ScreenSizeWidth/2 - 105, 255-15-50 ,210, 70);
                                imageView.alpha = 0;
                            }completion:^(BOOL finished) {
                                [imageView removeFromSuperview];
                            }];
                        }];
                    }];
                }];
            }];
            */
            _markImage.hidden = NO;
            self.markBtn.userInteractionEnabled = NO;
            self.markBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            [self.markBtn setTitle:[NSString stringWithFormat:@"已签到%d天",_continueMarkDays+1] forState:UIControlStateNormal];
        }else{

            self.markBtn.userInteractionEnabled = YES;

        }
    }else{
        self.markBtn.userInteractionEnabled = YES;

    }
}

- (void)loginView {
    LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
    [self presentViewController:nav animated:YES completion:nil];
}
#pragma mark - CustomMenuViewDelegate
//-----------导航栏元素项点击事件
- (void)customMenuViewSelectedWithIndex:(NSInteger)index{
    
    
     [self.scrollView setContentOffset:CGPointMake(index*KScreenWidth, 0) animated:NO];
}
#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView.tag == 1000 || tableView.tag == 1001 || tableView.tag == 1002 ||tableView.tag == 1003) {
        return 0;
    }else{
        return  45;
    }
}
//-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section{
//    return 0.01;
//}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView.tag == 1000 || tableView.tag == 1001 || tableView.tag == 1002 ||tableView.tag == 1003) {
        return nil;
    }else{
        return  self.titleView;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 1000) {
        return self.dataArray1.count;
    }else if (tableView.tag == 1001){
        return self.dataArray2.count;
    }else if (tableView.tag == 1002){
        return self.dataArray3.count;
    }else if (tableView.tag == 1003){
        return self.dataArray4.count;
    }else{
         return 0;
    }
   //self.dataArray.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (tableView.tag == 1000) {
        cell.textLabel.text = [self.dataArray1 objectAtIndex:indexPath.row];
    }else if (tableView.tag == 1001){
        cell.textLabel.text = [self.dataArray2 objectAtIndex:indexPath.row];
    }else if (tableView.tag == 1002){
        cell.textLabel.text = [self.dataArray3 objectAtIndex:indexPath.row];
    }else if (tableView.tag == 1003){
        cell.textLabel.text = [self.dataArray4 objectAtIndex:indexPath.row];
    }
    cell.textLabel.textColor = RGBACOLOR(108, 108, 108, 1.0);
    cell.textLabel.font = [UIFont systemFontOfSize:15];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.detailTextLabel.text = nil;
    cell.detailTextLabel.font = [UIFont systemFontOfSize:15];
    cell.detailTextLabel.textColor = CUSTOMTITLECOLOER1;
        UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(23, cell.contentView.size.height-1, KScreenWidth - 46, 1)];
        lineView.backgroundColor = RGBACOLOR(200, 200, 200, 1.0);
        [cell.contentView addSubview:lineView];
    NSLog(@"---%f",cell.contentView.size.height);
    UIImageView *goImage = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth - 40, 10, 20, cell.contentView.size.height-20)];
    goImage.image = [UIImage imageNamed:@"jinru"];
//    [cell.contentView addSubview:goImage];
//    cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"jinru"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!g_App.userID) {
        [self loginView];
        return;
    }
    if (tableView.tag == 1000) {
        switch (indexPath.row) {
            case 0: {
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"pc" to_section:@"profile" to_step:@"l" type:@"" id:@"0"];
                UserInfoViewController *infoView = [[UserInfoViewController alloc] initWithNibName:@"UserInfoViewController" bundle:nil];
                infoView.type = 0;
                [self.navigationController pushViewController:infoView animated:YES];
            }
                break;
            case 1:{
                
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"home" to_page:@"pmp" to_section:@"profile" to_step:@"l" type:@"" id:@"0"];
                UserInfoViewController *infoView = [[UserInfoViewController alloc] initWithNibName:@"UserInfoViewController" bundle:nil];
                infoView.type = 1;
                [self.navigationController pushViewController:infoView animated:YES];
            }
                break;
            case 2: {
                
            }
                break;
                
            default:
                break;
        }
    }else if (tableView.tag == 1001){
        NSString *to_page;
        switch (indexPath.row) {
            case 1:
                to_page = @"crl";
                break;
            case 2:
                to_page = @"cfl";
                break;
            case 3:
                to_page = @"cdl";
                break;
                
            default:
                break;
        }
        
        _SWDVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
        _SWDVC.moveType = indexPath.row-1;
//        SWDVC.bookDataArr = _bookDataArr;
//        SWDVC.collectionArr = _collectionArr;
        [self addBookDataLocal];
        [self addBookCollectionDataLocal];
        switch (indexPath.row) {
            case 0: {
                
                [[LogHelper shared] writeToApafrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"pcc" to_section:@"profile" to_step:@"l" type:@"" id:@"0"];
                CartoonCommentViewController *infoView = [[CartoonCommentViewController alloc] initWithNibName:@"CartoonCommentViewController" bundle:nil];
//                infoView.type = 10;
                [self.navigationController pushViewController:infoView animated:YES];
            }
                break;
            case 1:{
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"crl" to_section:@"c" to_step:@"l" type:@"" id:@"0"];
                [self presentViewController:_SWDVC animated:YES completion:nil];
            }
                break;
            case 2: {
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"cfl" to_section:@"c" to_step:@"l" type:@"" id:@"0"];
                [self presentViewController:_SWDVC animated:YES completion:nil];
            }
                break;
            case 3:{
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"cdl" to_section:@"c" to_step:@"l" type:@"" id:@"0"];
                [self presentViewController:_SWDVC animated:YES completion:nil];
            }
                break;
            default:
                break;
        }
    }else if (tableView.tag == 1002){
        switch (indexPath.row) {
            case 0: {
                [[LogHelper shared] writeToApafrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"pic" to_section:@"profile" to_step:@"l" type:@"" id:@""];
                PictureAboutViewController *infoView = [[PictureAboutViewController alloc] initWithNibName:@"PictureAboutViewController" bundle:nil];
                infoView.type = indexPath.row;
                [self.navigationController pushViewController:infoView animated:YES];
            }
                break;
            case 1:{
                [[LogHelper shared] writeToApafrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"pip" to_section:@"profile" to_step:@"l" type:@"" id:@"0"];
                
                PictureAboutViewController *infoView = [[PictureAboutViewController alloc] init];
                infoView.type = indexPath.row;
                [self.navigationController pushViewController:infoView animated:YES];
            }
                break;
            case 2: {
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"ipl" to_section:@"i" to_step:@"l" type:@"" id:@"0"];
                UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
                [self.navigationController pushViewController:useruploadPic animated:YES];
            }
                break;
                
            default:
                break;
        }
        
    }else if (tableView.tag == 1003){
        switch (indexPath.row) {
            case 0: {
                [[LogHelper shared] writeToFilefrom_page:@"ph" from_section:@"profile" from_step:@"h" to_page:@"pg" to_section:@"g" to_step:@"l" type:@"" id:@"0"];
                
                UserGiftViewController *giftView = [[UserGiftViewController alloc] initWithNibName:@"UserGiftViewController" bundle:nil];
                [self.navigationController pushViewController:giftView animated:YES];
            }
                break;
            case 1:{
                
            }
                break;
            default:
                break;
        }
    }
    
}

-(void)showWeb:(NSString *)dict{
    NSLog(@"==%@",dict);
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

#pragma mark - UIscrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.titleView.currentIndex = (self.scrollView.contentOffset.x + KScreenWidth/2.0) / self.scrollView.bounds.size.width;
//    if (self.titleView.currentIndex>3) {
//        self.titleView.currentIndex = 3;
//    }else if (self.titleView.currentIndex<0) {
//        self.titleView.currentIndex = 0;
//    }
}



#pragma mark - 加载数据

- (void)userDetail {
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    if(g_App.userID){
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_CENTER,@"r",
                                    nil];
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(userDetailFinish:) method:GETDATA];
}

- (void)userDetailFinish:(NSDictionary*)dic {
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (![[dic objectForKey:@"code"] integerValue]) {
        _dataDict = dic[@"results"];
        
        _continueMarkDays = [[NSString stringWithFormat:@"%@",dic[@"results"][@"continueSignDays"]] intValue];
        if ([[NSString stringWithFormat:@"%@",dic[@"results"][@"isSignOfToday"]] isEqualToString:@"1"]) {
            self.markBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            _markImage.hidden = NO;
            self.markBtn.userInteractionEnabled = NO;
            [self.markBtn setTitle:[NSString stringWithFormat:@"已签到%@天",dic[@"results"][@"continueSignDays"]] forState:UIControlStateNormal];
        }else{
            self.markBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
            _markImage.hidden = YES;
            self.markBtn.userInteractionEnabled = YES;
            [self.markBtn setTitle:@"签到" forState:UIControlStateNormal];
        }
        
        int num1 = [_dataDict[@"informCount"] intValue];
        if (num1 == 0) {
            self.dianImage.hidden = YES;
        }else{
            self.dianImage.hidden = NO;
        }
        self.sexImageView.hidden = NO;
        if (g_App.userInfo.sex == 1) {
            self.sexImageView.image = [UIImage imageNamed:@"boy"];
        }else{
            self.sexImageView.image = [UIImage imageNamed:@"girl"];
        }
        [self.topView bringSubviewToFront:self.sexImageView];
    }
}

- (void)noticeContentList {
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_INFORMMESSAGE_WITHINFORM,@"r",
                                    [NSString stringWithFormat:@"%d",10],@"size",
                                    [NSString stringWithFormat:@"%d",1],@"page",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeContentListFinish:) method:GETDATA];
}

- (void)noticeContentListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        if ([[dic objectForKey:@"results"] count]) {
            self.dianImage.hidden = NO;
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            NSString *noticeID = [userDefaults objectForKey:@"noticeID"];
            if (!noticeID) {
                self.hasNew = YES;
//                [userDefaults setObject:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"] forKey:@"noticeID"];
//                [userDefaults synchronize];
            } else if ([noticeID isEqualToString:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"]]) {
                self.hasNew = NO;
                
            } else if (![noticeID isEqualToString:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"]]) {
                self.hasNew = YES;
               
//                [userDefaults setObject:[[[dic objectForKey:@"results"] firstObject] objectForKey:@"id"] forKey:@"noticeID"];
//                [userDefaults synchronize];
            }
        }else{
             self.dianImage.hidden = YES;
        }
    }
}

- (void)addBookDataLocal{
    NSMutableArray *bookInfoArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"isRead":@"1"} andChooseType:@"lastReadTime"];
    _SWDVC.bookDataArr = bookInfoArr;
    
}

- (void)addBookCollectionDataLocal{
    NSMutableArray *bookCollectionArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"collectionStatus":@"1"} andChooseType:@"collectTime"];
    _SWDVC.collectionArr = bookCollectionArr;
}


@end
