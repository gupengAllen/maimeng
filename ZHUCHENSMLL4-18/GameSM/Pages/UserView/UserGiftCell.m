//
//  UserGiftCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserGiftCell.h"

@implementation UserGiftCell

+ (id)userGiftCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"UserGiftCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[UserGiftCell class]]) {
            return (UserGiftCell *)cellObject;
        }
    }
    return nil;
}


@end
