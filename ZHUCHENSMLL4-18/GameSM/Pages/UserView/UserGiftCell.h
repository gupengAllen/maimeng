//
//  UserGiftCell.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewCell.h"

@interface UserGiftCell : BaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *userGiftTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userGiftImageView;
@property (weak, nonatomic) IBOutlet UILabel *userGiftTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *userGiftPointLabel;
@property (weak, nonatomic) IBOutlet UILabel *userGiftCodelabel;
@property (weak, nonatomic) IBOutlet UIButton *userGiftBtn;

+ (id)userGiftCellOwner:(id)owner;

@end
