//
//  UserPictureWhoPraiseViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface UserPictureWhoPraiseViewController : BaseViewController

//--
@property (strong, nonatomic)  NSMutableArray *allDataArr;
@property (assign, nonatomic)  int  initNum;
@property (assign, nonatomic)  int  type;
@property (assign, nonatomic)  int  chooseIndex;
@property (strong, nonatomic)  NSString *pictureId;
@property (strong, nonatomic)  UIButton *backBtn;
@property (strong, nonatomic)  UICollectionView *superCollectionView;
@property (strong, nonatomic)  UICollectionView *collectionView;
@property (strong, nonatomic)  UIView *bottomView;
@property (strong, nonatomic)  UIView *rightBottomVew;


@end
