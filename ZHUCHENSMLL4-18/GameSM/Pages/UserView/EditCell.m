//
//  EditCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "EditCell.h"

@implementation EditCell

+ (id)editCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"EditCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[EditCell class]]) {
            return (EditCell *)cellObject;
        }
    }
    return nil;
}

- (void)resetSize {
    CGSize size = [self.editContentLabel.text sizeWithFont:self.editContentLabel.font constrainedToSize:CGSizeMake(self.editContentLabel.bounds.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    NSLog(@"size -----> %f", size.height);
//    if (size.height > 20) {
//        self.editContentLabel.frame = CGRectMake(self.editContentLabel.frame.origin.x, self.editContentLabel.frame.origin.y, self.editContentLabel.bounds.size.width, size.height);
//        self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, size.height - 20 + 44);
//        self.editContentLabel.textAlignment = NSTextAlignmentLeft;
//    } else {
//        self.editContentLabel.frame = CGRectMake(self.editContentLabel.frame.origin.x, self.editContentLabel.frame.origin.y, self.editContentLabel.bounds.size.width, 18);
//        self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, 44);
//        self.editContentLabel.textAlignment = NSTextAlignmentRight;
//    }
    
}

//自定义分割线
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect); //上分割线，
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(20, -1, rect.size.width - 40, 1)); //下分割线
    //    CGContextSetStrokeColorWithColor(context, [UIColor orangeColor].CGColor);
    //    CGContextStrokeRect(context, CGRectMake(5, rect.size.height, rect.size.width - 10, 1));
}

@end
