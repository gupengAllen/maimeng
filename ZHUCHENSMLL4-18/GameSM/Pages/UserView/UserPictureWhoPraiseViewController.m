//
//  UserPictureWhoPraiseViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

#import "UserPictureWhoPraiseViewController.h"
#import "Config.h"
#import "CommentCell.h"
#import "UMSocial.h"
#import "supePictureCell.h"
#import "UIImageView+WebCache.h"
#import "HZPhotoBrowser.h"
#import "FaceBagModel.h"
#import "FaceBagCell.h"
#import "Y_X_DataInterface.h"
#import "MobClick.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"

@interface UserPictureWhoPraiseViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,HZPhotoBrowserDelegate,UITextFieldDelegate,UMSocialDataDelegate,UIAlertViewDelegate>
{
    NSString *_superCellIdentifier;
    NSMutableArray *_dataSource;
    UITextField *_inputTextField;
    UIImageView *_btnImage;
    
    CGFloat     _keyboardHeight;
    NSMutableArray  *_faceDataArr;
    NSMutableArray  *_faceNameArr;
    BOOL            _isFace;
    BOOL            _isReply;
    UIPageControl   *_pageC;
    UIView          *_FaceBagView;
    NSString        *_cartoonId;
    NSDictionary    *_replydict;
    
    BOOL            _yaoyiyao;
}

@end

@implementation UserPictureWhoPraiseViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"goBigPicture" object:nil];
    [[NSNotificationCenter defaultCenter]removeObserver:self name:@"goOneBigPicture" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changePicArr1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"popKeyboard" object:nil];
}

-(void)initData{
    _isFace = YES;
    _yaoyiyao = YES;
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self initData];
    
    [self createSuperCollectionView];
    [self createBottomBtn];
//--------键盘
    [self createRightBottomView];
    [self initFaceData];
    [self createFaceBag];
    [self createPageControl];
    _dataSource = [NSMutableArray array];
    // Do any additional setup after loading the view.
    
    UIView *blackView = [[UIView alloc]initWithFrame:CGRectMake(12, 25, 40, 40)];
    blackView.backgroundColor = RGBACOLOR(0, 0, 0, 0.2);
    blackView.layer.masksToBounds = YES;
    blackView.layer.cornerRadius = 20;
    [self.view addSubview:blackView];
    self.backBtn =[[UIButton alloc]initWithFrame: CGRectMake(12, 25, 40, 40)];
    [self.backBtn addTarget:self action:@selector(goBack:) forControlEvents:UIControlEventTouchUpInside];
    [self.backBtn setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
    [self.view addSubview:self.backBtn];
    
    self.superCollectionView.contentOffset = CGPointMake(ScreenSizeWidth*_initNum, 0);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goBigPicture) name:@"goBigPicture" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(goOneBigPicture:) name:@"goOneBigPicture" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePicArr:) name:@"changePicArr1" object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popKeyboard:) name:@"popKeyboard" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)goOneBigPicture:(NSNotification *)noti{
    [_dataSource removeAllObjects];
    NSDictionary *dict = noti.object;
    [_dataSource addObject:[dict objectForKey:@"images"]];//sourceImages
    //启动图片浏览器
    HZPhotoBrowser *browserVc = [[HZPhotoBrowser alloc] init];
    //    browserVc.sourceImagesContainerView = self.collectionView; // 原图的父控件
    browserVc.imageCount = _dataSource.count; // 图片总数
    browserVc.currentImageIndex = 0;
    browserVc.type = 1;
    browserVc.delegate = self;
    browserVc.dataArr = _dataSource;
//    browserVc.block = ^(int i){
//        self.superCollectionView.contentOffset = CGPointMake(ScreenSizeWidth*i, 0);
//    };
    browserVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browserVc animated:YES completion:nil];
}

-(void)goBigPicture{
    [_dataSource removeAllObjects];
    for (NSDictionary *dic in _allDataArr) {
        [_dataSource addObject:[dic objectForKey:@"sourceImages"]];
    }
    
    //启动图片浏览器
    HZPhotoBrowser *browserVc = [[HZPhotoBrowser alloc] init];
    //    browserVc.sourceImagesContainerView = self.collectionView; // 原图的父控件
    browserVc.imageCount = _dataSource.count; // 图片总数
    browserVc.currentImageIndex = self.superCollectionView.contentOffset.x/ScreenSizeWidth;
    browserVc.type = 0;
    browserVc.delegate = self;
    browserVc.dataArr = _allDataArr;
    browserVc.block = ^(int i){
        self.superCollectionView.contentOffset = CGPointMake(ScreenSizeWidth*i, 0);
    };
    browserVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:browserVc animated:YES completion:nil];
}

-(void)createSuperCollectionView{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    
    self.superCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight-64) collectionViewLayout:flowLayout];
    self.superCollectionView.delegate = self;
    self.superCollectionView.dataSource = self;
    self.superCollectionView.tag = 1000;
    self.superCollectionView.pagingEnabled = YES;
    _superCellIdentifier = @"superCellIdentifier";
    self.superCollectionView.backgroundColor = [UIColor whiteColor];//RGBACOLOR(250, 250, 250, 1.0);
    [self.superCollectionView registerClass:[supePictureCell class] forCellWithReuseIdentifier:_superCellIdentifier];
    [self.view addSubview:self.superCollectionView];
    
}

#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
}
-(void)createBottomBtn{
    self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenSizeHeight-64, ScreenSizeWidth, 64)];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.bottomView];
    [self.view bringSubviewToFront:self.bottomView];
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 1)];
    lineView.backgroundColor = RGBACOLOR(236, 236, 236, 1.0);
    [self.bottomView addSubview:lineView];
    
    NSArray *nameArr = @[@"分享",@"下载",@"评论",@"举报"];
    NSArray *imageNormalArr = @[@"btn_fenxiang",@"btn_xiazai",@"btn_pinglun",@"btn_jubao"];
    for (int i = 0; i < nameArr.count; i ++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(i*KScreenWidth/4.0, 0, KScreenWidth/4.0, 49)];
        [btn setImage:[UIImage imageNamed:imageNormalArr[i]] forState:UIControlStateNormal];
        btn.tag = 1000+i;
        [btn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
        [self.bottomView addSubview:btn];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(i*KScreenWidth/nameArr.count, 41, KScreenWidth/nameArr.count, 15)];
        label.text = [NSString stringWithFormat:@"%@",nameArr[i]];
        label.textAlignment = 1;
        label.font = [UIFont systemFontOfSize:14.0];
        [self.bottomView addSubview:label];
    }
    
}
-(void)createRightBottomView{
    //创建键盘
    self.rightBottomVew = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenSizeHeight, ScreenSizeWidth, 44)];
    self.rightBottomVew.backgroundColor = RGBACOLOR(255, 255, 255, 1.0);
    [self.view addSubview:self.rightBottomVew];
    self.rightBottomVew.clipsToBounds = YES;
    
    _inputTextField = [[UITextField alloc]initWithFrame:CGRectMake(70, 5, KScreenWidth-80-70, 30)];
    _inputTextField.backgroundColor = [UIColor whiteColor];
    _inputTextField.placeholder = @"我来说两句";
    _inputTextField.font = [UIFont fontWithName:@"Arial" size:15.0f];
    _inputTextField.textColor = [UIColor blackColor];
    _inputTextField.backgroundColor = [UIColor colorWithRed:246/255.0 green:237/255.0 blue:237/255.0 alpha:1];
    _inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _inputTextField.clearsOnBeginEditing = NO;
    _inputTextField.returnKeyType = UIReturnKeyDone;
    _inputTextField.keyboardType = UIKeyboardTypeDefault;
    _inputTextField.delegate = self;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(50, 5, 20, 30)];
    view.backgroundColor = [UIColor colorWithRed:246/255.0 green:237/255.0 blue:237/255.0 alpha:1];//colorWithRed:242/255.0 green:234/255.0 blue:232/255.0 alpha:1
    [self.rightBottomVew addSubview:view];
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(3, 8, 15, 15)];
    image.image = [UIImage imageNamed:@"pinglun.png"];
    [view addSubview:image];

    _inputTextField.leftViewMode = UITextFieldViewModeUnlessEditing;
    [self.rightBottomVew addSubview:_inputTextField];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0, 60, 40)];
    _btnImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 30, 30)];
    _btnImage.image = [UIImage imageNamed:@"biaoqing.png"];
    [leftBtn addSubview:_btnImage];
    [leftBtn addTarget:self action:@selector(faceBag:) forControlEvents:UIControlEventTouchUpInside];
    [self.rightBottomVew addSubview:leftBtn];
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth - 80, 0, 80, 40)];
    UIImageView *rightImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 8, 60, 25)];
    rightImage.backgroundColor = [UIColor colorWithRed:228/255.0 green:63/255.0 blue:84/255.0 alpha:1];
    [rightBtn addSubview:rightImage];
    [rightBtn setTitle:@"吐槽" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    rightBtn.titleLabel.textAlignment = 1;
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightImage.layer.cornerRadius = 12.5;
    [rightBtn addTarget:self action:@selector(TuCao:) forControlEvents:UIControlEventTouchUpInside];
    [self.rightBottomVew addSubview:rightBtn];
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1];
    [self.rightBottomVew addSubview:lineView];
}
-(void)initFaceData{
    _faceDataArr = [NSMutableArray array];
    _faceNameArr = [NSMutableArray array];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        FaceBagModel *model = [[FaceBagModel alloc]init];
        NSString *imageName = [NSString stringWithFormat:@"%d#.png",i];
        model.imageName = imageName;
        [_faceDataArr addObject:model];
    }
}
-(void)createFaceBag{
    _FaceBagView = [[UIView alloc]init];
    _FaceBagView.frame = CGRectMake(0,0, KScreenWidth, 216);
    _FaceBagView.backgroundColor = [UIColor colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0];
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-60, 188, 60, 25)];
    [deleteBtn setImage:[UIImage imageNamed:@"shanchu_2.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deletaClike) forControlEvents:UIControlEventTouchUpInside];
    [_FaceBagView addSubview:deleteBtn];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 186) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];//colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0
    _collectionView.pagingEnabled = YES;
    _collectionView.tag = 2000;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_FaceBagView addSubview:_collectionView];
    NSString *identifier = @"faceCell";
    [_collectionView registerClass:[FaceBagCell class] forCellWithReuseIdentifier:identifier];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView"];
    [_inputTextField resignFirstResponder];
}
-(void)createPageControl{
    //分页控件：宽高是系统有默认值的
    _pageC = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 195, 40, 10)];
    _pageC.center = CGPointMake(KScreenWidth/2.0,_FaceBagView.bounds.size.height -15);
    UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth/2-20, 197.5, 40, 7)];
    iv.backgroundColor = [UIColor clearColor];
    [_FaceBagView addSubview:iv];
    UIView *iv1 = [[UIView alloc]initWithFrame:CGRectMake(0.5, 0, 7, 7)];
    iv1.backgroundColor = [UIColor lightGrayColor];
    iv1.layer.masksToBounds = YES;
    iv1.layer.cornerRadius = 3.5;
    [iv addSubview:iv1];
    UIView *iv2 = [[UIView alloc]initWithFrame:CGRectMake(16.5, 0, 7, 7)];
    iv2.backgroundColor = [UIColor lightGrayColor];
    iv2.layer.masksToBounds = YES;
    iv2.layer.cornerRadius = 3.5;
    [iv addSubview:iv2];
    UIView *iv3 = [[UIView alloc]initWithFrame:CGRectMake(33, 0, 7, 7)];
    iv3.backgroundColor = [UIColor lightGrayColor];
    iv3.layer.masksToBounds = YES;
    iv3.layer.cornerRadius = 3.5;
    [iv addSubview:iv3];
    
    _pageC.numberOfPages =3;
    _pageC.backgroundColor = [UIColor clearColor];
    _pageC.layer.masksToBounds = YES;
    _pageC.layer.cornerRadius = 5;
    _pageC.enabled =NO;
    //设置当前选中点得颜色
    _pageC.currentPageIndicatorTintColor = [UIColor redColor];
    _pageC.currentPage =0;
    [_FaceBagView addSubview:_pageC];
}
-(void)deletaClike{
    BOOL isDeleteAll;
    isDeleteAll = NO;
    if (_inputTextField.text.length) {
        NSString *str = [_inputTextField.text substringFromIndex:_inputTextField.text.length-1];
        if ([str isEqualToString:@"]"]) {
            for (int i = _inputTextField.text.length-1; i>0; i --) {
                NSString *str1 = [_inputTextField.text substringWithRange:NSMakeRange(i-1, 1)];
                if ([str1 isEqualToString:@"["]) {
                    _inputTextField.text = [_inputTextField.text substringToIndex:i-1];
                    isDeleteAll = NO;
                    break;
                }
                isDeleteAll = YES;
            }
            if (isDeleteAll) {
                _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
            }
            
        }else{
            _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
        }
    }
}



#pragma mark - 点击事件
-(void)popKeyboard:(NSNotification *)notice{
    _replydict = notice.object;
    if (g_App.userID) {
        int valueType = (int)[[_replydict objectForKey:@"valueType"] integerValue];
        if (valueType == 1) {
            if ([[[_replydict objectForKey:@"userIDInfo"] objectForKey:@"id"] isEqualToString:g_App.userID]) {
                
                _inputTextField.placeholder = @"真的不来几句吗骚年 ಠ౪ಠ";
                _isReply = NO;
            } else {
                _inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", [[_replydict objectForKey:@"userIDInfo"] objectForKey:@"name"]];
                _isReply = YES;
            }

        } else {
            if ([[[_replydict objectForKey:@"replyUserIDInfo"] objectForKey:@"id"] isEqualToString:g_App.userID]) {
                
                _inputTextField.placeholder = @"真的不来几句吗骚年 ಠ౪ಠ";
                _isReply = NO;
            } else {
                _inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", [[_replydict objectForKey:@"replyUserIDInfo"] objectForKey:@"name"]];
                _isReply = YES;
            }
        }
        
        [_inputTextField becomeFirstResponder];
        return;
    } else {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        return;
    }

}
-(void)goBack:(UIButton *)btn{
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    if (_type != 100) {

        [[NSNotificationCenter defaultCenter] postNotificationName:@"changePicArr" object:self.allDataArr];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshZanedImage" object:nil];
    }
}

-(void)btnClike:(UIButton *)btn{
    int dex = self.superCollectionView.contentOffset.x/ScreenSizeWidth;
    if (btn.tag == 1000) {
        [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"is" to_section:@"i" to_step:@"r" type:@"" id:@"0"];
        NSString *title = @"我在麦萌看到一张超赞的图片，安利给小伙伴";
        NSString *url = [NSString stringWithFormat:@"%@",_allDataArr[dex][@"sourceImages"]];
        
        [UMSocialData defaultData].extConfig.qzoneData.title = title;;
        
        [UMSocialData defaultData].extConfig.qzoneData.url = url;
        
        [UMSocialData defaultData].extConfig.qqData.title = title;;
        [UMSocialData defaultData].extConfig.qqData.url = url;
        
        
        [UMSocialData defaultData].extConfig.wechatTimelineData.title = title;;
        [UMSocialData defaultData].extConfig.wechatTimelineData.url = url;
        
        
        [UMSocialData defaultData].extConfig.wechatSessionData.title = title;;
        [UMSocialData defaultData].extConfig.wechatSessionData.url = url;
        
        
        [UMSocialData defaultData].extConfig.sinaData.shareText = [NSString stringWithFormat:@"%@%@分享自@麦萌",title,url];
        
        [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:url];
        
        [UMSocialSnsService presentSnsIconSheetView:self
                                             appKey:UMENGKEY
                                          shareText:url
                                         shareImage:[UIImage imageNamed:@"icon.png"]
                                    shareToSnsNames:@[UMShareToSina, UMShareToQQ, UMShareToQzone, UMShareToWechatSession,UMShareToWechatTimeline]
                                           delegate:self];
        NSString *imageId = _allDataArr[dex][@"id"];
        
        NSDictionary *dict = @{@"shareId" :imageId};
        [MobClick event:@"shareEvent" attributes:dict];
    }else if (btn.tag == 1001){
        
        [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"idl" to_section:@"i" to_step:@"a" type:@"" id:@"0"];
        UIImage *image1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_allDataArr[dex][@"sourceImages"]]]]];
        UIImageWriteToSavedPhotosAlbum(image1, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }else if (btn.tag == 1002){
        if (!g_App.userID) {
            [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"nl" to_section:@"i" to_step:@"r" type:@"c" id:@"0"];
            LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
            [alertView show];
            return;
        }
        
        [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"ic" to_section:@"i" to_step:@"a" type:@"c" id:@"0"];
        
        
        _yaoyiyao = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.bottomView.frame = CGRectMake(0,  ScreenSizeHeight, ScreenSizeWidth, 64);
            self.rightBottomVew.frame = CGRectMake(0,  ScreenSizeHeight-44, ScreenSizeWidth, 44);
        }];
        [_inputTextField becomeFirstResponder];
    }else if (btn.tag == 1003){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"" message:@"确定举报？" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
        alertView.tag = 1005;
        [alertView show];

        
        
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:@{@"r":API_URL_REPORT,@"type":@"0",@"valueType":@"1",@"valueID":_cartoonId,@"content":@"0"} object:self action:@selector(reportMsg:) method:POSTDATA];
    }
}


- (void)reportMsg:(NSDictionary *)dict {
    [MBProgressHUD showSuccess:@"举报成功" toView:self.view];
}



-(void)didSelectSocialPlatform:(NSString *)platformName withSocialData:(UMSocialData *)socialData{
    [[LogHelper shared] writeToFilefrom_page:@"is" from_section:@"i" from_step:@"d" to_page:@"is" to_section:@"i" to_step:@"a" type:platformName id:@"0"];
}


- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo{
    if (error != NULL){
        //失败
        [MBProgressHUD showSuccess:@"保存失败" toView:self.view];
    }else{
        //成功
        [MBProgressHUD showSuccess:@"保存成功" toView:self.view];
    }
}


-(void)TuCao:(UIButton *)btn{
    if (!_inputTextField.text.length) {
        [_inputTextField resignFirstResponder];
        self.touchBgView.hidden = YES;
        return;
    }
    if (_isReply) {
        [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"ir" to_section:@"i" to_step:@"a" type:@"" id:@"0"];
        [self replyMessage:_inputTextField.text];
    } else{
        [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"ic" to_section:@"i" to_step:@"a" type:@"" id:@"0"];
        [self commentMessage:_inputTextField.text];
    }
    
    [_inputTextField resignFirstResponder];
    self.touchBgView.hidden = YES;
    _inputTextField.text = @"";
    return;
}

- (void)touchBgViewTouchInView {
    //    self.inputTextField.text = nil;
    if ([_inputTextField.text isEqualToString:@""]) {
        _inputTextField.placeholder = @"我来说两句";
    }

    self.touchBgView.hidden = YES;
    [_inputTextField resignFirstResponder];
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    //kbSize即為鍵盤尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    _keyboardHeight = kbSize.height;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        if (_yaoyiyao) {
            self.isOrNoShowShake = NO;
            [UIView animateWithDuration:0.3 animations:^{
                self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight-self.feedBackView.bounds.size.height-kbSize.height, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
            }];
        }else{
            [UIView animateWithDuration:0.3 animations:^{
                self.rightBottomVew.frame = CGRectMake(0, self.view.bounds.size.height - self.rightBottomVew.frame.size.height - keyboardBounds.size.height, self.rightBottomVew.frame.size.width, self.rightBottomVew.frame.size.height);
                self.bottomView.frame = CGRectMake(0, ScreenSizeHeight, ScreenSizeWidth, 64);
            }];
        }
        
        

        //        [self performSelector:@selector(lag1) withObject:nil afterDelay:0.3];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification *)notification
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        if (_yaoyiyao) {
            self.isOrNoShowShake = YES;
            [UIView animateWithDuration:0.3 animations:^{
                self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
            }];
        }else{
            [UIView animateWithDuration:0.3 animations:^{
                self.rightBottomVew.frame = CGRectMake(0, self.view.bounds.size.height, self.rightBottomVew.frame.size.width, self.rightBottomVew.frame.size.height);
                self.bottomView.frame = CGRectMake(0, ScreenSizeHeight-64, ScreenSizeWidth, 64);
            }];
        }
        
        _yaoyiyao = YES;
        

        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}
-(void)faceBag:(UIButton *)btn{
    if (_isFace) {
        _btnImage.image = [UIImage imageNamed:@"jianpan.png"];
//        [btn setImage:[UIImage imageNamed:@"jianpan.png"] forState:UIControlStateNormal];
        _inputTextField.inputView = _FaceBagView;
        [_inputTextField becomeFirstResponder];
        [_inputTextField reloadInputViews];
        _isFace = NO;
    }else{
        _btnImage.image = [UIImage imageNamed:@"biaoqing.png"];
//        [btn setImage:[UIImage imageNamed:@"biaoqing.png"] forState:UIControlStateNormal];
        [_inputTextField becomeFirstResponder];
        _inputTextField.inputView = nil;
        [_inputTextField reloadInputViews];
        _isFace = YES;
    }
}

#pragma mark - UITextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    self.touchBgView.hidden = NO;
    [self.view bringSubviewToFront:self.touchBgView];
    [self.view bringSubviewToFront:self.rightBottomVew];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (!_inputTextField.text.length) {
        [_inputTextField resignFirstResponder];
        self.touchBgView.hidden = YES;
        return YES;
    }
    if (_isReply) {
        [self replyMessage:_inputTextField.text];
    } else{
        [self commentMessage:_inputTextField.text];
    }
    
    [_inputTextField resignFirstResponder];
    self.touchBgView.hidden = YES;
    _inputTextField.text = @"";
    return YES;
}
#pragma mark - UICollectionDelegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView.tag == 1000) {
        return _allDataArr.count;
    }else{
        return _faceDataArr.count;
    }
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 1000) {
        supePictureCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_superCellIdentifier forIndexPath:indexPath];
        cell.dataDic = _allDataArr[indexPath.row];
        cell.type = @"10";
//        cell.boolLoadData = [NSString stringWithFormat:@"%li",[indexPath row]];
        return cell;
    }else{
        static NSString *identify = @"faceCell";
        FaceBagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
        cell.model = _faceDataArr[indexPath.row];
        return cell;
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 1000) {
        
    }else{
        NSInteger row = [indexPath row];
        _inputTextField.text = [NSString stringWithFormat:@"%@[%@]",_inputTextField.text,_faceNameArr[row]];
    }
}


#pragma mark - UICollectionViewFlowLayoutDelegate

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 1000) {
         return CGSizeMake(KScreenWidth, KScreenheight-64);
    }else{
        return CGSizeMake((KScreenWidth-75)/7.0, 180/5.0);
    }
   
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (collectionView.tag == 1000) {
         return UIEdgeInsetsMake(0, 0, 0, 0);
    }else{
        return UIEdgeInsetsMake(0, 10, 5, 5);
    }
   
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"escTop" object:nil];
    int dex = self.superCollectionView.contentOffset.x/ScreenSizeWidth;
    _cartoonId = [NSString stringWithFormat:@"%@",_allDataArr[dex][@"id"]];
}
#pragma mark - photorowser
////临时占位图（thumbnail图
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}
////高清原图 （bmiddle图）
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[_dataSource objectAtIndex:index]];
}
//
- (NSString*)photoDrowserWithIndex:(NSInteger)index {
    return [[self.allDataArr objectAtIndex:index] objectForKey:@"id"];
}


#pragma mark - 加载数据
- (void)commentMessage:(NSString*)msg {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_COMMENT_MESSAGE,@"r",
                                    @"3",@"type",
                                    _cartoonId,@"valueID",
                                    msg,@"content",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(commentMessageFinish:) method:POSTDATA];
}

- (void)commentMessageFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        int dex = self.superCollectionView.contentOffset.x/ScreenSizeWidth;
        supePictureCell *cell = self.superCollectionView.visibleCells[0];
        cell.type = @"20";
        cell.dataDic = _allDataArr[dex];
    }
}

- (void)replyMessage:(NSString*)msg {
    _isReply = NO;
    _inputTextField.placeholder = @"真的不来几句吗骚年 ಠ౪ಠ";
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_REPLY_MESSAGE,@"r",
                                    [NSString stringWithFormat:@"%@",_replydict[@"id"]],@"contentID",
                                    msg,@"content",
                                    @"3",@"type",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(replyMessageFinish:) method:POSTDATA];
}

- (void)replyMessageFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        int dex = self.superCollectionView.contentOffset.x/ScreenSizeWidth;
        supePictureCell *cell = self.superCollectionView.visibleCells[0];
        cell.type = @"20";
        cell.dataDic = _allDataArr[dex];
    }
}

-(void)changePicArr:(NSNotification *)noti{
        int dex = self.superCollectionView.contentOffset.x/ScreenSizeWidth;
        NSMutableDictionary *dict = noti.object;
        [self.allDataArr replaceObjectAtIndex:dex withObject:dict];

    
}

-(void)setAllDataArr:(NSMutableArray *)allDataArr{
    if (_type == 100) {
        NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        @"prettyImages/detail",@"r",
                                        _pictureId,@"id",
                                        @"1",@"withPraise",
                                        @"1",@"withLabel",nil];
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadPictureData:) method:GETDATA];
    }else{
        _allDataArr = allDataArr;
        _cartoonId = [NSString stringWithFormat:@"%@",_allDataArr[_initNum][@"id"]];
    }
    
}

- (void)loadPictureData:(NSDictionary*)dic {
    _allDataArr = [NSMutableArray array];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [_allDataArr addObject:dic[@"results"]];
        //        [_allDataArr addObject:dic[@"results"]];
        _cartoonId = [NSString stringWithFormat:@"%@",_allDataArr[0][@"id"]];
        [self.superCollectionView reloadData];
    }
}

-(void)setType:(int)type{
    _type = type;
}

-(void)setPictureId:(NSString *)pictureId{
    _pictureId = pictureId;
}

#pragma mark - 不允许横屏

-(BOOL)shouldAutorotate{
    return NO;
}


-(BOOL)canBecomeFirstResponder{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
