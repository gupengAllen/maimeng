//
//  EditViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "EditViewController.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface EditViewController ()

@end

@implementation EditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.bounds = CGRectMake(0, 0, 60, 25);
//    [btn setTitle:@"保存" forState:UIControlStateNormal];
//    [btn setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    [btn setImage:[UIImage imageNamed:@"baocun.png"] forState:UIControlStateNormal];
//    btn.titleLabel.textColor = [UIColor whiteColor];
//    btn.backgroundColor = [UIColor colorWithRed:223/255.0 green:63/255.0 blue:84/255.0 alpha:0.7];
//    btn.layer.cornerRadius = 12.5;
//    btn.layer.masksToBounds = YES;
//    btn.layer.borderWidth = 1;
//    btn.layer.borderColor = [UIColor whiteColor].CGColor;
//    [btn setBackgroundImage:[UIImage imageNamed:@"baocun.png"] forState:UIControlStateNormal];
    [self addRightItemWithButton:btn itemTarget:self action:@selector(rightBtnPressed:)];
//    [self addRightItemWithImage:[UIImage imageNamed:@"baocun.png"] itemTarget:self action:@selector(rightBtnPressed:)];
    
    self.inputTextField.showBolder = NO;
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)rightBtnPressed:(UIButton*)btn {
    [self.inputTextField resignFirstResponder];
    [self.inputTextView resignFirstResponder];
    [self userUpdate];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initTitleName:self.titleName];
    if (self.type == 0) {
        self.contentBg.frame = CGRectMake(self.contentBg.frame.origin.x, self.contentBg.frame.origin.y, self.contentBg.bounds.size.width, self.inputTextField.frame.origin.y + self.inputTextField.frame.size.height + 5);
        self.inputTextView.hidden = YES;
        self.inputTextField.text = self.nickName;
        [self.inputTextField becomeFirstResponder];
    } else {
        self.inputTextField.hidden = YES;
        self.inputTextView.text = self.sign;
        
        [self.inputTextField addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
        
        self.contentBg.frame = CGRectMake(self.contentBg.frame.origin.x, self.contentBg.frame.origin.y, self.contentBg.bounds.size.width, self.inputTextView.frame.origin.y + self.inputTextView.frame.size.height + 5);
        [self.inputTextView becomeFirstResponder];
    }
}

- (IBAction)touchInView:(id)sender {
    
    [self.inputTextField resignFirstResponder];
    [self.inputTextView resignFirstResponder];
}

#pragma mark - UITextViewDelegate
- (void)textViewDidChange:(UITextView *)textView {
    if (textView.text.length > 40) {
        textView.text = [textView.text substringToIndex:40];
    } else {
        
    }
}

- (void)textFieldChanged:(UITextField *)text {
    NSString *text1 = text.text;
    if (text1.length < 15) {
        self.inputTextField.enabled = YES;
    }else {
       self.inputTextField.enabled = NO;
    }
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"---");
    if (textField.text.length>20) {
        textField.text = [textField.text substringToIndex:20];
        return NO;
    }else{
        return YES;
    }
}
- (void)userUpdate {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_USER_UPDATE,@"r",
                                    g_App.userID,@"id",nil];
    
    if (self.type == 0) {
        [exprame setObject:self.inputTextField.text forKey:@"name"];
    }
    if (self.type == 1) {
        [exprame setObject:self.inputTextView.text forKey:@"signature"];
    }
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(userUpdateFinish:) method:POSTDATA];
}

- (void)userUpdateFinish:(NSDictionary*)dic {
    [[LogHelper shared] writeToFilefrom_page:@"psis" from_section:@"profile" from_step:@"home" to_page:@"psis" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];

    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (![[dic objectForKey:@"code"] integerValue]) {
        
        [self goBackToPreview:nil];
    }
}
@end
