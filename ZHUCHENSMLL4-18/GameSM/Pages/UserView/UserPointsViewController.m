//
//  UserPointsViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "UserPointsViewController.h"
#import "LYTabBarController.h"
#import "PointsHelpViewController.h"

@interface UserPointsViewController ()
{
    LYTabBarController *_lyTabbar;
}
@end

@implementation UserPointsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackBtn];
    [self initTitleName:@"我的积分"];
    [self createUI];
}

-(void)createUI{
    NSArray *nameArr = @[@"礼包",@"手办",@"周边",@"门票"];
    for (int i = 0 ; i < 4; i ++) {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(10+(10+(ScreenSizeWidth-50)/4.0)*i,ScreenSizeWidth/32*25+ 40,
                               (ScreenSizeWidth-50)/4.0, (ScreenSizeWidth-50)/4.0);
        btn.backgroundColor = [UIColor blackColor];
        [btn setImage:[UIImage imageNamed:@"sousuo_3"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnCliked:) forControlEvents:UIControlEventTouchUpInside];
        btn.tag = 1000 + i;
        [self.view addSubview:btn];
        
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10+(10+(ScreenSizeWidth-50)/4.0)*i, ScreenSizeWidth/32*25+ 40+5+(ScreenSizeWidth-50)/4.0, (ScreenSizeWidth-50)/4.0, 30)];
        label.text = nameArr[i];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [UIFont fontWithName:@"Helvetica" size:20];
        label.tag = 1001+i;
        [self.view addSubview:label];
    }
}
-(void)btnCliked:(UIButton *)btn{
//    exit(0);//退出APP
    [MBProgressHUD showError:@"期待吧 渣渣们" toView:self.view];
}
-(void)viewWillAppear:(BOOL)animated{
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [(LYTabBarController*)self.tabBarController hiddenBar:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)helpBtnHit:(id)sender {
    
    [[LogHelper shared] writeToFilefrom_page:@"pi" from_section:@"profile" from_step:@"home" to_page:@"gl" to_section:@"gift" to_step:@"l" type:@"" id:@"0"];
    PointsHelpViewController *infoView = [[PointsHelpViewController alloc] init];
    [self.navigationController pushViewController:infoView animated:YES];
    
}
@end
