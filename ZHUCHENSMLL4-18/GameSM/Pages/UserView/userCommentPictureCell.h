//
//  userCommentPictureCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface userCommentPictureCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *commentPicture;

+ (id)userInfoCellOwner:(id)owner;
@end
