//
//  supePictureCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/16.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "supePictureCell.h"
#import "Config.h"
#import "CommentCell.h"
#import "UIImageView+WebCache.h"
#import "UIView+Addtion.h"
#import "HZPhotoBrowser.h"
#import "Config.h"
#import "MBProgressHUD+Add.h"
#import "Y_X_DataInterface.h"
#import "LoginAlertView.h"
#import "UIView+Addtion.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"

#define TMARGIN 5
#define COMMENTTITLECOLOR [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1]
@interface supePictureCell ()<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,LoginAlertViewDelegate>
{
    NSString *_cellIdentifier;
    NSString *_picCellIdentifier;
    UIImageView *_bgImageView;
    BOOL            _showAllContent;
    int             _cellNum;
    NSMutableArray *_faceNameArr;
    NSMutableArray *_loadNum;
    int             _deleteRow;
    NSString        *_deleteId;
    int _countNum;
    UILabel *_imageView;
}
@end

@implementation supePictureCell

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"escTop" object:nil];
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    _showAllContent = NO;
    _cellNum = -1;
    _countNum = 0;
    _faceNameArr = [NSMutableArray array];
    self.dataArray = [NSMutableArray array];
    _collectionArr = [NSMutableArray array];
    _loadNum = [NSMutableArray array];
    _picCollectionArr = [NSMutableArray array];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(escTop) name:@"escTop" object:nil];
        [self initFaceData];
        [self createTableView];
        [self createUI];
        [self createCollectionView];
        [self createPicCollectionView];
    }
    return self;
}
-(void)initFaceData{
    _faceNameArr = [NSMutableArray array];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
    }
}
-(void)escTop{
    self.tableView.contentOffset = CGPointMake(0, 0);
}
-(void)createTableView{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight-64)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.clipsToBounds = NO;
    [self.contentView addSubview:self.tableView];
}
-(void)createUI{

    self.scrollView = [[secondScrollView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeWidth + 160)];
    self.scrollView.backgroundColor = RGBACOLOR(255, 255, 255, 0.8);
    self.scrollView.clipsToBounds = NO;
    
    self.upPicture = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeWidth)];
    self.upPicture.contentMode = UIViewContentModeScaleAspectFill;
    self.upPicture.backgroundColor = RGBACOLOR(240, 240, 240, 1.0);
    self.upPicture.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(goBig:)];
    tap.numberOfTapsRequired = 1;
    [self.upPicture addGestureRecognizer:tap];
    [self.scrollView addSubview:self.upPicture];
        
    self.userImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, self.upPicture.bounds.size.height+20, 40, 40)];
    self.userImage.layer.masksToBounds = YES;
    self.userImage.layer.cornerRadius = self.userImage.bounds.size.height/2.0;
    [self.scrollView addSubview:self.userImage];
    
    self.userName = [[UILabel alloc]initWithFrame:CGRectMake(65, self.upPicture.bounds.size.height+20, 100, 20)];
    self.userName.font = [UIFont systemFontOfSize:16.0];
    self.userName.textColor = RGBACOLOR(145, 145, 145, 1.0);
    [self.scrollView addSubview:self.userName];
    
    self.upTime = [[UILabel alloc]initWithFrame:CGRectMake(65, self.upPicture.bounds.size.height+20+26, 100, 14)];
    self.upTime.font = [UIFont systemFontOfSize:12.0];
    self.upTime.textColor = RGBACOLOR(145, 145, 145, 1.0);
    [self.scrollView addSubview:self.upTime];
    
    self.loveImageBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth - 25 - 80, self.upPicture.bounds.size.height+25, 20, 20)];
    [self.loveImageBtn setImage:[UIImage imageNamed:@"btn_aixing"] forState:UIControlStateNormal];
    [self.loveImageBtn addTarget:self action:@selector(loveImageBtnClike:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:self.loveImageBtn];
    
    self.sanJiaoImage = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenSizeWidth - 25 - 80, self.upPicture.bounds.size.height+25+25, 20, 10)];
//    self.sanJiaoImage.hidden = YES;
    self.sanJiaoImage.image = [UIImage imageNamed:@"xiaosanjiao"];
    [self.scrollView addSubview:self.sanJiaoImage];
    
    self.praiseNum = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth - 80, self.upPicture.bounds.size.height+27, 70, 14)];
    self.praiseNum.font = [UIFont systemFontOfSize:13.0];
    self.praiseNum.textColor = RGBACOLOR(145, 145, 145, 1.0);
    self.praiseNum.textAlignment = 1;
    [self.scrollView addSubview:self.praiseNum];
    
    _tittttlabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, ScreenSizeWidth, 20)];
    _tittttlabel.text = @"o(￣▽￣)o还没有人点赞，快来点个赞吧~";
    _tittttlabel.backgroundColor = RGBACOLOR(243, 243, 243, 1.0);
    _tittttlabel.textAlignment = 1;
    _tittttlabel.tag = 1000;
    _tittttlabel.textColor = RGBACOLOR(180, 180, 180, 1.0);
    [self.scrollView addSubview:_tittttlabel];
//    self.lineImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, self.upPicture.bounds.size.height+80, ScreenSizeWidth-40, 1)];
//    [self.scrollView addSubview:self.lineImage];
    
//    self.otherOne = [[UILabel alloc]initWithFrame:CGRectMake(10, self.upPicture.bounds.size.height+80, ScreenSizeWidth-40, 20)];
//    self.otherOne.font = [UIFont systemFontOfSize:14];
//    self.otherOne.textColor = RGBACOLOR(145, 145, 145, 1.0);
//    self.otherOne.text = @"Ta赞了";
//    [self.scrollView addSubview:self.otherOne];
}

-(void)createCollectionView{
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumInteritemSpacing = 7.5;
    flowLayout.minimumLineSpacing = 7.5;
    
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, self.upPicture.bounds.size.height + 110, ScreenSizeWidth, 50) collectionViewLayout:flowLayout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.tag = 100;
    _cellIdentifier = @"cellIdentifier";
    self.collectionView.backgroundColor = RGBACOLOR(243, 243, 243, 1.0);
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:_cellIdentifier];
    [self.scrollView addSubview:self.collectionView];
     [self.scrollView bringSubviewToFront:_tittttlabel];
}
-(void)createPicCollectionView{
    self.picLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.upPicture.bounds.size.height+160, ScreenSizeWidth, 20)];
    self.picLabel.text = @"相关美图";
    self.picLabel.textColor = RGBACOLOR(145, 145, 145, 1.0);
    self.picLabel.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:self.picLabel];
    
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    flowLayout.minimumInteritemSpacing = 7.5;
    flowLayout.minimumLineSpacing = 7.5;
    self.picCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, self.upPicture.bounds.size.height+180, ScreenSizeWidth, 80) collectionViewLayout:flowLayout];
    self.picCollectionView.showsHorizontalScrollIndicator = NO;
    self.picCollectionView.delegate = self;
    self.picCollectionView.dataSource = self;
    self.picCollectionView.tag = 200;
    _picCellIdentifier = @"picCellIdentifier";
    self.picCollectionView.backgroundColor = RGBACOLOR(255, 255, 255, 1.0);
    [self.picCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:_picCellIdentifier];
    [self.scrollView addSubview:self.picCollectionView];
    
    self.tagLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.upPicture.bounds.size.height+180+80, ScreenSizeWidth, 20)];
    self.tagLabel.text = @"标签";
    self.tagLabel.textColor = RGBACOLOR(145, 145, 145, 1.0);
    self.tagLabel.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:self.tagLabel];
    
    self.tagBtnView = [[UIView alloc]initWithFrame:CGRectMake(0, self.upPicture.bounds.size.height+180+80+20, ScreenSizeWidth, 70)];
    [self.scrollView addSubview:self.tagBtnView];
    
    self.nCommentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.upPicture.bounds.size.height+180+80+20+70, ScreenSizeWidth,20)];
    self.nCommentLabel.text = @"最新评论";
    self.nCommentLabel.textColor = RGBACOLOR(145, 145, 145, 1.0);
    self.nCommentLabel.font = [UIFont systemFontOfSize:14];
    [self.scrollView addSubview:self.nCommentLabel];
    
}
#pragma mark - UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataArray.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  CommentCell*  cell = [CommentCell commentCellOwner:self];
    cell.userImageView.layer.cornerRadius = 18;
    cell.userImageView.layer.masksToBounds = YES;
    if([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]){
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    if (![self.dataArray count]) {
        return cell;
    }
    //楼层
    cell.floorNum.text = [NSString stringWithFormat:@"%ld楼",(long)[indexPath row]+1];
    cell.floorNum.textColor = RGBACOLOR(154, 205, 208, 1.0);
    
    int valueType = (int)[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"valueType"] integerValue];
    if (valueType == 1) {
        cell.userNameLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"];
        cell.userNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];//colorWithRed:192/255.0 green:126/255.0 blue:126/255.0 alpha:1.0
        [cell.userImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
        cell.userTimeLabel.text = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"];
    } else {
        cell.userNameLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"name"];
        cell.userNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];
        [cell.userImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"回复：%@ %@",[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"],[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"]]];
        
        if ([[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"] length] > 0) {
            [str addAttribute:NSForegroundColorAttributeName value:COMMENTTITLECOLOR range:NSMakeRange(3, [[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"] length])];
        }
        [cell.userTimeLabel setAttributedText:str];
    }
    
    NSString *cententString = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"content"];
    //创建显示所有内容的按钮
    UIButton *showAllBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-100, cell.height - 50, 150, 50)];
    showAllBtn.backgroundColor = [UIColor clearColor];
    [showAllBtn setTitleColor:RGBACOLOR(203, 203, 203, 1.0) forState:UIControlStateNormal];
    showAllBtn.titleLabel.font = [UIFont systemFontOfSize:13];
    [showAllBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 50)];
    showAllBtn.tag = (int)[indexPath row];
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(ScreenSizeWidth-100, cell.height - 40, 100, 40)];
    if (cententString.length>130) {
        showAllBtn.hidden = NO;
        image.hidden = NO;
        if (_cellNum == (int)[indexPath row]) {
            if (!_showAllContent) {
                cententString = [cententString substringToIndex:100];
                cententString = [cententString stringByAppendingString:@"\n......\n"];
            }else{
                cententString = [cententString stringByAppendingString:@"\n"];
            }
        }else{
            cententString = [cententString substringToIndex:100];
            cententString = [cententString stringByAppendingString:@"\n......\n"];
        }
    }else{
        showAllBtn.hidden = YES;
        image.hidden = YES;
    }
    UITextView *labelFu = [[UITextView alloc]init];
    labelFu.tag = (int)[indexPath row];
    labelFu.font = [UIFont boldSystemFontOfSize:16];
    labelFu.userInteractionEnabled = NO;
    labelFu.bounces = NO;
    labelFu.editable = NO;
    [cell.contentView addSubview:labelFu];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 8;// 字体的行间距
    NSDictionary *attributes = @{
                                 NSFontAttributeName:[UIFont systemFontOfSize:16],
                                 NSParagraphStyleAttributeName:paragraphStyle
                                 };
    //#warning 正则表达式 "\\[[^\\]]+\\]"--"\\#[^\\#]+\\#"
    NSString *regexString = @"\\[[^\\]]+\\]";
    NSMutableAttributedString *attri = [labelFu.attributedText mutableCopy];//_model.content
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
    NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
    if (arrr.count>0) {
        for (int i =0 ; i < arrr.count; i ++) {
            NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
            NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
            NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
            [attri appendAttributedString:attrStr1];
            NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
            for (int j = 0; j < _faceNameArr.count; j ++) {
                if ([faceStr isEqualToString:_faceNameArr[j]]) {
                    NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                    attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                    attch.bounds = CGRectMake(0, -2, 30, 30);
                    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                    [attri appendAttributedString:string1];
                    break;
                }
            }
            cententString = [cententString substringFromIndex:result.range.location+result.range.length];
        }
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
        [attri appendAttributedString:attrStr2];
        [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
        labelFu.attributedText = [attri copy];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attri boundingRectWithSize:CGSizeMake(ScreenSizeWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
        labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, ScreenSizeWidth - 2*TMARGIN-50-30, rect.size.height+15);
    }else{
        NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
        NSRange allRange = [cententString rangeOfString:cententString];
        [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
        [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(ScreenSizeWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
        labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
        int height = (int)rect.size.height/19;
        labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, ScreenSizeWidth - 2*TMARGIN-40-30, height*27+7);
    }
    cell.height = TMARGIN +32+labelFu.frame.size.height+6;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    showAllBtn.frame = CGRectMake(TMARGIN+17, cell.height - 26, 150, 40);
    image.frame = CGRectMake(showAllBtn.right-62,cell.height - 10, 12, 8);
    if (_cellNum == (int)[indexPath row]) {
        if (!_showAllContent) {
            [showAllBtn setTitle:@"展开全部" forState:UIControlStateNormal];
            image.image = [UIImage imageNamed:@"down"];
            showAllBtn.selected = YES;
        }else{
            showAllBtn.selected = NO;
            [showAllBtn setTitle:@"收起全部" forState:UIControlStateNormal];
            image.image = [UIImage imageNamed:@"up"];
        }
    }else{
        [showAllBtn setTitle:@"展开全部" forState:UIControlStateNormal];
        image.image = [UIImage imageNamed:@"down"];
        showAllBtn.selected = YES;
    }
    [showAllBtn addTarget:self action:@selector(showAllContentComment:) forControlEvents:UIControlEventTouchUpInside];
    [cell.contentView addSubview:showAllBtn];
    [cell.contentView addSubview:image];
    
    
    cell.replyBtn.frame = CGRectMake(cell.frame.size.width - 60, 15, 60, 20);//cell.frame.size.height/2-20
    [cell.replyBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 35)];
    cell.replyNameLabel.frame = CGRectMake(cell.frame.size.width - 30, 13.5, 30, 20);
    cell.replyNameLabel.textColor = RGBACOLOR(203, 203, 203, 1.0);
    [cell.contentView addSubview:cell.replyNameLabel];
    if ([cell.userNameLabel.text isEqualToString:g_App.userName]) {
        [cell.replyBtn setImage:[UIImage imageNamed:@"shanchu_3"] forState:UIControlStateNormal];
        cell.replyNameLabel.frame = CGRectMake(cell.frame.size.width - 30, 15, 30, 20);
        cell.replyNameLabel.text = @"删除";
        [cell.replyBtn addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
        cell.replyBtn.tag = [indexPath row];
    }else{
        [cell.replyBtn setImage:[UIImage imageNamed:@"huifu"] forState:UIControlStateNormal];
        cell.replyNameLabel.text = @"回复";
        
        [cell.replyBtn addTarget:self action:@selector(replyBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        cell.replyBtn.tag = indexPath.row + 100;
    }
    
    UIImageView *linevv = [[UIImageView alloc]initWithFrame:CGRectMake(0, -1, ScreenSizeWidth, 1)];
    linevv.backgroundColor = [UIColor whiteColor];
    [cell.contentView addSubview:linevv];
    
    return cell;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     NSLog(@"--%li",(long)indexPath.row);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cententString = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"content"];
    if (cententString.length>130) {
        if (_cellNum == (int)[indexPath row]) {
            if (!_showAllContent) {
                cententString = [cententString substringToIndex:100];
                cententString = [cententString stringByAppendingString:@"\n......\n"];
            }else{
                cententString = [cententString stringByAppendingString:@"\n"];
            }
        }else{
            cententString = [cententString substringToIndex:100];
            cententString = [cententString stringByAppendingString:@"\n......\n"];
        }
    }
    NSString *regexString = @"\\[[^\\]]+\\]";
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc]initWithString:@""];//_model.content
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
    NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
    if (arrr.count>0) {
        for (int i =0 ; i < arrr.count; i ++) {
            NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
            NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
            NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
            [attri appendAttributedString:attrStr1];
            NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
            for (int j = 0; j < _faceNameArr.count; j ++) {
                if ([faceStr isEqualToString:_faceNameArr[j]]) {
                    NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                    attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                    attch.bounds = CGRectMake(0, -2, 30, 30);
                    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                    [attri appendAttributedString:string1];
                    break;
                }
            }
            cententString = [cententString substringFromIndex:result.range.location+result.range.length];
        }
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
        [attri appendAttributedString:attrStr2];
        [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attri boundingRectWithSize:CGSizeMake(ScreenSizeWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
        return TMARGIN +32+rect.size.height+15+6+5;
    }else{
        NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
        NSRange allRange = [cententString rangeOfString:cententString];
        [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
        [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(ScreenSizeWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
        int height = (int)rect.size.height/19;
        return TMARGIN +32+height*27+15+5;
    }

}


#pragma mark - UICollectionDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView.tag == 100) {
        if (_collectionArr.count == 0) {
//            self.sanJiaoImage.hidden = NO;
            return 1;
        }
//        self.sanJiaoImage.hidden = YES;
        return  _collectionArr.count;
    }else if (collectionView.tag == 200){
        return _picCollectionArr.count;
    }else{
        return 0;
    }
    
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 100) {
        if (_collectionArr.count == 0) {
            self.collectionView.hidden = YES;
            _tittttlabel.hidden = NO;
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_cellIdentifier forIndexPath:indexPath];
//            [cell.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//            _imageView.hidden = NO;
//            _imageView = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, ScreenSizeWidth, 20)];
//            _imageView.text = @"o(￣▽￣)o还没有人点赞，快来点个赞吧~";
//            _imageView.textAlignment = 1;
//            _imageView.tag = 1000;
//            _imageView.textColor = RGBACOLOR(180, 180, 180, 1.0);
//            [cell.contentView addSubview:_imageView];
            return cell;
        }else{
            self.collectionView.hidden = NO;
            _tittttlabel.hidden = YES;
            UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_cellIdentifier forIndexPath:indexPath];
            UIImageView *image = (UIImageView *)[cell.contentView viewWithTag:1000];
            [image removeFromSuperview];
            UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
            imageView.layer.masksToBounds = YES;
            imageView.layer.cornerRadius = imageView.bounds.size.height/2;
            [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_collectionArr[indexPath.row][@"userIDInfo"][@"images"]]]];
            [cell.contentView addSubview:imageView];
            return cell;
        }
    }else if (collectionView.tag == 200){
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_picCellIdentifier forIndexPath:indexPath];
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 80)];
        imageView.contentMode=UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds=YES;
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_picCollectionArr[indexPath.row][@"smallImages"]]]];
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = 5;
        [cell.contentView addSubview:imageView];
        return cell;
    }else{
        return nil;
    }
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 200) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"goOneBigPicture" object:_picCollectionArr[indexPath.row]];
    }
}

#pragma mark - UICollectionViewFlowLayoutDelegate

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView.tag == 100) {
        return CGSizeMake(35, 35);
    }else{
        return CGSizeMake(80, 80);
    }
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (collectionView.tag == 100) {
        return UIEdgeInsetsMake(7.5, 7.5, 7.5, 7.5);
    }else{
        return UIEdgeInsetsMake(0, 7.5, 0, 7.5);
    }
    
}
#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.tableView.contentOffset.y <= 0) {
        CGRect frame = self.upPicture.frame;
        frame.origin.y = self.tableView.contentOffset.y;
        frame.size.height = ScreenSizeWidth*[_dataDic[@"height"] floatValue]/[_dataDic[@"width"] floatValue] - self.tableView.contentOffset.y;
        self.upPicture.frame = frame;
    }
}
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self.viewController presentViewController:nav animated:YES completion:nil];
    }
}
#pragma mark - 点击事件
-(void)loveImageBtnClike:(UIButton *)btn{
    if (!g_App.userID) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以点赞哦~"];
        [alertView show];
        return;
    }
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_PRETTYIMAGESPRAISE,@"r",
                                    [NSString stringWithFormat:@"%@",_dataDic[@"id"]],@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loveImageBtnClikeFinish:) method:POSTDATA];
}
- (void)loveImageBtnClikeFinish:(NSDictionary*)dic {
    
    int praiseCount = [[NSString stringWithFormat:@"%@",self.praiseNum.text] intValue];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithDictionary:_dataDic];
        if (![dic[@"results"] isEqualToString:@"取消赞"]) {
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"a" type:@"insert" id:_dataDic[@"id"]];

            praiseCount ++;
            [MBProgressHUD showSuccess:@"已点赞" toView:self];
            [self.loveImageBtn setImage:[UIImage imageNamed:@"btn_aixing_2"] forState:UIControlStateNormal];
//            self.praiseNum.text = [NSString stringWithFormat:@"%i",praiseCount];
            
            [dict setObject:@"1" forKey:@"isPraise"];
            [dict setObject:[NSString stringWithFormat:@"%i",praiseCount] forKey:@"praiseCount"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changePicArr1" object:dict];
        }else{
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"a" type:@"cancel" id:_dataDic[@"id"]];
            
            praiseCount --;
            [MBProgressHUD showSuccess:@"取消赞" toView:self];
            [self.loveImageBtn setImage:[UIImage imageNamed:@"btn_aixing"] forState:UIControlStateNormal];
//             self.praiseNum.text = [NSString stringWithFormat:@"%i",praiseCount];
            [dict setObject:@"0" forKey:@"isPraise"];
            [dict setObject:[NSString stringWithFormat:@"%i",praiseCount] forKey:@"praiseCount"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"changePicArr1" object:dict];
        }
        [self loadPictureAllData];
//        [self.collectionView reloadData];
//        [self.tableView reloadData];
//        self.tableView.tableHeaderView = self.scrollView;
    }
}

-(void)showAlert:(UIButton *)sender{
    _deleteRow = sender.tag;
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"您是否确定删除此回复" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.center = CGPointMake(ScreenSizeWidth/2,ScreenSizeHeight-100);
    [alertView show];
}

-(void)goBig:(UITapGestureRecognizer *)recognizer{
    [[LogHelper shared] writeToFilefrom_page:@"id" from_section:@"i" from_step:@"d" to_page:@"ibd" to_section:@"i" to_step:@"d" type:@"" id:@"0"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"goBigPicture" object:nil];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    _deleteId = _dataArray[_deleteRow][@"id"];
    if (buttonIndex == 1) {
        [self deleteComments];
    }
}

-(void)deleteComments{    
    [_dataArray removeObjectAtIndex:_deleteRow];
    [self.tableView reloadData];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_DELETE,@"r",
                                    _deleteId,@"id",nil
                                    ];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(deleteCommentsFinish:) method:POSTDATA];
}
- (void)deleteCommentsFinish:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [self loadMessageData];
    }
}
-(void)showAllContentComment:(UIButton *)btn{
    _cellNum = btn.tag;
    
    if (btn.selected) {
        _showAllContent = YES;
    }else{
        _showAllContent = NO;
    }
    [self.tableView reloadData];
}
-(void)replyBtnPressed:(UIButton *)btn{
    if (!g_App.userID) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        return;
    }
    NSDictionary *dict = self.dataArray[btn.tag-100];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"popKeyboard" object:dict];
}

#pragma mark - 加载数据
-(void)setType:(NSString *)type{
    _type = type;
}
-(void)setBoolLoadData:(NSString *)boolLoadData{
    if (_loadNum.count == 0) {
        [_loadNum addObject:boolLoadData];
        [self loadMessageData];
        [self loadPraiseData];
    }else{
        BOOL isLoad = YES;
        for (int i = 0; i < _loadNum.count; i ++) {
            if ([_loadNum[i] isEqualToString:boolLoadData]) {
                isLoad = NO;
            }
        }
        if (isLoad) {
            [_loadNum addObject:boolLoadData];
            [self loadMessageData];
            [self loadPraiseData];
        }
    }
    
}
-(void)setDataDic:(NSMutableDictionary *)dataDic{
    
    _dataDic = dataDic;
   
    [self loadPictureAllData];
    
    [self loadMessageData];
//    [self loadPraiseData];
//    [self loadSomePicData];
    NSString *str = _dataDic[@"label"];
    NSArray *arrr = [str componentsSeparatedByString:@","];
    [self.tagBtnView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    for (int i = 0; i < arrr.count; i++) {
        UILabel *btn = [[UILabel alloc]initWithFrame:CGRectMake(10+((ScreenSizeWidth-60)/5+10)*i, 5, (ScreenSizeWidth-60)/5, 30)];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 5;
        btn.layer.borderWidth = 1;
        btn.text = [NSString stringWithFormat:@"%@",arrr[i]];
//        [btn setTitle:[NSString stringWithFormat:@"%@",arrr[i]] forState:UIControlStateNormal];
        btn.textAlignment = 1;
        btn.font = [UIFont systemFontOfSize:11];
        btn.textColor = RGBACOLOR(148, 148, 148, 1.0);
        btn.layer.borderColor = RGBACOLOR(200, 200, 200, 1.0).CGColor;
        [self.tagBtnView addSubview:btn];
    }
    
    [self.upPicture sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_dataDic[@"sourceImages"]]]];
    [self.userImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_dataDic[@"userIDInfo"][@"images"]]]];
    if ([_dataDic[@"isPraise"] isEqualToString:@"1"]) {
        [self.loveImageBtn setImage:[UIImage imageNamed:@"btn_aixing_2"] forState:UIControlStateNormal];
    }else{
        [self.loveImageBtn setImage:[UIImage imageNamed:@"btn_aixing"] forState:UIControlStateNormal];
        
    }
    NSString *nameStr = [NSString stringWithFormat:@"%@",_dataDic[@"userIDInfo"][@"name"]];
    if ([nameStr isEqualToString:@"(null)"]) {
        self.userName.text = @"";
    }else{
        self.userName.text = nameStr;
    }
//日期
    NSString *stt = [NSString stringWithFormat:@"%@",_dataDic[@"createTimeValue"]];
    if (stt.length >=10) {
        stt = [stt substringToIndex:10];
    }
    self.upTime.text = stt;
//    self.praiseNum.text = [NSString stringWithFormat:@"%@",_dataDic[@"praiseCount"]];
    
    self.upPicture.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeWidth*[_dataDic[@"height"] floatValue]/[_dataDic[@"width"] floatValue]);
    self.userImage.frame = CGRectMake(10, self.upPicture.bounds.size.height+12, 40, 40);
    self.userName.frame = CGRectMake(65, self.upPicture.bounds.size.height+16, 100, 20);
    self.upTime.frame = CGRectMake(65, self.upPicture.bounds.size.height+20+18, 100, 14);
    self.loveImageBtn.frame = CGRectMake(ScreenSizeWidth - 30 - 50, self.upPicture.bounds.size.height+18, 30, 30);
    self.praiseNum.frame = CGRectMake(ScreenSizeWidth - 50, self.upPicture.bounds.size.height+25, 50, 14);
//    self.otherOne.frame = CGRectMake(10, self.upPicture.bounds.size.height+70, ScreenSizeWidth-40, 20);
    _tittttlabel.frame = CGRectMake(0, self.upPicture.bounds.size.height + 64, ScreenSizeWidth, 50);
    self.collectionView.frame = CGRectMake(0, self.upPicture.bounds.size.height + 64, ScreenSizeWidth, 50);
    self.sanJiaoImage.frame = CGRectMake(ScreenSizeWidth - 30 - 50, self.upPicture.bounds.size.height+50, 30, 15);
    self.picLabel.frame = CGRectMake(10, self.upPicture.bounds.size.height+126, ScreenSizeWidth, 20);
    self.picCollectionView.frame = CGRectMake(0, self.upPicture.bounds.size.height+158, ScreenSizeWidth, 80);
    self.tagLabel.frame = CGRectMake(10, self.upPicture.bounds.size.height+250, ScreenSizeWidth, 20);
    self.tagBtnView.frame = CGRectMake(0, self.upPicture.bounds.size.height+282-5, ScreenSizeWidth, 40);
    self.nCommentLabel.frame = CGRectMake(10, self.upPicture.bounds.size.height+335-10, ScreenSizeWidth, 20);
    self.scrollView.frame = CGRectMake(0, 0, ScreenSizeWidth, self.upPicture.bounds.size.height + 365-10);
}

-(void)loadPictureAllData{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/detail",@"r",
                                    [NSString stringWithFormat:@"%@",_dataDic[@"id"]],@"id",
                                    @"1",@"withPraise",
                                    @"1",@"withLabel",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadPictureAllDataFinish:) method:GETDATA];
}

-(void)loadPictureAllDataFinish:(NSDictionary *)dic{
    if (dic && ![[dic objectForKey:@"code"] integerValue]){
        [_collectionArr removeAllObjects];
        [_picCollectionArr removeAllObjects];
        for (NSDictionary *item in dic[@"results"][@"praiseList"]) {
            [self.collectionArr addObject:item];
        }
        for (NSDictionary *item in dic[@"results"][@"relativeList"]) {
            [self.picCollectionArr addObject:item];
        }
//        [self.collectionView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
self.praiseNum.text = [NSString stringWithFormat:@"%@",dic[@"results"][@"praiseCount"]];
        [self.collectionView reloadData];
        [self.picCollectionView reloadData];
        self.tableView.tableHeaderView = self.scrollView;
        
    }
}
-(void)loadMessageData{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/contentList",@"r",
                                    [NSString stringWithFormat:@"%@",_dataDic[@"id"]],@"id",
                                    @"1",@"page",
                                    [NSString stringWithFormat:@"%d",9999],@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadMessageDataFinish:) method:GETDATA];
}
-(void)loadMessageDataFinish:(NSDictionary *)dic{
    if (dic && ![[dic objectForKey:@"code"] integerValue]){
        [self.dataArray removeAllObjects];
        for (NSDictionary *item in [dic objectForKey:@"results"]) {
            [self.dataArray addObject:item];
        }
        [self.tableView reloadData];
    }
    if ([_type isEqualToString:@"20"]) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height-ScreenSizeHeight+64) animated:YES];
    }
}
-(void)loadPraiseData{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/praiseList",@"r",
                                    [NSString stringWithFormat:@"%@",_dataDic[@"id"]],@"id",
                                    @"1",@"page",
                                    [NSString stringWithFormat:@"%d",9999],@"size",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadPraiseDataFinish:) method:GETDATA];
}
-(void)loadPraiseDataFinish:(NSDictionary *)dic{
    if (dic && ![[dic objectForKey:@"code"] integerValue]){
        [self.collectionArr removeAllObjects];
        for (NSDictionary *item in [dic objectForKey:@"results"]) {
            [self.collectionArr addObject:item];
        }
//        if (_collectionArr.count == 0) {
//            self.collectionView.hidden = YES;
//            self.otherOne.hidden = YES;
//            self.scrollView.frame = CGRectMake(0, 0, ScreenSizeWidth, self.upPicture.bounds.size.height + 80);
//            self.tableView.tableHeaderView = self.scrollView;
//        }else{
//            self.collectionView.hidden = NO;
//            self.otherOne.hidden = NO;
//            self.scrollView.frame = CGRectMake(0, 0, ScreenSizeWidth, self.upPicture.bounds.size.height + 140);
            self.tableView.tableHeaderView = self.scrollView;
             [self.collectionView reloadData];
//        }
    }
}
-(void)loadSomePicData{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/contentList",@"r",
                                    [NSString stringWithFormat:@"%@",_dataDic[@"id"]],@"id",
                                    @"1",@"page",
                                    [NSString stringWithFormat:@"%d",9999],@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadSomePicDataFinish:) method:GETDATA];
}
-(void)loadSomePicDataFinish:(NSDictionary *)dic{
    if (dic && ![[dic objectForKey:@"code"] integerValue]){
        [self.dataArray removeAllObjects];
        for (NSDictionary *item in [dic objectForKey:@"results"]) {
            [self.dataArray addObject:item];
        }
        [self.tableView reloadData];
    }
    if ([_type isEqualToString:@"20"]) {
        [self.tableView setContentOffset:CGPointMake(0, self.tableView.contentSize.height-ScreenSizeHeight+64) animated:YES];
    }
}
@end
