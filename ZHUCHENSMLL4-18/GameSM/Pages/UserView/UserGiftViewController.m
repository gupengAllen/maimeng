//
//  UserGiftViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "UserGiftViewController.h"
#import "UserGiftCell.h"
#import "GiftDetailController.h"
#import "MBProgressHUD+Add.h"
#import "GiftViewController.h"
#import "CustomNavigationController.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface UserGiftViewController ()
{
    UIButton *_rightBtn;
}
@end

@implementation UserGiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"我的礼包"];
    [self addBackBtn];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self loadData:NO];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
    
    _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightBtn setTitle:@"礼包中心" forState:UIControlStateNormal];
    [_rightBtn setTitleColor:RGBACOLOR(148, 148, 148, 1.0) forState:UIControlStateNormal];
    _rightBtn.bounds = CGRectMake(0, 0,70, 20);
    _rightBtn.titleLabel.textAlignment = 2;
    [self addRightItemWithButton:_rightBtn itemTarget:self action:@selector(rightBtnPressed:)];
}

-(void)rightBtnPressed:(UIButton *)btn{
    
    [[LogHelper shared] writeToFilefrom_page:@"pg" from_section:@"profile" from_step:@"l" to_page:@"gl" to_section:@"gift" to_step:@"l" type:@"" id:@""];
    GiftViewController *giftView = [[GiftViewController alloc] initWithNibName:@"GiftViewController" bundle:nil];
    [self.navigationController pushViewController:giftView animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
}

#pragma mark - 按钮事件
- (void)userGiftBtnPressed:(UIButton*)btn {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = [[self.dataArray objectAtIndex:btn.tag - 100] objectForKey:@"code"];
    [MBProgressHUD showSuccess:@"复制成功" toView:self.view];
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}


- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"userGiftCell";
    UserGiftCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [UserGiftCell userGiftCellOwner:self];
        
//        cell.userGiftImageView.layer.borderColor = [UIColor darkGrayColor].CGColor;
//        cell.userGiftImageView.layer.borderWidth = 0.5;
//        cell.userGiftCodelabel.layer.cornerRadius = cell.userGiftCodelabel.bounds.size.height / 2;
//        cell.userGiftCodelabel.layer.masksToBounds = YES;
        cell.userGiftImageView.layer.cornerRadius = 10;
        cell.userGiftImageView.layer.masksToBounds = YES;
    }
    if (!self.dataArray.count) {
        return cell;
    }
    cell.userGiftCodelabel.textColor = [UIColor colorWithRed:241/255.0 green:180/255.0 blue:186/255.0 alpha:1];
    cell.userGiftTimeLabel.text = [NSString stringWithFormat:@"领取时间 %@", [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"getTime"]];
    [cell.userGiftImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"giftIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    cell.userGiftTitleLabel.text = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"giftIDInfo"] objectForKey:@"title"];
    cell.userGiftPointLabel.text = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"giftIDInfo"] objectForKey:@"label"];
    cell.userGiftCodelabel.text = [NSString stringWithFormat:@"礼包卡号：%@",[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"code"]];
    cell.userGiftBtn.tag = 100 + indexPath.section;
    [cell.userGiftBtn addTarget:self action:@selector(userGiftBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GiftDetailController *detailView = [[GiftDetailController alloc] initWithNibName:@"GiftDetailController" bundle:nil];
    detailView.giftID = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"giftIDInfo"] objectForKey:@"id"];
    detailView.giftTitle = [[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"giftIDInfo"] objectForKey:@"title"];
    [self.navigationController pushViewController:detailView animated:YES];
}

-(void)loadData:(BOOL)isOrmore{
    if (isOrmore == YES) {
        page++;
    } else {
        page = 1;
        [self.dataArray removeAllObjects];
    }
    [self giftList];
}

#pragma mark - 加载数据
- (void)giftList {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_GIFTCODE_WITHUSER,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    [NSString stringWithFormat:@"%d",page],@"page",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(giftListFinish:) method:GETDATA];
}

- (void)giftListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        
//        if ([[dic objectForKey:@"results"] count] < size) {
//            self.tableView.reachedTheEnd = YES;
//            [self bottomShow];
//        } else {
//            self.tableView.reachedTheEnd = NO;
//        }
        if ([[dic objectForKey:@"results"] count] == 0) {
            [self bottomShow];
        }
        
        [self.tableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else
            self.contentStatusLabel.hidden = YES;
    }
    
    [self.tableView tableViewDidFinishedLoading];
}

@end
