//
//  PictureAboutViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/18.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "PictureAboutViewController.h"
#import "PullingRefreshTableView.h"
#import "ZanedTableViewCell.h"
#import "userCommentPictureCell.h"
#import "UIButton+AFNetworking.h"
#import "UserPictureWhoPraiseViewController.h"
@interface PictureAboutViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *_tableView ;
    NSMutableArray  *_faceNameArr;
    UIImageView *_netImageView;
    UIImageView     *_tipImageView;
    
    
    
}
@property (nonatomic, strong) UITableView *noticeTableView;

@end

@implementation PictureAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    LYTabBarController *tabBar = (LYTabBarController *)self.tabBarController;
    [tabBar hiddenBar:YES animated:YES];
    if(_type == 1){
        [self initTitleName:@"赞过的美图"];
    }else{
        [self initTitleName:@"评论过的美图"];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshUI:) name:@"refreshZanedImage" object:nil];
    [self.view addSubview:self.noticeTableView];
    [self addBackBtn];
    [self noticeList];
    [self createTipImageView];
    [self.view addSubview:self.sheetView];
    

    
}

- (void)refreshUI:(NSNotification *)noti{
    [self noticeList];

}

-(void)createTipImageView{
    _tipImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    _tipImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_tipImageView];
    _tipImageView.hidden = YES;
}


- (void)noticeList {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    if (_type == 0) {
        NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        (NSString *)API_URL_USERPICCOMMMENT,@"r",
                                        [NSString stringWithFormat:@"%d",999],@"size",
                                        [NSString stringWithFormat:@"%d",1],@"page",nil];
        
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeListFinish:) method:GETDATA];
    }else if(_type == 1){
        NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        (NSString *)API_URL_PRAISEPIC,@"r",
                                        [NSString stringWithFormat:@"%d",999],@"size",
                                        [NSString stringWithFormat:@"%d",1],@"page",nil];
        
        [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(noticeListFinish:) method:GETDATA];
        
    }
}

- (void)noticeListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    ////    self.isLoading = NO;
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        _netImageView.alpha = 0;
        self.dataArray = dic[@"results"];
        

    }
    //
    [self.noticeTableView reloadData];
    if (![self.dataArray count]) {
        self.contentStatusLabel.hidden = NO;
        _tipImageView.hidden = NO;
        _tipImageView.image = [UIImage imageNamed:@"pinglunTip.png"];
    } else{
        _tipImageView.hidden = YES;
        self.contentStatusLabel.hidden = YES;
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    if (_type == 0) {
        userCommentPictureCell *userCommentPCell = [[[NSBundle mainBundle] loadNibNamed:@"userCommentPictureCell" owner:self options:nil] lastObject];
        userCommentPCell.titleLabel.text = self.dataArray[indexPath.row][@"content"];
        [userCommentPCell.commentPicture setImageWithURL:[NSURL URLWithString:self.dataArray[indexPath.row][@"prettyImagesInfo"][@"images"]]];
        userCommentPCell.timeLabel.text = self.dataArray[indexPath.row][@"createTime"];
        
        userCommentPCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return userCommentPCell;
    }else if(_type == 1){
        
        ZanedTableViewCell *zanedCell = [[[NSBundle mainBundle] loadNibNamed:@"ZanedTableViewCell" owner:self options:nil] lastObject];
        
//        NSMutableAttributedString *str = [NSMutableAttributedString stringWithFormat:@"我赞了%@的图片",self.dataArray[indexPath.row][@"prettyImagesInfo"][@"userIDInfo"][@"name"]];
        NSString *nameStr = self.dataArray[indexPath.row][@"prettyImagesInfo"][@"userIDInfo"][@"name"];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"我赞了%@的美图",nameStr]];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(3,nameStr.length)];
        
        

        zanedCell.zanLabel.attributedText = str;
        [zanedCell.userImage setImageWithURL:[NSURL URLWithString:self.dataArray[indexPath.row][@"prettyImagesInfo"][@"images"]]];

        zanedCell.timeLabel.text = self.dataArray[indexPath.row][@"createTime"];
        zanedCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return zanedCell;
    }else{
        return nil;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 95;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *lastDict = self.dataArray[indexPath.row];
    
    [[LogHelper shared] writeToApafrom_page:@"pic" from_section:@"profile" from_step:@"l" to_page:@"id" to_section:@"i" to_step:@"detail" type:@"" id:lastDict[@"valueID"]];
    UserPictureWhoPraiseViewController *userPicVC = [[UserPictureWhoPraiseViewController alloc] init];
//    userPicVC.allDataArr = @[@{@"sourceImages":lastDict[@"prettyImagesInfo"][@"images"],@"id":lastDict[@"valueID"],@"userIDInfo":lastDict[@"prettyImagesInfo"][@"userIDInfo"],@"width":lastDict[@"prettyImagesInfo"][@"width"],@"height":lastDict[@"prettyImagesInfo"][@"height"],@"createTimeValue":lastDict[@"createTime"],@"praiseCount":lastDict[@"prettyImagesInfo"][@"praiseCount"]}].mutableCopy;
    userPicVC.pictureId = lastDict[@"valueID"];
    
    userPicVC.type = 100;
    userPicVC.allDataArr = @[];
    [self presentViewController:userPicVC animated:YES completion:nil];
    
}

- (UITableView*)noticeTableView {
    if (!_noticeTableView) {
        _noticeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64) ];
        _noticeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//        _noticeTableView.separatorColor = [UIColor lightGrayColor];
        _noticeTableView.backgroundColor = [UIColor whiteColor];
        _noticeTableView.backgroundView = nil;
        _noticeTableView.dataSource = self;
        _noticeTableView.delegate = self;
    }
    return _noticeTableView;
}


- (NSMutableArray *)dataArray {
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
