
//
//  HomeViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "HomeViewController.h"
#import "InfoCell.h"
#import "InfoDetailViewController.h"
#import "CategeryCell.h"
#import "PicDetailViewController.h"
#import "NoticeViewController.h"
//#import "GuideViewController.h"
#import "WebViewController.h"
#import "GiftDetailController.h"
#import "GiftViewController.h"
#import "MobClick.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
#import "HZPhotoBrowser.h"
#import "HZPhotoItemModel.h"
#import "HZImagesGroupView.h"
#import "LYTabBarController.h"
#import "testViewController.h"
#import "DataBaseHelper.h"
#import "CustomTool.h"
//缓存
#import "ASIDownloadCache.h"
#import "ASIHTTPRequest.h"
#import "NSDate+OTS.h"
#import "CustomIOSAlertView.h"
#import "ConstantViewController.h"
#import "Y_X_DataInterface.h"
#import <StoreKit/StoreKit.h>

#import "HomeSearchCell.h"
#import "UserUpLoadPicViewController.h"

#import "LGPhotoPickerViewController.h"
#import "PictureViewController.h"

#import "UserPictureWhoPraiseViewController.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface PictureViewController ()<HZPhotoBrowserDelegate,CustomAlertViewDelegate,LYTabBarViewDelegate,CustomIOSAlertViewDelegate,SKStoreProductViewControllerDelegate,UISearchBarDelegate,LGPhotoPickerViewControllerDelegate,UISearchBarDelegate>
{
    NSInteger currentStar;
    NSMutableArray *_articleIsReadArr;
    NSMutableArray *_searchResultArr;
    NSArray *_picTagArr;
    UIView      *_searchTagView;
//    UISearchBar *_searchBar;
    NSMutableArray *_searchTag;
    NSMutableArray *_searchTagName;
    UIButton *_searBtn;
}
@property (nonatomic, strong) NSMutableArray *starArray;
@property (nonatomic, strong) NSMutableArray *picArray;
@property (nonatomic, strong) NSMutableArray *currentPicArray;
@property (nonatomic, strong) NSString *infoCount;
@property (nonatomic, strong) NSString *picCount;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isMore;

@property (nonatomic, strong) CustomAdView *adView;
@property (nonatomic, strong) NSMutableArray *adInfoArray;
@property (nonatomic, strong) UITableView *findTableView;//102
@property (nonatomic, strong) UIScrollView *pictureTagView;
@property (nonatomic, strong) UIView    *pictureTopView;

@end

@implementation PictureViewController
{
    UIImageView *helpIV;
    CGFloat _lastContentOffset;
    CGFloat _beginScrollViewX;
    LYTabBarController *_lyTabbar;
    HZPhotoBrowser *_browserVc;
    int _prePage;
    int _indexI;
    UIImageView *_netImageView;
    UIButton    *_refreshBtn1;
    UIButton    *_refreshBtn2;
    UILabel     *_label;
    UIView     *_picTitleView;
    BOOL         _isDeleteCache;
    BOOL         _isMoreRecord;
    BOOL         _isSearchArticle;
    BOOL         _isShowPicTag;
    BOOL         _isResultGoDetail;
    BOOL         _isToTopShow;
    BOOL         _isTap;
    //    UIButton *_refreshBtn;
    CustomButton *_leftBtn;
    CustomButton *_rightBtn;
    NSString *_plistPath;
    NSString *_thirdBtnName;
    //----
    NSMutableArray *_ArticleSearchRecordArr;
    NSMutableArray *_picHeadViewTagArr;
    NSMutableArray *_picTopTagArr;
    CGFloat _searchCurrentOffset;
    CGFloat _searchMaximumOffset;
    int _searchPageNum;
    int _searchCountTotal;
    int _selectBtnTag;
    int _homeOrPicIndex;
    NSString *_picBtnTagName;
    UIButton *_goTopBtn;
    
    //    UIView *_twoBtnView;
    UITapGestureRecognizer *_tapGesture;
    UITapGestureRecognizer *_tap1Gesture;
    UIAlertView *alert;
    
    //
    UIView *_hisRecordView;
    
    BOOL _goTopPicture;
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:HomeNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TOROOT" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"picUpLoadSuccessed" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"haveNewNotice" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changePicArr" object:nil];
}


- (NSMutableArray *)starArray {
    if (_starArray == nil) {
        _starArray = [[NSMutableArray alloc] init];
    }
    return _starArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    [[LogHelper shared] writeToFilefrom_page:@"null" from_section:@"null" from_step:@"null" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"" id:@""];
    _ArticleSearchRecordArr = [NSMutableArray array];
    _picHeadViewTagArr = [NSMutableArray array];
    _currentPicArray = [NSMutableArray array];
    _picTopTagArr = [NSMutableArray array];
    _isDeleteCache = YES;
    _searchResultArr = [NSMutableArray array];
    _searchTag = [NSMutableArray array];
    _searchTagName = [NSMutableArray array];
    _isMoreRecord = NO;
    _isSearchArticle = NO;
    _isShowPicTag = NO;
    _isToTopShow = NO;
    _isResultGoDetail = NO;
    _isTap = NO;
    _prePage = 1;
    _goTopPicture = NO;
    _lyTabbar = (LYTabBarController *)self.tabBarController;
    _picTagArr = [NSArray array];
    
    // Do any additional setup after loading the view from its nib.
    //    [self initTitleName:@"麦萌"];
    
    
    if (IsIOS7) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    _menuView.frame = CGRectMake(0, 0, 160, 44);
    _menuView.delegate = self;
    _menuView.titleArray = @[@"美图"];
    _menuView.currentIndex = 0;
    
    //    [self createFindView];
    
#warning =
    [self createPictureTagView];
    UINavigationItem *item = self.navigationItem;
    item.titleView = _menuView;
    
    
    self.picArray = [[NSMutableArray alloc] init];
    
    [self initView];
    
    //    self.tableView.frame = self.scrollView.bounds;
    //-----
    
    _rightBtn = [CustomButton buttonWithType:UIButtonTypeCustom];
    [_rightBtn setImage:[UIImage imageNamed:@"shagnchuanjilv"] forState:UIControlStateNormal];
    _rightBtn.bounds = CGRectMake(0, 0, _rightBtn.imageView.image.size.width, _rightBtn.imageView.image.size.height);
    _rightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
    [self addRightItemWithButton:_rightBtn itemTarget:self action:@selector(rightBtnPressed:)];
    
    _leftBtn = [CustomButton buttonWithType:UIButtonTypeCustom];
    _leftBtn.bounds = CGRectMake(0, 0, 40, 40);
    _leftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, -20, 0, 0);
    [_leftBtn setImage:[UIImage imageNamed:@"tianjai"] forState:UIControlStateNormal];
    [self addLeftItemWithButton:_leftBtn itemTarget:self action:@selector(leftBtnPressed:)];
    
    [self readUserInfo];
    
    [self loadData:NO];
//    [self refreshContinueData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveHomeNotification:) name:HomeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveRefreshNotification:) name:RefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(uploadSuccess) name:@"TOROOT" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(doneLoadingTableViewData) name:@"picUpLoadSuccessed" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRedImage:) name:@"haveNewNotice" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changePicArr:) name:@"changePicArr" object:nil];
    
    [self.view bringSubviewToFront:self.hud];
    //    [self versionUpdate];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UPLOGTIME"]) {
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970])forKey:@"UPLOGTIME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    if (([[NSDate date] timeIntervalSince1970] - [[[NSUserDefaults standardUserDefaults] objectForKey:@"UPLOGTIME"] integerValue])/3600 > 0.1) {
        [self upLog];
        [[NSUserDefaults standardUserDefaults] setObject:@([[NSDate date] timeIntervalSince1970])forKey:@"UPLOGTIME"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    //--
    [_scrollView addSubview:_netImageView];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(showBar)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [_netImageView addGestureRecognizer:swipe];
    
    
    UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineImageView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:0.5];
    [self.view addSubview:lineImageView];
    
    NSInteger countDays = 0;
    NSInteger todatDays = [[NSDate new] distanceNowDays];
    for (NSNumber *savedDate in [[DataBaseHelper shared] fetchDateTime]) {
        if ( [savedDate integerValue] > todatDays - 7 && [savedDate integerValue] < todatDays ) {
            countDays ++;
        }
    }
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"APPEARED"] isEqualToString:@"0"] && countDays >= 5) {
        CustomIOSAlertView *cusIOSAlert = [[CustomIOSAlertView alloc] init];
        cusIOSAlert.tag = 0;
        [cusIOSAlert setContainerView:[self createDemoView]];
        
        [cusIOSAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"日后再说", @"提交", nil]];
        [cusIOSAlert setDelegate:self];
        
        [cusIOSAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
            NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
            [alertView close];
        }];
        
        [cusIOSAlert setUseMotionEffects:true];
        
        // And launch the dialog
        [cusIOSAlert show];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"APPEARED"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)changePicArr:(NSNotification *)noti{
    self.picArray = noti.object;
    [self.collectionView reloadData];
}


-(void)leftBtnPressed:(UIButton *)btn{
    if (!g_App.userID) {
        [self loginView];
        return;
    }
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"iuc" to_section:@"i" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        LGPhotoPickerViewController *pickerVc = [[LGPhotoPickerViewController alloc] initWithShowType:LGShowImageTypeImageBroswer];
        pickerVc.status = PickerViewShowStatusCameraRoll;
        pickerVc.maxCount = 1;   // 最多能选9张图片
        pickerVc.delegate = self;
        //    self.showType = style;
        [pickerVc showPickerVc:self];
}

#pragma mark - searchBar Delegate
//开始
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [_searBtn setTitle:@"搜索" forState:UIControlStateNormal];
}

//结束
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [_searBtn setTitle:@"取消" forState:UIControlStateNormal];
}

//取消按钮点击
-(void)misSearchView{
    [self.searchBar resignFirstResponder];
    [self.navigationController setNavigationBarHidden:NO];
    [_lyTabbar hiddenBar:NO animated:YES];
    _searchBar.text = @"";
    self.searchView.alpha = 0;
    self.searchBgView.alpha = 0;
    self.searchResultTableView.alpha = 0;
    self.searchResultBgView.alpha = 0;
    self.searchTableView.alpha = 0;
    self.searchBar.frame = CGRectMake(0, -40, KScreenWidth, 40);
    _isTap = NO;
    _isMoreRecord = NO;
    _isSearchArticle = NO;
    _isResultGoDetail = NO;
}


- (void)synInfo{
    
    NSDictionary *saveDict = @{@"r":@"cartoonCollection/synUserCollectionInfo",@"postData":[[DataBaseHelper shared] fetchCartoonPartCollectionStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:saveDict object:self action:@selector(upCollectionData:) method:POSTDATA];
    
    
    NSDictionary *readDict = @{@"r":@"cartoonReadHistory/synUserReadInfo",@"postData":    [[DataBaseHelper shared] fetchCartoonPartReadStatus]};
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(upReadData:) method:POSTDATA];
    
}

- (void)upReadData:(NSDictionary *)readDict {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] forKey:@"SYNTIME"];
    
}

- (void)upCollectionData:(NSDictionary *)collectionDict {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] forKey:@"SYNTIME"];
}

- (void)upLog{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [[Y_X_DataInterface commonParams] enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        [manager.requestSerializer setValue:obj forHTTPHeaderField:key];
    }];
    
    
    [manager POST:INTERFACE_PREFIXD parameters:@{@"r":@"system/uploadLog"}
constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         NSLog(@"path%@",[[LogHelper shared] getPath]);
         //        [[NSBundle mainBundle] ur]
         
         BOOL isS = [formData appendPartWithFileURL:[NSURL URLWithString:[NSString stringWithFormat:@"file://%@",[[LogHelper shared] getPath]]] name:@"logFile" fileName:@"log" mimeType:@"application/rtf" error:nil];
         
     } success:^(AFHTTPRequestOperation *operation, id responseObject) {
         NSString *log = @" ";
         [log writeToFile:[[LogHelper shared] getPath] atomically:YES encoding:NSUTF8StringEncoding error:nil];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
         NSLog(@"--error:%@",error);
     }];
}



- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
    
    if (alertView.tag == 0) {
        
        if (buttonIndex == 1) {
            
            [YK_API_request startLoad:@"http://api.playsm.com/index.php" extraParams:@{@"r":@"feedback/score",@"score":@(currentStar)} object:self action:@selector(starRequire:) method:POSTDATA];
            
            CustomIOSAlertView *cusIOSAlert = [[CustomIOSAlertView alloc] init];
            
            cusIOSAlert.tag = 1;
            if (currentStar <3) {
                [cusIOSAlert setContainerView:[self secondCreateView:@"您愿意向我们反馈您在使用中遇到的问题吗"]];
            }else{
                [cusIOSAlert setContainerView:[self secondCreateView:@"你愿意嫁给我吗?不是不是~您愿意给我们5星好评咩"]];
            }
            
            [cusIOSAlert setButtonTitles:[NSMutableArray arrayWithObjects:@"日后再说", @"I do", nil]];
            [cusIOSAlert setDelegate:self];
            
            [cusIOSAlert setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
                NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
                [alertView close];
            }];
            
            [cusIOSAlert setUseMotionEffects:true];
            
            // And launch the dialog
            [cusIOSAlert show];
        }}else if (alertView.tag == 1){
            if (buttonIndex == 1) {
                if (currentStar < 3) {
                    ConstantViewController *feedbackView = [[ConstantViewController alloc] initWithNibName:@"ConstantViewController" bundle:nil];
                    feedbackView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    UINavigationController *bac = [[UINavigationController alloc]initWithRootViewController:feedbackView];
                    [self presentViewController:bac animated:YES completion:nil];
                    
                }else {
                    
                    //                    https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=999759889&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8
                    
                    NSString *str = @"";
                    
                    if( ([[[UIDevice currentDevice] systemVersion] doubleValue]>=7.0))
                    {
                        str = @"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1023347547&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
                        
                    }else {
                        
                        str = @"https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=1023347547&pageNumber=0&sortOrdering=2&type=Purple+Software&mt=8";
                        
                    }
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
                }
            }
        }
}

- (void)starRequire:(NSDictionary *)requireStar {
    
}

- (UIView *)secondCreateView:(NSString *)titleDes {
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 120)];
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 250, 80)];
    textLabel.textColor = [UIColor blackColor];
    textLabel.text = titleDes;
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentCenter;
    [demoView addSubview:textLabel];
    return demoView;
}

- (UIView *)createDemoView {
    UIView *demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 120)];
    //
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 250, 80)];
    textLabel.textColor = [UIColor blackColor];
    textLabel.text = @"  通过这几天的使用,你对我们的服务满意吗";
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentCenter;
    [demoView addSubview:textLabel];
    
    
    for(int i = 0; i < 5; i ++){
        UIButton *cusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cusButton.frame = CGRectMake(40 + 50 * i,  80, 20, 20);
        NSLog(@"cgrect %@",NSStringFromCGRect(cusButton.frame));
        [cusButton setImage:[UIImage imageNamed:@"star_1.png"] forState:UIControlStateNormal];
        [cusButton setImage:[UIImage imageNamed:@"star_2.png"] forState:UIControlStateSelected];
        [cusButton addTarget:self action:@selector(starButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        cusButton.tag = 1000 + i;
        [demoView addSubview:cusButton];
        [self.starArray addObject:cusButton];
    }
    
    return demoView;
}

- (void)starButtonClick:(UIButton *)sender {
    //    sender.selected = !sender.selected;
    for (UIButton *curBtn in self.starArray) {
        curBtn.selected = NO;
    }
    for (int i = 0; i <= sender.tag - 1000; i ++) {
        UIButton *btn = self.starArray[i];
        btn.selected = YES;
        currentStar = sender.tag - 1000;
    }
}



- (void)advertiseMent:(NSDictionary *)advDict
{
    if([advDict[@"results"] count] > 0){
        
        if (![advDict[@"results"][0][@"customValue"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"OUTVALUE"]]) {
            [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"ADVISECOUNT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        //        [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:@"ADVISECOUNT"];
        
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"images"]forKey:@"ADVTISE"];
        //        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"id"] forKey:@"HOMEADID"];
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"customType"] forKey:@"HOMEADCUSTOMTYPE"];
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"customValue"] forKey:@"OUTVALUE"];
        [[NSUserDefaults standardUserDefaults] setObject:advDict[@"results"][0][@"duration"] forKey:@"DURATION"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else {
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"ADVTISE"];
        //        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"HOMEADID"];
        [[NSUserDefaults standardUserDefaults] setObject:@""forKey:@"HOMEADCUSTOMTYPE"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"OUTVALUE"];
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"DURATION"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    
}

-(void)showBar{
    [_lyTabbar hiddenBar:NO animated:YES];
}
- (void)refreshContinueData{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"currentCharpteId"]&&[[NSUserDefaults standardUserDefaults] objectForKey:@"currentAlbumId"]) {
        NSDictionary * readDict = @{@"chapterId":[[NSUserDefaults standardUserDefaults] objectForKey:@"currentCharpteId"],@"r":@"cartoonReadHistory/add",@"albumId":[[NSUserDefaults standardUserDefaults] objectForKey:@"currentAlbumId"]};
        
        NSDictionary *headers;
        if (g_App.userInfo.userID != nil) {
            headers = @{@"userID":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
        }else {
            headers = @{@"userID":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
        }
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:readDict object:self action:@selector(saveReadData:) method:@"POST" andHeaders:headers];
    }
}

- (void)saveReadData:(NSDictionary *)saveDict{
    
}
-(void)viewDidDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
    _isMoreRecord = NO;
    //    self.searchBgView.alpha = 0;
    //    self.searchView.alpha = 0;
    //    self.searchResultTableView.alpha = 0;
}

-(void)viewDidAppear:(BOOL)animated{
    
}

-(void)misHelp:(UIControl *)sender{
    [helpIV removeFromSuperview];
    [sender removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    if (_isResultGoDetail) {
        [self.navigationController setNavigationBarHidden:YES];
    }
    NSLog(@"--%@",(LYTabBarController*)self.tabBarController);
    
    [(LYTabBarController*)self.tabBarController hiddenBar:NO animated:YES];
    
    if (g_App.userID) {
        [self.view bringSubviewToFront:self.bottomStatusLabel];
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];//即使没有显示在window上，也不会自动的将self.view释放。
    
    [self dismissViewControllerAnimated:YES completion:^{
        //        UIAlertView *view = [[UIAlertView alloc]initWithTitle:@"提示" message:@"加载失败" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        //        [view show];
    }];
    
}


- (void)readUserInfo {
    NSUserDefaults *userDefauls = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", [userDefauls objectForKey:@"userID"]);
    if ([userDefauls objectForKey:@"userID"] && [[userDefauls objectForKey:@"userID"] length]) {
        g_App.userID = [userDefauls objectForKey:@"userID"];
        g_App.userName = [userDefauls objectForKey:@"name"];
        g_App.telephone = [userDefauls objectForKey:@"telephone"];
        [Y_X_DataInterface setCommonParam:YX_KEY_USERID value:g_App.userID];
        [Y_X_DataInterface setCommonParam:YX_KEY_LOGINTIME value:[userDefauls objectForKey:@"loginTime"]];
        g_App.userInfo = [[UserInfo alloc] init];
        g_App.userInfo.userID = [userDefauls objectForKey:@"userID"];
        g_App.userInfo.sex = (int)[[userDefauls objectForKey:@"sex"] integerValue];
        g_App.userInfo.name = [userDefauls objectForKey:@"name"];
        g_App.userInfo.telephone = [userDefauls objectForKey:@"telephone"];
        g_App.userInfo.signature = [userDefauls objectForKey:@"signature"];
        g_App.userInfo.images = [userDefauls objectForKey:@"images"];
        g_App.userInfo.loginType = [userDefauls objectForKey:@"loginType"];
    }
}

#pragma mark - 界面初始化
- (PullPsCollectionView*)collectionView {
    if (!_collectionView) {
        _collectionView = [[PullPsCollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, _scrollView.frame.size.height-60)];
        _collectionView.collectionViewDelegate = self;
        _collectionView.collectionViewDataSource = self;
        //        _collectionView.pullDelegate=self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collectionView.showsVerticalScrollIndicator = YES;
        _collectionView.scrollsToTop = YES;
        _collectionView.numColsPortrait = 2;
        _collectionView.numColsLandscape = 2;
        _collectionView.pullArrowImage = [UIImage imageNamed:@"blueArrow.png"];
        _collectionView.pullBackgroundColor = [UIColor clearColor];
        _collectionView.pullTextColor = [UIColor blackColor];
    }
    return _collectionView;
}

- (void)initView {
    
    [self.view addSubview:self.scrollView];
    self.pictureTopView.frame = CGRectMake(0, 0, self.pictureTopView.bounds.size.width, self.pictureTopView.bounds.size.height);
    [self.scrollView addSubview:self.pictureTopView];
    
    _hisRecordView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height - _hisRecordView.bounds.size.height, _hisRecordView.bounds.size.width, _hisRecordView.bounds.size.height);
    [self.scrollView addSubview:_hisRecordView];
    self.pictureTagView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
    [self.scrollView addSubview:self.pictureTagView];
    
    self.collectionView.frame = CGRectMake(0, 50, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height-6);
    [self.scrollView addSubview:self.collectionView];

    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - KScreenheight, KScreenWidth, KScreenheight)];
        view.delegate = self;
        [_collectionView addSubview:view];
        _refreshHeaderView = view;
    }
    
#warning 第一天修改的
    [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width, 0)];
    //    self.scrollView.backgroundColor = [UIColor grayColor];
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.bounces = NO;
    self.scrollView.scrollsToTop = NO;
    self.collectionView.scrollsToTop = NO;
    
    self.topShowView = [[TopShowView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 0)];
    [self.view addSubview:_topShowView];
    
    [self.scrollView bringSubviewToFront:_collectionView];
    [self.scrollView bringSubviewToFront:self.pictureTagView];
    [self.scrollView bringSubviewToFront:_hisRecordView];
    [self.scrollView bringSubviewToFront:self.pictureTopView];
    
}


- (void)receiveRefreshNotification:(NSNotification*)notification {
    if (g_App.userID) {
    }
}

#pragma mark - 按钮事件
- (void)rightBtnPressed:(UIButton*)btn {

        if (!g_App.userID) {
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"nl" to_section:@"i" to_step:@"l" type:@"history" id:[NSString stringWithFormat:@"%d",0]];
            [self loginView];
            return;
        }
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"ipl" to_section:@"i" to_step:@"l" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
//    useruploadPic.itemDic = self.picArray[0];
        [self.navigationController pushViewController:useruploadPic animated:YES];
    
}
- (void)loginView {
    LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    [loginView setSkipNext:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
        [self.navigationController pushViewController:useruploadPic animated:YES];
        
    }];
    
    CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
}

#pragma mark - getter && setter
- (UIScrollView*)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64  - 49  + 80)];
        _scrollView.scrollsToTop = NO;
        
    }
    return _scrollView;
}


#pragma mark - CustomMenuViewDelegate
//-----------导航栏元素项点击事件
- (void)customMenuViewSelectedWithIndex:(NSInteger)index {

}

#pragma mark - PSCollectionViewDelegate
- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView viewAtIndex:(NSInteger)index
{
    CategeryCell *cell = (CategeryCell*)[collectionView dequeueReusableView];
    
    if (!cell) {
        cell = [[CategeryCell alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth/2 - 5, 30)];
    }
    [cell.categoryImage setImageWithURL:[NSURL URLWithString:[[self.picArray objectAtIndex:index] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    cell.praiseNum.text = [NSString stringWithFormat:@"%@",[[self.picArray objectAtIndex:index] objectForKey:@"praiseCount"]];
    [cell.userImage setImageWithURL:[NSURL URLWithString:[[self.picArray objectAtIndex:index] objectForKey:@"userIDInfo"][@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    cell.userName.text = [[self.picArray objectAtIndex:index] objectForKey:@"userIDInfo"][@"name"];
    cell.praCount.text = [[self.picArray objectAtIndex:index] objectForKey:@"praiseCount"];
    cell.commentCount.text = [[self.picArray objectAtIndex:index] objectForKey:@"contentCount"];
    NSString *string = [[self.picArray objectAtIndex:index] objectForKey:@"isPraise"];
    if ([string isEqualToString:@"0"]) {
        cell.praCountImage.image = [UIImage imageNamed:@"meitu_dianzan_5"];
    }else{
        cell.praCountImage.image = [UIImage imageNamed:@"shoudaozan"];
    }
    return cell;
}

- (CGFloat)heightForViewAtIndex:(NSInteger)index
{
    if (self.picArray.count) {
        if (![[[self.picArray objectAtIndex:index] objectForKey:@"width"] integerValue]) {
            return ScreenSizeWidth / 2 - 5+30;
        } else {
            return [[[self.picArray objectAtIndex:index] objectForKey:@"height"] integerValue] * (1.0*(ScreenSizeWidth / 2 - 5) / [[[self.picArray objectAtIndex:index] objectForKey:@"width"] integerValue])+30.0;
        }
    } else {
        return ScreenSizeWidth / 2 - 5+30;
    }
}
- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index
{
    [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:[NSString stringWithFormat:@"%d",index]];
    UserPictureWhoPraiseViewController *userP = [[UserPictureWhoPraiseViewController alloc]init];
    userP.initNum = (int)index;
    userP.allDataArr = self.picArray;
    userP.type = 11;
    [self presentViewController:userP animated:YES completion:nil];
    
    
    
//    [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:[NSString stringWithFormat:@"%d",index]];
//    NSMutableArray *data = [NSMutableArray array];
//    for (NSDictionary *dic in self.picArray) {
//        [data addObject:[dic objectForKey:@"sourceImages"]];
//    }
//    self.currentPicArray = data;
//    
//    //启动图片浏览器
//    HZPhotoBrowser *browserVc = [[HZPhotoBrowser alloc] init];
//    //    browserVc.sourceImagesContainerView = self.collectionView; // 原图的父控件
//    browserVc.imageCount = self.currentPicArray.count; // 图片总数
//    browserVc.currentImageIndex = (int)index;
//    browserVc.type = 0;
//    browserVc.delegate = self;
//    browserVc.dataArr = self.picArray;
//    browserVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:browserVc animated:YES completion:nil];
}


- (NSInteger)numberOfViewsInCollectionView:(PSCollectionView *)collectionView
{
    return [self.picArray count];
}


#pragma mark - photorowser
////临时占位图（thumbnail图
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}
////高清原图 （bmiddle图）
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[self.currentPicArray objectAtIndex:index]];
}
//
- (NSString*)photoDrowserWithIndex:(NSInteger)index {
    return [[self.picArray objectAtIndex:index] objectForKey:@"id"];
}


#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)doneLoadingTableViewData{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_collectionView];
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSFileManager *fileManager = [[NSFileManager alloc] init];
            NSString *path1 = [documentDiretory stringByAppendingPathComponent:@"cartoonPrettyImagesList"];
            [fileManager removeItemAtPath:path1 error:nil];
    }
    [self loadData:NO];
}
//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:1.0];
}
#pragma mark - scrollView
//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
//    [self.tableView tableViewDidEndDragging:scrollView];
//}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _lastContentOffset = scrollView.contentOffset.y;
    _beginScrollViewX = scrollView.contentOffset.x;
    
}
//always
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y< _lastContentOffset )
    {//向下拉
        if (!_isSearchArticle) {
            [_lyTabbar hiddenBar:NO animated:YES];
        }
    }
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        
    } else if([scrollView isKindOfClass:[PullPsCollectionView class]]) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {
        [_searchBar resignFirstResponder];
        CGRect frame = _hisRecordView.frame;
        frame.origin.y =  50+60 -self.pictureTagView.contentOffset.y;
        _hisRecordView.frame =frame;
        
        if (_isToTopShow) {
            [_lyTabbar hiddenBar:YES animated:YES];
            if (self.pictureTagView.contentOffset.y <= 1) {
                _goTopBtn.alpha = 0;
            }else{
                _goTopBtn.alpha = 1;
            }
        }
    }
}

//停止拖动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGSize size1 = _collectionView.contentSize;
    CGFloat maximumOffset = size1.height;

    if (scrollView. contentOffset.y >_lastContentOffset ){//向上拉
        [_lyTabbar hiddenBar:YES animated:YES];
    }
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
    } else if([scrollView isKindOfClass:[PullPsCollectionView class]]) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
        if (_collectionView.contentOffset.y>=maximumOffset - KScreenheight ) {
            [self loadData:YES];
        }
    } else if([scrollView isKindOfClass:[UIScrollView class]]) {//用来控制红色滚动条
    }
}
//减速停止
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
}

//上拉加载更多---资讯
-(void)loadData:(BOOL)isOrmore{
    
    if (self.isLoading) {
        return;
    }
    //    if (!_isShowPicTag) {
    if (isOrmore == YES) {
        page++;
        self.isMore = YES;
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"more" id:@"0"];

    } else {
        page = 1;
        self.isMore = NO;
            [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"refresh" id:@"0"];
            [self.picArray removeAllObjects];
    }
    
    self.isLoading = YES;
    
        if (_isShowPicTag) {
            [self loadTagPicData];
        }else{
            [self prettyImageList];
        }
}


#pragma mark - PullingRefreshCollectionViewDelegate

- (NSDate *)pullingTableViewRefreshingFinishedDate
{
    return [NSDate date];
}

#pragma mark - UISearchBarDelegate
//-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
//    _searchTag = _picTagArr;
//    return YES;
//}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [_searchTag removeAllObjects];
    [_searchTagName removeAllObjects];
    [self.pictureTagView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSLog(@"==");
    for (int i = 0; i < _picTagArr.count; i ++) {
        NSRange range = [_picTagArr[i] rangeOfString:searchText options:NSCaseInsensitiveSearch];
        if (range.length != 0) {
            [_searchTag insertObject:_picTagArr[i] atIndex:0];
            [_searchTagName insertObject:[NSString stringWithFormat:@"%d",i] atIndex:0];
        }
    }
    
    self.pictureTagView.contentSize = CGSizeMake(0, (_searchTag.count/3+1)*45+100+160);
    CGFloat joinComicWidth = KScreenWidth/3;
    for(int i = 0; i < _searchTag.count; i++) {
        UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
        
        joinComic.frame = CGRectMake(6 + joinComicWidth * (i%3), 45 * (i/3)+7 +_hisRecordView.frame.size.height+60, joinComicWidth - 10, 35);
        
        joinComic.backgroundColor = [UIColor whiteColor];
        joinComic.layer.cornerRadius = 4.0;
        joinComic.layer.borderWidth = 1.0;
        joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [joinComic setTitle:[NSString stringWithFormat:@"%@",_searchTag[i]] forState:UIControlStateNormal];
        [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        joinComic.tag = 100+[_searchTagName[i] intValue];
        [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
        [self.pictureTagView addSubview:joinComic];
    }
    if (searchText.length == 0) {
        [self.pictureTagView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
       
        CGFloat joinComicWidth = KScreenWidth/3;
        int numCount = _picTagArr.count;
        if (numCount > 100) {
            numCount = 100;
        }
        self.pictureTagView.contentSize = CGSizeMake(0, (numCount/3+1)*45+100+160);
        for(int i = 0; i < numCount; i++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            joinComic.frame = CGRectMake(6 + joinComicWidth * (i%3), 45 * (i/3)+7 +_hisRecordView.frame.size.height+60, joinComicWidth - 10, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",_picTagArr[i]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            joinComic.tag = 100+i;
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [self.pictureTagView addSubview:joinComic];
        }
    }
}
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [_searchBar resignFirstResponder];
//    [self.pictureTagView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
//
//    self.pictureTagView.contentSize = CGSizeMake(0, (_picTagArr.count/3+1)*45+100+160);
//    CGFloat joinComicWidth = KScreenWidth/3;
//    for(int i = 0; i < _picTagArr.count; i++) {
//        UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
//        joinComic.frame = CGRectMake(6 + joinComicWidth * (i%3), 45 * (i/3)+7 +_hisRecordView.frame.size.height+60, joinComicWidth - 10, 35);
//        joinComic.backgroundColor = [UIColor whiteColor];
//        joinComic.layer.cornerRadius = 4.0;
//        joinComic.layer.borderWidth = 1.0;
//        joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
//        joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
//        [joinComic setTitle:[NSString stringWithFormat:@"%@",_picTagArr[i]] forState:UIControlStateNormal];
//        [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
//        joinComic.tag = 100+i;
//        [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
//        [self.pictureTagView addSubview:joinComic];
//    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [_searchBar resignFirstResponder];
    [self.pictureTagView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    self.pictureTagView.contentSize = CGSizeMake(0, (100/3+1)*45+100+160);//_picTagArr.count
    CGFloat joinComicWidth = KScreenWidth/3;
    for(int i = 0; i < _picTagArr.count; i++) {
        UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
        joinComic.frame = CGRectMake(6 + joinComicWidth * (i%3), 45 * (i/3)+7 +_hisRecordView.frame.size.height+60, joinComicWidth - 10, 35);
        joinComic.backgroundColor = [UIColor whiteColor];
        joinComic.layer.cornerRadius = 4.0;
        joinComic.layer.borderWidth = 1.0;
        joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
        [joinComic setTitle:[NSString stringWithFormat:@"%@",_picTagArr[i]] forState:UIControlStateNormal];
        [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        joinComic.tag = 100+i;
        [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
        [self.pictureTagView addSubview:joinComic];
    }
}

#pragma mark - 加载数据
- (void)prettyImageList {
    
    [self.hud show:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_PRETTYIMAGESLIST,@"r",
                                    [NSString stringWithFormat:@"%d",size],@"size",
                                    @"1",@"page",nil];
    if (self.isMore) {
        if (self.picArray.count) {
            [exprame setObject:[[self.picArray objectAtIndex:self.picArray.count -1] objectForKey:@"id"] forKey:@"lastID"];
        }
    }
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(prettyImageListFinish:) method:GETDATA];
}
-(void)misTopView{
    [self.topShowView hide];
}
- (void)prettyImageListFinish:(NSDictionary*)dic {
    self.isLoading = NO;
    [self.hud hide:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        _netImageView.alpha = 0;
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.picArray addObject:item];
        }
        [self.collectionView reloadData];
        if (![self.picArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else{
            self.contentStatusLabel.hidden = NO;
        }
        if (!self.isMore) {//图片总数
            if (self.picCount) {
                if ([[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue] <= [self.picCount intValue]){

                }else{
                    int i = [[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue]-[self.picCount intValue];
                    self.topShowView.showInfo = [NSString stringWithFormat:@"美图%d张参上~",i];
                    [self.topShowView show];
                    [self performSelector:@selector(misTopView) withObject:nil afterDelay:2.5];
                }
            }
            self.picCount = [[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"];
        } else {
            if ([[dic objectForKey:@"results"] count] == 0 ) {
                self.bottomStatusLabel.text = @"图都被你啃光啦~";
                [self bottomShow];
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"ISCACHEPRETTYIMAGELISTDATA"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }else{
        if (self.picArray.count == 0) {
            [_refreshBtn1 removeFromSuperview];
            _netImageView.frame = CGRectMake(KScreenWidth, 0, KScreenWidth, KScreenheight - 64 - 40);
            _netImageView.alpha = 0;
            
            _refreshBtn2 = [CustomTool createBtn];
            [_netImageView addSubview:_refreshBtn2];
            [_refreshBtn2 addTarget:self action:@selector(prettyImageList) forControlEvents:UIControlEventTouchUpInside];
        }else{
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2-45, KScreenheight-80-64, 90, 40)];
            label.backgroundColor = [UIColor grayColor];
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = 6;
            label.textColor = [UIColor blackColor];
            label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            label.alpha = 1;
            label.text = @"木有网啊";
            label.textAlignment = 1;
            [self.view addSubview:label];
            [UIView animateWithDuration:1 animations:^{
                label.alpha = 0;
            }];
            label = nil;
        }
    }
    
    _collectionView.pullLastRefreshDate = [NSDate date];
    _collectionView.pullTableIsRefreshing = NO;
    _collectionView.pullTableIsLoadingMore = NO;
}


#pragma mark - 美图标签

-(void)createPictureTagView{
    //----top
    self.pictureTopView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 50)];
    self.pictureTopView.backgroundColor = [UIColor whiteColor];
    UIButton *allButton = [[UIButton alloc]initWithFrame:CGRectMake(5, 10, 60, 35)];
    allButton.backgroundColor = [UIColor whiteColor];
    allButton.layer.cornerRadius = 4.0;
    allButton.layer.borderWidth = 1.0;
    allButton.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
    allButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [allButton setTitle:@"全部" forState:UIControlStateNormal];
    [allButton addTarget:self action:@selector(showAllPicture) forControlEvents:UIControlEventTouchUpInside];
    [allButton setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
    [self.pictureTopView addSubview:allButton];
    
    _picTitleView = [[UIView alloc]initWithFrame:CGRectMake(70, 0, KScreenWidth-130, 50)];
    _picTitleView.backgroundColor = [UIColor whiteColor];
    _picTitleView.alpha = 1;
    [self.pictureTopView addSubview:_picTitleView];
    
    _goTopBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-55, KScreenheight-44 - 52 - 64-6, 50, 50)];
    _goTopBtn.alpha = 0;
    [_goTopBtn setImage:[UIImage imageNamed:@"top"] forState:UIControlStateNormal];
    [_goTopBtn addTarget:self action:@selector(goToTop:) forControlEvents:UIControlEventTouchUpInside];
    [self.scrollView addSubview:_goTopBtn];
    
    UIButton *moreTagButton = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth - 60, 10, 60, 35)];
    moreTagButton.backgroundColor = [UIColor whiteColor];
    moreTagButton.layer.cornerRadius = 4.0;
    moreTagButton.tag = 10001;
    [moreTagButton setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];//shouqiDetail
    [moreTagButton addTarget:self action:@selector(showAllPicTag:) forControlEvents:UIControlEventTouchUpInside];
    [self.pictureTopView addSubview:moreTagButton];
    
//------tag
    self.pictureTagView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, -(ScreenSizeHeight -60), ScreenSizeWidth,ScreenSizeHeight -60)];
    self.pictureTagView.backgroundColor = [UIColor whiteColor];
    self.pictureTagView.contentSize = CGSizeMake(0, ScreenSizeHeight+100+60);
    self.pictureTagView.delegate = self;
    
    [self createSearchTagView];
    [self createHisRecordView];
    
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/getLabelList",@"r",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(createButtonForPicture:) method:GETDATA];
    
}
-(void)goToTop:(UIButton *)btn{
    [self.pictureTagView setContentOffset:CGPointMake(0, 0) animated:YES];
}
-(void)showAllPicture{
    [self.collectionView setContentOffset:CGPointMake(0, 0) animated:NO];
    [_searchBar resignFirstResponder];
    _isShowPicTag = NO;
    UIButton *btn = (UIButton *)[self.pictureTopView viewWithTag:10001];
    //    [self.scrollView bringSubviewToFront:_collectionView];
    //    [self.scrollView bringSubviewToFront:_twoBtnView];
    _goTopBtn.alpha = 0;
    //    _twoBtnView.alpha = 1;
    btn.selected = !btn.selected;
    btn.transform = CGAffineTransformMakeRotation(0);
    [UIView animateWithDuration:0.1 animations:^{
        _searchTagView.alpha = 0;
        _picTitleView.alpha = 1;
        self.pictureTagView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
        _hisRecordView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height - _hisRecordView.bounds.size.height, _hisRecordView.bounds.size.width, _hisRecordView.bounds.size.height);
    }];
    
    
    
    if (_selectBtnTag) {
        UIButton *button = (UIButton *)[self.pictureTagView viewWithTag:_selectBtnTag];
        button.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        [button setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        
        UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:_selectBtnTag];
        button1.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        [button1 setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        
        UIButton *button2 = (UIButton *)[_hisRecordView viewWithTag:_selectBtnTag];
        button2.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        [button2 setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        
    }    //    [_collectionView setContentOffset:CGPointMake(0, 41) animated:YES];
    _selectBtnTag = nil;
    [self doneLoadingTableViewData];
}

-(void)showAllPicTag:(UIButton *)btn{
    
    [_searchBar resignFirstResponder];
    if(btn.selected){
        //        [self.scrollView bringSubviewToFront:_collectionView];
        //        [self.scrollView bringSubviewToFront:_twoBtnView];
        [[LogHelper shared] writeToFilefrom_page:@"itl" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        _goTopBtn.alpha = 0;
        [UIView animateWithDuration:0.3 animations:^{
            _picTitleView.alpha = 1;
            btn.transform = CGAffineTransformMakeRotation(0);
            self.pictureTagView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
            _hisRecordView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height - _hisRecordView.bounds.size.height, _hisRecordView.bounds.size.width, _hisRecordView.bounds.size.height);
            _searchTagView.alpha = 0;
        }];
        [_lyTabbar hiddenBar:NO animated:YES];
        //        _twoBtnView.alpha = 1;
        _isToTopShow = NO;
        
        //        [btn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
    }else{
        //        [self.scrollView bringSubviewToFront:self.pictureTagView];
        
        //        _goTopBtn.alpha = 1;
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"itl" to_section:@"i" to_step:@"l" type:@"" id:[NSString stringWithFormat:@"%d",0]];
        _isToTopShow = YES;
        self.pictureTagView.alpha = 1;
        [UIView animateWithDuration:0.3 animations:^{
            _picTitleView.alpha = 0;
            _searchTagView.alpha = 1;
            btn.transform = CGAffineTransformMakeRotation(-(M_PI-0.000001));
            _hisRecordView.frame = CGRectMake(0, 50+60, ScreenSizeWidth, _hisRecordView.bounds.size.height);
            self.pictureTagView.frame = CGRectMake(0, 50, ScreenSizeWidth,ScreenSizeHeight -60);
            
        }];
        //        _twoBtnView.alpha = 0;
        [_lyTabbar hiddenBar:YES animated:YES];
        //        [btn setImage:[UIImage imageNamed:@"shouqiDetail"] forState:UIControlStateNormal];
        [self.scrollView bringSubviewToFront:_searchTagView];
        [self.scrollView bringSubviewToFront:_goTopBtn];
    }
    btn.selected = !btn.selected;
}
-(void)createSearchTagView{
    _searchTagView = [[UIView alloc]initWithFrame:CGRectMake(0, 50, ScreenSizeWidth, 60)];
    _searchTagView.backgroundColor = [UIColor whiteColor];
    _searchTagView.alpha = 0;
    [self.scrollView addSubview:_searchTagView];

    
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(10, 10, KScreenWidth-60-5,30)];
    self.searchBar.layer.masksToBounds = YES;
    self.searchBar.layer.cornerRadius = 7;
    self.searchBar.layer.borderWidth = 0.7;
    self.searchBar.layer.borderColor = RGBACOLOR(228, 169, 180, 1.0).CGColor;
    self.searchBar.tintColor = RGBACOLOR(170, 170, 170, 1.0);
    if (IsIOS7) {
        self.searchBar.barTintColor = RGBACOLOR(170, 170, 170, 1.0);
    }
    
    self.searchBar.keyboardType = UIKeyboardTypeDefault;
    self.searchBar.placeholder = @"搜索内容";
    self.searchBar.delegate = self;
    self.searchBar.backgroundImage = [self imageWithColor:[UIColor clearColor] size:self.searchBar.bounds.size];
    [_searchTagView addSubview:self.searchBar];
    
//    self.backBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-60, -40, 60, 30)];
//    [self.backBtn setTitle:@"取消" forState:UIControlStateNormal];
//    [self.backBtn setTitleColor:RGBACOLOR(100, 100, 100, 1.0) forState:UIControlStateNormal];
//    self.backBtn.titleLabel.textAlignment = 1;
//    [self.backBtn addTarget:self action:@selector(misSearchView) forControlEvents:UIControlEventTouchUpInside];
//    [self.searchView addSubview:self.backBtn];

    
    
//    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(10, 10, ScreenSizeWidth-80, 40)];
////    _searchBar.layer.borderWidth = 1.0;
////    _searchBar.layer.borderColor = RGBACOLOR(148, 148, 148, 1.0).CGColor;
////    _searchBar.layer.masksToBounds = YES;
////    _searchBar.layer.cornerRadius = 8.0;
//    _searchBar.tintColor = RGBACOLOR(237, 237, 237, 1.0);
//    _searchBar.backgroundColor = RGBACOLOR(255, 255, 255, 1);
//    _searchBar.backgroundImage = [self imageWithColor:RGBACOLOR(237, 237, 237, 1.0) size:_searchBar.bounds.size];
//    _searchBar.delegate = self;
//    [_searchTagView addSubview:_searchBar];
    
    _searBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-60, 10, 60, 30)];
    [_searBtn setTitle:@"取消" forState:UIControlStateNormal];
    [_searBtn setTitleColor:RGBACOLOR(100, 100, 100, 1.0) forState:UIControlStateNormal];
    [_searBtn addTarget:self action:@selector(searbtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [_searchTagView addSubview:_searBtn];
}
-(void)searbtnClicked:(UIButton *)btn{
    if ([btn.currentTitle isEqualToString:@"取消"]) {
        _searchBar.text = @"";
        [self searchBar:_searchBar textDidChange:@""];
    }else{
        [_searchBar resignFirstResponder];
    }
   
}
//取消searchbar背景色
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    [self setSearchTextFieldBackgroundColor:RGBACOLOR(250, 250, 250, 1.0)];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
//修改searchbar输入框的颜色
- (void)setSearchTextFieldBackgroundColor:(UIColor *)backgroundColor
{
    UIView *searchTextField = nil;
    if (IOS7_OR_LATER) {
                 // 经测试, 需要设置barTintColor后, 才能拿到UISearchBarTextField对象
        _searchBar.barTintColor = [UIColor whiteColor];
        searchTextField = [[[_searchBar.subviews firstObject] subviews] lastObject];
    } else { // iOS6以下版本searchBar内部子视图的结构不一样
        for (UIView *subView in _searchBar.subviews) {
            if ([subView isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                searchTextField = subView;
            }
        }
    }
     searchTextField.backgroundColor = backgroundColor;
}
- (void)createHisRecordView{
    //拿本地的文件
    //获取本地沙盒路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    _plistPath = [documentsPath stringByAppendingPathComponent:@"pictureHistoricalRecords.plist"];
    NSLog(@"plistPath = %@",_plistPath);
    NSFileManager *fm = [NSFileManager defaultManager];
    if (![fm fileExistsAtPath:_plistPath]) {
        [fm createFileAtPath:_plistPath contents:nil attributes:nil];
        NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
        [arr writeToFile:_plistPath atomically:YES];
    }
    
    //创建历史视图
    NSMutableArray *pictureRecordArray = [NSMutableArray arrayWithContentsOfFile:_plistPath];
    _hisRecordView = [[UIView alloc]init];
    if (pictureRecordArray.count != 0) {
        if (pictureRecordArray.count%3 != 0) {
            _hisRecordView.frame = CGRectMake(0, 60, KScreenWidth, (pictureRecordArray.count/3+1)*43 + 30);
        }else{
            _hisRecordView.frame = CGRectMake(0, 60, KScreenWidth, (pictureRecordArray.count/3)*43 + 30);
        }
        _hisRecordView.backgroundColor = RGBACOLOR(237, 237, 237, 1.0);
        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 20)];
        nameLabel.backgroundColor = RGBACOLOR(237, 237, 237, 1.0);
        nameLabel.font = [UIFont systemFontOfSize:12];
        nameLabel.textColor = RGBACOLOR(172, 172, 172, 1.0);
        nameLabel.text = @"  历史";
        [_hisRecordView addSubview:nameLabel];
        
        for (int i = 0; i < pictureRecordArray.count ; i ++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            joinComic.frame = CGRectMake(6 + KScreenWidth/3 * (i%3), 45 * (i/3)+7 + 15, KScreenWidth/3 - 10, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",pictureRecordArray[i][@"searchName"]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            joinComic.tag = [pictureRecordArray[i][@"tagId"] intValue];
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [_hisRecordView addSubview:joinComic];
        }
    }
}

-(void)createButtonForPicture:(NSDictionary *)dict{
    
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        NSArray *picTagArr = dict[@"results"];
        _picTagArr = picTagArr;
        self.pictureTagView.contentSize = CGSizeMake(0, (100/3+1)*45+100+160);//picTagArr.count
        CGFloat joinComicWidth = KScreenWidth/3;
        
        for (int i = 0; i < 3; i ++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            joinComic.frame = CGRectMake(5 + (KScreenWidth-130)/3 * (i%4), 40 * (i/3)+10, (KScreenWidth-130)/3 - 7, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",picTagArr[i]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            //            joinComic.tag = [picTagArr[i][@"id"] integerValue];
            joinComic.tag = 100+i;
            [_picHeadViewTagArr addObject:[NSString stringWithFormat:@"%li",(long)joinComic.tag]];
            [_picTopTagArr addObject:[NSString stringWithFormat:@"%li",(long)joinComic.tag]];
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [_picTitleView addSubview:joinComic];
            if (i == 2) {
                _thirdBtnName = [NSString stringWithFormat:@"%@",picTagArr[i]];
            }
        }
        int numCount = picTagArr.count;
        if (numCount>100) {
            numCount = 100;
        }
        for(int i = 0; i < numCount; i++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            
            joinComic.frame = CGRectMake(6 + joinComicWidth * (i%3), 45 * (i/3)+7 +_hisRecordView.frame.size.height+60, joinComicWidth - 10, 35);

            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",picTagArr[i]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            joinComic.tag = 100+i;
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [self.pictureTagView addSubview:joinComic];
        }
    }
}


-(void)picWitchYouWant:(UIButton *)btn{
   
    [self.collectionView setContentOffset:CGPointMake(0, 0) animated:NO];
    if (_selectBtnTag!=0) {
        if (btn.tag != _selectBtnTag) {
            [self.picArray removeAllObjects];
            page = 1;
            
            UIButton *button = (UIButton *)[self.pictureTagView viewWithTag:_selectBtnTag];
            button.layer.borderWidth = 1.0;
            [button setTitleColor:RGBACOLOR(155, 155, 155, 1.0) forState:UIControlStateNormal];
            button.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
            
            UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:_selectBtnTag];
            button1.layer.borderWidth = 1.0;
            [button1 setTitleColor:RGBACOLOR(155, 155, 155, 1.0) forState:UIControlStateNormal];
            button1.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1.0].CGColor;
        }
    }else{
        [self.picArray removeAllObjects];
    }
    if (!_selectBtnTag) {
        [self.picArray removeAllObjects];
    }
    BOOL isInTopArr = false;
    for (int i = 0; i < 3; i ++) {
        if (btn.tag == [_picHeadViewTagArr[i] intValue]) {//btn.tag == [_picHeadViewTagArr[i] intValue]
            UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:btn.tag];
            [button1 setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
            button1.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
            isInTopArr = YES;
        }
    }
    if (isInTopArr) {
        UIButton *button1 = (UIButton *)[self.pictureTagView viewWithTag:btn.tag];
        [button1 setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
        button1.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
    }else {
        UIButton *button1 = (UIButton *)[_picTitleView viewWithTag:[_picHeadViewTagArr[2] intValue]];
        [_picHeadViewTagArr removeLastObject];
        [_picHeadViewTagArr addObject:[NSString stringWithFormat:@"%ld",btn.tag]];
        [button1 setTitle:btn.currentTitle forState:UIControlStateNormal];
        [button1 setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
        button1.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
        button1.tag = btn.tag;
    }
    _picBtnTagName = [NSString stringWithFormat:@"%@",btn.currentTitle];
    btn.layer.borderWidth = 1.0;
    [btn setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
    btn.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
    _selectBtnTag = (int)btn.tag;
    [self loadTagPicData];
   
//写入文件--并重新创建子视图
    NSMutableArray *pictureRecordArray = [NSMutableArray arrayWithContentsOfFile:_plistPath];
    NSDictionary *searchDic;
    searchDic = @{@"tagId":[NSString stringWithFormat:@"%ld",btn.tag],@"searchName":_picBtnTagName};
    
    for (int i = 0; i < pictureRecordArray.count; i ++) {
        NSDictionary *dic = pictureRecordArray[i];
        if ([dic[@"searchName"] isEqualToString:_picBtnTagName]) {
            [pictureRecordArray removeObjectAtIndex:i];
        }
    }
    if (pictureRecordArray.count <6) {
        [pictureRecordArray insertObject:searchDic atIndex:0];
        [pictureRecordArray writeToFile:_plistPath atomically:YES];
    }else{
        [pictureRecordArray removeObjectAtIndex:5];
        [pictureRecordArray insertObject:searchDic atIndex:0];
        [pictureRecordArray writeToFile:_plistPath atomically:YES];
    }
    
    [_hisRecordView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (pictureRecordArray.count%3 != 0) {
        CGRect frame = _hisRecordView.frame;
        frame.size.height = (pictureRecordArray.count/3+1)*43 + 30;
        _hisRecordView.frame = frame;
    }else{
        CGRect frame = _hisRecordView.frame;
        frame.size.height = (pictureRecordArray.count/3)*43 + 30;
        _hisRecordView.frame = frame;
    }
    
    if (pictureRecordArray.count == 1 || pictureRecordArray.count == 4) {
        [self.pictureTagView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        int numCount = _picTagArr.count;
        if (numCount>100) {
            numCount = 100;
        }
        for(int i = 0; i < numCount; i++) {
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            
            joinComic.frame = CGRectMake(6 + KScreenWidth/3 * (i%3), 45 * (i/3)+7 +_hisRecordView.frame.size.height+60, KScreenWidth/3 - 10, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 4.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
            [joinComic setTitle:[NSString stringWithFormat:@"%@",_picTagArr[i]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            joinComic.tag = 100+i;
            if (joinComic.tag == _selectBtnTag) {
                [joinComic setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
                joinComic.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
            }
            [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
            [self.pictureTagView addSubview:joinComic];
        }
    }
    
    _hisRecordView.backgroundColor = RGBACOLOR(237, 237, 237, 1.0);
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 20)];
    nameLabel.backgroundColor = RGBACOLOR(237, 237, 237, 1.0);
    nameLabel.font = [UIFont systemFontOfSize:12];
    nameLabel.textColor = RGBACOLOR(172, 172, 172, 1.0);
    nameLabel.text = @"  历史";
    [_hisRecordView addSubview:nameLabel];
    for (int i = 0; i < pictureRecordArray.count ; i ++) {
        UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
        joinComic.frame = CGRectMake(6 + KScreenWidth/3 * (i%3), 45 * (i/3)+7 + 15, KScreenWidth/3 - 10, 35);
        joinComic.backgroundColor = [UIColor whiteColor];
        joinComic.layer.cornerRadius = 4.0;
        joinComic.layer.borderWidth = 1.0;
        joinComic.titleLabel.font = [UIFont systemFontOfSize:14.0];
        if (i == 0) {
            [joinComic setTitleColor:RGBACOLOR(212, 150, 150, 1.0) forState:UIControlStateNormal];
            joinComic.layer.borderColor = [UIColor colorWithRed:212/255.0 green:150/255.0 blue:150/255.0 alpha:1.0].CGColor;
        }else{
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        }
        [joinComic setTitle:[NSString stringWithFormat:@"%@",pictureRecordArray[i][@"searchName"]] forState:UIControlStateNormal];
       
        joinComic.tag = [pictureRecordArray[i][@"tagId"] intValue];
        [joinComic addTarget:self action:@selector(picWitchYouWant:) forControlEvents:UIControlEventTouchUpInside];
        [_hisRecordView addSubview:joinComic];
    }
    
//--------------------------------
    
    
    
}

-(void)loadTagPicData{
    [self.hud show:YES];
    [[LogHelper shared] writeToFilefrom_page:@"itl" from_section:@"i" from_step:@"l" to_page:@"il" to_section:@"i" to_step:@"l" type:@"tag" id:[NSString stringWithFormat:@"%d",0] detail:[NSString stringWithFormat:@"{'tag':'%@'}",_picBtnTagName]];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/list",@"r",[NSString stringWithFormat:@"%d",page],@"page",[NSString stringWithFormat:@"%d",size],@"size",_picBtnTagName,@"searchLabel",nil];
    //    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                                    (NSString *)API_URL_PRETTYIMAGESLIST,@"r",
    //                                    [NSString stringWithFormat:@"%d",size],@"size",
    //                                    @"1",@"page",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(showTagPic:) method:GETDATA];
}
-(void)showTagPic:(NSDictionary *)dict{
    [self.hud hide:YES];
    
    //    [self.scrollView bringSubviewToFront:_collectionView];
    //    [self.scrollView bringSubviewToFront:_twoBtnView];;
    //    _twoBtnView.alpha = 1;
    [_searchBar resignFirstResponder];
    _goTopBtn.alpha = 0;
    _searchTagView.alpha = 0;
    [UIView animateWithDuration:0.1 animations:^{
        _picTitleView.alpha = 1;
        self.pictureTagView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height, self.pictureTagView.bounds.size.width, self.pictureTagView.bounds.size.height-6);
        _hisRecordView.frame = CGRectMake(0, -self.pictureTagView.bounds.size.height - _hisRecordView.bounds.size.height, _hisRecordView.bounds.size.width, _hisRecordView.bounds.size.height);
    }];
    self.isLoading = NO;
    UIButton *btn = (UIButton *)[self.pictureTopView viewWithTag:10001];
    
    //    [btn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
    btn.selected = !btn.selected;
    btn.transform = CGAffineTransformMakeRotation(0);
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        _isShowPicTag = YES;
        NSArray *picarr = dict[@"results"];
        for (NSDictionary *item in picarr ) {
            [self.picArray addObject:item];
        }
        [self.collectionView reloadData];
    }
}

#pragma mark -
- (void)uploadSuccess{
    
//    _showNoMore = NO;
    UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-100, ScreenSizeHeight/2 - 45, 200, 45)];
    tipView.text = @"发布成功";
    tipView.layer.masksToBounds = YES;
    tipView.textAlignment = 1;
    tipView.textColor = RGBACOLOR(229, 96, 115, 1.0);
    tipView.layer.cornerRadius = 22.5;
    tipView.backgroundColor = [UIColor whiteColor];
    [self.navigationController.view addSubview:tipView];
//    [self.view bringSubviewToFront:tipView];
    [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:2.0];
//    alert = [[UIAlertView alloc] initWithTitle:nil message:@"发布成功" delegate:self cancelButtonTitle:nil otherButtonTitles:nil, nil];
//    [alert show];
    
//    NSTimer *timer;
//    
//    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(doTime) userInfo:nil repeats:NO];
    
}
-(void)missTipView:(UILabel *)label{
//    _showNoMore = YES;
    [label removeFromSuperview];
}
//- (void)doTime{
//    [alert dismissWithClickedButtonIndex:0 animated:NO];
//    
//}



@end
