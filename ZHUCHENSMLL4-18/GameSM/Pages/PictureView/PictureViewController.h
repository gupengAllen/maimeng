//
//  HomeViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"
#import "CustomMenuView.h"
#import "PullingRefreshTableView.h"
//#import "PullingRefreshCollectionView.h"
#import "PullPsCollectionView.h"
#import "CustomButton.h"
#import "TopShowView.h"
#import "CustomAdView.h"
#import "LoginAlertView.h"

@interface PictureViewController : BaseTableViewController<CustomMenuViewDelegate, UITableViewDataSource, UITableViewDelegate, PullingRefreshTableViewDelegate, UIScrollViewDelegate, PSCollectionViewDelegate,PSCollectionViewDataSource, PullPsCollectionViewDelegate, UIGestureRecognizerDelegate, EGORefreshTableHeaderDelegate,CustomAdViewDelegate, LoginAlertViewDelegate>
{
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
}
@property (weak, nonatomic) IBOutlet CustomMenuView *menuView;

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIView *searchBgView;
@property (nonatomic, strong) UIView *searchResultBgView;
@property (nonatomic, strong) UIView *searchView;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UITableView *searchTableView;
@property (nonatomic, strong) UITableView *searchResultTableView;

@property (nonatomic, strong) PullPsCollectionView *collectionView;
//@property (nonatomic,strong) PSCollectionView *collectionView;


@property (nonatomic, strong) TopShowView *topShowView;

@end
