//
//  CustomTextField.m
//  bang
//
//  Created by wt on 15/5/13.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self resetView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self resetView];
    }
    return self;
}

- (void)setShowBolder:(BOOL)showBolder {
    _showBolder = showBolder;
    if (_showBolder) {
        self.layer.cornerRadius = 3;
        self.layer.borderColor = [UIColor colorWithRed:251/255.0 green:136/255.0 blue:148/255.0 alpha:1].CGColor;
        self.layer.borderWidth = 0;//1
    } else {
        self.layer.cornerRadius = 0;
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.layer.borderWidth = 0;
    }
}

- (void)resetView {
    self.showBolder = YES;
    [self addTopBtn];
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    CGRect newbounds = CGRectMake(bounds.origin.x+10, bounds.origin.y, bounds.size.width-10, bounds.size.height);
    return newbounds;
}

-(CGRect)editingRectForBounds:(CGRect)bounds
{
    CGRect newbounds = CGRectMake(bounds.origin.x + 10, bounds.origin.y, bounds.size.width -10, bounds.size.height);
    return newbounds;
}

- (void)addTopBtn {
    UIToolbar * top=[[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 30)];
    UIBarButtonItem *speace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    UIBarButtonItem *dimissKey = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissKeyBoard)];

    top.items=[NSArray arrayWithObjects:speace,dimissKey, nil];
    self.inputAccessoryView = top;
}

- (void)dismissKeyBoard {
    [self resignFirstResponder];
}
@end
