//
//  CustomAdView.m
//  GameSM
//
//  Created by 王涛 on 15/8/4.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "CustomAdView.h"
#import "Config.h"
#import "UIImageView+AFNetworking.h"
#import "SpecailDetailViewController.h"
#import "UIView+Addtion.h"
#import "UIButton+AFNetworking.h"

@interface CustomAdView() {
    NSTimer *timer;
    int _pageNum;
}

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CustomPageControl *pageControl;
@property (nonatomic, strong) UILabel *bottomLabel;
@property (nonatomic, strong) UIView *subjectView;

@end

@implementation CustomAdView

- (void)dealloc {
    [self stopTiming];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)createSubjectView{
    self.subjectView = [[UIView alloc]initWithFrame:CGRectMake(0, 180, self.bounds.size.width, 80*_Scale6+18)];//135
    [self addSubview:self.subjectView];
    for (int i = 0; i < _specialListData.count; i ++) {
        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake((ScreenSizeWidth-170*_Scale6*2)/4+self.bounds.size.width/2.0*i, 14, 170*_Scale6, 80*_Scale6)];
        btn.layer.masksToBounds = YES;
        btn.layer.cornerRadius = 10;
        [btn setImageForState:UIControlStateNormal withURL:[NSURL URLWithString:_specialListData[i][@"images"]]];
        btn.tag = 100+i;
        [btn addTarget:self action:@selector(toDetail:) forControlEvents:UIControlEventTouchUpInside];
        [self.subjectView addSubview:btn];
        
//        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, (80*_Scale6-30)/2, 170*_Scale6, 30)];
//        label.text = [NSString stringWithFormat:@"%@",_specialListData[i][@"name"]];
//        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];;
//        label.textColor = [UIColor whiteColor];
//        label.textAlignment = 1;
//        [btn addSubview:label];
        
        
    }
//    UIView *titleView = [[UIView alloc]initWithFrame:CGRectMake(0, 80*_Scale6+18, self.bounds.size.width, 135)];
//    [self.subjectView addSubview:titleView];
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 100, 20)];
//    titleLabel.text = @"热门标签";
//    titleLabel.textColor = RGBACOLOR(237, 103, 117, 1.0);
//    titleLabel.font = [UIFont systemFontOfSize:14.0];
//    [titleView addSubview:titleLabel];
    
//    UIButton *moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(self.bounds.size.width-60, 0, 60, 20)];
//    [moreBtn setTitle:@"更多" forState:UIControlStateNormal];
//    moreBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
//    [moreBtn setTitleColor:RGBACOLOR(148, 148, 148, 1.0) forState:UIControlStateNormal];
//    [moreBtn addTarget:self action:@selector(toMoreView:) forControlEvents:UIControlEventTouchUpInside];
//    [titleView addSubview:moreBtn];
    
//    for (int i=0; i < 4; i ++) {
//        UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake((ScreenSizeWidth - 50*4)/8+self.bounds.size.width/4*i, 30, 50, 50)];
//        btn.backgroundColor = [UIColor cyanColor];
//        btn.layer.masksToBounds = YES;
//        btn.layer.cornerRadius = btn.bounds.size.width/2;
//        btn.tag = 200+i;
//        [btn addTarget:self action:@selector(btnClike:) forControlEvents:UIControlEventTouchUpInside];
//        [titleView addSubview:btn];
//        
//        UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake((ScreenSizeWidth - 50*4)/8+self.bounds.size.width/4*i, 90, 50, 20)];
//        nameLabel.backgroundColor = [UIColor cyanColor];
//        nameLabel.tag = 300+i;
//        [titleView addSubview:nameLabel];
//    }
//    
//    UIView *lineView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 120, self.bounds.size.width, 1)];
//    lineView1.backgroundColor = RGBACOLOR(184, 184, 184, 1.0);
//    [titleView addSubview:lineView1];
//    
//    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 145, self.bounds.size.width, 1)];
//    lineView.backgroundColor = [UIColor whiteColor];
//    [titleView addSubview:lineView];
    
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(10, 130, ScreenSizeWidth-40, 15)];
//    label.text = @"热门资讯";
//    label.textColor = RGBACOLOR(237, 103, 117, 1.0);
//    label.font = [UIFont systemFontOfSize:14.0];
//    [titleView addSubview:label];
    
}

-(void)toDetail:(UIButton *)btn{
    if (btn.tag == 100) {
        [[LogHelper shared] writeToApafrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"mtd" to_section:@"m" to_step:@"l" type:@"topic" id:_specialListData[0][@"id"]];
        
        SpecailDetailViewController *specialV = [[SpecailDetailViewController alloc]init];
        specialV.specialId = [NSString stringWithFormat:@"%@",_specialListData[0][@"id"]];
        [self.viewController.navigationController pushViewController:specialV animated:YES];
        NSLog(@"100");
    }else if (btn.tag == 101){
        [[LogHelper shared] writeToApafrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"mtd" to_section:@"m" to_step:@"l" type:@"topic" id:_specialListData[1][@"id"]];

        SpecailDetailViewController *specialV = [[SpecailDetailViewController alloc]init];
        specialV.specialId = [NSString stringWithFormat:@"%@",_specialListData[1][@"id"]];
        [self.viewController.navigationController pushViewController:specialV animated:YES];
        NSLog(@"101");
    }
}
-(void)toMoreView:(UIButton *)btn{
    NSLog(@"toMore");
}
-(void)btnClike:(UIButton *)btn{
    if (btn.tag == 200) {
        NSLog(@"200");
    }else if (btn.tag == 201){
        NSLog(@"201");
    }else if (btn.tag == 202){
        NSLog(@"202");
    }else if (btn.tag == 203){
        NSLog(@"203");
    }
}

- (void)resetView {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
//    [self createSubjectView];
    
    [self stopTiming];
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 180)];
    [self addSubview:self.scrollView];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    self.scrollView.scrollsToTop = NO;
    

    
    for (int i = 0 ; i < self.infoData.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*self.scrollView.bounds.size.width, 0, self.scrollView.bounds.size.width, 180)];
        [imageView setImageWithURL:[NSURL URLWithString:[[self.infoData objectAtIndex:i] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
        [self.scrollView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchInImage:)];
        [imageView addGestureRecognizer:recognizer];
    }
    
    UIImageView *bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 180 - 36, self.bounds.size.width, 36)];
    bottomView.image = [[UIImage imageNamed:@"main_1"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];
    [self addSubview:bottomView];
    
    self.bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 12, self.bounds.size.width - 80, 24)];
    self.bottomLabel.font = [UIFont boldSystemFontOfSize:14];
    self.bottomLabel.textColor = [UIColor whiteColor];
    self.bottomLabel.backgroundColor = [UIColor clearColor];
    [bottomView addSubview:self.bottomLabel];
    
    if (self.infoData.count) {
        self.bottomLabel.text = [self.infoData.firstObject objectForKey:@"title"];
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width * self.infoData.count, 0)];
        self.pageControl = [[CustomPageControl alloc] initWithFrame:CGRectMake(self.bounds.size.width - (11 * self.infoData.count), 12, (11 * self.infoData.count), 24) withNormalImage:[UIImage imageNamed:@"page.png"] currentImage:[UIImage imageNamed:@"page_a.png"] pageCount:(int)self.infoData.count];
        [self startTiming];
        [self.pageControl resetCurrentPageIndex:_pageNum];
        [self.scrollView setContentOffset:CGPointMake(_pageNum * self.scrollView.bounds.size.width, 0) animated:NO];
        [bottomView addSubview:self.pageControl];
    }
}

- (void)setInfoData:(NSMutableArray *)infoData {
    _infoData = infoData;
    [self resetView];
}
-(void)setSpecialListData:(NSMutableArray *)specialListData{
    _specialListData = specialListData;
    if (_specialListData.count>0) {
         [self createSubjectView];
    }
   
}
#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int index = (int)(scrollView.contentOffset.x / scrollView.bounds.size.width);
//    self.pageControl.currentPage = index;
    [self.pageControl resetCurrentPageIndex:index];
    self.bottomLabel.text = [[self.infoData objectAtIndex:self.pageControl.currentIndex] objectForKey:@"title"];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self stopTiming];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self startTiming];
}

- (void)touchInImage:(UITapGestureRecognizer*)recognizer {
    if (_delegate && [_delegate respondsToSelector:@selector(customAdViewSelectWithIndex:)]) {
        [_delegate customAdViewSelectWithIndex:self.pageControl.currentIndex];
    }
}

- (void)scrollToNext {
    int index = self.pageControl.currentIndex;
    if (index < self.infoData.count - 1) {
        index++;
    } else {
        index = 0;
    }
    if (index) {
        [self.scrollView setContentOffset:CGPointMake(index * self.scrollView.bounds.size.width, 0) animated:YES];
    } else
        [self.scrollView setContentOffset:CGPointMake(index * self.scrollView.bounds.size.width, 0) animated:NO];
    _pageNum = index;
}

#pragma mark - 计时
- (void)startTiming {
    [self stopTiming];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollToNext) userInfo:nil repeats:YES];
}

- (void)stopTiming {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}
@end
