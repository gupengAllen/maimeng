//
//  CircleView.h
//  StudyProject
//
//  Created by wt on 15/4/24.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleView : UIView

- (id)initWithFrame:(CGRect)frame withAngle:(float)angle;

@end
