//
//  SingleImageView.m
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SingleImageView.h"

#import "Config.h"
#import "UIImageView+WebCache.h"

#pragma mark -定义宏常量
#define kImgViewCount 3

#define kImgZoomScaleMin 1
#define kImgZoomScaleMax 3

@interface SingleImageView (){
    
    UIScrollView *_scrCenter;
    CGSize imageSize;
}

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, assign) float width;
@property (nonatomic, assign) float height;

@property (nonatomic, assign) float picWidth;
@property (nonatomic, assign) float picHeight;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, assign) UIDeviceOrientation orientation;
@property (nonatomic, assign) BOOL isDoubleTap;
@end

@implementation SingleImageView

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (id)initWithFrame:(CGRect)frame withSourceData:(NSMutableArray *)imgSource withIndex:(NSInteger)index{
    
    self = [super initWithFrame:frame];
    if (self) {
        self.dataArray = imgSource;
        // 初始化控件属性
        self.width = ScreenSizeWidth;
        self.height = ScreenSizeHeight;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
        
        [self initScrollView];
        
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [self addSubview:self.indicatorView];
        self.indicatorView.center = self.center;
        self.orientation = [UIDevice currentDevice].orientation;
        
    }
    return self;
}

#pragma mark -初始化控件
- (void)initScrollView{
    
    // 设置代理
    self.delegate = self;
    
    // 不显示滚动条
    self.showsHorizontalScrollIndicator = YES;
    self.showsVerticalScrollIndicator = YES;
    
    // 设置分页显示
    self.pagingEnabled = YES;
    
    //设置背景颜色
    self.backgroundColor = [UIColor blackColor];
    
    self.maximumZoomScale = kImgZoomScaleMax;
    self.minimumZoomScale = kImgZoomScaleMin;
    
    // 构建展示组
    [self performSelector:@selector(initImgViewDic) withObject:nil afterDelay:0];
}


// 通过创建展示板
- (UIImageView *)creatImageView{
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    return imgView;
}

// 通过位置创建展示板
- (UIScrollView *)scrollViewWithPositionWithImgView:(UIImageView *)imgView{
    
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    
    [scrollView addSubview:imgView];
    // 设置图片显示样式
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    return scrollView;
}


// 初始化展示板组
- (void)initImgViewDic{
    UIImageView *imgCenter =[self creatImageView];
    self.showImageView = imgCenter;
    
    _scrCenter = [self scrollViewWithPositionWithImgView:self.showImageView];
    
    _scrCenter.maximumZoomScale = kImgZoomScaleMax;
    _scrCenter.minimumZoomScale = kImgZoomScaleMin;
    
    _scrCenter.delegate = self;
    _scrCenter.center = self.center;

    [self addSubview:_scrCenter];
    imageSize = CGSizeMake(self.bounds.size.width, self.bounds.size.height);
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        
        if (self.orientation == UIDeviceOrientationPortrait) {
            
            self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
            self.indicatorView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2);
        } else if (self.orientation == UIDeviceOrientationLandscapeLeft) {
            
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.indicatorView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
        } else if (self.orientation == UIDeviceOrientationLandscapeRight) {
            
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.indicatorView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
        } else {
            
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.indicatorView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
            
        }
    } else {
        self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        self.indicatorView.center = self.center;
    }
    
    [self.indicatorView startAnimating];
    
    [imgCenter setImageWithURL:[NSURL URLWithString:[self.dataArray firstObject]] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType) {
        self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        _scrCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        NSLog(@"error %@",error);
        
        if (image) {
            if ([[UIDevice currentDevice].systemVersion floatValue] < 8) {
                if (self.orientation == UIDeviceOrientationPortrait) {
                    if (image.size.height / image.size.width > ScreenSizeHeight/ ScreenSizeWidth) {
                        
                        imgCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeWidth / image.size.width));
                        
                    } else {
                        imgCenter.frame = CGRectMake(0, (ScreenSizeHeight - (image.size.height * (ScreenSizeWidth / image.size.width)))/2, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeWidth / image.size.width));
                        
                    }
                } else if (self.orientation == UIDeviceOrientationLandscapeLeft) {
                    _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
                    self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
                    
                    if (image.size.height / image.size.width > ScreenSizeHeight/ ScreenSizeWidth) {
                        
                        imgCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, image.size.height * (ScreenSizeHeight / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeHeight / image.size.width));
                        
                    } else {
                        imgCenter.frame = CGRectMake((ScreenSizeHeight - (image.size.width * (ScreenSizeWidth / image.size.height)))/2, 0, (image.size.width * (ScreenSizeWidth / image.size.height)), ScreenSizeWidth);
                        _scrCenter.contentSize = CGSizeMake(0, (image.size.width * (ScreenSizeWidth / image.size.height)));
                    }
                } else if (self.orientation == UIDeviceOrientationLandscapeRight) {
                    
                    _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
                    self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
                    
                    if (image.size.height / image.size.width > ScreenSizeHeight/ ScreenSizeWidth) {
                        
                        imgCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, image.size.height * (ScreenSizeHeight / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeHeight / image.size.width));
                        
                    } else {
                        imgCenter.frame = CGRectMake((ScreenSizeHeight - (image.size.width * (ScreenSizeWidth / image.size.height)))/2, 0, (image.size.width * (ScreenSizeWidth / image.size.height)), ScreenSizeWidth);
                        _scrCenter.contentSize = CGSizeMake(0, imgCenter.frame.size.height);
                    }
                }
                
            } else {
                if (self.orientation == UIDeviceOrientationPortrait) {
                    if (image.size.height / image.size.width > ScreenSizeHeight/ ScreenSizeWidth) {
                        
                        imgCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeWidth / image.size.width));
                        
                    } else {
                        imgCenter.frame = CGRectMake(0, (ScreenSizeHeight - (image.size.height * (ScreenSizeWidth / image.size.width)))/2, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeWidth / image.size.width));
                        //                    _scrCenter.scrollIndicatorInsets = UIEdgeInsetsMake((ScreenSizeHeight - (image.size.height * (ScreenSizeWidth / image.size.width)))/2, 0, 0, (ScreenSizeHeight - (image.size.height * (ScreenSizeWidth / image.size.width)))/2);
                    }
                } else if (self.orientation == UIDeviceOrientationLandscapeLeft) {
                    if (image.size.height / image.size.width > ScreenSizeWidth/ ScreenSizeHeight) {
                        
                        imgCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeWidth / image.size.width));
                        
                    } else {
                        imgCenter.frame = CGRectMake((ScreenSizeWidth - (image.size.width * (ScreenSizeHeight / image.size.height)))/2, 0, (image.size.width * (ScreenSizeHeight / image.size.height)), ScreenSizeHeight);
                        _scrCenter.contentSize = CGSizeMake(0, imgCenter.frame.size.height);
                    }
                } else if (self.orientation == UIDeviceOrientationLandscapeRight) {
                    
                    if (image.size.height / image.size.width > ScreenSizeWidth/ ScreenSizeHeight) {
                        
                        imgCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                        _scrCenter.contentSize = CGSizeMake(0, image.size.height * (ScreenSizeWidth / image.size.width));
                        
                    } else {
                        imgCenter.frame = CGRectMake((ScreenSizeWidth - (image.size.width * (ScreenSizeHeight / image.size.height)))/2, 0, (image.size.width * (ScreenSizeHeight / image.size.height)), ScreenSizeHeight);
                        _scrCenter.contentSize = CGSizeMake(0, imgCenter.frame.size.height);
                    }
                }
            }

        }
        
        
        self.picWidth = image.size.width;
        self.picHeight = image.size.height;
        imageSize = image.size;
        
        [self.indicatorView stopAnimating];
    }];
    imgCenter.userInteractionEnabled = YES;
    
    // 添加双击手势
    UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapClick:)];
    doubleTap.numberOfTapsRequired = 2;
    [self addGestureRecognizer:doubleTap];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapClick:)];
    singleTap.numberOfTapsRequired = 1;
    [self addGestureRecognizer:singleTap];
    [singleTap requireGestureRecognizerToFail:doubleTap];
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        
        _scrCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        
        self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        if (self.orientation == UIDeviceOrientationPortrait) {
//
        } else if (self.orientation == UIDeviceOrientationLandscapeLeft) {
            
            _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            self.showImageView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
        } else if (self.orientation == UIDeviceOrientationLandscapeRight) {
            
            _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            
            self.showImageView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
        } else {
            
            _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
        }
    } else {
        self.showImageView.center = _scrCenter.center;
    }
    
    [_scrCenter setZoomScale:kImgZoomScaleMin animated:YES];
    [_scrCenter setContentSize:CGSizeMake(0, imageSize.height)];
}

#pragma mark -UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (_scrCenter == scrollView) {
//        CGFloat height = self.frame.size.height;
//        CGFloat offsetY = self.contentOffset.y;
        
        if (_scrCenter.zoomScale > kImgZoomScaleMin) {

        } else {
//            [self resetView];
            self.isDoubleTap = NO;
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {

}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    self.showImageView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                            scrollView.contentSize.height * 0.5 + offsetY);
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale {
    if (_scrCenter == scrollView) {
        if (_scrCenter.zoomScale > kImgZoomScaleMin) {
            
        } else {
//            [self resetView];
            self.isDoubleTap = NO;
        }
    }
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.showImageView;
}

#pragma mark -Tap手势处理
- (void)singleTapClick:(UITapGestureRecognizer *)tap{
    if (_sDelegate && [_sDelegate respondsToSelector:@selector(singleImageViewSelectWithSingleTap)]) {
        [_sDelegate singleImageViewSelectWithSingleTap];
    }
}

- (void)doubleTapClick:(UITapGestureRecognizer *)tap{
    //判断当前放大的比例
    if (_scrCenter.zoomScale > kImgZoomScaleMin) {
        //缩小
        
        [_scrCenter setZoomScale:kImgZoomScaleMin animated:YES];
        
        self.isDoubleTap = NO;
        
        [self resetView];
        
    }else{
        //放大
        self.isDoubleTap = YES;
        [_scrCenter setZoomScale:kImgZoomScaleMax animated:YES];
        [UIView animateWithDuration:0.2 animations:^{
            self.showImageView.frame = CGRectMake(0, 0, self.showImageView.frame.size.width, self.showImageView.frame.size.height);
        }];
        [_scrCenter setContentSize:CGSizeMake(self.showImageView.frame.size.width, self.showImageView.frame.size.height)];
    }
}

// 谦让双击放大手势
- (void)requireDoubleGestureRecognizer:(UITapGestureRecognizer *)tep{
    [tep requireGestureRecognizerToFail:[[self gestureRecognizers] lastObject]];
}

- (void)orientChange:(NSNotification *)noti{

    UIDeviceOrientation  orient = [UIDevice currentDevice].orientation;
    self.orientation = orient;
    
    [self resetView];
}

- (void)resetView {
    [_scrCenter setZoomScale:kImgZoomScaleMin animated:YES];
    [_scrCenter setContentSize:CGSizeMake(0, imageSize.height)];
    self.isDoubleTap = NO;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 8.0) {
        _scrCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        
        [_scrCenter setContentOffset:CGPointMake(0, 0)];
        self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        
        if (self.orientation == UIDeviceOrientationPortrait) {
            
            self.indicatorView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2);
            
            if (imageSize.height / imageSize.width > ScreenSizeHeight/ ScreenSizeWidth) {
                
                self.showImageView.frame = CGRectMake(0, 0, ScreenSizeWidth, imageSize.height * (ScreenSizeWidth / imageSize.width));
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeWidth / imageSize.width));
                
            } else {
                self.showImageView.frame = CGRectMake(0, (ScreenSizeHeight - (imageSize.height * (ScreenSizeWidth / imageSize.width)))/2, ScreenSizeWidth, imageSize.height * (ScreenSizeWidth / imageSize.width));
                //            imgCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                //            imgCenter.center = _scrCenter.center;
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeWidth / imageSize.width));
            }
        } else if (self.orientation == UIDeviceOrientationLandscapeLeft) {
            
            self.indicatorView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            if (imageSize.height / imageSize.width > ScreenSizeHeight/ ScreenSizeWidth) {
                
                self.showImageView.frame = CGRectMake(0, 0, ScreenSizeHeight, imageSize.height * (ScreenSizeHeight / imageSize.width));
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeHeight / imageSize.width));
                
            } else {
                self.showImageView.frame = CGRectMake((ScreenSizeHeight - (imageSize.width * (ScreenSizeWidth / imageSize.height)))/2, 0, (imageSize.width * (ScreenSizeWidth / imageSize.height)), ScreenSizeWidth);
                _scrCenter.contentSize = CGSizeMake(self.showImageView.frame.size.width, self.showImageView.frame.size.height);
            }
            
        } else if (self.orientation == UIDeviceOrientationLandscapeRight) {
            
            self.indicatorView.center = CGPointMake(ScreenSizeHeight/2, ScreenSizeWidth/2);
            self.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            _scrCenter.frame = CGRectMake(0, 0, ScreenSizeHeight, ScreenSizeWidth);
            
            if (imageSize.height / imageSize.width > ScreenSizeHeight/ ScreenSizeWidth) {
                
                self.showImageView.frame = CGRectMake(0, 0, ScreenSizeHeight, imageSize.height * (ScreenSizeHeight / imageSize.width));
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeHeight / imageSize.width));
                
            } else {
                self.showImageView.frame = CGRectMake((ScreenSizeHeight - (imageSize.width * (ScreenSizeWidth / imageSize.height)))/2, 0, (imageSize.width * (ScreenSizeWidth / imageSize.height)), ScreenSizeWidth);
                _scrCenter.contentSize = CGSizeMake(0, self.showImageView.frame.size.height);
            }
        }
        
    } else {
        
        _scrCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        
        [_scrCenter setContentOffset:CGPointMake(0, 0)];
        self.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
        
        if (self.orientation == UIDeviceOrientationPortrait) {
            
            self.indicatorView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2);
            if (imageSize.height / imageSize.width > ScreenSizeHeight/ ScreenSizeWidth) {
                
                self.indicatorView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2);
                self.showImageView.frame = CGRectMake(0, 0, ScreenSizeWidth, imageSize.height * (ScreenSizeWidth / imageSize.width));
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeWidth / imageSize.width));
                
            } else {
                self.showImageView.frame = CGRectMake(0, (ScreenSizeHeight - (imageSize.height * (ScreenSizeWidth / imageSize.width)))/2, ScreenSizeWidth, imageSize.height * (ScreenSizeWidth / imageSize.width));
                //            imgCenter.frame = CGRectMake(0, 0, ScreenSizeWidth, image.size.height * (ScreenSizeWidth / image.size.width));
                //            imgCenter.center = _scrCenter.center;
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeWidth / imageSize.width));
            }
        } else if (self.orientation == UIDeviceOrientationLandscapeLeft) {
            self.indicatorView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2);
            if (imageSize.height / imageSize.width > ScreenSizeWidth/ ScreenSizeHeight) {
                
                self.showImageView.frame = CGRectMake(0, 0, ScreenSizeWidth, imageSize.height * (ScreenSizeWidth / imageSize.width));
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeWidth / imageSize.width));
                
            } else {
                self.showImageView.frame = CGRectMake((ScreenSizeWidth - (imageSize.width * (ScreenSizeHeight / imageSize.height)))/2, 0, (imageSize.width * (ScreenSizeHeight / imageSize.height)), ScreenSizeHeight);
                _scrCenter.contentSize = CGSizeMake(self.showImageView.frame.size.width, self.showImageView.frame.size.height);
            }
            
        } else if (self.orientation == UIDeviceOrientationLandscapeRight) {
            
            self.indicatorView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2);
            if (imageSize.height / imageSize.width > ScreenSizeWidth/ ScreenSizeHeight) {
                
                self.showImageView.frame = CGRectMake(0, 0, ScreenSizeWidth, imageSize.height * (ScreenSizeWidth / imageSize.width));
                _scrCenter.contentSize = CGSizeMake(0, imageSize.height * (ScreenSizeWidth / imageSize.width));
                
            } else {
                self.showImageView.frame = CGRectMake((ScreenSizeWidth - (imageSize.width * (ScreenSizeHeight / imageSize.height)))/2, 0, (imageSize.width * (ScreenSizeHeight / imageSize.height)), ScreenSizeHeight);
                _scrCenter.contentSize = CGSizeMake(0, self.showImageView.frame.size.height);
            }
        } else if (self.orientation == UIDeviceOrientationPortraitUpsideDown) {
            
        }

    }
}
@end
