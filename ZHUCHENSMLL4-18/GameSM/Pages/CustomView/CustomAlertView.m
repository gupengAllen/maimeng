//
//  CustomAlertView.m
//  AnXin
//
//  Created by wt on 14-3-27.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import "CustomAlertView.h"
#import "Config.h"

#define kTitleFontSize 17
#define kSubtitleFontSize 14

@interface CustomAlertView ()

@property (nonatomic, copy) NSString *titleText;
@property (nonatomic, copy) NSString *subText;
@property (nonatomic, copy) NSString *leftButtonText;
@property (nonatomic, copy) NSString *rightButtonText;
@property (nonatomic, retain) UIView *bgView;
@property (nonatomic, assign) id<CustomAlertViewDelegate> delegate;
@end

@implementation CustomAlertView

- (void)dealloc {
    _delegate = nil;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message delegate:(id<CustomAlertViewDelegate>)delegate cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles {
    self = [super init];
    if (self) {
//        self.titleText = title;
        self.titleText = @"麦萌君";
        self.subText = message;
        self.leftButtonText = cancelButtonTitle;
        self.rightButtonText = otherButtonTitles;
        self.delegate = delegate;
    }
    return self;
}

-(void)createBackgroundView
{
	self.frame = [[UIScreen mainScreen] bounds];
	self.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.0];
    self.opaque = NO;//设置不透明，优化性能view看为完全不透明，这样绘图系统就可以优化一些绘制操作以提升性能。如果设置
//    为NO，那么绘图系统结合其它内容来处理view。默认情况下，这个属性是YES
	UIWindow *appWindow = [[UIApplication sharedApplication] keyWindow];
	[appWindow addSubview:self];
	
	[UIView animateWithDuration:0.2 animations:^{
		self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
	}];
}

- (void)show {
    [self createBackgroundView];
    self.bgView = [[UIView alloc]initWithFrame:CGRectZero];
    
    [self.bgView setFrame:CGRectMake(0, 0, 260, 110)];
    self.bgView.center = CGPointMake(ScreenSizeWidth/2, ScreenSizeHeight/2 - 50);
//    if (iPhone5) {
//        [self.bgView setFrame:CGRectMake(30, 180, 260, 110)];
//    } else {
//        [self.bgView setFrame:CGRectMake(30, 155, 260, 110)];
//    }
    self.bgView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1];
    
    [self addSubview:self.bgView];
    
    int height = 25;
    if (self.titleText.length > 0) {
        UILabel *tLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, height, self.bgView.frame.size.width-30, 25)];
        
        tLabel.tag = 100;
        tLabel.textColor = [UIColor darkTextColor];
        tLabel.backgroundColor = [UIColor clearColor];
        tLabel.textAlignment = NSTextAlignmentCenter;
        tLabel.font = [UIFont systemFontOfSize:kTitleFontSize];
        [self.bgView addSubview:tLabel];
        tLabel.text = _titleText;
        height += 25;
    }
    
    
    if (self.subText.length > 0) {
        CGSize size = [_subText sizeWithFont:[UIFont systemFontOfSize:kSubtitleFontSize] constrainedToSize:CGSizeMake(self.bgView.frame.size.width - 30, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
        UILabel *tLabel = [[UILabel alloc] init];
        tLabel.numberOfLines = 0;
        if (size.height > 21) {
            tLabel.frame = CGRectMake(15, height, self.bgView.frame.size.width-30, 5 + size.height);
            tLabel.textAlignment = NSTextAlignmentLeft;
        } else {
            tLabel.frame = CGRectMake(15, height, self.bgView.frame.size.width-30, 21);
            tLabel.textAlignment = NSTextAlignmentCenter;
        }
        tLabel.text = _subText;
        tLabel.tag = 100;
        tLabel.textColor = [UIColor darkTextColor];
        tLabel.backgroundColor = [UIColor clearColor];
        tLabel.font = [UIFont systemFontOfSize:kSubtitleFontSize];
        [self.bgView addSubview:tLabel];
        
        height += size.height > 21 ? (5+size.height+10) : 26;
    }
    
    height += 15;
    
    UIButton *buttonLeft = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonLeft.frame = CGRectMake(20, height, 80, 36);
    [buttonLeft setTitle:[NSString stringWithFormat:@"%@",_leftButtonText] forState:UIControlStateNormal];
    [buttonLeft setBackgroundColor:CUSTOMREDCOLOR];
    [buttonLeft setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonLeft.tag = 100;
    [buttonLeft addTarget:self action:@selector(btnWasPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.bgView addSubview:buttonLeft];
    buttonLeft.layer.cornerRadius = 5;
    buttonLeft.layer.masksToBounds = YES;
    buttonLeft.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    
    if (_rightButtonText.length) {
        UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonRight.frame = CGRectMake(self.bgView.frame.size.width/2+(self.bgView.frame.size.width/2-80-20), height, 80, 36);
        buttonRight.tag = 101;
        
        buttonRight.layer.cornerRadius = 5;
        buttonRight.layer.masksToBounds = YES;
        [buttonRight setTitle:[NSString stringWithFormat:@"%@",_rightButtonText] forState:UIControlStateNormal];
        [buttonRight addTarget:self action:@selector(btnWasPressed:) forControlEvents:UIControlEventTouchUpInside];
        [buttonRight setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.bgView addSubview:buttonRight];
        [buttonRight setBackgroundColor:[UIColor lightGrayColor]];
        [buttonLeft setBackgroundColor:[UIColor colorWithRed:236/255.0 green:46/255.0 blue:44/255.0 alpha:1]];
        
        buttonRight.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    } else {
        buttonLeft.frame = CGRectMake((self.bgView.frame.size.width - 80)/2, height, 80, 36);
    }
    
    height += 50;
    
    if (height > 110) {
        self.bgView.frame = CGRectMake(self.bgView.frame.origin.x, self.bgView.frame.origin.y - (height - 110)/2, 260, height);
    } else {
        if ((110 - height) > 30) {
            self.bgView.frame = CGRectMake(self.bgView.frame.origin.x, self.bgView.frame.origin.y + (110 - height)/2, 260, height);
        }
    }
    
    [self.bgView.layer setCornerRadius:5];
    
    [self animateIn];
}

- (void)hide {
    [self animateOut];
}

- (void)btnWasPressed:(UIButton*)button {
    [self animateOut];
    if (_delegate && [_delegate respondsToSelector:@selector(customAlertViewBtnWasSeletecd:withIndex:)]) {
        [_delegate customAlertViewBtnWasSeletecd:self withIndex:button.tag - 100];
    }
}

- (void)animateIn {
	self.bgView.transform = CGAffineTransformMakeScale(0.6, 0.6);
	[UIView animateWithDuration:0.2 animations:^{
		self.bgView.transform = CGAffineTransformMakeScale(1.1, 1.1);
	} completion:^(BOOL finished){
		[UIView animateWithDuration:1.0/15.0 animations:^{
			self.bgView.transform = CGAffineTransformMakeScale(0.9, 0.9);
		} completion:^(BOOL finished){
			[UIView animateWithDuration:1.0/7.5 animations:^{
				self.bgView.transform = CGAffineTransformIdentity;
			}];
		}];
	}];
}

- (void)animateOut{

    [UIView animateWithDuration:0.15 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
	} completion:^(BOOL finished) {
        [self removeFromSuperview];
	}];
}

@end
