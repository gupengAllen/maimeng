//
//  LoginAlertView.h
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginAlertViewDelegate <NSObject>

@optional
- (void)loginAlertViewPressedWithIndex:(int)index ;

@end

@interface LoginAlertView : UIView

- (id)initWithDelegate:(id<LoginAlertViewDelegate>)delegate title:(NSString*)title;

- (void)show;

@end
