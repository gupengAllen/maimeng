//
//  CircleLoadingView.h
//  GameSM
//
//  Created by 王涛 on 15/7/17.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleLoadingView : UIView

- (void)updateAngle:(float)angle;

- (void)startCircleAnimation;

- (void)stopCircleAnimation;

@end
