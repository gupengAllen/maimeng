//
//  CalenderView.m
//  bang
//
//  Created by 王涛 on 15/6/3.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "CalenderView.h"
#import "CustomButton.h"

@interface CalenderView ()

@property (nonatomic, strong) NSCalendar *calendar;

@property (nonatomic, assign) NSCalendarUnit calenderUnit;

@property (nonatomic, strong) NSDate *selectedDate;

@property (nonatomic, strong) NSArray *weekDayNames;

@property (nonatomic, strong) UILabel *monthLabel;

@property (nonatomic, assign) NSInteger weekdayBeginning;

@property (nonatomic, assign) NSInteger monthLength;

@property (nonatomic, strong) NSMutableArray *chooseDays;

@property (nonatomic, strong) NSMutableArray *chooseWeeks;

@property (nonatomic, assign) int currentMonth;

@end

@implementation CalenderView
@synthesize weekdayBeginning=weekdayBeginning;
@synthesize monthLength=monthLength;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        _calenderUnit = NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
        
        NSDateComponents *components = [_calendar components:_calenderUnit fromDate:[NSDate date]];
        components.hour = 0;
        components.minute = 0;
        components.second = 0;
        
        _selectedDate = [_calendar dateFromComponents:components];
        _weekDayNames  = @[@"日",@"一",@"二",@"三",@"四",@"五",@"六"];
        
        self.chooseDays = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)setCalendarDate:(NSDate *)calendarDate {
    _calendarDate = calendarDate;
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    for (UIView *subV in self.subviews) {
        [subV removeFromSuperview];
    }
    
    NSDateComponents *components = [_calendar components:_calenderUnit fromDate:_calendarDate];
    components.day = 1;
    
    NSDate *firstDayOfMonth = [_calendar dateFromComponents:components];
    NSDateComponents *comps = [_calendar components:NSWeekdayCalendarUnit fromDate:firstDayOfMonth];
    
    weekdayBeginning = [comps weekday];  // Starts at 1 on Sunday
    weekdayBeginning -= 1;
    if(weekdayBeginning < 0)
        weekdayBeginning += 7;  // Starts now at 0 on Monday
    
    NSRange days = [_calendar rangeOfUnit:NSDayCalendarUnit
                                    inUnit:NSMonthCalendarUnit
                                   forDate:_calendarDate];
    
    monthLength = days.length;
    NSInteger remainingDays = (monthLength + weekdayBeginning) % 7;
    
    // Frame drawing
    int dayWidth = (self.bounds.size.width - 20)/_weekDayNames.count;
    
    // Previous and next button
    UIButton * buttonPrev = [[UIButton alloc] initWithFrame:CGRectMake(10, 0, dayWidth, dayWidth)];
    [buttonPrev setTitle:@"<" forState:UIControlStateNormal];
    [buttonPrev setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [buttonPrev addTarget:self action:@selector(showPreviousMonth) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buttonPrev];
    
    UIButton * buttonNext = [[UIButton alloc] initWithFrame:CGRectMake(self.bounds.size.width - dayWidth - 10, 0, dayWidth, dayWidth)];
    [buttonNext setTitle:@">" forState:UIControlStateNormal];
    [buttonNext setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [buttonNext addTarget:self action:@selector(showNextMonth) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:buttonNext];
    
        // Month label
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy年MM月"];
    NSString *dateString = [[format stringFromDate:_calendarDate] uppercaseString];
    
    [format setDateFormat:@"MM"];
    self.currentMonth = [[format stringFromDate:_calendarDate] integerValue];

    _monthLabel = [[UILabel alloc] initWithFrame:CGRectMake((self.bounds.size.width - 120)/2, (dayWidth - 20)/2, 120, 20)];
    _monthLabel.font = [UIFont systemFontOfSize:14];
    _monthLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_monthLabel];
    _monthLabel.text = dateString;
        // Day labels
    int weekY = 55;
    int dayHeight = dayWidth * 28 / 42;
    
    __block CGRect frameWeek = CGRectMake(0, weekY, dayWidth, dayHeight);
    [_weekDayNames  enumerateObjectsUsingBlock:^(NSString * dayOfWeekString, NSUInteger idx, BOOL *stop)
     {
         frameWeek.origin.x = 10+(dayWidth*idx);
         UIButton *weekBtn = [[UIButton alloc] initWithFrame:frameWeek];
         [weekBtn setTitle:dayOfWeekString forState:UIControlStateNormal];
         [weekBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
         weekBtn.titleLabel.font = [UIFont systemFontOfSize:14];
         weekBtn.tag = idx + 1000;
//         [weekBtn addTarget:self action:@selector(weekBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
         [self addSubview:weekBtn];
     }];
    
    int originX = 10;
    int originY = weekY + dayHeight;
    // Current month
    for (NSInteger i= 0; i<monthLength; i++)
    {
        components.day = i+1;
        NSInteger offsetX = (dayWidth *((i+weekdayBeginning)%7));
        NSInteger offsetY = (dayHeight *((i+weekdayBeginning)/7));
        CustomButton *button = [[CustomButton alloc ] initWithFrame:CGRectMake(originX+offsetX, originY+offsetY, dayWidth, dayHeight)];
        button.index = i+weekdayBeginning;
        [self configureDayButton:button withDate:[_calendar dateFromComponents:components]];
        [self addSubview:button];
    }
    
    // Previous month
    NSDateComponents *previousMonthComponents = [_calendar components:_calenderUnit fromDate:_calendarDate];
    previousMonthComponents.month --;
    NSDate *previousMonthDate = [_calendar dateFromComponents:previousMonthComponents];
    NSRange previousMonthDays = [_calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:previousMonthDate];
    NSInteger maxDate = previousMonthDays.length - weekdayBeginning;
    for (int i=0; i<weekdayBeginning; i++)
    {
        previousMonthComponents.day = maxDate+i+1;
        NSInteger offsetX = (dayWidth*(i%7));
        NSInteger offsetY = (dayHeight *(i/7));
        CustomButton *button = [[CustomButton alloc ] initWithFrame:CGRectMake(originX+offsetX, originY+offsetY, dayWidth, dayHeight)];
        button.index = i;
        [self configureDayButton:button withDate:[_calendar dateFromComponents:previousMonthComponents]];
        [self addSubview:button];
    }
    
    // Next month
    if(remainingDays == 0)
        return ;
    
    NSDateComponents *nextMonthComponents = [_calendar components:_calenderUnit fromDate:_calendarDate];
    nextMonthComponents.month ++;
    
    for (NSInteger i=remainingDays; i<7; i++)
    {
        nextMonthComponents.day = (i+1)-remainingDays;
        NSInteger offsetX = (dayWidth*((i) %7));
        NSInteger offsetY = (dayHeight *((monthLength+weekdayBeginning)/7));
        CustomButton *button = [[CustomButton alloc ] initWithFrame:CGRectMake(originX+offsetX, originY + offsetY, dayWidth, dayHeight)];
        button.index = (monthLength+weekdayBeginning)+i-remainingDays;
        [self configureDayButton:button withDate:[_calendar dateFromComponents:nextMonthComponents]];
        [self addSubview:button];
    }
}

-(void)configureDayButton:(UIButton *)button withDate:(NSDate*)date
{
    NSDateComponents *components = [_calendar components:_calenderUnit fromDate:date];
    NSString *day;
    if (components.day < 10) {
        day = [NSString stringWithFormat:@" %ld",(long)components.day];
    } else
        day = [NSString stringWithFormat:@"%ld",(long)components.day];
    [button setTitle:day forState:UIControlStateNormal];
    button.tag = [self buttonTagForDate:date];
    
    [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(dateButtonWasPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if([_selectedDate compare:date] == NSOrderedSame)
    {
        // Selected button
        button.layer.borderWidth = 0;
        [button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    }
    else
    {
        // Unselected button
        [button setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    }
    
    NSDateComponents * componentsDateCal = [_calendar components:_calenderUnit fromDate:_calendarDate];
    if (components.month != componentsDateCal.month)
        button.alpha = 0.6f;
}

-(NSInteger)buttonTagForDate:(NSDate *)date
{
    NSDateComponents * componentsDate = [_calendar components:_calenderUnit fromDate:date];
    NSDateComponents * componentsDateCal = [_calendar components:_calenderUnit fromDate:_calendarDate];
    
    if (componentsDate.month == componentsDateCal.month && componentsDate.year == componentsDateCal.year)
    {
        // Both dates are within the same month : buttonTag = day
        return componentsDate.day;
    }
    else
    {
        //  buttonTag = deltaMonth * 40 + day
        NSInteger offsetMonth =  (componentsDate.year - componentsDateCal.year)*12 + (componentsDate.month - componentsDateCal.month);
        return componentsDate.day + offsetMonth*40;
    }
}

-(void)showPreviousMonth
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MM"];

    
    NSDateComponents *components = [_calendar components:_calenderUnit fromDate:_calendarDate];
    components.day = 1;
    components.month --;
    NSDate * prevMonthDate = [_calendar dateFromComponents:components];
    
    if ([[format stringFromDate:prevMonthDate] integerValue] < [[format stringFromDate:[NSDate date]] integerValue]) {
        return;
    }
    
    _calendarDate = prevMonthDate;
    [self setNeedsDisplay];
}

-(void)showNextMonth
{
    NSDateComponents *components = [_calendar components:_calenderUnit fromDate:_calendarDate];
    components.day = 1;
    components.month ++;
    NSDate *nextMonthDate =[_calendar dateFromComponents:components];
    _calendarDate = nextMonthDate;
    [self setNeedsDisplay];
}

- (void)dateButtonWasPressed:(CustomButton*)btn {
//    NSLog(@"%d",btn.tag);
    int day;
    int month;
    if (btn.tag < 0) {
        day = 40 + btn.tag;
        month = self.currentMonth - 1;
        
    } else if (btn.tag > 40) {
        day = btn.tag - 40;
        month = self.currentMonth + 1;
    } else {
        day = btn.tag;
        month = self.currentMonth;
    }
    
    if (month<0) {
        month = 12;
    } else if (month > 12) {
        month = 1;
    }
    if (!btn.selected) {
        [self.chooseDays addObject:[NSString stringWithFormat:@"%d月%d号",month,day]];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        
        btn.selected = YES;
    }
    if (_delegate && [_delegate respondsToSelector:@selector(calenderChooseDays:)]) {
        [_delegate calenderChooseDays:self.chooseDays];
    }
}

- (void)dateButtonWasDeselectPressed:(CustomButton*)btn {
    [btn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
}

- (void)weekBtnPressed:(UIButton*)btn {
    btn.selected = !btn.selected;
    if (btn.selected) {
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    } else {
        [btn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    }
    int index = btn.tag - 1000;
    for (CustomButton *button in self.subviews) {
        if ([button isKindOfClass:[CustomButton class]] && button.index >= 0 && button.index <= 50) {
            if (button.index % 7 == index) {
                if (btn.selected) {
                    [self dateButtonWasPressed:button];
                } else {
                    [self dateButtonWasDeselectPressed:button];
                }
            }
        }
    }
}

@end
