//
//  CustomTextView.h
//  AnXin
//
//  Created by wt on 14-4-11.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CustomTextViewDelegate <NSObject>

- (void)customTextViewFinish:(BOOL)isFinish;

@end


@interface CustomTextView : UITextView

@property (nonatomic, assign) id<CustomTextViewDelegate> CDelegate;

@end
