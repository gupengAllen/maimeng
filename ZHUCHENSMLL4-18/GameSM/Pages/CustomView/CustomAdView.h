//
//  CustomAdView.h
//  GameSM
//
//  Created by 王涛 on 15/8/4.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomView.h"
#import "CustomPageControl.h"

@protocol CustomAdViewDelegate <NSObject>

@optional
- (void)customAdViewSelectWithIndex:(int)index;

@end

@interface CustomAdView : CustomView<UIScrollViewDelegate>

@property (nonatomic, strong) NSMutableArray *infoData;

@property (nonatomic, strong) NSMutableArray *specialListData;

@property (nonatomic, assign) id<CustomAdViewDelegate> delegate;

@end
