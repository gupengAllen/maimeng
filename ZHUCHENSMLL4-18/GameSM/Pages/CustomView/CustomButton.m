//
//  CustomButton.m
//  bang
//
//  Created by wt on 15/5/13.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "CustomButton.h"

@interface CustomButton ()

@property (nonatomic, strong) UILabel *budgeLabel;

@end

@implementation CustomButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self resetView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self resetView];
    }
    return self;
}

- (void)resetView {
    self.layer.cornerRadius = 3;
}

- (void)setShowBudge:(BOOL)showBudge {
    _showBudge = showBudge;
    [self resetShowBudgeView];
}

- (void)setBudegColor:(UIColor *)budegColor {
    _budegColor = budegColor;
    if (_budgeLabel) {
        _budgeLabel.backgroundColor = budegColor;//budegColor
    }
}

- (void)resetBudgeWithNum:(NSString *)budgeNum {
    CGSize size = [budgeNum sizeWithFont:_budgeLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, 20) lineBreakMode:NSLineBreakByWordWrapping];
    if (size.width > 10) {
        _budgeLabel.frame = CGRectMake(_budgeLabel.frame.origin.x, _budgeLabel.frame.origin.y, _budgeLabel.frame.size.width + size.width - 10, _budgeLabel.frame.size.height);
    }
    
    if (budgeNum && [budgeNum integerValue]) {
        _budgeLabel.hidden = NO;
        _budgeLabel.text = budgeNum;
    } else {
        _budgeLabel.text = nil;
        _budgeLabel.hidden = YES;
    }
}

- (void)resetShowBudgeView {
    if (!_budgeLabel) {
//        _budgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width / 2 + 25, (self.bounds.size.height - 20)/2, 30, 20)];
        
        _budgeLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width - 7, -3, 10, 10)];
        _budgeLabel.layer.cornerRadius = _budgeLabel.bounds.size.height / 2;
        _budgeLabel.layer.masksToBounds= YES;
        _budgeLabel.hidden = NO;
        _budgeLabel.textColor = [UIColor whiteColor];
        _budgeLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_budgeLabel];
        _budgeLabel.backgroundColor = [UIColor whiteColor];
    }
    if (_showBudge) {
        self.budgeLabel.hidden = NO;
    } else {
        self.budgeLabel.hidden = YES;
    }
}

@end
