//
//  SingleImageView.h
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SvGifView.h"
#pragma mark -ENUM
typedef NS_ENUM(NSInteger, MRImgLocation) {
    MRImgLocationLeft,
    MRImgLocationCenter,
    MRImgLocationRight,
};

@protocol SingleImageViewDelegate <NSObject>

- (void)singleImageViewSelectWithSingleTap;

@end

@interface SingleImageView : UIScrollView<UIScrollViewDelegate>
{
    NSDictionary* _imgViewDic;   // 展示板组
}

@property (nonatomic, assign) id<SingleImageViewDelegate> sDelegate;
@property (nonatomic, strong) UIImageView *showImageView;

@property(nonatomic ,readonly)MRImgLocation imgLocation;    // 图片在空间中的位置

@property(nonatomic ,assign)NSInteger curIndex;     // 当前显示图片在数据源中的下标

- (id)initWithFrame:(CGRect)frame withSourceData:(NSMutableArray *)imgSource withIndex:(NSInteger)index;

// 谦让双击放大手势
- (void)requireDoubleGestureRecognizer:(UITapGestureRecognizer *)tep;

@end
