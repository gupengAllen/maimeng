//
//  CustomTool.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/12/1.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomTool : NSObject

//创建按钮Btn
+(UIButton *)createBtn;

//创建加载失败视图
+(UIImageView *)createImageView;

//自定义Nav
+(UIView *)createNavView;

//创建Label
+(UILabel *)createLabelWithFrame:(CGRect)frame andType:(float)type andColor:(UIColor *)color;


@end
