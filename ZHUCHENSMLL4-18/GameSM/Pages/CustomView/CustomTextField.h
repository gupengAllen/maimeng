//
//  CustomTextField.h
//  bang
//
//  Created by wt on 15/5/13.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@property (nonatomic, assign) BOOL showBolder;

@end
