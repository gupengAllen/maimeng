//
//  BaseTableViewController.h
//  bang
//
//  Created by 王涛 on 15/6/5.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "BaseViewController.h"
#import "PullingRefreshTableView.h"

@interface BaseTableViewController : BaseViewController<PullingRefreshTableViewDelegate> {
    int page;
    int size;
}

@property (nonatomic, strong) PullingRefreshTableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) UILabel *status;

@end
