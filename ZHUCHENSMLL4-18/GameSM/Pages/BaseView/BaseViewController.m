//
//  BaseViewController.m
//  bang
//
//  Created by wt on 15/5/13.
//  Copyright (c) 2015年 wt. All rights reserved.
//

#import "BaseViewController.h"
#import "Config.h"
#import "Tool.h"
#import "UIViewController+Utils.h"
#import <AVFoundation/AVFoundation.h>

@interface BaseViewController ()<UITextViewDelegate>
{
    UIView *_helpIV;
}
@end

@implementation BaseViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1];
    
    _isOrNoShowShake = YES;
    
    self.contentStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 200, ScreenSizeWidth - 20, 20)];
    self.contentStatusLabel.textColor = [UIColor lightGrayColor];
    self.contentStatusLabel.textAlignment = NSTextAlignmentCenter;
    self.contentStatusLabel.text = @"";//此目录下尚无内容
    self.contentStatusLabel.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:self.contentStatusLabel];
    self.contentStatusLabel.hidden = YES;
    
    self.hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:self.hud];
    
    [self.view addSubview:self.touchBgView];
    
    self.bottomStatusLabel = [[UILabel alloc] initWithFrame:CGRectMake((ScreenSizeWidth - 180)/2, ScreenSizeHeight / 2 + 50, 180, 36)];
    self.bottomStatusLabel.textColor = [UIColor lightGrayColor];
    [self.view addSubview:self.bottomStatusLabel];
    self.bottomStatusLabel.hidden = YES;
    self.bottomStatusLabel.font = [UIFont systemFontOfSize:15];
    self.bottomStatusLabel.textAlignment = NSTextAlignmentCenter;
    self.bottomStatusLabel.text= @"暂时没有更多内容了哟！";
    self.bottomStatusLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
    self.bottomStatusLabel.layer.cornerRadius = 5;
    self.bottomStatusLabel.layer.masksToBounds = YES;
    
    
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [[UIApplication sharedApplication] setApplicationSupportsShakeToEdit:YES];
    
    [self becomeFirstResponder];

    
    [self createShakeView];
    
}

-(void)createShakeView{
            NSArray *windows = [UIApplication sharedApplication].windows;
            UIWindow *win = [windows objectAtIndex:0];
    
            _helpIV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
            _helpIV.userInteractionEnabled = YES;
            _helpIV.backgroundColor = RGBACOLOR(0, 0, 0, 0.6);
            _helpIV.hidden = YES;
            [win addSubview:_helpIV];
            UIControl *helpC = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
            [helpC addTarget:self action:@selector(misFeedBackView:) forControlEvents:UIControlEventTouchUpInside];
            [_helpIV addSubview:helpC];
    self.shakeView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenSizeHeight+100, ScreenSizeWidth, 105)];
    self.shakeView.backgroundColor = [UIColor clearColor];
    [_helpIV addSubview:self.shakeView];
    
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 0, ScreenSizeWidth-40, 47)];
    deleteBtn.backgroundColor = [UIColor whiteColor];
    deleteBtn.layer.masksToBounds = YES;
    deleteBtn.layer.cornerRadius = 4;
    [deleteBtn setTitleColor:RGBACOLOR(253, 85, 103, 1.0) forState:UIControlStateNormal];
    deleteBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
    [deleteBtn setTitle:@"发送反馈" forState:UIControlStateNormal];
    
    [deleteBtn addTarget:self action:@selector(showFeedbackView:) forControlEvents:UIControlEventTouchUpInside];
    [self.shakeView addSubview:deleteBtn];
    
    UIButton *cancleBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, 52, ScreenSizeWidth-40, 47)];
    cancleBtn.backgroundColor = [ UIColor whiteColor];
    cancleBtn.layer.masksToBounds = YES;
    cancleBtn.layer.cornerRadius = 4;
    [cancleBtn setTitle:@"没啥事" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:RGBACOLOR(253, 85, 103, 1.0) forState:UIControlStateNormal];
    cancleBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    [cancleBtn addTarget:self action:@selector(misDeleteView) forControlEvents:UIControlEventTouchUpInside];
    [self.shakeView addSubview:cancleBtn];
    
//createFeedBackTextView
    
    self.feedBackView = [[UIView alloc]initWithFrame:CGRectMake(24, ScreenSizeHeight, ScreenSizeWidth-48, (ScreenSizeWidth-24)/2.0)];
    self.feedBackView.backgroundColor = [UIColor whiteColor];
    
    [_helpIV addSubview:self.feedBackView];
    self.feedBackTextView = [[UITextView alloc]initWithFrame:CGRectMake(8, 7, self.feedBackView.bounds.size.width-16,self.feedBackView.bounds.size.height-40-7)];
    self.feedBackTextView.layer.cornerRadius = 6.0;
    self.feedBackTextView.backgroundColor = RGBACOLOR(239, 239, 239, 1.0);
    self.feedBackTextView.font = [UIFont fontWithName:@"Verdana" size:18];
    [self.feedBackView addSubview:self.feedBackTextView];
    
    UIButton *cancleBtn1 = [self createFeedBtnWithFrame:CGRectMake(5, self.feedBackView.bounds.size.height-29, 60, 25) andTitle:@"取消" andTextAlignment:0];
    [cancleBtn1 addTarget:self action:@selector(misFeedBackView:) forControlEvents:UIControlEventTouchUpInside];
    [cancleBtn1 setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 20)];
    [self.feedBackView addSubview:cancleBtn1];
    
    UIButton *sureBtn = [self createFeedBtnWithFrame:CGRectMake(self.feedBackView.bounds.size.width - 65, self.feedBackView.bounds.size.height-29, 60, 25) andTitle:@"反馈" andTextAlignment:2];
    [sureBtn setTitleColor:RGBACOLOR(253, 97, 114, 1.0) forState:UIControlStateNormal];
    [sureBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 20, 0, 0)];
    [sureBtn addTarget:self action:@selector(postFeed:) forControlEvents:UIControlEventTouchUpInside];
    [self.feedBackView addSubview:sureBtn];
    
//    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
}

-(void)misFeedBackView:(UIButton *)btn{
    _helpIV.hidden = YES;
    self.feedBackTextView.text = @"";
    [self.feedBackTextView resignFirstResponder];
}

-(void)postFeed:(UIButton *)btn{
    _helpIV.hidden = YES;
    [self.feedBackTextView resignFirstResponder];
    if (!self.feedBackTextView.text.length) {
        [MBProgressHUD showError:@"内容不能为空哦~" toView:self.view];
        return;
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSString *className = [NSString stringWithUTF8String:object_getClassName([UIViewController currentViewController])];
    
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_FEEDBACK_ADD,@"r",
                                    self.feedBackTextView.text,@"content",nil];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"logList" ofType:@"plist"];
    NSDictionary *vcName = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    NSString *vcValue = vcName[className];
    
    [[LogHelper shared] writeToFilefrom_page:vcValue from_section:@"p" from_step:@"l" to_page:@"sf" to_section:@"p" to_step:@"l" type:@"a" id:@"0"];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(feedbackAddFinish:) method:POSTDATA];
}

- (void)feedbackAddFinish:(NSDictionary*)dic {
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        self.feedBackTextView.text = @"";
        [MBProgressHUD showSuccess:@"反馈成功" toView:self.view];
    }
}

-(UIButton *)createFeedBtnWithFrame:(CGRect)frame andTitle:(NSString *)title andTextAlignment:(int)textAlignment{
    UIButton *btn = [[UIButton alloc]initWithFrame:frame];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:RGBACOLOR(111, 111, 111, 1.0) forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:18];
    btn.titleLabel.textAlignment = textAlignment;
    return btn;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    //键盘
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        _isOrNoShowShake = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight-self.feedBackView.bounds.size.height-kbSize.height, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
        }];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification *)notification
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        _isOrNoShowShake = YES;
        [UIView animateWithDuration:0.3 animations:^{
            self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
        }];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

-(void)showFeedbackView:(UIButton *)btn{
    self.shakeView.frame = CGRectMake(0, ScreenSizeHeight+100, ScreenSizeWidth, 105);
    [self.feedBackTextView becomeFirstResponder];
//    [self.feedBackTextView becomeFirstResponder];
}

-(void)misDeleteView{
    _helpIV.hidden = YES;
    self.shakeView.frame = CGRectMake(0, ScreenSizeHeight+100, ScreenSizeWidth, 105);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    //    self.navigationController.extendedLayoutIncludesOpaqueBars = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];//colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1
    //    self.navigationController.navigationBar.backgroundColor = [UIColor redColor];
    if (IsIOS7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];//colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
    }
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        NSArray *list=self.navigationController.navigationBar.subviews;
        for (id obj in list) {
            if ([obj isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView=(UIImageView *)obj;
                NSArray *list2=imageView.subviews;
                for (id obj2 in list2) {
                    if ([obj2 isKindOfClass:[UIImageView class]]) {
                        UIImageView *imageView2=(UIImageView *)obj2;
                        //                        imageView2.backgroundColor = [UIColor colorWithRed:239/255.0 green:105/255.0 blue:125/255.0 alpha:1];
                        imageView2.hidden = YES;
                    }
                }
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - 初始化Nav
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    
    
    if (_isFirst) {
        [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
        [buttonBack setImage:[UIImage imageNamed:@"menu_list.png"] forState:UIControlStateNormal];
    } else {
        [buttonBack setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
    }
    
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
//    buttonBack.backgroundColor = [UIColor redColor];
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)initTitleName:(NSString *)title {
    if (!_baseTitleLabel) {
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0 , ScreenSizeWidth- 160, 44)];
        titleLabel.backgroundColor = [UIColor clearColor];
        // titleLabel.font = [UIFont boldSystemFontOfSize:20];
        titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
        self.baseTitleLabel = titleLabel;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text = title;
    } else {
        self.baseTitleLabel.text = title;
    }
    
    self.navigationItem.titleView = self.baseTitleLabel;
}

- (void)changeTitleView:(NSString *)title {
    UILabel *label = (UILabel *)self.navigationItem.titleView;
    label.text = title;
}

- (void)goBackToPreview:(id)btn {
//    if (_isFirst) {
//    } else {
        [self.navigationController popViewControllerAnimated:YES];
    
}

- (void)addRightItemWithTitle:(NSString*)itemTitle itemTarget:(id)target action:(SEL)selector {
    UIButton *regsiterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [regsiterBtn setTitle:itemTitle forState:UIControlStateNormal];
    regsiterBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    regsiterBtn.bounds = CGRectMake(0, 0, 50, 40);
    regsiterBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.rightBtn = regsiterBtn;
    [regsiterBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [regsiterBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:regsiterBtn];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)addRightItemWithImage:(UIImage*)image itemTarget:(id)target action:(SEL)selector {
    UIButton *regsiterBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [regsiterBtn setImage:image forState:UIControlStateNormal];
    regsiterBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    regsiterBtn.bounds = CGRectMake(0, 0, image.size.width, image.size.height);
    regsiterBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    self.rightBtn = regsiterBtn;
    [regsiterBtn setTitleColor:[UIColor darkTextColor] forState:UIControlStateNormal];
    [regsiterBtn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:regsiterBtn];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)addRightItemWithButton:(UIButton*)btn itemTarget:(id)target action:(SEL)selector {
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.rightBtn = btn;
    
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)addLeftItemWithButton:(UIButton*)btn itemTarget:(id)target action:(SEL)selector{
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    self.leftBtn = btn;
    
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    self.navigationItem.leftBarButtonItem = item;
}

- (UIControl*)touchBgView {
    if (!_touchBgView) {
        _touchBgView = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
        [_touchBgView addTarget:self action:@selector(touchBgViewTouchInView) forControlEvents:UIControlEventTouchDown];
        _touchBgView.backgroundColor = [UIColor clearColor];
        _touchBgView.hidden = YES;
    }
    
    return _touchBgView;
}

- (void)touchBgViewTouchInView {
    
}

- (void)bottomShow {
    [self.view bringSubviewToFront:self.bottomStatusLabel];
    self.bottomStatusLabel.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        
        self.bottomStatusLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
    }];
    [self performSelector:@selector(hideBottomView) withObject:nil afterDelay:1];
}

- (void)hideBottomView {
    [UIView animateWithDuration:1 animations:^{
        
        self.bottomStatusLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0];
    } completion:^(BOOL finished) {
        
        self.bottomStatusLabel.hidden = YES;
    }];
}

#pragma mark - 摇一摇
// 摇一摇开始摇动
- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSLog(@"开始摇动");
    return;
}

// 摇一摇取消摇动
- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    NSLog(@"取消摇动");
    return;
}

// 摇一摇摇动结束
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.subtype == UIEventSubtypeMotionShake) { // 判断是否是摇动结束
        if (_isOrNoShowShake) {
            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

            _helpIV.hidden = NO;
            self.shakeView.frame = CGRectMake(0, ScreenSizeHeight-105, ScreenSizeWidth, 105);
        }
        NSLog(@"摇动结束");
    }
    return;
}


@end
