//
//  UserProtocalViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/25.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "UserProtocalViewController.h"

@interface UserProtocalViewController ()

@end

@implementation UserProtocalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    [self initTitleName:@"免责声明"];
    // Do any additional setup after loading the view from its nib.
    UIWebView *_webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
    //    _webView.scrollView.contentSize = CGSizeMake(0, ScreenSizeHeight + 100);
    NSString *strUrl = @"http://api.playsm.com/disclaimer.html";
    NSURL *url = [NSURL URLWithString:strUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [_webView loadRequest:request];
    [_webView setScalesPageToFit:YES];
    
    [self.view addSubview:_webView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
