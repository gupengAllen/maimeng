//
//  FeedbackViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"

@interface FeedbackViewController : BaseTableViewController

@property (weak, nonatomic) IBOutlet UITextView *inputText;

@end
