//
//  ConstantViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/27.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "ConstantViewController.h"
#import "Config.h"
#import "AFNetworking.h"
#import "Y_X_DataInterface.h"
#import "feedbackModel.h"
#import "MsgCell.h"
#import "CustomTool.h"
#define COMMENTTITLECOLOR [UIColor colorWithRed:75/255.0 green:186/255.0 blue:202/255.0 alpha:1]
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5

@interface ConstantViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UIView          *_FaceBagView;
    UICollectionView *_collectionView;
    BOOL            _isFace;
    NSMutableArray  *_faceDataArr;
    NSMutableArray  *_faceNameArr;
    CGFloat         _keyboardHeight;
    UIView          *_shadowView;
    UITextField     *textFeildPing;
    UITableView     *_chatTableView;
    BOOL            _isFirst;
}
@property (nonatomic,retain)NSMutableArray *chatArr;
@end

@implementation ConstantViewController


- (NSMutableArray *)chatArr{
        if (_chatArr == nil) {
            _chatArr = [[NSMutableArray alloc] init];
        }
        return _chatArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
}


- (void)keyboardWillShow:(NSNotification *)notification {
    [textFeildPing becomeFirstResponder];
    if (_chatArr.count > 4) {
        NSDictionary* info = [notification userInfo];
        CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
        _chatTableView.frame=CGRectMake(0, -kbSize.height + 64 + 44, self.view.frame.size.width, _chatTableView.frame.size.height);
    }

}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if (_chatArr.count > 4) {
        _chatTableView.frame=CGRectMake(0, 64, self.view.frame.size.width, _chatTableView.frame.size.height);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    _isFirst = YES;
    _chatTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, KScreenWidth, KScreenheight - 64 - 45) style:UITableViewStylePlain];
    _chatTableView.backgroundColor = [UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0];
    _chatTableView.delegate = self;
    _chatTableView.dataSource = self;
    _chatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_chatTableView];
    
    UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGes)];
    [_chatTableView addGestureRecognizer:tapGes];
    
    [self getChatArr];
    _inputTextField.delegate = self;
    
    
    UIView *vottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 40)];
    vottomView.backgroundColor = [UIColor whiteColor];
    
    UIView *secondBottomView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, 283*_Scale6, 30)];
    secondBottomView.backgroundColor = [UIColor colorWithRed:255/255.0 green:238/255.0 blue:227/255.0 alpha:1];
    [vottomView addSubview:secondBottomView];
    
    UIImageView *lineImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, -0.5, KScreenWidth, 0.5)];
    lineImageView.backgroundColor = RGBACOLOR(203, 203, 203, 1.0);//colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:0.5
    [vottomView addSubview:lineImageView];
    
    UIImageView *pencilView = [[UIImageView alloc] initWithFrame:CGRectMake(6, 7, 12, 15)];
    pencilView.image = [UIImage imageNamed:@"pinglun.png"];
    [secondBottomView addSubview:pencilView];
    
    textFeildPing = [[UITextField alloc] initWithFrame:CGRectMake(20, 0, 246 * _Scale6, 30)];
    textFeildPing.font = [UIFont systemFontOfSize:14.0];
    textFeildPing.placeholder = @"我来说两句";
    [secondBottomView addSubview:textFeildPing];
    
    UIButton *sendButton =[UIButton buttonWithType:UIButtonTypeCustom];
    sendButton.frame = CGRectMake(304*_Scale6, 8, 52*_Scale6, 24);
    sendButton.layer.masksToBounds = YES;
    sendButton.layer.cornerRadius = 5;
    sendButton.backgroundColor = [UIColor colorWithRed:254/255.0 green:65/255.0 blue:83/255.0 alpha:1];
//    [sendButton setBackgroundImage:[UIImage imageNamed:@"fasong_anniu.png"] forState:UIControlStateNormal];
    [sendButton setTitle:@"吐槽" forState:UIControlStateNormal];
    sendButton.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendButton addTarget:self action:@selector(sendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [vottomView addSubview:sendButton];
    
    _inputTextField.inputAccessoryView = vottomView;
    [self setNav];
}

- (void)tapGes{
    [textFeildPing resignFirstResponder];
    [_inputTextField resignFirstResponder];
    
    if (_chatArr.count > 4) {
        _chatTableView.frame=CGRectMake(0, 64, self.view.frame.size.width, _chatTableView.frame.size.height);
    }

}

- (void)sendButtonAction:(UIButton *)sendButton{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSDictionary * readDict = @{@"r":@"feedback/add",@"type":@"0",@"content":textFeildPing.text};
    
    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:readDict object:self action:@selector(sendMessage:) method:POSTDATA];
    
}



- (void)sendMessage:(NSDictionary *)messageDict{
    
    [[LogHelper shared] writeToFilefrom_page:@"sf" from_section:@"profile" from_step:@"l" to_page:@"sff" to_section:@"profile" to_step:@"a" type:@"" id:@"0"];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [textFeildPing resignFirstResponder];
    if (_chatArr.count > 4) {
        _chatTableView.frame=CGRectMake(0, 64, self.view.frame.size.width, _chatTableView.frame.size.height);
    }
    feedbackModel *feedModelFirst = [[feedbackModel alloc] init];
    feedModelFirst.content = textFeildPing.text;
    feedModelFirst.type = @"0";
    CGSize sizeToFit = [textFeildPing.text sizeWithFont:[UIFont systemFontOfSize:12.0] constrainedToSize:CGSizeMake(150, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//
    feedModelFirst.cellSize = sizeToFit;
    [self.chatArr addObject:feedModelFirst];
    [_chatTableView reloadData];
    textFeildPing.text = @"";
    [textFeildPing resignFirstResponder];
    [_inputTextField resignFirstResponder];
    
    if (_isFirst) {
        feedbackModel *feedModel = [[feedbackModel alloc] init];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@r=%@",INTERFACE_PREFIXD,API_URL_FEEDBACK_ADD_CONSTANT];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSDictionary *extraInfo = backData[@"extraInfo"];
            feedModel.content = extraInfo[@"systemContent"];
            feedModel.type = @"1";
            CGSize sizeToFitTWO = [feedModel.content sizeWithFont:[UIFont systemFontOfSize:12.0] constrainedToSize:CGSizeMake(150, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//
            feedModel.cellSize = sizeToFitTWO;
            [self.chatArr addObject:feedModel];
            _isFirst = NO;
            [_chatTableView reloadData];
            
            textFeildPing.text = @"";
            [textFeildPing resignFirstResponder];
            [_inputTextField resignFirstResponder];
            
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
    }
}


- (void)getChatArr{
//    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:@{@"r":@"feedback/withSystemReply",@"size":@"999",@"page":@"1"} object:self action:@selector(asyRequire:) method:GETDATA];
    
    
//    [YK_API_request startGetLoad:@"http://api.playsm.com/index.php?r=feedback/withSystemReply" extraParams:nil object:self action:@selector(asyRequire:) header:headers];
}

- (void)asyRequire:(NSDictionary *)requireDict{
    self.chatArr = [feedbackModel paraseFeedBackArr:requireDict[@"results"]];

//    for (int i = 0; i < self.chatArr.count; i ++) {
//        feedbackModel *feeModel = self.chatArr[i];
//        feedbackModel *twoFeeModel = self.chatArr[i + 1];

//        if ([feeModel.type isEqualToString:@"0"]&&[twoFeeModel.type isEqualToString:@"0"] ) {
    if(self.chatArr.count > 0){
        _isFirst = NO;
            feedbackModel *feeModelXI = [[feedbackModel alloc] init];
        feeModelXI.content = requireDict[@"extraInfo"][@"systemContent"];
        feeModelXI.type = @"1";
            CGSize sizeToFitTWO = [feeModelXI.content sizeWithFont:[UIFont systemFontOfSize:12.0] constrainedToSize:CGSizeMake(150, CGFLOAT_MAX) lineBreakMode:UILineBreakModeWordWrap];//
            feeModelXI.cellSize = sizeToFitTWO;
        [self.chatArr insertObject:feeModelXI atIndex:1];
    }
//        }
//    }
    [_chatTableView reloadData];
    
    if (self.chatArr.count > 0) {
        NSUInteger ii[2] = {0, self.chatArr.count - 1};
        NSIndexPath* indexPath = [NSIndexPath indexPathWithIndexes:ii length:2];
        [_chatTableView scrollToRowAtIndexPath:indexPath
                              atScrollPosition:UITableViewScrollPositionTop animated:NO];

    }
    
}




- (void)setNav{
//    UINavigationBar *bar = self.navigationController.navigationBar;
//    bar.barTintColor = [UIColor whiteColor];
//        bar.tintColor = [UIColor whiteColor];
//        titleLabel.font = [UIFont fontWithName:@"迷你简菱心" size:20];
//    titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    
    UIView *Nav = [CustomTool createNavView];
    [self.view addSubview:Nav];
    UILabel * _titleName = [[UILabel alloc]init];
    _titleName.center = CGPointMake((ScreenSizeWidth-70)/2, 23);
    _titleName.size = CGSizeMake(200, 40);
    _titleName.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    _titleName.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    _titleName.textAlignment = 0;
    _titleName.text = @"意见反馈";
    [Nav addSubview:_titleName];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(7, 25.5, 33, 33);
    [backButton setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    
    [Nav addSubview:backButton];
}

#pragma mark -- tableview delegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.chatArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cellID = @"CHATCELL";
//    MsgCell *cell = [_chatTableView dequeueReusableCellWithIdentifier:cellID];
//    if (cell == nil) {
      MsgCell*  cell = [[MsgCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    cell.backgroundColor = RGBACOLOR(225, 225, 225, 1.0);
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    }
    cell.model = self.chatArr[indexPath.row];
    [cell fillData];
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGSize size = [self.chatArr[indexPath.row] cellSize];
    return size.height + 50;
}

- (void)misBack{
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate
{
    
    return NO;
    
}

- (NSUInteger)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
