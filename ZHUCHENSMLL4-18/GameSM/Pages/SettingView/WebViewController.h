//
//  WebViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/20.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface WebViewController : BaseViewController<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSString *url;

@property (nonatomic, assign) BOOL needSecret;

@end
