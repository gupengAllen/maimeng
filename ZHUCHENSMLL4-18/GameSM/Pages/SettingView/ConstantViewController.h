//
//  ConstantViewController.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/27.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConstantViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *inputView;
@property (weak, nonatomic) IBOutlet UITextField *inputTextField;

@end
