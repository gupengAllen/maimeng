//
//  FeedbackViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/16.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "FeedbackViewController.h"
#import "MBProgressHUD+Add.h"

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initTitleName:@"意见反馈"];
    [self addBackBtn];
    
    [self addRightItemWithTitle:@"提交" itemTarget:self action:@selector(rightBtnPressed:)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 按钮事件
- (void)rightBtnPressed:(UIButton*)btn {
    [self.inputText resignFirstResponder];
    if (!self.inputText.text.length) {
        [MBProgressHUD showError:@"内容不能为空哦~" toView:self.view];
        return;
    }
    [self feedbackAdd];
}

#pragma mark - 加载数据
- (void)feedbackAdd {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_FEEDBACK_ADD,@"r",
                                    self.inputText.text,@"content",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(feedbackAddFinish:) method:POSTDATA];
}

- (void)feedbackAddFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [MBProgressHUD showSuccess:@"反馈成功" toView:self.view];
        [self performSelector:@selector(goBackToPreview:) withObject:nil afterDelay:1];
    }
}

- (void)goBackToPreview:(id)btn {
    [self dismissViewControllerAnimated:YES completion:nil];
//    [self dismissViewControllerAnimated:YES completion:nil];
}
//- (void)goBackToPreview:(id)btn {
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
@end
