//
//  FaceBagCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FaceBagModel;

@interface FaceBagCell : UICollectionViewCell

@property (nonatomic, strong) FaceBagModel *model;

@end
