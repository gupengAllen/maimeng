//
//  CommentDetailViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/4/19.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface CommentDetailViewController : BaseViewController

//主
@property (strong,nonatomic)UITableView *tableView;
//层
@property (strong,nonatomic)UITableView *florTableView;

@property (strong,nonatomic)UIView *headView;
//楼主
@property (strong,nonatomic)UIImageView *landlordImageView;
@property (strong,nonatomic)UILabel *landlordNameLabel;
@property (strong,nonatomic)UILabel *landlordTimeLabel;
@property (strong,nonatomic)UITextView *landlordContentTextView;
@property (strong,nonatomic)UIButton *landlordPraiseNumBtn;
//漫画
@property (strong,nonatomic)UIView *cartoonView;
@property (strong,nonatomic)UIImageView *cartoonImage;
@property (strong,nonatomic)UILabel *cartoonName;
@property (strong,nonatomic)UILabel *authorName;
@property (strong,nonatomic)UILabel *cartoonType;
@property (strong,nonatomic)UILabel *cartoonUpdate;
//赞
@property (strong,nonatomic)UIView *praiseView;

//数据
@property (strong,nonatomic)NSDictionary *landlordContentDict;
@property (strong,nonatomic)NSDictionary *cartoonInfomationDict;

@end
