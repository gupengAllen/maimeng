//
//  CommentTableViewCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5
#define LFCellID @"commentTVC"

#import "CommentTableViewCell.h"
#import "CommentModel.h"
#import "Config.h"
#import "UIImageView+AFNetworking.h"
#import "CustomTool.h"

@interface  CommentTableViewCell ()<UITextViewDelegate>
{
    UIImageView     *_iv;
    
    UILabel         *_timeStatus;
    UILabel         *_contentLabel;
    UIButton        *_btn;
    NSMutableArray  *_pictiueArr;
    NSMutableArray  *_faceNameArr;
    UIView          *_testIv;
    UIScrollView    *_contentScrollView;
    BOOL            _OUT;
    UITextView      *_labelFu;
}
@end

@implementation CommentTableViewCell
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


-(instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _fontSize = 16;
        _faceNameArr = [NSMutableArray array];
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
        NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
        for (int i =1; i < 72; i ++) {
            [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        }
        //        [self createView];
    }
    return self;
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    UITextView *tv = object;
    CGFloat bottomCorrent = ([tv bounds].size.height - [tv contentSize].height*[tv zoomScale])/2.0;
    bottomCorrent = (bottomCorrent <0.0 ? 0.0 : bottomCorrent);
    tv.contentOffset = (CGPoint){.x=0,.y=-bottomCorrent};
}

-(void)createView{
        
    _iv = [[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 35, 35)];
    _iv.layer.masksToBounds = YES;
    _iv.layer.cornerRadius = _iv.bounds.size.width/2;
    [self.contentView addSubview:_iv];
    
    self.floorLabel = [CustomTool createLabelWithFrame:CGRectMake(15, 50, 35, 20) andType:12 andColor:CUSTOMBLUECOLOR];
    self.floorLabel.textAlignment = 1;
    [self.contentView addSubview:self.floorLabel];
    
    _nameLabel = [CustomTool createLabelWithFrame:CGRectMake(_iv.right+15, 10, 200, 14) andType:14 andColor:CUSTOMREDCOLOR];
    [self.contentView addSubview:_nameLabel];
    
    _timeStatus = [CustomTool createLabelWithFrame:CGRectMake(_iv.right+15, _nameLabel.bottom+5, ScreenSizeWidth - 2*TMARGIN + 40, 10) andType:10 andColor:CUSTOMTITLECOLOER3];
    [self.contentView addSubview:_timeStatus];
    

// UITextView-富文本
    NSString *cententString = _model.content;
    BOOL eee=_showAllContent;
    if (cententString.length > 120) {
        if (!_showAllContent) {
            cententString = [cententString substringToIndex:100];
            cententString = [cententString stringByAppendingString:@"\n......\n"];
        }else{
            cententString = [cententString stringByAppendingString:@"\n"];
        }
    }
    
    _labelFu = [[UITextView alloc]initWithFrame:CGRectMake(_iv.right+15, 45, KScreenWidth - 80, 20)];
    _labelFu.backgroundColor = [UIColor clearColor];
    _labelFu.userInteractionEnabled = NO;
    _labelFu.editable = NO;
    [self.contentView addSubview:_labelFu];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.lineSpacing = 5;// 字体的行间距
    NSDictionary *attributes = @{ NSFontAttributeName:[UIFont systemFontOfSize:_fontSize],NSParagraphStyleAttributeName:paragraphStyle};
    
    NSString *regexString = @"\\[[^\\]]+\\]";
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc]init];//_model.content
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
    NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
    if (arrr.count>0) {
        for (int i =0 ; i < arrr.count; i ++) {
            NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
            NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
            NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
            [attri appendAttributedString:attrStr1];
            NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
            for (int j = 0; j < _faceNameArr.count; j ++) {
                if ([faceStr isEqualToString:_faceNameArr[j]]) {
                    NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                    attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                    attch.bounds = CGRectMake(0, -2, 30, 30);
                    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                    [attri appendAttributedString:string1];
                    break;
                }
            }
            cententString = [cententString substringFromIndex:result.range.location+result.range.length];
        }
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
        [attri appendAttributedString:attrStr2];
        [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:_fontSize] range:NSMakeRange(0, attri.length)];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attri boundingRectWithSize:CGSizeMake(KScreenWidth - 80-16, MAXFLOAT) options:options context:nil];
        _labelFu.attributedText = [[NSAttributedString alloc] initWithAttributedString:attri];
        _labelFu.frame = CGRectMake(_iv.right+15, 45, KScreenWidth - 80, rect.size.height+12);
    }else{
        NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
        NSRange allRange = [cententString rangeOfString:cententString];
        [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:_fontSize] range:allRange];
        [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
        NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(KScreenWidth - 80-16, MAXFLOAT) options:options context:nil];
        _labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
        int height;
        if (_fontSize == 14) {
            height = (int)rect.size.height/16;
             _labelFu.frame = CGRectMake(_iv.right+15, 45, KScreenWidth - 80, height*24+10);
        }else{
            height = (int)rect.size.height/19;
             _labelFu.frame = CGRectMake(_iv.right+15, 45, KScreenWidth - 80, height*27+10);
        }
    }
    
//----赋值
    _timeStatus.text = _model.createTimeValue;
    if (_model.replyUserIDInfo) {
        _nameLabel.text = _model.replyUserIDInfo[@"name"];
        [_iv setImageWithURL:[NSURL URLWithString:_model.replyUserIDInfo[@"images"]]];
    }else{
        _nameLabel.text = _model.userIDInfo[@"name"];
        [_iv setImageWithURL:[NSURL URLWithString:_model.userIDInfo[@"images"]]];
    }    
}
-(void)setFontSize:(int)fontSize{
    _fontSize = fontSize;
}
-(void)setModel:(CommentModel *)model{
    _model = model;
    [self createView];
}
//自定义分割线
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect); //上分割线，
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(0, rect.size.height-0.5, 0.01, 0.01));    //    CGContextSetStrokeColorWithColor(context, [UIColor orangeColor].CGColor);
    //    CGContextStrokeRect(context, CGRectMake(5, rect.size.height, rect.size.width - 10, 1));
}

@end
