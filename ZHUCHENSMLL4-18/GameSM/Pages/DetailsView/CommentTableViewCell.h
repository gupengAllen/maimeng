//
//  CommentTableViewCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CommentModel;

@interface CommentTableViewCell : UITableViewCell

@property (nonatomic, strong) CommentModel *model;
@property (nonatomic, strong) UILabel *floorLabel;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, assign) BOOL showAllContent;
@property (nonatomic, assign) int fontSize;

@end
