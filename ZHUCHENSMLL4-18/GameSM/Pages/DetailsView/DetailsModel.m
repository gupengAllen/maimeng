//
//  DetailsModel.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "DetailsModel.h"

@implementation DetailsModel

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{}

+(instancetype)modelWithIconName:(NSString *)episodeNum{
    DetailsModel *model = [[DetailsModel alloc]init];
    model.episodeNum = episodeNum;
    return model;
}

@end
