//
//  testViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/19.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "testViewController.h"
#import "DetailsModel.h"
#import "Y_X_DataInterface.h"
#import "AFNetworking.h"
#import "ReadController.h"
#import "YKHttpRequest.h"
#import "YK_API_request.h"
#import "KeychainItemWrapper.h"
#import "downViewBtn.h"
#import "SecondReadController.h"
#import "cartoonChapterListModel.h"
#import "CommentTableViewController.h"
#import "XLScrollViewer.h"
#import "CommentTableViewCell.h"
#import "CommentModel.h"
#import "FaceBagCell.h"
#import "FaceBagModel.h"
#import "LoginAlertView.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
#import "DownLoadViewController.h"
#import "chapterIdModel.h"
#import "DataBaseHelper.h"
#import "CustomTool.h"
#import "ManhuaViewController.h"
#import "Config.h"
#import "CartoonInfoAll.h"
#import "UMSocial.h"
//2.8
#import "CommentDetailViewController.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5
#define EMOJI_CODE_TO_SYMBOL(x) ((((0x808080F0 | (x & 0x3F000) >> 4) | (x & 0xFC0) << 10) | (x & 0x1C0000) << 18) | (x & 0x3F) << 24)

@interface testViewController () <UIAlertViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIScrollViewDelegate,LoginAlertViewDelegate>{
    NSString        *_urlStr;
    NSDictionary    *_dataDic;
    NSDictionary    *_allDict;
    NSInteger       _cartoonChapterCount;
    NSInteger       _cartoonId;
    NSMutableArray  *_dataIDArr;
    NSMutableArray  *_subButtons;
    UILabel         *_titleName;
    UIButton        *_btnMU;
    UIButton        *_btnPING;
    UIButton        *updown;
    
    NSString        *chapteID;
    UIView          *_commentView;
    UIView          *_MuIv;
    UIImageView     *_btnImage;
    NSMutableDictionary *_tableViewDics;
    NSMutableArray  *_faceDataArr;
    NSMutableArray  *_faceNameArr;
    NSMutableArray  *_dataArr;
    UITableView     *_tableView;
    UICollectionView *_collectionView;
    UITextView     *_inputTextField;
    UIImageView     *_slideView;
    UIView          *_FaceBagView;
    UIView          *_textFieldView;
    NSMutableArray  *_commentListArr;
    CGFloat         _keyboardHeight;
    BOOL            _isFace;
    BOOL            _isList;
    BOOL            _isRefresh;
    BOOL            _isShow;
    BOOL            _isLoadComment;
    BOOL            _isSave;
    BOOL            _isAlertShow;
    UIPageControl   *_pageC;
    NSArray        *_allInfo;
    NSMutableArray *_dataRequireArr;
    NSArray *cartoonChapListArr;
    UIButton *downLoadBtn;
    
    UIImageView     *_netImageView;
    UIButton        *_refreshBtn1;
    downViewBtn     *_bigDesView;
    CGFloat         _lastContentOffset;
    CGFloat         _beginScrollViewY;
    
    NSInteger       _deleteRow;
    UIView          *_deleteView;
    UIView          *_blackBgView;
    NSString        *_deleteId;
    CartoonInfoAll  *_CartoonInfoAllModel;
    CartoonInfoAll  *cartoonInfoAll;
    NSInteger       tagMarkCartoon;
    
    BOOL            _isReply;
    BOOL            _yaoyiyao;
    
    UIView          *_whiteView;
    UIButton        *_sendMes;
    NSInteger       _page;
    //2.8
    NSDictionary    *_cartoonInfomationDict;
    UIButton        *_deleteCommentBtn;
    UIButton        *_replyCommentBtn;
    UIButton        *_praiseCommentBtn;
    UILabel         *_praiseNumLabel;
    UILabel         *_replyNumLabel;
    UIView          *_bgBlackView;
     UITextView *_testView;
}
@end

@implementation testViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self createUI];
    [self createRefreshUI];
    [self createTableViewUI];
    //初始化数据
    _isRefresh = YES;
    _isShow = YES;
    _isLoadComment = YES;
    _isAlertShow = NO;
    _isReply = NO;
    _yaoyiyao = YES;
    cartoonChapListArr = [NSMutableArray array];
    _dataArr = [NSMutableArray array];
    _allInfo = [NSArray array];
    
    self.commicName.textColor = [UIColor whiteColor];
    _testView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth - 80, 100)];
    
    self.bgCartoonImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.bgCartoonImageView.clipsToBounds = YES;
    
    // Do any additional setup after loading the view from its nib.
    [self addNoti];
    _commicImage.layer.cornerRadius = 2.0;
    _commicImage.layer.masksToBounds = YES;
    _isFace = YES;
    _addSav.layer.cornerRadius = 2.0;
    _addSav.layer.borderColor = [[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] CGColor];
    _addSav.layer.borderWidth = 0.5;
    [self addGes];
    [self initFaceData];
    [self setNavBar];
    [self createCommentTableView];
    [self createFaceBag];
    [self createPageControl];
    //--
    _netImageView = [CustomTool createImageView];
    [self.view addSubview:_netImageView];
    
    //--tupian  mohu
    _blackBgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    _blackBgView.backgroundColor = [UIColor blackColor];
    _blackBgView.alpha = 0;
    [self.view addSubview:_blackBgView];
    
    self.touchBgView = [[UIControl alloc]initWithFrame:CGRectMake(0, 100, KScreenWidth, KScreenheight - 100 - _keyboardHeight)];
    [self.touchBgView addTarget:self action:@selector(touchBgViewTouchInView) forControlEvents:UIControlEventTouchUpInside];
    self.touchBgView.backgroundColor = [UIColor clearColor];
    self.touchBgView.hidden = YES;
    [self.view addSubview:self.touchBgView];
    
    
    _sendMes = [UIButton buttonWithType:UIButtonTypeCustom];
    _sendMes.frame = CGRectMake(KScreenWidth - 55, KScreenheight - 55, 50, 50);
    [_sendMes setImage:[UIImage imageNamed:@"icon_fabiao"] forState:UIControlStateNormal];
    [_sendMes addTarget:self action:@selector(sendMesClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_sendMes];
    _sendMes.hidden = YES;
    
    _bgBlackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
    UIControl *misCon = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
    [misCon addTarget:self action:@selector(cancalKeyBoard) forControlEvents:UIControlEventTouchUpInside];
    [_bgBlackView addSubview:misCon];
    _bgBlackView.backgroundColor = [UIColor blackColor];
    _bgBlackView.alpha = 0.4;
    _bgBlackView.hidden = YES;
    [self.view addSubview:_bgBlackView];
    
    
}

- (void)sendMesClick {
    
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cc" to_section:@"c" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%ld",(long)_cartoonId]];
    if (!g_App.userID) {
        [[LogHelper shared] writeToFilefrom_page:@"cc" from_section:@"c" from_step:@"r" to_page:@"nl" to_section:@"c" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%ld",(long)_cartoonId]];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        _isRefresh = NO;
        return;
    }else{
    _yaoyiyao = NO;
        _bgBlackView.hidden = NO;
        [_inputTextField becomeFirstResponder];
    }
}

-(void)createTableViewUI{
    self.tableViewUI = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    self.tableViewUI.tableHeaderView = self.topView;
    self.tableViewUI.tableFooterView = self.bottomView;
    //    self.tableViewUI.pagingEnabled = YES;
    self.tableViewUI.delegate = self;
    self.tableViewUI.dataSource = self;
    self.tableViewUI.tag = 100;
    self.tableViewUI.backgroundColor= [UIColor whiteColor];
    self.tableViewUI.showsVerticalScrollIndicator = NO;
    self.tableViewUI.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.tableViewUI];
}

-(void)createUI{
    //    self.SuperScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    //    self.SuperScrollView.contentSize = CGSizeMake(KScreenWidth, KScreenheight);
    //    [self.view addSubview:self.SuperScrollView];
    self.topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 315)];
    
    self.bgCartoonImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 191)];
    [self.topView addSubview:self.bgCartoonImageView];
    
    self.bgBigView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 191)];
    self.bgBigView.backgroundColor = [UIColor blackColor];
    self.bgBigView.alpha = 0.5;
    [self.topView addSubview:self.bgBigView];
    
    UIView *bgSmallView = [[UIView alloc]initWithFrame:CGRectMake(10, 70, 110, 128)];
    bgSmallView.backgroundColor = [UIColor whiteColor];
    bgSmallView.layer.masksToBounds = YES;
    bgSmallView.layer.cornerRadius = 5.0;
    [self.topView addSubview:bgSmallView];
    
    self.commicImage = [[UIImageView alloc]initWithFrame:CGRectMake(12, 72, 106, 152)];
    self.commicImage.backgroundColor = [UIColor clearColor];
    [self.topView addSubview:self.commicImage];
    
    self.commicName = [[UILabel alloc]initWithFrame:CGRectMake(122, 64, KScreenWidth-132, 30)];
    self.commicName.backgroundColor = [UIColor clearColor];
    self.commicName.font = CUSTOMTITLESIZE18;//[UIFont systemFontOfSize:18.0];
    [self.topView addSubview:self.commicName];
    
    self.commicAuthor = [[UILabel alloc]initWithFrame:CGRectMake(122, 95, KScreenWidth-132, 25)];
    self.commicAuthor.backgroundColor = [UIColor clearColor];
    self.commicAuthor.font = CUSTOMTITLESIZE12;
    self.commicAuthor.textColor = [UIColor whiteColor];
    [self.topView addSubview:self.commicAuthor];
    
    self.updateValueLabel = [[UILabel alloc]initWithFrame:CGRectMake(122, 135, KScreenWidth-132, 29)];
    self.updateValueLabel.backgroundColor = [UIColor clearColor];
    self.updateValueLabel.font = CUSTOMTITLESIZE16;
    self.updateValueLabel.textColor = RGBACOLOR(235.0, 235.0, 235.0, 1.0);
    [self.topView addSubview:self.updateValueLabel];
    
    self.updateTime = [[UILabel alloc]initWithFrame:CGRectMake(122, 162, KScreenWidth-132, 29)];
    self.updateTime.backgroundColor = [UIColor clearColor];
    self.updateTime.font = CUSTOMTITLESIZE16;
    //    self.updateTime.textColor = RGBACOLOR(255, 108, 97, 1.0);
    self.updateTime.textColor = RGBACOLOR(232.0, 232.0, 232.0, 1.0);
    [self.topView addSubview:self.updateTime];
    
    self.addSav = [[UIButton alloc]initWithFrame:CGRectMake(130, 198, (KScreenWidth-122-40)/2.0, 30)];
    self.addSav.backgroundColor = [UIColor clearColor];
    
    [self.addSav setTitle:@"已收藏" forState:UIControlStateSelected];
    [self.addSav setImage:[UIImage imageNamed:@"quxiaoshoucang"] forState:UIControlStateSelected];
    
    [self.addSav setTitle:@"加入收藏" forState:UIControlStateNormal];
    [self.addSav setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.addSav setImage:[UIImage imageNamed:@"soucang"] forState:UIControlStateNormal];
    self.addSav.layer.cornerRadius = 5.0;
    self.addSav.titleLabel.font = CUSTOMTITLESIZE14;
    [self.addSav addTarget:self action:@selector(addSave:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.addSav];
    
    self.countinueWatch = [[UIButton alloc]initWithFrame:CGRectMake(122+(KScreenWidth-122-20)/2.0+10, 198, (KScreenWidth-122-40)/2.0, 30)];
    self.countinueWatch.layer.cornerRadius = 5.0;
    [self.countinueWatch setTitle:@"开始阅读" forState:UIControlStateNormal];
    [self.countinueWatch setBackgroundImage:[UIImage imageNamed:@"lingqu_big"] forState:UIControlStateNormal];
    
    [self.countinueWatch setTitle:@"继续往下撸" forState:UIControlStateSelected];
    self.countinueWatch.titleLabel.font = CUSTOMTITLESIZE14;
    [self.countinueWatch addTarget:self action:@selector(continueWatch:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.countinueWatch];
    
    
    self.commicDes = [[UITextView alloc]initWithFrame:CGRectMake(8, 233, KScreenWidth-16, 60.1)];
    self.commicDes.backgroundColor = [UIColor clearColor];
    self.commicDes.font = CUSTOMTITLESIZE14;
    self.commicDes.userInteractionEnabled = NO;
    [self.topView addSubview:self.commicDes];
    self.commicDes.alpha = 0;
    
    self.contenttLabel = [[UILabel alloc]initWithFrame:CGRectMake(13, 236.5, KScreenWidth-26, 60.1)];
    self.contenttLabel.backgroundColor = [UIColor clearColor];
    self.contenttLabel.font = CUSTOMTITLESIZE14;
    self.contenttLabel.numberOfLines = NO;
    [self.topView addSubview:self.contenttLabel];
    
    
    self.updownButton = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-100, self.commicDes.bottom, 100, 25)];
    [self.updownButton setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
    [self.updownButton setTitle:@" 展开全部" forState:UIControlStateNormal];
    self.updownButton.titleLabel.font = CUSTOMTITLESIZE10;
    [self.updownButton setTitleColor:LIGHTTEXTCOLOR2D1 forState:UIControlStateNormal];
    [self.updownButton addTarget:self action:@selector(downPress:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:self.updownButton];
}

-(void)createRefreshUI{
    self.bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight-64)];
    UIView *lineTopView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 6)];
    lineTopView.backgroundColor = RGBACOLOR(245, 245, 245, 1.0);
    [self.bottomView addSubview:lineTopView];
    //布局MuIv
    _MuIv = [[UIView alloc]initWithFrame:CGRectMake(0, 6,KScreenWidth, 30)];
    _MuIv.backgroundColor = [UIColor whiteColor];
    [self.bottomView addSubview:_MuIv];
    UIView *linevvIv = [[UIView alloc]initWithFrame:CGRectMake(0, 0,KScreenWidth, 1)];
    linevvIv.backgroundColor = RGBACOLOR(245, 245, 245, 1.0);
    [_MuIv addSubview:linevvIv];
    UIView *linevIV = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth*1.0/2-0.5, 3, 0.5, 20)];
    //    linevIV.backgroundColor = [UIColor grayColor];
    [_MuIv addSubview:linevIV];
    UIView *lineIv = [[UIView alloc]initWithFrame:CGRectMake(0, 29.5,KScreenWidth, 0.5)];
    lineIv.backgroundColor = RGBACOLOR(220, 220, 220, 1.0);
    [_MuIv addSubview:lineIv];
    _slideView = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth/4, 27.5, KScreenWidth/4, 2.5)];
    _slideView.backgroundColor = [UIColor colorWithRed:228/255.0 green:63/255.0 blue:84/255.0 alpha:1];
    //    _slideView.image = [UIImage imageNamed:@"line"];
    [_MuIv addSubview:_slideView];
    
    self.fatherView = [[UIView alloc]initWithFrame:CGRectMake(0, 36, KScreenWidth, 28)];
    self.fatherView.backgroundColor = RGBACOLOR(245, 245, 245, 1.0);
    [self.bottomView addSubview:self.fatherView];
    UIButton *netBtn = [[UIButton alloc]initWithFrame:CGRectMake(8, 0, 75, 28)];
    [netBtn setTitle:@"连载（话）" forState:UIControlStateNormal];
    [netBtn setTitleColor:RGBACOLOR(147, 147, 147, 1.0) forState:UIControlStateNormal];
    netBtn.titleLabel.font = CUSTOMTITLESIZE14;
    [self.fatherView addSubview:netBtn];
    UIButton *orderBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-70, 3, 54, 22)];
    [orderBtn setTitle:@" 正序" forState:UIControlStateNormal];
    [orderBtn setImage:[UIImage imageNamed:@"zheng"] forState:UIControlStateNormal];
    [orderBtn setTitleColor:RGBACOLOR(124, 135, 154, 1.0) forState:UIControlStateNormal];
    orderBtn.titleLabel.font = CUSTOMTITLESIZE12;
    [orderBtn addTarget:self action:@selector(posNavBtn:) forControlEvents:UIControlEventTouchUpInside];
    [self.fatherView addSubview:orderBtn];
    
    self.commicCounts = [[UIScrollView alloc]initWithFrame:CGRectMake(0, self.fatherView.bottom, KScreenWidth, self.bottomView.height-self.fatherView.bottom)];
    self.commicCounts.scrollEnabled = NO;
    self.commicCounts.tag = 1001;
    self.commicCounts.backgroundColor = RGBACOLOR(245, 245, 245, 1.0);
    [self.bottomView addSubview:self.commicCounts];
    self.bgBigView1 = [[UIView alloc]initWithFrame:CGRectMake(0, self.commicCounts.bottom, KScreenWidth, 1)];
    self.bgBigView1.backgroundColor = RGBACOLOR(245, 245, 245, 1.0);
    [self.bottomView addSubview:self.bgBigView1];
}

#pragma mark - 创建UI
-(void)addGes{
    UITapGestureRecognizer *tapGest = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap:)];
    [self.SuperView addGestureRecognizer:tapGest];
}

-(void)createCommentTableView{
    
    //布局评论
    _commentView = [[UIView alloc]initWithFrame:CGRectMake(0, _MuIv.bottom,KScreenWidth, self.bottomView.height-30)];
    _commentView.backgroundColor = [UIColor whiteColor];
    _commentView.alpha = 0;
    [self.bottomView addSubview:_commentView];
    _tableView = [[UITableView alloc]init];
    _tableView.frame = CGRectMake(0, 0, KScreenWidth, self.bottomView.height - 40);
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = NO;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>7.0) {
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }else{
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20);
    }
    
    [_commentView addSubview:_tableView];

    
    _textFieldView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenheight-293*_Scale6, KScreenWidth, 293*_Scale6)];
    _textFieldView.alpha = 0;
    _textFieldView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_textFieldView];
    _textFieldView.hidden = YES;
    _textFieldView.clipsToBounds = YES;
    _inputTextField = [[UITextView alloc]initWithFrame:CGRectMake(15, 45, KScreenWidth-30, 198*_Scale6)];
    _inputTextField.backgroundColor = [UIColor grayColor];
    _inputTextField.font = CUSTOMTITLESIZE16;
    _inputTextField.textColor = [UIColor blackColor];
    _inputTextField.backgroundColor = [UIColor colorWithRed:246/255.0 green:237/255.0 blue:237/255.0 alpha:1];    _inputTextField.returnKeyType = UIReturnKeyDone;
    _inputTextField.keyboardType = UIKeyboardTypeDefault;
    _inputTextField.delegate = self;

    [_textFieldView addSubview:_inputTextField];
    
    UIButton *cancelKeyBoard = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelKeyBoard.frame = CGRectMake(0, 0, 80, 40);
    cancelKeyBoard.titleLabel.font = CUSTOMTITLESIZE16;
    [cancelKeyBoard setTitle:@"取消" forState:UIControlStateNormal];
    [cancelKeyBoard setTitleColor:LIGHTTEXTCOLOR160 forState:UIControlStateNormal];
    [cancelKeyBoard addTarget:self action:@selector(cancalKeyBoard) forControlEvents:UIControlEventTouchUpInside];
    [_textFieldView addSubview:cancelKeyBoard];
    
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(15,_inputTextField.bottom + 6*_Scale6, 30,30)];
    [leftBtn setImage:[UIImage imageNamed:@"biaoqing"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(faceBag:) forControlEvents:UIControlEventTouchUpInside];
    [_textFieldView addSubview:leftBtn];
    
    
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth - 80, 0, 80, 40)];
    [rightBtn setTitle:@"发布" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = CUSTOMTITLESIZE16;
    [rightBtn setTitleColor:LIGHTTEXTCOLOR160 forState:UIControlStateNormal];
    rightBtn.titleLabel.textAlignment = 1;
    [rightBtn addTarget:self action:@selector(TuCao:) forControlEvents:UIControlEventTouchUpInside];
    [_textFieldView addSubview:rightBtn];
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1];
    [_textFieldView addSubview:lineView];
    
    _bigDesView = [[downViewBtn alloc] initWithFrame:CGRectMake(0, 236, ScreenSizeWidth, 0)];
    [self.SuperScrollView addSubview:_bigDesView];
    
    NSArray *nameArr= @[@"目录",@"帖子"];
    
    for (int i = 0; i < 2; i ++) {
        UIButton *btn = [[UIButton alloc]init];
        if (i == 0) {
            btn.frame = CGRectMake(KScreenWidth/4, 0, KScreenWidth/4, 30);
        }else if (i == 1){
            btn.frame = CGRectMake(KScreenWidth/2, 0, KScreenWidth/4, 30);
        }
        [btn setTitle:nameArr[i] forState:UIControlStateNormal];
        btn.titleEdgeInsets = UIEdgeInsetsMake(5, 0, 5, 0);
        btn.titleLabel.font = CUSTOMTITLESIZE16;
        [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        btn.tag = i;
        [btn addTarget:self action:@selector(clike:) forControlEvents:UIControlEventTouchUpInside];
        [_MuIv addSubview:btn];
        if (i == 0) {
            _btnMU = btn;
        }else if (i == 1){
            _btnPING = btn;
        }
    }
    [_btnMU setTitleColor:CUSTOMREDCOLOR forState:UIControlStateNormal];
    
    
    
    
}

-(void)cancalKeyBoard {
    _textFieldView.hidden = YES;
    _bgBlackView.hidden = YES;
    [_inputTextField resignFirstResponder];
}

-(void)createFaceBag{
    _FaceBagView = [[UIView alloc]init];
    _FaceBagView.frame = CGRectMake(0,0, KScreenWidth, 216);
    _FaceBagView.backgroundColor = [UIColor colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0];
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-60, 188, 60, 25)];
    [deleteBtn setImage:[UIImage imageNamed:@"shanchu_2.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deletaClike) forControlEvents:UIControlEventTouchUpInside];
    [_FaceBagView addSubview:deleteBtn];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 186) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];//colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0
    _collectionView.pagingEnabled = YES;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_FaceBagView addSubview:_collectionView];
    NSString *identifier = @"faceCell";
    [_collectionView registerClass:[FaceBagCell class] forCellWithReuseIdentifier:identifier];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView"];
    [_inputTextField resignFirstResponder];
}
-(void)createPageControl{
    //分页控件：宽高是系统有默认值的
    _pageC = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 195, 40, 10)];
    _pageC.center = CGPointMake(KScreenWidth/2.0,_FaceBagView.bounds.size.height -15);
    UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth/2-20, 197.5, 40, 7)];
    iv.backgroundColor = [UIColor clearColor];
    [_FaceBagView addSubview:iv];
    UIView *iv1 = [[UIView alloc]initWithFrame:CGRectMake(0.5, 0, 7, 7)];
    iv1.backgroundColor = [UIColor lightGrayColor];
    iv1.layer.masksToBounds = YES;
    iv1.layer.cornerRadius = 3.5;
    [iv addSubview:iv1];
    UIView *iv2 = [[UIView alloc]initWithFrame:CGRectMake(16.5, 0, 7, 7)];
    iv2.backgroundColor = [UIColor lightGrayColor];
    iv2.layer.masksToBounds = YES;
    iv2.layer.cornerRadius = 3.5;
    [iv addSubview:iv2];
    UIView *iv3 = [[UIView alloc]initWithFrame:CGRectMake(33, 0, 7, 7)];
    iv3.backgroundColor = [UIColor lightGrayColor];
    iv3.layer.masksToBounds = YES;
    iv3.layer.cornerRadius = 3.5;
    [iv addSubview:iv3];
    
    
    _pageC.numberOfPages =3;
    _pageC.backgroundColor = [UIColor clearColor];
    _pageC.layer.masksToBounds = YES;
    _pageC.layer.cornerRadius = 5;
    _pageC.enabled =NO;
    //设置当前选中点得颜色
    _pageC.currentPageIndicatorTintColor = [UIColor redColor];
    _pageC.currentPage =0;
    [_FaceBagView addSubview:_pageC];
}

-(void)deletaClike{
    BOOL isDeleteAll;
    isDeleteAll = NO;
    if (_inputTextField.text.length) {
        NSString *str = [_inputTextField.text substringFromIndex:_inputTextField.text.length-1];
        if ([str isEqualToString:@"]"]) {
            for (int i = _inputTextField.text.length-1; i>0; i --) {
                NSString *str1 = [_inputTextField.text substringWithRange:NSMakeRange(i-1, 1)];
                if ([str1 isEqualToString:@"["]) {
                    _inputTextField.text = [_inputTextField.text substringToIndex:i-1];
                    isDeleteAll = NO;
                    break;
                }
                isDeleteAll = YES;
            }
            if (isDeleteAll) {
                _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
            }
            
        }else{
            _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
        }
    }}

-(void)initFaceData{
    _faceDataArr = [NSMutableArray array];
    _faceNameArr = [NSMutableArray array];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        FaceBagModel *model = [[FaceBagModel alloc]init];
        NSString *imageName = [NSString stringWithFormat:@"%d#.png",i];
        model.imageName = imageName;
        [_faceDataArr addObject:model];
    }
}

#pragma mark - 点击事件
-(void)tap:(UITapGestureRecognizer *)recognizer{
    [_inputTextField resignFirstResponder];
    _inputTextField.text = @"";
    
}
-(void)misComment{
    [_btnMU setTitleColor:[UIColor colorWithRed:234/255.0 green:74/255.0 blue:97/255.0 alpha:1] forState:UIControlStateNormal];
    [_btnPING setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [UIView animateWithDuration:(0.3) animations:^{
        _commentView.alpha = 0;
        _slideView.frame = CGRectMake(KScreenWidth/4, 27.5, KScreenWidth/4, 2.5);
    }];
}
-(void)clike:(UIButton *)btn{
    switch (btn.tag%100) {
        case 0:
            _textFieldView.alpha = 0;
            if (_isList) {
                [self performSelector:@selector(misComment) withObject:nil afterDelay:0.3];
            }else{
                [self performSelector:@selector(misComment) withObject:nil afterDelay:0];
            }
            [_inputTextField resignFirstResponder];
            self.bgBigView1.backgroundColor = RGBACOLOR(229, 229, 229, 1.0);
            _sendMes.hidden = YES;
            break;
        case 1:
            self.bgBigView1.backgroundColor = [UIColor whiteColor];
            [_btnMU setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [_btnPING setTitleColor:CUSTOMREDCOLOR forState:UIControlStateNormal];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDelay:0.0f];
            [UIView setAnimationDuration:0.3f];
            [UIView setAnimationCurve:UIViewAnimationCurveLinear];
            [UIView setAnimationRepeatCount:1];
            [UIView setAnimationDelegate:self];
            _commentView.alpha = 1;
            _textFieldView.alpha = 1;
            _slideView.frame = CGRectMake(KScreenWidth/2, 27.5, KScreenWidth/4, 2.5);
            [UIView commitAnimations];
            if (_isLoadComment) {
                [self loadCommentData];
            }
            _sendMes.hidden = NO;
            break;
        default:
            break;
    }
}


-(BOOL)TuCao:(UIButton *)btn{
    _textFieldView.hidden = YES;
    _bgBlackView.hidden = YES;
    [_inputTextField resignFirstResponder];
    if (!_inputTextField.text.length) {
        self.touchBgView.hidden = YES;
        return YES;
    }
        [self commentMessage:_inputTextField.text];
    self.touchBgView.hidden = YES;
    _inputTextField.text = @"";
    return YES;
}


-(void)faceBag:(UIButton *)btn{
    if (_isFace) {
        [btn setImage:[UIImage imageNamed:@"jianpan.png"] forState:UIControlStateNormal];
        _inputTextField.inputView = _FaceBagView;
//        [_FaceBagView removeFromSuperview];
        
        [_inputTextField becomeFirstResponder];
        [_inputTextField reloadInputViews];
        _isFace = NO;
    }else{
        [btn setImage:[UIImage imageNamed:@"biaoqing"] forState:UIControlStateNormal];
        [_inputTextField becomeFirstResponder];
        _inputTextField.inputView = nil;
        [_inputTextField reloadInputViews];
        _isFace = YES;
    }
}

#pragma mark - 获取键盘View && 坐标
- (void)goBackToPreview:(UIControl *)cont{
    [cont removeFromSuperview];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([self.type isEqualToString:@"4"]) {
        NSArray *windows = [UIApplication sharedApplication].windows;
        UIWindow *win = [windows objectAtIndex:0];
        
        _whiteView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
        _whiteView.backgroundColor = [UIColor whiteColor];
        [win addSubview:_whiteView];
        
        UIControl *control = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
        [control addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
        [_whiteView addSubview:control];
        [MBProgressHUD showHUDAddedTo:_whiteView animated:YES];
        
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    //编辑
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeHeight:) name:UITextFieldTextDidChangeNotification object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    
}
//当文字改变时 改变高度
-(void)changeHeight:(NSNotification *)notification{
    
}

-(void)deleteBackward{
    
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    _isList = YES;
    NSDictionary* info = [notification userInfo];
    //kbSize即為鍵盤尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    _keyboardHeight = kbSize.height;
    //    NSLog(@"hight_hitht:%f",kbSize.height);
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        
        //        [UIView animateWithDuration:0.5 animations:^{
        //            _textFieldView.frame = CGRectMake(0, KScreenheight - 40, KScreenWidth, 40);
        //        }];
        
        if (!_yaoyiyao) {
            [self startAnimation];
        }else{
            self.isOrNoShowShake = NO;
            [UIView animateWithDuration:0.3 animations:^{
                self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight-self.feedBackView.bounds.size.height-kbSize.height, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
            }];
        }
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    _isList = NO;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        
        if (!_yaoyiyao) {
            [self stopAnimation];
        }else{
            self.isOrNoShowShake = YES;
            [UIView animateWithDuration:0.3 animations:^{
                self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
            }];
        }
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    _lastContentOffset = scrollView.contentOffset.y;
    //    _beginScrollViewY = scrollView.contentOffset.y;
}
//always
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y< _lastContentOffset )
    {//向下拉
        if (scrollView.tag == 1001) {
            //            NSLog(@"---%f",self.commicCounts.contentOffset.y);
            if (self.commicCounts.contentOffset.y<0) {
                CGPoint offset = self.commicCounts.contentOffset;
                offset.y = 0;
                self.commicCounts.contentOffset = offset;
                [UIView animateWithDuration:0.4 animations:^{
                    self.tableViewUI.contentOffset = CGPointMake(0, 0);
                }];
                
            }
        }else{
            if (_tableView.contentOffset.y<0) {
                CGPoint offset = self.commicCounts.contentOffset;
                offset.y = 0;
                _tableView.contentOffset = offset;
                [UIView animateWithDuration:0.4 animations:^{
                    self.tableViewUI.contentOffset = CGPointMake(0, 0);
                }];
            }
        }
    }
    
    // 计算当前页数
    int currentPage = _collectionView.contentOffset.x / (KScreenWidth*3/4);
    _pageC.currentPage = currentPage;
    
    CGPoint offset = self.tableViewUI.contentOffset;
    CGRect bounds = self.tableViewUI.bounds;
    CGSize size = self.tableViewUI.contentSize;
    UIEdgeInsets inset = self.tableViewUI.contentInset;
    CGFloat currentOffset = offset.y + bounds.size.height - inset.bottom;
    CGFloat maximumOffset = size.height;
    if (currentOffset >maximumOffset) {
        CGPoint offset = self.tableViewUI.contentOffset;
        offset.y = self.topView.height-64;
        self.tableViewUI.contentOffset = offset;
        
    }else if (currentOffset >= maximumOffset - 100 && currentOffset <= maximumOffset) {
        self.topView.alpha = (maximumOffset - currentOffset - 30)/100*1.0;
        _titleName.alpha = 1.0 - (maximumOffset - currentOffset)/100*1.0;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        self.commicCounts.scrollEnabled = YES;
        _tableView.scrollEnabled = YES;
    }else{
        self.topView.alpha = 1.0;
        _titleName.alpha = 0;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
        self.commicCounts.scrollEnabled = NO;
        _tableView.scrollEnabled = NO;
    }
    
    if (currentOffset >= maximumOffset -3) {
        self.commicCounts.scrollEnabled = YES;
        _tableView.scrollEnabled = YES;
    }else{
        self.commicCounts.scrollEnabled = NO;
        _tableView.scrollEnabled = NO;
    }
    
    if (self.tableViewUI.contentOffset.y<0) {
        CGRect frame = self.bgCartoonImageView.frame;
        frame.size.height = 191-self.tableViewUI.contentOffset.y;
        frame.origin.y = self.tableViewUI.contentOffset.y;
        self.bgCartoonImageView.frame = frame;
        self.bgBigView.frame = frame;
    }
    if (self.tableViewUI.contentOffset.y + KScreenheight>= maximumOffset) {
        CGRect frame = self.bgBigView1.frame;
        frame.size.height = self.tableViewUI.contentOffset.y-251 + 1;
        self.bgBigView1.frame = frame;
    }
}


#pragma mark - UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _faceDataArr.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"faceCell";
    FaceBagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.model = _faceDataArr[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KScreenWidth-75)/7.0, 185/5.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 5, 5);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
#warning - 表情点击
    NSInteger row = [indexPath row];
    _inputTextField.text = [NSString stringWithFormat:@"%@[%@]",_inputTextField.text,_faceNameArr[row]];
}

#pragma mark - UITextFieldDelegate

- (void)touchBgViewTouchInView {
    _textFieldView.hidden = YES;
    _bgBlackView.hidden = YES;
    [_inputTextField resignFirstResponder];
    if ([_inputTextField.text isEqualToString:@""]) {
        _isReply = NO;
    }
    
    self.touchBgView.hidden = YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    _yaoyiyao = NO;
    _textFieldView.hidden = NO;
    textView.text = _inputTextField.text;
    self.touchBgView.hidden = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
     [textField resignFirstResponder];
    _textFieldView.hidden = YES;
    _bgBlackView.hidden = YES;
    if (!textField.text.length) {
        self.touchBgView.hidden = YES;
        return YES;
    }
        [self commentMessage:textField.text];
    textField.text = @"";
    self.touchBgView.hidden = YES;
    return YES;
}

- (void)startAnimation {
    CGRect frame1 = _textFieldView.frame;
    frame1.origin.y = KScreenheight - _keyboardHeight-293*_Scale6;
    [UIView animateWithDuration:0.3 animations:^{
//        [self.tableViewUI setContentOffset:CGPointMake(0, self.topView.height-64)];
        _textFieldView.frame = frame1;
    }];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view bringSubviewToFront:_textFieldView];
    }];
}

- (void)stopAnimation {
    CGRect frame1 = _textFieldView.frame;
    
    frame1.origin.y = KScreenheight - 293*_Scale6;
    
    [UIView animateWithDuration:0.3 animations:^{
        _textFieldView.frame = frame1;
    }];
    _yaoyiyao = YES;
}

#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        [[LogHelper shared] writeToFilefrom_page:@"nl" from_section:@"c" from_step:@"r" to_page:@"pl" to_section:@"c" to_step:@"r" type:@"comment" id:[NSString stringWithFormat:@"%ld",_cartoonId]];
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

#pragma mark - tableView代理
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 100) {
        return 0;
    }else{
        if (_dataArr.count == 0) {
            _tableView.rowHeight = KScreenheight - 375+120;
            return 1;
        }else{
            return _dataArr.count;
        }
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 100) {
        return 100;
    }else{
        if (_dataArr.count == 0) {
            return KScreenheight - 375+120;
        }else{
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            paragraphStyle.lineSpacing = 5;
            NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:16],NSParagraphStyleAttributeName:paragraphStyle};
            NSString *cententString = [[_commentListArr objectAtIndex:indexPath.row] objectForKey:@"content"];
            //#warning 正则表达式 "\\[[^\\]]+\\]"--"\\#[^\\#]+\\#"
            NSString *regexString = @"\\[[^\\]]+\\]";
            NSMutableAttributedString *attri = [[NSMutableAttributedString alloc]initWithString:@""];//_model.content
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
            NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
            if (arrr.count>0) {
                for (int i =0 ; i < arrr.count; i ++) {
                    NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
                    NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
                    NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
                    [attri appendAttributedString:attrStr1];
                    NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
                    for (int j = 0; j < _faceNameArr.count; j ++) {
                        if ([faceStr isEqualToString:_faceNameArr[j]]) {
                            NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                            attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                            attch.bounds = CGRectMake(0, -2, 30, 30);
                            NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                            [attri appendAttributedString:string1];
                            break;
                        }
                    }
                    cententString = [cententString substringFromIndex:result.range.location+result.range.length];
                }
                NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
                [attri appendAttributedString:attrStr2];
                
                _testView.attributedText = attri;
                NSLog(@"---%f",_testView.contentSize.height);
                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                    [_testView.layoutManager ensureLayoutForTextContainer:_testView.textContainer];
                    CGRect textFrame=[[_testView layoutManager]usedRectForTextContainer:[_testView textContainer]];
                    float height = textFrame.size.height;
                    return height+50+16;
                }else{
                    return _testView.contentSize.height+50;
                }            }else{
                NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString attributes:attributes];
                _testView.attributedText = attrStr3;
                NSLog(@"---%f",_testView.contentSize.height);
                if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7) {
                    [_testView.layoutManager ensureLayoutForTextContainer:_testView.textContainer];
                    CGRect textFrame=[[_testView layoutManager]usedRectForTextContainer:[_testView textContainer]];
                    float height = textFrame.size.height;
                    return height+50+16;
                }else{
                    return _testView.contentSize.height+50;
                }
            }
            
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.tag == 100) {
        UITableViewCell *cell = [[UITableViewCell alloc]init];
        return cell;
    }else{
        CommentTableViewCell *cell = [[CommentTableViewCell alloc]init];
        if (_dataArr.count == 0) {
            UIImageView * image = [[UIImageView alloc]initWithFrame:CGRectMake(KScreenWidth/2.0 - 40, 40, 80, 100)];
            image.image = [UIImage imageNamed:@"0pinglun.png"];
            [cell.contentView addSubview:image];
            UILabel *oneLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, image.bottom+30, KScreenWidth, 20)];
            oneLabel.text = @"第一个评论的勇士快快粗线(￣3￣)!";
            oneLabel.textAlignment = 1;
            [cell.contentView addSubview:oneLabel];
        }else{
            cell.showAllContent = YES;
            cell.model = _dataArr[indexPath.row];
            
            _replyCommentBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-80, 8, 30, 30)];
            [_replyCommentBtn setImage:[UIImage imageNamed:@"huifu"] forState:UIControlStateNormal];
//            _replyCommentBtn.tag = [indexPath row];
//            [_replyCommentBtn addTarget:self action:@selector(replyBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:_replyCommentBtn];
            
            _praiseCommentBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-120, 5, 30, 30)];
            [_praiseCommentBtn setImage:[UIImage imageNamed:@"btn_dianzan"] forState:UIControlStateNormal];
//            _praiseCommentBtn.tag = [indexPath row];
//            [_praiseCommentBtn addTarget:self action:@selector(praiseComment:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:_praiseCommentBtn];
            if ([g_App.userID isEqualToString:cell.model.userIDInfo[@"id"]]) {
                _deleteCommentBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-40, 6, 30, 30)];
                [_deleteCommentBtn setImage:[UIImage imageNamed:@"qinli_2"] forState:UIControlStateNormal];
                _deleteCommentBtn.tag = [indexPath row];
                [_deleteCommentBtn addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:_deleteCommentBtn];
            }else{
                _replyCommentBtn.frame = CGRectMake(ScreenSizeWidth-40, 8, 30, 30);
                _praiseCommentBtn.frame = CGRectMake(ScreenSizeWidth-80, 5, 30, 30);
            }
            
            _praiseNumLabel = [CustomTool createLabelWithFrame:CGRectMake(_praiseCommentBtn.right-3, 17, 20, 10) andType:10 andColor:CUSTOMTITLECOLOER3];
            _praiseNumLabel.text = _commentListArr[indexPath.row][@"praiseCount"];
            [cell.contentView addSubview:_praiseNumLabel];
            _replyNumLabel = [CustomTool createLabelWithFrame:CGRectMake(_replyCommentBtn.right-3, 17, 20, 10) andType:10 andColor:CUSTOMTITLECOLOER3];
            _replyNumLabel.text = _commentListArr[indexPath.row][@"commentCount"];
            [cell.contentView addSubview:_replyNumLabel];
        }
        return cell;
    }
}

- (void)replyBtnPressed:(UIButton*)btn {
    CommentDetailViewController *commentDV = [[CommentDetailViewController alloc]init];
    commentDV.landlordContentDict = _commentListArr[btn.tag];
    commentDV.cartoonInfomationDict = _cartoonInfomationDict;
    [self presentViewController:commentDV animated:YES completion:nil];
}

-(void)showAlert:(UIButton *)btn{
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cc" to_section:@"c" to_step:@"r" type:@"delete" id:[NSString stringWithFormat:@"%ld",btn.tag
                                                                                                                                                  ]];
    _deleteRow = btn.tag;
    _deleteId = _commentListArr[_deleteRow][@"id"];
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"您是否确定删除此帖子" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.center = CGPointMake(KScreenWidth/2,KScreenheight-100);
    alertView.tag = 100;
    _isAlertShow = YES;
    [alertView show];
}

-(void)deleteComments{
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_DELETE,@"r",
                                    _deleteId,@"id",nil
                                    ];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(deleteCommentsFinish:) method:POSTDATA];
}

- (void)deleteCommentsFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
        [_dataArr removeObjectAtIndex:_deleteRow];
        [_commentListArr removeObjectAtIndex:_deleteRow];
        [_tableView reloadData];
        //        [self misDeleteView];
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    [[LogHelper shared] writeToFilefrom_page:@"cc" from_section:@"c" from_step:@"l" to_page:@"cpl" to_section:@"c" to_step:@"d" type:@"" id:@"0"];
    _yaoyiyao = NO;
    CommentDetailViewController *commentDV = [[CommentDetailViewController alloc]init];
    commentDV.landlordContentDict = _commentListArr[indexPath.row];
    commentDV.cartoonInfomationDict = _cartoonInfomationDict;
    [self presentViewController:commentDV animated:YES completion:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
-(void)setNavBar{
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 64)];
    [self.view addSubview:view];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 20, 100, 38);//5, 25, 33, 33
    [backButton setImage:[UIImage imageNamed:@"btn_fanhui"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 65)];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    view.backgroundColor = [UIColor clearColor];
    [view addSubview:backButton];
    
    //元素项
    _titleName = [[UILabel alloc]initWithFrame:CGRectMake(60, 33, KScreenWidth-120, 30)];
    _titleName.center = CGPointMake(ScreenSizeWidth/2, 42);
    //    _titleName.size = CGSizeMake(200, 50);
    _titleName.font = CUSTOMTITLESIZE20;
    _titleName.textColor = CUSTOMREDCOLOR;
    _titleName.alpha = 0;
    _titleName.textAlignment = NSTextAlignmentCenter;
    [view addSubview:_titleName];
    
    UIButton *downLoadBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    downLoadBtn1.frame = CGRectMake(KScreenWidth - 38, 25, 33, 33);
    [downLoadBtn1 setImage:[UIImage imageNamed:@"btn_xiazair"] forState:UIControlStateNormal];
    [downLoadBtn1 addTarget:self action:@selector(downLoadViewControl:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:downLoadBtn1];
    
    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shareBtn.frame = CGRectMake(ScreenSizeWidth - 80, 25, 33, 33);
    [shareBtn setImage:[UIImage imageNamed:@"btn_fenxiang"] forState:UIControlStateNormal];
    [shareBtn addTarget:self action:@selector(shareCartoon:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:shareBtn];
}

-(void)shareCartoon:(UIButton *)btn{
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cds" to_section:@"c" to_step:@"r" type:@"" id:_valueId];
    
    if (_dataDic == nil) {
        return;
    }
    [UMSocialData defaultData].extConfig.qzoneData.title = [_dataDic objectForKey:@"name"];;
    [UMSocialData defaultData].extConfig.qzoneData.url = [_dataDic objectForKey:@"shareUrl"];//shareUrl
    
    [UMSocialData defaultData].extConfig.qqData.title = [_dataDic objectForKey:@"name"];;
    [UMSocialData defaultData].extConfig.qqData.url = [_dataDic objectForKey:@"shareUrl"];
    
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = [_dataDic objectForKey:@"name"];;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = [_dataDic objectForKey:@"shareUrl"];
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = [_dataDic objectForKey:@"name"];;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [_dataDic objectForKey:@"shareUrl"];
    
    
    [UMSocialData defaultData].extConfig.sinaData.shareText = [NSString stringWithFormat:@"%@%@分享自@麦萌",[_dataDic objectForKey:@"name"],[_dataDic objectForKey:@"shareUrl"]];
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:[_dataDic objectForKey:@"images"]];
    
    //    [UMSocialData defaultData].shareImage =self.commicImage.image;
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENGKEY
                                      shareText:[_dataDic objectForKey:@"shareContent"]
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:@[UMShareToSina, UMShareToQQ, UMShareToQzone, UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:self];
    
    NSDictionary *dict = @{@"shareId" : [_dataDic objectForKey:@"id"]};
    //    [MobClick event:@"shareEvent" attributes:dict];
}

- (void)downLoadViewControl:(UIButton *)btn {
    _isRefresh = NO;
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cdd" to_section:@"c" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%d",_cartoonId]];
    DownLoadViewController *downLoadVC = [[DownLoadViewController alloc] init];
    downLoadVC.charpList = [cartoonChapterListModel parasArr:_dataDic[@"cartoonChapterList"]];
    downLoadVC.allInfo = _allInfo;
    downLoadVC.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    [self presentViewController:downLoadVC animated:YES completion:nil];
}

#pragma mark - 加载数据
-(void)loadCommentData{
    _dataArr = [NSMutableArray array];
    NSString *urlString = [NSString stringWithFormat:@"%@r=%@&id=%@&withReply=0&size=999&page=%ld",INTERFACE_PREFIXD,API_URL_CARTOONSET_CONTENTLIST,_allInfo[0],_page];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:urlString parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        _commentListArr = backData[@"results"];
        for (NSDictionary *dict in _commentListArr) {
            CommentModel *model = [[CommentModel alloc]init];
            [model setValuesForKeysWithDictionary:dict];
            [_dataArr addObject:model];
        }
        [_tableView reloadData];
        _isLoadComment = NO;
        
        if ([self.type isEqualToString:@"4"]) {
            for (int i = 0; i < _dataArr.count; i ++) {
                CommentModel *model1 = _dataArr[i];
                if ([model1.id isEqualToString:self.contentID] ) {
                    //                    _tableView.contentOffset = CGPointMake(0, 400);
                    self.tableViewUI.contentOffset = CGPointMake(0, 251);
                    //                    self.bottomView.frame = CGRectMake(0, 64, ScreenSizeWidth, ScreenSizeHeight);
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
                    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
                    [_whiteView removeFromSuperview];
                }
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

//通知跳
//- (void)


- (void)commentMessage:(NSString*)msg {
    //    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cd" to_page:@"" to_section:@"c" to_step:@"c" type:@"i" channel:@"" id:[NSString stringWithFormat:@"%d",_cartoonId]];
    
    [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"c" from_step:@"r" to_page:@"cc" to_section:@"c" to_step:@"a" type:@"comment" id:[NSString stringWithFormat:@"%d",_cartoonId]];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_COMMENT_MESSAGE,@"r",
                                    @"4",@"type",
                                    [NSString stringWithFormat:@"%ld",(long)_cartoonId],@"valueID",
                                    msg,@"content",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(commentMessageFinish:) method:POSTDATA];
}

- (void)commentMessageFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [self messageDetail];
    }
}
-(void)praiseComment:(UIButton *)btn{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"content/praise",@"r",
                                    _commentListArr[btn.tag][@"id"],@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(praiseCommentFinished:) method:POSTDATA];
}
- (void)praiseCommentFinished:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [self loadCommentData];
    }
}
- (void)messageDetail {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CARTOONSET_LIST,@"r",
                                    [NSString stringWithFormat:@"%ld",(long)_cartoonId],@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(messageDetailFinish:) method:GETDATA];
}

- (void)messageDetailFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _commentListArr = [NSMutableArray array];
        [_dataArr removeAllObjects];
        NSDictionary *dict = dic[@"results"];
        _commentListArr = dict[@"commentList"];
        for (NSDictionary *dict in _commentListArr) {
            CommentModel *model = [[CommentModel alloc]init];
            [model setValuesForKeysWithDictionary:dict];
            [_dataArr addObject:model];
        }
        [_tableView reloadData];
    } else {
        if ([[dic objectForKey:@"code"] integerValue] == 51) {
            CustomAlertView * alertView=[[CustomAlertView alloc]initWithTitle:@"脾气差的提示君" message:[dic objectForKey:@"error"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }
    }
}

#pragma mark -
-(void)misBack{
    _changeCollection();
    
    if ([self.customType isEqualToString:@"5"]) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CONTINUEREADINRECOMMENT"] isEqualToString:@"1"]) {
            SecondReadController *sec = [[SecondReadController alloc]init];
            [sec.scrollView removeFromSuperview];
            [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"CONTINUEREADINRECOMMENT"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self dismissViewControllerAnimated:NO completion:^{
                
            }];
        }
        else{
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }
    
}




-(void)downloadView{
    NSLog(@"调转到下载界面");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDetailStr:(NSString *)detailStr {
    _detailStr = detailStr;
    _urlStr = detailStr;
    [self reqiureData];
}

- (void)setDetailModel:(DetailsModel *)detailModel {
    _detailModel = detailModel;
    _urlStr = [NSString stringWithFormat:@"%@r=%@&id=%d",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,[detailModel.id intValue]];
    //    NSLog(@"%@",_urlStr);
    [self reqiureData];
    
}

-(void)setValueId:(NSString *)valueId{
    _valueId = valueId;
    _urlStr = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_valueId];
    //    NSLog(@"%@",_urlStr);
    [self reqiureData];
}

- (NSString *)getUUID
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc]
                                         
                                         initWithIdentifier:@"UUID"
                                         
                                         accessGroup:@"YOUR_BUNDLE_SEED.com.yourcompany.userinfo"];
    
    
    NSString *strUUID = [keychainItem objectForKey:(id)CFBridgingRelease(kSecValueData)];
    
    //首次执行该方法时，uuid为空
    if ([strUUID isEqualToString:@""]) {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
        
        [keychainItem setObject:strUUID forKey:(id)CFBridgingRelease(kSecValueData)];
        
    }
    return strUUID;
}
- (UIImage*) blur:(UIImage*)theImage
{
    // create our blurred image
    CIContext *context = [CIContext contextWithOptions:nil];
    CIImage *inputImage = [CIImage imageWithCGImage:theImage.CGImage];
    
    CIFilter *affineClampFilter = [CIFilter filterWithName:@"CIAffineClamp"];
    CGAffineTransform xform = CGAffineTransformMakeScale(1.0, 1.0);
    [affineClampFilter setValue:inputImage forKey:kCIInputImageKey];
    [affineClampFilter setValue:[NSValue valueWithBytes:&xform
                                               objCType:@encode(CGAffineTransform)]
                         forKey:@"inputTransform"];
    
    CIImage *extendedImage = [affineClampFilter valueForKey:kCIOutputImageKey];
    
    // setting up Gaussian Blur (could use one of many filters offered by Core Image)
    CIFilter *blurFilter = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blurFilter setValue:extendedImage forKey:kCIInputImageKey];
    [blurFilter setValue:[NSNumber numberWithFloat:10.0f] forKey:@"inputRadius"];
    CIImage *result = [blurFilter valueForKey:kCIOutputImageKey];
    
    // CIGaussianBlur has a tendency to shrink the image a little,
    // this ensures it matches up exactly to the bounds of our original image
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage *returnImage = [UIImage imageWithCGImage:cgImage];
    //create a UIImage for this function to "return" so that ARC can manage the memory of the blur...
    //ARC can't manage CGImageRefs so we need to release it before this function "returns" and ends.
    CGImageRelease(cgImage);//release CGImageRef because ARC doesn't manage this on its own.
    
    return returnImage;
}

- (void)asyRequire:(NSDictionary *)dict {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    //    if (![[dict objectForKey:@"code"] integerValue]) {
    //
    //    downLoadBtn.hidden = NO;
    //    _commentListArr = [NSMutableArray array];
    //    _netImageView.alpha = 0;
    //    _isShow = YES;
    //    NSLog(@"%@",dict);
    _dataRequireArr = [NSMutableArray array];
    for (chapterIdModel *chapModel in  [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:[NSString stringWithFormat:@"%ld",(long)_cartoonId]])  {
        //        NSLog(@"chapterId%@",chapModel.chapterId);
        if ([chapModel.status isEqualToString:@"1"]) {
            [_dataRequireArr addObject:chapModel.chapterId];
        }
    }
    
    
    if (dict) {
        NSDictionary *dic = dict[@"results"];
        _cartoonInfomationDict = [NSDictionary dictionaryWithDictionary:dic];
        cartoonChapListArr = dic[@"cartoonChapterList"];
        _CartoonInfoAllModel = [[CartoonInfoAll alloc] init];
        _CartoonInfoAllModel.id = dic[@"id"];
        _CartoonInfoAllModel.name = dic[@"name"];
        _CartoonInfoAllModel.images = dic[@"images"];
        _CartoonInfoAllModel.author = dic[@"author"];
        _CartoonInfoAllModel.introduction = dic[@"introduction"];
        _CartoonInfoAllModel.priority = dic[@"priority"];
        _CartoonInfoAllModel.totalChapterCount = dic[@"totalChapterCount"];
        _CartoonInfoAllModel.updateInfo = dic[@"updateInfo"];
        _CartoonInfoAllModel.recentUpdateTime = dic[@"recentUpdateTime"];
        _CartoonInfoAllModel.updateValueLabel = dic[@"updateValueLabel"];
        [[DataBaseHelper shared] insertExistCartoonInfo:_CartoonInfoAllModel andValue:@{@"collectionStatus":@"",@"collectTime": [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]],@"updateValueLabel":dic[@"updateValueLabel"]}];
        
        //        [cartoonChapterListModel parasArr:cartoonChapListArr];
        
        NSLog(@"%@",dic);
        _allInfo = @[dict[@"params"][@"id"],dic[@"name"],dic[@"images"]];
        
        
        
        NSLog(@"----%@",_dataRequireArr);
        _commentListArr = dic[@"commentList"];
        _titleName.text = dic[@"name"];
        _allDict = dict;
        _dataDic = dic;
        _cartoonChapterCount = [dic[@"cartoonChapterCount"] intValue];
        [_commicImage setImageWithURL:[NSURL URLWithString:dic[@"images"]]];
        //        self.bgCartoonImageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"images"]]]]];
        self.bgCartoonImageView.image = [self blur:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"images"]]]]]];
        
        
        
        
        
        _commicAuthor.text = [NSString stringWithFormat:@"  作者:%@",dic[@"author"]];
        _updateValueLabel.text = [NSString stringWithFormat:@"  更新时间:%@",dic[@"updateValueLabel"]];
        //--分割字符串
        NSString *stt = [NSString stringWithFormat:@"%@",dic[@"recentUpdateTime"]];
        if (stt.length >=10) {
            stt = [stt substringToIndex:10];
        }
        _updateTime.text = [NSString stringWithFormat:@"  最新更新时间:%@",stt];
        _commicDes.text = dic[@"introduction"];
        _contenttLabel.text = dic[@"introduction"];
        _commicName.text = [NSString stringWithFormat:@"  %@",dic[@"name"]];
        _titleName.text = [NSString stringWithFormat:@"  %@",dic[@"name"]];
        _cartoonId = [dic[@"id"] intValue];
        
        if ( [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"id":[NSString stringWithFormat:@"%ld",(long)_cartoonId]} andChooseType:@"lastReadTime"].count > 0) {
            cartoonInfoAll = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"id":[NSString stringWithFormat:@"%ld",(long)_cartoonId]} andChooseType:@"lastReadTime"][0];
            
        }
        
        if([cartoonInfoAll.collectionStatus isEqualToString:@"1"]){
            _addSav.selected = YES;
        }
        
        if ([cartoonInfoAll.isRead isEqualToString:@"1"]) {
            _countinueWatch.selected = YES;
        }
        
        CGSize desSize = [self sizeOfStr:_commicDes.text andFont:CUSTOMTITLESIZE14 andMaxSize:CGSizeMake(ScreenSizeWidth-16, 99999) andLineBreakMode:0];
        if(desSize.height < 55){
            _updownButton.hidden = YES;
        }
        NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
        NSString *chapters = [userDefaultes stringForKey:@"chapterId"];
        int number = [chapters intValue];
        [cartoonChapterListModel parasArr:dic[@"cartoonChapterList"]];
        chapteID = cartoonChapListArr[0][@"id"];
        CGFloat joinComicWidth = KScreenWidth/4;
        for(int i = 0; i < cartoonChapListArr.count; i++) {
            
            UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
            joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 40 * (i/4), joinComicWidth - 10, 35);
            joinComic.backgroundColor = [UIColor whiteColor];
            joinComic.layer.cornerRadius = 8.0;
            joinComic.layer.borderWidth = 1.0;
            joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
            joinComic.titleLabel.font = CUSTOMTITLESIZE12;
            [joinComic setTitle:[NSString stringWithFormat:@"%@",cartoonChapListArr[i][@"priority"]] forState:UIControlStateNormal];
            [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
            joinComic.tag = [cartoonChapListArr[i][@"id"] integerValue];
            [joinComic addTarget:self action:@selector(joinComicForce:) forControlEvents:UIControlEventTouchUpInside];
            if ([cartoonChapListArr[i][@"id"] isEqualToString:cartoonInfoAll.currentReadChapterId]) {
                
                UIImageView *havenWatch = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 10, 14)];
                havenWatch.image = [UIImage imageNamed:@"yuanlaibiaoqian"];
                [joinComic addSubview:havenWatch];
            }
            
            if([_dataRequireArr indexOfObject:cartoonChapListArr[i][@"id"]] != NSNotFound){
                UIImageView *imageSaveData = [[UIImageView alloc] initWithFrame:CGRectMake(KScreenWidth/4-10-15, 22, 10, 10)];
                imageSaveData.image = [UIImage imageNamed:@"xiazai_pic"];
                [joinComic addSubview:imageSaveData];
            }
            
            [_commicCounts addSubview:joinComic];
            [_subButtons addObject:joinComic];
        }
        if ([self.type isEqualToString:@"4"]){
            UIButton *btn = [[UIButton alloc]init];
            btn.tag = 101;
            [self clike:btn];
        }
        
//        UILabel *cartoonNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KScreenWidth/3, 248, KScreenWidth/3, 30)];
//        cartoonNameLabel.text = [NSString stringWithFormat:@"《%@》",dic[@"name"]];
//        cartoonNameLabel.textColor = LIGHTTEXTCOLOR160;
//        cartoonNameLabel.font = CUSTOMTITLESIZE16;
//        cartoonNameLabel.textAlignment = 2;
//        [_textFieldView addSubview:cartoonNameLabel];
//        
//        
//        UILabel *AuthNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(KScreenWidth*2/3 - 15, 248, KScreenWidth/3, 30)];
//        AuthNameLabel.text = [NSString stringWithFormat:@"作者 :%@",dic[@"author"]];
//        AuthNameLabel.textColor = LIGHTTEXTCOLOR160;
//        AuthNameLabel.font = CUSTOMTITLESIZE16;
//        AuthNameLabel.textAlignment = 2;

        UILabel *label = [CustomTool createLabelWithFrame:CGRectMake(80, _inputTextField.bottom+6*_Scale6, ScreenSizeWidth-80-15, 30) andType:16 andColor:LIGHTTEXTCOLOR160];
        label.textAlignment = 2;
        label.text = [NSString stringWithFormat:@"《%@》作者 :%@",dic[@"name"],dic[@"author"]];
        
        [_textFieldView addSubview:label];

        
    }else{
        //        _isShow = NO;
        //        _netImageView.alpha = 1;
        //        _refreshBtn1 = [CustomTool createBtn];
        //        [_netImageView addSubview:_refreshBtn1];
        //        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight - 64);
        //        _refreshBtn1.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 64 - 80, 100, 30);
        //        [_refreshBtn1 addTarget:self action:@selector(reqiureData) forControlEvents:UIControlEventTouchUpInside];
        if([[DataBaseHelper shared] isExistChapList:[NSString stringWithFormat:@"%d",_cartoonId]]){
            cartoonInfoAll = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"id":[NSString stringWithFormat:@"%d",_cartoonId]} andChooseType:@"lastReadTime"][0];
            
            _allInfo = @[cartoonInfoAll.id,cartoonInfoAll.name,cartoonInfoAll.images];
            
            
            
            NSLog(@"----%@",_dataRequireArr);
            _titleName.text = cartoonInfoAll.name;
            _allDict = dict;
            [_commicImage setImageWithURL:[NSURL URLWithString:cartoonInfoAll.images]];
            self.bgCartoonImageView.image = [self blur:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",cartoonInfoAll.images]]]]];
            
            
            
            
            
            //        _commicName.text = [NSString stringWithFormat:@"  %@",dic[@"name"]];//dic[@"name"];
            _commicAuthor.text = [NSString stringWithFormat:@"  作者:%@",cartoonInfoAll.author] ;
            _updateValueLabel.text = [NSString stringWithFormat:@"  更新时间:%@",cartoonInfoAll.updateValueLabel];
            //--分割字符串
            NSString *stt = [NSString stringWithFormat:@"%@",cartoonInfoAll.recentUpdateTime];
            if (stt.length >=10) {
                stt = [stt substringToIndex:10];
            }
            _updateTime.text = [NSString stringWithFormat:@"  最新更新时间:%@",stt];
            _commicDes.text = cartoonInfoAll.introduction;
            _contenttLabel.text = cartoonInfoAll.introduction;
            _commicName.text = [NSString stringWithFormat:@"  %@",cartoonInfoAll.name];
            _titleName.text = [NSString stringWithFormat:@"  %@",cartoonInfoAll.name];
            _cartoonId = [cartoonInfoAll.id intValue];
            
            if ([[DataBaseHelper shared] isExistsSaveHistory:_allInfo[0] andIsStatus:@{@"collectionStatus":@"1"}]) {
                _addSav.selected = YES;
            }
            
            if ([cartoonInfoAll.isRead isEqualToString:@"1"]) {
                _countinueWatch.selected = YES;
            }
            
            
            //        if ([dic[@"collectionStatus"] isEqualToString:@"1"]) {
            //            _addSav.selected = YES;
            //        }
            //        if ([dic[@"currentReadChapterId"] length] > 0) {
            //            _countinueWatch.selected = YES;
            //        }
            CGSize desSize = [self sizeOfStr:_commicDes.text andFont:CUSTOMTITLESIZE14 andMaxSize:CGSizeMake(ScreenSizeWidth-16, 99999) andLineBreakMode:0];
            if(desSize.height < 55){
                _updownButton.hidden = YES;
            }
            NSUserDefaults *userDefaultes = [NSUserDefaults standardUserDefaults];
            NSString *chapters = [userDefaultes stringForKey:@"chapterId"];
            int number = [chapters intValue];
            //        [cartoonChapterListModel parasArr:dic[@"cartoonChapterList"]];
            CGFloat joinComicWidth = KScreenWidth/4;
            NSMutableArray *dataBaseArr = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%d",_cartoonId]}];
            chapteID = [dataBaseArr[0] id];
            
            for(int i = 0; i < dataBaseArr.count; i++) {
                
                cartoonChapterListModel *cartoonModel = dataBaseArr[i];
                UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
                joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 40 * (i/4), joinComicWidth - 10, 35);
                joinComic.backgroundColor = [UIColor whiteColor];
                joinComic.layer.cornerRadius = 8.0;
                joinComic.layer.borderWidth = 1.0;
                joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
                joinComic.titleLabel.font = CUSTOMTITLESIZE12;
                [joinComic setTitle:[NSString stringWithFormat:@"%@",cartoonModel.priority] forState:UIControlStateNormal];
                [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
                joinComic.tag = [[dataBaseArr[i] id] integerValue];
                [joinComic addTarget:self action:@selector(joinComicForce:) forControlEvents:UIControlEventTouchUpInside];
                if ([cartoonInfoAll.currentReadChapterId isEqualToString:cartoonModel.id]) {
                    
                    UIImageView *havenWatch = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 10, 14)];
                    havenWatch.image = [UIImage imageNamed:@"yuanlaibiaoqian"];
                    [joinComic addSubview:havenWatch];
                    //                tagMarkCartoon = [cartoonInfoAll.currentReadChapterId integerValue];
                    
                }
                NSLog(@"databaseA%@",[dataBaseArr[i]id]);
                if([_dataRequireArr indexOfObject:[dataBaseArr[i] id]] != NSNotFound){
                    UIImageView *imageSaveData = [[UIImageView alloc] initWithFrame:CGRectMake(KScreenWidth/4-10-15, 22, 10, 10)];
                    imageSaveData.image = [UIImage imageNamed:@"xiazai_pic"];
                    [joinComic addSubview:imageSaveData];
                }
                
                [_commicCounts addSubview:joinComic];
                [_subButtons addObject:joinComic];
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
            }
            
            
            
            
            
        }}
    _commicCounts.delegate = self;
    _commicCounts.contentSize = CGSizeMake(0, (_cartoonChapterCount/4) * 45);
    
    
    
    
}

- (void)reqiureData {
    //    if (_isRefresh &&_isShow) {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //    }
    
    _dataIDArr = [[NSMutableArray alloc] init];
    _subButtons = [[NSMutableArray alloc] init];
    //    NSDictionary *headers;
    //    if (g_App.userInfo.userID != nil) {
    //        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
    //    }else {
    //        headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
    //    }
    //    [YK_API_request startGetLoad:_urlStr extraParams:nil object:self action:@selector(asyRequire:) header:headers];
    
    NSArray *urlArr = [_urlStr componentsSeparatedByString:@"&"];
    _cartoonId = [[urlArr[1] substringFromIndex:3] integerValue];
    _urlStr = [_urlStr stringByAppendingString:@"&withContent=0&withChapter=1"];
    
    [YK_API_request startLoad:_urlStr extraParams:nil object:self action:@selector(asyRequire:) method:GETDATA];
    
}



- (void)joinComicForce:(id)sender {
    _isRefresh = NO;
    UIButton *joinComicForceBtn = (UIButton *)sender;
    //    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cd" to_page:@"crd" to_section:@"c" to_step:@"l" type:@"c" channel:@"" id:[NSString stringWithFormat:@"%d",joinComicForceBtn.tag]];
    
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"menu" id:[NSString stringWithFormat:@"%d",joinComicForceBtn.tag]];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"] && [_dataRequireArr indexOfObject:[NSString stringWithFormat:@"%d",joinComicForceBtn.tag ]] == NSNotFound) {
        NSLog(@"+++++%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"]);
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] isEqualToString:@"0"]) {
            UIAlertView *wlanAlert = [[UIAlertView alloc]   initWithTitle:@"提醒" message:@"当前为非WIFI网络，继续阅读将消耗流量。是否继续阅读" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续阅读", nil];
            wlanAlert.tag = joinComicForceBtn.tag;
            [wlanAlert show];
            return;
            
        }
    }
    
    
    
    NSString *cartoonNum = [NSString stringWithFormat:@"%ld",(joinComicForceBtn.tag - 100) + 1];
    NSArray *arr = _dataDic[@"cartoonChapterList"];
    for (NSDictionary *dic in arr) {
        NSString *idStr = dic[@"id"];
        [_dataIDArr addObject:idStr];
    }
    
    //        NSString *strrrr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,[_dataDic[@"currentReadChapterId"] length]>0?_dataDic[@"currentReadChapterId"]:chapteID];
    //    //
    //    NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CHAPTERALBUM_LIST,_dataIDArr[joinComicForceBtn.tag - 100]];
    
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CONTINUEREADINRECOMMENT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,(long)joinComicForceBtn.tag];
    SecondReadController *readC = [[SecondReadController alloc]init];
    readC.charpList = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%ld",(long)_cartoonId]}];
    readC.downLoadCharpList = _dataRequireArr;
    readC.allInfo = _allInfo;
    
    [readC setChangeMark:^(NSInteger markTag) {
        [self changeTagMark];
        self.countinueWatch.selected = YES;
        
    }];
    
    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:readC animated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:str];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"idNum" object:_dataIDArr];
        //        [[NSNotificationCenter defaultCenter] postNotificationName:@"cartoonNum" object:cartoonNum];
    }];}




- (void)changeTagMark {
    NSMutableArray *dataBaseArr = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%d",_cartoonId]}];
    chapteID = [dataBaseArr[0] id];
    CGFloat joinComicWidth = KScreenWidth/4;
    CartoonInfoAll *cartoonContinueW = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"id":[NSString stringWithFormat:@"%d",_cartoonId]} andChooseType:@"lastReadTime"][0];
    
    for (int i = 0; i < _subButtons.count; i ++) {
        [_subButtons[i] removeFromSuperview];
    }
    [_subButtons removeAllObjects];
    for(int i = 0; i < dataBaseArr.count; i++) {
        
        cartoonChapterListModel *cartoonModel = dataBaseArr[i];
        UIButton * joinComic = [UIButton buttonWithType:UIButtonTypeCustom];
        joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 40 * (i/4), joinComicWidth - 10, 35);
        joinComic.backgroundColor = [UIColor whiteColor];
        joinComic.layer.cornerRadius = 8.0;
        joinComic.layer.borderWidth = 1.0;
        joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        joinComic.titleLabel.font = CUSTOMTITLESIZE12;
        [joinComic setTitle:[NSString stringWithFormat:@"%@",cartoonModel.priority] forState:UIControlStateNormal];
        [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        joinComic.tag = [[dataBaseArr[i] id] integerValue];
        [joinComic addTarget:self action:@selector(joinComicForce:) forControlEvents:UIControlEventTouchUpInside];
        if ([cartoonContinueW.currentReadChapterId isEqualToString:cartoonModel.id]) {
            
            UIImageView *havenWatch = [[UIImageView alloc] initWithFrame:CGRectMake(5, 0, 10, 14)];
            havenWatch.image = [UIImage imageNamed:@"yuanlaibiaoqian"];
            [joinComic addSubview:havenWatch];
            //            tagMarkCartoon = [cartoonInfoAll.ReadChapterId integerValue];
            
        }
        NSLog(@"databaseA%@",[dataBaseArr[i]id]);
        if([_dataRequireArr indexOfObject:[dataBaseArr[i] id]] != NSNotFound){
            UIImageView *imageSaveData = [[UIImageView alloc] initWithFrame:CGRectMake(KScreenWidth/4-10-15, 22, 10, 10)];
            imageSaveData.image = [UIImage imageNamed:@"xiazai_pic"];
            [joinComic addSubview:imageSaveData];
        }
        
        [_commicCounts addSubview:joinComic];
        [_subButtons addObject:joinComic];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
    }
    
    
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */



- (void)addSave:(id)sender {
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HAVELOGINED"] isEqualToString:@"1"]&&g_App.userInfo.userID == nil) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以收藏哦~"];
        [alertView show];
        return;
        
    }
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cf" to_section:@"c" to_step:@"a" type:@"" id:[NSString stringWithFormat:@"%d", _cartoonId]];
    UIButton *saveButton = (UIButton *)sender;
    saveButton.selected = !saveButton.selected;
    _isSave = saveButton.selected;
    
    if (saveButton.selected) {
        _CartoonInfoAllModel.collectionStatus = @"1";
        _CartoonInfoAllModel.collectTime = [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]];
        [[DataBaseHelper shared] insertExistCartoonInfo:_CartoonInfoAllModel andValue:@{@"collectionStatus":@"1",@"collectTime": [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]]}];
        //        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cd" to_page:@"" to_section:@"c" to_step:@"d" type:@"i" channel:@"" id:_CartoonInfoAllModel.id];
        
        
        
    }else{
        //        [self.addSav setTitle:@"加入收藏" forState:UIControlStateNormal];
        //        [self.addSav setImage:[UIImage imageNamed:@"soucang"] forState:UIControlStateNormal];
        //        [[DataBaseHelper shared] deleteSaveHistory:_bookModel.id];
        _CartoonInfoAllModel.collectionStatus = @"0";
        [[DataBaseHelper shared] insertExistCartoonInfo:_CartoonInfoAllModel andValue:@{@"collectionStatus":@"0",@"collectTime": [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]]}];
        
        //        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cd" to_page:@"" to_section:@"c" to_step:@"d" type:@"d" channel:@"" id:_CartoonInfoAllModel.id];
        
        
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
    
    
    /*   NSString *saveUrl = [NSString stringWithFormat:@"%@r=%@",INTERFACE_PREFIXD,@"cartoonCollection/add"];
     
     NSString *saveaddUrl = @"http://api.playsm.com/index.php";
     NSDictionary *commicDict = @{@"cartoonId":_allDict[@"params"][@"id"],@"r":(saveButton.selected? @"cartoonCollection/add":@"cartoonCollection/cancel") };
     
     [YK_API_request startLoad:saveaddUrl extraParams:commicDict object:self action:@selector(saveAdd:) method:POSTDATA];*/
}

/*- (void)saveAdd:(NSDictionary *)saveDict {
 if (_isSave) {
 [MBProgressHUD showSuccess:@"收藏成功" toView:self.view];
 }else{
 [MBProgressHUD showSuccess:@"已取消收藏" toView:self.view];
 }
 [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
 }*/

- (void)continueData:(NSDictionary *)continueData{
    NSArray *arr = continueData[@"results"];
    NSString *strrrr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,_dataDic[@"currentReadChapterId"]];
    ReadController *readC = [[ReadController alloc]init];
    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:readC animated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"continueLu1" object:strrrr];
    }];
}

- (IBAction)continueWatch:(id)sender {
    
    _isRefresh = NO;
    
    
    CartoonInfoAll *cartoonContinueW = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"id":[NSString stringWithFormat:@"%d",_cartoonId]} andChooseType:@"lastReadTime"][0];
    [[LogHelper shared] writeToFilefrom_page:@"cd" from_section:@"c" from_step:@"d" to_page:@"cr" to_section:@"c" to_step:@"a" type:@"button" id:[NSString stringWithFormat:@"%@",[cartoonContinueW.currentReadChapterId length]>0?cartoonContinueW.currentReadChapterId:chapteID]];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"] && [_dataRequireArr indexOfObject:_dataDic[@"currentReadChapterId"]] == NSNotFound) {
        NSLog(@"+++++%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"]);
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] isEqualToString:@"0"]) {
            UIAlertView *wlanAlert = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"当前为非WIFI网络，继续阅读将消耗流量。是否继续阅读" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续阅读", nil];
            wlanAlert.tag = 10000;
            _isAlertShow = NO;
            [wlanAlert show];
            return;
        }
    }
    
    //    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cd" to_page:@"crd" to_section:@"c" to_step:@"l" type:@"c" channel:@"" id:[NSString stringWithFormat:@"%@",cartoonContinueW.currentReadChapterId]];
    
    
    
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CONTINUEREADINRECOMMENT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *strrrr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,[cartoonContinueW.currentReadChapterId length]>0?cartoonContinueW.currentReadChapterId:chapteID];
    NSLog(@"===%@",strrrr);
    SecondReadController *readC = [[SecondReadController alloc]init];
    readC.charpList = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%ld",(long)_cartoonId]}];
    readC.allInfo = _allInfo;
    [readC setChangeMark:^(NSInteger markTag) {
        [self changeTagMark];
        self.countinueWatch.selected = YES;
        
    }];
    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:readC animated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:strrrr];
    }];
    
}



- (void)posNavBtn:(id)sender {
    UIButton *posNavBtn = (UIButton *)sender;
    posNavBtn.selected = !posNavBtn.selected;
    NSInteger countButton = _subButtons.count;
    CGFloat joinComicWidth = KScreenWidth/4;
    
    [UIView animateWithDuration:0.0 animations:^{
        for (int i = 0; i < countButton; i ++) {
            UIButton *joinComic =     posNavBtn.selected ?(_subButtons[countButton - i - 1]):(_subButtons[i]);
            joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 40 * (i/4), joinComicWidth - 10, 35);
        }
    } completion:^(BOOL finished) {
        
    }];
    if (!posNavBtn.selected) {
        [posNavBtn setTitle:@" 正序" forState:UIControlStateNormal];
        [posNavBtn setImage:[UIImage imageNamed:@"zheng"] forState:UIControlStateNormal];
    }else{
        [posNavBtn setTitle:@" 反序" forState:UIControlStateNormal];
        [posNavBtn setImage:[UIImage imageNamed:@"fan"] forState:UIControlStateNormal];
    }
    
}

- (CGSize)sizeOfStr:(NSString *)str andFont:(UIFont *)font andMaxSize:(CGSize)size andLineBreakMode:(NSLineBreakMode)mode
{
    NSLog(@"版本号:%f",[[[UIDevice currentDevice]systemVersion]doubleValue]);
    CGSize s;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>=7.0) {
        NSLog(@"ios7以后版本");
        NSMutableDictionary  *mdic=[NSMutableDictionary dictionary];
        [mdic setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
        [mdic setObject:font forKey:NSFontAttributeName];
        s = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                           attributes:mdic context:nil].size;
    }else{
        NSLog(@"ios7之前版本");
        s=[str sizeWithFont:font constrainedToSize:size lineBreakMode:mode];
    }
    return s;
}

- (void)updown:(id)sender {
    
    //    [self.view bringSubviewToFront:_bigDesView];
    [UIView animateWithDuration:0.5 animations:^{
        _bigDesView.frame = CGRectMake(_bigDesView.frame.origin.x, _bigDesView.frame.origin.y, _bigDesView.frame.size.width, 0);
        NSLog(@"%@",NSStringFromCGRect(_bigDesView.frame));
    } completion:^(BOOL finished) {
        
        _bigDesView.hidden = YES;
        [updown removeFromSuperview];
        
    }];
    
}

- (void)downPress:(id)sender {
    
    //    CGSize desSize = [self sizeOfStr:_commicDes.text andFont:[UIFont systemFontOfSize:14.0] andMaxSize:CGSizeMake(ScreenSizeWidth-16, MAXFLOAT) andLineBreakMode:0];
    //---
    CGRect commicFrame = self.commicDes.frame;
    CGRect updownFrame = self.updownButton.frame;
    CGRect topViewFrame = self.topView.frame;
    //---
    CGSize size = [self.commicDes.text sizeWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:CGSizeMake(KScreenWidth-26, 1000) lineBreakMode:0];
    commicFrame.size.height = size.height+10;
    //    commicFrame.size.height = desSize.height;
    updownFrame.origin.y = commicFrame.origin.y+size.height;
    topViewFrame.size.height = topViewFrame.size.height+size.height - 55;
    //
    UIButton *contentBtn = (UIButton *)sender;
    contentBtn.selected = !contentBtn.selected;
    //
    if (!contentBtn.selected) {
        
        [contentBtn setTitle:@" 展开全部" forState:UIControlStateNormal];
        [contentBtn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.1 animations:^{
            self.commicDes.frame = CGRectMake(8, 233, KScreenWidth-16, 60.1);
            self.updownButton.frame = CGRectMake(KScreenWidth-100, self.commicDes.bottom, 100, 25);
            self.topView.frame = CGRectMake(0, 0, KScreenWidth, 315);
            self.tableViewUI.tableHeaderView = self.topView;
        }];
        self.contenttLabel.alpha = 1;
        self.commicDes.alpha =0;
        //        self.tableViewUI.tableHeaderView = self.topView;
    }else{
        self.contenttLabel.alpha = 0;
        self.commicDes.alpha =1;
        [contentBtn setTitle:@" 收起全部" forState:UIControlStateNormal];
        [contentBtn setImage:[UIImage imageNamed:@"shouqiDetail"] forState:UIControlStateNormal];
        [UIView animateWithDuration:.1 animations:^{
            self.commicDes.frame = commicFrame;
            self.updownButton.frame = updownFrame;
            self.topView.frame = topViewFrame;
            self.tableViewUI.tableHeaderView = self.topView;
        }];
    }
}

- (void)test{
    NSLog(@"失败");
}

-(void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reqiureData) name:@"refreshTag" object:nil];
}

-(void)dealloc{//UIKeyboardWillShowNotification
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshTag" object:nil];
}


#pragma mark --alertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (_isAlertShow) {
        if (buttonIndex == 1) {
            [[LogHelper shared] writeToFilefrom_page:@"cc" from_section:@"c" from_step:@"r" to_page:@"cc" to_section:@"c" to_step:@"a" type:@"delete" id:[NSString stringWithFormat:@"%ld",_cartoonId]];
            
            [self deleteComments];
        }
    }else{
        if (alertView.tag != 10000) {
            if (buttonIndex == 0) {
            }else{
                
                //                NSString *strrrr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,[cartoonContinueW.currentReadChapterId length]>0?cartoonContinueW.currentReadChapterId:chapteID];
                //                NSLog(@"===%@",strrrr);
                //                SecondReadController *readC = [[SecondReadController alloc]init];
                //                readC.charpList = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%ld",(long)_cartoonId]}];
                //                readC.allInfo = _allInfo;
                //                [readC setChangeMark:^(NSInteger markTag) {
                //                    [self changeTagMark];
                //                    self.countinueWatch.selected = YES;
                //
                //                }];
                //                readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                //                [self presentViewController:readC animated:YES completion:^{
                //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:strrrr];
                //                }];
                [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CONTINUEREADINRECOMMENT"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSString *cartoonNum = [NSString stringWithFormat:@"%ld",(alertView.tag - 100) + 1];
                NSArray *arr = _dataDic[@"cartoonChapterList"];
                for (NSDictionary *dic in arr) {
                    NSString *idStr = dic[@"id"];
                    [_dataIDArr addObject:idStr];
                }
                NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,(long)alertView.tag];
                SecondReadController *readC = [[SecondReadController alloc]init];
                readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                readC.charpList = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%ld",(long)_cartoonId]}];
                readC.allInfo = _allInfo;
                
                
                [readC setChangeMark:^(NSInteger markTag) {
                    [self changeTagMark];
                    self.countinueWatch.selected = YES;
                    
                    
                }];
                [self presentViewController:readC animated:YES completion:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:str];
                    //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"idNum" object:_dataIDArr];
                    //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cartoonNum" object:cartoonNum];
                }];
            }
        }else{
            if (buttonIndex == 0) {
            }else {
                [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CONTINUEREADINRECOMMENT"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                NSArray *arr = _dataDic[@"cartoonChapterList"];
                NSString *strrrr = [NSString stringWithFormat:@"%@r=%@&chapterId=%@",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,[_dataDic[@"currentReadChapterId"] length]>0?_dataDic[@"currentReadChapterId"]:chapteID];
                SecondReadController *readC = [[SecondReadController alloc]init];
                readC.charpList = [[DataBaseHelper shared] fetchCartoonChapList:@{@"cartoonId":[NSString stringWithFormat:@"%ld",(long)_cartoonId]}];
                readC.allInfo = _allInfo;
                [readC setChangeMark:^(NSInteger markTag) {
                    [self changeTagMark];
                    self.countinueWatch.selected = YES;
                    
                }];
                
                readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [self presentViewController:readC animated:YES completion:^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:strrrr];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"idNum" object:_dataIDArr];
                    //                    [[NSNotificationCenter defaultCenter] postNotificationName:@"cartoonNum" object:cartoonNum];
                }];
            }
            
        }
    }
}
//- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    return UIInterfaceOrientationMaskPortrait;
//}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation

{
    
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate

{
    
    return NO;
    
}

-(BOOL)canBecomeFirstResponder{
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
    
}

//-(BOOL)canBecomeFirstResponder{
//    return YES;
//}

@end
