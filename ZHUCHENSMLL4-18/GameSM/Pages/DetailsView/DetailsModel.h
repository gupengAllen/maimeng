//
//  DetailsModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DetailsModel : NSObject

@property(nonatomic,strong)NSString *episodeNum;

@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *images;
@property(nonatomic,strong)NSString *author;
@property(nonatomic,strong)NSString *categorys;
@property(nonatomic,strong)NSString *createTime;
@property(nonatomic,strong)NSString *modifyTime;
@property(nonatomic,strong)NSString *introduction;
@property(nonatomic,strong)NSString *id;


+(instancetype)modelWithIconName:(NSString *)episodeNum;


@end
