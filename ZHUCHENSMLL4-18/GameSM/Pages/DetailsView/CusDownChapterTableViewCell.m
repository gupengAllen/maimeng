//
//  CusDownChapterTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CusDownChapterTableViewCell.h"
#import "chapterIdModel.h"
#import "GSDownloadManager.h"
@implementation CusDownChapterTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setChapIdModel:(chapterIdModel *)chapIdModel{
    _chapterNameLabel.text = chapIdModel.chapterName;
    _chapIdModel = chapIdModel;
    _chooseDelete.tag = [chapIdModel.chapterId integerValue];
    if ([chapIdModel.status isEqualToString:@"1"]) {
        [_chapterProgress setProgress:1.0];
        _chapterProtion.text = [NSString stringWithFormat:@"%d%%",100];
        [_ONOFFButton  setImage:[UIImage imageNamed:@"gouxuan"] forState:UIControlStateNormal];
        _ONOFFButton.userInteractionEnabled = NO;
    }else {
        [_chapterProgress setProgress:0.0];
        [_ONOFFButton  setImage:[UIImage imageNamed:@"dengdaishizhen"] forState:UIControlStateNormal];
        
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)onOFFButtonClick:(id)sender {
    UIButton *onoff = (UIButton *)sender;
    onoff.selected = !onoff.selected;
    if (onoff.selected) {
        [[GSDownloadManager manager] recoverDownloadWithCartoonId:_chapIdModel.cartoonId chapterId:_chapIdModel.chapterId];
    }
    
}
//- (IBAction)chooseDelete:(id)sender {
//    UIButton *button = (UIButton *)sender;
//    button.selected = YES;
//    NSLog(@"失败");
//}
@end
