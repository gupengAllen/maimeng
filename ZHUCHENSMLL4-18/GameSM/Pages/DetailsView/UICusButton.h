//
//  UICusButton.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/17.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "cartoonChapterListModel.h"
@interface UICusButton : UIButton
@property(nonatomic,retain)cartoonChapterListModel *chapterListModel;
@end
