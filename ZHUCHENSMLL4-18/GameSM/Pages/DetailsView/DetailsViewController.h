//
//  DetailsViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ReadBlock)(int pageNum);


@interface DetailsViewController : UIViewController

@property(nonatomic,strong)ReadBlock readBlock;

@property(nonatomic,strong)NSDictionary *moddel;

@end
