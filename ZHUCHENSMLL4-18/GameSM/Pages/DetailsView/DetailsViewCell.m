//
//  DetailsViewCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "DetailsViewCell.h"
#import "DetailsModel.h"

@interface DetailsViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *episode;

@end

@implementation DetailsViewCell


- (void)awakeFromNib {
    _episode.adjustsLetterSpacingToFitWidth = YES;
}

-(void)drawRect:(CGRect)rect{
//    [super drawRect:rect];
    self.layer.cornerRadius = 4;
    self.clipsToBounds = YES;
    self.layer.borderWidth = 1;
    self.layer.borderColor = [UIColor colorWithRed:240/255.0 green:214/255.0 blue:216/255.0 alpha:1].CGColor;
    
}

-(void)setModel:(DetailsModel *)model{
    _model = model;
    _episode.text = model.episodeNum;
}

-(void)setTitle:(NSString *)title{
    _episode.text = title;
}

+(NSString *)identifier{
    return @"DetailCell";
}

@end
