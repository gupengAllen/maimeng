//
//  CommentModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/11/10.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommentModel : NSObject

@property (nonatomic, strong)NSString *images;
@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *createTimeValue;
@property (nonatomic, strong)NSString *createTime;
@property (nonatomic, strong)NSString *content;
@property (nonatomic, strong)NSString *zan;
@property (nonatomic, strong)NSString *id;
@property (nonatomic, strong)NSDictionary *userIDInfo;
@property (nonatomic, strong)NSDictionary *replyUserIDInfo;
@property (nonatomic, strong)NSString *valueType;

@end
