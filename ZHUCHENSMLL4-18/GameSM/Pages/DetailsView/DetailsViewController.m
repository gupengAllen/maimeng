//
//  DetailsViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "DetailsViewController.h"
#import "DetailsHeaderView.h"
#import "DetailsViewCell.h"
#import "DetailsModel.h"
#import "ReadController.h"
#import "AFNetworking.h"
#import "Tool.h"

#import "Y_X_DataInterface.h"

#define CWIDTH self.view.bounds.size.width
#define CHEIGHT self.view.bounds.size.height
#define CMARGIN 10

@interface DetailsViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    
    UICollectionView *_detailsCollectionView;
    
    NSMutableArray *_dataArr;
    NSDictionary   *_dataDic;
    NSMutableArray *_dataIDArr;
    NSMutableArray *_dataidArr;
    id _data;
    NSString       *_path;
    NSString       *_urlStr;
    
    UILabel     *_titleName;
    UIImageView *_imageView;
    UILabel     *_nameLabel;
    UILabel     *_authorLabel;
    UILabel     *_timeLabel;
    UIButton    *_addCollectionBtn;
    UIButton    *_continueReadBtn;
    UIButton    *_turnBtn;
    UILabel     *_contentLabel;
    
    id _tag;
    NSString    *_Continue;
    int          _cartoonChapterCount;
    int          _cartoonId;
    float        _contentHeight;
}
@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addNoti];//添加通知
    
    _Continue = @"no";
    _cartoonChapterCount = 0;
    _dataidArr = [NSMutableArray arrayWithCapacity:0];
    _dataIDArr = [NSMutableArray arrayWithCapacity:0];
    self.view.backgroundColor = [UIColor whiteColor];
    
    //设置导航条
    [self setNavBar];
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    

    for (int i = 0 ; i < 4000; i ++) {
        NSString *title = [NSString stringWithFormat:@"%i话",i +1];
        DetailsModel *model = [DetailsModel modelWithIconName:title];
        [_dataArr addObject:model];
    }

    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = CMARGIN;
    flowLayout.minimumLineSpacing = CMARGIN;
    _detailsCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, CWIDTH, CHEIGHT) collectionViewLayout:flowLayout];
    [_detailsCollectionView registerNib:[UINib nibWithNibName:@"DetailsViewCell" bundle:nil] forCellWithReuseIdentifier:[DetailsViewCell identifier]];
    [_detailsCollectionView registerClass:[DetailsHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:[DetailsHeaderView identifier]];
    _detailsCollectionView.delegate = self;
    _detailsCollectionView.dataSource = self;
    
    _detailsCollectionView.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
    [self.view addSubview:_detailsCollectionView];
    
}
-(void)loadData:(NSNotification *)noti{
    _moddel = noti.object;
}

-(void)setNavBar{
    UINavigationBar *bar = self.navigationController.navigationBar;
    bar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];

    //元素项
    _titleName = [[UILabel alloc]initWithFrame:CGRectMake(100, 20, self.view.bounds.size.width - 200, 40)];
    _titleName.font = [UIFont boldSystemFontOfSize:20];
    _titleName.textColor = [UIColor whiteColor];
    _titleName.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:_titleName];
    UINavigationItem *item = self.navigationItem;
    item.titleView = _titleName;

    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"fanhui@2x"] style:UIBarButtonItemStylePlain target:self action:@selector(misBack)];
    UIBarButtonItem *downLoadBtn = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"xiazai@3x"] style:UIBarButtonItemStylePlain target:self action:@selector(downloadView)];
    item.leftBarButtonItem = backBtn;
    item.rightBarButtonItem = downLoadBtn;
}

#pragma mark 点击事件
-(void)misBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)downloadView{

}

-(void)addLove:(UIButton *)btn{
    [btn setImage:[UIImage imageNamed:@"shoucang@2x.png"] forState:UIControlStateNormal];
    [btn setTitle:@"加入收藏" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(misLove:) forControlEvents:UIControlEventTouchUpInside];
#warning 还没通讯
}

-(void)misLove:(UIButton *)btn{
    [btn setImage:[UIImage imageNamed:@"quxiaoshoucang@2x.png"] forState:UIControlStateNormal];
    [btn setTitle:@"取消收藏" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(addLove:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)changeTurnFan:(UIButton *)btn{
    [btn setImage:[UIImage imageNamed:@"fan@3x"] forState:UIControlStateNormal];
    [btn setTitle:@"反序" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(changeTurnZheng:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)changeTurnZheng:(UIButton *)btn{
    [btn setImage:[UIImage imageNamed:@"zheng@3x"] forState:UIControlStateNormal];
    [btn setTitle:@"正序" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(changeTurnFan:) forControlEvents:UIControlEventTouchUpInside];
}
-(void)continueRead{
        NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_RECOMMENTLU_LIST];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
            NSArray *arr = backData[@"results"];
            for (NSDictionary *dict in arr) {
                if ([dict[@"id"] intValue] == _cartoonId) {
                    NSString *chapterId = dict[@"chapterId"];
                    NSString *strrrr = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTER_DETAIL,chapterId];
                    ReadController *readC = [[ReadController alloc]init];
                    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                    [self presentViewController:readC animated:YES completion:^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"continueLu1" object:strrrr];
                    }];
                }
            }
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        }];
}
-(void)continueReadPage:(NSNotification *)noti{

    _tag = noti.object;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_tag forKey:@"pageN"];
    [defaults synchronize];

}
#pragma mark - UICollectionView Delegate & DataSource

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _cartoonChapterCount;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    DetailsViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[DetailsViewCell identifier] forIndexPath:indexPath];
    cell.model = _dataArr[indexPath.row];
    return cell;
    
}

#pragma mark - UICollectionViewDelegateFlowLayout

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((CWIDTH-50)/4, 30);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    return CGSizeMake(CWIDTH, 240);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    DetailsHeaderView *myView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:[DetailsHeaderView identifier] forIndexPath:indexPath];
    if (kind == UICollectionElementKindSectionHeader) {
        myView.backgroundColor = [UIColor whiteColor];
        _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(CMARGIN, CMARGIN, 100, 130)];
        _imageView.layer.cornerRadius = 4;
        _imageView.clipsToBounds = YES;
        [myView addSubview:_imageView];
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(100 + 2*CMARGIN, CMARGIN, 150, 30)];
        _nameLabel.textColor = [UIColor colorWithRed:17/255.0 green:17/255.0 blue:17/255.0 alpha:1];
        _nameLabel.font = [UIFont systemFontOfSize:20];
        [myView addSubview:_nameLabel];
        _authorLabel = [[UILabel alloc]initWithFrame:CGRectMake(100 + 2*CMARGIN, CMARGIN*2 + 20, 150, 20)];
        _authorLabel.textColor = [UIColor colorWithRed:120/255.0 green:120/255.0 blue:120/255.0 alpha:1];
        _authorLabel.font = [UIFont systemFontOfSize:15];
        [myView addSubview:_authorLabel];
        _timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(100 + 2*CMARGIN, CMARGIN*3+50, 200, 20)];
        _timeLabel.textColor = [UIColor colorWithRed:193/255.0 green:20/255.0 blue:53/255.0 alpha:1];
        _timeLabel.font = [UIFont systemFontOfSize:15];
        [myView addSubview:_timeLabel];
        _addCollectionBtn = [[UIButton alloc]initWithFrame:CGRectMake(100 + 2*CMARGIN, CMARGIN*4 + 65, 100, 30)];
        [_addCollectionBtn setImage:[UIImage imageNamed:@"quxiaoshoucang@2x.png"] forState:UIControlStateNormal];
        [_addCollectionBtn setTitle:@"取消收藏" forState:UIControlStateNormal];
        [_addCollectionBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _addCollectionBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        _addCollectionBtn.layer.cornerRadius = 4;
        _addCollectionBtn.clipsToBounds = YES;
        _addCollectionBtn.layer.borderWidth = 1;
        _addCollectionBtn.layer.borderColor = [UIColor colorWithRed:228/255.0 green:228/255.0 blue:228/255.0 alpha:1].CGColor;
        [_addCollectionBtn addTarget:self action:@selector(addLove:) forControlEvents:UIControlEventTouchUpInside];
        [myView addSubview:_addCollectionBtn];
        _continueReadBtn = [[UIButton alloc]initWithFrame:CGRectMake(100 + 3*CMARGIN + 100, CMARGIN * 4 +65, 100, 30)];
        [_continueReadBtn setBackgroundImage:[UIImage imageNamed:@"lingqu_big@2x"] forState:UIControlStateNormal];
        [_continueReadBtn setTitle:@"继续往下撸" forState:UIControlStateNormal];
        [_continueReadBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _continueReadBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [_continueReadBtn addTarget:self action:@selector(continueRead) forControlEvents:UIControlEventTouchUpInside];
        _continueReadBtn.layer.cornerRadius = 4;
        _continueReadBtn.clipsToBounds = YES;
        [myView addSubview:_continueReadBtn];
        _contentLabel = [[UILabel alloc]initWithFrame:CGRectMake(CMARGIN, 130 + 2*CMARGIN, CWIDTH - 2 * CMARGIN, _contentHeight+50)];
        _contentLabel.font = [UIFont systemFontOfSize:12];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = [UIColor colorWithRed:77/255.0 green:77/255.0 blue:77/255.0 alpha:1];
        [myView addSubview:_contentLabel];
        UIButton *moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(CWIDTH - 30, _contentLabel.bottom, 20, 20)];
        [moreBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [myView addSubview:moreBtn];
        UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(0, moreBtn.bottom, CWIDTH, 30)];
        iv.backgroundColor = [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1];
        [myView addSubview:iv];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(CMARGIN, CMARGIN - 5, 80, 20)];
        label.text = @"连载 (话)";
        label.textColor = [UIColor grayColor];
        label.textAlignment = NSTextAlignmentLeft;
        [iv addSubview:label];
        _turnBtn = [[UIButton alloc]initWithFrame:CGRectMake(CWIDTH - 60, CMARGIN - 5, 60, 20)];
        [_turnBtn setImage:[UIImage imageNamed:@"zheng@3x"] forState:UIControlStateNormal];
        [_turnBtn setTitle:@"正序" forState:UIControlStateNormal];
        [_turnBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [_turnBtn addTarget:self action:@selector(changeTurnFan:) forControlEvents:UIControlEventTouchUpInside];
        [iv addSubview:_turnBtn];
    }else {
    }
    return myView;
}

-(void)initHeaderView{
    
}

#pragma mark 被选择时
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    _Continue = @"no";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:_Continue forKey:@"Bb"];
    [defaults synchronize];
    NSString *cartoonNum = [NSString stringWithFormat:@"%ld",[indexPath row]+1];
    NSArray *arr = _dataDic[@"cartoonChapterList"];
    for (NSDictionary *dic in arr) {
        NSString *idStr = dic[@"id"];
        [_dataIDArr addObject:idStr];
    }
    
    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTER_DETAIL,_dataIDArr[indexPath.row]];
    ReadController *readC = [[ReadController alloc]init];
    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:readC animated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"content" object:str];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"idNum" object:_dataIDArr];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cartoonNum" object:cartoonNum];
    }];
}

-(void)addData:(NSNotification *)noti{
    DetailsModel *model = noti.object;
    _urlStr = [NSString stringWithFormat:@"%@r=%@&id=%d",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,[model.id intValue]];
    [self analyzeData];

}

-(void)addData1:(NSNotification *)noti{
    _urlStr = noti.object;
    [self analyzeData];
}

-(void)analyzeData{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:_urlStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSDictionary *dic = backData[@"results"];
        _dataDic = dic;
        _cartoonChapterCount = [dic[@"cartoonChapterCount"] intValue];
        _imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",dic[@"images"]]]]];
        _nameLabel.text = dic[@"name"];
        _authorLabel.text = [NSString stringWithFormat:@"作者:%@",dic[@"author"]];
//--分割字符串
        NSString *stt = [NSString stringWithFormat:@"%@",dic[@"createTime"]];
        stt = [stt substringToIndex:10];
        _timeLabel.text = [NSString stringWithFormat:@"最新更新时间:%@",stt];
        _contentLabel.text = dic[@"introduction"];
        _titleName.text = dic[@"name"];
        _cartoonId = [dic[@"id"] intValue];
        NSString *strr = dic[@"introduction"];
        _contentHeight = [self heightForString:strr andWidth:CWIDTH];
        
        [_detailsCollectionView reloadData];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

#pragma mark - 动态的计算高度

-(float)heightForString:(NSString *)value andWidth:(float)width{
    NSAttributedString *attrStr = [[NSAttributedString alloc]initWithString:value];
    NSRange range = NSMakeRange(0, attrStr.length);
    NSDictionary *dic = [attrStr attributesAtIndex:0 effectiveRange:&range];
    CGSize sizeToFit = [value boundingRectWithSize:CGSizeMake(width - 2*CMARGIN, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil].size;
    return sizeToFit.height * 2*CMARGIN;
}

#pragma mark - 监听通知
-(void)addNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addData:) name:@"DETAILS" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(continueReadPage:) name:@"continuePage" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addData1:) name:@"details" object:nil];
}

#pragma mark 移除监听者
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"continuePage" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DETAILS" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"details" object:nil];
}

@end
