//
//  CusProgressView.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/13.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CusProgressView : UIView
@property(nonatomic,copy)UIProgressView *progressView;
@property(nonatomic,copy)UILabel *commicPercent;
- (instancetype)initWithFrame:(CGRect)frame withName:(NSString *)commicName withPercent:(float)commicPercent;
@end
