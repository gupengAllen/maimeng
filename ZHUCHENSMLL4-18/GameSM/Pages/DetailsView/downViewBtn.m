//
//  downViewBtn.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/22.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "downViewBtn.h"

@implementation downViewBtn
- (void)setDesTitle:(NSString *)desTitle andTarget:(id)target andAction:(SEL)action{
    UILabel *desTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, self.frame.size.width - 5, self.frame.size.height - 50)];
    desTitleLabel.text = desTitle;
    desTitleLabel.font = [UIFont systemFontOfSize:14.0];
    desTitleLabel.numberOfLines = 0;
    desTitleLabel.backgroundColor = [UIColor whiteColor];
    desTitleLabel.userInteractionEnabled = YES;
    [self addSubview:desTitleLabel];
    
    UIButton *upButton = [UIButton buttonWithType:UIButtonTypeCustom];
    upButton.frame = CGRectMake(0, CGRectGetMaxY(desTitleLabel.frame), self.frame.size.width, 50);
    upButton.backgroundColor = [UIColor whiteColor];
    upButton.imageEdgeInsets = UIEdgeInsetsMake(0, self.bounds.size.width-35, 0, 0);
    [upButton addTarget:target action:@selector(action) forControlEvents:UIControlEventTouchUpInside];
    [upButton setImage:[UIImage imageNamed:@"shouqiDetail"] forState:UIControlStateNormal];
    [self addSubview:upButton];
    
//    self.backgroundColor = [UIColor whiteColor];

}
- (void)updown{
    NSLog(@"=====>");
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
