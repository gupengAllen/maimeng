//
//  DetailsHeaderView.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsHeaderView : UICollectionReusableView

+(NSString *)identifier;

@end
