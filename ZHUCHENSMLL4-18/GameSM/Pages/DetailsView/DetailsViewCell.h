//
//  DetailsViewCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailsModel;

@interface DetailsViewCell : UICollectionViewCell

@property(nonatomic,strong)DetailsModel *model;

-(void)setTitle:(NSString *)title;

+(NSString *)identifier;

@end
