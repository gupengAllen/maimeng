//
//  CusDownChapterTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>


@class chapterIdModel;


@interface CusDownChapterTableViewCell : UITableViewCell

@property (assign,nonatomic) BOOL isCellSelected;
@property (weak, nonatomic) IBOutlet UIButton *chooseDelete;
@property (nonatomic,copy)chapterIdModel *chapIdModel;
@property (weak, nonatomic) IBOutlet UILabel *chapterNameLabel;
//- (IBAction)chooseDelete:(id)sender;

@property (weak, nonatomic) IBOutlet UIProgressView *chapterProgress;
@property (weak, nonatomic) IBOutlet UIButton *ONOFFButton;
- (IBAction)onOFFButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *chapterProtion;
@end
