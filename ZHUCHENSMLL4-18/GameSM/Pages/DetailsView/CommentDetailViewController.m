//
//  CommentDetailViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/4/19.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "CommentDetailViewController.h"
#import "CustomTool.h"
#import "FaceBagCell.h"
#import "FaceBagModel.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
#import "CommentTableViewCell.h"
#import "CommentModel.h"

@interface CommentDetailViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UITextFieldDelegate,UITextViewDelegate,LoginAlertViewDelegate>
{
    //2.8
    UILabel          *_nameLabel;
    UIView           *_FaceBagView;
    UIView           *_textFieldView;
    UICollectionView *_collectionView;
    UITextField      *_inputTextField;
    UIPageControl    *_pageC;
    NSMutableArray   *_faceDataArr;
    NSMutableArray   *_faceNameArr;
    UIImageView      *_btnImage;
    BOOL             _isFace;
    BOOL             _yaoyiyao;
    BOOL             _isReply;
    BOOL             _isAlertShow;
    BOOL             _isLouzhu;
    BOOL             _showFloor;
    CGFloat          _keyboardHeight;
    NSMutableArray   *_dataArr;
    NSMutableArray   *_commentFloorArr;
    NSMutableArray   *_praiseDataArr;
    CGFloat          _sumHeight;
    NSString         *_commentId;
    NSString         *_floorId;
    NSString         *_deleteId;
    NSString         *_praiseId;
    NSString         *_praiseTotalNum;
    UILabel          *_praiseTotalLabel;
    NSMutableParagraphStyle *_paragraphStyle;
    UIButton         *_keyboardRightBtn;
    UIButton         *_keyboardRSBtn;
    NSString         *_isPraise;
    //评论限制字数
    BOOL            _showAllContent;
    int             _cellNum;
    int             _floorNum;
    BOOL            _showFlorContent;
    BOOL            _haveOrPicture;
    int             _florCellNum;
    UITextView      *_textView;
}
@end

@implementation CommentDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initData];
    [self setNavBar];
    [self createHideView];
    [self initTableView];
    [self initHeadView];
    [self createKeyboard];
    // Do any additional setup after loading the view.
    [self loadPraiseData];
    [self loadData];
    
    
}
//初始化数据
-(void)initData{
    _isFace = YES;
    _yaoyiyao = YES;
    _isReply = NO;
    _isAlertShow = NO;
    _isLouzhu = NO;
    _sumHeight = 0;
    _showAllContent = NO;
    _showFloor = NO;
    _haveOrPicture = NO;
    _cellNum = -1;
    _floorNum = -1;
    _paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    _paragraphStyle.lineSpacing = 5;// 字体的行间距
    _dataArr = [NSMutableArray array];
    _commentFloorArr = [NSMutableArray array];
    _faceDataArr = [NSMutableArray array];
    _faceNameArr = [NSMutableArray array];
    _praiseDataArr = [NSMutableArray array];
    _textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 100)];
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        FaceBagModel *model = [[FaceBagModel alloc]init];
        NSString *imageName = [NSString stringWithFormat:@"%d#.png",i];
        model.imageName = imageName;
        [_faceDataArr addObject:model];
    }
}
#pragma makr - UI
-(void)setNavBar{
//    NSArray *windows = [UIApplication sharedApplication].windows;
//    UIWindow *win = [windows objectAtIndex:0];
    UIView *view = [CustomTool createNavView];
    [self.view addSubview:view];
    [view addSubview:_nameLabel];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 20, 100, 38);//5, 25, 33, 33
    [backButton setImage:[UIImage imageNamed:@"btn_fanhui"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 65)];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 18 , ScreenSizeWidth- 160, 44)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    titleLabel.textColor = CUSTOMREDCOLOR;
    titleLabel.text = @"评论详情";
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    
}
-(void)createHideView{
    self.touchBgView = [[UIControl alloc]initWithFrame:CGRectMake(0, 64, ScreenSizeWidth, ScreenSizeHeight-64-40-_keyboardHeight)];
    [self.touchBgView addTarget:self action:@selector(touchBgViewTouchInView) forControlEvents:UIControlEventTouchUpInside];
    self.touchBgView.backgroundColor = [UIColor clearColor];
    self.touchBgView.hidden = YES;
    [self.view addSubview:self.touchBgView];
}
-(void)initTableView{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, ScreenSizeWidth, ScreenSizeHeight-64-40) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.tag = 100;
    [self.view addSubview:self.tableView];
    
}
-(void)initHeadView{
    _headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 300)];
    //
    UIView *landlordView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 60)];
    [_headView addSubview:landlordView];
    _landlordImageView = [[UIImageView alloc]initWithFrame:CGRectMake(15, 15, 40, 40)];
    _landlordImageView.layer.masksToBounds = YES;
    _landlordImageView.layer.cornerRadius = _landlordImageView.bounds.size.width/2.0;
    [_landlordImageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_landlordContentDict[@"userIDInfo"][@"images"]]]];
    [landlordView addSubview:_landlordImageView];
    _landlordNameLabel = [CustomTool createLabelWithFrame:CGRectMake(70, 17, ScreenSizeWidth-70, 20) andType:16 andColor:CUSTOMBLUECOLOR];
    _landlordNameLabel.text = _landlordContentDict[@"userIDInfo"][@"name"];
    [landlordView addSubview:_landlordNameLabel];
    _landlordTimeLabel = [CustomTool createLabelWithFrame:CGRectMake(70, 37, ScreenSizeWidth-70, 20) andType:12 andColor:CUSTOMTITLECOLOER2];
    _landlordTimeLabel.text = _landlordContentDict[@"createTimeValue"];
    [landlordView addSubview:_landlordTimeLabel];
    _landlordPraiseNumBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-70, 12, 30, 30)];
    [_landlordPraiseNumBtn setImage:[UIImage imageNamed:@"btn_dianzan"] forState:UIControlStateNormal];
//    [_landlordPraiseNumBtn addTarget:self action:@selector(dianZanBtnClike1:) forControlEvents:UIControlEventTouchUpInside];
    [landlordView addSubview:_landlordPraiseNumBtn];
    
    if ([g_App.userID isEqualToString:_landlordContentDict[@"userIDInfo"][@"id"] ]) {
        UIButton *shanchuBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-35, 13, 30, 30)];
        [shanchuBtn addTarget:self action:@selector(shanchuLouzhu:) forControlEvents:UIControlEventTouchUpInside];
        [shanchuBtn setImage:[UIImage imageNamed:@"btn_shanchu_2"] forState:UIControlStateNormal];
        [landlordView addSubview:shanchuBtn];
    }
    
    _praiseTotalLabel = [CustomTool createLabelWithFrame:CGRectMake(_landlordPraiseNumBtn.right-3, 19.5, 40, 20) andType:12 andColor:CUSTOMTITLECOLOER2];
    _praiseTotalLabel.text = _landlordContentDict[@"praiseCount"];
    [landlordView addSubview:_praiseTotalLabel];
    UIImageView *line1 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 65, ScreenSizeWidth-30, 1)];
    line1.backgroundColor = CUSTOMBGCOLOR;
    [landlordView addSubview:line1];
    
    //--textView
//    CGFloat height = [self heightForStr:_landlordContentDict[@"content"] andSize:CGSizeMake(ScreenSizeWidth-30-16, MAXFLOAT) andFont:16];
    _landlordContentTextView = [[UITextView alloc]initWithFrame:CGRectMake(15, line1.bottom+10, ScreenSizeWidth-30, 100)];
    _landlordContentTextView.attributedText = [self attributedStriWithStr:_landlordContentDict[@"content"] andFont:CUSTOMTITLESIZE16];
    float height;
    if (IsIOS7) {
        _textView = [[UITextView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 100)];
        CGRect textFrame=[[_landlordContentTextView layoutManager]usedRectForTextContainer:[_landlordContentTextView textContainer]];
        height = textFrame.size.height+16;
    }else{
        height = _landlordContentTextView.contentSize.height+16;
    }
    _landlordContentTextView.frame = CGRectMake(15, line1.bottom+10, ScreenSizeWidth-30, height);
    _landlordContentTextView.userInteractionEnabled = NO;
    [_headView addSubview:_landlordContentTextView];
    
    _cartoonView = [[UIView alloc]initWithFrame:CGRectMake(15, _landlordContentTextView.bottom+20, ScreenSizeWidth-30, 90)];
    _cartoonView.backgroundColor = CUSTOMBGCOLOR;
    [_headView addSubview:_cartoonView];
    _cartoonImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 8, 74/___Scale, 74)];
    [_cartoonImage setImageWithURL:[NSURL URLWithString:_cartoonInfomationDict[@"images"]]];
    _cartoonName = [CustomTool createLabelWithFrame:CGRectMake(_cartoonImage.right+15, 8, _cartoonView.bounds.size.width-_cartoonImage.size.width-30, 20) andType:14 andColor:CUSTOMTITLECOLOER1];
    _cartoonName.text = _cartoonInfomationDict[@"name"];
    _authorName = [CustomTool createLabelWithFrame:CGRectMake(_cartoonImage.right+15, _cartoonName.bottom+5, _cartoonName.bounds.size.width, 15) andType:12 andColor:CUSTOMTITLECOLOER2];
    _authorName.text = _cartoonInfomationDict[@"author"];
    _cartoonType = [CustomTool createLabelWithFrame:CGRectMake(_cartoonImage.right+15, _authorName.bottom+5, _cartoonName.bounds.size.width, 15) andType:12 andColor:CUSTOMTITLECOLOER2];
    _cartoonType.text = _cartoonInfomationDict[@""];
    _cartoonUpdate = [CustomTool createLabelWithFrame:CGRectMake(_cartoonImage.right+15, _cartoonImage.bottom-15, _cartoonName.bounds.size.width, 15) andType:12 andColor:CUSTOMREDCOLOR];
    _cartoonUpdate.text = _cartoonInfomationDict[@"updateValueLabel"];
    [_cartoonView addSubview:_cartoonImage];
    [_cartoonView addSubview:_cartoonName];
    [_cartoonView addSubview:_authorName];
    [_cartoonView addSubview:_cartoonType];
    [_cartoonView addSubview:_cartoonUpdate];
    
    _praiseView = [[UIView alloc]initWithFrame:CGRectMake(0, _cartoonView.bottom+10, ScreenSizeWidth, 45)];
    [_headView addSubview:_praiseView];
    
    UIImageView *lineV = [[UIImageView alloc]initWithFrame:CGRectMake(0, _praiseView.bottom+15, ScreenSizeWidth, 10)];
    lineV.backgroundColor = CUSTOMBGCOLOR;
    [_headView addSubview:lineV];
    _headView.frame = CGRectMake(0, 0, ScreenSizeWidth, _praiseView.bottom+40);
    
    self.tableView.tableHeaderView = _headView;
    
}
-(void)createPraiseView{
    [_praiseView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UILabel *label = [CustomTool createLabelWithFrame:CGRectMake(15, 15, 70, 20) andType:14 andColor:CUSTOMTITLECOLOER3];
    label.text = @"赞过的人：";
    [_praiseView addSubview:label];
    UIView *imagesView = [[UIView alloc]initWithFrame:CGRectMake(85, 8, ScreenSizeWidth-15-85, 35)];
    [_praiseView addSubview:imagesView];
    int totalCount = imagesView.bounds.size.width/47;
    totalCount = (totalCount < _praiseDataArr.count)?totalCount :(int)_praiseDataArr.count;
    _praiseTotalLabel.text = _praiseTotalNum;
    for (int i = 0; i < totalCount; i ++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(47*i, 0, 35, 35)];
        imageView.layer.masksToBounds = YES;
        imageView.backgroundColor = CUSTOMTITLECOLOER3;
        imageView.layer.cornerRadius = imageView.bounds.size.width/2.0;
        [imageView setImageWithURL:[NSURL URLWithString:_praiseDataArr[i][@"userIDInfo"][@"images"]]];
        [imagesView addSubview:imageView];
    }
}
-(void)createKeyboard{
    [self createFaceBag];
    [self createPageControl];
    [self createInputTextField];
}
-(void)createInputTextField{
    _inputTextField = [[UITextField alloc]init];
    _inputTextField.delegate = self;
    
    _textFieldView = [[UIView alloc]initWithFrame:CGRectMake(0, ScreenSizeHeight-40, ScreenSizeWidth, 40)];
    _textFieldView.backgroundColor = [UIColor whiteColor];//colorWithRed:230/255.0 green:90/255.0 blue:100/255.0 alpha:1
    [self.view addSubview:_textFieldView];
    _textFieldView.clipsToBounds = YES;
    _inputTextField = [[UITextField alloc]initWithFrame:CGRectMake(70, 5, ScreenSizeWidth-80-70, 30)];
    _inputTextField.backgroundColor = [UIColor whiteColor];
    _inputTextField.placeholder = @"我来说两句";
    _inputTextField.font = [UIFont fontWithName:@"Arial" size:15.0f];
    _inputTextField.textColor = [UIColor blackColor];
    _inputTextField.backgroundColor = [UIColor colorWithRed:246/255.0 green:237/255.0 blue:237/255.0 alpha:1];
    _inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _inputTextField.clearsOnBeginEditing = NO;
    _inputTextField.returnKeyType = UIReturnKeyDone;
    _inputTextField.keyboardType = UIKeyboardTypeDefault;
    _inputTextField.delegate = self;
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(50, 5, 20, 30)];
    view.backgroundColor = [UIColor colorWithRed:246/255.0 green:237/255.0 blue:237/255.0 alpha:1];
    [_textFieldView addSubview:view];
    UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(3, 8, 15, 15)];
    image.image = [UIImage imageNamed:@"pinglun.png"];
    [view addSubview:image];
    _inputTextField.leftViewMode = UITextFieldViewModeUnlessEditing;
    [_textFieldView addSubview:_inputTextField];
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0,0, 60, 40)];
    _btnImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 5, 30, 30)];
    _btnImage.image = [UIImage imageNamed:@"biaoqing.png"];
    [leftBtn addSubview:_btnImage];
    [leftBtn addTarget:self action:@selector(faceBag:) forControlEvents:UIControlEventTouchUpInside];
    [_textFieldView addSubview:leftBtn];
    
    _keyboardRightBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth - 80, 0, 80, 40)];
    [_keyboardRightBtn setImage:[UIImage imageNamed:@"dianzan_wei.png"] forState:UIControlStateNormal];
    [_keyboardRightBtn addTarget:self action:@selector(dianZanBtnClike1:) forControlEvents:UIControlEventTouchUpInside];
    [_textFieldView addSubview:_keyboardRightBtn];
    
    _keyboardRSBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-70, 5, 60, 30)];
    _keyboardRSBtn.layer.masksToBounds = YES;
    _keyboardRSBtn.layer.cornerRadius = 6;
    _keyboardRSBtn.backgroundColor = CUSTOMREDCOLOR;
    [_keyboardRSBtn setTitle:@"吐槽" forState:UIControlStateNormal];
    [_keyboardRSBtn addTarget:self action:@selector(TuCao:) forControlEvents:UIControlEventTouchUpInside];
    _keyboardRSBtn.hidden = YES;
    [_textFieldView addSubview:_keyboardRSBtn];
    
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1];
    [_textFieldView addSubview:lineView];
}
-(void)createFaceBag{
    _FaceBagView = [[UIView alloc]init];
    _FaceBagView.frame = CGRectMake(0,0, ScreenSizeWidth, 216);
    _FaceBagView.backgroundColor = [UIColor colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0];
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-60, 188, 60, 25)];
    [deleteBtn setImage:[UIImage imageNamed:@"btn_shanchu_2.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deletaClike) forControlEvents:UIControlEventTouchUpInside];
    [_FaceBagView addSubview:deleteBtn];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 186) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];//colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0
    _collectionView.pagingEnabled = YES;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_FaceBagView addSubview:_collectionView];
    NSString *identifier = @"faceCell";
    [_collectionView registerClass:[FaceBagCell class] forCellWithReuseIdentifier:identifier];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView"];
    [_inputTextField resignFirstResponder];
}
-(void)createPageControl{
    //分页控件：宽高是系统有默认值的
    _pageC = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 195, 40, 10)];
    _pageC.center = CGPointMake(ScreenSizeWidth/2.0,_FaceBagView.bounds.size.height -15);
    UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-20, 197.5, 40, 7)];
    iv.backgroundColor = [UIColor clearColor];
    [_FaceBagView addSubview:iv];
    UIView *iv1 = [[UIView alloc]initWithFrame:CGRectMake(0.5, 0, 7, 7)];
    iv1.backgroundColor = [UIColor lightGrayColor];
    iv1.layer.masksToBounds = YES;
    iv1.layer.cornerRadius = 3.5;
    [iv addSubview:iv1];
    UIView *iv2 = [[UIView alloc]initWithFrame:CGRectMake(16.5, 0, 7, 7)];
    iv2.backgroundColor = [UIColor lightGrayColor];
    iv2.layer.masksToBounds = YES;
    iv2.layer.cornerRadius = 3.5;
    [iv addSubview:iv2];
    UIView *iv3 = [[UIView alloc]initWithFrame:CGRectMake(33, 0, 7, 7)];
    iv3.backgroundColor = [UIColor lightGrayColor];
    iv3.layer.masksToBounds = YES;
    iv3.layer.cornerRadius = 3.5;
    [iv addSubview:iv3];
    
    
    _pageC.numberOfPages =3;
    _pageC.backgroundColor = [UIColor clearColor];
    _pageC.layer.masksToBounds = YES;
    _pageC.layer.cornerRadius = 5;
    _pageC.enabled =NO;
    //设置当前选中点得颜色
    _pageC.currentPageIndicatorTintColor = [UIColor redColor];
    _pageC.currentPage =0;
    [_FaceBagView addSubview:_pageC];
}

-(void)deletaClike{
    BOOL isDeleteAll;
    isDeleteAll = NO;
    if (_inputTextField.text.length) {
        NSString *str = [_inputTextField.text substringFromIndex:_inputTextField.text.length-1];
        if ([str isEqualToString:@"]"]) {
            int length = (int)_inputTextField.text.length-1;
            for (int i = length; i>0; i --) {
                NSString *str1 = [_inputTextField.text substringWithRange:NSMakeRange(i-1, 1)];
                if ([str1 isEqualToString:@"["]) {
                    _inputTextField.text = [_inputTextField.text substringToIndex:i-1];
                    isDeleteAll = NO;
                    break;
                }
                isDeleteAll = YES;
            }
            if (isDeleteAll) {
                _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
            }
            
        }else{
            _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
        }
    }
}

#pragma mark - 点击事件
-(void)huifu:(CustomButton *)btn{
    [_inputTextField becomeFirstResponder];
    if (btn.dict) {
       int key1 = [[btn.dict objectForKey:@"key1"] intValue];
        int key2 = [[btn.dict objectForKey:@"key2"] intValue];
        _inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", _commentFloorArr[key1][@"commentList"][key2][@"replyUserIDInfo"][@"name"]];
        _commentId = _commentFloorArr[key1][@"commentList"][key2][@"id"];
        _floorId = _commentFloorArr[key1][@"id"];
    }else{
        _inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", _commentFloorArr[btn.tag][@"replyUserIDInfo"][@"name"]];
        _commentId = _commentFloorArr[btn.tag][@"id"];
        _floorId = _commentFloorArr[btn.tag][@"id"];
    }
   
    _isReply = YES;
}
-(BOOL)TuCao:(UIButton *)btn{
    [_inputTextField resignFirstResponder];
    self.touchBgView.hidden = YES;
    _keyboardRightBtn.hidden = NO;
    _keyboardRSBtn.hidden = YES;
    _isReply = NO;
    _inputTextField.placeholder = @"我来说两句";
    if (!_inputTextField.text.length) {
        return YES;
    }
    [self commentMessage:_inputTextField.text];
    _inputTextField.text = @"";
    return YES;
}
-(void)faceBag:(UIButton *)btn{
    if (_isFace) {
        _btnImage.image = [UIImage imageNamed:@"jianpan.png"];
        _btnImage.frame = CGRectMake(10, 7, 30, 25);
        _inputTextField.inputView = _FaceBagView;
        [_inputTextField becomeFirstResponder];
        [_inputTextField reloadInputViews];
        _isFace = NO;
    }else{
        _btnImage.image = [UIImage imageNamed:@"biaoqing.png"];
        _btnImage.frame = CGRectMake(10, 5, 30, 30);
        [_inputTextField becomeFirstResponder];
        _inputTextField.inputView = nil;
        [_inputTextField reloadInputViews];
        _isFace = YES;
    }
}
-(void)misBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)dianZanBtnClike1:(UIButton *)btn{
    //1.
    [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"l" to_page:@"nl" to_section:@"c" to_step:@"r" type:@"insert" id:_landlordContentDict[@"id"]];
    _praiseId = _landlordContentDict[@"id"];
    [self praiseComment];
}
-(void)showAllFloorComment:(UIButton *)btn{
    _floorNum = (int)btn.tag;
    if (btn.selected) {
        _showFloor = YES;
    }else{
        _showFloor = NO;
    }
    [self.tableView reloadData];
}
-(void)showAllContentComment:(UIButton *)btn{
    _cellNum = (int)btn.tag;
    if (btn.selected) {
        _showAllContent = YES;
    }else{
        _showAllContent = NO;
    }
    [self.tableView reloadData];
}
#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
}
#pragma mark - 键盘高度
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    //kbSize即為鍵盤尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    _keyboardHeight = kbSize.height;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        if (!_yaoyiyao) {
            [self startAnimation];
        }else{
            self.isOrNoShowShake = NO;
            [UIView animateWithDuration:0.3 animations:^{
                self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight-self.feedBackView.bounds.size.height-kbSize.height, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
            }];
        }
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}

- (void)keyboardWillHide:(NSNotification *)notification
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        
        if (!_yaoyiyao) {
            [self stopAnimation];
        }else{
            self.isOrNoShowShake = YES;
            [UIView animateWithDuration:0.3 animations:^{
                self.feedBackView.frame = CGRectMake(24, ScreenSizeHeight, ScreenSizeWidth-48, self.feedBackView.bounds.size.height);
            }];
        }
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}
- (void)startAnimation {
    self.touchBgView.hidden = NO;
    CGRect frame1 = _textFieldView.frame;
    frame1.origin.y = ScreenSizeHeight - _keyboardHeight-40;
    [UIView animateWithDuration:0.3 animations:^{
        _textFieldView.frame = frame1;
        [self.view bringSubviewToFront:_textFieldView];
    }];
}

- (void)stopAnimation {
    self.touchBgView.hidden = YES;
    CGRect frame1 = _textFieldView.frame;
    frame1.origin.y = ScreenSizeHeight - 40;
    
    [UIView animateWithDuration:0.3 animations:^{
        _textFieldView.frame = frame1;
    }];
    _yaoyiyao = YES;
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    //2.
    [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"l" to_page:@"cpl" to_section:@"c" to_step:@"r" type:@"comment" id:_landlordContentDict[@"id"]];
    if (!g_App.userID) {
        [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"l" to_page:@"nl" to_section:@"c" to_step:@"r" type:@"comment" id:_landlordContentDict[@"id"]];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
    }
    
    _yaoyiyao = NO;
    textField.text = _inputTextField.text;
    [self.view bringSubviewToFront:self.touchBgView];
    if (!_isReply) {
        _commentId = _landlordContentDict[@"id"];
        _floorId = _landlordContentDict[@"id"];
    }
    if (!g_App.userID) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        [textField resignFirstResponder];
        return;
    }
    self.touchBgView.hidden = NO;
    _keyboardRightBtn.hidden = YES;
    _keyboardRSBtn.hidden = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    _isReply = NO;
     [textField resignFirstResponder];
     self.touchBgView.hidden = YES;
    _keyboardRightBtn.hidden = NO;
    _keyboardRSBtn.hidden = YES;
    if (!textField.text.length) {
        return YES;
    }
    [self commentMessage:textField.text];
    textField.text = @"";
    return YES;
}
- (void)touchBgViewTouchInView {
    [_inputTextField resignFirstResponder];
    if ([_inputTextField.text isEqualToString:@""]) {
        _isReply = NO;
        _inputTextField.placeholder = @"我来说两句";
    }
    self.touchBgView.hidden = YES;
    _keyboardRightBtn.hidden = NO;
    _keyboardRSBtn.hidden = YES;
    
}
#pragma mark - TableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArr.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CommentTableViewCell *cell = [[CommentTableViewCell alloc]init];
    cell.fontSize = 14;
    if (_cellNum == (int)[indexPath section]) {
        cell.showAllContent = _showAllContent;
    }
    cell.model = _dataArr[indexPath.section];
    cell.nameLabel.textColor = CUSTOMBLUECOLOR;
    
    cell.floorLabel.text = [NSString stringWithFormat:@"%ld楼",_dataArr.count -(long)[indexPath section]];
    
    if ([g_App.userID isEqualToString:_commentFloorArr[indexPath.section][@"replyUserIDInfo"][@"id"]]) {
        UIButton *shanchuBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-50, 10, 35, 35)];
        shanchuBtn.tag = [indexPath section];
        [shanchuBtn addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
        [shanchuBtn setImage:[UIImage imageNamed:@"btn_shanchu_2"] forState:UIControlStateNormal];
        [cell.contentView addSubview:shanchuBtn];
    }else{
        CustomButton *huifuBtn = [[CustomButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-50, 10, 35, 35)];
        huifuBtn.tag = [indexPath section];
        [huifuBtn addTarget:self action:@selector(huifu:) forControlEvents:UIControlEventTouchUpInside];
        [huifuBtn setImage:[UIImage imageNamed:@"huifu"] forState:UIControlStateNormal];
        [cell.contentView addSubview:huifuBtn];
    }
    NSString *cententString = _commentFloorArr[indexPath.section][@"content"];
    if (cententString.length>110) {
        if (_cellNum == (int)[indexPath section]) {
            if (!_showAllContent) {
                cententString = [cententString substringToIndex:100];
                cententString = [cententString stringByAppendingString:@"\n......\n"];
            }else{
                cententString = [cententString stringByAppendingString:@"\n"];
            }
        }else{
            cententString = [cententString substringToIndex:100];
            cententString = [cententString stringByAppendingString:@"\n......\n"];
        }
        CGFloat height = 50+[self heightForStr:cententString andSize:CGSizeMake(ScreenSizeWidth-80, MAXFLOAT) andFont:14];
        
        UIButton *showAllBtn = [[UIButton alloc]initWithFrame:CGRectMake(15+8+15, height - 30, 150, 30)];
        showAllBtn.backgroundColor = [UIColor clearColor];
        [showAllBtn setTitleColor:CUSTOMTITLECOLOER2 forState:UIControlStateNormal];
        showAllBtn.titleLabel.font = CUSTOMTITLESIZE12;
        [showAllBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 15, 0, 50)];
        showAllBtn.tag = [indexPath section];
        [showAllBtn addTarget:self action:@selector(showAllContentComment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:showAllBtn];
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(showAllBtn.right-62,height - 20, 12, 8)];
        [cell.contentView addSubview:image];
        
        if (_cellNum == (int)[indexPath section]) {
            if (!_showAllContent) {
                [showAllBtn setTitle:@"展开全部" forState:UIControlStateNormal];
                image.image = [UIImage imageNamed:@"down"];
                showAllBtn.selected = YES;
            }else{
                showAllBtn.selected = NO;
                [showAllBtn setTitle:@"收起全部" forState:UIControlStateNormal];
                image.image = [UIImage imageNamed:@"up"];
            }
        }else{
            [showAllBtn setTitle:@"展开全部" forState:UIControlStateNormal];
            image.image = [UIImage imageNamed:@"down"];
            showAllBtn.selected = YES;
        }
    }
    
    UIView *lineH = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 1)];
    lineH.backgroundColor = CUSTOMBGCOLOR;
    [cell.contentView addSubview:lineH];
    
    return cell;
}

//高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *cententString = _commentFloorArr[indexPath.section][@"content"];
    if (cententString.length>110) {
        if (_cellNum == (int)[indexPath section]) {
            if (!_showAllContent) {
                cententString = [cententString substringToIndex:100];
                cententString = [cententString stringByAppendingString:@"\n......\n"];
            }else{
                cententString = [cententString stringByAppendingString:@"\n"];
            }
        }else{
            cententString = [cententString substringToIndex:100];
            cententString = [cententString stringByAppendingString:@"\n......\n"];
        }
    }
    CGFloat height = [self heightForStr:cententString andSize:CGSizeMake(ScreenSizeWidth-80, MAXFLOAT) andFont:14];
    return height +50+8;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    NSArray *arr = [NSArray arrayWithArray:_commentFloorArr[section][@"commentList"]];
    CGFloat sum = 0;
    if (arr.count>0) {
        int count = (int)arr.count;
        if (!_showFloor) {
            count = (count <= 3)?count :3;
        }else{
            if (_floorNum == section) {
                count = (int)arr.count;
            }else{
                count = (count <= 3)?count :3;
            }
        }
        NSString *str;
        for (int i=0; i<count; i++) {
            if ([arr[i][@"userIDInfo"][@"id"] isEqualToString:_commentFloorArr[section][@"replyUserIDInfo"][@"id"]]) {
                if ([arr[i][@"replyUserIDInfo"][@"id"] isEqualToString:_landlordContentDict[@"userIDInfo"][@"id"]]) {
                    str = [NSString stringWithFormat:@"%@(楼主) : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"content"]];
                }else{
                    str = [NSString stringWithFormat:@"%@ : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"content"]];
                }
            }else{
                if ([arr[i][@"replyUserIDInfo"][@"id"] isEqualToString:_landlordContentDict[@"userIDInfo"][@"id"]]) {
                    str = [NSString stringWithFormat:@"%@(楼主) 回复了 %@ : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"userIDInfo"][@"name"],arr[i][@"content"]];
                }else{
                    str = [NSString stringWithFormat:@"%@ 回复了 %@ : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"userIDInfo"][@"name"],arr[i][@"content"]];
                }
            }
            
             CGFloat height = [self heightForStr:str andSize:CGSizeMake(ScreenSizeWidth-80-20, MAXFLOAT) andFont:12]+5;
            sum += height;
        }
        if (arr.count>3) {
            _sumHeight = sum+20;
            return sum+20;
        }
        _sumHeight = sum;
        return sum;
    }
    return 0.01;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 30;
    }
    return 0.01;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    NSArray *arr = [NSArray arrayWithArray:_commentFloorArr[section][@"commentList"]];
    
    if (arr.count>0) {
        CGFloat heightY = 0;
        UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, _sumHeight)];
//        [footView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        footView.clipsToBounds = YES;
        footView.tag = 100+section;
        
        int count = (int)arr.count;
        if (!_showFloor) {
            count = (count <= 3)?count :3;
        }else{
            if (_floorNum == section) {
                count = (int)arr.count;
            }else{
                count = (count <= 3)?count :3;
            }
        }
        
        for (int i = 0; i < count; i ++) {
            NSString *str;
            NSString *nameStr;
            NSString *nameStr1;
            
            if ([arr[i][@"userIDInfo"][@"id"] isEqualToString:_commentFloorArr[section][@"replyUserIDInfo"][@"id"]]) {
                
                if ([arr[i][@"replyUserIDInfo"][@"id"] isEqualToString:_landlordContentDict[@"userIDInfo"][@"id"]]) {
                    str = [NSString stringWithFormat:@"%@(楼主) : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"content"]];
                    nameStr = [NSString stringWithFormat:@"%@(楼主)",arr[i][@"replyUserIDInfo"][@"name"]];
                }else{
                    str = [NSString stringWithFormat:@"%@ : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"content"]];
                    nameStr = [NSString stringWithFormat:@"%@ :",arr[i][@"replyUserIDInfo"][@"name"]];
                }
            }else{
                if ([arr[i][@"replyUserIDInfo"][@"id"] isEqualToString:_landlordContentDict[@"userIDInfo"][@"id"]]) {
                    str = [NSString stringWithFormat:@"%@(楼主) 回复了 %@ : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"userIDInfo"][@"name"],arr[i][@"content"]];
                    nameStr = [NSString stringWithFormat:@"%@(楼主)",arr[i][@"replyUserIDInfo"][@"name"]];
                }else{
                    str = [NSString stringWithFormat:@"%@ 回复了 %@ : %@",arr[i][@"replyUserIDInfo"][@"name"],arr[i][@"userIDInfo"][@"name"],arr[i][@"content"]];
                    nameStr = arr[i][@"replyUserIDInfo"][@"name"];
                }
                nameStr1 = arr[i][@"userIDInfo"][@"name"];
            }

            
            CGFloat height = [self heightForStr:str andSize:CGSizeMake(ScreenSizeWidth-80-20, MAXFLOAT) andFont:12]+5;
            UIView *subView = [[UIView alloc]initWithFrame:CGRectMake(0, heightY, ScreenSizeWidth, height)];
            subView.tag = 1000+i;
            
            if ([g_App.userID isEqualToString:arr[i][@"replyUserIDInfo"][@"id"]]) {
                UIButton *shanchuBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-50, 0, 35, 35)];
                shanchuBtn.tag = [arr[i][@"id"] integerValue];//height/2.0-15
                [shanchuBtn addTarget:self action:@selector(showAlert1:) forControlEvents:UIControlEventTouchUpInside];
                [shanchuBtn setImage:[UIImage imageNamed:@"btn_shanchu_2"] forState:UIControlStateNormal];
                [subView addSubview:shanchuBtn];
            }
            
            UITextView *textView = [[UITextView alloc]initWithFrame:CGRectMake(50+15, 0, ScreenSizeWidth-80-20, height-5)];
            textView.backgroundColor = [UIColor clearColor];
            textView.userInteractionEnabled = NO;
            textView.editable = NO;
            [subView addSubview:textView];
            
            NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self attributedStriWithStr:str andFont:CUSTOMTITLESIZE12]];
            NSDictionary *attri;
            if (_haveOrPicture) {
                attri = [NSDictionary dictionaryWithObjectsAndKeys:CUSTOMREDCOLOR,NSForegroundColorAttributeName,nil];
            }else{
                attri = [NSDictionary dictionaryWithObjectsAndKeys:CUSTOMREDCOLOR,NSForegroundColorAttributeName,_paragraphStyle,NSParagraphStyleAttributeName,nil];
            }
            [contentStr setAttributes:attri range:NSMakeRange(0, nameStr.length)];
            [contentStr setAttributes:attri range:NSMakeRange(nameStr.length+5, nameStr1.length)];
            textView.attributedText = [contentStr copy];
            
            [footView addSubview:subView];
            heightY += height;
            
            CustomButton *control = [[CustomButton alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth-60, height)];
            control.backgroundColor = [UIColor clearColor];
            control.dict = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",section],@"key1",[NSString stringWithFormat:@"%d",i],@"key2", nil];
            [control addTarget:self action:@selector(huifu:) forControlEvents:UIControlEventTouchUpInside];
            [subView addSubview:control];
        }

        if (arr.count>3) {
            UILabel *label = [CustomTool createLabelWithFrame:CGRectMake(65+5, heightY, 100, 19) andType:14 andColor:CUSTOMTITLECOLOER3];
            label.backgroundColor = [UIColor clearColor];
            [footView addSubview:label];
            UIButton *moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(65+5, heightY, 100, 20)];
            moreBtn.backgroundColor = [UIColor clearColor];
            [moreBtn addTarget:self action:@selector(showAllFloorComment:) forControlEvents:UIControlEventTouchUpInside];
            moreBtn.tag = section;
            [footView addSubview:moreBtn];
            UIImageView *image = [[UIImageView alloc]init];
            [footView addSubview:image];
            if (_floorNum == section) {
                if (!_showFloor) {
                    label.text = @"展开全部留言";
                    image.frame = CGRectMake(moreBtn.right-15,heightY+6, 12, 8);
                     image.image = [UIImage imageNamed:@"down"];
                    moreBtn.selected = YES;
                }else{
                    label.text = @"收起留言";
                    image.frame = CGRectMake(moreBtn.right-15-25,heightY+6, 12, 8);
                     image.image = [UIImage imageNamed:@"up"];
                    moreBtn.selected = NO;
                }
            }else{
                label.text = @"展开全部留言";
                image.frame = CGRectMake(moreBtn.right-15,heightY+6, 12, 8);
                 image.image = [UIImage imageNamed:@"down"];
                moreBtn.selected = YES;
            }
        }
        return footView;
    }
    return nil;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 30)];
        UILabel *label = [CustomTool createLabelWithFrame:CGRectMake(15, 0, ScreenSizeWidth-30, 30) andType:14 andColor:CUSTOMTITLECOLOER1];
        label.text = [NSString stringWithFormat:@"全部留言 (%d)",_dataArr.count];
        [view addSubview:label];
        return view;
    }
    return nil;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _yaoyiyao = NO;
        if (g_App.userID) {
            [_inputTextField becomeFirstResponder];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            _inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", _commentFloorArr[indexPath.section][@"replyUserIDInfo"][@"name"]];
            _commentId = _commentFloorArr[indexPath.section][@"id"];
            _floorId =  _commentFloorArr[indexPath.section][@"id"];
            _isReply = YES;
            return;
        } else {
            LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
            [alertView show];
            return;
        }
}
#pragma mark - UICollectionViewDelegate键盘
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _faceDataArr.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"faceCell";
    FaceBagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.model = _faceDataArr[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((ScreenSizeWidth-75)/7.0, 185/5.0);
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 5, 5);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    _inputTextField.text = [NSString stringWithFormat:@"%@[%@]",_inputTextField.text,_faceNameArr[row]];
}

#pragma mark - 加载数据
-(void)setLandlordContentDict:(NSDictionary *)landlordContentDict{
    _landlordContentDict = landlordContentDict;
}
-(void)setCartoonInfomationDict:(NSDictionary *)cartoonInfomationDict{
    _cartoonInfomationDict = cartoonInfomationDict;
}
-(void)loadPraiseData{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_PRAISELIST,@"r",
                                    _landlordContentDict[@"id"],@"id",
                                    @"1",@"page",
                                    @"10",@"size",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(loadPraiseDataFinished:) method:GETDATA];
}

-(void)loadPraiseDataFinished:(NSDictionary *)dict{
    if (dict && ![[dict objectForKey:@"code"] integerValue]){
        _praiseTotalNum = dict[@"extraInfo"][@"countTotal"];
        [_praiseDataArr removeAllObjects];
        for (NSDictionary *dic in dict[@"results"]) {
            [_praiseDataArr addObject:dic];
        }
        [self createPraiseView];
    }
}

-(void)loadData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_REPLYLIST,@"r",
                                    _landlordContentDict[@"id"],@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(loadDataFinished:) method:GETDATA];
}
-(void)loadDataFinished:(NSDictionary *)dict{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dict && ![[dict objectForKey:@"code"] integerValue]){
        [_dataArr removeAllObjects];
        [_commentFloorArr removeAllObjects];
        _isPraise = [NSString stringWithFormat:@"%@",dict[@"extraInfo"][@"isPraise"]];
        for (NSDictionary *item in dict[@"results"]) {
            [_commentFloorArr addObject:item];
            CommentModel *model = [[CommentModel alloc]init];
            [model setValuesForKeysWithDictionary:item];
            [_dataArr addObject:model];
        }
        [_tableView reloadData];
        if ([_isPraise isEqualToString:@"1"]) {
            [_keyboardRightBtn setImage:[UIImage imageNamed:@"dianzan_3.png"] forState:UIControlStateNormal];
        }else{
            [_keyboardRightBtn setImage:[UIImage imageNamed:@"dianzan_wei.png"] forState:UIControlStateNormal];
        }
    }
}
- (void)commentMessage:(NSString*)msg {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_REPLY_MESSAGE,@"r",
                                    msg,@"content",
                                    _commentId,@"contentID",
                                    @"4",@"type",
                                    _floorId,@"floorID",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(commentMessageFinish:) method:POSTDATA];
}

- (void)commentMessageFinish:(NSDictionary*)dic {
    //3.
    [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"r" to_page:@"cpl" to_section:@"c" to_step:@"a" type:@"comment" id:_landlordContentDict[@"id"]];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [self loadData];
    }
}

-(void)praiseComment{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"content/praise",@"r",
                                    _praiseId,@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(praiseCommentFinished:) method:POSTDATA];
}
- (void)praiseCommentFinished:(NSDictionary*)dic {
    //4.
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        NSString *strInfo = dic[@"results"];
        if ([strInfo isEqualToString:@"取消赞"]) {
            [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"d" to_page:@"cpl" to_section:@"c" to_step:@"a" type:@"cancel" id:_landlordContentDict[@"id"]];
            
            [MBProgressHUD showSuccess:@"已取消" toView:self.view];
            [_keyboardRightBtn setImage:[UIImage imageNamed:@"dianzan_wei.png"] forState:UIControlStateNormal];
        }else{
            [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"d" to_page:@"cpl" to_section:@"c" to_step:@"a" type:@"insert" id:_landlordContentDict[@"id"]];
            
            [MBProgressHUD showSuccess:@"点赞成功" toView:self.view];
            [_keyboardRightBtn setImage:[UIImage imageNamed:@"dianzan_3.png"] forState:UIControlStateNormal];
        }
        [self loadPraiseData];
    }
}
//删除评论
#pragma mark --alertDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (_isAlertShow) {
        if (buttonIndex == 1) {
            [self deleteComments];
        }
    }
    [alertView removeFromSuperview];
    _isAlertShow = NO;
}
-(void)shanchuLouzhu:(UIButton *)btn{
    _isLouzhu = YES;
    [self showAlert:btn];
}
-(void)showAlert1:(UIButton *)btn{
    //5.
    [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"d" to_page:@"cpl" to_section:@"c" to_step:@"r" type:@"delete" id:[NSString stringWithFormat:@"%d",btn.tag]];
    _isAlertShow = YES;
    _deleteId =[NSString stringWithFormat:@"%ld",btn.tag];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"您是否确定删除此回复" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.center = CGPointMake(ScreenSizeWidth/2,ScreenSizeHeight-100);
    [alertView show];
    _isLouzhu = NO;
}
-(void)showAlert:(UIButton *)btn{
    _isAlertShow = YES;
    if (_isLouzhu) {
        _deleteId =_landlordContentDict[@"id"];
    }else{
        _deleteId =_commentFloorArr[btn.tag][@"id"];
    }
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"您是否确定删除此回复" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alertView.center = CGPointMake(ScreenSizeWidth/2,ScreenSizeHeight-100);
    [alertView show];
    _isLouzhu = NO;
}
-(void)deleteComments{
    //6.
    [[LogHelper shared] writeToFilefrom_page:@"cpl" from_section:@"c" from_step:@"r" to_page:@"cpl" to_section:@"c" to_step:@"a" type:@"delete" id:_deleteId];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_DELETE,@"r",
                                    _deleteId,@"id",nil
                                    ];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(deleteCommentsFinish:) method:POSTDATA];
}

- (void)deleteCommentsFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
        if ([_deleteId isEqualToString:_cartoonInfomationDict[@"id"]]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [self loadData];
        }
    }
}


#pragma mark - 自定义方法
//计算高度的方法
-(CGFloat)heightForStr:(NSString *)str andSize:(CGSize)sizeCG andFont:(CGFloat)font{
    _textView.frame = CGRectMake(0, 0,sizeCG.width, 100);
    NSMutableAttributedString *contentStr = [[NSMutableAttributedString alloc]initWithAttributedString:[self attributedStriWithStr:str andFont:[UIFont systemFontOfSize:font]]];
    _textView.attributedText = [contentStr copy];
    NSLog(@"--%@",_textView.attributedText);
    if (IsIOS7) {
        [_textView.layoutManager ensureLayoutForTextContainer:_textView.textContainer];
        CGRect textFrame=[[_textView layoutManager]usedRectForTextContainer:[_textView textContainer]];
        
        
        float height = textFrame.size.height;
        return height+16;
    }else{
        return _textView.contentSize.height+16;
    }
}
//返回字符串的方法
-(NSAttributedString *)attributedStriWithStr:(NSString *)str andFont:(UIFont *)font{
    NSDictionary *attributes = @{NSFontAttributeName:font,NSParagraphStyleAttributeName:_paragraphStyle};
    NSString *regexString = @"\\[[^\\]]+\\]";
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc]init];//_model.content
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
    NSArray *arrr = [regex matchesInString:str options:0 range:NSMakeRange(0, str.length)];
    if (arrr.count>0) {
        for (int i =0 ; i < arrr.count; i ++) {
            NSTextCheckingResult *result = [regex firstMatchInString:str options:0 range:NSMakeRange(0, str.length)];
            NSString *faceString = [str substringToIndex:result.range.location];//表情之前
            NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
            [attri appendAttributedString:attrStr1];
            NSString *faceStr = [str substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
            for (int j = 0; j < _faceNameArr.count; j ++) {
                if ([faceStr isEqualToString:_faceNameArr[j]]) {
                    NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                    attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                    attch.bounds = CGRectMake(0, -2, 30, 30);
                    NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                    [attri appendAttributedString:string1];
                    break;
                }
            }
            str = [str substringFromIndex:result.range.location+result.range.length];
        }
        NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:str];
        [attri appendAttributedString:attrStr2];
        [attri addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, attri.length)];
        _haveOrPicture = YES;
        return attri;
    }else{
        attri = [[NSMutableAttributedString alloc]initWithString:str attributes:attributes];
        _haveOrPicture = NO;
        return attri;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
