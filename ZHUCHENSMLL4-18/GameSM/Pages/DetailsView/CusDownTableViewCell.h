//
//  CusDownTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class cartoonIdModel;
@interface CusDownTableViewCell : UITableViewCell
@property(nonatomic,copy)NSString *documentUrl;
@property (weak, nonatomic) IBOutlet UIImageView *cartoonImage;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *ONOFFButton;
@property (weak, nonatomic) IBOutlet UILabel *cartoonName;
- (IBAction)ONOFFTouch:(id)sender;
@property (copy, nonatomic) cartoonIdModel *cartoonModel;
@end
