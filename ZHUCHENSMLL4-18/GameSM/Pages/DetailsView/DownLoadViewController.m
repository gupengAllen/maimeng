//
//  DownLoadViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/4.
//  Copyright © 2015年 王涛. All rights reserved.
//
#import "UICusButton.h"
#import "DownLoadViewController.h"
#import "Config.h"
#import "cartoonChapterListModel.h"
#import "TDNetworkQueue.h"
#import "DownLoadListViewController.h"
#import "cartoonDetailModel.h"
#import "DataBaseHelper.h"
#import "Y_X_DataInterface.h"
#import "SDWebImageDownloader.h"
#import <objc/runtime.h>
#include <sys/param.h>
#include <sys/mount.h>
#import "AFNetworking.h"
//#import "SDImageHelperManager.h"
#import "NSString+MD5Addition.h"
#import "GSDownloadManager.h"
#import "chapterIdModel.h"

#import "CustomTool.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
@interface DownLoadViewController ()<UIAlertViewDelegate>
{
    NSMutableDictionary *allCartoonModelDict;
    UILabel *bottomLabel;
    NSMutableArray *selectedArr;
    NSInteger current;
    NSInteger commicSie;
    int currentAsyRequire;
    NSMutableArray *_dataRequireArr;
    NSMutableArray *_noDataRequireArr;
    UIAlertView *alertViewDown;
    UILabel * _titleName;
    NSMutableArray  *_subButtons;
    BOOL _isContinue;
    MBProgressHUD *hud;
    
    NSString *_idAllStr;
    NSMutableArray *_idArr;
    
}
@property(nonatomic,copy)NSMutableArray *downPages;
@end

@implementation DownLoadViewController
-(instancetype)init {
    if(self = [super init]){
        _charpList = [[NSArray alloc] init];
    }
    return self;
}

- (NSMutableArray *)getdownPages{
    if (!_downPages) {
        _downPages = [[NSMutableArray alloc] init];
    }
    return _downPages;
}

- (void)setCharpList:(NSArray *)charpList {
    _charpList = charpList;
}

- (void)setAllInfo:(NSArray *)allInfo{
    _allInfo = allInfo;
    _dataRequireArr = [NSMutableArray array];
    _noDataRequireArr = [NSMutableArray array];
    _titleName = [[UILabel alloc]init];
    _titleName.center = CGPointMake(ScreenSizeWidth/2, 10);
    _titleName.size = CGSizeMake(200, 50);
    _titleName.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    _titleName.textColor = [UIColor colorWithRed:203/255.0 green:74/255.0 blue:89/255.0 alpha:1];
    _titleName.textAlignment = NSTextAlignmentCenter;
    _titleName.text = _allInfo[1];
    for (chapterIdModel *chapModel in  [[DataBaseHelper shared] fetchSaveChapterWithCartoonId:_allInfo[0]]) {
        if ([chapModel.status isEqualToString:@"1"]) {
            [_dataRequireArr addObject:chapModel.chapterId];
        }
        if ([chapModel.status isEqualToString:@"2"]){
            [_noDataRequireArr addObject:chapModel.chapterId];
        }
    }
}

- (void)joinComicForce:(UICusButton *)joinComicforce {
    
    joinComicforce.selected = !joinComicforce.selected;
    joinComicforce.layer.borderColor = joinComicforce.selected? RGBACOLOR(234, 74, 97, 1.0).CGColor:RGBACOLOR(220, 220, 220, 1.0).CGColor;
    
    if(joinComicforce.selected){
        [selectedArr addObject:joinComicforce];
        NSInteger allCio = 0;
        for (UICusButton *cusButton in selectedArr) {
            allCio = allCio + [cusButton.chapterListModel.totalChapterSize integerValue];
        }
        bottomLabel.text = [NSString stringWithFormat:@"已选%d项,共%dM,剩余%@",selectedArr.count,allCio/1024/1024,[self freeDiskSpaceInBytes]];
    }else{
        
        NSInteger lastallCio = 0;
        [selectedArr removeObject:joinComicforce];
        for (UICusButton *cusButton in selectedArr) {
            lastallCio = lastallCio + [cusButton.chapterListModel.totalChapterSize integerValue];
        }
        bottomLabel.text = [NSString stringWithFormat:@"已选%d项,共%dM,剩余%@",selectedArr.count,lastallCio/1024/1024,[self freeDiskSpaceInBytes]];
    }
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _subButtons = [NSMutableArray array];
    selectedArr = [[NSMutableArray alloc] init];
    allCartoonModelDict = [[NSMutableDictionary alloc] init];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"存储中";
    [self.view addSubview:hud];
    
    [self getdownPages];
    [self Nav];
    [self createFatherView];
    [self bottomView];
}

- (void)bottomView{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenSizeHeight - 50, ScreenSizeWidth, 50)];
    bottomView.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    [self.view addSubview:bottomView];
    
    bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, ScreenSizeWidth - 120, 50)];
    bottomLabel.textColor = [UIColor whiteColor];
    [bottomView addSubview:bottomLabel];
    
    UIButton *downBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    downBtn.layer.cornerRadius = 2.0;
    downBtn.layer.masksToBounds = YES;
    downBtn.frame = CGRectMake(ScreenSizeWidth - 80, 10, 60, 30);
    downBtn.backgroundColor = [UIColor whiteColor];
    [downBtn setTitle:@"下载" forState:UIControlStateNormal];
    [downBtn setTitleColor:RGBACOLOR(234, 74, 97, 1.0) forState:UIControlStateNormal];
    [downBtn addTarget:self action:@selector(downLoadBtn:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:downBtn];
}

- (void)downLoadBtn:(UICusButton *)btn{
    [[LogHelper shared] writeToFilefrom_page:@"cdd" from_section:@"c" from_step:@"r" to_page:@"cdd" to_section:@"c" to_step:@"a" type:@"" id:[NSString stringWithFormat:@"%@",_allInfo[0]] detail:[NSString stringWithFormat:@"{'size':'%ld'}",(unsigned long)selectedArr.count]];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"]&&[[[NSUserDefaults standardUserDefaults] objectForKey:@"DOWNLOADBUTTON"] isEqualToString:@"0"]) {
        UIAlertView *liuliangAlert = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"当前为非WIFI网络，继续下载将消耗流量。是否继续下载" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        liuliangAlert.tag = 20000;
        [liuliangAlert show];
        return;
    }
    
    
    [hud show:YES];
    
    NSMutableString *chapterIds = [NSMutableString string];
    for (UICusButton *selectBtn in selectedArr) {
        selectBtn.backgroundColor = RGBACOLOR(200.0, 200.0, 200.0, 1.0);
        selectBtn.layer.borderWidth = 0;
        selectBtn.userInteractionEnabled = NO;
        UIImageView *imageSaveData = [[UIImageView alloc] initWithFrame:CGRectMake(52 * _Scale, 15*__Scale, 10, 10)];
        imageSaveData.image = [UIImage imageNamed:@"xiazai"];
        [selectBtn addSubview:imageSaveData];
        NSString *commicUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,selectBtn.tag];
        
        NSDictionary *headers;
        if (g_App.userInfo.userID != nil) {
            headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
        }else {
            headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
        }
        [YK_API_request startGetLoad:commicUrl extraParams:nil object:self action:@selector(asyRequire:) header:headers];
        [chapterIds appendString:[NSString stringWithFormat:@"%d",selectBtn.tag]];
        
        [chapterIds appendString:@","];
    }
    NSString *lastStr =  [chapterIds substringToIndex:chapterIds.length - 1];
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:@{@"r":@"cartoonDownload/batchAdd",@"chapterIds":lastStr} object:self action:@selector(downLoadUp:) method:POSTDATA];
    
}

- (void)downLoadUp:(NSDictionary *)downLoadUpDict {
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 20000) {
        if (buttonIndex == 1) {
            [alertViewDown show];
            
            _isContinue = YES;
            [hud show:YES];
            for (UICusButton *selectBtn in selectedArr) {
                selectBtn.backgroundColor = RGBACOLOR(200.0, 200.0, 200.0, 1.0);
                selectBtn.userInteractionEnabled = NO;
                selectBtn.layer.borderWidth = 0;
                UIImageView *imageSaveData = [[UIImageView alloc] initWithFrame:CGRectMake(52 * _Scale, 15*__Scale, 10, 10)];
                imageSaveData.image = [UIImage imageNamed:@"xiazai"];
                [selectBtn addSubview:imageSaveData];
                NSString *commicUrl = [NSString stringWithFormat:@"%@r=%@&chapterId=%ld&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,selectBtn.tag];
                
                NSDictionary *headers;
                if (g_App.userInfo.userID != nil) {
                    headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
                }else {
                    headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
                }
                [YK_API_request startGetLoad:commicUrl extraParams:nil object:self action:@selector(asyRequire:) header:headers];
            }
        }
    }else if (alertView.tag == 30000){
        //        [self dismissViewControllerAnimated:YES completion:nil];
    }
    //    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (void)doTime{
//    [alertViewDown dismissWithClickedButtonIndex:0 animated:NO];
//
//}

- (void)asyRequire:(NSDictionary *)dict{
    currentAsyRequire ++;
    
    
    [[DataBaseHelper shared] insertCartoonId:_allInfo[0] Name:_allInfo[1]
                                       Image:_allInfo[2] IdAllStr:_idAllStr];
    [[DataBaseHelper shared] insertCharpterModel:[cartoonDetailModel parasArr:dict][0] andStatus:@"2"];
    for (cartoonDetailModel *cartoon in [cartoonDetailModel parasArr:dict]) {
        [[DataBaseHelper shared] insertAlbumModel:cartoon];
    }
    if(selectedArr.count == currentAsyRequire){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            if (_isContinue) {
                [[GSDownloadManager manager] startDownloadWithCartoonId:_allInfo[0] andDownLoad:YES];
            }else{
                [[GSDownloadManager manager] startDownloadWithCartoonId:_allInfo[0] andDownLoad:NO];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UPDATEDOWNLOAD" object:nil];
        });
        [hud hide:YES];
        alertViewDown = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"已开始下载,可在书架-下载查看下载进度" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        alertViewDown.tag = 30000;
        [alertViewDown show];
    }
}

- (void)downImage:(NSArray *)requestArr andPathArr:(NSArray *)pathArr{
    
}


- (void)Nav{
    UIView *view = [CustomTool createNavView];
    [self.view addSubview:view];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 25, 33, 33);
    [backButton setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    
    _titleName.center = CGPointMake(ScreenSizeWidth/2, 42);
    [view addSubview:_titleName];
    
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(KScreenWidth - 38, 30, 33, 33);
    [rightButton setTitle:@"全选" forState:UIControlStateNormal];
    [rightButton setTitle:@"取消" forState:UIControlStateSelected];
    [rightButton setTitleColor:[UIColor colorWithRed:229/255.0 green:62/255.0 blue:82/255.0 alpha:1] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [rightButton addTarget:self action:@selector(chooseAll:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:rightButton];
}

- (void)chooseAll:(UIButton *)chooseAllButton{
    [[LogHelper shared] writeToFilefrom_page:@"cdd" from_section:@"c" from_step:@"d" to_page:@"cdde" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
    commicSie = 0;
    chooseAllButton.selected = !chooseAllButton.selected;
    if(chooseAllButton.selected){
        for (UIButton *pageButton in _downPages) {
            pageButton.layer.borderColor = chooseAllButton.selected? RGBACOLOR(234, 74, 97, 1.0).CGColor:RGBACOLOR(220, 220, 220, 1.0).CGColor;
            pageButton.selected = YES;
        }
        
        
        selectedArr = [_downPages mutableCopy];
        for (UICusButton *cusButton in selectedArr) {
            commicSie = commicSie + [cusButton.chapterListModel.totalChapterSize integerValue];
        }
        bottomLabel.text = [NSString stringWithFormat:@"已选%ld项,共%ldM,剩余%@",(unsigned long)_downPages.count,commicSie/1024/1024,[self freeDiskSpaceInBytes]];
    }else{
        
        for (UIButton *pageButton in _downPages) {
            pageButton.layer.borderColor = chooseAllButton.selected? RGBACOLOR(234, 74, 97, 1.0).CGColor:RGBACOLOR(220, 220, 220, 1.0).CGColor;
            pageButton.selected = NO;
        }
        
        [selectedArr removeAllObjects];
        bottomLabel.text = [NSString stringWithFormat:@"已选%d项,共%dM,剩余%@",0,0,[self freeDiskSpaceInBytes]];
        
    }
    
}

-(void)createFatherView{
    CGFloat joinComicWidth = ScreenSizeWidth/4;
    _idArr = [NSMutableArray array];
    
    for(int i = 0; i < _charpList.count; i++) {
        //        _idAllStr = [_idAllStr stringByAppendingString:_charpList[i]];
        cartoonChapterListModel *cartoonModel = _charpList[i];
        UICusButton * joinComic = [[UICusButton alloc] init];
        joinComic.chapterListModel = cartoonModel;
        joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 20 + 40 * (i/4), joinComicWidth - 10, 35);
        joinComic.backgroundColor = [UIColor whiteColor];
        joinComic.layer.cornerRadius = 4.0;
        joinComic.layer.borderWidth = 1.0;
        joinComic.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        joinComic.titleLabel.font = [UIFont systemFontOfSize:13.0];
        [joinComic setTitle:[NSString stringWithFormat:@"%@话",cartoonModel.priority] forState:UIControlStateNormal];
        [joinComic setTitleColor:[UIColor colorWithRed:155.f/255 green:155.f/255 blue:155.f/255 alpha:1.0] forState:UIControlStateNormal];
        joinComic.imageEdgeInsets = UIEdgeInsetsMake(20 * __Scale, 55 * _Scale, 2, 8*_Scale);
        
        if([_dataRequireArr indexOfObject:cartoonModel.id] != NSNotFound){
            joinComic.backgroundColor = RGBACOLOR(200.0, 200.0, 200.0, 1.0);
            UIImageView *imageSaveData = [[UIImageView alloc] initWithFrame:CGRectMake(52 * _Scale, 15*__Scale, 10, 10)];
            imageSaveData.image = [UIImage imageNamed:@"xiazai"];
            [joinComic addSubview:imageSaveData];
            joinComic.userInteractionEnabled = NO;
        }
        if ([_noDataRequireArr indexOfObject:cartoonModel.id] != NSNotFound) {
            joinComic.backgroundColor = RGBACOLOR(200.0, 200.0, 200.0, 1.0);
            joinComic.userInteractionEnabled = NO;
        }
        
        joinComic.tag = [[_charpList[i] id] integerValue];
        [_idArr addObject:[_charpList[i] id]];
        [joinComic addTarget:self action:@selector(joinComicForce:) forControlEvents:UIControlEventTouchUpInside];
        
        [_fatherScrollView addSubview:joinComic];
        if ([_dataRequireArr indexOfObject:cartoonModel.id] == NSNotFound) {
            [_downPages addObject:joinComic];
        }
        [_subButtons addObject:joinComic];
    }
    _fatherScrollView.contentSize = CGSizeMake(0, 40 * (_charpList.count)/4 + 40);
    _idAllStr = [_idArr componentsJoinedByString:@","];
    //    NSLog(@"--%@",_idAllStr);
    
}


-(void)misBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --获取存储空间
- (NSString *) freeDiskSpaceInBytes{
    struct statfs buf;
    long long freespace = -1;
    if(statfs("/var", &buf) >= 0){
        freespace = (long long)(buf.f_bsize * buf.f_bfree);
    }
    return [NSString stringWithFormat:@"%qi G" ,freespace/1024/1024/1024];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotate {
    
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//(6 + joinComicWidth * (i%4), 20 + 40 * (i/4), joinComicWidth - 10, 35

- (IBAction)zhenFanButton:(id)sender {
    UIButton *posNavBtn = (UIButton *)sender;
    posNavBtn.selected = !posNavBtn.selected;
    NSInteger countButton = _subButtons.count;
    CGFloat joinComicWidth = KScreenWidth/4;
    
    [UIView animateWithDuration:0.0 animations:^{
        for (int i = 0; i < countButton; i ++) {
            UIButton *joinComic =     posNavBtn.selected ?(_subButtons[countButton - i - 1]):(_subButtons[i]);
            joinComic.frame = CGRectMake(6 + joinComicWidth * (i%4), 20 + 40 * (i/4), joinComicWidth - 10, 35);
        }
    } completion:^(BOOL finished) {
        
    }];
    
    
}
@end
