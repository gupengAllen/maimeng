//
//  GiftViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"
#import "GiftDetailController.h"
#import "TopShowView.h"

@interface GiftViewController : BaseTableViewController<UITableViewDataSource, UITableViewDelegate, PullingRefreshTableViewDelegate, UIScrollViewDelegate,UISearchBarDelegate, UIGestureRecognizerDelegate, GiftDetailDelegate>

@property (strong, nonatomic) IBOutlet UIView *noresultView;
@property (weak, nonatomic) IBOutlet UILabel *noresultLabel;

@property (strong, nonatomic) IBOutlet UIView *resultTopView;
@property (weak, nonatomic) IBOutlet UILabel *resultTopLabel;

@property (nonatomic, strong) TopShowView *topShowView;

@end
