//
//  GiftViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "GiftViewController.h"
#import "GiftCell.h"
#import "GiftDetailController.h"

#import "CustomTool.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface GiftViewController ()
{
    BOOL _isShow;
    CGFloat _lastContentOffset;
    LYTabBarController *_lyTabbar;
    
    UIImageView *_netImageView;
    UIButton    *_refreshBtn1;
}
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) NSString *keyword;

@property (nonatomic, assign) int type;  //0 正常 1 搜索有结果的 2 搜索没结果的

@property (nonatomic, assign) int currentSize;
@property (nonatomic, assign) int currentIndex;

@end

@implementation GiftViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"gl" to_section:@"g" to_step:@"l" type:@"" id:@"0"];
    _isShow = YES;
    _lyTabbar = (LYTabBarController *)self.tabBarController;
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    [self initTitleName:@"礼包中心"];
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(2, 2, KScreenWidth-4, 30)];
//去除背景颜色
    _searchBar.backgroundColor = RGBACOLOR(255, 255, 255, 1);
    _searchBar.backgroundImage = [self imageWithColor:[UIColor clearColor] size:_searchBar.bounds.size];
//    [_searchBar setLeft:0];
//    [_searchBar setRight:0];
    _searchBar.placeholder = @"搜索";
    _searchBar.layer.borderWidth = 1;
    _searchBar.layer.borderColor = [UIColor colorWithRed:237/255.0 green:237/255.0 blue:237/255.0 alpha:1].CGColor;
    _searchBar.layer.masksToBounds = YES;
    _searchBar.layer.cornerRadius = 4;
    _searchBar.delegate = self;
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 34)];
    topView.backgroundColor = [UIColor whiteColor];
    [topView addSubview:_searchBar];
    
    self.tableView.tableHeaderView = topView;
    
    self.tableView.delegate = self;
//    self.tableView.pullingDelegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self addRightItemWithTitle:@"取消" itemTarget:self action:@selector(rightBtnPressed:)];
    self.rightBtn.hidden = YES;
    if (IsIOS7) {
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    [self loadData:NO];
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
    //---- 去掉下方的线||换颜色
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        NSArray *list=self.navigationController.navigationBar.subviews;
        for (id obj in list) {
            if ([obj isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView=(UIImageView *)obj;
                NSArray *list2=imageView.subviews;
                for (id obj2 in list2) {
                    if ([obj2 isKindOfClass:[UIImageView class]]) {
                        UIImageView *imageView2=(UIImageView *)obj2;
                        //                        imageView2.backgroundColor = [UIColor colorWithRed:239/255.0 green:105/255.0 blue:125/255.0 alpha:1];
                        imageView2.hidden = YES;
                    }
                }
            }
        }
    }

    
}
//取消searchbar背景色
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 49);
    [self.view bringSubviewToFront:self.bottomStatusLabel];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)rightBtnPressed:(UIButton*)btn {
    self.rightBtn.hidden = YES;
    self.keyword = nil;
    self.searchBar.text = nil;
    [self.searchBar resignFirstResponder];
    [self loadData:NO];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.rightBtn.hidden = NO;
    self.keyword = searchBar.text;
    [searchBar resignFirstResponder];
    [self loadData:NO];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    self.rightBtn.hidden = NO;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    self.rightBtn.hidden = YES;
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        if (self.type == 0) {
            return nil;
        } else if (self.type == 1) {
            return self.resultTopView;
        } else {
            return self.noresultView;
        }
    } else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.type == 0) {
        return 0.01;
    } else if (self.type == 1) {
        if (section) {
            return 0.01;
        } else
            return self.resultTopView.bounds.size.height;
    } else {
        if (section) {
            return 0.01;
        } else
            return self.noresultView.bounds.size.height;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"giftCell";
    GiftCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [GiftCell giftCellOwner:self];
        cell.giftImageView.layer.cornerRadius = 10;
        cell.giftImageView.layer.masksToBounds = YES;
    }
    if (!self.dataArray.count) {
        return cell;
    }
    [cell.giftImageView setImageWithURL:[NSURL URLWithString:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    cell.giftTitleLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"title"];
    cell.giftInfoLabel.text = [NSString stringWithFormat:@"%@",[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"label"]];
    if ([[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"isGet"] integerValue]) {
        [cell.giftBtn setTitle:@"已领取" forState:UIControlStateNormal];
        [cell.giftBtn setBackgroundImage:[UIImage imageNamed:@"lingqu_gray.png"] forState:UIControlStateNormal];
    } else {
        if (![[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"giftCodeCount"] integerValue]) {
            [cell.giftBtn setTitle:@"已抢完" forState:UIControlStateNormal];
            [cell.giftBtn setBackgroundImage:[UIImage imageNamed:@"lingqu_gray.png"] forState:UIControlStateNormal];
        } else {
            [cell.giftBtn setTitle:@"领取" forState:UIControlStateNormal];
            [cell.giftBtn setBackgroundImage:[UIImage imageNamed:@"lingqu.png"] forState:UIControlStateNormal];
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"gl" to_page:@"gd" to_section:@"c" to_step:@"d" type:@"c" channel:@"" id:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"]];
    [[LogHelper shared] writeToFilefrom_page:@"gl" from_section:@"g" from_step:@"l" to_page:@"gd" to_section:@"g" to_step:@"d" type:@"" id:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"]];
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    GiftDetailController *detailView = [[GiftDetailController alloc] initWithNibName:@"GiftDetailController" bundle:nil];
    detailView.giftID = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"];
    detailView.giftTitle = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"title"];
    self.currentIndex = indexPath.section;
    detailView.delegate = self;
    [self.navigationController pushViewController:detailView animated:YES];
}

-(void)loadData:(BOOL)isOrmore{
    if (isOrmore == YES) {
        page++;
    } else {
        page = 1;
        [self.dataArray removeAllObjects];
    }
    [self giftList];
}
#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{

    CGPoint contentOffsetPoint = self.tableView.contentOffset;
    CGRect frame = self.tableView.frame;
    if (contentOffsetPoint.y >= self.tableView.contentSize.height - frame.size.height + 2 || self.tableView.contentSize.height < frame.size.height)
    {
        [self.tableView tableViewDidEndDragging:scrollView];
//        [self loadData:YES];
    }
    [self.tableView tableViewDidScroll:scrollView];
//    CGPoint contentOffsetPoint = self.tableView.contentOffset;
//    CGRect frame = self.tableView.frame;
//    if (contentOffsetPoint.y >= self.tableView.contentSize.height - frame.size.height + 2 || self.tableView.contentSize.height < frame.size.height)
//    {
//        [self loadData:YES];
//    }
}
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    _lastContentOffset = scrollView.contentOffset.y;
    [self.searchBar resignFirstResponder];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    CGPoint contentOffsetPoint = self.tableView.contentOffset;
    if (contentOffsetPoint.y < -60)
    {
        [self.tableView tableViewDidEndDragging:scrollView];
    }
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self.tableView tableViewDidScroll:scrollView];
    CGPoint contentOffsetPoint = self.tableView.contentOffset;
    CGRect frame = self.tableView.frame;
    if (contentOffsetPoint.y >= self.tableView.contentSize.height - frame.size.height + 2 || self.tableView.contentSize.height < frame.size.height)
    {
        if (self.type==0) {
            [self loadData:YES];
        }
    }
}
#pragma mark - GiftDetailDelegate
- (void)giftDetailWithResult:(NSString *)giftID status:(NSString *)status {
    if (self.dataArray.count) {
        if (self.currentIndex < self.dataArray.count) {
            NSMutableDictionary *item = [NSMutableDictionary dictionaryWithDictionary:[self.dataArray objectAtIndex:self.currentIndex]] ;
            [item setValue:@"1" forKey:@"isGet"];
            [self.dataArray replaceObjectAtIndex:self.currentIndex withObject:item];
            
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:self.currentIndex]] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

#pragma mark - 加载数据
- (void)giftList {
    if (_isShow) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _isShow = NO;
    }
    
    
    NSMutableDictionary *exprame;
    if (self.currentSize) {
        exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   (NSString *)API_URL_GIFT_LIST,@"r",
                   [NSString stringWithFormat:@"%d",self.currentSize],@"size",
                   [NSString stringWithFormat:@"%d",page],@"page",nil];
        
    } else {
        exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                   (NSString *)API_URL_GIFT_LIST,@"r",
                   [NSString stringWithFormat:@"%d",size],@"size",
                   [NSString stringWithFormat:@"%d",page],@"page",nil];
        
    }
    if (self.keyword) {
        [exprame setObject:self.keyword forKey:@"searchTitle"];
    }
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(giftListFinish:) method:GETDATA];
}

- (void)giftListFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        self.type = [[[dic objectForKey:@"extraInfo"] objectForKey:@"otherType"] integerValue];
        
        if (self.type == 1) {
            self.resultTopLabel.text = [NSString stringWithFormat:@"找到\"%@\"相关的礼包%@个", self.keyword, [[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"]];
        } else if (self.type == 2) {
            self.noresultLabel.text = [NSString stringWithFormat:@"未能找到\"%@\"相关的礼包", self.keyword];
        }
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }
        
        [self.tableView reloadData];
        
        if (self.currentSize) {
            if ([[dic objectForKey:@"results"] count] < self.currentSize) {
//                self.tableView.reachedTheEnd = YES;
            } else {
//                self.tableView.reachedTheEnd = NO;
            }
            page = self.currentSize / size + 1;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:self.currentIndex];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
            self.currentSize = 0;
            
            if ([[dic objectForKey:@"results"] count] == 0) {
                [self bottomShow];
            }
            
            if (![self.dataArray count]) {
                self.contentStatusLabel.hidden = NO;
            } else
                self.contentStatusLabel.hidden = YES;
        }
    } else {
        if (self.dataArray.count == 0) {
            _netImageView.frame = CGRectMake(KScreenWidth, 0, KScreenWidth, KScreenheight - 64 - 40);
            _netImageView.alpha = 1;
            
            _refreshBtn1 = [CustomTool createBtn];
            [_netImageView addSubview:_refreshBtn1];
            [_refreshBtn1 addTarget:self action:@selector(prettyImageList) forControlEvents:UIControlEventTouchUpInside];
        }else{
            UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2-45, KScreenheight-80-64, 90, 40)];
            label.backgroundColor = [UIColor grayColor];
            label.layer.masksToBounds = YES;
            label.layer.cornerRadius = 6;
            label.textColor = [UIColor blackColor];
            label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
            label.alpha = 1;
            label.text = @"木有网啊";
            label.textAlignment = 1;
            [self.view addSubview:label];
            [UIView animateWithDuration:1 animations:^{
                label.alpha = 0;
            }];
            label = nil;
        }
    }
    [self.tableView tableViewDidFinishedLoading];
}

#pragma mark -
- (void)pullingCollectionViewDidStartRefreshing:(PullingRefreshTableView *)tableView
{
    [self performSelector:@selector(refreshData) withObject:nil afterDelay:0.2f];
}

- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView
{
//    if (self.type == 0) {
    [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.2f];
//    }else{
//        [self performSelector:@selector(lllll) withObject:nil afterDelay:0.2f];
//    }
}
-(void)lllll{
    
}
-(void)refreshData
{
    [[LogHelper shared] writeToFilefrom_page:@"gl" from_section:@"m" from_step:@"l" to_page:@"gl" to_section:@"m" to_step:@"l" type:@"refresh" id:@"0"];
    [self loadData:NO];
}

-(void)loadMoreData
{
    [[LogHelper shared] writeToFilefrom_page:@"gl" from_section:@"m" from_step:@"l" to_page:@"gl" to_section:@"m" to_step:@"l" type:@"more" id:@"0"];

    [self loadData:YES];
}
- (NSDate *)pullingTableViewRefreshingFinishedDate
{
    return [NSDate date];
}
@end
