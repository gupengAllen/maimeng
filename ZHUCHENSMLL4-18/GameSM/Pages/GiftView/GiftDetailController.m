//
//  GiftDetailController.m
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "GiftDetailController.h"
#import "UIImageView+AFNetworking.h"
#import "LoginViewController.h"
#import "MBProgressHUD+Add.h"
#import "CustomNavigationController.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface GiftDetailController ()

@property (nonatomic, strong) NSMutableDictionary *dataDic;
@property (nonatomic, strong) NSString *codeValue;

@end

@implementation GiftDetailController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self initTitleName:self.giftTitle];
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    
    [self giftDetail];
}

#pragma mark - 按钮事件

- (IBAction)giftBtnPressed:(id)sender {
    
    if (!g_App.userID) {
        [[LogHelper shared] writeToFilefrom_page:@"gd" from_section:@"g" from_step:@"d" to_page:@"nl" to_section:@"g" to_step:@"r" type:@"" id:self.giftID];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才能领取礼包哦~"];
        [alertView show];
        return;
    }
    
    [self giftGetCode];
}

- (IBAction)copyBtnPressed:(id)sender {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"gcc" to_page:@"" to_section:@"c" to_step:@"c" type:@"c" channel:@"" id:self.giftID];
    [[LogHelper shared] writeToFilefrom_page:@"gd" from_section:@"g" from_step:@"d" to_page:@"gc" to_section:@"g" to_step:@"a" type:@"" id:self.giftID];
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = self.codeValue;
//    [[[self.dataDic objectForKey:@"getCode"] firstObject] objectForKey:@"code"];
    [MBProgressHUD showSuccess:@"复制成功" toView:self.view];
}

#pragma mark - LoginAlertViewDelegate

- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        [[LogHelper shared] writeToFilefrom_page:@"nl" from_section:@"g" from_step:@"r" to_page:@"pl" to_section:@"g" to_step:@"r" type:@"" id:self.giftID];
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
        return;
    }
    [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"g" from_step:@"r" to_page:@"gg" to_section:@"g" to_step:@"a" type:@"" id:self.giftID];
}

- (void)refreshView {
    [self.giftImageView setImageWithURL:[NSURL URLWithString:[self.dataDic objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    self.giftImageView.layer.cornerRadius = 10;
    self.giftImageView.layer.masksToBounds = YES;
    
    self.giftTitleLabel.text = self.giftTitle;
    if (!self.giftTitle) {
        self.giftTitleLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"ch_name"];
        [self initTitleName:[[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"ch_name"]];
    }
    CGSize size = [self.giftTitleLabel.text sizeWithFont:self.giftTitleLabel.font constrainedToSize:CGSizeMake(self.giftTitleLabel.bounds.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    if (size.height > 22) {
        self.giftTitleLabel.frame = CGRectMake(self.giftTitleLabel.frame.origin.x, self.giftTitleLabel.frame.origin.y, self.giftTitleLabel.frame.size.width, size.height);
    } else {
        self.giftTitleLabel.frame = CGRectMake(self.giftTitleLabel.frame.origin.x, self.giftTitleLabel.frame.origin.y, self.giftTitleLabel.frame.size.width, 22);
    }
    self.remainLabel.frame = CGRectMake(self.remainLabel.frame.origin.x, self.giftTitleLabel.frame.origin.y + self.giftTitleLabel.bounds.size.height, self.remainLabel.frame.size.width, self.remainLabel.frame.size.height);
    self.giftTimeView.frame = CGRectMake(self.giftTimeView.frame.origin.x, self.giftTitleLabel.frame.origin.y + self.giftTitleLabel.frame.size.height + 20, self.giftTimeView.frame.size.width, self.giftTimeView.frame.size.height);
    
    self.giftTopView.frame = CGRectMake(self.giftTopView.frame.origin.x, self.giftTopView.frame.origin.y, self.giftTopView.frame.size.width, self.giftTimeView.frame.origin.y + self.giftTimeView.frame.size.height + 8);
    
    self.infoLabel.frame = CGRectMake(self.infoLabel.frame.origin.x, self.giftTopView.frame.size.height + 5, self.infoLabel.frame.size.width, self.infoLabel.frame.size.height);
    
    self.webView.frame = CGRectMake(self.giftTopView.frame.origin.x, self.libaoLabel.bottom, self.giftTopView.frame.size.width, self.webView.frame.size.height);//self.giftTopView.frame.size.height+40
    self.giftTimeLabel.text = [NSString stringWithFormat:@"领取时间：%@",[self.dataDic objectForKey:@"rangeTimeValue"]];
    
    self.remainLabel.text = [NSString stringWithFormat:@"剩余%@", [self.dataDic objectForKey:@"giftCodeCount"]];
    [self.webView loadHTMLString:[self.dataDic objectForKey:@"contentValue"] baseURL:nil];
    
    self.giftBtn.hidden = NO;
    if ([[self.dataDic objectForKey:@"isGet"] integerValue]) {
        self.giftBtn.hidden = YES;
        self.codeLayerView.hidden = YES;
        self.codeLabel.text = [NSString stringWithFormat:@"礼包卡号： %@", [[[self.dataDic objectForKey:@"getCode"] firstObject] objectForKey:@"code"]];
        self.codeValue = [[[self.dataDic objectForKey:@"getCode"] firstObject] objectForKey:@"code"];
    } else {
        if (![[self.dataDic objectForKey:@"giftCodeCount"] integerValue]) {
            [self.giftBtn setTitle:@"抢完啦" forState:UIControlStateNormal];
            [self.giftBtn setBackgroundImage:[UIImage imageNamed:@"lingqu_big_gray"] forState:UIControlStateNormal];
            self.giftBtn.enabled = YES;
        } else {
            [self.giftBtn setTitle:@"领取" forState:UIControlStateNormal];
            self.giftBtn.enabled = YES;
            [self.giftBtn setBackgroundImage:[UIImage imageNamed:@"lingqu_big"] forState:UIControlStateNormal];
        }
    }
    
}

#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGRect frame = webView.frame;
    NSString *fitHeight = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight;"];
    frame.size.height = [fitHeight floatValue];
    webView.frame = frame;
    webView.hidden = NO;
    
    [self.scrollView setContentSize:CGSizeMake(0,  webView.frame.origin.y + webView.frame.size.height)];
    if (self.scrollView.contentSize.height < self.view.bounds.size.height - self.bottomView.bounds.size.height) {
        [self.scrollView setContentSize:CGSizeMake(0, self.view.bounds.size.height + 1)];
    }
}


#pragma mark - 加载数据
- (void)giftDetail {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LogHelper shared] writeToFilefrom_page:@"pl" from_section:@"g" from_step:@"r" to_page:@"gg" to_section:@"g" to_step:@"g" type:@"a" id:self.giftID];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_GIFT_DETAIL,@"r",
                                    self.giftID,@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(giftDetailFinish:) method:GETDATA];
}

- (void)giftDetailFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        self.dataDic = [dic objectForKey:@"results"];
        [self refreshView];
    }
}

- (void)giftGetCode {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"gc" to_page:@"" to_section:@"c" to_step:@"c" type:@"c" channel:@"" id:self.giftID];
    
    [[LogHelper shared] writeToFilefrom_page:@"gd" from_section:@"g" from_step:@"d" to_page:@"gg" to_section:@"g" to_step:@"a" type:@"" id:self.giftID];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_GIFTCODE_GETCODE,@"r",
                                    self.giftID,@"giftID",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(giftGetCodeFinish:) method:POSTDATA];
}

- (void)giftGetCodeFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        self.codeLabel.text = [NSString stringWithFormat:@"礼包卡号： %@", [[dic objectForKey:@"results"] objectForKey:@"code"]];
        self.codeValue = [[dic objectForKey:@"results"] objectForKey:@"code"];
        self.giftBtn.hidden = YES;
        [UIView animateWithDuration:0.3 animations:^{
            self.codeLayerView.backgroundColor = [UIColor clearColor];
        } completion:^(BOOL finished) {
            self.codeLayerView.hidden = YES;
        }];
        if (_delegate && [_delegate respondsToSelector:@selector(giftDetailWithResult:status:)]) {
            [_delegate giftDetailWithResult:self.giftID status:nil];
        }
    }
}

@end
