//
//  GiftCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "GiftCell.h"

@implementation GiftCell

+ (id)giftCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"GiftCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[GiftCell class]]) {
            return (GiftCell *)cellObject;
        }
    }
    return nil;
}
//自定义分割线
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect); //上分割线，
//    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
//    CGContextStrokeRect(context, CGRectMake(20, -1, rect.size.width - 40, 1)); //下分割线
        CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
        CGContextStrokeRect(context, CGRectMake(20, rect.size.height, rect.size.width - 40, 1));
}

@end
