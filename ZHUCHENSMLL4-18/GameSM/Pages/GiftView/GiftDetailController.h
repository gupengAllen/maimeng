//
//  GiftDetailController.h
//  GameSM
//
//  Created by 王涛 on 15/7/15.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@protocol GiftDetailDelegate <NSObject>

@optional
- (void)giftDetailWithResult:(NSString*)giftID status:(NSString*)status;

@end

@interface GiftDetailController : BaseViewController<LoginAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *giftTopView;
@property (weak, nonatomic) IBOutlet UIImageView *giftImageView;
@property (weak, nonatomic) IBOutlet UILabel *giftTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *giftTimeView;
@property (weak, nonatomic) IBOutlet UILabel *giftTimeLabel;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *remainLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIButton *giftBtn;

@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UIView *codeLayerView;

@property (nonatomic, assign) id<GiftDetailDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *libaoLabel;

@property (nonatomic, strong) NSString *giftTitle;
@property (nonatomic, strong) NSString *giftID;

- (IBAction)giftBtnPressed:(id)sender;
- (IBAction)copyBtnPressed:(id)sender;
@end
