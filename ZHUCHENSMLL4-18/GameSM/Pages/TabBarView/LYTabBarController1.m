//
//  LYTabBarController.m
//  Linyi360
//
//  Created by wt on 14-9-19.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import "LYTabBarController.h"
#import "HomeViewController.h"
#import "GiftViewController.h"
#import "UserViewController.h"
#import "CustomNavigationController.h"
#import "GuideViewController.h"
#import "ManhuaViewController.h"
#import "PictureViewController.h"
#import "LYTabBarView.h"
#import "Y_X_DataInterface.h"

@interface LYTabBarController ()
{
    int _giftStatus;
    UIImageView *_bgiv;
    UILabel *_nameLabel1;
    UILabel *_nameLabel2;
    UILabel *_nameLabel3;
    UILabel *_nameLabel4;
    int _toTop;
}
@end

@implementation LYTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.tabBarView = [[LYTabBarView alloc]init];
    // Do any additional setup after loading the view.
    [self hideExistingTabBar];
//    self.tabBarView = [[LYTabBarView alloc]init];
//    _bgiv = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
//    if (self.view.frame.size.height <= 481){//4
//        _bgiv.image = [UIImage imageNamed:@"640*960.png"];
//    }else if (self.view.frame.size.height<=569 && self.view.frame.size.height>=500) {//5
//        _bgiv.image = [UIImage imageNamed:@"640*1136.png"];
//    }else if (self.view.frame.size.height<=1000 && self.view.frame.size.height>=660){//6
//        _bgiv.image = [UIImage imageNamed:@"750*1334.png"];
//    }else{//6p
//        _bgiv.image = [UIImage imageNamed:@"1242*2208.png"];
//    }
//    [self.view addSubview:_bgiv];
    
    [self initView];
    [self initTabView];
    [self createArticleIsReadPlistFile];
}

-(void)createArticleIsReadPlistFile{
    //获取本地沙盒路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"articleIsRead.plist"];
    if (![fm fileExistsAtPath:plistPath]) {
        [fm createFileAtPath:plistPath contents:nil attributes:nil];
        NSArray *arr = [[NSArray alloc] initWithObjects:@"userArticleIsRead", nil];
        [arr writeToFile:plistPath atomically:YES];
    }
}
- (void)initView {
    HomeViewController *homeView = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    homeView.hidesBottomBarWhenPushed = YES;
    CustomNavigationController *homeNav = [[CustomNavigationController alloc] initWithRootViewController:homeView];
    
    UserViewController *userView = [[UserViewController alloc] initWithNibName:@"UserViewController" bundle:nil];
    userView.hidesBottomBarWhenPushed = YES;
    CustomNavigationController *userNav = [[CustomNavigationController alloc] initWithRootViewController:userView];
  
//    // 礼包以后修改
//    GiftViewController *giftView = [[GiftViewController alloc] initWithNibName:@"GiftViewController" bundle:nil];
//    giftView.hidesBottomBarWhenPushed = YES;
//    CustomNavigationController *giftNav = [[CustomNavigationController alloc]initWithRootViewController:giftView];
    
    ManhuaViewController *manhuaView = [[ManhuaViewController alloc]initWithNibName:@"ManhuaViewController" bundle:nil];
    manhuaView.hidesBottomBarWhenPushed = YES;
    CustomNavigationController *manhuaNav = [[CustomNavigationController alloc]initWithRootViewController:manhuaView];

#warning 第一天修改加个导航控制器
    
    PictureViewController *picView = [[PictureViewController alloc] initWithNibName:@"PictureViewController" bundle:nil];
    picView.hidesBottomBarWhenPushed = YES;
    CustomNavigationController *picNav = [[CustomNavigationController alloc] initWithRootViewController:picView];
    
    self.viewControllers = @[homeNav,manhuaNav,picNav,userNav];
#warning 第一天修改标签栏
//    NSLog(@"--%@",[Y_X_DataInterface commonParams]);
//    NSString *clientversion = [Y_X_DataInterface commonParams][@"clientversion"];
//    NSString *giftStr = [NSString stringWithFormat:@"%@r=%@&deviceType=4&clientVersion=%@",INTERFACE_PREFIX,API_URL_GIFT_SWITCH,clientversion];    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager GET:giftStr parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        NSString *status = backData[@"status"];
//        _giftStatus = [status intValue];
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:status forKey:@"giftStatus"];
//        [userDefaults synchronize];
//        
//        
////        if (_giftStatus == 1) {
////            //            self.viewControllers = @[manhuaNav,homeNav,giftNav,userNav];//
////            self.viewControllers = @[homeNav,manhuaNav,giftNav,userNav];
////        }else if (_giftStatus == 0){
////            //            self.viewControllers = @[manhuaNav,homeNav,userNav];
////            self.viewControllers = @[homeNav,manhuaNav,userNav];
////        }
//        
//         self.viewControllers = @[homeNav,manhuaNav,picNav,userNav];
//        [self initTabView];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        _giftStatus = 0;
//        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//        [userDefaults setObject:@(_giftStatus) forKey:@"giftStatus"];
//        [userDefaults synchronize];
//        
////        if (_giftStatus == 1) {
////            //            self.viewControllers = @[manhuaNav,homeNav,giftNav,userNav];//
////            self.viewControllers = @[homeNav,manhuaNav,giftNav,userNav];
////        }else if (_giftStatus == 0){
////            //            self.viewControllers = @[manhuaNav,homeNav,userNav];
////            self.viewControllers = @[homeNav,manhuaNav,userNav];
////        }
//        self.viewControllers = @[homeNav,manhuaNav,picNav,userNav];
//        [self initTabView];
//    }];
}

- (void)initTabView {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDelay:0.0f];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationCurve:UIViewAnimationCurveLinear];
    [UIView setAnimationRepeatCount:1];
//    [UIView setAnimationDelegate:_bgiv];
//    _bgiv.alpha = 0;
    [UIView commitAnimations];
//    NSArray *nameArr3 = @[@"萌资讯",@"漫画",@"萌窝"];
    NSArray *nameArr4 = @[@"萌资讯",@"漫画",@"美图",@"萌窝"];
    
//    if (_giftStatus == 1) {
        self.tabBarView = [[LYTabBarView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 49, self.view.bounds.size.width, 49) withImages:@[@"btn_zixun_2",@"btn_manhua_2",@"btn_meitu_2",@"btn_mengwo_2"] highlightImages:@[@"btn_zixun_1",@"btn_manhua_1",@"btn_meitu_1",@"btn_mengwo_1"] withName:nameArr4 withLightName:nameArr4];
//        for (int i = 0; i <4; i ++) {
//            UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(i*self.view.bounds.size.width/4, 30, self.view.bounds.size.width/4, 19)];
//            nameLabel.text = nameArr4[i];
//            nameLabel.textAlignment = 1;
//            nameLabel.tag = i;
//            nameLabel.font = [UIFont systemFontOfSize:14];
//            nameLabel.textColor = [UIColor colorWithRed:254/255.0 green:121/255.0 blue:137/255.0 alpha:1];
//            [self.tabBarView addSubview:nameLabel];
//        }
//    }else{
//        self.tabBarView = [[LYTabBarView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 49, self.view.bounds.size.width, 49) withImages:@[@"btn_zixun_2",@"btn_manhua_2",@"btn_mengwo_2"] highlightImages:@[@"btn_zixun_1",@"btn_manhua_1",@"btn_mengwo_1"]
//                           withName:nameArr3 withLightName:nameArr3];
//    }
//    self.tabBarView.statusBar = _giftStatus;
    int width = 0;
    if (self.view.bounds.size.width < self.view.bounds.size.height) {
        width = self.view.bounds.size.width;
    } else {
        width = self.view.bounds.size.height;
    }
    
//    if (_giftStatus == 1) {
        self.tabBarView = [[LYTabBarView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 49, width, 49) withImages:@[@"btn_zixun_2",@"btn_manhua_2",@"btn_meitu_2.png",@"btn_mengwo_2"] highlightImages:@[@"btn_zixun_1",@"btn_manhua_1",@"btn_meitu_1.png",@"btn_mengwo_1"] withName:nameArr4 withLightName:nameArr4];
//    }else if (_giftStatus == 0){
//        self.tabBarView = [[LYTabBarView alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 49, width, 49) withImages:@[@"btn_zixun_2",@"btn_manhua_2",@"btn_mengwo_2"] highlightImages:@[@"btn_zixun_1",@"btn_manhua_1",@"btn_mengwo_1"] withName:nameArr3 withLightName:nameArr3];
//    }
//    self.tabBarView.statusBar = _giftStatus;
    [self.view addSubview:_tabBarView];
    _tabBarView.delegate = self;
    self.tabBarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    UIButton *btn = (UIButton*)[_tabBarView viewWithTag:100];
    [_tabBarView tabWasSelected:btn];
}

-(void)GoBackToHome{
    UIButton *btn = (UIButton*)[_tabBarView viewWithTag:100];
    [_tabBarView tabWasSelected:btn];
}

-(void)gotoViewController:(NSNotification *)centInfo{

    int tagValue=[[centInfo.userInfo objectForKey:@"tag"] intValue];
    UIButton *btn = (UIButton*)[_tabBarView viewWithTag:tagValue];
    [_tabBarView tabWasSelected:btn];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)hideExistingTabBar {
    for(UIView *view in self.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
    	{
            view.hidden = YES;
            break;
        }
    }
}

#pragma mark - LYTabBarViewDelegate
- (void)tabbarWasSelectedWithIndex:(int)index {
    NSInteger lastSelectedIndex = self.selectedIndex;
    CustomNavigationController *nav = [self.viewControllers objectAtIndex:index];
    [nav popToRootViewControllerAnimated:YES];
    self.selectedIndex = index;
    if(lastSelectedIndex != self.selectedIndex){
        if (lastSelectedIndex == 0&&self.selectedIndex == 1) {
            [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"crh" to_section:@"c" to_step:@"h" type:@"" id:@"0"];
        }else if (lastSelectedIndex == 0&&self.selectedIndex == 2){
            [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"gl" to_section:@"g" to_step:@"l" type:@"" id:@"0"];
        }else if (lastSelectedIndex == 1&&self.selectedIndex == 0){
            [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"" id:@"0"];
        }else if (lastSelectedIndex == 1&&self.selectedIndex == 2){
            [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"gl" to_section:@"g" to_step:@"l" type:@"" id:@"0"];
        }else if (lastSelectedIndex == 2&&self.selectedIndex == 0){
            [[LogHelper shared] writeToFilefrom_page:@"gl" from_section:@"g" from_step:@"l" to_page:@"ml" to_section:@"m" to_step:@"l" type:@"" id:@"0"];
        }else if (lastSelectedIndex == 2&&self.selectedIndex == 1){
            [[LogHelper shared] writeToFilefrom_page:@"gl" from_section:@"g" from_step:@"l" to_page:@"crh" to_section:@"c" to_step:@"h" type:@"" id:@"0"];
        }
    }
        
    
    if (index == 0) {
        if (_toTop == 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"toNewsTop" object:nil];
        }
    }
    _toTop = index;
}

- (NSString *)stringWithProtocol:(NSInteger)index{
    NSString *returnStr = @"";
    switch (index) {
        case 0:
            return returnStr = @"crh";
            break;
        case 1:
            return returnStr = @"csh";
            break;
        case 2:
            return returnStr = @"cbh";
            break;
        case 3:
            return returnStr = @"cch";
            break;
        case 4:
            return returnStr = @"cfh";
            
        default:
            return returnStr;
            break;
    }
    
}


- (void)hiddenBar:(BOOL)hidden animated:(BOOL)animated {
    
    self.tabBarView.hidden = NO;
    if (hidden) {
        if (self.tabBarView.frame.origin.y == self.view.frame.size.height - 49 + self.tabBarView.frame.size.height) {
            self.tabBarView.hidden = hidden;
            return;
        }
    } else {
        if (self.tabBarView.frame.origin.y == self.view.frame.size.height - 49) {
            self.tabBarView.hidden = NO;
            return;
        }
    }
    
    if (animated) {
        [UIView animateWithDuration:0.3 animations:^{
            if (hidden) {
                self.tabBarView.frame = CGRectMake(0, self.view.frame.size.height - 49 + self.tabBarView.frame.size.height, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
            } else {
                self.tabBarView.frame = CGRectMake(0, self.view.frame.size.height - 49, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
            }
        } completion:^(BOOL finished) {
            self.tabBarView.hidden = hidden;
        }];
    } else {
        if (hidden) {
            self.tabBarView.frame = CGRectMake(0, self.view.frame.size.height - 49 + self.tabBarView.frame.size.height, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
        } else {
            self.tabBarView.frame = CGRectMake(0, self.view.frame.size.height - 49, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
        }
        
        self.tabBarView.hidden = hidden;
    }
    
    
//    if (hidden) {
//        if (self.tabBarView.frame.origin.x == -self.tabBarView.frame.size.width) {
//            return;
//        }
//    } else {
//        if (self.tabBarView.frame.origin.x == 0) {
//            return;
//        }
//    }
//    
//    if (animated) {
//        [UIView animateWithDuration:0.25f animations:^{
//            if (hidden) {
//                self.tabBarView.frame = CGRectMake(-self.tabBarView.frame.size.width, self.tabBarView.frame.origin.y, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
//            } else {
//                self.tabBarView.frame = CGRectMake(0, self.tabBarView.frame.origin.y, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
//            }
//        }];
//    } else {
//        if (hidden) {
//            self.tabBarView.frame = CGRectMake(-self.tabBarView.frame.size.width, self.tabBarView.frame.origin.y, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
//        } else {
//            self.tabBarView.frame = CGRectMake(0, self.tabBarView.frame.origin.y, self.tabBarView.frame.size.width, self.tabBarView.frame.size.height);
//        }
//    }
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}


@end
