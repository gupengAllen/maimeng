//
//  SSTabBarItem.m
//  SevenSnack
//
//  Created by 顾鹏 on 15/12/7.
//  Copyright © 2015年 顾鹏. All rights reserved.
//

#import "SSTabBarItem.h"
#import <QuartzCore/QuartzCore.h>

@interface SSTabBarItem()
{
    UILabel * _badgeLabel;
}
@end


@implementation SSTabBarItem

#define __TabBar_Font       [UIFont systemFontOfSize:11.0f]

-(id) initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
//        UIColor * textColor= RGBA_COLOR(0, 136, 66, 1.0);
//        UIColor * textColor_normal= RGBA_COLOR(141, 138, 138, 1.0);
        
        self.titleLabel.font = __TabBar_Font;
        self.adjustsImageWhenHighlighted = NO;
        self.showsTouchWhenHighlighted = NO;
//        [self setTitleColor:textColor_normal forState:UIControlStateNormal];
//        [self setTitleColor:textColor forState:UIControlStateSelected];
        
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    return self;
}

//重写父类高亮状态
-(void) setHighlighted:(BOOL)highlighted{
    
}

-(void) setBadgeValue:(NSString *)badgeValue{
    if (!_badgeLabel) {
        _badgeLabel = [[UILabel alloc] init];
        CGRect frame = CGRectMake(0, 0, 17, 17);
        _badgeLabel.bounds = frame;
        //_badgeLabel.backgroundColor = RGBA_COLOR(81, 194, 0, 1.0);
        _badgeLabel.backgroundColor = [UIColor redColor];
        _badgeLabel.textColor = [UIColor whiteColor];
        _badgeLabel.font = [UIFont systemFontOfSize:12.0f];
        _badgeLabel.textAlignment = NSTextAlignmentCenter;
        _badgeLabel.layer.cornerRadius = frame.size.width / 2;
        _badgeLabel.layer.masksToBounds = YES;
        [self addSubview:_badgeLabel];
    }
    
    if (badgeValue != nil) {
        _badgeLabel.hidden = NO;
        _badgeLabel.text = badgeValue;
        
        CGRect frame = CGRectMake((self.bounds.size.width-30)/2+27, 5, 17, 17);
        
        if (badgeValue.length >= 2) {
            frame.origin.x = (self.bounds.size.width-30)/2+25;
            frame.size.width = 21.0f;
            _badgeLabel.frame = frame;
        }
        else{
            _badgeLabel.frame = frame;
        }
    }
    else{
        _badgeLabel.hidden = YES;
    }
}

-(CGRect) imageRectForContentRect:(CGRect)contentRect{
    contentRect.origin.x = (self.bounds.size.width - self.currentImage.size.width)/2;
    contentRect.origin.y = (self.bounds.size.height - self.currentImage.size.height)/2-5;
    contentRect.size.height = self.currentImage.size.height-3;
    contentRect.size.width = self.currentImage.size.width-2;
    return contentRect;
}

-(CGRect) titleRectForContentRect:(CGRect)contentRect{
    contentRect.origin.x = 0;
    contentRect.origin.y = contentRect.size.height - 20 -2;
    contentRect.size.height = 20;
    return contentRect;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (!self.selected) {
        if (_backgroundImage) {
            [[UIColor colorWithPatternImage:_backgroundImage] set];
            CGContextFillRect(context, rect);
        }
        else {
            [[UIColor whiteColor] set];
            CGContextFillRect(context, rect);
        }
    } else {
        if (_selectedBackgroundImage) {
            [[UIColor colorWithPatternImage:_selectedBackgroundImage] set];
            CGContextFillRect(context, rect);
        }
        else {
            [[UIColor whiteColor] set];
            CGContextFillRect(context, rect);
        }
    }
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
