//
//  LYTabBarController.h
//  Linyi360
//
//  Created by wt on 14-9-19.
//  Copyright (c) 2014年 wt. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LYTabBarView.h"

@interface LYTabBarController : UITabBarController<LYTabBarViewDelegate>

@property (nonatomic, retain) LYTabBarView *tabBarView;

- (void)hiddenBar:(BOOL)hidden animated:(BOOL)animated;

@end
