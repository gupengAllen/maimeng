//
//  CommicTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class bigRecommentModel;
@class CartoonInfoAll;
@interface CommicTableViewCell : UITableViewCell
@property(nonatomic,copy)bigRecommentModel *bigRecommModel;
@property(nonatomic,copy)CartoonInfoAll *cartoonModel;
@end
