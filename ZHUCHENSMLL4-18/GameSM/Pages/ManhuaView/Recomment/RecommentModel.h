//
//  RecommentModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/10.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RecommentModel : NSObject

@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *images;
@property(nonatomic,strong)NSString *author;
@property(nonatomic,strong)NSString *categorys;
@property(nonatomic,strong)NSString *introduction;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *createTime;
@property(nonatomic,strong)NSString *modifyTime;
@property(nonatomic,strong)NSString *userID;
@property(nonatomic,strong)NSString *id;
@property (nonatomic, copy)NSString *updateInfo;
@property (nonatomic, copy)NSString *totalChapterCount;
@property (nonatomic, copy)NSString *hitCount;
@property (nonatomic, copy)NSString *thirdUpdateTime;
@property (nonatomic, copy)NSString *thirdID;
@property (nonatomic, copy)NSString *level;
@property (nonatomic, copy)NSString *remark;
@property (nonatomic, copy)NSString *priority;
@property (nonatomic, copy)NSString *isOver;
@property (nonatomic, copy)NSString *albumId;
@property (nonatomic, copy)NSString *chapterIndex;
@property (nonatomic, copy)NSString *chapterNameLabel;
@property (nonatomic, copy)NSString *chapterName;
@property (nonatomic, copy)NSString *chapterId;
@property (nonatomic,strong)NSArray *chapterListArr;

@end
