//
//  RecommentCell.h
//  GameSM
//
//  Created by mac on 15/9/22.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "RecommentTableView.h"
#import "RecommentCollection.h"
#import "BaseTableViewCell.h"
@class RecommentModel;
@class CartoonInfoAll;
@interface RecommentCell : BaseTableViewCell

@property(nonatomic, strong)RecommentCollection *recommentCollection;

//+(instancetype)recommentCellWithTableView:(UITableView *)tv;

//+(CGFloat)cellHeight;

@property(nonatomic,strong)CartoonInfoAll *model;
@property(nonatomic,strong)CartoonInfoAll *model1;
@property(nonatomic,strong)CartoonInfoAll *model2;

//@property(nonatomic,strong)RecommentModel *model;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andSection:(NSInteger)section andContinueBool:(BOOL)continueBool andMaxSection:(NSInteger)maxSection;


@end
