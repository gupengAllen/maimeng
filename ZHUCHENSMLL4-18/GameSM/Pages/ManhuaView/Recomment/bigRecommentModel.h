//
//  bigRecommentModel.h
//  GameSM
//
//  Created by 顾鹏 on 15/12/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CartoonInfoAll;
@interface bigRecommentModel : NSObject
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSString *title;
@property (nonatomic, copy)NSString *images;
@property (nonatomic, copy)NSString *valueType;
@property (nonatomic, copy)NSString *valueID;
@property (nonatomic, copy)NSString *url;
@property (nonatomic, copy)NSString *priority;
@property (nonatomic, copy)NSString *status;
@property (nonatomic, copy)NSString *desc;
@property (nonatomic, copy)NSString *createTime;
@property (nonatomic, copy)NSString *modifyTime;
@property (nonatomic, copy)NSString *userID;
@property (nonatomic, copy)NSString *type;
@property (nonatomic, copy)NSString *valueType2;
@property (nonatomic, copy)NSString *valueID2;
@property (nonatomic, copy)NSString *images2;
@property (nonatomic, copy)NSString *url2;
@property (nonatomic, copy)NSString *cartoonID;
@property (nonatomic, retain)CartoonInfoAll *cartoonInfoAll;
@property (nonatomic, copy)NSMutableArray *cartoonSetList;

+ (NSMutableArray *)paraseArr:(NSDictionary *)allDict;
@end
