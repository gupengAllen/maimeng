//
//  RecommentCollection.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "RecommentCollection.h"
#import "Y_X_DataInterface.h"
#import "RecommentCollectionViewCell.h"
#import "RecommentCollectionModel.h"
#import "testViewController.h"
#import "UIView+UIViewController.h"
#import "LogHelper.h"
#import "bigRecommentModel.h"
#import "CartoonInfoAll.h"
@interface RecommentCollection ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *_dataArr;
    NSString        *_identfy;
}
@end

@implementation RecommentCollection
- (instancetype)initWithFrame:(CGRect)frame{
    _dataArr = [NSMutableArray array];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.minimumInteritemSpacing = 10;
    layout.minimumLineSpacing = 10;
    layout.sectionInset = UIEdgeInsetsMake(0, 0, 15, 15);
    layout.itemSize = CGSizeMake((ScreenSizeWidth-40)/3,(ScreenSizeWidth-6*krecommentCellInset)/3*___Scale+20);
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        _identfy = @"recommentKeyCell";
        [self registerClass:[RecommentCollectionViewCell class] forCellWithReuseIdentifier:_identfy];
    }
    return self;
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _dataArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RecommentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identfy forIndexPath:indexPath];
    cell.model = _dataArr[indexPath.row];
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
   CartoonInfoAll* model = _dataArr[indexPath.row];
    [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:model.id];
    
    if (model.currentReadChapterId) {
        NSArray *allInfo = @[model.id,model.name,model.images];
        NSString *str = [NSString stringWithFormat:@"%@r=%@&chapterId=%@&page=1&size=100",INTERFACE_PREFIXD,API_URL_CARTOONCHAPTERALBUM_DETAIL,model.currentReadChapterId];
        NSArray *strAllInfo = @[allInfo,str];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu-2" object:strAllInfo];
    }else{
    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,model.id];
    testViewController *vc = [[testViewController alloc]init];
    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    vc.detailStr = str;
    [vc setChangeCollection:^{
    }];
    
        [self.viewController presentViewController:vc animated:YES completion:nil];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

-(void)setLudic:(bigRecommentModel *)ludic{
    NSArray *arr = ludic.cartoonSetList;
    for (CartoonInfoAll *carModel in arr) {
        [_dataArr addObject:carModel];
    }
    [self reloadData];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
