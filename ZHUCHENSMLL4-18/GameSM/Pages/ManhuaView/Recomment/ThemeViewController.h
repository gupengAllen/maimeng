//
//  ThemeViewController.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"

@interface ThemeViewController : BaseViewController
@property (nonatomic, copy)NSString * id;
@property (nonatomic, copy)NSString *url;
@property (nonatomic, copy)NSString *titleName;
@end
