//
//  RecommentTableView.h
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableView.h"
#import "RecommentHeadView.h"
#import "RecommentModel.h"
#import "PullTableView.h"
#import "PullingRefreshTableView.h"
#import "EGORefreshTableHeaderView.h"

@interface RecommentTableView : BaseTableView<EGORefreshTableHeaderDelegate,PullTableViewDelegate,PullingRefreshTableViewDelegate,UITableViewDataSource,UITableViewDelegate>
{//
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
}
@property(nonatomic,strong)RecommentHeadView *recommentHeardView;

@property(nonatomic,strong)NSMutableDictionary *recommentDataCount;

@property(nonatomic,strong)NSMutableArray *recommentDataArr;
@property(nonatomic,strong)NSMutableArray *recommentCollectionDataArr;
@property(nonatomic,strong)NSMutableArray *recommentHotDataArr;
@property(nonatomic,strong)NSMutableArray *recommentLuDataArr;
@property(nonatomic,strong)NSDictionary *recommentAllDict;


-(void)setRecommentDataArr:(NSMutableArray *)recommentDataArr andRecommentHotDataArr:(NSMutableArray *)recommentHotDataArr andRecommentCollectionDataArr:(NSMutableArray *)recommentCollectionDataArr andRecommentLuDataArr:(NSMutableArray *)recommentLuDataArr;


- (void)reloadTableViewDataSource;
- (void)doneLoadingTableViewData;

@end
