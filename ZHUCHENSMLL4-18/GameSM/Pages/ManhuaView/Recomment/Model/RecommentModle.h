//
//  RecommentModle.h
//  GameSM
//
//  Created by mac on 15/9/22.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseModel.h"

@interface RecommentModle : BaseModel
/*
 {
 "post":{
 "id":13468,
 "genre":1,
 "title":"抖森新角色画风大变，从洛基变成了1940年代乡村歌手",
 "description":"从《雷神》中的大反派洛基直接变成弹着吉他、悠悠唱歌的乡村歌手，不知道抖森能否顺利转型。",
 "publish_time":1439514548,
 "comment_count":2,
 "praise_count":1,
 "super_tag":null,
 "category":{
 "id":3,
 "title":"娱乐",
 "image":"http://app.qdaily.com/system/categories/iconblacks/3/medium/3.png?1411569578",
 "image_small":"http://app.qdaily.com/system/categories/iconyellowapps/3/medium/3.png?1433891560",
 "white_image":"http://app.qdaily.com/system/categories/iconwhiteapps/3/medium/3.png?1433904682"
 },
 "page_style":0,
 "appview":"http://app.qdaily.com/app/articles/13468.html"
 },
 "designs":[
 
 ],
 "image":"http://app.qdaily.com/system/articles/articleshows/13468/medium/13468.jpg?1439446015",
 "type":1,
 "cover":{
 "image":"http://app.qdaily.com/img/missing_article.png",
 "title":null
 }
 }
 */

@property (nonatomic, strong) NSDictionary *post;

@property (nonatomic, strong) NSArray *designs;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, strong) NSDictionary *cover;

@property (nonatomic, assign) NSNumber *type;

@end
