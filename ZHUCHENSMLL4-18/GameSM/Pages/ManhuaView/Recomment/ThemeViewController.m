//
//  ThemeViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/15.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "ThemeViewController.h"
#import "PullingRefreshTableView.h"
#import "CommicTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "CategoryDetailsModel.h"
//#import "bigRecommentModel.h"
#import "CartoonInfoAll.h"
#import "testViewController.h"
#import "ThemeImageTableViewCell.h"
//#import ""
@interface ThemeViewController ()<UITableViewDataSource,UITableViewDelegate,PullingRefreshTableViewDelegate>
{
    UIImageView *themeImage;
    UILabel *themeLabel;
    NSMutableArray *_dataArr;
}
@property (nonatomic, strong) UITableView *noticeTableView;

@end

@implementation ThemeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _dataArr = [NSMutableArray array];
    [self addBackBtn];
    [self.view addSubview:self.noticeTableView];
    UIView *hearderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 250)];
    themeImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 184)];
//    themeImage.backgroundColor = [UIColor redColor];
    
    themeLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 184, ScreenSizeWidth - 14, hearderView.height - themeImage.height)];
    themeLabel.numberOfLines = 0;
    themeLabel.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
    themeLabel.font = [UIFont systemFontOfSize:14.0];
    [hearderView addSubview:themeImage];
    [hearderView addSubview:themeLabel];
    _noticeTableView.tableHeaderView =  hearderView;
    
    
//    UIView *footer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 100)];
//    [footer addSubview:themeImage];
//    [footer addSubview:themeLabel];
//    
//    _noticeTableView.tableFooterView = footer;
    
    
}


- (void)setTitleName:(NSString *)titleName{
    [self initTitleName:titleName];

}
- (void)addBackBtn {
    UIButton *buttonBack=[UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(0, 0, 40, 40);
    
//    if (_isFirst) {
//        [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 10)];
//        [buttonBack setImage:[UIImage imageNamed:@"menu_list.png"] forState:UIControlStateNormal];
//    } else {
        [buttonBack setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
//    }
    
    [buttonBack setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
    [buttonBack addTarget:self action:@selector(goBackToPreview:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:buttonBack];
    self.navigationItem.leftBarButtonItem = backItem;
}

- (void)goBackToPreview:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (UITableView*)noticeTableView {
    if (!_noticeTableView) {
        _noticeTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64)];
        _noticeTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _noticeTableView.separatorColor = [UIColor lightGrayColor];
        _noticeTableView.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
        _noticeTableView.backgroundView = nil;
        _noticeTableView.dataSource = self;
        _noticeTableView.delegate = self;
        _noticeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
    }
    return _noticeTableView;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentidy = @"cellID";
    ThemeImageTableViewCell *commicCell = [_noticeTableView dequeueReusableCellWithIdentifier:cellIdentidy];
    if (commicCell == nil) {
        commicCell = [[ThemeImageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentidy];
    }
    commicCell.selectionStyle = UITableViewCellSelectionStyleNone;
    commicCell.cartoonModel = _dataArr[indexPath.row];
    return commicCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160 * __Scale6;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CartoonInfoAll *carAll = _dataArr[indexPath.row];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,carAll.id];
    [[LogHelper shared] writeToApafrom_page:@"ctd" from_section:@"c" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:carAll.id];
    
    testViewController *vc = [[testViewController alloc]init];
    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    vc.detailStr = str;
    [vc setChangeCollection:^{
        
    }];
    
    [self presentViewController:vc animated:YES completion:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setId:(NSString *)id{
    _id = id;
}

- (void)setUrl:(NSString *)url{
    _url = url;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [YK_API_request startGetLoad:INTERFACE_PREFIX extraParams:@{@"r":_url,@"id":_id,@"size":@"999",@"page":@"1"} object:self action:@selector(requireData:)];
    
}

- (void)requireData:(NSDictionary *)dict{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [themeImage sd_setImageWithURL:[NSURL URLWithString:dict[@"extraInfo"][@"cover"]]];
    if (dict[@"extraInfo"][@"introduction"] != [NSNull null]) {
        themeLabel.text = dict[@"extraInfo"][@"introduction"];
    }
    _dataArr = [CartoonInfoAll paraCartoonInfoAll:dict];
    [_noticeTableView reloadData];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
