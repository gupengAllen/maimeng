//
//  ThemeImageTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/22.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "ThemeImageTableViewCell.h"
#import "bigRecommentModel.h"
#import "UIImageView+AFNetworking.h"
#import "CartoonInfoAll.h"
#import "Config.h"

@implementation ThemeImageTableViewCell


{
    UILabel *_titleName;
    UIImageView *_imageCommic;
    UILabel *_commicTitle;
    UILabel *_autherName;
    UILabel *_commicDes;
    UILabel *_updateTime;
    UIView *_grayView;
    UILabel *categoryName;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.backgroundColor = [UIColor whiteColor];
//        _grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 6)];
//        _grayView.backgroundColor = RGBACOLOR(239, 239, 239, 1.0);
//        _grayView.hidden = YES;
//        [self addSubview:_grayView];
        
//        _titleName = [[UILabel alloc] initWithFrame:CGRectMake(8, 2, 95, 21)];
//        _titleName.textColor =  [UIColor blackColor];
//        _titleName.font = [UIFont systemFontOfSize:14.0];
//        
//        [self addSubview:_titleName];
        _imageCommic = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, (ScreenSizeWidth - 30)/3, 125*__Scale6)];
        _imageCommic.layer.cornerRadius = 3.0;
        _imageCommic.layer.masksToBounds = YES;
        [self addSubview:_imageCommic];
        
        _commicTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageCommic.frame) + 15, 15, 183, 21)];
        _commicTitle.font = [UIFont systemFontOfSize:14.0];
        [self addSubview:_commicTitle];
        
        UILabel *auther = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageCommic.frame)+15, CGRectGetMaxY(_commicTitle.frame)+ 1, 40, 20)];
        auther.text = @"作者";
        auther.font = [UIFont systemFontOfSize:14.0];
        
        [self addSubview:auther];
        
        
        _autherName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(auther.frame), CGRectGetMaxY(_commicTitle.frame)+ 1, 100, 20)];
        _autherName.font = [UIFont systemFontOfSize:14.0];
        [self addSubview:_autherName];
        
        UILabel *category = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageCommic.frame)+15, CGRectGetMaxY(_autherName.frame)+ 1, 40, 20)];
        category.text = @"类型";
        category.font = [UIFont systemFontOfSize:14.0];
        [self addSubview:category];
        
        categoryName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(category.frame) + 10, CGRectGetMaxY(_autherName.frame) + 1, 150, 20)];
        categoryName.font = [UIFont systemFontOfSize:14.0];
        [self addSubview:categoryName];
        
        
        
        _commicDes = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageCommic.frame)+15, CGRectGetMaxY(category.frame)+ 1, ScreenSizeWidth - CGRectGetMaxX(_imageCommic.frame) - 20, 50)];
        _commicDes.numberOfLines = 4;
        _commicDes.font = [UIFont systemFontOfSize:12.0];
        _commicDes.textColor = RGBACOLOR(108, 108, 108,1.0);
        [self addSubview:_commicDes];
        
        _updateTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageCommic.frame)+15, CGRectGetMaxY(_imageCommic.frame) - 15, 203, 20)];
        _updateTime.font = [UIFont systemFontOfSize:12.0];
        _updateTime.textColor = RGBACOLOR(254, 77, 97, 1);
        [self addSubview:_updateTime];
        
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 160 * __Scale6- 4, ScreenSizeWidth, 4)];
        bottomView.backgroundColor = [UIColor colorWithRed:239/255.0 green:239/255.0 blue:239/255.0 alpha:1.0];
        [self addSubview:bottomView];
        
        
    }
    return self;
}

- (void)setCartoonModel:(CartoonInfoAll *)cartoonModel {
    
    _grayView.hidden = NO;
    _commicTitle.text  = cartoonModel.name;
    //    _titleName.text = bigRecommModel.title;
    [_imageCommic setImageWithURL:[NSURL URLWithString:cartoonModel.images]];
    _autherName.text = cartoonModel.author;
    _commicDes.text = cartoonModel.introduction;
    _updateTime.text = cartoonModel.updateInfo;
    
}

- (void)setBigRecommModel:(bigRecommentModel *)bigRecommModel {
    //    _imageCommic.frame = CGRectMake(_imageCommic.frame.origin.x, _imageCommic.frame.origin.y, _imageCommic.frame.size.width * __Scale6, _imageCommic.frame.size.height );
    _commicTitle.text  = bigRecommModel.cartoonInfoAll.name;
    _titleName.text = bigRecommModel.title;
    [_imageCommic setImageWithURL:[NSURL URLWithString:bigRecommModel.cartoonInfoAll.images]];
    _autherName.text = bigRecommModel.cartoonInfoAll.author;
    _commicDes.text = bigRecommModel.cartoonInfoAll.introduction;
    _updateTime.text = bigRecommModel.cartoonInfoAll.updateInfo;
    categoryName.text = bigRecommModel.cartoonInfoAll.categoryLabel;
}


- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
