//
//  RecommentCollectionViewCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/3/2.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RecommentCollectionModel;

@interface RecommentCollectionViewCell : UICollectionViewCell

@property (nonatomic, copy)RecommentCollectionModel *model;

@property (nonatomic, copy)NSDictionary *dict;

@end
