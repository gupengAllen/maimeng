//
//  RecommentHeadView.m
//  GameSM
//
//  Created by mac on 15/9/22.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "RecommentHeadView.h"
#import "Config.h"
#import "UIImageView+AFNetworking.h"
#import "Y_X_DataInterface.h"
#import "InfoDetailViewController.h"
#import "GiftDetailController.h"
#import "PicDetailViewController.h"
@interface RecommentHeadView(){
    NSTimer *timer;
    NSMutableArray *_pictureId;
    NSString *_adImageId;
    UIScrollView *_scrollView;
    NSString *_str;
    NSMutableArray *_typeId;
    int _pageNum;
}
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) CustomPageControl *pageControl;//自定义了一个分页视图。和分页控件有点关系。
@property (nonatomic, strong) UILabel *bottomLabel;

@end

@implementation RecommentHeadView


//关闭计时器
- (void)dealloc {
    [self stopTiming];
}

//通过代码创建的
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _pageNum = 0;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

//重新建立视图
- (void)resetView {
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }//移除视图
    
    [self stopTiming];//关闭计时器
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height)];
    [self addSubview:self.scrollView];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.delegate = self;
    self.scrollView.scrollsToTop = NO;//是否有回弹效果
    _scrollView = self.scrollView;
    for (int i = 0 ; i < self.infoData.count; i++) {
        
#warning 疑问1......
        
        //1.创建UIimageView，放置图片和添加手势
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(i*self.scrollView.bounds.size.width, 0, self.scrollView.bounds.size.width, self.scrollView.bounds.size.height)];
        imageView.tag = i;
//        _t = imageView.tag;
        [imageView setImageWithURL:[NSURL URLWithString:[[self.infoData objectAtIndex:i] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
        //1.这句代码的作用。扩展(category) UIImageView, 这样写出的代码更整洁，
        //        2 GCD 异步下载
        //        3 重用 UITableViewCell 加异步下载会出现图片错位，所以每次 cell 渲染时都要预设一个图片 (placeholder)，
        [self.scrollView addSubview:imageView];
        imageView.userInteractionEnabled = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;//图片模式，自适应。
        imageView.clipsToBounds = YES;
        
        //2.加载一个手势，添加到imageView上面
        UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchInImage1)];
        [imageView addGestureRecognizer:recognizer];
    }
    //3.创建一个放置pagecontrol的父视图和title的父视图
    UIImageView *bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 36, self.bounds.size.width, 36)];
    bottomView.image = [[UIImage imageNamed:@"main_1"] stretchableImageWithLeftCapWidth:10 topCapHeight:10];//设置拉伸点
    [self addSubview:bottomView];
    
    //4.创建一个textlable
    self.bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(7, 12, self.bounds.size.width - 80, 24)];
    self.bottomLabel.font = [UIFont boldSystemFontOfSize:14];
    self.bottomLabel.textColor = [UIColor whiteColor];
    self.bottomLabel.backgroundColor = [UIColor clearColor];
    [bottomView addSubview:self.bottomLabel];
    
    //5.在这里判断，数组是空的就返回，不是空的设置滑动范围
    if (self.infoData.count) {
        self.bottomLabel.text = [self.infoData.firstObject objectForKey:@"title"];
        
        [self.scrollView setContentSize:CGSizeMake(self.scrollView.bounds.size.width * self.infoData.count, 0)];
        
        //6.在set初始化pagecontrol中，传了frame，初始化图片，当前图片，总页。
        self.pageControl = [[CustomPageControl alloc] initWithFrame:CGRectMake(self.bounds.size.width - (11 * self.infoData.count), 12, (11 * self.infoData.count), 24) withNormalImage:[UIImage imageNamed:@"page.png"] currentImage:[UIImage imageNamed:@"page_a.png"] pageCount:(int)self.infoData.count];
        [self startTiming];
        [self.pageControl resetCurrentPageIndex:_pageNum];
        [self.scrollView setContentOffset:CGPointMake(_pageNum * self.scrollView.bounds.size.width, 0) animated:NO];
//        self.pageControl.currentIndex = _pageNum;
        [bottomView addSubview:self.pageControl];
    }
}

-(void)touchInImage1{
    int witchOne = _scrollView.contentOffset.x/self.frame.size.width;
    
    NSInteger type = [_typeId[witchOne] integerValue];
    NSString *value = _pictureId[witchOne];
    
    LYTabBarController *mmdrawControl = (LYTabBarController *)[self.window rootViewController];
    UINavigationController *nav = [mmdrawControl.viewControllers objectAtIndex:mmdrawControl.selectedIndex];
    UIViewController *currentViewControl = nav.topViewController;
    
    switch (type) {
        case 1: {
            [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"manhuago"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
//            [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"md" to_section:@"m" to_step:@"d" type:@"c" channel:@"b" id:value];
            nav.navigationBar.hidden = NO;
            InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
            detailView.infoID = value;
            detailView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            [nav pushViewController:detailView animated:YES];
            
            
        }
            break;
            //        case 2: {
            //            PicDetailViewController *picDetailView = [[PicDetailViewController alloc] initWithNibName:@"PicDetailViewController" bundle:nil];
            //            picDetailView.imageID = value;
            //            [currentViewControl.navigationController pushViewController:picDetailView animated:YES];
            //        }
            //            break;
            //        case 3: {
            //            GiftDetailController *detailView = [[GiftDetailController alloc] initWithNibName:@"GiftDetailController" bundle:nil];
            //            detailView.giftID = value;
            //            [currentViewControl.navigationController pushViewController:detailView animated:YES];
            //        }
            //            break;
        case 5:{
//            [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"c" channel:@"" id:_pictureId[witchOne]];
            
            _str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_pictureId[witchOne]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"adImageTap" object:_str];
        }
            break;
            //        case 11: case 12:{
            //            [mmdrawControl.tabBarView tabWasSelected:(UIButton*)[mmdrawControl.tabBarView viewWithTag:100]];
            //            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",type], @"type", nil];
            //            [[NSNotificationCenter defaultCenter] postNotificationName:HomeNotification object:dic];
            //        }
            //            break;
            //        case 13: {
            //            [mmdrawControl.tabBarView tabWasSelected:(UIButton*)[mmdrawControl.tabBarView viewWithTag:101]];
            //        }
            //            break;
            //        case 20: {
            //            WebViewController *webView = [[WebViewController alloc] initWithNibName:@"WebViewController" bundle:nil];
            //            webView.url = value;
            //            [currentViewControl.navigationController pushViewController:webView animated:YES];
            //        }
            //            break;
            //
        case 21: {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:value]];
        }
            break;
        case 22: {
            [[NSNotificationCenter defaultCenter] postNotificationName:RefreshNotification object:nil];
        }
            break;
        default:
            break;
    }
}


- (void)setInfoData:(NSMutableArray *)infoData {
    _pictureId = [NSMutableArray arrayWithCapacity:0];
    _typeId = [NSMutableArray arrayWithCapacity:0];
    _infoData = infoData;
    for (NSDictionary *dict in _infoData) {
        [_pictureId addObject:dict[@"customValue"]];
        [_typeId addObject:dict[@"customType"]];
    }    /*
     {
     adImageLogCount = 0;
     adminID = 10;
     beginTime = "2015-10-20 00:00:00";
     clientType = "3,4";
     content = "";
     createTime = "2015-10-20 12:14:37";
     customPosition = 2;
     customType = 5;
     customValue = 18;
     endTime = "2015-10-27 00:00:00";
     id = 84;
     images = "http://7xkbpd.com2.z0.glb.qiniucdn.com/6d46285644c6d9b1a6b30c2d7c6d59dd_60103.png";
     imagesValue = "http://7xkbpd.com2.z0.glb.qiniucdn.com/6d46285644c6d9b1a6b30c2d7c6d59dd_60103.png?imageView2/1/q/50/w/120/";
     modifyTime = "";
     shortNum = 7;
     status = 1;
     title = "\U5c0f\U6843\U5c0f\U6817 Love Love\U7269\U8a9e";
     }
     */
    [self resetView];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    int index = (int)(scrollView.contentOffset.x / scrollView.bounds.size.width);
    //    self.pageControl.currentPage = index;
    [self.pageControl resetCurrentPageIndex:index];
    self.bottomLabel.text = [[self.infoData objectAtIndex:self.pageControl.currentIndex] objectForKey:@"title"];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self stopTiming];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self startTiming];
}

//手势的方法，手势跟自定义的pagecoctroll同步。
- (void)touchInImage:(UITapGestureRecognizer*)recognizer {
    if (_delegate && [_delegate respondsToSelector:@selector(customAdViewSelectWithIndex:)]) {
        [_delegate customAdViewSelectWithIndex:self.pageControl.currentIndex];
    }
}

//监听滚动视图，是否超过了数组的范围，在范围之内++，否则为0.
- (void)scrollToNext {
    int index = self.pageControl.currentIndex;
    
    if (index < self.infoData.count - 1) {
        index++;
    } else {
        index = 0;
    }
    //为0没有动画，否则有动画。
    if (index) {
        [self.scrollView setContentOffset:CGPointMake(index * self.scrollView.bounds.size.width, 0) animated:YES];
    } else
        [self.scrollView setContentOffset:CGPointMake(index * self.scrollView.bounds.size.width, 0) animated:NO];
    _pageNum = index;
}

#warning 关键---自定义pagecotroll的关键。
//控制同步的方法2种，在set方法中开启计时器。一定时器调用自定义的pagecotroll视图（类似于pagecotroll控制器），使红色点不断到当前页。且这个定时器方法还不断调用当前页面从新加载视图，之后有调用了协议方法。
//2种，手动滑动，开始关掉计时器，结束手动滑动开启定时器，和上面的一样。


#pragma mark - 计时
- (void)startTiming {
    [self stopTiming];
    timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(scrollToNext) userInfo:nil repeats:YES];
}

- (void)stopTiming {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}
@end
