//
//  bigRecommentModel.m
//  GameSM
//
//  Created by 顾鹏 on 15/12/16.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "bigRecommentModel.h"
#import "CartoonInfoAll.h"
#import "DataBaseHelper.h"
#import "RecommentModel.h"
#import "DataBaseHelper.h"
#import "CartoonInfoAll.h"
@implementation bigRecommentModel
- (instancetype)init {
    if (self = [super init]) {
        _cartoonSetList = [NSMutableArray array];
    }
    return self;
}
//@property (nonatomic, copy)NSString *type;
//@property (nonatomic, copy)NSString *valueType2;
//@property (nonatomic, copy)NSString *valueID2;
//@property (nonatomic, copy)NSString *images2;
//@property (nonatomic, copy)NSString *url2;

+ (NSMutableArray *)paraseArr:(NSDictionary *)allDict {
    NSArray *allArr = allDict[@"results"];
    NSMutableArray *lastResultArr = [NSMutableArray array];
    for (NSDictionary *recommentDict in allArr) {
        bigRecommentModel *bigModel = [[bigRecommentModel alloc] init];
        bigModel.id = recommentDict[@"id"];
        bigModel.title = recommentDict[@"title"];
        bigModel.images = recommentDict[@"images"];
        bigModel.valueType = recommentDict[@"valueType"];
        bigModel.valueID = recommentDict[@"valueID"];
        bigModel.url = recommentDict[@"url"];
        bigModel.priority = recommentDict[@"priority"];
        bigModel.status = recommentDict[@"status"];
        bigModel.desc = recommentDict[@"desc"];
        bigModel.createTime = recommentDict[@"createTime"];
        bigModel.modifyTime = recommentDict[@"modifyTime"];
        bigModel.userID = recommentDict[@"userID"];
        bigModel.type = recommentDict[@"type"];
        bigModel.valueType2 = recommentDict[@"valueType2"];
        bigModel.valueID2 = recommentDict[@"valueID2"];
        bigModel.images2 = recommentDict[@"images2"];
        bigModel.url2 = recommentDict[@"url2"];
        bigModel.cartoonID = recommentDict[@"cartoonID"];
        if (recommentDict[@"cartoonIDInfo"]) {
            bigModel.cartoonInfoAll = [CartoonInfoAll paraCartoonInfoAllSingle:recommentDict[@"cartoonIDInfo"]];
        }
        
        for (NSDictionary *cartoonSetListDict in recommentDict[@"cartoonSetList"]) {
            [bigModel.cartoonSetList addObject:[CartoonInfoAll paraCartoonInfoAllSingle:cartoonSetListDict]];
//            RecommentModel *model = [[RecommentModel alloc]init];
//            [model setValuesForKeysWithDictionary:cartoonSetListDict];
//            [bigModel.cartoonSetList addObject:model];
//            if ([bigModel.id isEqualToString:@"1"]) {
            
            
//            CartoonInfoAll *cartoonInfo = [[CartoonInfoAll alloc] init];
//            cartoonInfo.id = model.id;
//            cartoonInfo.name = model.name;
//            cartoonInfo.images = model.images;
//            cartoonInfo.author = model.author;
//            cartoonInfo.introduction = model.introduction;
//            cartoonInfo.priority = model.priority;
//            cartoonInfo.updateInfo = model.updateInfo;
//                cartoonInfo.isRead = @"1";
//                cartoonInfo.ReadChapterId = model.chapterId;
//                cartoonInfo.readAlbumId = model.albumId;
//                
//                [[DataBaseHelper shared] insertCartoonInfo:cartoonInfo];
//            }
        }
        [lastResultArr addObject:bigModel];
    }
    NSMutableArray *cartoonCartoonArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"isRead":@"1"} andChooseType:@"lastReadTime"];
    NSRange range;
    range.location = 0;
    range.length = 3;
    if (cartoonCartoonArr.count > 0) {
        bigRecommentModel *bigModel = [[bigRecommentModel alloc] init];
        bigModel.id = @"1";
        bigModel.type = @"0";
        bigModel.title = @"继续撸";
        [bigModel.cartoonSetList addObjectsFromArray: cartoonCartoonArr.count > 3?[cartoonCartoonArr subarrayWithRange:range]:cartoonCartoonArr];
        [lastResultArr replaceObjectAtIndex:0 withObject:bigModel];
        
    }else{
        [lastResultArr removeObjectAtIndex:0];
    }
    
    return lastResultArr;
}
@end
