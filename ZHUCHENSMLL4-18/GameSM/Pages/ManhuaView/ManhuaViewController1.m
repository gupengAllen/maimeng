//
//  ManhuaViewController.m
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "ManhuaViewController.h"
#import "XLScrollViewer.h"
#import "MyDataService.h"
#import "AppDelegate.h"

#import "testViewController.h"
#import "CategoryDetailsController.h"
#import "SearchHeaderCollecationView.h"
#import "LikereadConllectionView.h"

#import "RecommentTableView.h"
#import "SearchTavleView.h"
#import "TOPtableView.h"
#import "CategoryCollectionView.h"
#import "CategoryDetailsModel.h"
#import "DetailsModel.h"
#import "BookTableView.h"
#import "customImage.h"
#import "ReadController.h"
#import "RecommentCell.h"
#import "RecommentModel.h"
#import "KeychainItemWrapper.h"
#import "SecondReadController.h"
#import "InfoDetailViewController.h"
#import "DataBaseHelper.h"
#import "SaveWatchDownViewController.h"
#import "CartoonInfoAll.h"
#import "CustomTool.h"
#import "NSString+MD5.h"

#define Dailystring @"http://app.qdaily.com/app/homes/index/0.json?"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface ManhuaViewController ()<UIAlertViewDelegate>
{
    NSMutableArray *_dataArr;
    NSMutableArray *_searchArr;
    NSMutableArray *_keywordArr;
    UIScrollView *scr ;
    UITextField *textview;
    
    SaveWatchDownViewController *_SWDVC;
    
    BOOL _isRefresh1;
    BOOL _isRefresh2;
    BOOL _isRefresh3;
    BOOL _isRefresh4;
    BOOL _isRefresh0;
    
    UIImageView *_netImageView;
    UIButton    *_refreshBtn0;
    UIButton    *_refreshBtn1;
    UIButton    *_refreshBtn2;
    UIButton    *_refreshBtn3;
    UIButton    *_refreshBtn4;
    
     NSString    *_documentDiretory;
    NSArray *strAllInfo;
}
@end

@implementation ManhuaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"] length] == 0) {
        [[NSUserDefaults standardUserDefaults] setObject:[self getUUID] forKey:@"UUID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    _documentDiretory = [paths objectAtIndex:0];
    self.navigationController.navigationBar.hidden = YES;
    _SWDVC = [[SaveWatchDownViewController alloc] init];
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    _keywordArr = [NSMutableArray arrayWithArray:0];
    self.view.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
    _isRefresh0 = NO;
    _isRefresh1 = YES;
    _isRefresh2 = YES;
    _isRefresh3 = YES;
    _isRefresh4 = YES;
//    self.view.backgroundColor = [UIColor redColor];
    // 1.创建子视图
    [self creatView];
    
    // 2.调用通知
    [self notification];
    [self receiveNoti];
    
    // 3.加载数据
    [self refreshRecomment];
    //4.加载推荐页数据
//    [self initRecommentData2];
//    [self initRecommentData3];
    textview =[[UITextField alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    textview.hidden = YES;
    [self.view addSubview:textview];
    
//    [[self getUUID] ]
#warning 加载超时视图
    _netImageView = [CustomTool createImageView];
    [self.view addSubview:_netImageView];
    scr = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 35, KScreenWidth, KScreenheight - 49-35-64)];
    
}

#pragma mark- 初始化创建子视图
-(void)receiveNoti{
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(YXGetUrl:) name:@"continueLOL" object:nil];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"MANHUACONTINUE" object:str];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpCateContinueDetails:) name:@"MANHUACONTINUE" object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshRecomment) name:@"refreshRecommentTableView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeScr:) name:@"removeScr" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpCategoryDetails:) name:@"categoryDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpCategoryDetails:) name:@"nearread" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpCategoryDetails:) name:@"newnew" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpCategoryDetails:) name:@"LuLu" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpTopDetails:) name:@"topDetails" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpRecomment:) name:@"recommentDetails" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpRecomment:) name:@"recommentDetail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"likeRead:" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fugai:) name:@"searchKeyword" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fugai:) name:@"searchContent" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpCategoryDetails:) name:@"MoreC" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recomment-1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recomment-2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recomment-3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpRead:) name:@"recommentLu-1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpRead:) name:@"recommentLu-2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpRead:) name:@"recommentLu-3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentLu--1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentLu--2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentLu--3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentTop-1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentTop-2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentTop-3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentTop--1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentTop--2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentTop--3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentCollection-1" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentCollection-2" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"recommentCollection-3" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fugai:) name:@"everybodywatch" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reuseData:) name:@"changeSave" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(jumpDetails:) name:@"adImageTap" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDownLoad:) name:@"UPDATEDOWNLOAD" object:nil];
    

}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MANHUACONTINUE" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"refreshRecommentTableView" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UPDATEDOWNLOAD" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"removeScr" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentDetails" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"categoryDetails" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LuLu" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"MoreC" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"topDetails" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"moreDetails" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentDetails" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentDetails1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentDetail" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"likeRead" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchKeyword" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchContent" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"newnew" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"nearread" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recomment-1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recomment-2" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recomment-3" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentLu-1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentLu-2" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentLu-3" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentTop-1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentTop-2" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentTop-3" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentLu--1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentLu--2" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentLu--3" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentTop--1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentTop--2" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentTop--3" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentCollection-1" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentCollection-2" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"recommentCollection-3" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"changeSave" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"adImageTap" object:nil];
    
}

#pragma mark-
- (void)removeScr:(NSNotification *)noti {
    [scr removeFromSuperview];
//    scr = nil;//设置nil
}

- (void)fugai:(NSNotification *)noti{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    for (UIView *subview in scr.subviews) {
        [subview removeFromSuperview];
    }
    
    NSString *str = noti.object;
    NSArray *array = [str componentsSeparatedByString:@"searchName="];
    NSString *keyWordsStr = [array lastObject];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if ([backData[@"results"] isKindOfClass:[NSArray class]]) {
            _searchArr = [[NSMutableArray alloc] init];
            
            scr.backgroundColor = [UIColor whiteColor];
            [_searchTV addSubview:scr];
            NSArray *arr = backData[@"results"];
                UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, 30)];
            if ([backData[@"extraInfo"][@"otherType"]integerValue] == 2) {
                
                nameLabel.text = [NSString stringWithFormat: @"  没有搜索到关于'%@'的漫画，小编为你推荐以下漫画",[keyWordsStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]];
                }else{
                    
                    nameLabel.text = [NSString stringWithFormat:@"  搜索到%ld条关于'%@'的漫画",arr.count,[keyWordsStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding ]];
                }
                nameLabel.font = [UIFont systemFontOfSize:12.0];
                [scr addSubview:nameLabel];
            
            
            for(int i = 0;i < arr.count;i ++){
                CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
                [md setValuesForKeysWithDictionary:arr[i]];
                customImage *cusIm = [[customImage alloc] initWithFrame:CGRectMake(KScreenWidth/3*(i%3), 35 + ((KScreenWidth - 24)/3*___Scale+4+20) * (i/3), KScreenWidth/3, (KScreenWidth - 24)/3*___Scale+4+20)];
                cusIm.categoryModel = md;
                cusIm.tag = 1000 + i;
                [scr addSubview:cusIm];
                [_searchArr addObject:md];
                
                UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap:)];
                [cusIm addGestureRecognizer:tapGes];
            }
            scr.contentSize = CGSizeMake(0, (arr.count/3+1) * ((KScreenWidth - 24)/3*___Scale+4+20)+49);
        }else{
            DetailsModel *md = [[DetailsModel alloc]init];
            [md setValuesForKeysWithDictionary:backData[@"results"]];
            //            if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]) {
            
            testViewController *testVC = [[testViewController alloc] init];
            [testVC setChangeCollection:^{
                
            }];
            testVC.detailModel = md;
            //            UINavigationController *nvc = [[UINavigationController alloc]initWithRootViewController:testVC];
            //    [self.navigationController pushViewController:MMDetailVC animated:YES];
            [self presentViewController:testVC animated:YES completion:nil];
            //            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(KScreenWidth/2-45, KScreenheight-80-64, 90, 40)];
        label.backgroundColor = [UIColor grayColor];
        label.layer.masksToBounds = YES;
        label.layer.cornerRadius = 6;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
        label.alpha = 1;
        label.text = @"木有网啊";
        label.textAlignment = 1;
        [self.view addSubview:label];
        [UIView animateWithDuration:1 animations:^{
            label.alpha = 0;
        }];
        label = nil;
    }];
    
}


- (NSString *)replaceUnicode:(NSString *)unicodeStr {
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u" withString:@"\\U"];
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSString *tempStr3 = [[@"\"" stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
    NSString* returnStr = [NSPropertyListSerialization propertyListFromData:tempData
                                                           mutabilityOption:NSPropertyListImmutable
                                                                     format:NULL
                                                           errorDescription:NULL];
    
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n" withString:@"\n"];
}

- (void)tap:(UITapGestureRecognizer *)tap {
//    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]) {
    
    [[LogHelper shared] writeToFilefrom_page:@"csl" from_section:@"c" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"recommand" id: [_searchArr[tap.view.tag - 1000] id]];
    testViewController *testVC = [[testViewController alloc] init];
    [testVC setChangeCollection:^{
        
    }];
    testVC.detailModel = _searchArr[tap.view.tag - 1000];
//    UINavigationController *nvc = [[UINavigationController alloc]initWithRootViewController:testVC];
        [self presentViewController:testVC animated:YES completion:nil];
//}
}


-(void)jumpSearchDetails:(NSNotification *)noti{
    _modell = noti.object;
    
}

-(void)jumpLikeRead:(NSNotification *)noti{
    
}
-(void)jumpRecomment:(NSNotification *)noti{
//    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]) {

    testViewController *vc = [[testViewController alloc]init];
    [vc setChangeCollection:^{
        
    }];

    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:vc animated:YES completion:^{
        _modell = noti.object;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"detail" object:_modell];
    }];
//}
}

//-(void)jumpRecomment1:(NSNotification *)noti{
//    ReadController *vc = [[ReadController alloc]init];
//    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
//    [self presentViewController:nac animated:YES completion:^{
//        _modell = noti.object;
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"detail" object:_modell];
//    }];
//}

-(void)jumpTopDetails:(NSNotification *)noti{
    
    CategoryDetailsController *vc = [[CategoryDetailsController alloc]init];
    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
    vc.titleName = noti.object[1];
    vc.type = @"top";
    [self presentViewController:vc animated:YES completion:^{
        _modell = noti.object[0];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"topDetail" object:_modell];
    }];
}

- (void)jumpDiffenert:(NSNotification *)noti{
    if ([noti.object[0] integerValue] == 1) {
        InfoDetailViewController *detailView = [[InfoDetailViewController alloc] initWithNibName:@"InfoDetailViewController" bundle:nil];
        detailView.infoID = noti.object[2];
        detailView.titleName = noti.object[3];
        detailView.hidesBottomBarWhenPushed = NO;
        [self.navigationController pushViewController:detailView animated:YES];
    }else{
//        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]) {

    testViewController *vc = [[testViewController alloc]init];
        [vc setChangeCollection:^{
            
        }];

    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    vc.detailStr = noti.object[1];
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
    
        [self presentViewController:vc animated:YES completion:nil];}
//    }
}

-(void)jumpDetails:(NSNotification *)noti{
    NSLog(@"jumpDetail失败");
        testViewController *vc = [[testViewController alloc]init];
        vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
        vc.detailStr = noti.object;
        [vc setChangeCollection:^{
        }];
    
        [self presentViewController:vc animated:YES completion:nil];

}

-(void)jumpRead:(NSNotification *)noti{
    strAllInfo = noti.object;
    NSArray *urlArr = [strAllInfo[1] componentsSeparatedByString:@"&"];
    NSString *chapterId = [urlArr[1] substringFromIndex:10];
    
//判断是否从漫画继续撸直接进入漫画阅读页
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"CONTINUEREADINRECOMMENT"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"0"]&&![[DataBaseHelper shared] fetchpartIsSaveExistchapterId:chapterId]) {
        NSLog(@"+++++%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"]);
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"OPENTURN"] isEqualToString:@"0"]) {
            UIAlertView *wlanAlert = [[UIAlertView alloc] initWithTitle:@"提醒" message:@"当前为非WIFI网络，继续阅读将消耗流量。是否继续阅读" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"继续阅读", nil];
            wlanAlert.tag = 10000;
            [wlanAlert show];
            return;
        }
    }
    SecondReadController *readC = [[SecondReadController alloc]init];
    readC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    [readC setChangeMark:^(NSInteger tagMark) {
        
    }];
    readC.charpList = @[];
    readC.allInfo = strAllInfo[0];
    [self presentViewController:readC animated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"readDetails" object:strAllInfo[1]];
    }];

}


- (void)jumpCateContinueDetails:(NSNotification *)noti{
    _SWDVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    _SWDVC.moveType = 0;
    [self presentViewController:_SWDVC animated:YES completion:nil ];
}

-(void)jumpCategoryDetails:(NSNotification *)noti{
    CategoryDetailsController *vc = [[CategoryDetailsController alloc]init];
    vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    vc.titleName = noti.object[1];
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
    [self presentViewController:vc animated:YES completion:^{
        _modell = noti.object[0];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"categoryDetail" object:_modell];
    }];
}
//-(void)jumpMoreDetails{
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:[[moreController alloc] init]];
//    [self presentViewController:nac animated:YES completion:nil];
//}

- (void)notification{
    //接收通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestDoubleData:) name:kchangethumeNameCurrepagr object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (NSString *)getUUID
{
    KeychainItemWrapper *keychainItem = [[KeychainItemWrapper alloc]
                                         
                                         initWithIdentifier:@"UUID"
                                         
                                         accessGroup:@"YOUR_BUNDLE_SEED.com.yourcompany.userinfo"];
    
    
    NSString *strUUID = [keychainItem objectForKey:(id)CFBridgingRelease(kSecValueData)];
    
    //首次执行该方法时，uuid为空
    if ([strUUID isEqualToString:@""])
        
    {
        CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
        
        strUUID = (NSString *)CFBridgingRelease(CFUUIDCreateString (kCFAllocatorDefault,uuidRef));
        
        [keychainItem setObject:strUUID forKey:(id)CFBridgingRelease(kSecValueData)];
        
    }
    return strUUID;
}

#pragma mark- 创建子视图
- (void)creatView{
    
    //1.创建滚动视图
    
    CGRect ScrollFrame = CGRectMake(0,0, KScreenWidth,KScreenheight);
    NSMutableArray *views = [NSMutableArray array];
    NSArray *names = @[@"推荐",@"搜索",@"榜单",@"类别",@"书架"];
    NSMutableArray *titleNames = [[NSMutableArray alloc]initWithArray:names];
    _tableViewDics = [NSMutableDictionary dictionary];
    
    
    for (int i=0; i<names.count; i++) {
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.backgroundColor = [UIColor whiteColor];
        view.tag = i;
        [views addObject:view];
        
        CGRect frame = CGRectMake(0, 0, KScreenWidth,KScreenheight);
        if (i ==0) {
            _recommentTV = [[RecommentTableView alloc]initWithFrame:frame style:UITableViewStylePlain];
            _recommentTV.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
            _recommentTV.tag = i;
             [view addSubview:_recommentTV];
            [_tableViewDics setObject:_recommentTV forKey:@"推荐"];
        }else if(i ==1){
            _searchTV = [[SearchTavleView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight )];
            _searchTV.tag = i;
            _searchTV.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
             [view addSubview:_searchTV];
            [_tableViewDics setObject:_searchTV forKey:@"搜索"];
        }else if(i ==2){
            _TOPTV = [[TOPtableView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight - 96) style:UITableViewStylePlain];
            _TOPTV.tag = i;
            _searchTV.backgroundColor = [UIColor colorWithRed:235/255.0 green:235/255.0 blue:235/255.0 alpha:1];
             [view addSubview:_TOPTV];
            [_tableViewDics setObject:_TOPTV forKey:@"榜单"];
        }else if(i ==3){
            _categoryCV = [[CategoryCollectionView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight - 20)];
            _categoryCV.tag = i;
            _categoryCV.backgroundColor = [UIColor whiteColor];
             [view addSubview:_categoryCV];
            [_tableViewDics setObject:_categoryCV forKey:@"类别"];
        }else if(i ==4){
//            if (g_App.userInfo.userID) {
                _bookTV = [[BookTableView alloc]initWithFrame:frame style:UITableViewStylePlain];
                _bookTV.tag = i;
                _bookTV.backgroundColor = [UIColor whiteColor];
                [view addSubview:_bookTV];
                [_tableViewDics setObject:_bookTV forKey:@"书架"];
            
//            }
        }
    }
    
    UIImage *Bgimage = [UIImage imageNamed:@"topbg.png"];
    NSArray *choose = @[@NO,@YES,@NO];
    
    XLScrollViewer *Scroll = [XLScrollViewer scrollWithFrame:ScrollFrame withViews:views withButtonNames:titleNames withThreeAnimation:choose];
    Scroll.xl_topWidth = KScreenWidth;
    Scroll.xl_topHeight = 64;
    Scroll.xl_buttonFont = 15;
    Scroll.xl_topBackColor = [UIColor clearColor];
    Scroll.xl_sliderColor = [UIColor colorWithRed:218/255.0 green:56/255.0 blue:79/255.0 alpha:1];
    Scroll.xl_buttonToSlider  = 7;
    Scroll.xl_sliderHeight = 8;
    Scroll.xl_isSliderCorner = NO;
    Scroll.xl_buttonColorSelected = [UIColor colorWithRed:219/255.0 green:42/255.0 blue:68/255.0 alpha:1];
    Scroll.xl_buttonColorNormal = [UIColor grayColor];
    Scroll.xl_topBackColor = [UIColor whiteColor];
    Scroll.xl_topBackImage = Bgimage;
    Scroll.xl_isMoveSlider = YES;
    Scroll.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:Scroll];
    
    for (int i = 0; i < 4; i ++) {
        UIImageView *iv = [[UIImageView alloc]init];
        iv.frame = CGRectMake( ( 55 + i * (self.view.bounds.size.width)/5.5) * _Scale, 38, 8, 8);
        iv.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"dianzan_2"]];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:iv];
    }
    UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(0, 63, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
}

#pragma mark- 数据请求
//-(void)loadTopData{
//    NSMutableDictionary *topData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                    (NSString *)API_URL_TOP_LIST,@"r",
//                                    @"1",@"page",
//                                    @"3",@"size", nil];
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:topData object:self action:@selector(addTopData:) method:GETDATA];
//}
//-(void)addTopData:(NSDictionary *)dict{
//    if (![[dict objectForKey:@"code"] integerValue]) {
////        _topDataArr = [dict objectForKey:@"results"];
////        _topDataCount = [dict objectForKey:@"extraInfo"];
////        _countNum = [_topDataCount[@"countTotal"] intValue];
//    }
//}



#pragma mark- 调用通知重新加载数据，和请求数据
- (void)requestDoubleData:(NSNotification *)notification{
    NSArray *thuemearrays =  notification.userInfo[@"name"];
    NSInteger currentPage = [notification.userInfo[@"page"] integerValue];
    NSString *thuemeName = thuemearrays[currentPage] ;
//    _tableView.tag = currentPage;
    id tableView = [_tableViewDics objectForKey:thuemeName];
    
    if (currentPage == 0) {
        if (_isRefresh0) {
            [self refreshRecomment];
            _isRefresh0 = NO;
        }
    }else if (currentPage == 1){
        if (_isRefresh1) {
            [self downloadSearchData];
            _isRefresh1 = NO;
        }
    }else if (currentPage == 2){
        if (_isRefresh2) {
            [self downloadTopData];
            _isRefresh2 = NO;
        }
    }else if (currentPage == 3){
        if (_isRefresh3) {
            [self downloadCategoryData];
            _isRefresh3 = NO;
        }
    }else if (currentPage == 4){
        if (_isRefresh4) {
            [self downloadBookData];
            _isRefresh4 = NO;
        }
        
    }
}

- (void)addLoadData:(NSDictionary *)dict{
    if (![[dict objectForKey:@"code"] integerValue]) {
        self.recommentTV.recommentHeardView.infoData = [dict objectForKey:@"results"];
    }
}

- (void)reuseData:(NSNotification *)noti {
    
//    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }
    
//    headers = @{@"clientid":[self };
//    NSMutableDictionary *recommentNewData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                             (NSString *)API_URL_RECOMMENTALL_LIST,@"r",
//                                             @"1",@"page",
//                                             @"999",@"size", nil];
//    
//    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:recommentNewData object:self action:@selector(addRecommentData:) method:GETDATA];
//
//    

//    NSMutableDictionary *bookData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                     (NSString *)API_URL_CARTOONUSERREADHISTORY_DETAIL,@"r",
//                                     @"1",@"page",
//                                     @"999",@"size", nil];
    //    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) method:GETDATA];
//    [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) header:headers];
    
//    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:bookData object:self action:@selector(addBookData:) method:GETDATA];
//    [self addBookDataLocal];
    
    //----BOOKCOLLECTION
//    NSMutableDictionary *bookCollectionData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                               (NSString *)API_URL_BOOKCOLLECTIONLIST_LIST,@"r",
//                                               @"1",@"page",
//                                               @"999",@"size",
//                                               nil];
    //    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) method:GETDATA];
//    [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) header:headers];
//    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) method:GETDATA];
//    [self addBookCollectionDataLocal];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HAVENREQUEST"] isEqualToString:@"1"]||g_App.userInfo.userID == nil) {
        [self addBookCollectionDataLocal];
        [self addBookDataLocal];
    }else if(g_App.userInfo.userID != nil){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"HAVENREQUEST"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSMutableDictionary *bookCollectionData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   (NSString *)API_URL_BOOKCOLLECTIONLIST_LIST,@"r",
                                                   @"1",@"page",
                                                   @"100",@"size",
                                                   @"0",@"withDevice",

                                                   nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) method:GETDATA];
        
        //        NSMutableDictionary *recommentLuData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
        //                                                (NSString *)API_URL_RECOMMENTLU_LIST,@"r",
        //                                                @"1",@"page",
        //                                                @"100",@"size", nil];
        //        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentLuData object:self action:@selector(addRecommentLuData:) method:GETDATA];
        NSMutableDictionary *bookData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         (NSString *)API_URL_CARTOONUSERREADHISTORY_DETAIL,@"r",
                                         @"1",@"page",
                                         @"100",@"size",
                                         @"0",@"withDevice"
                                         , nil];
        //    [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) header:headers];
        
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) method:GETDATA];
        
        
    }
    
    
    NSMutableDictionary *recommentNewData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                             (NSString *)API_URL_RECOMMENTALL_LIST,@"r",
                                             @"1",@"page",
                                             @"999",@"size", nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:recommentNewData object:self action:@selector(addRecommentData:) method:GETDATA];
    
    
}


- (void)adImageLogAddFinish:(NSDictionary *)dict{

}


#pragma mark - 数据请求
//搜索页
-(void)downloadSearchData{
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cs" to_page:@"" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:@""];
    
    //    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"SEARCHSWITCHDOWNLOADNEWDATA"] isEqualToString:@"0"]){
    //
    //    }
    NSString *searchFolder = [NSString stringWithFormat:@"%@/SearchFolder",_documentDiretory];
    NSString *plistPath = [searchFolder stringByAppendingPathComponent:@"searchLikeRead.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *searchData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           (NSString *)API_URL_SEARCH_LIST,@"r",
                                           @"1",@"page",
                                           @"6",@"size", nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:searchData object:self action:@selector(addSearchData:) method:GETDATA];
        NSMutableDictionary *searchListData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                               (NSString *)API_URL_EVERYBODYWATCHING_LIST,@"r",
                                               @"1",@"page",
                                               @"10",@"size", nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:searchListData object:self action:@selector(addSearchListData:) method:GETDATA];
    }else{
        self.searchTV.searchDataArr = nil;
        self.searchTV.searchHeaderView.searchHeaderVC.searchDataArr = nil;
        self.searchTV.searchFootArr = nil;
        self.searchTV.searchFooterView.likereadVC.searchDataListArr = nil;
        
        NSDate *now = [NSDate date];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
        NSString *nextUpdateTime = [userDefaults stringForKey:@"updateSearchDataTime"];
        NSDate *date1 = [formatter dateFromString:nextUpdateTime];
        NSTimeInterval time = [date1 timeIntervalSinceDate:now];
        if (time <= 0) {
            [userDefaults setObject:nil forKey:@"updateSearchDataTime"];
            NSDate *updataTime = [now dateByAddingTimeInterval:30*24*60*60 - 8*60*60];
            NSString *date = [formatter stringFromDate:updataTime];
            [userDefaults setObject:date forKey:@"updateSearchDataTime"];
            //            [userDefaults setObject:@"1" forKey:@"SEARCHSWITCHDOWNLOADNEWDATA"];
            [userDefaults synchronize];
        }
    }
}

//榜单
-(void)downloadTopData{
    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cb" to_page:@"" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:@""];
    
    
    //    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"TOPSWITCHDOWNLOADNEWDATA"] isEqualToString:@"0"]){
    //
    //
    //    }
    NSString *topFolder = [NSString stringWithFormat:@"%@/TOPFolder",_documentDiretory];
    NSString *plistPath = [topFolder stringByAppendingPathComponent:@"topID.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]){
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *topData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                        (NSString *)API_URL_TOP_LIST,@"r",
                                        @"1",@"page",
                                        @"5",@"size", nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:topData object:self action:@selector(addTopData:) method:GETDATA];
    }else{
        self.TOPTV.topDataArr = nil;
        
        NSDate *now = [NSDate date];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
        NSString *nextUpdateTime = [userDefaults stringForKey:@"updateTopDataTime"];
        NSDate *date1 = [formatter dateFromString:nextUpdateTime];
        NSTimeInterval time = [date1 timeIntervalSinceDate:now];
        if (time <= 0) {
            [userDefaults setObject:nil forKey:@"updateTopDataTime"];
            NSDate *updataTime = [now dateByAddingTimeInterval:30*24*60*60 - 8*60*60];
            NSString *date = [formatter stringFromDate:updataTime];
            [userDefaults setObject:date forKey:@"updateTopDataTime"];
            //            [userDefaults setObject:@"1" forKey:@"TOPSWITCHDOWNLOADNEWDATA"];
            [userDefaults synchronize];
        }
    }
}


//类!
-(void)downloadCategoryData{
    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ct" to_page:@"" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:@""];

    //    if(![[[NSUserDefaults standardUserDefaults] objectForKey:@"CATEGORYSWITCHDOWNLOADNEWDATA"] isEqualToString:@"0"]){
    //    }
    NSString *categoryFolder = [NSString stringWithFormat:@"%@/CategoryFolder",_documentDiretory];
    NSString *plistPath = [categoryFolder stringByAppendingPathComponent:@"categoryID.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSMutableDictionary *categoryData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                             (NSString *)API_URL_CATEGORY_LIST,@"r",
                                             @"1",@"page",
                                             @"24",@"size", nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:categoryData object:self action:@selector(addCategoryData:) method:GETDATA];
    }else{
        self.categoryCV.categoryDataArr = nil;
        NSDate *now = [NSDate date];
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
        NSString *nextUpdateTime = [userDefaults stringForKey:@"updateCategoryDataTime"];
        NSDate *date1 = [formatter dateFromString:nextUpdateTime];
        NSTimeInterval time = [date1 timeIntervalSinceDate:now];
        if (time <= 0) {
            [userDefaults setObject:nil forKey:@"updateCategoryDataTime"];
            NSDate *updataTime = [now dateByAddingTimeInterval:30*24*60*60 - 8*60*60];
            NSString *date = [formatter stringFromDate:updataTime];
            [userDefaults setObject:date forKey:@"updateCategoryDataTime"];
            [userDefaults synchronize];
        }
    }
}

//书架
-(void)downloadBookData{
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cf" to_page:@"" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:@""];
    
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }
    
//    NSDate *now = [NSDate date];
//    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
//    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"updateUserRecordTime"] length] == 0) {
//
////        [self performSelector:@selector(isup) withObject:nil afterDelay:0.5];
//        NSDate *updataTime = [now dateByAddingTimeInterval:7*24*60*60 - 8*60*60];//one week
//        NSString *date = [formatter stringFromDate:updataTime];
//        [userDefaults setObject:date forKey:@"updateUserRecordTime"];
//        [userDefaults synchronize];
//    }else{
//        NSString *nextUpdateTime = [userDefaults stringForKey:@"updateUserRecordTime"];
//        NSDate *date1 = [formatter dateFromString:nextUpdateTime];
//        NSTimeInterval time = [date1 timeIntervalSinceDate:now];
//        if (time <= 0) {
//            [userDefaults setObject:nil forKey:@"updateUserRecordTime"];
//            [self performSelector:@selector(isup) withObject:nil afterDelay:0.5];
//            NSDate *updataTime = [now dateByAddingTimeInterval:7*24*60*60 - 8*60*60];
//            NSString *date = [formatter stringFromDate:updataTime];
//            [userDefaults setObject:date forKey:@"updateUserRecordTime"];
//            [userDefaults synchronize];
//            
//        }
//    }
    
    //----BOOKREAD
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HAVENREQUEST"] isEqualToString:@"1"]||g_App.userInfo.userID == nil) {
//        [self addBookCollectionDataLocal];
//        [self addBookDataLocal];
//    }else if(g_App.userInfo.userID != nil){

    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HAVENREQUEST"] isEqualToString:@"1"]||g_App.userInfo.userID == nil) {
        [self addBookDataLocal];
        [self addBookCollectionDataLocal];
        
        self.bookTV.type = @"1";
        
    }else if(g_App.userInfo.userID != nil){
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"HAVENBOOKSAVE"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSMutableDictionary *bookData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         (NSString *)API_URL_CARTOONUSERREADHISTORY_DETAIL,@"r",
                                         @"1",@"page",
                                         @"100",@"size",
                                         @"0",@"withDevice",

                                         nil];
        //    [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) header:headers];
        
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) method:GETDATA];
        
        NSMutableDictionary *bookCollectionData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   (NSString *)API_URL_BOOKCOLLECTIONLIST_LIST,@"r",
                                                   @"1",@"page",
                                                   @"100",@"size",
                                                   nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) method:GETDATA];
    }

    
    //----BOOKCOLLECTION
//    NSMutableDictionary *bookCollectionData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                               (NSString *)API_URL_BOOKCOLLECTIONLIST_LIST,@"r",
//                                               @"1",@"page",
//                                               @"100",@"size",
//                                               nil];
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) method:GETDATA];
}



//推荐
-(void)refreshRecomment{
    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"" to_section:@"c" to_step:@"l" type:@"" channel:@"b" id:@""];
//    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cr" to_page:@"" to_section:@"c" to_step:@"d" type:@"m" channel:@"" id:@""];
//    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cf" to_page:@"" to_section:@"c" to_step:@"l" type:@"" channel:@"" id:@""];
//    
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cf" to_page:@"" to_section:@"c" to_step:@"l" type:@"" channel:@"" id:@""];
    
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"HAVENREQUEST"] isEqualToString:@"1"]||g_App.userInfo.userID == nil) {
        [self addBookCollectionDataLocal];
        [self addBookDataLocal];
    }else if(g_App.userInfo.userID != nil){
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"HAVENREQUEST"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSMutableDictionary *bookCollectionData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                   (NSString *)API_URL_BOOKCOLLECTIONLIST_LIST,@"r",
                                                   @"1",@"page",
                                                   @"100",@"size",
                                                   @"0",@"withDevice",
                                                   nil];
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookCollectionData object:self action:@selector(addBookCollectionData:) method:GETDATA];
        
//        NSMutableDictionary *recommentLuData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                                (NSString *)API_URL_RECOMMENTLU_LIST,@"r",
//                                                @"1",@"page",
//                                                @"100",@"size", nil];
//        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentLuData object:self action:@selector(addRecommentLuData:) method:GETDATA];
        NSMutableDictionary *bookData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                         (NSString *)API_URL_CARTOONUSERREADHISTORY_DETAIL,@"r",
                                         @"1",@"page",
                                         @"100",@"size",
                                         @"0",@"withDevice"
                                            , nil];
        //    [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) header:headers];
        
        [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:bookData object:self action:@selector(addBookData:) method:GETDATA];

        
    }
    
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_ADIMAGE_LIST,@"r",
                                    @"1",@"page",
                                    @"999",@"size",
                                    @"2",@"customPosition",nil];
    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(addLoadData:) method:GETDATA];
//    NSMutableDictionary *exprame1 = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                     (NSString *)API_URL_ADIMAGE_LOG_ADD,@"r",
//                                     @"79",@"id",nil];
//    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame1 object:self action:@selector(adImageLogAddFinish:) method:POSTDATA];
    //----RECOMMENT
//    NSMutableDictionary *recommentNewData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                             (NSString *)API_URL_RECENTUPDATE_LIST,@"r",
//                                             @"1",@"page",
//                                             @"3",@"size", nil];
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentNewData object:self action:@selector(addRecommentData:) method:GETDATA];
    
    

    NSMutableDictionary *recommentNewData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                                                          (NSString *)API_URL_RECOMMENTALL_LIST,@"r",
                                                                                          @"1",@"page",
                                                                                          @"999",@"size", nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:recommentNewData object:self action:@selector(addRecommentData:) method:GETDATA];
    
    //----HOT
//    NSMutableDictionary *recommentHotData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                             (NSString *)API_URL_RECOMMENTTOP_LIST,@"r",
//                                             @"1",@"page",
//                                             @"3",@"size", nil];
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentHotData object:self action:@selector(addRecommentHotData:) method:GETDATA];
    //----COLLECTION
//    NSMutableDictionary *recommentCollectionData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                                    (NSString *)API_URL_RECOMMENTCOLLECTIN_LIST,@"r",
//                                                    @"1",@"page",
//                                                    @"3",@"size", nil];
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentCollectionData object:self action:@selector(addRecommentCollectionData:) method:GETDATA];
    //----LU
//    NSMutableDictionary *recommentLuData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
//                                            (NSString *)API_URL_RECOMMENTLU_LIST,@"r",
//                                            @"1",@"page",
//                                            @"100",@"size", nil];
//    NSDictionary *headers;
//    if (g_App.userInfo.userID != nil) {
//        headers = @{@"userid":g_App.userInfo.userID,@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }else {
//        headers = @{@"userid":@"",@"clientid":[[NSUserDefaults standardUserDefaults] objectForKey:@"UUID"]};
//    }
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentLuData object:self action:@selector(addRecommentLuData:) method:GETDATA andHeaders:headers];
    
    
    
//    [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:recommentLuData object:self action:@selector(addRecommentLuData:) method:GETDATA];

    //----BOOKCOLLECTION
}

#pragma mark - 数据加载
//- (void)addContinueData:(NSDictionary *)dict{
//    NSLog(@"%@",dict);
//    self.recommentTV.recommentLuDataArr = [dict objectForKey:@"results"];
//
//}

-(void)addRecommentData:(NSDictionary *)dict{

    
//    _netImageView.alpha = 0;
//    _netImageView.alpha = 0;

    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
//        self.recommentTV.recommentDataArr = [dict objectForKey:@"results"][0][@"cartoonSetList"];
////        self.recommentTV.recommentDataCount = [dict objectForKey:@"extraInfo"];
//        self.recommentTV.recommentLuDataArr = [dict objectForKey:@"results"][1][@"cartoonSetList"];
//        self.recommentTV.recommentHotDataArr = [dict objectForKey:@"results"][2][@"cartoonSetList"];
//        self.recommentTV.recommentCollectionDataArr = [dict objectForKey:@"results"][3][@"cartoonSetList"];

//        [self.recommentTV setRecommentDataArr:[dict objectForKey:@"results"][0][@"cartoonSetList"] andRecommentHotDataArr:[dict objectForKey:@"results"][2][@"cartoonSetList"] andRecommentCollectionDataArr: [dict objectForKey:@"results"][3][@"cartoonSetList"] andRecommentLuDataArr:[dict objectForKey:@"results"][1][@"cartoonSetList"]];
        
        [self.recommentTV setRecommentAllDict:dict];
        [self writeConfigFile:dict];
    }else{
        NSDictionary *fileDict = [self requireData];
        
        if (fileDict) {
            [self.recommentTV setRecommentAllDict:fileDict];
        }else{
//        _isRefresh0 = YES;
//        [_refreshBtn1 removeFromSuperview];
//        [_refreshBtn2 removeFromSuperview];
//        [_refreshBtn3 removeFromSuperview];
//        [_refreshBtn4 removeFromSuperview];
//        //
//        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight - 20 - 40);
//        _netImageView.alpha = 0;
//        
//        _refreshBtn0 = [CustomTool createBtn];
//        [_netImageView addSubview:_refreshBtn0];
//            [_refreshBtn0 addTarget:self action:@selector(refreshRecomment) forControlEvents:UIControlEventTouchUpInside];
       }
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];

}


//写入文件

-(void)writeConfigFile:(NSDictionary *)documentDict
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) ; //得到documents的路径，为当前应用程序独享
    
    NSString *documentD = [paths objectAtIndex:0];
    
    
    NSString *configFile = [documentD stringByAppendingPathComponent:@"collection.plist"];
    
    [documentDict writeToFile:configFile atomically:YES];
    
    //}
    
}

- (NSDictionary *)requireData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) ; //得到documents的路径，为当前应用程序独享
    
    NSString *documentD = [paths objectAtIndex:0];
    
    
    NSString *configFile = [documentD stringByAppendingPathComponent:@"collection.plist"];
    NSDictionary *fileDict = [[NSDictionary alloc] initWithContentsOfFile:configFile];
    return fileDict;
}

//-(void)addBookDownloadData:(NSDictionary *)dict{
//    if (![[dict objectForKey:@"code"] integerValue]) {
//        self.bookTV.collectionArr = [dict objectForKey:@"results"];
//    }
//    //    NSLog(@"=-=-=-=-=-=-=-=-=%@",dict);
//}

-(void)addRecommentLuData:(NSDictionary *)dict{
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        self.recommentTV.recommentLuDataArr = [dict objectForKey:@"results"];
        _SWDVC.bookDataArr = [dict objectForKey:@"results"];
        
    }
}

-(void)addRecommentHotData:(NSDictionary *)dict{
    if (![[dict objectForKey:@"code"] integerValue]) {
        self.recommentTV.recommentHotDataArr = [dict objectForKey:@"results"];
    }
}

-(void)addRecommentCollectionData:(NSDictionary *)dict{
    if (![[dict objectForKey:@"code"] integerValue]) {
        self.recommentTV.recommentCollectionDataArr = [dict objectForKey:@"results"];
    }
}

-(void)addSearchData:(NSDictionary *)dict{
    if (![[dict objectForKey:@"code"] integerValue]) {
        self.searchTV.searchDataArr = [dict objectForKey:@"results"];
        self.searchTV.searchHeaderView.searchHeaderVC.searchDataArr = [dict objectForKey:@"results"];
        _keywordArr = [dict objectForKey:@"results"];
        
        NSArray *contentArr = [NSArray arrayWithArray:[dict objectForKey:@"results"]];
        //创建图片存储位置
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSString *searchFolder = [NSString stringWithFormat:@"%@/SearchFolder",documentDiretory];
        NSString *plistPath = [searchFolder stringByAppendingPathComponent:@"searchKeyword.plist"];
        for (int i = 0 ; i < contentArr.count; i ++) {
            if (![[NSFileManager defaultManager] fileExistsAtPath:searchFolder]) {
                [fileManager createDirectoryAtPath:searchFolder withIntermediateDirectories:YES attributes:nil error:nil];
                [fileManager createFileAtPath:plistPath contents:nil attributes:nil];
                NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
                [arr writeToFile:plistPath atomically:YES];
            }
            
            NSMutableArray *searchKeyArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
            NSDictionary *searchKeywordDic = contentArr[i];
            if (searchKeyArray.count <= contentArr.count) {
                [searchKeyArray addObject:searchKeywordDic];
                [searchKeyArray writeToFile:plistPath atomically:YES];
            }
            [self.searchTV.searchHeaderView.searchHeaderVC initData];
        }
    }
//    NSLog(@"------%@",[dict objectForKey:@"results"]);
}

-(void)addSearchListData:(NSDictionary *)dict{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _netImageView.alpha = 0;
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        self.searchTV.searchFootArr = [dict objectForKey:@"results"];
        self.searchTV.searchFooterView.likereadVC.searchDataListArr = [dict objectForKey:@"results"];
        
        NSArray *contentArr = [NSArray arrayWithArray:[dict objectForKey:@"results"]];
        //创建图片存储位置
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSString *searchFolder = [NSString stringWithFormat:@"%@/SearchFolder",documentDiretory];
        NSString *plistPath = [searchFolder stringByAppendingPathComponent:@"searchLikeRead.plist"];
        for (int i = 0 ; i < contentArr.count; i ++) {
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:contentArr[i][@"images"]]]];
            if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
                [fileManager createFileAtPath:plistPath contents:nil attributes:nil];
                NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
                [arr writeToFile:plistPath atomically:YES];
            }
            //id
            NSMutableArray *isearchLikeReadArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
            NSDictionary *searchLikeReadDic = contentArr[i];
            if (isearchLikeReadArray.count <= contentArr.count) {
                [isearchLikeReadArray addObject:searchLikeReadDic];
                [isearchLikeReadArray writeToFile:plistPath atomically:YES];
            }
            //image
            NSString *imagePaths = [searchFolder stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",contentArr[i][@"id"]]];
            [UIImagePNGRepresentation(image) writeToFile:imagePaths atomically:YES];
            
            
            [self.searchTV.searchFooterView.likereadVC initData];
        }
    }else{
//        _isRefresh1 = YES;
//        [_refreshBtn0 removeFromSuperview];
//        [_refreshBtn2 removeFromSuperview];
//        [_refreshBtn3 removeFromSuperview];
//        [_refreshBtn4 removeFromSuperview];
//        //
//        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight - 20 - 40);
//        _netImageView.alpha = 1;
//        
//        _refreshBtn1 = [CustomTool createBtn];
//        [_netImageView addSubview:_refreshBtn1];
//        [_refreshBtn1 addTarget:self action:@selector(downloadSearchData) forControlEvents:UIControlEventTouchUpInside];
    }
    
}

-(void)addTopData:(NSDictionary *)dict{
    
//    NSArray *nameArr = @[@"id11",@"id1",@"id3",@"id4"];
//    NSArray *contentArr = [[NSArray alloc]init];
//    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
//    _netImageView.alpha = 0;
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        self.TOPTV.topDataArr = [NSArray arrayWithArray:[dict objectForKey:@"results"]];
        NSArray *contentArr = [NSArray arrayWithArray:[dict objectForKey:@"results"]];
        //创建图片存储位置
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSString *topFolder = [NSString stringWithFormat:@"%@/TOPFolder",documentDiretory];
        NSString *plistPath = [topFolder stringByAppendingPathComponent:@"topID.plist"];
        NSString *namePath = [topFolder stringByAppendingPathComponent:@"topName.plist"];
        for (int i = 0 ; i < contentArr.count; i ++) {
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:contentArr[i][@"images"]]]];
            if (![[NSFileManager defaultManager] fileExistsAtPath:topFolder]) {
                [fileManager createDirectoryAtPath:topFolder withIntermediateDirectories:YES attributes:nil error:nil];
                [fileManager createFileAtPath:plistPath contents:nil attributes:nil];
                [fileManager createFileAtPath:namePath contents:nil attributes:nil];
                NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
                [arr writeToFile:plistPath atomically:YES];
                [arr writeToFile:namePath atomically:YES];
            }
            
            //id
            NSMutableArray *idArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
            NSString *topIDData = contentArr[i][@"id"];
            if (idArray.count <=contentArr.count) {
                [idArray addObject:topIDData];
                [idArray writeToFile:plistPath atomically:YES];
            }
            
            //name
            NSMutableArray *nameArray = [NSMutableArray arrayWithContentsOfFile:namePath];
            NSString *topNameData = contentArr[i][@"name"];
            if (nameArray.count <= contentArr.count) {
                [nameArray addObject:topNameData];
                [nameArray writeToFile:namePath atomically:YES];
            }
            
            [_TOPTV initData];
            //image
            NSString *imagePaths = [topFolder stringByAppendingPathComponent:topIDData];
            [UIImagePNGRepresentation(image) writeToFile:imagePaths atomically:YES];
        }
    }else{
//        _isRefresh2 = YES;
//        [_refreshBtn0 removeFromSuperview];
//        [_refreshBtn1 removeFromSuperview];
//        [_refreshBtn3 removeFromSuperview];
//        [_refreshBtn4 removeFromSuperview];
//        
//        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight - 20 - 40);
//        _netImageView.alpha = 1;
//        
//        _refreshBtn2 = [CustomTool createBtn];
//        [_netImageView addSubview:_refreshBtn2];
//        [_refreshBtn2 addTarget:self action:@selector(downloadTopData) forControlEvents:UIControlEventTouchUpInside];
    }
    
//    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:contentArr[0][@"images"]]]];
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentDiretory = [paths objectAtIndex:0];
//    NSString *imagePaths =
    
}

-(void)addCategoryData:(NSDictionary *)dict{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _netImageView.alpha = 0;
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        self.categoryCV.categoryDataArr = [dict objectForKey:@"results"];
        
        NSArray *contentArr = [NSArray arrayWithArray:[dict objectForKey:@"results"]];
        //创建图片存储位置
        NSFileManager *fileManager = [[NSFileManager alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSString *categoryFolder = [NSString stringWithFormat:@"%@/CategoryFolder",documentDiretory];
        NSString *plistPath = [categoryFolder stringByAppendingPathComponent:@"categoryID.plist"];
        NSString *namePath = [categoryFolder stringByAppendingPathComponent:@"categoryName.plist"];
        for (int i = 0 ; i < contentArr.count; i ++) {
            
            UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:contentArr[i][@"images"]]]];
            if (![[NSFileManager defaultManager] fileExistsAtPath:categoryFolder]) {
                [fileManager createDirectoryAtPath:categoryFolder withIntermediateDirectories:YES attributes:nil error:nil];
                [fileManager createFileAtPath:plistPath contents:nil attributes:nil];
                [fileManager createFileAtPath:namePath contents:nil attributes:nil];
                NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
                [arr writeToFile:plistPath atomically:YES];
                [arr writeToFile:namePath atomically:YES];
            }
            //id
            NSMutableArray *idArray = [NSMutableArray arrayWithContentsOfFile:plistPath];
            NSString *topIDData = contentArr[i][@"id"];
            if (idArray.count <= contentArr.count) {
                [idArray addObject:topIDData];
                [idArray writeToFile:plistPath atomically:YES];
            }
            
            //name
            NSMutableArray *nameArray = [NSMutableArray arrayWithContentsOfFile:namePath];
            NSString *topNameData = contentArr[i][@"name"];
            if (nameArray.count <= contentArr.count) {
                [nameArray addObject:topNameData];
                [nameArray writeToFile:namePath atomically:YES];
            }
            
            //image
            NSString *imagePaths = [categoryFolder stringByAppendingPathComponent:topIDData];
            [UIImagePNGRepresentation(image) writeToFile:imagePaths atomically:YES];
            
            [self.categoryCV initData];
        }
    }else{
//        _isRefresh3 = YES;
//        [_refreshBtn0 removeFromSuperview];
//        [_refreshBtn2 removeFromSuperview];
//        [_refreshBtn1 removeFromSuperview];
//        [_refreshBtn4 removeFromSuperview];
//        
//        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight - 20 - 40);
//        _netImageView.alpha = 1;
//        
//        _refreshBtn3 = [CustomTool createBtn];
//        [_netImageView addSubview:_refreshBtn3];
//        [_refreshBtn3 addTarget:self action:@selector(downloadCategoryData) forControlEvents:UIControlEventTouchUpInside];
    }
}



- (void)addBookDataLocal{
    
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableArray *bookInfoArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"isRead":@"1"} andChooseType:@"lastReadTime"];
    self.bookTV.bookDataArr = bookInfoArr;
    if(bookInfoArr.count > 60){
        NSRange range;
        range.location = 60;
        range.length = bookInfoArr.count - 60;
        [bookInfoArr removeObjectsInRange:range];
    }
    
    _SWDVC.bookDataArr = bookInfoArr;

}

-(void)addBookData:(NSDictionary *)dict{
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _netImageView.alpha = 0;
    if (dict && ![[dict objectForKey:@"code"] integerValue]) {
        NSMutableArray *cartoonInfoAllArr = [CartoonInfoAll paraCartoonInfoAll:dict andReadSaveType:@"ISREAD"];
        
        if(cartoonInfoAllArr.count > 60){
            NSRange range;
            range.location = 60;
            range.length = cartoonInfoAllArr.count - 60;
            [cartoonInfoAllArr removeObjectsInRange:range];
        }
        
        self.bookTV.bookDataArr = cartoonInfoAllArr;
        _SWDVC.bookDataArr = cartoonInfoAllArr;
        
        
        
    }else{
        _isRefresh4 = YES;
    }
}

- (void)updateDownLoad:(NSDictionary *)dict{
    self.bookTV.downLoadArr = @[];
}

- (void)addBookCollectionDataLocal{
//    [MBProgressHUD hideHUDForView:self.view animated:YES];
    NSMutableArray *bookCollectionArr = [[DataBaseHelper shared] fetchCartoonAllInfoWithCondition:@{@"collectionStatus":@"1"} andChooseType:@"collectTime"];
    self.bookTV.collectionArr = bookCollectionArr;
    _SWDVC.collectionArr = bookCollectionArr;
}

-(void)addBookCollectionData:(NSDictionary *)dict{
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (![[dict objectForKey:@"code"] integerValue]) {
        NSMutableArray *bookCollectionArr = [CartoonInfoAll paraCartoonInfoAll:dict andReadSaveType:@"ISCOLLECTION"];
        self.bookTV.collectionArr = bookCollectionArr;
        _SWDVC.collectionArr = bookCollectionArr;
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0) {
        
    }else {
//        SecondReadController *readC = [[SecondReadController alloc]init];
//        readC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//        //    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
//        //    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:readC];
//        
//        readC.charpList = @[];
//        [self presentViewController:readC animated:YES completion:^{
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"readDetails" object:_modell];
//        }];

        
//        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]) {
        SecondReadController *readC = [[SecondReadController alloc]init];
        readC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
        //    readC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        //    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:readC];
        readC.charpList = @[];
        readC.allInfo = strAllInfo[0];
        [readC setChangeMark:^(NSInteger tagMark) {
            
        }];

        [self presentViewController:readC animated:YES completion:^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"readDetails" object:strAllInfo[1]];
        }];}
//    }
}


#pragma mark - 改变判断条件
-(void)newSearch{
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"SEARCHSWITCHDOWNLOADNEWDATA"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //写入现在时间
    NSDate *now = [NSDate date];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSDate *updataTime = [now dateByAddingTimeInterval:30*24*60*60 - 8*60*60];
    NSString *date = [formatter stringFromDate:updataTime];
    [userDefaults setObject:date forKey:@"updateSearchDataTime"];
    [userDefaults synchronize];
}
-(void)newTop{
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"TOPSWITCHDOWNLOADNEWDATA"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //写入现在时间
    NSDate *now = [NSDate date];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSDate *updataTime = [now dateByAddingTimeInterval:30*24*60*60 - 8*60*60];
    NSString *date = [formatter stringFromDate:updataTime];
    [userDefaults setObject:date forKey:@"updateTopDataTime"];
    [userDefaults synchronize];
}
-(void)newCategory{
    [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"CATEGORYSWITCHDOWNLOADNEWDATA"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //写入现在时间
    NSDate *now = [NSDate date];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"YYYY-MM-dd hh:mm:ss"];
    NSDate *updataTime = [now dateByAddingTimeInterval:30*24*60*60 - 8*60*60];
    NSString *date = [formatter stringFromDate:updataTime];
    [userDefaults setObject:date forKey:@"updateCategoryDataTime"];
    [userDefaults synchronize];
}
@end
