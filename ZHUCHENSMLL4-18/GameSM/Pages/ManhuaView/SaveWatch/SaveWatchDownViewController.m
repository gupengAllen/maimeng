//
//  SaveWatchDownViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/11/9.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "SaveWatchDownViewController.h"
#import "XLScrollViewer.h"
#import "Config.h"
#import "DownLoadListViewController.h"
#import "LYTabBarController.h"
#import "BookDetailsViewController.h"

#import "CustomTool.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
@interface SaveWatchDownViewController ()<UIScrollViewDelegate>
{
    UISegmentedControl *segmentedControl;
    UIScrollView *Scroll;
    UIButton *ArrangeBtn;
    NSInteger _lastPage;
}
@end

@implementation SaveWatchDownViewController

//- (void)loadView{
//    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//    [self.view setBackgroundColor:[UIColor whiteColor]];
//}
//
- (void)viewWillAppear:(BOOL)animated{
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    [self initSegmentedControl];
    
    [self initXLScrollView];
    
    segmentedControl.selectedSegmentIndex = _moveType;
    if (_moveType == 0) {
        [Scroll addSubview:_recentWatchVC.view];
    }else if (_moveType == 1){
        [Scroll addSubview:_bookDetailsVC.view];
    }
    Scroll.contentOffset = CGPointMake(ScreenSizeWidth * _moveType, 0);

}

- (void)setMoveType:(NSInteger)moveType{
    _moveType = moveType;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)initXLScrollView{
    
    
    
    _recentWatchVC = [[BookDetailsViewController alloc] init];
    _recentWatchVC.view.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight);
    _recentWatchVC.bookArr = _bookDataArr;
    _recentWatchVC.rootArray = _rootArr;
    
    
    
    _bookDetailsVC = [[BookDetailsViewController alloc] init];
    _bookDetailsVC.view.frame = CGRectMake(ScreenSizeWidth , 0, ScreenSizeWidth, ScreenSizeHeight);
    _bookDetailsVC.bookArr = _collectionArr;
    _bookDetailsVC.rootArray = _rootArr;
    
    
    DownLoadListViewController *downListVCTwo = [[DownLoadListViewController alloc] init];
    //    downListVCTwo.zipUrl = @[@"http://pixelresort.com/downloads/safariset_mac.zip",@"http://pixelresort.com/downloads/safariset_mac.zip"];
    [self addChildViewController:downListVCTwo];
    downListVCTwo.view.frame = CGRectMake(ScreenSizeWidth * 2, 0, ScreenSizeWidth, ScreenSizeHeight);
    
//    [views addObject:downListVC.view];
//    [views addObject:downListVC.view];
//    [views addObject:downListVC.view];
//    UIScrollView *Scroll = [XLScrollViewer scrollWithFrame:ScrollFrame withViews:views withButtonNames:titleNames withThreeAnimation:choose];
    Scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 64, ScreenSizeWidth, ScreenSizeHeight - 64)];
    Scroll.delegate = self;
//    [Scroll addSubview:_recentWatchVC.view];
    [Scroll addSubview:downListVCTwo.view];
//    [Scroll addSubview:_bookDetailsVC.view];
    Scroll.backgroundColor = [UIColor whiteColor];
    Scroll.pagingEnabled = YES;
    Scroll.scrollEnabled = NO;
    Scroll.contentSize = CGSizeMake(3 * ScreenSizeWidth, 0);
    [self.view addSubview:Scroll];
}



- (void)initSegmentedControl
{
    
//    UINavigationBar *bar = self.navigationController.navigationBar;
//    bar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
//    
//    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    backButton.frame = CGRectMake(0, 0, 33, 33);
//    [backButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backBtn;
//    
//    ArrangeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    ArrangeBtn.frame = CGRectMake(0, 0, 33, 33);
//    ArrangeBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
//    [ArrangeBtn setTitle:@"整理" forState:UIControlStateNormal];
//    [ArrangeBtn setTitle:@"取消" forState:UIControlStateSelected];
//    [ArrangeBtn addTarget:self action:@selector(arrangeBtn:) forControlEvents:UIControlEventTouchUpInside];
//    if(_moveType == 2){
//        ArrangeBtn.hidden = NO;
//    }else{
//        ArrangeBtn.hidden = YES;
//    }
//    UIBarButtonItem *ArrangeBtnItem = [[UIBarButtonItem alloc]initWithCustomView:ArrangeBtn];
//    self.navigationItem.rightBarButtonItem = ArrangeBtnItem;
//
//    
//    
//    NSArray *segmentedData = [[NSArray alloc]initWithObjects:@"历史",@"收藏",@"下载",nil];
//    segmentedControl = [[UISegmentedControl alloc]initWithItems:segmentedData];
////    segmentedControl.frame = CGRectMake(50.0, 5.0,250, 30.0);
//    segmentedControl.center = CGPointMake(ScreenSizeWidth/2 - 28 * _Scale, 20);
//    segmentedControl.size = CGSizeMake(200, 30.0);
//    /*
//     这个是设置按下按钮时的颜色
//     */
//    segmentedControl.tintColor = [UIColor whiteColor];
//    segmentedControl.selectedSegmentIndex = 0;//默认选中的按钮索引
//    
//    
//    /*
//     下面的代码实同正常状态和按下状态的属性控制,比如字体的大小和颜色等
//     */
//    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:12],NSFontAttributeName,[UIColor whiteColor], NSForegroundColorAttributeName, nil];
//    
//    
//    [segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
//    
//    
//    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
//    
//    [segmentedControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateHighlighted];
//    
//    //设置分段控件点击相应事件
//    [segmentedControl addTarget:self action:@selector(doSomethingInSegment:)forControlEvents:UIControlEventValueChanged];
//    
//    [self.navigationController.navigationBar addSubview:segmentedControl];
    UIView *view = [CustomTool createNavView];
    [self.view addSubview:view];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 25, 33, 33);
    [backButton setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
    
    ArrangeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    ArrangeBtn.frame = CGRectMake(KScreenWidth - 38, 30, 33, 33);
    ArrangeBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
    [ArrangeBtn setTitleColor:[UIColor colorWithRed:228/255.0 green:62/255.0 blue:82/255.0 alpha:1] forState:UIControlStateNormal];
    [ArrangeBtn setTitle:@"整理" forState:UIControlStateNormal];
    [ArrangeBtn setTitle:@"取消" forState:UIControlStateSelected];
    [ArrangeBtn addTarget:self action:@selector(arrangeBtn:) forControlEvents:UIControlEventTouchUpInside];
    if(_moveType == 2||_moveType == 1){
        ArrangeBtn.hidden = NO;
    }else{
        ArrangeBtn.hidden = YES;
    }
    [view addSubview:ArrangeBtn];
    
    NSArray *segmentedData = [[NSArray alloc]initWithObjects:@"继续撸",@"收藏",@"下载",nil];
    segmentedControl = [[UISegmentedControl alloc]initWithItems:segmentedData];
    segmentedControl.frame = CGRectMake(60, 20.0,200, 30.0);
//    [segmentedControl setBackgroundImage:[UIImage imageNamed:@"fudaohang"] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//    segmentedControl.layer.masksToBounds = YES;
//    segmentedControl.layer.cornerRadius = 15;
//    segmentedControl.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"fudaohang"]];
    segmentedControl.center = CGPointMake(ScreenSizeWidth/2, 40);
//    segmentedControl.layer.masksToBounds = YES;
//    segmentedControl.layer.cornerRadius = 4;
//    segmentedControl.layer.borderWidth = 1;
//    segmentedControl.layer.borderColor = [UIColor colorWithRed:211/255.0 green:190/255.0 blue:190/255.0 alpha:1].CGColor;
    //    segmentedControl.size = CGSizeMake(200, 30.0);
    /*
     这个是设置按下按钮时的颜色
     */
    segmentedControl.tintColor = [UIColor colorWithRed:255/255.0 green:128/255.0 blue:138/255.0 alpha:1];//colorWithRed:252/255.0 green:67/255.0 blue:91/255.0 alpha:1
    segmentedControl.selectedSegmentIndex = 0;//默认选中的按钮索引
    /*
     下面的代码实同正常状态和按下状态的属性控制,比如字体的大小和颜色等
     */
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont boldSystemFontOfSize:12],NSFontAttributeName,[UIColor colorWithRed:207/255.0 green:97/255.0 blue:106/255.0 alpha:1], NSForegroundColorAttributeName, nil];
    [segmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
    NSDictionary *highlightedAttributes = [NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName];
    [segmentedControl setTitleTextAttributes:highlightedAttributes forState:UIControlStateHighlighted];
    //设置分段控件点击相应事件
    [segmentedControl addTarget:self action:@selector(doSomethingInSegment:)forControlEvents:UIControlEventValueChanged];
    [view addSubview:segmentedControl];
}

- (void)arrangeBtn:(UIButton *)arrangeBtn{
    arrangeBtn.selected = !arrangeBtn.selected;
    if (arrangeBtn.selected) {
        segmentedControl.userInteractionEnabled = NO;
    }else {
        segmentedControl.userInteractionEnabled = YES;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"DELETECOMMIC" object:nil];

}

- (void)doSomethingInSegment:(UISegmentedControl *)seg{
    NSString *to_page;
    switch (seg.selectedSegmentIndex) {
        case 0:
            to_page = @"crl";
            break;
        case 1:
            to_page = @"cfl";
        case 2:
            to_page = @"cdl";
            
        default:
            break;
    }
    NSString *from_page;
    switch (_lastPage) {
        case 0:
            from_page = @"crl";
            break;
        case 1:
            from_page = @"cfl";
            break;
        case 2:
            from_page = @"cdl";
            break;
        default:
            break;
    }
    
    [[LogHelper shared] writeToFilefrom_page:from_page from_section:@"c" from_step:@"l" to_page:to_page to_section:@"c" to_step:@"l" type:@"" id:@"0"];
    
    if(seg.selectedSegmentIndex == 2||seg.selectedSegmentIndex == 1){
        ArrangeBtn.hidden = NO;
    }else{
        ArrangeBtn.hidden = YES;
    }
    Scroll.contentOffset =  CGPointMake(seg.selectedSegmentIndex * ScreenSizeWidth, 0);
    if (seg.selectedSegmentIndex == 0) {
        [Scroll addSubview:_recentWatchVC.view];
    }else if (seg.selectedSegmentIndex == 1){
        [Scroll addSubview:_bookDetailsVC.view];
    }
    _lastPage = seg.selectedSegmentIndex;
    
}

-(void)misBack{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewWillDisappear:(BOOL)animated{
//    [(LYTabBarController *)self.tabBarController hiddenBar:NO animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    segmentedControl.selectedSegmentIndex = scrollView.contentOffset.x/ScreenSizeWidth;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation

{
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate

{
    return NO;
}

-(BOOL)canBecomeFirstResponder{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations

{
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)--竖直
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
