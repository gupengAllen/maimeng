//
//  SaveWatchDownViewController.h
//  GameSM
//
//  Created by 顾鹏 on 15/11/9.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@class BookDetailsViewController;

@interface SaveWatchDownViewController : BaseViewController
@property(nonatomic,copy)BookDetailsViewController *bookDetailsVC;
@property(nonatomic,copy)BookDetailsViewController *recentWatchVC;
@property(nonatomic,copy)NSArray *bookDataArr;
@property(nonatomic,copy)NSArray *collectionArr;
@property(nonatomic,retain)NSMutableArray *rootArr;
@property(nonatomic,assign)NSInteger moveType;
@end
