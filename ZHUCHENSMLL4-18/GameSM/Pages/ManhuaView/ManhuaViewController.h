//
//  ManhuaViewController.h
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"
@class RecommentTableView;
@class SearchTavleView;
@class TOPtableView;
@class BookTableView;
@class CategoryCollectionView;
@class RecommentCell;
@class SearchHeaderCollecationView;
@class LikereadConllectionView;

@interface ManhuaViewController : BaseTableViewController
@property(nonatomic,strong)RecommentTableView *recommentTV;
@property(nonatomic,strong)SearchTavleView *searchTV;
@property(nonatomic,strong)TOPtableView *TOPTV;
@property(nonatomic,strong)SearchHeaderCollecationView *SearchHCV;
@property(nonatomic,strong)LikereadConllectionView *likeReadCV;
@property(nonatomic,strong)BookTableView *bookTV;
@property(nonatomic,strong)CategoryCollectionView *categoryCV;

@property(nonatomic,strong)NSMutableDictionary *tableViewDics;

@property(nonatomic,strong)NSDictionary *modell;


@end
