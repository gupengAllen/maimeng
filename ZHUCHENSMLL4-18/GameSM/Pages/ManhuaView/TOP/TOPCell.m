//
//  TOPCell.m
//  GameSM
//
//  Created by mac on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "TOPCell.h"
#import "TopModel.h"

#import "UIImageView+AFNetworking.h"

@interface TOPCell ()
{
    UIImageView *_iv;
    UILabel     *_topLabel;
    NSInteger   _row;
    NSString    *_plistPath;
}
@end

@implementation TOPCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andRow:(NSInteger)row{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _row = row;
    }
    return self;
}

- (void)awakeFromNib {
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)creatView{
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, KTOPCellHeight*_Scale6 - 10 * __Scale6)];
    imgView.backgroundColor = [UIColor clearColor];
        [imgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model[@"images"]]]];    
    [self.contentView addSubview:imgView];
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(-1,imgView.bottom, ScreenSizeWidth + 2, 10 * __Scale6)];
    bottomView.layer.borderWidth = 1.0;
    bottomView.layer.borderColor = [[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1.0] CGColor];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:bottomView];
}

-(void)setModel:(NSDictionary *)model{
    _model = model;
    [self creatView];
}
-(void)setRowNum:(int)rowNum{
    _rowNum = rowNum;
    [self creatView];
}

@end
