//
//  TopModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/13.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TopModel : NSObject

@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *images;
@property(nonatomic,strong)NSString *url;
@property(nonatomic,strong)NSString *statisticMethod;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *createTime;
@property(nonatomic,strong)NSString *userID;
@property(nonatomic,strong)NSString *id;




@end
