//
//  TOPCell.h
//  GameSM
//
//  Created by mac on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Config.h"

@class TopModel;

@interface TOPCell : UITableViewCell

//@property(nonatomic,strong)TopModel *model;
@property(nonatomic,strong)NSDictionary *model;

@property(nonatomic,assign)int rowNum;
@property(nonatomic,strong)NSArray *topCellIdArr;

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andRow:(NSInteger)row;

@end
