//
//  TOPtableView.m
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "TOPtableView.h"
#import "ManhuaViewController.h"
#import "MBProgressHUD.h"


#import "TOPCell.h"
#import "TopModel.h"

@interface TOPtableView ()
{
    int _countNum;
    NSMutableArray *_dataArr;
    NSMutableDictionary *_dic;
    
    NSString *_plistPath;
}
@end

@implementation TOPtableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{

    _dataArr = [NSMutableArray arrayWithCapacity:0];
    self = [super initWithFrame:frame style:style];
    if (self ) {
        self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 20);
        [self creatView];
    }
    return self;
}

#pragma mark- 初始化方法
- (void)creatView{
    //2.设置代理
    self.delegate = self;
    self.dataSource = self;
    
    self.showsVerticalScrollIndicator = NO;
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

#pragma mark-UITableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return _topDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"TOPCell";
    TOPCell *cell = [tableView dequeueReusableCellWithIdentifier:identify];
    cell = [[TOPCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identify andRow:indexPath.row];
    [self deselectRowAtIndexPath:indexPath animated:YES];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.model = _topDataArr[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_TOP_DETAIL,_dataArr[row]];
        NSArray *titleArr = @[str,_topDataArr[indexPath.row][@"name"]];
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cb" to_page:@"cbl" to_section:@"c" to_step:@"l" type:@"" channel:@"" id:_dataArr[indexPath.row]];
        [[LogHelper shared] writeToFilefrom_page:@"cbh" from_section:@"c" from_step:@"h" to_page:@"cbl" to_section:@"c" to_step:@"l" type:@"" id:_dataArr[indexPath.row]];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"topDetails" object:titleArr];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return KTOPCellHeight*_Scale6;
}

-(void)setTopDataArr:(NSMutableArray *)topDataArr{
    _dataArr = [NSMutableArray array];
    if (topDataArr) {
        [MBProgressHUD hideHUDForView:self animated:YES];
    }else{
        [MBProgressHUD hideHUDForView:self animated:YES];
    }
    _topDataArr = topDataArr;
    for (NSDictionary *dic in _topDataArr) {
        NSString *str = [NSString stringWithFormat:@"%@",dic[@"id"]];
        [_dataArr addObject:str];
    }
    [self reloadData];
}



@end
