//
//  TOPtableView.h
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableView.h"

@class TopModel;

@interface TOPtableView : BaseTableView<UITableViewDelegate,UITableViewDataSource>

@property(nonatomic,strong)NSMutableArray *topDataArr;

@property(nonatomic,strong)NSMutableArray *topIdArray;
@property(nonatomic,strong)NSMutableArray *topNameArray;



@end
