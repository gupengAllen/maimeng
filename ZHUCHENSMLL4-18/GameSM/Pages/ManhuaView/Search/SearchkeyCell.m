//
//  SearchkeyCell.m
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchkeyCell.h"
#import "SearchHeaderModel.h"
#define kcellHeight 30

@interface SearchkeyCell ()
{
    UILabel *_keywordLable;
    UIView  *_keywordView;
}
@end


@implementation SearchkeyCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


- (void)creatView{
    _keywordView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, kcellHeight)];
    _keywordView.layer.borderColor = [UIColor colorWithRed:218/255.0 green:56/255.0 blue:79/255.0 alpha:1].CGColor;
    _keywordView.layer.borderWidth = 1.0f;
    _keywordView.layer.cornerRadius = 3.0;
    _keywordView.layer.masksToBounds = YES;
    _keywordView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_keywordView];
    
    _keywordLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, kcellHeight)];
        _keywordLable.text = _model.keyword;

    _keywordLable.font = [UIFont systemFontOfSize:13.0];
    _keywordLable.textColor = [UIColor colorWithRed:218/255.0 green:56/255.0 blue:79/255.0 alpha:1];
    _keywordLable.textAlignment = NSTextAlignmentCenter;
    [_keywordView addSubview:_keywordLable];
}

-(void)setModel:(SearchHeaderModel *)model{
    _model = model;
    [self creatView];
}

-(void)setIdStr:(NSString *)idStr{
    _idStr = idStr;
    [self creatView];
}

@end
