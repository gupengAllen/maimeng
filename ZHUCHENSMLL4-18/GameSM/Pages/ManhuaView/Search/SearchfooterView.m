//
//  SearchfooterView.m
//  GameSM
//
//  Created by ABC on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchfooterView.h"
#include "UIViewExt.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@implementation SearchfooterView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatView];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}


- (void)creatView{
    
    _likereadLable = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.width, 30)];
    _likereadLable.text = @"  大家都爱看";
    _likereadLable.textColor = [UIColor colorWithRed:212/255.0 green:56/255.0 blue:79/255.0 alpha:1];
    _likereadLable.textAlignment = NSTextAlignmentLeft;
    _likereadLable.font = [UIFont systemFontOfSize:16];
    [self addSubview:_likereadLable];
    
    _likereadVC = [[LikereadConllectionView alloc]initWithFrame:CGRectMake(0, _likereadLable.bottom, self.width, KScreenheight - _likereadLable.bottom - 290)];
    _likereadVC.backgroundColor = [UIColor whiteColor];
    [self addSubview:_likereadVC];
}

#pragma get方法计算高度
- (void)setLikereadArrays:(NSArray *)likereadArrays{
    _likereadArrays = likereadArrays;
    //计算_searchdvc的高度
    _likereadVC.likeReadArr = _likereadArrays;
    float height  = _likereadLable.height + _likereadVC.height;
    CGRect kframe = self.frame;
    kframe.size.height = height;
    self.frame = kframe;
    [self creatView];
}

@end
