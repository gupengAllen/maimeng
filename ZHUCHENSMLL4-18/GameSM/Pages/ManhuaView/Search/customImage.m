//
//  customImage.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/19.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "customImage.h"
#import "CategoryDetailsModel.h"
#import "BookModel.h"
#import "Config.h"

@implementation customImage
- (void)setCategoryModel:(CategoryDetailsModel *)categoryModel{
    UIImageView * imageV = [[UIImageView alloc] initWithFrame:CGRectMake(4, 4, self.frame.size.width-8, (self.frame.size.width-8)*___Scale)];
    imageV.layer.cornerRadius = 3.0;
    imageV.layer.masksToBounds = YES;
    imageV.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",categoryModel.images]]]];
    
    [self addSubview:imageV];
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(4, imageV.bottom, self.frame.size.width-8, 20)];
    name.font = [UIFont systemFontOfSize:13.0];
    name.textAlignment = 0;
    name.text = categoryModel.name;
    [self addSubview:name];
}

- (void)setBookModel:(BookModel *)bookModel{
    UIImageView * imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 25)];
    imageV.layer.cornerRadius = 3.0;
    imageV.layer.masksToBounds = YES;
    imageV.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",bookModel.images]]]];
    
    [self addSubview:imageV];
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 20, self.frame.size.width, 20)];
    name.font = [UIFont systemFontOfSize:13.0];
    name.textAlignment = NSTextAlignmentCenter;
    name.text = bookModel.name;
    [self addSubview:name];

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
