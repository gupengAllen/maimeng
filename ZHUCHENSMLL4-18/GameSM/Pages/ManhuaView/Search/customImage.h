//
//  customImage.h
//  GameSM
//
//  Created by 顾鹏 on 15/10/19.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CategoryDetailsModel;
@class BookModel;
@interface customImage : UIView
@property(nonatomic,copy)CategoryDetailsModel *categoryModel;
@property(nonatomic,copy)BookModel *bookModel;
@end
