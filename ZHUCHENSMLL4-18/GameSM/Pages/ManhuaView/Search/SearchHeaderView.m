//
//  SearchHeaderView.m
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchHeaderView.h"
#import "UIViewExt.h"
#import "UIView+UIViewController.h"
#import "SearchDetailViewcController.h"
#import "Config.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface SearchHeaderView (){
    SearchDetailViewcController *_searchDVC;
    UIButton *_cancelButton;
    
}
@property (nonatomic, assign) int currentSize;
@property (nonatomic, assign) int currentIndex;

@end

@implementation SearchHeaderView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatView];
        self.backgroundColor = [UIColor whiteColor];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resignSearch) name:@"resignSearch" object:nil];
    }
    return self;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
//    _searchBar.showsCancelButton = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeScr" object:searchBar];
    [searchBar resignFirstResponder];
    
}
-(void)misSearchResult:(UIButton *)btn{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeScr" object:_searchBar];
    [_searchBar resignFirstResponder];
}
- (void)resignSearch{
    [_searchBar resignFirstResponder];
}

- (void)creatView{
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 34)];
    topView.backgroundColor = [UIColor whiteColor];
    [self addSubview:topView];
    //1.创建搜索控件
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(2, 2, ScreenSizeWidth-50, 30)];
    _searchBar.layer.borderWidth = 1;
    _searchBar.layer.borderColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1].CGColor;
    _searchBar.backgroundColor = RGBACOLOR(255, 255, 255, 1);
    _searchBar.backgroundImage = [self imageWithColor:[UIColor clearColor] size:_searchBar.bounds.size];
    _searchBar.placeholder = @"搜索";
    _searchBar.delegate = self;
    _searchBar.layer.masksToBounds = YES;
    _searchBar.layer.cornerRadius = 4;
    [topView addSubview:_searchBar];
    //取消按钮
    _cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(topView.width-45, 2, 40, 30)];
    [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [_cancelButton setTitleColor:[UIColor colorWithRed:212/255.0 green:56/255.0 blue:79/255.0 alpha:1] forState:UIControlStateNormal];
    [_cancelButton addTarget:self action:@selector(misSearchResult:) forControlEvents:UIControlEventTouchUpInside];
    _cancelButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
    [topView addSubview:_cancelButton];
    
    //2.创建组的标题lable
    _sessionLable = [[UILabel alloc]initWithFrame:CGRectMake(KSearchHeaderInset, _searchBar.bottom, ScreenSizeWidth-KSearchHeaderInset, 30)];
    _sessionLable.text = @" 大家都在搜";
    _sessionLable.font = [UIFont systemFontOfSize:16];
    _sessionLable.textAlignment = NSTextAlignmentLeft;
    _sessionLable.textColor = [UIColor colorWithRed:212/255.0 green:56/255.0 blue:79/255.0 alpha:1];
    [self addSubview:_sessionLable ];
    
    //3.创建一个collecationview
    _searchHeaderVC = [[SearchHeaderCollecationView alloc]initWithFrame:CGRectMake(0, _sessionLable.bottom, ScreenSizeWidth, 0)];
    __weak typeof(self)weakSelf = self;
    [_searchHeaderVC setChangeText:^(NSString *text) {
//        _searchBar.showsCancelButton = YES;
//        for (UIView *subView in topView.subviews) {
//            
//            if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
//                
//                UIButton * cancelButton = (UIButton*)subView;
//                
//                [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
//                [cancelButton setTitleColor:[UIColor colorWithRed:212/255.0 green:56/255.0 blue:79/255.0 alpha:1] forState:UIControlStateNormal];
//                cancelButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
//                _cancelButton = cancelButton;
//            }
//        }
        
        __strong typeof(weakSelf)self = weakSelf;
        _searchBar.text = text;
//        [_searchBar becomeFirstResponder];
    }];
    //    _searchHeaderVC.backgroundColor = [UIColor redColor];
    [self addSubview:_searchHeaderVC];
}
//取消searchbar背景色
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
#pragma mark-UISearchBar-delegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"cs" to_page:@"" to_section:@"c" to_step:@"l" type:@"s" channel:@"" id:@""];
    
    
    [[LogHelper shared] writeToFilefrom_page:@"csh" from_section:@"c" from_step:@"h" to_page:@"csl" to_section:@"c" to_step:@"l" type:@"input" id:@"0" detail:@{@"keyword":searchBar.text}];
//    _searchBar.showsCancelButton = NO;
    self.keyword = searchBar.text;
    [searchBar resignFirstResponder];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=9&searchName=%@",INTERFACE_PREFIXD,API_URL_RECOMMENT_LIST,self.keyword];
    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"==%@",str );
    [[NSNotificationCenter defaultCenter] postNotificationName:@"searchContent" object:str];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    //    self.rightBtn.hidden = NO;
//    _searchBar.showsCancelButton = YES;
//    UIView *topView = _searchBar.subviews[0];
//    for (UIView *subView in topView.subviews) {
//        
//        if ([subView isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
//            
//            UIButton * cancelButton = (UIButton*)subView;
//            
//            [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
//            [cancelButton setTitleColor:[UIColor colorWithRed:212/255.0 green:56/255.0 blue:79/255.0 alpha:1] forState:UIControlStateNormal];
//            cancelButton.titleLabel.font = [UIFont systemFontOfSize:14.0];
//            _cancelButton = cancelButton;
//        }
//    }
    NSLog(@"---b---");
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
//    NSLog(@"---c---");
//    self.keyword = searchBar.text;
//    [searchBar resignFirstResponder];
//    
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10&searchName＝%@",INTERFACE_PREFIXD,API_URL_RECOMMENT_LIST,self.keyword];
//    str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"searchContents" object:str];
    //    self.rightBtn.hidden = YES;
//    if (_type==1) {
//        if (_searchDVC==nil) {
//            _searchDVC = [[SearchDetailViewcController alloc]initWithNibName:@"SearchDetailViewcController" bundle:nil];
//        }
//        [self.viewController presentViewController:_searchDVC animated:YES completion:^{
//            NSLog(@"搜素详情");
//        }];
//    }
    [searchBar resignFirstResponder];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
//    NSString *inputStr = searchText;
//    if ([inputStr length] == 0) {
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeScr" object:searchBar];
//        [searchBar resignFirstResponder];
//    }
}



- (void)setKeyArrays:(NSArray *)keyArrays{
    _keyArrays = keyArrays  ;
    //计算_searchdvc的高度
    _searchHeaderVC.keyArrays = _keyArrays;
    float height  = _searchBar.height + _sessionLable.height +_searchHeaderVC.getcollecationViewHeight;
    CGRect kframe = self.frame;
    kframe.size.height = height;
    self.frame = kframe;
}


@end
