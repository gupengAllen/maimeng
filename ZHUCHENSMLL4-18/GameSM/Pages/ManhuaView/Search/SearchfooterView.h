//
//  SearchfooterView.h
//  GameSM
//
//  Created by ABC on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LikereadConllectionView.h"
@interface SearchfooterView : UIView{
    UILabel *_likereadLable;
    LikereadConllectionView *_likereadVC;
}
@property(nonatomic,strong)LikereadConllectionView *likereadVC;
@property(nonatomic,strong)NSArray *likereadArrays;
@end
