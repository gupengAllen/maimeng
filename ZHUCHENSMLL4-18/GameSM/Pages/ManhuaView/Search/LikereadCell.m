//
//  LikereadCell.m
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "LikereadCell.h"
#import "UIViewExt.h"
#import "LikereadModel.h"
#import "Config.h"
#import "UIImageView+AFNetworking.h"

@interface LikereadCell ()
{
    UIImageView *_likeImgView;
    UILabel *_tuijianLable;
    
    NSMutableArray *_idArr;
    NSMutableArray *_nameArr;
}
@end

@implementation LikereadCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self creatView];
    }
    return self;
}

- (void)creatView{
    _likeImgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width * ___Scale)];
    _likeImgView.layer.cornerRadius = 2.0;
    _likeImgView.layer.masksToBounds = YES;
    [self addSubview:_likeImgView];
    
    _tuijianLable = [[UILabel alloc]initWithFrame:CGRectMake(0, _likeImgView.bottom + 2, _likeImgView.width, 20)];
    _tuijianLable.textColor = [UIColor blackColor];
    _tuijianLable.textAlignment = 0;
    _tuijianLable.font = [UIFont systemFontOfSize:12.0];
    [self addSubview:_tuijianLable];
}

  //填充数据
-(void)setModel:(LikereadModel *)model{
    _model = model;
    [_likeImgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",model.images]]];
    _tuijianLable.text = model.name;
}



@end
