//
//  SearchHeaderView.h
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchHeaderCollecationView.h"
#import "LikereadConllectionView.h"
@interface SearchHeaderView : UIView<UISearchBarDelegate>{
    UISearchBar *_searchBar;
    UILabel *_sessionLable;
    SearchHeaderCollecationView *_searchHeaderVC;
}
@property(nonatomic,strong)UISearchBar *searchBar;
@property(nonatomic,strong)UILabel *sessionLable;
@property(nonatomic,strong)SearchHeaderCollecationView *searchHeaderVC;
@property(nonatomic,strong)NSArray *keyArrays;
@property(nonatomic,strong)NSString *keyword;
@property(nonatomic,assign)int type;//0 正常 1 搜索有结果的 2 搜索没结果的
@end

