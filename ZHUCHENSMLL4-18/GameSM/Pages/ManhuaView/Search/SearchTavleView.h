//
//  SearchTavleView.h
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableView.h"
#import "BaseScrollView.h"
#import "SearchHeaderView.h"
#import "SearchfooterView.h"
@interface SearchTavleView : BaseScrollView

@property(nonatomic,strong)SearchHeaderView *searchHeaderView;
@property(nonatomic,strong)SearchfooterView *searchFooterView;

@property(nonatomic,strong)NSMutableArray *searchDataArr;
@property(nonatomic,strong)NSMutableArray *searchFootArr;

@property(nonatomic,strong)NSMutableDictionary *searchDataCount;

@end
