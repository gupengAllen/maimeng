//
//  SearchkeyCell.h
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchHeaderModel.h"

@interface SearchkeyCell : UICollectionViewCell
//{
////    UILabel *_keywordLable;
//    UIView *_keywordView;
//}

//@property(nonatomic,copy)NSString *keyword;
//UILabel *_keywordLable;
//UIView  *_keywordView;
@property(nonatomic,copy)SearchHeaderModel *model;

@property(nonatomic,copy)UILabel *keywordLable;
@property(nonatomic,copy)UIView *keyWordView;

@property(nonatomic,strong)NSArray *keywordArr;
@property(nonatomic,strong)NSString *idStr;
@property(nonatomic,strong)NSString *plistPath;


@end
