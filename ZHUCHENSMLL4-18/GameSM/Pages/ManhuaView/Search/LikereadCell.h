//
//  LikereadCell.h
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewExt.h"

@class LikereadModel;

@interface LikereadCell : UICollectionViewCell

@property(nonatomic,strong)LikereadModel *model;

@end
