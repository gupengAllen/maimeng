//
//  SearchHeaderCollecationView.m
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchHeaderCollecationView.h"
#import "SearchkeyCell.h"
#import "SearchHeaderView.h"
#import "SearchHeaderModel.h"
#import "Y_X_DataInterface.h"
#import "Config.h"

#define cellheight 40
#define cellWidth (([UIScreen mainScreen].bounds.size.width-50)/3.0)

@interface SearchHeaderCollecationView ()
{
    NSString *_identfy;
    NSMutableArray *_dataArr;
    SearchHeaderModel *_mb;
    
    UILabel *_keywordLable;
    UIView  *_keywordView;
    NSString *_plistPath;
}
@end

@implementation SearchHeaderCollecationView

- (instancetype)initWithFrame:(CGRect)frame{
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumInteritemSpacing = 0;
    layout.minimumLineSpacing = 10;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    layout.itemSize = CGSizeMake(cellWidth, cellheight);
    self.backgroundColor = [UIColor clearColor];
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self) {
        [self creatView];
        self.delegate = self;
        self.dataSource = self;
        self.pagingEnabled = YES;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator  = NO;
        //3.注册单元格
        _identfy = @"SearchkeyCell.h";
        [self registerClass:[SearchkeyCell class] forCellWithReuseIdentifier:_identfy];
    }
    return self;
}

- (void)creatView{
}

#pragma mark - UICollectionView delegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
        return _dataArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

        SearchkeyCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identfy forIndexPath:indexPath];
        cell.model = _dataArr[indexPath.row];
        return cell;
    
}


//点击事件，把关键字传入搜索栏，调用搜索的协议方法。
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[LogHelper shared] writeToFilefrom_page:@"csh" from_section:@"c" from_step:@"h" to_page:@"csl" to_section:@"c" to_step:@"l" type:@"click" id:@"0"];
        
        SearchHeaderModel *model = [[SearchHeaderModel alloc]init];
        model = _dataArr[indexPath.row];
        NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=9&searchName=%@",INTERFACE_PREFIXD,API_URL_RECOMMENT_LIST,model.keyword];
        _changeText(model.keyword);
        str = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"searchContent" object:str];
}

#pragma mark-set 方法计算高度

#pragma mark - 加载数据

-(void)setSearchDataArr:(NSMutableArray *)searchDataArr{
    _dataArr = [NSMutableArray array];
    _searchDataArr = searchDataArr;
    for (NSDictionary *dict in _searchDataArr) {
        _mb = [[SearchHeaderModel alloc]init];
        [_mb setValuesForKeysWithDictionary:dict];
        [_dataArr addObject:_mb];
        NSLog(@"%@",_mb.keyword);
    }
    _keyArrays = _dataArr;
    [self reloadData];
}
- (float)getcollecationViewHeight{
    int  count1 = 6/3;
    int count2 = 6%3;
    float height = ((count1+count2) *cellheight + (count1+1) * 10);
    [self reloadData];
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
    return height;
}

@end
