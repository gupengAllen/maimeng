//
//  LikereadConllectionView.h
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface LikereadConllectionView : UICollectionView<UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate>


@property(nonatomic,strong)NSArray *likeReadArr;

@property(nonatomic,strong)NSMutableArray *searchDataListArr;
@property(nonatomic,strong)NSMutableDictionary *searchDataCount;


- (float)getimageCollecationViewHeight;
@end
