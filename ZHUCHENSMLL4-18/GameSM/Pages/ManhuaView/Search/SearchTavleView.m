//
//  SearchTavleView.m
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "SearchTavleView.h"
#import "UIView+UIViewController.h"

#import "SearchHeaderView.h"
#import "SearchfooterView.h"
#import "LikereadConllectionView.h"
#import "UIViewExt.h"

#import "SearchHeaderModel.h"
#import "LikereadModel.h"

@interface SearchTavleView ()
{
    NSMutableArray *_titleArr;
    NSMutableArray *_imageArray;
}
@end

@implementation SearchTavleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    _titleArr = [NSMutableArray arrayWithCapacity:0];
    _imageArray = [NSMutableArray arrayWithCapacity:0];
    
    if (self) {
        [self creatView];
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator  = NO;
    }
    return self;
}

- (void)creatView{

    [self setSearchDataArr:_searchDataArr];
    [self setSearchFootArr:_searchFootArr];
    
    //2.创建头部视图
    _searchHeaderView = [[SearchHeaderView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, (_titleArr.count/3+1)*40+140)];//(_titleArr.count/3+1)*40)
    NSLog(@"%lu",_titleArr.count);
    [self addSubview:_searchHeaderView];
    
    //3.创建尾部视图
    _searchFooterView = [[SearchfooterView alloc]initWithFrame:CGRectMake(0, _searchHeaderView.bottom, self.bounds.size.width, (_imageArray.count/4+1)*300)];//(_searchFootArr.count/3+1)*120+90
    [self addSubview:_searchFooterView];
    
    
}

#pragma mark - 填充数据
-(void)setSearchDataArr:(NSMutableArray *)searchDataArr{
    _titleArr = [NSMutableArray array];
    _searchDataArr = searchDataArr;
    for (NSDictionary *dic in _searchDataArr) {
        SearchHeaderModel *model = [[SearchHeaderModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [_titleArr addObject:model.keyword];
    }
    _searchHeaderView.keyArrays = _titleArr;
}
-(void)setSearchFootArr:(NSMutableArray *)searchFootArr{
    _imageArray = [NSMutableArray array];
    _searchFootArr = searchFootArr;
    for (NSDictionary *dic in _searchDataArr) {
        LikereadModel *model = [[LikereadModel alloc]init];
        [model setValuesForKeysWithDictionary:dic];
        [_imageArray addObject:model];
    }
    _searchFooterView.likereadArrays = _imageArray;
    
    
    CGRect frame = _searchFooterView.frame;
    frame.origin.y = _searchHeaderView.height;
    _searchFooterView.frame = frame;
    [self addSubview:_searchFooterView];
    
    CGRect selfframe = self.frame;
    CGFloat height = _searchFooterView.height + _searchHeaderView.height;
    selfframe.size.height = height;
    self.frame = selfframe;
    self.contentOffset = CGPointMake(0,  _searchFooterView.height+_searchHeaderView.height-self.height);
    self.contentInset = UIEdgeInsetsMake(0, 0, 100, 0);
    
    
    
}



@end
