//
//  SearchresultsCollectionView.h
//  GameSM
//
//  Created by mac on 15/9/28.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseCollectionView.h"
#import "Config.h"
@interface SearchresultsCollectionView : BaseCollectionView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@end
