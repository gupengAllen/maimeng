//
//  CategoryCell.m
//  GameSM
//
//  Created by mac on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "CategoryCell.h"
#import "UIViewExt.h"
#import "CategoryDetailsModel.h"
#import "Config.h"

#import "AFNetworking.h"
#import "UIImageView+AFNetworking.h"


@implementation CategoryCell


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self ;
}
-(void)initData{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDiretory = [paths objectAtIndex:0];
    NSString *categoryFolder = [NSString stringWithFormat:@"%@/CategoryFolder",documentDiretory];
    self.plistPath = [categoryFolder stringByAppendingPathComponent:@"categoryID.plist"];
    NSString *namePath = [categoryFolder stringByAppendingPathComponent:@"categoryName.plist"];
    self.idArray = [NSMutableArray arrayWithContentsOfFile:self.plistPath];
    self.nameArray = [NSMutableArray arrayWithContentsOfFile:namePath];
}

#pragma mark-初始化
- (void)creatView{
    [_titleLabel removeFromSuperview];
    [_imgView removeFromSuperview];
    
    _imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height-26)];
    _imgView.userInteractionEnabled = NO;
    _imgView.backgroundColor = [UIColor clearColor];
    _imgView.layer.cornerRadius = _imgView.width/2;
    _imgView.layer.masksToBounds = YES;
    _imgView.layer.borderWidth = 1.0;
    _imgView.layer.borderColor = [[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1.0] CGColor];
    [self addSubview:_imgView];
    
    _titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, _imgView.bottom, self.bounds.size.width, 26)];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    _titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:_titleLabel];

    if(![[NSFileManager defaultManager] fileExistsAtPath:self.plistPath]){
        [_imgView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model.images]]];
        _titleLabel.text = _model.name;
    }else{
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSString *categoryFolder = [NSString stringWithFormat:@"%@/CategoryFolder",documentDiretory];
        NSString *imagePaths = [categoryFolder stringByAppendingPathComponent:self.idArray[_rowNum]];
        _imgView.image = [UIImage imageWithContentsOfFile:imagePaths];
        _titleLabel.text = self.nameArray[_rowNum];
    }
}

-(void)setModel:(CategoryDetailsModel *)model{
    _model = model;
    [self creatView];
}

-(void)setRowNum:(int)rowNum{
    _rowNum = rowNum;
    [self initData];
    [self creatView];
}

@end
