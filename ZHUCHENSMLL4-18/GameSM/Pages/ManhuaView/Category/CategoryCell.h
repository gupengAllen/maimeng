//
//  CategoryCell.h
//  GameSM
//
//  Created by mac on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CategoryDetailsModel;

@interface CategoryCell : UICollectionViewCell{
    UIImageView *_imgView;
//    UILabel *_titleLabel;
}
@property(nonatomic,strong)UILabel *titleLabel;
@property(nonatomic,strong)CategoryDetailsModel *model;

@property(nonatomic,strong)NSMutableArray *idArray;
@property(nonatomic,strong)NSMutableArray *nameArray;

@property(nonatomic,assign)int rowNum;
@property(nonatomic,strong)NSString *plistPath;

@end
