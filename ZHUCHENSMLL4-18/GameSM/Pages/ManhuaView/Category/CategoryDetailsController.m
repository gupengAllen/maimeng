//
//  CategoryDetailsController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "CategoryDetailsController.h"
#import "CategoryDetailsCell.h"
#import "CategoryDetailsModel.h"
#import "testViewController.h"
#import "AFNetworking.h"
#import "Y_X_DataInterface.h"

#import "EGORefreshTableHeaderView.h"


#import "CustomTool.h"
//#import "MJRefresh.h"
//#import "PullingRefreshTableView.h"


#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface CategoryDetailsController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,PullingRefreshTableViewDelegate,PSCollectionViewDelegate,PSCollectionViewDataSource, PullPsCollectionViewDelegate,EGORefreshTableHeaderDelegate, UIScrollViewDelegate>
{
    UIImageView     *_netImageView;
    UIButton    *_refreshBtn1;
    UIButton    *_refreshBtn2;
    UIButton    *_refreshBtn0;
    NSMutableArray *_dataArr;
    UILabel        *_nameLabel;
//    PullPsCollectionView *_collectionView;
    UINavigationItem *_item;
    NSString        *_nameStr;
    NSString        *_nameId;
    NSString        *_refreshUrl;
    
    int             _dataArrNum;
    
    EGORefreshTableHeaderView *_refreshHeaderView;
    BOOL _reloading;
    BOOL _isTopUrl;
}
@property(nonatomic,copy)PullPsCollectionView *collectionView;
@end

@implementation CategoryDetailsController

- (void)viewDidLoad {
    [super viewDidLoad];
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    [self loadNoti];
    [self setNavBar];
    _isTopUrl = NO;
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.minimumLineSpacing = 5;
//    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) collectionViewLayout:flowLayout];
//    
//    _collectionView.delegate = self;
//    _collectionView.dataSource = self;
//    _collectionView.backgroundColor = [UIColor whiteColor];

//    self.collectionView.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);

    
//    [_collectionView addHeaderWithTarget:self action:@selector(refreshData)];
//    [_collectionView addFooterWithTarget:self action:@selector(moreData)];
    
    [self.view addSubview:[self collectionView]];
//    [_collectionView registerClass:[CategoryDetailsCell class] forCellWithReuseIdentifier:@"detailscell"];
//    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableVIew"];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(misSelf)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
//-----
    _netImageView =[CustomTool createImageView];
    [self.view addSubview:_netImageView];
    
    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - KScreenheight, KScreenWidth, KScreenheight)];
        view.delegate = self;
        [_collectionView addSubview:view];
        _refreshHeaderView = view;
    }
    
}


#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)doneLoadingTableViewData{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_collectionView];
    [self refreshData];
}
//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:1.0];
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGSize size1 = _collectionView.contentSize;
    CGFloat maximumOffset = size1.height;
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    if (_collectionView.contentOffset.y>=maximumOffset - KScreenheight ) {
        [self moreData];
    }
}


- (PullPsCollectionView*)collectionView {
    if (!_collectionView) {
        _collectionView = [[PullPsCollectionView alloc] initWithFrame:CGRectMake(0, 64, ScreenSizeWidth,ScreenSizeHeight-64)];
        _collectionView.collectionViewDelegate = self;
        _collectionView.collectionViewDataSource = self;
        _collectionView.delegate = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.numColsPortrait = 3;
        _collectionView.numColsLandscape = 2;
        _collectionView.pullArrowImage = [UIImage imageNamed:@"blueArrow.png"];
        _collectionView.pullBackgroundColor = [UIColor clearColor];
        _collectionView.pullTextColor = [UIColor blackColor];
        
    }
    return _collectionView;
}


#pragma mark - PSCollectionViewDelegate
- (CategoryDetailsCell *)collectionView:(PSCollectionView *)collectionView viewAtIndex:(NSInteger)index
{
    CategoryDetailsCell *cell = (CategoryDetailsCell*)[collectionView dequeueReusableView];
    
//    if (!cell) {
        cell = [[CategoryDetailsCell alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth/3 -11, 30)];
    if (_isTopUrl) {
        if (index < 3) {
            UIImage *imageRingt = [UIImage imageNamed:@"paihangbang"];
            UIImageView *rightImage = [[UIImageView alloc] initWithFrame:CGRectMake(cell.width - imageRingt.size.width, 0, imageRingt.size.width, imageRingt.size.height)];
            rightImage.image = imageRingt;
            [cell addSubview:rightImage];
            
            UILabel *noLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, imageRingt.size.width/2, imageRingt.size.height/2)];
            noLabel.center = CGPointMake(imageRingt.size.width/4+2, 5);
            noLabel.text = @"NO";
            noLabel.textAlignment = 0;
            noLabel.font = [UIFont boldSystemFontOfSize:11.0];
            noLabel.textColor = [UIColor whiteColor];
            [rightImage addSubview:noLabel];
            
            UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(imageRingt.size.width - 12, imageRingt.size.height/2,  imageRingt.size.width/2, imageRingt.size.height)];
            numberLabel.text = [NSString stringWithFormat:@"%ld",(long)index + 1];
            numberLabel.textAlignment = 2;
            numberLabel.center = CGPointMake(imageRingt.size.width-11, imageRingt.size.height/2);
            
            numberLabel.font = [UIFont boldSystemFontOfSize:25.0];
            numberLabel.textColor = [UIColor whiteColor];
            [rightImage addSubview:numberLabel];
    }
    

    }
//    [cell.categoryImage setImageWithURL:[NSURL URLWithString:[[self.picArray objectAtIndex:index] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazai_bg.png"]];
//    
//    while ([cell.subviews lastObject] != nil) {
//        [(UIView *)[cell.contentView.subviews lastObject] removeFromSuperview];
//    }
    

    
    cell.model = _dataArr[index];
    return cell;
}

- (CGFloat)heightForViewAtIndex:(NSInteger)index
{
    return (KScreenWidth-24)/3*___Scale+20;
}

- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index
{
//    [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ctl" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:[_dataArr[index] id]];

//    PicDetailViewController *detailView = [[PicDetailViewController alloc] initWithNibName:@"PicDetailViewController" bundle:nil];
//    detailView.imageID = [[self.picArray objectAtIndex:index] objectForKey:@"id"];
//    detailView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//    [self presentViewController:detailView animated:YES completion:nil];
    
    NSString *str1 = @"cartoonBillBoard";
    
    NSString *str = @"cartoonCategory";
    
    //在str1这个字符串中搜索\n，判断有没有
    
    if ([_refreshUrl rangeOfString:str].location != NSNotFound) {
        
        [[LogHelper shared] writeToFilefrom_page:@"ccl" from_section:@"c" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:[_dataArr[index] id]];
    }
    
    if ([_refreshUrl rangeOfString:str1].location != NSNotFound) {
        [[LogHelper shared] writeToFilefrom_page:@"cbl" from_section:@"c" from_step:@"l" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:[_dataArr[index] id]];

    }

    
        testViewController *vc = [[testViewController alloc]init];
        [vc  setChangeCollection:^{
            
        }];
        vc.detailModel = _dataArr[index];
        vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
//        UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
    
        [self presentViewController:vc animated:YES completion:nil];
}

- (NSInteger)numberOfViewsInCollectionView:(PSCollectionView *)collectionView
{
    return _dataArr.count;
}

- (void)pullPsCollectionViewDidTriggerRefresh:(PullPsCollectionView *)pullTableView
{
    [self performSelector:@selector(refreshData) withObject:nil afterDelay:0.2f];
}

- (void)pullPsCollectionViewDidTriggerLoadMore:(PullPsCollectionView *)pullTableView
{

    [self performSelector:@selector(moreData) withObject:nil afterDelay:0.2f];
    
}



-(void)misSelf{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - MJ刷新代理

-(void)refreshData{
}


//if (urlArr.count == 5) {
//    _refreshUrl = [NSString stringWithFormat:@"%@=%@=%@=%@=%@",urlArr[0],urlArr[1],urlArr[2],urlArr[3],urlArr[4]];
//}else if (urlArr.count == 4){
//    _refreshUrl = [NSString stringWithFormat:@"%@=%@=%@=%@",urlArr[0],urlArr[1],urlArr[2],urlArr[3]];
//}
//

-(void)moreData{
////shang拉加载
    static int i=2;
    
    NSArray *strarr = [_refreshUrl componentsSeparatedByString:@"&"];
    NSMutableArray *urlArr = [NSMutableArray arrayWithCapacity:0];
    if (strarr.count == 4) {
        for (int j =0; j < strarr.count; j++) {
            if (j == 2) {
                [urlArr addObject:[NSString stringWithFormat:@"page=%d",i]];
            }else{
                [urlArr addObject:strarr[j]];}
        }
        //    [urlArr addObject:[NSString stringWithFormat:@"%d",24*i]];
        _refreshUrl = [NSString stringWithFormat:@"%@&%@&%@&%@",urlArr[0],urlArr[1],urlArr[2],urlArr[3]];
        
    }
    if (strarr.count == 3) {
        for (int j =0; j < strarr.count; j++) {
            if (j == 1) {
                [urlArr addObject:[NSString stringWithFormat:@"page=%d",i]];
            }else{
                [urlArr addObject:strarr[j]];}
        }
        //    [urlArr addObject:[NSString stringWithFormat:@"%d",24*i]];
        _refreshUrl = [NSString stringWithFormat:@"%@&%@&%@",urlArr[0],urlArr[1],urlArr[2]];
    }
#warning 12：51改
    if ([_refreshUrl rangeOfString:@"userContinueReadList"].location == NSNotFound) {
        [self refreshNewData];
        i ++;
    }
}

-(void)setNavBar{
    UIView *view = [CustomTool createNavView];
    [self.view addSubview:view];
    //    UINavigationBar *bar = self.navigationController.navigationBar;
    //    bar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
    ////    bar.tintColor = [UIColor whiteColor];
    //    _item = self.navigationItem;
    ////    _item.
    //    _item.titleView = _nameLabel;
    [view addSubview:_nameLabel];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(5, 20, 100, 38);//5, 25, 33, 33
    [backButton setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(10, 0, 0, 65)];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:backButton];
//    UINavigationBar *bar = self.navigationController.navigationBar;
//    bar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
////    bar.tintColor = [UIColor whiteColor];
//    _item = self.navigationItem;
////    _item.
//    _nameLabel = [[UILabel alloc]init];
//    _nameLabel.center = CGPointMake(ScreenSizeWidth/2, 10);
//    _nameLabel.size = CGSizeMake(200, 50);
//
//    _nameLabel.textAlignment = 1;
//    _nameLabel.font = [UIFont fontWithName:@"迷你简菱心" size:20.0];
//    _nameLabel.textColor = [UIColor whiteColor];
//    _item.titleView = _nameLabel;
//    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    backButton.frame = CGRectMake(0, 0, 33, 33);
//    [backButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
//    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
//    
//    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
//    self.navigationItem.leftBarButtonItem = backBtn;
}
#pragma mark - 点击事件
-(void)misBack{
    
//    _collectionView.pullDelegate = nil;
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - collection代理
//-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
//    return 1;
//}
//-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
//    return _dataArr.count;
//}
//-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *identify = @"detailscell";
//    CategoryDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
//    if (!cell) {
//    }
//    cell.model = _dataArr[indexPath.row];
//    return cell;
//}
//
//#pragma mark -
//-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
////    CGRect r = [UIScreen mainScreen].applicationFrame;
////    float height = r.size.width;
//    return CGSizeMake((self.view.bounds.size.width-60)/3.0, 150);//self.view.bounds.size.width
//}
//-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
//    return UIEdgeInsetsMake(10, 10, 20, 10);
//}
//
//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    testViewController *vc = [[testViewController alloc]init];
//    vc.detailModel = _dataArr[indexPath.row];
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:vc];
//    
////    [self presentViewController:nac animated:YES completion:nil];
//    [self.navigationController pushViewController:vc animated:YES];
////    testViewController *vc = [[testViewController alloc]init];
////    UINavigationController *nvc = [[UINavigationController alloc]initWithRootViewController:vc];
////    [self presentViewController:nvc animated:YES completion:^{
////        [[NSNotificationCenter defaultCenter] postNotificationName:@"DETAILS" object:_dataArr[indexPath.row]];
////    }];
//}

#pragma mark - 通知&加载数据
-(void)loadNoti{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUrl:) name:@"topDetail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUrl:) name:@"categoryDetail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getUrl:) name:@"searchKeyword" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(YXGetUrl:) name:@"continueLOL" object:nil];
}

- (void)YXGetUrl:(NSNotification *)noti{
    [self showHudToView:self.view text:@""];
    [YK_API_request startLoad:noti.object extraParams:nil object:self action:@selector(continueLOL:) method:GETDATA];
}
- (void)continueLOL:(NSDictionary *)continueDict{
    NSArray *arr = continueDict[@"results"];
    for (NSDictionary *dict in arr) {
        CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
        [md setValuesForKeysWithDictionary:dict];
        [_dataArr addObject:md];
    }
    [self hideHudFromView:self.view];
    [_collectionView reloadData];

}

-(void)refreshNewData{
    _netImageView.alpha = 0;
    [self showHudToView:self.view text:@"一大波麦萌军正在拼命加载..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:_refreshUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        [_dataArr removeAllObjects];
        
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
            [md setValuesForKeysWithDictionary:dict];
            [_dataArr addObject:md];
        }
        [self hideHudFromView:self.view];
        [_collectionView reloadData];
//        [_collectionView footerEndRefreshing];
//        _collectionView.pullLastRefreshDate = [NSDate date];
        
        _collectionView.pullTableIsLoadingMore = NO;
        _collectionView.pullTableIsRefreshing  = NO;
        
        if (_dataArr.count != _dataArrNum) {
            _dataArrNum = _dataArr.count;
        }else{
            UILabel *tipView = [[UILabel alloc]initWithFrame:CGRectMake(ScreenSizeWidth/2-145, ScreenSizeHeight - 150, 290, 45)];
            if ([self.type isEqualToString:@"top"]) {
                tipView.text = @"亲，没有更多漫画进入该榜单了哦~";
            }else{
                tipView.text = @"亲，没有更多漫画进入该类别了哦~";
            }
            
            tipView.layer.masksToBounds = YES;
            tipView.textAlignment = 1;
            tipView.textColor = [UIColor whiteColor];
            tipView.layer.cornerRadius = 22.5;
            tipView.backgroundColor = [UIColor blackColor];
            tipView.alpha = 1;
            [self.view addSubview:tipView];
            [self performSelector:@selector(missTipView:) withObject:tipView afterDelay:1.0];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideHudFromView:self.view];
        [_refreshBtn1 removeFromSuperview];
        [_refreshBtn2 removeFromSuperview];
        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight -64);
        _netImageView.alpha = 1.0;
        _refreshBtn0 = [CustomTool createBtn];
        _refreshBtn0.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 64 - 80, 100, 30);
        [_netImageView addSubview:_refreshBtn0];
        [_refreshBtn0 addTarget:self action:@selector(refreshNewData) forControlEvents:UIControlEventTouchUpInside];
    }];
}
-(void)missTipView:(UILabel *)label{
    [label removeFromSuperview];
}
//_collectionView.pullLastRefreshDate = [NSDate date];
//_collectionView.pullTableIsRefreshing = NO;
//_collectionView.pullTableIsLoadingMore = NO;
- (void)loadNewData{
    _netImageView.alpha = 0;
    
    [self showHudToView:self.view text:@"一大波麦萌军正在拼命加载..."];
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:_refreshUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
                [_dataArr removeAllObjects];
        
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
            [md setValuesForKeysWithDictionary:dict];
            [_dataArr addObject:md];
        }
        [self hideHudFromView:self.view];
        [_collectionView reloadData];
        //        [_collectionView footerEndRefreshing];
//        _collectionView.pullLastRefreshDate = [NSDate date];
        
//        _collectionView.pullTableIsLoadingMore = NO;
//        _collectionView.pullTableIsRefreshing  = NO;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self hideHudFromView:self.view];
        [_refreshBtn0 removeFromSuperview];
        [_refreshBtn2 removeFromSuperview];
        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight -64);
        _netImageView.alpha = 1.0;
        _refreshBtn1 = [CustomTool createBtn];
        _refreshBtn1.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 64 - 80, 100, 30);
        [_netImageView addSubview:_refreshBtn1];
        [_refreshBtn1 addTarget:self action:@selector(loadNewData) forControlEvents:UIControlEventTouchUpInside];
    }];

}

-(void)getUrl:(NSNotification *)noti{
    _refreshUrl = noti.object;
    
    NSString *str1 = @"cartoonBillBoard";
    
    NSString *str = @"cartoonCategory";
    
    if ([_refreshUrl rangeOfString:str].location != NSNotFound) {
        
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"cbl" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
    }
    
    if ([_refreshUrl rangeOfString:str1].location != NSNotFound) {
        [[LogHelper shared] writeToFilefrom_page:@"crh" from_section:@"c" from_step:@"h" to_page:@"ccl" to_section:@"c" to_step:@"a" type:@"" id:@"0"];
        
    }

    
    [self loadData];
    
    //NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_RECOMMENTLU_LIST];_isTopUsrStr//SetListByBillBoard
}
-(void)loadData{
    
    
//    _refreshUrl = noti.object;
//    NSString *str = [NSString alloc]initWithData:[NSDate dataWithContentsOfURL:str1] encoding:nil;
    NSString *str1 = [_refreshUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    _refreshUrl = str1;
    
    if ([_refreshUrl rangeOfString:@"SetListByBillBoard"].location != NSNotFound)  {
        _isTopUrl = YES;
    }
    
    
    _netImageView.alpha = 0;
//---分割网址
   NSArray *strarr = [_refreshUrl componentsSeparatedByString:@"="];
//    if ([strarr[1] isEqualToString:@"cartoonSet/userContinueReadList&page"]) {
//        _nameLabel.text = @"继续撸";
//    }
//    if ([strarr[1] isEqualToString:@"cartoonSet/recentUpdateBillBoard&page"]) {
//        _nameLabel.text = @"最新更新";
//    }
//    if ([strarr[1] isEqualToString:@"cartoonSet/hitBillBoard&page"]) {
//        _nameLabel.text = @"最多点击";
//    }
//    if ([strarr[1] isEqualToString:@"cartoonBillBoard/collectionBillBoard&page"]) {
//        _nameLabel.text = @"最多收藏";
//    }
    
    [self showHudToView:self.view text:@"一大波麦萌军正在拼命加载..."];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:_refreshUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        if (backData[@"params"]) {
            if ([strarr[1] isEqualToString:@"cartoonBillBoard/getCartoonSetListByBillBoard&id"]) {
                _nameId = backData[@"params"][@"id"];
                [self loadTopName];
            }else if ([strarr[1] isEqualToString:@"cartoonCategory/getCartoonSetListByCategory&id"]){
                _nameId = backData[@"params"][@"id"];
                [self loadName];
            }
        }
        for (NSDictionary *dict in arr) {
            CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
            [md setValuesForKeysWithDictionary:dict];
            [_dataArr addObject:md];
        }
        [self hideHudFromView:self.view];
        [_collectionView reloadData];
        _dataArrNum = _dataArr.count;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [self hideHudFromView:self.view];
        [_refreshBtn0 removeFromSuperview];
        [_refreshBtn1 removeFromSuperview];
        _netImageView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight -64);
        _netImageView.alpha = 1.0;
        _refreshBtn2 = [CustomTool createBtn];
        _refreshBtn2.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 64 - 80, 100, 30);
        [_netImageView addSubview:_refreshBtn2];
        [_refreshBtn2 addTarget:self action:@selector(loadData) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (void)setTitleName:(NSString *)titleName {
    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 30, KScreenWidth-120, 30)];
    _nameLabel.center = CGPointMake(ScreenSizeWidth/2, 42);
    //    _nameLabel.size = CGSizeMake(200, 50);
    
    _nameLabel.textAlignment = 1;
    _nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20.0];
    _nameLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];

    _nameLabel.text = titleName;
}

-(void)dealloc{

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"topDetail" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"categoryDetail" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchKeyword" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"nameget" object:nil];
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchContents" object:nil];
}
-(void)loadTopName{
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_TOP_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            if ([_nameId isEqualToString:dict[@"id"]]) {
//                _nameLabel.text =dict[@"name"];
                //                _item.title =
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}
-(void)loadName{
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_CATEGORY_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            if ([_nameId isEqualToString:dict[@"id"]]) {
//                _nameLabel.text =dict[@"name"];
//                _item.title =
            }
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
    
}


-(void)showHudToView:(UIView *)view text:(NSString *)text{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    hud.labelText = text;
}

-(void)hideHudFromView:(UIView *)view{
    [MBProgressHUD hideAllHUDsForView:view animated:YES];
}

//- (BOOL)shouldAutorotate {
//    return YES;
//}
//
//- (NSUInteger)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskPortrait;
//}
//- (NSDate *)pullingTableViewRefreshingFinishedDate
//{
//    return [NSDate date];
//}

- (BOOL)shouldAutorotate {
    
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

#pragma mark --刷新
//- (void)pullingCollectionViewDidStartRefreshing:(PullingRefreshTableView *)tableView
//{
//    [self performSelector:@selector(refreshData) withObject:nil afterDelay:0.2f];
//    
//}
//
//- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView
//{
//    [self performSelector:@selector(moreData) withObject:nil afterDelay:0.2f];
//}

-(BOOL)canBecomeFirstResponder{
    return YES;
}


@end
