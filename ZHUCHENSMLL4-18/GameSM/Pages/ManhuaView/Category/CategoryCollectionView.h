//
//  CategoryCollectionView.h
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseCollectionView.h"

@interface CategoryCollectionView : BaseCollectionView<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property(nonatomic,strong)NSMutableArray *categoryDataArr;
@property(nonatomic,strong)NSMutableDictionary *categoryDataCount;

@property(nonatomic,strong)NSMutableArray *idArray;
@property(nonatomic,strong)NSMutableArray *nameArray;

-(void)initData;

@end
