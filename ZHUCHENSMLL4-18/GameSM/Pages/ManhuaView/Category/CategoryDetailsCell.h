//
//  CategoryDetailsCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CategoryDetailsModel;
@class BookModel;
@class CartoonInfoAll;
@interface CategoryDetailsCell : UICollectionViewCell

@property(nonatomic,strong)CategoryDetailsModel *model;
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UIImageView *redImageView;
@property(nonatomic,strong)UILabel     *titleLabel;
@property(nonatomic,strong)UILabel     *updataLabel;
@property(nonatomic,copy)CartoonInfoAll *bookModel;
@property(nonatomic,copy)UIButton *add;
@property(nonatomic,assign)BOOL isSelected;
@end
