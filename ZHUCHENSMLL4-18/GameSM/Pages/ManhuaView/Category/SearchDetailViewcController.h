//
//  SearchDetailViewcController.h
//  GameSM
//
//  Created by mac on 15/9/24.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableViewController.h"
#import "SearchresultsCollectionView.h"
@interface SearchDetailViewcController : BaseTableViewController{
    
    __weak IBOutlet UIButton *_cancelButton;
    __weak IBOutlet UISearchBar *_searchBar;
    __weak IBOutlet UILabel *_resultLabel;
    SearchresultsCollectionView *_searchresultsCollectionView;
    
}

@end
