//
//  CategoryCollectionView.m
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "CategoryCollectionView.h"
#import "Y_X_DataInterface.h"

#import "CategoryCell.h"
#import "CategoryDetailsModel.h"
#import "DetailsViewController.h"



#define kCategoryInteritemSpacing 10
#define kCategoryLineSpacing      10

@interface CategoryCollectionView ()
{
    NSString *_identfy;
    NSMutableArray *_dataArr;
    NSMutableArray *_idNum;
    NSMutableArray *_nameArr;
    
    NSString        *_plistPath;
}
@end

@implementation CategoryCollectionView

- (instancetype)initWithFrame:(CGRect)frame{
    _idNum = [[NSMutableArray alloc] init];
    _nameArr = [[NSMutableArray alloc] init];
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    
    //1.创建布局对象
    UICollectionViewFlowLayout *Layout = [[UICollectionViewFlowLayout alloc]init];
    Layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    Layout.minimumInteritemSpacing = kCategoryInteritemSpacing;
    Layout.minimumLineSpacing = kCategoryLineSpacing;
    Layout.sectionInset = UIEdgeInsetsMake(10, kCategoryInteritemSpacing, 0, 10);
    Layout.itemSize = CGSizeMake(kCategoryCellWidth, kCategoryCellWidth+30);
    self.backgroundColor = [UIColor clearColor];
    
    self = [super initWithFrame:frame collectionViewLayout:Layout];
    if (self) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentDiretory = [paths objectAtIndex:0];
        NSString *categoryFolder = [NSString stringWithFormat:@"%@/CategoryFolder",documentDiretory];
        _plistPath = [categoryFolder stringByAppendingPathComponent:@"categoryID.plist"];
//        [self initData];
        self.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - 90);
        //2.设置代理
        self.delegate = self;
        self.dataSource = self;
        self.showsHorizontalScrollIndicator = NO;
        self.showsVerticalScrollIndicator   = NO;
        
        //3.注册单元格类型
        _identfy = @"CategoryCell";
        [self registerClass:[CategoryCell class] forCellWithReuseIdentifier:_identfy];
        
    }
    
    return self;
}


-(void)initData{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDiretory = [paths objectAtIndex:0];
    NSString *categoryFolder = [NSString stringWithFormat:@"%@/CategoryFolder",documentDiretory];
    _plistPath = [categoryFolder stringByAppendingPathComponent:@"categoryID.plist"];
    NSString *namePath = [categoryFolder stringByAppendingPathComponent:@"categoryName.plist"];
    self.idArray = [NSMutableArray arrayWithContentsOfFile:_plistPath];
    self.nameArray = [NSMutableArray arrayWithContentsOfFile:namePath];
}


#pragma mark UICollectionView
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(_dataArr.count != 0){
        return _dataArr.count;
    }else{
        return self.idArray.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //3.注册单元格类型
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identfy forIndexPath:indexPath];
    if(_dataArr.count != 0){
        cell.model = _dataArr[indexPath.row];
    }else{
        cell.rowNum = (int)[indexPath row];
    }
    return cell;
}

//单元格的点击事件,跳转到下个界面
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = [indexPath row];
    if(_dataArr.count != 0){
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_CATEGORY_DETAL,_idNum[row]];
        NSArray *titleStr = @[str,_nameArr[row]];
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ct" to_page:@"ctl" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:_idNum[indexPath.row]];
        [[LogHelper shared] writeToFilefrom_page:@"cch" from_section:@"c" from_step:@"h" to_page:@"ccl" to_section:@"c" to_step:@"l" type:@"" id:_idNum[row]];
        
        [[NSNotificationCenter defaultCenter]postNotificationName:@"categoryDetails" object:titleStr];
    }else{
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_CATEGORY_DETAL,self.idArray[row]];
        NSArray *titleStr = @[str,self.nameArray[row]];
//        [[LogHelper shared] writeToFileDt:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]] from_page:@"ct" to_page:@"ctl" to_section:@"c" to_step:@"d" type:@"" channel:@"" id:self.idArray[row]];
        [[LogHelper shared] writeToFilefrom_page:@"cch" from_section:@"c" from_step:@"h" to_page:@"ccl" to_section:@"c" to_step:@"l" type:@"" id:self.idArray[row]];

        [[NSNotificationCenter defaultCenter]postNotificationName:@"categoryDetails" object:titleStr];
    }
    
    
}

-(void)setCategoryDataArr:(NSMutableArray *)categoryDataArr{
    _dataArr = [NSMutableArray array];
    _nameArr = [NSMutableArray array];
    _idNum = [NSMutableArray array];
    _categoryDataArr = categoryDataArr;
    if(![[NSFileManager defaultManager] fileExistsAtPath:_plistPath]){
        for (NSDictionary *dic in _categoryDataArr) {
            CategoryDetailsModel *model = [[CategoryDetailsModel alloc]init];
            [model setValuesForKeysWithDictionary:dic];
            [_dataArr addObject:model];
            [_nameArr addObject:model.name];
            [_idNum addObject:model.id];
        }
    }else{
        [self initData];
    }

    [self reloadData];
}


@end
