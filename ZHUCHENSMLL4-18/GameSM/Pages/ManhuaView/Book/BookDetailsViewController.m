//
//  BookDetailsViewController.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/22.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "BookDetailsViewController.h"
#import "CategoryDetailsCell.h"
#import "CategoryDetailsModel.h"
#import "testViewController.h"
#import "AFNetworking.h"
#import "Y_X_DataInterface.h"
#import "BookModel.h"
#import "Config.h"
#import "CartoonInfoAll.h"
#import "DataBaseHelper.h"
#import "NSDate+OTS.h"
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@interface BookDetailsViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    NSMutableArray *_dataArr;
    UILabel        *_nameLabel;
    UICollectionView *_collectionView;
    UINavigationItem *_item;
    NSString        *_nameStr;
    NSString        *_nameId;
    NSString        *_urlStr;
    NSString        *_plistPath;
//    NSMutableArray  *_rootArray;
    BOOL isDeleteCommic;
    UIButton            *onOff;
    UIButton            *searchContents;
    UIView              *bottomView;
    NSMutableArray *_deleteArr;
    NSMutableDictionary *_chooseButtonDict;
    NSMutableArray *_dataSourceArr;
    UIImageView     *_bookTipImageView;

}

@end

@implementation BookDetailsViewController


-(void)createBookImageView{
    _bookTipImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, ScreenSizeHeight - 64)];
    _bookTipImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:_bookTipImageView];
    
    _bookTipImageView.hidden = YES;
}

- (void)updateInfo {
    
    if ([[NSDate date] distanceNowDays]%2 == 0) {
        NSMutableArray *arrInfo = [[DataBaseHelper shared] fetchCartoonAllInfo];

        NSMutableString *strInfo = [NSMutableString string];
        for (int i = 0; i < arrInfo.count; i ++) {
            [strInfo appendString:arrInfo[i]];
            if (i < arrInfo.count - 1) {
                [strInfo appendString:@","];
            }
        }
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UPDATEINFO"]){
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]-10000] forKey:@"UPDATEINFO"];
    //        [[NSUserDefaults standardUserDefaults] setObject: ]) forKey:@"UPDATEINFO"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        if (strInfo.length > 0) {
            [YK_API_request startGetLoad:INTERFACE_PREFIXD extraParams:@{@"r":API_URL_UPDATEINFO,@"ids":strInfo,@"lastSynTime":[[NSUserDefaults standardUserDefaults] objectForKey:@"UPDATEINFO"]} object:self action:@selector(updateInfoClick:)];
        }
    }
    
}

- (void)updateInfoClick:(NSDictionary *)updateDict {
    NSArray *updateInfoArr = updateDict[@"results"];
        
    //获取本地沙盒路径
    NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    //获取完整路径
    NSString *documentsPath = [path objectAtIndex:0];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"oldUpdateTime.plist"];
    if (![fm fileExistsAtPath:plistPath]) {
        [fm createFileAtPath:plistPath contents:nil attributes:nil];
    }
    NSMutableArray *arr = [NSMutableArray array];
    [arr writeToFile:plistPath atomically:YES];
    
    
    for (int i = 0; i < updateInfoArr.count; i ++) {
        [[DataBaseHelper shared] updateCartoonInfoCodition:updateInfoArr[i][@"id"] andValue:@{@"updateInfo":[NSString stringWithFormat:@"'%@'", updateInfoArr[i][@"updateInfo"]]}];
        NSString *idstr = [NSString stringWithFormat:@"%@",updateInfoArr[i][@"id"]];
        [arr addObject:idstr];
        [arr writeToFile:plistPath atomically:NO];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self createBookImageView];
    // Do any additional setup after loading the view from its nib.
    _dataArr = [NSMutableArray arrayWithCapacity:0];
    _deleteArr = [NSMutableArray array];
    _chooseButtonDict = [NSMutableDictionary dictionary];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0, 0, 33, 33);
    [backButton setImage:[UIImage imageNamed:@"fanhui"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *backBtn = [[UIBarButtonItem alloc]initWithCustomView:backButton];
    self.navigationItem.leftBarButtonItem = backBtn;
    
    [self loadNoti];
    [self setNavBar];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.minimumLineSpacing = 5;
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height-64) collectionViewLayout:flowLayout];
    
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:_collectionView];
    [_collectionView registerClass:[CategoryDetailsCell class] forCellWithReuseIdentifier:@"detailscell"];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"reusableVIew"];
    
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, ScreenSizeHeight - 110, ScreenSizeWidth, 50)];
    bottomView.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    bottomView.backgroundColor = [UIColor clearColor];
    bottomView.hidden = YES;
    [self.view addSubview:bottomView];
    
    onOff = [UIButton buttonWithType:UIButtonTypeCustom];
    onOff.frame = CGRectMake(5, 0, (ScreenSizeWidth - 15)/2, 40);
    onOff.titleLabel.font = [UIFont systemFontOfSize:13.0];
    onOff.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    onOff.layer.cornerRadius = 4.0;
    [onOff setTitle:@"全选" forState:UIControlStateNormal];
    [onOff setTitle:@"取消全选" forState:UIControlStateSelected];
    [onOff setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [onOff addTarget:self action:@selector(wholeOnOff:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:onOff];
    
    
    searchContents = [UIButton buttonWithType:UIButtonTypeCustom];
    searchContents.frame = CGRectMake((ScreenSizeWidth - 15)/2 + 10, 0,(ScreenSizeWidth - 15)/2, 40);
    searchContents.titleLabel.font = [UIFont systemFontOfSize:13.0];
    searchContents.backgroundColor = RGBACOLOR(234, 74, 97, 1.0);
    searchContents.layer.cornerRadius = 4.0;
    [searchContents setTitle:@"删除" forState:UIControlStateNormal];
    [searchContents setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [searchContents addTarget:self action:@selector(searchContentBtn:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:searchContents];
    
     [self.view sendSubviewToBack:_collectionView];
    
    
    [self updateInfo];
}


- (void)wholeOnOff:(UIButton *)onOffBtn {
    onOffBtn.selected = !onOffBtn.selected;
    if (onOffBtn.selected) {
//        for (UIButton *chooseButton in _chooseButtonDict.allValues) {
//            chooseButton.selected = YES;
//            [_deleteArr addObject:@(chooseButton.tag)];
//        }
        
        [_dataSourceArr removeAllObjects];
        for (int i = 0; i < _bookArr.count; i ++) {
            [_dataSourceArr addObject:@(1)];
            [_deleteArr addObject:@(i)];
        }
        [_collectionView reloadData];
        
    }else{
//        for (UIButton *chooseButton in _chooseButtonDict.allValues) {
//            chooseButton.selected = NO;
//            [_deleteArr removeObject:@(chooseButton.tag)];
//        }
        [_dataSourceArr removeAllObjects];
        for (int i = 0; i < _bookArr.count; i ++) {
            [_dataSourceArr addObject:@(0)];
            
        }
        [_deleteArr removeAllObjects];
        [_collectionView reloadData];

    }
}

- (void)searchContentBtn:(UIButton *)searchContentBtn{
    searchContentBtn.selected = !searchContentBtn.selected;
    if (searchContentBtn.selected) {
        
        NSMutableArray *indexPaths = [NSMutableArray array];
        NSMutableIndexSet *indexSet = [[NSMutableIndexSet alloc] init];
        NSMutableArray *cartoonIdArr = [NSMutableArray array];
        for(int i = 0;i < _deleteArr.count;i ++){
            NSInteger chooseChapInt = [_deleteArr[i] integerValue];
            [indexSet addIndex:[_deleteArr[i] integerValue]];
            [indexPaths addObject:[NSIndexPath indexPathForRow:chooseChapInt inSection:0]];
            
            [cartoonIdArr addObject:_bookArr[chooseChapInt]];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDirectory = [paths objectAtIndex:0];
//            
//            NSFileManager *fileMgr = [NSFileManager defaultManager];
//            NSString *MapLayerDataPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",[_bookArr[chooseChapInt] id]]];
//            BOOL bRet = [fileMgr fileExistsAtPath:MapLayerDataPath];
//            if (bRet) {
//                NSError *err;
//                [fileMgr removeItemAtPath:MapLayerDataPath error:&err];
//            }
        }
        
        for (int j = 0;j < cartoonIdArr.count ; j ++) {
            NSLog(@"cartoonIdArrCount%lu",(unsigned long)cartoonIdArr.count);
            [[DataBaseHelper shared] insertExistCartoonInfo:cartoonIdArr[j] andValue:@{@"collectionStatus":@"0",@"collectTime": [NSString stringWithFormat:@"%.f",[[NSDate date] timeIntervalSince1970]]}];
       
        }

        
//            [[DataBaseHelper shared] deleteCartoonId:cartoonIdArr[j]];
//            [[DataBaseHelper shared] deleteChapterId:cartoonIdArr[j]];
        [_bookArr removeObjectsAtIndexes:indexSet];
        [_dataSourceArr removeObjectsAtIndexes:indexSet];
        //        [self.downCollectionView reloadData];
        [_collectionView deleteItemsAtIndexPaths:indexPaths];
        [_deleteArr removeAllObjects];
        
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"changeSave" object:nil];
    }
}


- (void)loadNoti{
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deleteCommic:) name:@"DELETECOMMIC" object:nil];
}

- (void)deleteCommic:(NSNotification *)noti {
    isDeleteCommic = !isDeleteCommic;
    
    if(isDeleteCommic){
//        [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:5.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//            bottomView.frame = CGRectMake(0, ScreenSizeHeight - 100, ScreenSizeWidth, 40);
//        } completion:^(BOOL finished) {
//            
//        }];
        bottomView.hidden = NO;
        
    }else{
//        [UIView animateWithDuration:1.0 delay:0 usingSpringWithDamping:5.0 initialSpringVelocity:5.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//            bottomView.frame = CGRectMake(0, ScreenSizeHeight - 60, ScreenSizeWidth, 40);
//        } completion:^(BOOL finished) {
//            
//        }];
        
        bottomView.hidden = YES;
//        for (UIButton *button in _chooseButtonDict.allValues) {
//            [button removeFromSuperview];
//        }
//        
//        [_chooseButtonDict removeAllObjects];

    }
    [_collectionView reloadData];

    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DELETECOMMIC" object:nil];
}


- (void)setRootArray:(NSMutableArray *)rootArray {
    
    if (rootArray) {
        _rootArray = rootArray;
    }else {
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //获取完整路径
        NSString *documentsPath = [path objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"oldUpdateTime.plist"];
        _rootArray = [NSMutableArray arrayWithContentsOfFile:plistPath];

    }
}

- (void)setBookArr:(NSMutableArray *)bookArr{
    _bookArr = [bookArr mutableCopy];
    if (self.view.frame.origin.x >= ScreenSizeWidth) {
        if (_bookArr.count == 0) {
            _bookTipImageView.image = [UIImage imageNamed:@"shoucangTip"];
            _bookTipImageView.hidden = NO;
        }else{
            _bookTipImageView.hidden = YES;
        }
    }else{
        if (_bookArr.count == 0) {
            _bookTipImageView.image = [UIImage imageNamed:@"jisuluTip"];
            _bookTipImageView.hidden = NO;
        }else{
            _bookTipImageView.hidden = YES;
        }
    }
    _dataSourceArr = [NSMutableArray array];
    for (int i = 0; i < _bookArr.count; i ++) {
        [_dataSourceArr addObject:@(0)];
    }
    
}

//- (void)setTitleName:(NSString *)titleName{
//    UINavigationBar *bar = self.navigationController.navigationBar;
//    bar.barTintColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];    //元素项
////    bar.tintColor = [UIColor whiteColor];
//    
//    //    _item.
//    _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30,self.view.bounds.size.width-120, 25)];
//    _nameLabel.textAlignment = 1;
//    _nameLabel.font = [UIFont fontWithName:@"迷你简菱心" size:20.0];
//    _nameLabel.textColor = [UIColor whiteColor];
//    //    _nameLabel.text = @"hello";
//    self.navigationItem.titleView = _nameLabel;
//
//    _nameLabel.text = titleName;
//}

-(void)setNavBar{
    
}

#pragma mark - 点击事件
-(void)misBack{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - collection代理
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _bookArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"detailscell";
    CategoryDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    
//
    
    cell.bookModel = _bookArr[indexPath.row];
    cell.redImageView.hidden = YES;
    
    if (isDeleteCommic) {
//        UIButton *add = [UIButton buttonWithType:UIButtonTypeCustom];
//        add.frame = CGRectMake(cell.width - 40, 0, 30, 30);
//        [add setImage:[UIImage imageNamed:@"recipe_normal"] forState:UIControlStateNormal];
//        [add setImage:[UIImage imageNamed:@"recipe_add"] forState:UIControlStateSelected];
//        [add addTarget:self action:@selector(addDeleteBtn:) forControlEvents:UIControlEventTouchUpInside];
//        add.tag = indexPath.row;
//        [cell addSubview:add];
        
//        [_chooseButtonDict setObject:add forKey:@(indexPath.row)];

        cell.add.hidden = NO;
        cell.add.selected =  [_dataSourceArr[indexPath.row] boolValue];
    }else{
        cell.add.hidden = YES;

    }
//        downListCollectionCell.isCommicHidden = isDeleteCommic;
    
    
    int row = (int)[indexPath row];
    cell.tag = row;
    for (NSString *str in _rootArray) {
        if ([cell.bookModel.id isEqualToString:str]) {
            cell.redImageView.hidden = NO;
        }
    }
    
    return cell;
}

- (void)addDeleteBtn:(UIButton *)addDeleteBtn {
    addDeleteBtn.selected = !addDeleteBtn.selected;
    CategoryDetailsCell *cell = (CategoryDetailsCell *)addDeleteBtn.superview;
    NSInteger row = [[_collectionView indexPathForCell:cell] row];
    if (addDeleteBtn.selected) {
        [_deleteArr addObject:@(row)];
    }else {
        [_deleteArr removeObject:@(row)];
    }

}


#pragma mark -
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    //    CGRect r = [UIScreen mainScreen].applicationFrame;
    //    float height = r.size.width;
    return CGSizeMake((KScreenWidth-24)/3.0, (KScreenWidth-24)/3.0*___Scale+20+15);//self.view.bounds.size.width
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(4, 4, 4, 4);
}
////http://apitest.playsm.com/index.php?r=cartoonSet/detail&id=51
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    //重用cell
//    CategoryDetailsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"detailscell"forIndexPath:indexPath];
//    //赋值
////    UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
////    UILabel *label = (UILabel *)[cell viewWithTag:2];
////    NSString *imageName = [NSString stringWithFormat:@"%ld.JPG",(long)indexPath.row];
////    imageView.image = [UIImage imageNamed:imageName];
////    label.text = imageName;
//    
//    cell.backgroundColor = [UIColor redColor];
//    return cell;
//    
//}
//// The view that is returned must be retrieved from a call to -dequeueReusableSupplementaryViewOfKind:withReuseIdentifier:forIndexPath:
////定义每个UICollectionViewCell 的大小
//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    return CGSizeMake(60, 80);
//}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (isDeleteCommic) {
         CategoryDetailsCell *cateCell =  (CategoryDetailsCell *)[collectionView cellForItemAtIndexPath:indexPath];
        cateCell.isSelected = !cateCell.isSelected;
        
        [_dataSourceArr replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:cateCell.isSelected]];
        
        NSLog(@"cell iSselect%d",cateCell.isSelected);
        if (cateCell.selected) {
            [_deleteArr addObject:@(indexPath.row)];
        }else {
            [_deleteArr removeObject:@(indexPath.row)];
        }
    }else{
        testViewController *vc = [[testViewController alloc]init];
        [vc setChangeCollection:^{
            if (self.navigationController) {
                _collectionView.frame = CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight );
            }else{
                _collectionView.frame = CGRectMake(0, 64, ScreenSizeWidth, ScreenSizeHeight );
            }
        }];
        
        vc.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
        vc.detailStr = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,[_bookArr[indexPath.row] id] ];
        NSArray *path = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        //获取完整路径
        NSString *documentsPath = [path objectAtIndex:0];
        NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"oldUpdateTime.plist"];
        [self presentViewController:vc animated:YES completion:^{
            CategoryDetailsCell *cell = (CategoryDetailsCell *)[_collectionView cellForItemAtIndexPath:indexPath];
            cell.bookModel = _bookArr[indexPath.row];
            cell.redImageView.hidden = YES;
            [ _rootArray removeObject:cell.bookModel.id];
            [_rootArray writeToFile:plistPath atomically:NO];
            
            //        for (NSString *str in _rootArray) {
            //            if ([cell.bookModel.id isEqualToString:str]) {
            //                [_rootArray removeObject:str];
            //                [_rootArray writeToFile:plistPath atomically:NO];
            //            }
            //        }
        }];

    }
//    [self.navigationController pushViewController:vc animated:YES];
    //    testViewController *vc = [[testViewController alloc]init];
    //    UINavigationController *nvc = [[UINavigationController alloc]initWithRootViewController:vc];
    //    [self presentViewController:nvc animated:YES completion:^{
    //        [[NSNotificationCenter defaultCenter] postNotificationName:@"DETAILS" object:_dataArr[indexPath.row]];
    //    }];
}


//#pragma mark - 通知&加载数据
//-(void)loadNoti{
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"topDetail" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"categoryDetail" object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadData:) name:@"searchKeyword" object:nil];
//}


- (BOOL)shouldAutorotate {
    
    return YES;
}
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}


//-(void)refreshNewData{
//    [self showHudToView:self.view text:@"一大波麦萌军正在拼命加载..."];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager GET:_refreshUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        NSArray *arr = backData[@"results"];
//        for (NSDictionary *dict in arr) {
//            CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
//            [md setValuesForKeysWithDictionary:dict];
//            [_dataArr addObject:md];
//        }
//        [self hideHudFromView:self.view];
//        [_collectionView reloadData];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
//}

//-(void)loadData:(NSNotification *)noti{
//    _refreshUrl = noti.object;
//    //    NSString *str = [NSString alloc]initWithData:[NSDate dataWithContentsOfURL:str1] encoding:nil;
//    NSString *str1 = [_refreshUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    _refreshUrl = str1;
//    
//    //---分割网址
//    NSArray *strarr = [_refreshUrl componentsSeparatedByString:@"="];
//    NSLog(@"%@",strarr);
//    
//    [self showHudToView:self.view text:@"一大波麦萌军正在拼命加载..."];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager GET:_refreshUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        NSArray *arr = backData[@"results"];
//        if (backData[@"params"]) {
//            if ([strarr[1] isEqualToString:@"cartoonBillBoard/getCartoonSetListByBillBoard&id"]) {
//                _nameId = backData[@"params"][@"id"];
//                [self loadTopName];
//            }else if ([strarr[1] isEqualToString:@"cartoonCategory/getCartoonSetListByCategory&id"]){
//                _nameId = backData[@"params"][@"id"];
//                [self loadName];
//            }
//        }
//        for (NSDictionary *dict in arr) {
//            CategoryDetailsModel *md = [[CategoryDetailsModel alloc]init];
//            [md setValuesForKeysWithDictionary:dict];
//            [_dataArr addObject:md];
//        }
//        [self hideHudFromView:self.view];
//        [_collectionView reloadData];
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
//}


//-(void)dealloc{
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"topDetail" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"categoryDetail" object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchKeyword" object:nil];
//    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"nameget" object:nil];
//    
//    //    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"searchContents" object:nil];
//}
//-(void)loadTopName{
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_TOP_LIST];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        NSArray *arr = backData[@"results"];
//        for (NSDictionary *dict in arr) {
//            if ([_nameId isEqualToString:dict[@"id"]]) {
//                _nameLabel.text =dict[@"name"];
//                //                _item.title =
//            }
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
//    
//}
//-(void)loadName{
//    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=24",INTERFACE_PREFIXD,API_URL_CATEGORY_LIST];
//    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
//        NSArray *arr = backData[@"results"];
//        for (NSDictionary *dict in arr) {
//            if ([_nameId isEqualToString:dict[@"id"]]) {
//                _nameLabel.text =dict[@"name"];
//                //                _item.title =
//            }
//        }
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//    }];
//    
//}
//
//
//-(void)showHudToView:(UIView *)view text:(NSString *)text{
//    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//    hud.labelText = text;
//}
//-(void)hideHudFromView:(UIView *)view{
//    [MBProgressHUD hideAllHUDsForView:view animated:YES];
//}
//
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
