//
//  BookModel.h
//  GameSM
//
//  Created by 祝嘉蔓 on 15/10/14.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookModel : NSObject

@property(nonatomic,strong)NSString *images;
@property(nonatomic,strong)NSString *name;

@property(nonatomic,strong)NSString *author;
@property(nonatomic,strong)NSString *categorys;
@property(nonatomic,strong)NSString *createTime;
@property(nonatomic,strong)NSString *modifyTime;
@property(nonatomic,strong)NSString *introduction;
@property(nonatomic,strong)NSString *id;
@property(nonatomic,strong)NSString *isOver;
@property(nonatomic,strong)NSString *priority;
@property(nonatomic,strong)NSString *remark;
@property(nonatomic,strong)NSString *status;
@property(nonatomic,strong)NSString *thirdID;
@property(nonatomic,strong)NSString *thirdUpdateTime;
@property(nonatomic,strong)NSString *userID;





@end
