//
//  BookCell.m
//  GameSM
//
//  Created by mac on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BookCell.h"
#import "BookModel.h"
#import "Config.h"
#import "Y_X_DataInterface.h"
#import "AFNetworking.h"
#import "customImage.h"
#import "CategoryDetailsModel.h"
#import "BookModel.h"
#import "CusBookImage.h"
#import "testViewController.h"
#import "UIView+Addtion.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)



@interface BookCell ()
{
    UILabel *_nameLabel1;
    UIImageView *_iv1;
    NSMutableArray *_dataArr;
    NSMutableArray *_LudataArr;
    NSMutableArray *_LuChapterId;
    NSMutableArray *_NearReaddataArr;
    NSMutableArray *_CollectiondataArr;
    NSInteger  _tag;
    
    UITapGestureRecognizer *_kiTap;
    
    UIView *_cellView;
    UIView *_sessionView;
    UIView *_msgView;
}
@end

@implementation BookCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andSection:(NSInteger)section{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _tag = section;
        //        [self creatView];
    }
    return self;
    
}
//http://apitest.playsm.com/index.php?r=cartoonSet/detail&id=18

- (void)tapGes:(UITapGestureRecognizer *)tap {
//    NSLog(@"idcus%ld",tap.view.tag);
    [[LogHelper shared] writeToFilefrom_page:@"cfh" from_section:@"c" from_step:@"h" to_page:@"cd" to_section:@"c" to_step:@"d" type:@"" id:[NSString stringWithFormat:@"%ld",tap.view.tag]];
    testViewController *testVC = [[testViewController alloc] init];
    [testVC setChangeCollection:^{
        
    }];
    testVC.modalTransitionStyle =  UIModalTransitionStyleCrossDissolve;
    testVC.detailStr = [NSString stringWithFormat:@"%@r=%@&id=%ld",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,tap.view.tag];
//    UINavigationController *nac = [[UINavigationController alloc]initWithRootViewController:testVC];
    
    [self.viewController presentViewController:testVC animated:YES completion:nil];
}


- (void)creatView:(NSArray *)commicArr {
    
//    [self analyzeNearReadData];
//    [self analyzeColllectionData];
    NSInteger commicCount = (commicArr.count >=3)?3:commicArr.count;
    CGFloat widthCommic = KScreenWidth/3;
    for (int i = 0; i < commicCount; i ++) {
        CusBookImage *cusIma = [[CusBookImage alloc] initWithFrame:CGRectMake(4 + widthCommic * i, 0, (KScreenWidth-24)/3, (KScreenWidth-24)/3*___Scale+20)];
            cusIma.bookModel = commicArr[i];
        cusIma.tag = [[commicArr[i] id] integerValue];
        
        [self addSubview:cusIma];
        
        UITapGestureRecognizer *tapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGes:)];
        [cusIma addGestureRecognizer:tapGes];
    }
    
    //    [self analyzeData];
    
//    _cellView = [[UIView alloc]initWithFrame:CGRectMake(krecommentCellInset, krecommentCellInset, self.contentView.bounds.size.width-2*krecommentCellInset, KrecommentCellHeight-krecommentCellInset*2-10)];
//    _cellView.backgroundColor = [UIColor whiteColor];
//    _cellView.layer.borderColor =[UIColor colorWithRed:225/255.0 green:225/255.0 blue:225/255.0 alpha:1].CGColor;
//    _cellView.layer.borderWidth = 0.5f;
//    _cellView.layer.masksToBounds = YES;
//    [self.contentView addSubview:_cellView];
//    
//    _msgView = [[UIView alloc]initWithFrame:CGRectMake( 0, _sessionView.bottom, _cellView.width,KrecommentCellHeight)];//-_sessionView.height
//    _msgView.backgroundColor  = [UIColor clearColor];
//    [_cellView addSubview:_msgView];
//    
//    //-----------------------1话
//    UIControl *manhuaControl1 = [[UIControl alloc]initWithFrame:CGRectMake(krecommentCellInset, 3, (_cellView.width-6*krecommentCellInset)/3, KrecommentCellImageHeight + 52)];
//    [_msgView addSubview:manhuaControl1];
//    
//    _iv1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,(_cellView.width-6*krecommentCellInset)/3, KrecommentCellImageHeight)];
//    _iv1.layer.cornerRadius = 4;
//    _iv1.clipsToBounds = YES;
//    [_iv1 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model[@"images"]]]]]];
//    _iv1.userInteractionEnabled = YES;
//    UITapGestureRecognizer *kitTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jump1:)];
//    _kiTap = kitTap;
//    [_iv1 addGestureRecognizer:kitTap];
//    [manhuaControl1 addSubview:_iv1];
//    
//    _nameLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(_iv1.left, _iv1.bottom,(_cellView.width-6*krecommentCellInset)/3, 20)];
//    _nameLabel1.font = [UIFont systemFontOfSize:14];
//    _nameLabel1.text = _model[@"name"];
//    [manhuaControl1 addSubview:_nameLabel1];
//    
//    UILabel *gengxinLable1 = [[UILabel alloc]initWithFrame:CGRectMake(_iv1.left, _nameLabel1.bottom-krecommentCellInset, (_cellView.width-6*krecommentCellInset)/3, 14)];
//    if (_model[@"chapterNameLabel"]) {
//        gengxinLable1.text = _model[@"chapterNameLabel"];
//    }
//    gengxinLable1.font = [ UIFont systemFontOfSize:10];
//    gengxinLable1.textColor = [UIColor grayColor];
//    [manhuaControl1 addSubview:gengxinLable1];
//    
//    //---------------------------------------2话
//    UIControl *manhuaControl2 = [[UIControl alloc]initWithFrame:CGRectMake(manhuaControl1.right + krecommentCellInset*2 , 3, (_cellView.width-6*krecommentCellInset)/3, KrecommentCellImageHeight + 52)];
//    [_msgView addSubview:manhuaControl2];
//    
//    UIImageView *manhuaimgv2 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, (_cellView.width-6*krecommentCellInset)/3, KrecommentCellImageHeight)];
//    [manhuaimgv2 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model1[@"images"]]]]]];
//    manhuaimgv2.userInteractionEnabled = YES;
//    manhuaimgv2.layer.cornerRadius = 4;
//    manhuaimgv2.clipsToBounds = YES;
//    UITapGestureRecognizer *kitTap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jump2)];
//    [manhuaimgv2 addGestureRecognizer:kitTap1];
//    
//    [manhuaControl2 addSubview:manhuaimgv2];
//    UILabel *titleLable2 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv2.left, manhuaimgv2.bottom,(_cellView.width-6*krecommentCellInset)/3, 20)];
//    titleLable2.text = _model1[@"name"];
//    titleLable2.font  = [UIFont systemFontOfSize:14];
//    [manhuaControl2 addSubview:titleLable2];
//    
//    UILabel *gengxinLable2 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv2.left, titleLable2.bottom-krecommentCellInset, (_cellView.width-6*krecommentCellInset)/3,14)];
//    if (_model1[@"chapterNameLabel"]) {
//        gengxinLable2.text = _model1[@"chapterNameLabel"];
//    }
//    gengxinLable2.font = [UIFont systemFontOfSize:10];
//    gengxinLable2.textColor = [UIColor grayColor];
//    [manhuaControl2 addSubview:gengxinLable2];
//    
//    //-----------------3.话
//    UIControl *manhuaControl3 = [[UIControl alloc]initWithFrame:CGRectMake(manhuaControl2.right + krecommentCellInset*2, 3, (_cellView.width-6*krecommentCellInset)/3, KrecommentCellImageHeight + 52)];
//    [_msgView addSubview:manhuaControl3];
//    
//    UIImageView *manhuaimgv3 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, (_cellView.width-6*krecommentCellInset)/3, KrecommentCellImageHeight)];
//    [manhuaimgv3 setImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_model2[@"images"]]]]]];
//    manhuaimgv3.userInteractionEnabled = YES;
//    manhuaimgv3.layer.cornerRadius = 4;
//    manhuaimgv3.clipsToBounds = YES;
//    UITapGestureRecognizer *kitTap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(jump3)];
//    [manhuaimgv3 addGestureRecognizer:kitTap2];
//    [manhuaControl3 addSubview:manhuaimgv3];
//    UILabel *titleLable3 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv3.left, manhuaimgv3.bottom,(_cellView.width-6*krecommentCellInset)/3, 20)];
//    titleLable3.text = _model2[@"name"];
//    titleLable3.font = [UIFont systemFontOfSize:14];
//    [manhuaControl3 addSubview:titleLable3];
//    
//    UILabel *gengxinLable3 = [[UILabel alloc]initWithFrame:CGRectMake(manhuaimgv3.left, titleLable3.bottom-krecommentCellInset,(_cellView.width-6*krecommentCellInset)/3, 14)];
//    if (_model2[@"chapterNameLabel"]) {
//        gengxinLable3.text = _model2[@"chapterNameLabel"];
//    }
//    gengxinLable3.font = [UIFont systemFontOfSize:10];
//    gengxinLable3.textColor = [UIColor grayColor];
//    [manhuaControl3 addSubview:gengxinLable3];
}
- (void)TapViewWithView:(UITapGestureRecognizer *)tapGR{
    UIView *view = tapGR.view;
}
#pragma mark - 点击事件
-(void)jump1:(id)sender{
    if (_tag == 0) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_NearReaddataArr[0]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu--1" object:str];
    }
    if (_tag == 1) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_CollectiondataArr[0]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentTop--1" object:str];
    }
}
-(void)jump2{
    
    if (_tag == 0) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_NearReaddataArr[1]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu--2" object:str];
    }
    if (_tag == 1) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONSET_LIST,_CollectiondataArr[1]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentTop--2" object:str];
    }
}
-(void)jump3{
    if (_tag == 0) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_CARTOONREADHISTORY_DETAIL,_NearReaddataArr[2]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentLu--3" object:str];
    }
    if (_tag == 1) {
        NSString *str = [NSString stringWithFormat:@"%@r=%@&id=%@",INTERFACE_PREFIXD,API_URL_BOOKCOLLECTIONLIST_LIST,_CollectiondataArr[2]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"recommentTop--3" object:str];
    }
}
#pragma mark - 数据解析

-(void)analyzeNearReadData{
    _NearReaddataArr = [NSMutableArray arrayWithCapacity:0];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_CARTOONREADHISTORY_DETAIL];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            NSString *idNum = dict[@"cartoonId"];
            [_NearReaddataArr addObject:idNum];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}
-(void)analyzeColllectionData{
    _CollectiondataArr = [NSMutableArray arrayWithCapacity:0];
    NSString *str = [NSString stringWithFormat:@"%@r=%@&page=1&size=10",INTERFACE_PREFIXD,API_URL_BOOKCOLLECTIONLIST_LIST];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager GET:str parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        id backData = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        NSArray *arr = backData[@"results"];
        for (NSDictionary *dict in arr) {
            NSString *idNum = dict[@"cartoonId"];
            [_CollectiondataArr addObject:idNum];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }];
}

#pragma mark-跳转

//-(void)setModel:(NSDictionary *)model{
//    _model = model;
//
//    NSLog(@"==%@",_model);
//    [self creatView];
//}
//-(void)setModel1:(NSDictionary *)model1{
//    _model1 = model1;
//
//    [self creatView];
//}
//-(void)setModel2:(NSDictionary *)model2{
//    _model2 = model2;
//   
//    [self creatView];
//}
- (void)setCateModels:(NSArray *)cateModels {
    [self creatView:cateModels];
}




-(void)tagNum:(NSNotification *)noti{
    _tag = [noti.object intValue];
}
@end


