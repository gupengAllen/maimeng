//
//  UIView+Addtion.h
//  味库1.0
//
//  Created by qianfeng on 14-11-10.
//  Copyright (c) 2014年 alex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Addtion)

- (UIViewController *)viewController;

@end
