//
//  CusBookImage.m
//  GameSM
//
//  Created by 顾鹏 on 15/10/21.
//  Copyright © 2015年 王涛. All rights reserved.
//

#import "CusBookImage.h"
#import "CartoonInfoAll.h"
#import "UIImageView+AFNetworking.h"
@implementation CusBookImage
- (void)setBookModel:(CartoonInfoAll *)bookModel {
    UIImageView * imageV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 15)];
    imageV.layer.cornerRadius = 3.0;
    imageV.layer.masksToBounds = YES;
    [imageV setImageWithURL:[NSURL URLWithString:bookModel.images]];
    
    [self addSubview:imageV];
    
    UILabel *name = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 10, self.frame.size.width, 20)];
    name.font = [UIFont systemFontOfSize:13.0];
    name.textAlignment = NSTextAlignmentLeft;
    name.text = bookModel.name;
    [self addSubview:name];
    
    

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
