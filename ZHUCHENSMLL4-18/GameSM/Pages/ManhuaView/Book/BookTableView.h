//
//  BookTableView.h
//  GameSM
//
//  Created by mac on 15/9/21.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseTableView.h"

@interface BookTableView : BaseTableView<UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,strong)NSMutableArray *bookDataArr;
@property(nonatomic,strong)NSMutableArray *downArr;
@property(nonatomic,strong)NSMutableArray *collectionArr;
@property(nonatomic,strong)NSMutableArray *downLoadArr;

@property (nonatomic,strong)NSString *type;

@property(nonatomic,strong)NSMutableDictionary *bookDataCount;

@end
