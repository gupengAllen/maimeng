//
//  BookCell.h
//  GameSM
//
//  Created by mac on 15/9/23.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BookModel;

@interface BookCell : UITableViewCell

@property(nonatomic,strong)BookModel *model3;

@property(nonatomic,strong)NSArray *cateModels;
@property(nonatomic,strong)NSDictionary *model;
@property(nonatomic,strong)NSDictionary *model1;
@property(nonatomic,strong)NSDictionary *model2;

//@property(nonatomic,strong)RecommentModel *model;
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier andSection:(NSInteger)section;

@end
