//
//  XLScrollView.m
//  XLScrollDemo
//
//  Created by 章晓亮 on 15/1/12.
//  Copyright (c) 2015年 ___章晓亮___. All rights reserved.
//
//新浪微博：@亮亮亮亮亮靓啊
//工作邮箱：k52471@126.com

#define screen_width [UIScreen mainScreen].bounds.size.width
#import "XLScrollViewer.h"

@interface XLScrollViewer ()<UIScrollViewDelegate>
{   //按钮的下标
    int _x;
    CGFloat _x0;
    CGFloat _TopButtonWidth;
    UIButton *_commonButton;
    NSInteger _markMain;
}
@property(nonatomic,strong)UIScrollView *scroll2;
@property(nonatomic,strong)UIView *view2;

@property(nonatomic ,strong)NSMutableArray *buttons;

@end

@implementation XLScrollViewer

#warning 移除通知
- (void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self name:kchangethumeNameCurrepagr object:nil];
}

-(instancetype)initWithFrame:(CGRect)frame withViews:(NSArray *)views withButtonNames:(NSArray *)names withThreeAnimation:(NSArray *)choose{
    self =[super initWithFrame:frame];
    if (self) {
        self.xl_views =views;
        self.xl_buttonNames =names;
        self.xl_isMoveButton = [choose[0] boolValue];
        self.xl_isScaleButton = [choose[1] boolValue];
        self.xl_isMoveSlider = [choose[2] boolValue];
    }
    return self;
}
+(instancetype)scrollWithFrame:(CGRect)frame withViews:(NSArray *)views withButtonNames:(NSArray *)names withThreeAnimation:(NSArray *)choose{
    return [[self alloc]initWithFrame:frame withViews:views withButtonNames:names withThreeAnimation:choose];
}

-(void)drawRect:(CGRect)rect {
    
    [self addAll];
}
-(void)addAll {
    if ((self.xl_buttonNames.count || self.xl_views.count) && self.xl_buttonNames.count !=self.xl_views.count) {
        UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"XLScroll友情提醒！" message:@"您填写的按钮数与视图数不一致，请仔细检查代码" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }else {
        self.xl_buttonNames =self.xl_buttonNames?self.xl_buttonNames:@[@"视图三",@"视图三",@"视图三"];
        [self addScroll2];
        [self addScroll1];
    }
}
-(void)addScroll1{
    
    self.scroll1 =[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.xl_topWidth?self.xl_topWidth:screen_width, self.xl_topHeight?self.xl_topHeight:50)];
    if (self.xl_buttonNames.count <=5) {
        self.scroll1.contentSize =CGSizeMake(self.xl_topWidth?self.xl_topWidth:screen_width, 0);
    }else {
        CGFloat width = self.xl_topWidth?self.xl_topWidth:screen_width;
        CGFloat itemWidth = width/5;
        self.scroll1.contentSize =CGSizeMake(itemWidth*self.xl_buttonNames.count, 0);
    }
    if (self.xl_topBackImage) {
        self.scroll1.backgroundColor =[UIColor clearColor];
        UIImageView *temp =[[UIImageView alloc]initWithFrame:self.scroll1.frame];
        temp.image =self.xl_topBackImage;
        [self insertSubview:temp belowSubview:self.scroll1];
    }else {
        self.scroll1.backgroundColor =self.xl_topBackColor?self.xl_topBackColor:[UIColor lightGrayColor];
    }
    
    self.scroll1.showsHorizontalScrollIndicator =NO;
    self.scroll1.showsVerticalScrollIndicator =NO;
    self.scroll1.bounces =NO;
    self.scroll1.contentOffset=CGPointZero;
    self.scroll1.scrollsToTop =NO;
    
    self.buttons =[NSMutableArray array];
    for (int i =0; i<self.xl_buttonNames.count; i++) {
        
        UIButton *temp =[UIButton buttonWithType:UIButtonTypeCustom];
        if (self.xl_buttonNames.count <=5) {
            _TopButtonWidth = (self.xl_topWidth?self.xl_topWidth:screen_width)/self.xl_buttonNames.count;
            temp.frame =CGRectMake(_TopButtonWidth*i, 10, _TopButtonWidth, self.xl_topHeight?self.xl_topHeight:50);
        }else {
            _TopButtonWidth =(self.xl_topWidth?self.xl_topWidth:screen_width)/5;
            temp.frame =CGRectMake(_TopButtonWidth*i, 0, _TopButtonWidth, self.xl_topHeight?self.xl_topHeight:50);
        }
        
        temp.titleLabel.font =[UIFont systemFontOfSize:self.xl_buttonFont?self.xl_buttonFont:18];
        [temp setTitle:self.xl_buttonNames[i] forState:UIControlStateNormal];
        [temp setTitleColor:self.xl_buttonColorNormal?self.xl_buttonColorNormal:[UIColor blackColor] forState:UIControlStateNormal];
        [temp setTitleColor:self.xl_buttonColorSelected?self.xl_buttonColorSelected:[UIColor whiteColor] forState:UIControlStateSelected];
        if (i == 0) {
            temp.selected =YES;
            temp.titleLabel.font = [UIFont systemFontOfSize:18.0];
            [temp setTitleColor:self.xl_buttonColorSelected?self.xl_buttonColorSelected:[UIColor whiteColor] forState:UIControlStateNormal];
            _commonButton = temp;
        }
        [temp addTarget:self action:@selector(changed:) forControlEvents:UIControlEventTouchUpInside];
        [self.scroll1 addSubview:temp];
        [self.buttons addObject:temp];
    }
    //滑动视图
    CGSize size0 =[self.xl_buttonNames[0] sizeWithAttributes:@{NSFontAttributeName :[UIFont systemFontOfSize:self.xl_buttonFont?self.xl_buttonFont:18]}];
    UIButton *button0 =self.buttons[0];

    _x0 =button0.center.x -size0.width/2;
//    _x0 = 0;
//    self.view2 =[[UIView alloc]initWithFrame:CGRectMake(_x0,CGRectGetMaxY(button0.frame)-(self.xl_buttonToSlider?self.xl_buttonToSlider:10), size0.width, self.xl_sliderHeight?self.xl_sliderHeight:2)];
   
    //1.初始化的宽度
    self.view2 =[[UIView alloc]initWithFrame:CGRectMake(0,CGRectGetMaxY(button0.frame)-(self.xl_buttonToSlider?self.xl_buttonToSlider:10)-11, screen_width/5.0, self.xl_sliderHeight?self.xl_sliderHeight:2)];
    UIImageView *lineimg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, screen_width/5.0, self.xl_sliderHeight?self.xl_sliderHeight:2)];
    UIImage *img = [UIImage imageNamed:@"line"];
    [img stretchableImageWithLeftCapWidth:2 topCapHeight:5];
    lineimg.image = img;
 
    [self.view2 addSubview:lineimg];
    
//    self.view2.backgroundColor =self.xl_sliderColor?self.xl_sliderColor:[UIColor blueColor];
    if (self.xl_isSliderCorner) {
        [self.view2.layer setCornerRadius:self.xl_sliderCorner?self.xl_sliderCorner:5];
    }
    
    [self.scroll1 insertSubview:self.view2 atIndex:0];
    
    [self addSubview:self.scroll1];
}

-(void)addScroll2{
    self.scroll2 =[[UIScrollView alloc]initWithFrame:CGRectMake(0, self.xl_topHeight?self.xl_topHeight:50, screen_width, self.frame.size.height -(self.xl_topHeight?self.xl_topHeight:50))];
    self.scroll2.contentOffset=CGPointZero;
    self.scroll2.contentSize=CGSizeMake(screen_width*self.xl_buttonNames.count, 0);
    self.scroll2.showsHorizontalScrollIndicator =NO;
    self.scroll2.showsVerticalScrollIndicator =NO;
    self.scroll2.delegate =self;
    self.scroll2.tag = 200;
    self.scroll2.pagingEnabled =YES;
    self.scroll2.bounces =NO;
    self.scroll2.scrollsToTop =NO;
    
    for (int i =0; i<self.xl_buttonNames.count; i++) {
        
        if (!self.xl_views) {
            UIView *temp =[[UIView alloc]initWithFrame:(CGRect){{screen_width*i, 0},self.scroll2.frame.size}];
            NSArray *cls =@[[UIColor redColor],[UIColor orangeColor],[UIColor yellowColor]];
            temp.backgroundColor =cls[i];
            [self.scroll2 addSubview:temp];
        }else {
            UIView *temp = self.xl_views[i];
            temp.frame =(CGRect){{screen_width*i, 0},self.scroll2.frame.size};
            [self.scroll2 addSubview:temp];
        }
    }
    
    [self addSubview:self.scroll2];
}

#pragma mark - buttonAction
-(void)changed:(UIButton *)button{
    
    
    if ([self.xl_buttonNames[1] isEqualToString:@"搜索"]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"resignSearch" object:nil];
    }
    
    _commonButton.titleLabel.font = [UIFont systemFontOfSize:self.xl_buttonFont?self.xl_buttonFont:18];
    _commonButton = button;
    button.titleLabel.font = [UIFont systemFontOfSize:18.0];
    if (self.xl_isScaleButton) {
        if (!button.selected) {
            [UIView animateWithDuration:0.2 animations:^{
                button.transform =CGAffineTransformScale(button.transform, 0.7, 0.7);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.2 animations:^{
                    button.transform =CGAffineTransformScale(button.transform, 1/0.6, 1/0.6);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.2 animations:^{
                        button.transform =CGAffineTransformScale(button.transform, 1/0.7*0.6, 1/0.7*0.6);
                    }];
                }];
            }];
        }
    }
    
    for (UIButton *temp in self.buttons) {
        
        if (temp.selected && temp !=button) {
            temp.selected =NO;
        }
        if (temp ==button) {
            [temp setTitleColor:self.xl_buttonColorSelected?self.xl_buttonColorSelected:[UIColor whiteColor] forState:UIControlStateNormal];
            
        }else {
            [temp setTitleColor:self.xl_buttonColorNormal?self.xl_buttonColorNormal:[UIColor blackColor] forState:UIControlStateNormal];
            
        }
    }
    button.selected =YES;
    button.titleLabel.font = [UIFont systemFontOfSize:18.0];
    _commonButton = button;
    self.scroll2.delegate =nil;
    NSInteger index = button.center.x /_TopButtonWidth;
    
    [[LogHelper shared] writeToFilefrom_page:[self stringWithProtocol:self.currentPage] from_section:@"c" from_step:@"h" to_page:[self stringWithProtocol:index] to_section:@"c" to_step:@"h" type:@"click" id:@"0"];
    self.currentPage = index;
    self.scroll2.contentOffset =CGPointMake((int)index*screen_width, 0);
    CGSize size =[button.titleLabel.text sizeWithAttributes:@{NSFontAttributeName :[UIFont systemFontOfSize:self.xl_buttonFont?self.xl_buttonFont:18]}];
    
    
    
    if (self.xl_isMoveSlider) {
        [UIView animateWithDuration:0.3 animations:^{
            CGRect rect =self.view2.frame;
            rect.size.width =screen_width/5.0;
            self.view2.frame =rect;
//第二次修改frame
//            self.view2.transform =CGAffineTransformMakeTranslation(button.frame.origin.x +button.frame.size.width/2 -size.width/2 -_x0, 0);
            self.view2.transform =CGAffineTransformMakeTranslation( screen_width/5.0*index,0);//button.center.x  - button.frame.size.width/2,
        }];
    }else {
        CGRect rect =self.view2.frame;
        rect.size.width =size.width;
        self.view2.frame =rect;
//        self.view2.transform =CGAffineTransformMakeTranslation(button.frame.origin.x +button.frame.size.width/2 -size.width/2-_x0, 0);
//第二次修改frame
        self.view2.transform =CGAffineTransformMakeTranslation(button.center.x -  button.frame.size.width/2
                                                               , 0);

       
       
    }
    
    
    self.scroll2.delegate =self;
    
}

- (NSString *)stringWithProtocol:(NSInteger)index{
    NSString *returnStr = @"";
    switch (index) {
        case 0:
            return returnStr = @"crh";
            break;
        case 1:
            return returnStr = @"csh";
            break;
        case 2:
            return returnStr = @"cbh";
            break;
        case 3:
            return returnStr = @"cch";
            break;
        case 4:
            return returnStr = @"cfh";
            
        default:
            return returnStr;
            break;
    }
    
}

#pragma mark - UIScrollView delegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    _x =scrollView.contentOffset.x/screen_width;
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    UIButton *button =self.buttons[_x];
    [button setTitleColor:self.xl_buttonColorNormal?self.xl_buttonColorNormal:[UIColor blackColor] forState:UIControlStateNormal];
    
    CGPoint point=self.scroll2.contentOffset;
    point.y =0;
    self.scroll2.contentOffset =point;
    
    CGFloat move1 = scrollView.contentOffset.x/screen_width*self.xl_topWidth;
    CGFloat move = self.xl_topWidth?move1:scrollView.contentOffset.x;
    
    if (self.xl_isMoveButton) {
        if (self.xl_buttonNames.count <=5) {
            if ([self.xl_buttonNames[1] isEqualToString:@"搜索"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resignSearch" object:nil];
            }
            button.transform =CGAffineTransformMakeTranslation((scrollView.contentOffset.x -button.frame.size.width*self.xl_buttonNames.count*_x)/self.xl_buttonNames.count/3, 0);
            self.view2.transform =CGAffineTransformMakeTranslation(scrollView.contentOffset.x/self.xl_buttonNames.count, 0);
        }else {
            button.transform =CGAffineTransformMakeTranslation((scrollView.contentOffset.x -button.frame.size.width*5*_x)/5/3, 0);
            self.view2.transform =CGAffineTransformMakeTranslation(scrollView.contentOffset.x/5, 0);
        }
        
        
        
    }else {
        if (self.xl_buttonNames.count <=5) {
            
            if ([self.xl_buttonNames[1] isEqualToString:@"搜索"]) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"resignSearch" object:nil];
            }

            self.view2.transform =CGAffineTransformMakeTranslation(move/self.xl_buttonNames.count+10, 0);
        }else {
            self.view2.transform =CGAffineTransformMakeTranslation(move/5, 0);
        }
    }
    
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    
    for (UIButton *temp in self.buttons) {
        
        [UIView animateWithDuration:0.2 animations:^{
            
            temp.transform =CGAffineTransformMakeTranslation(0, 0);
        }];
        
        if (temp.selected) {
            temp.selected =NO;
        }
        int x1 =temp.frame.origin.x;
        int x2 =0;
        
        CGFloat to1 = scrollView.contentOffset.x/screen_width*self.xl_topWidth;
        CGFloat to = self.xl_topWidth?to1:scrollView.contentOffset.x;
        int xAfter =scrollView.contentOffset.x/screen_width;
        if (self.xl_buttonNames.count <=5) {
            x2 =to/self.xl_buttonNames.count;
        }else {
            x2 =to/5;
            
        }
        
        if (x1 == x2) {

            _commonButton.titleLabel.font = [UIFont systemFontOfSize:self.xl_buttonFont?self.xl_buttonFont:18];
            temp.selected =YES;
            temp.titleLabel.font = [UIFont systemFontOfSize:18.0];
            _commonButton = temp;
            [temp setTitleColor:self.xl_buttonColorSelected?self.xl_buttonColorSelected:[UIColor whiteColor] forState:UIControlStateNormal];
            
            CGSize size =[temp.titleLabel.text sizeWithAttributes:@{NSFontAttributeName :[UIFont systemFontOfSize:self.xl_buttonFont?self.xl_buttonFont:18]}];
            [UIView animateWithDuration:0.2 animations:^{
                CGRect rect =self.view2.frame;
                rect.size.width =size.width;
                self.view2.frame =rect;
                self.view2.transform =CGAffineTransformMakeTranslation(temp.frame.origin.x +temp.frame.size.width/2 -size.width/2 -_x0, 0);
            }];
        }
        if (self.currentPage != xAfter) {
            [[LogHelper shared] writeToFilefrom_page:[self stringWithProtocol:self.currentPage] from_section:@"c" from_step:@"h" to_page:[self stringWithProtocol:xAfter] to_section:@"c" to_step:@"h" type:@"move" id:@"0"];

        }

        self.currentPage = xAfter;
    }
    
    if (self.buttons.count >5) {
        UIButton *button =self.buttons[_x];
        int xAfter =scrollView.contentOffset.x/screen_width;
        if (_x<xAfter) {
            
            if (_x>=2 && _x<=self.xl_buttonNames.count-4 ) {
                [UIView animateWithDuration:0.2 animations:^{
                    self.scroll1.contentOffset =CGPointMake(button.center.x -_TopButtonWidth*1.5, 0);
                }];
            }
            if (self.buttons.count == (self.xl_buttonNames.count-5) &&_x>3){
                [UIView animateWithDuration:0.2 animations:^{
                    self.scroll1.contentOffset =CGPointMake(button.frame.size.width*(self.xl_buttonNames.count-5), 0);
                }];
            }
            //            if ((self.buttons.count ==6)|7 &&_x>3) {
            //                if ((_x ==4)|5) {
            //                    [UIView animateWithDuration:0.2 animations:^{
            //                        self.scroll1.contentOffset =CGPointMake(button.frame.size.width*(self.buttons.count==7?2:1), 0);
            //                    }];
            //                }
            //            }
        }else if (_x>xAfter) {
            
            if (_x>=3 && _x<=self.xl_buttonNames.count-2 ) {
                [UIView animateWithDuration:0.2 animations:^{
                    self.scroll1.contentOffset =CGPointMake(button.center.x -_TopButtonWidth*3.5, 0);
                }];
            }
            if ((self.buttons.count ==6)|7 &&_x<3) {
                if ((_x ==1)|2) {
                    [UIView animateWithDuration:0.2 animations:^{
                        self.scroll1.contentOffset =CGPointMake(0, 0);
                    }];
                }
            }
        }
        
//        [[LogHelper shared] writeToFilefrom_page:[self stringWithProtocol:self.currentPage] from_section:@"c" from_step:@"h" to_page:[self stringWithProtocol:xAfter] to_section:@"c" to_step:@"h" type:@"move" id:@"0"];

        self.currentPage = xAfter;
        
        
    }
}


- (void)setCurrentPage:(NSInteger)currentPage{
    if (_currentPage!=currentPage) {
        
        NSNumber *page = [NSNumber numberWithInteger:currentPage];
        NSDictionary *dic = @{
                              @"name":_xl_buttonNames,
                              @"page":page
                              };
        [[NSNotificationCenter defaultCenter] postNotificationName:kchangethumeNameCurrepagr object:nil userInfo:dic];
        
    }
    _currentPage = currentPage;
}


@end

