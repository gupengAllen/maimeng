//
//  ResetPasswordViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/14.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTextField.h"

@interface ResetPasswordViewController : BaseViewController

@property (weak, nonatomic) IBOutlet CustomTextField *passWordText;
@property (nonatomic, strong) NSString *telephone;
@property (nonatomic, strong) NSString *code;

- (IBAction)changePasswdTextPressed:(id)sender;

- (IBAction)submitBtnPressed:(id)sender;
@end
