//
//  ThreeTagTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/4/26.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TagModel;
@interface ThreeTagTableViewCell : UITableViewCell
@property(nonatomic, retain)NSMutableArray <TagModel *>*tagArr;
@property (weak, nonatomic) IBOutlet UIButton *firstTag;
@property (weak, nonatomic) IBOutlet UIButton *secondTag;
@property (weak, nonatomic) IBOutlet UIButton *thirdTag;
- (IBAction)firstPress:(id)sender;
- (IBAction)secondPress:(id)sender;
- (IBAction)thirdPress:(id)sender;

@end
