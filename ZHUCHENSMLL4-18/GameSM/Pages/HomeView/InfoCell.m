//
//  InfoCell.m
//  GameSM
//
//  Created by 王涛 on 15/7/8.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "InfoCell.h"

@implementation InfoCell

+ (id)infoCellOwner:(id)owner {
    if (!owner) {
        return nil;
    }
    NSArray *nibEles = [[NSBundle mainBundle] loadNibNamed:@"InfoCell" owner:owner options:nil];
    for (id cellObject in nibEles) {
        if ([cellObject isKindOfClass:[InfoCell class]]) {
            return (InfoCell *)cellObject;
        }
    }
    return nil;
}

- (void)resetSize {
    CGSize size = [self.infoContentLabel.text sizeWithFont:self.infoContentLabel.font constrainedToSize:CGSizeMake(self.infoContentLabel.bounds.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    if (size.height > 18) {
        if (size.height < 36) {
            self.infoContentLabel.frame = CGRectMake(self.infoContentLabel.frame.origin.x, self.infoContentLabel.frame.origin.y, self.infoContentLabel.bounds.size.width, size.height + 1);
        } else {
            self.infoContentLabel.frame = CGRectMake(self.infoContentLabel.frame.origin.x, self.infoContentLabel.frame.origin.y, self.infoContentLabel.bounds.size.width, 36);
        }
    } else {
        self.infoContentLabel.frame = CGRectMake(self.infoContentLabel.frame.origin.x, self.infoContentLabel.frame.origin.y, self.infoContentLabel.bounds.size.width, 18);
    }
}

@end
