//
//  AllTagViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/21.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "AllTagViewController.h"
#import "tagCollectionCell.h"
#import "TagModel.h"
#import "InfoTagViewController.h"
@interface AllTagViewController ()<UIScrollViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate>
@property(nonatomic,copy)UICollectionView *collectionView;
@end

@implementation AllTagViewController

- (void)viewWillAppear:(BOOL)animated{
    LYTabBarController *lyTabBar = (LYTabBarController *)self.tabBarController;
    [lyTabBar hiddenBar:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addBackBtn];
    [self initTitleName:@"全部标签"];
    [self.view addSubview:self.collectionView];
}

- (void)setTagArr:(NSMutableArray *)tagArr{
    _tagArr = tagArr;
}




- (UICollectionView*)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        flowLayout.minimumInteritemSpacing = 5;
        flowLayout.minimumLineSpacing = 3;
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(15, 0, ScreenSizeWidth - 30, ScreenSizeHeight) collectionViewLayout:flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        [_collectionView registerClass:[tagCollectionCell class] forCellWithReuseIdentifier:@"cellIdentify"];
        
    }
    return _collectionView;
}


#pragma mark -CollectionView datasource
//section
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
//item个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _tagArr.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //重用cell
    NSString *kcellIdentifier = @"cellIdentify";
    tagCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kcellIdentifier forIndexPath:indexPath];
    //赋值
    [cell.tagButton setTitle:[_tagArr[indexPath.row] name] forState:UIControlStateNormal];
    
    return cell;
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((ScreenSizeWidth - 50)/3 , 40);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [[LogHelper shared] writeToFilefrom_page:@"mll" from_section:@"m" from_step:@"l" to_page:@"mldl" to_section:@"m" to_step:@"l" type:@"" id:@"0" detail:@{@"keyword":[_tagArr[indexPath.row] name]}];
    InfoTagViewController *infoTag = [[InfoTagViewController alloc] init];
    infoTag.tagName = [_tagArr[indexPath.row] name];
    [self.navigationController pushViewController:infoTag animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
