//
//  TagModel.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/21.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "TagModel.h"

@implementation TagModel
+ (NSMutableArray *)paraseDict:(NSDictionary *)dict {
    NSArray *tagResults = dict[@"results"];
    NSMutableArray *tagAllArr = [NSMutableArray array];
    for (int j = 0; j < tagResults.count/4; j ++) {
        
    NSMutableArray *tagArr = [NSMutableArray array];
        
        for (int i = j*4; i < ((tagResults.count%4 == 0 ?(j+1):j)*4 + tagResults.count%4) ; i ++) {
        TagModel *model = [[TagModel alloc] init];
        model.id = tagResults[i][@"id"];
        model.name = tagResults[i][@"name"];
        model.images = tagResults[i][@"images"];
        model.priority = tagResults[i][@"priority"];
        model.status = tagResults[i][@"status"];
        model.createTime = tagResults[i][@"createTime"];
        model.modifyTime = tagResults[i][@"modifyTime"];
        model.userID = tagResults[i][@"userID"];
        [tagArr addObject:model];
    }
        [tagAllArr addObject:tagArr];
    }
    return tagAllArr;
}

+ (NSMutableArray *)paraseDictThird:(NSDictionary *)dict {
    NSArray *tagResults = dict[@"results"];
    NSMutableArray *tagAllArr = [NSMutableArray array];
    for (int j = 0; j < tagResults.count/3; j ++) {
        
        NSMutableArray *tagArr = [NSMutableArray array];
        
        for (int i = j*3; i < (j + 1)*3 ; i ++) {
            TagModel *model = [[TagModel alloc] init];
            model.id = tagResults[i][@"id"];
            model.name = tagResults[i][@"name"];
            model.images = tagResults[i][@"images"];
            model.priority = tagResults[i][@"priority"];
            model.status = tagResults[i][@"status"];
            model.createTime = tagResults[i][@"createTime"];
            model.modifyTime = tagResults[i][@"modifyTime"];
            model.userID = tagResults[i][@"userID"];
            [tagArr addObject:model];
        }
        [tagAllArr addObject:tagArr];
    }
    
    if (tagResults.count%3 != 0) {
        NSMutableArray *tagArr = [NSMutableArray array];
        for (int m = tagResults.count/3 *3; m < tagResults.count; m++) {
            TagModel *model = [[TagModel alloc] init];
            model.id = tagResults[m][@"id"];
            model.name = tagResults[m][@"name"];
            model.images = tagResults[m][@"images"];
            model.priority = tagResults[m][@"priority"];
            model.status = tagResults[m][@"status"];
            model.createTime = tagResults[m][@"createTime"];
            model.modifyTime = tagResults[m][@"modifyTime"];
            model.userID = tagResults[m][@"userID"];
            [tagArr addObject:model];
            
        }
        [tagAllArr addObject:tagArr];
    }
    return tagAllArr;

}

+ (NSMutableArray *)paraseAllDict:(NSDictionary *)dict{
    NSArray *tagResults = dict[@"results"];
    NSMutableArray *tagAllArr = [NSMutableArray array];
    for (int i = 0; i < tagResults.count; i ++) {
        TagModel *model = [[TagModel alloc] init];
        model.id = tagResults[i][@"id"];
        model.name = tagResults[i][@"name"];
        model.images = tagResults[i][@"images"];
        model.priority = tagResults[i][@"priority"];
        model.status = tagResults[i][@"status"];
        model.createTime = tagResults[i][@"createTime"];
        model.modifyTime = tagResults[i][@"modifyTime"];
        model.userID = tagResults[i][@"userID"];
        [tagAllArr addObject:model];
    }
    return tagAllArr;
}

@end
