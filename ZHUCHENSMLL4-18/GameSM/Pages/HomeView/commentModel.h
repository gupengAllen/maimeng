//
//  commentModel.h
//  GameSM
//
//  Created by 顾鹏 on 16/3/1.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>
@class userOrreplyIDInfo;
@interface commentModel : NSObject
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSString *type;
@property (nonatomic, retain)userOrreplyIDInfo *userIDInfo;
@property (nonatomic, copy)NSString *userID;
@property (nonatomic, retain)userOrreplyIDInfo *replyUserIDInfo;
@property (nonatomic, copy)NSString *replyUserID;
@property (nonatomic, copy)NSString *contentID;
@property (nonatomic, copy)NSString *valueType;
@property (nonatomic, copy)NSString *valueID;
@property (nonatomic, copy)NSString *status;
@property (nonatomic, copy)NSString *createTime;
@property (nonatomic, copy)NSString *modifyTime;
@property (nonatomic, copy)NSString *content;
@property (nonatomic, copy)NSString *ip;
@property (nonatomic, copy)NSString *viewType;
@property (nonatomic, copy)NSString *deviceID;
@property (nonatomic, copy)NSString *createTimeValue;
@property (nonatomic, copy)NSString *contentValue;
//@property (nonatomic, retain)NSMutableArray *faceNameArr;
@property (nonatomic, copy)NSAttributedString *attrStr;
@property (nonatomic, assign)CGRect rect;
+(NSMutableArray *)paraDict:(NSDictionary *)dict;
@end
