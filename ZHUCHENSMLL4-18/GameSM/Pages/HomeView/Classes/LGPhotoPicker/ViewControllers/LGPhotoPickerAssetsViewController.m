//
//  LGPhotoPickerAssetsViewController.m
//  LGPhotoBrowser
//
//  Created by ligang on 15/10/27.
//  Copyright (c) 2015年 L&G. All rights reserved.


#import <AssetsLibrary/AssetsLibrary.h>
#import "LGPhoto.h"
#import "LGPhotoPickerCollectionView.h"
#import "LGPhotoPickerGroup.h"
#import "LGPhotoPickerCollectionViewCell.h"
#import "LGPhotoPickerFooterCollectionReusableView.h"
#import "Tool.h"
#import "Y_X_DataInterface.h"
#import "QiniuSDK.h"
#import "LoginViewController.h"
#import "CustomNavigationController.h"
#import "MBProgressHUD.h"

static CGFloat CELL_ROW = 4;
static CGFloat CELL_MARGIN = 2;
static CGFloat CELL_LINE_MARGIN = 2;
static CGFloat TOOLBAR_HEIGHT = 44;

static NSString *const _cellIdentifier = @"cell";
static NSString *const _footerIdentifier = @"FooterView";
static NSString *const _identifier = @"toolBarThumbCollectionViewCell";
@interface LGPhotoPickerAssetsViewController () <LGPhotoPickerCollectionViewDelegate,UICollectionViewDataSource,LGPhotoPickerBrowserViewControllerDataSource,LGPhotoPickerBrowserViewControllerDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UITextFieldDelegate,LoginAlertViewDelegate>
{
    UITextField *_postAddText;
    BOOL _isMissUp;
}
// View
// 相片View
@property (nonatomic , strong) LGPhotoPickerCollectionView *collectionView;
// 标记View
@property (nonatomic , weak) UILabel *makeView;
@property (nonatomic , strong) UIButton *sendBtn;
@property (nonatomic , strong) UIButton *previewBtn;
@property (nonatomic , weak) UIToolbar *toolBar;
@property (assign,nonatomic) NSUInteger privateTempMaxCount;
@property (nonatomic , strong) NSMutableArray *assets;
@property (nonatomic , strong) NSMutableArray *selectAssets;
@property (strong,nonatomic) NSMutableArray *takePhotoImages;
// 1 - 相册浏览器的数据源是 selectAssets， 0 - 相册浏览器的数据源是 assets
@property (nonatomic, assign) BOOL isPreview;
// 是否发送原图
@property (nonatomic, assign) BOOL isOriginal;

@end

@implementation LGPhotoPickerAssetsViewController
{
    UILabel *titleLabel;
    UIView *postView;
    UIView *bottomView;
    BOOL isUpBottom;
    CGRect p;
    NSIndexPath *ind;
    MBProgressHUD *hud;

}
#pragma mark - dealloc

- (void)dealloc{
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    // 赋值给上一个控制器,以便记录上次选择的照片
//    if (self.selectedAssetsBlock) {
//        self.selectedAssetsBlock(self.selectAssets);
//    }
}


- (instancetype)initWithShowType:(LGShowImageType)showType{
    self = [super init];
    if (self) {
        self.showType = showType;
    }
    return self;
}

#pragma mark - getter
#pragma mark Get Data
- (NSMutableArray *)selectAssets{
    if (!_selectAssets) {
        _selectAssets = [NSMutableArray array];
    }
    return _selectAssets;
}

- (NSMutableArray *)takePhotoImages{
    if (!_takePhotoImages) {
        _takePhotoImages = [NSMutableArray array];
    }
    return _takePhotoImages;
}

#pragma mark Get View
- (UIButton *)sendBtn{
    if (!_sendBtn) {
        UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightBtn setTitleColor:[UIColor colorWithRed:0x45 green:0x9a blue:0x00 alpha:1] forState:UIControlStateNormal];
        [rightBtn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        rightBtn.enabled = YES;
        rightBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        rightBtn.frame = CGRectMake(0, 0, 60, 45);
        NSString *title = [NSString stringWithFormat:@"发送(%d)",0];
        [rightBtn setTitle:title forState:UIControlStateNormal];
        [rightBtn addTarget:self action:@selector(sendBtnTouched) forControlEvents:UIControlEventTouchUpInside];
        self.sendBtn = rightBtn;
    }
    return _sendBtn;
}

- (UIButton *)previewBtn
{
    if (!_previewBtn) {
        UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [leftBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [leftBtn setTitleColor:[UIColor grayColor] forState:UIControlStateDisabled];
        leftBtn.enabled = YES;
        leftBtn.titleLabel.font = [UIFont systemFontOfSize:17];
        leftBtn.frame = CGRectMake(0, 0, 45, 45);
        [leftBtn setTitle:@"预览" forState:UIControlStateNormal];
        [leftBtn addTarget:self action:@selector(previewBtnTouched) forControlEvents:UIControlEventTouchUpInside];
        self.previewBtn = leftBtn;
    }
    return _previewBtn;
}

- (void)previewBtnTouched
{
    [self setupPhotoBrowserInCasePreview:YES CurrentIndexPath:0];
}

/**
 *  跳转照片浏览器
 *
 *  @param preview YES - 从‘预览’按钮进去，浏览器显示的时被选中的照片
 *                  NO - 点击cell进去，浏览器显示所有照片
 *  @param CurrentIndexPath 进入浏览器后展示图片的位置
 */
- (void) setupPhotoBrowserInCasePreview:(BOOL)preview
                       CurrentIndexPath:(NSIndexPath *)indexPath{
    
    self.isPreview = preview;
    // 图片游览器
    LGPhotoPickerBrowserViewController *pickerBrowser = [[LGPhotoPickerBrowserViewController alloc] init];
    pickerBrowser.showType = self.showType;
    // 数据源/delegate
    pickerBrowser.delegate = self;
    pickerBrowser.dataSource = self;
    pickerBrowser.maxCount = self.maxCount;
    pickerBrowser.selectedAssets = [self.selectAssets mutableCopy];
    // 数据源可以不传，传photos数组 photos<里面是ZLPhotoPickerBrowserPhoto>
//    pickerBrowser.photos = self.selectAssets;
    // 是否可以删除照片
    pickerBrowser.editing = NO;
    // 当前选中的值
    pickerBrowser.currentIndexPath = indexPath;    // 展示控制器
//    [pickerBrowser showPickerVc:self];
    [self.navigationController presentViewController:pickerBrowser animated:YES completion:nil];
    
}

- (void)setSelectPickerAssets:(NSArray *)selectPickerAssets{
    NSSet *set = [NSSet setWithArray:selectPickerAssets];
    _selectPickerAssets = [set allObjects];
    
    if (!self.assets) {
        self.assets = [NSMutableArray arrayWithArray:selectPickerAssets];
    }else{
        [self.assets addObjectsFromArray:selectPickerAssets];
    }
    
    self.selectAssets = [selectPickerAssets mutableCopy];
    self.collectionView.lastDataArray = nil;
    self.collectionView.isRecoderSelectPicker = YES;
    self.collectionView.selectAssets = self.selectAssets;
    NSInteger count = self.selectAssets.count;
    self.makeView.hidden = !count;
    self.makeView.text = [NSString stringWithFormat:@"%ld",(long)count];
    self.sendBtn.enabled = (count > 0);
    self.previewBtn.enabled = (count > 0);
    
    [self updateToolbar];
}

- (void)setTopShowPhotoPicker:(BOOL)topShowPhotoPicker{
    _topShowPhotoPicker = topShowPhotoPicker;

    if (self.topShowPhotoPicker == YES) {
        NSMutableArray *reSortArray= [[NSMutableArray alloc] init];
        for (id obj in [self.collectionView.dataArray reverseObjectEnumerator]) {
            [reSortArray addObject:obj];
        }
        
        LGPhotoAssets *lgAsset = [[LGPhotoAssets alloc] init];
        [reSortArray insertObject:lgAsset atIndex:0];
        
        self.collectionView.status = LGPickerCollectionViewShowOrderStatusTimeAsc;
        self.collectionView.topShowPhotoPicker = topShowPhotoPicker;
        self.collectionView.dataArray = reSortArray;
        [self.collectionView reloadData];
    }
}

#pragma mark collectionView
- (LGPhotoPickerCollectionView *)collectionView{
    if (!_collectionView) {
        
        CGFloat cellW = (self.view.frame.size.width - CELL_MARGIN * CELL_ROW + 1) / CELL_ROW;
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(cellW, cellW);
        layout.minimumInteritemSpacing = 0;
        layout.minimumLineSpacing = CELL_LINE_MARGIN;
        layout.footerReferenceSize = CGSizeMake(self.view.frame.size.width, TOOLBAR_HEIGHT * 2);
        
        LGPhotoPickerCollectionView *collectionView = [[LGPhotoPickerCollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        // 时间置顶
        collectionView.status = LGPickerCollectionViewShowOrderStatusTimeDesc;
        collectionView.translatesAutoresizingMaskIntoConstraints = NO;
        [collectionView registerClass:[LGPhotoPickerCollectionViewCell class] forCellWithReuseIdentifier:_cellIdentifier];
        // 底部的View
        [collectionView registerClass:[LGPhotoPickerFooterCollectionReusableView class]  forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:_footerIdentifier];
        
        collectionView.contentInset = UIEdgeInsetsMake(5, 0,TOOLBAR_HEIGHT, 0);
        collectionView.collectionViewDelegate = self;
        [self.view insertSubview:_collectionView = collectionView belowSubview:self.toolBar];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(collectionView);
        
        NSString *widthVfl = @"H:|-0-[collectionView]-0-|";
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:widthVfl options:0 metrics:nil views:views]];
        
        NSString *heightVfl = @"V:|-0-[collectionView]-0-|";
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heightVfl options:0 metrics:nil views:views]];
        
    }
    return _collectionView;
}

//#pragma mark makeView 红点标记View
//- (UILabel *)makeView{
//    if (!_makeView) {
//        UILabel *makeView = [[UILabel alloc] init];
//        makeView.textColor = [UIColor whiteColor];
//        makeView.textAlignment = NSTextAlignmentCenter;
//        makeView.font = [UIFont systemFontOfSize:13];
//        makeView.frame = CGRectMake(-5, -5, 20, 20);
//        makeView.hidden = YES;
//        makeView.layer.cornerRadius = makeView.frame.size.height / 2.0;
//        makeView.clipsToBounds = YES;
//        makeView.backgroundColor = [UIColor redColor];
//        [self.view addSubview:makeView];
//        self.makeView = makeView;
//        
//    }
//    return _makeView;
//}

#pragma mark - circle life

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.view.bounds = [UIScreen mainScreen].bounds;
    self.view.backgroundColor = [UIColor whiteColor];
    _isMissUp = YES;
    // 获取相册
    [self setupAssets];
    
    [self addNavBarCancelButton];
    // 初始化底部ToorBar
    [self setupToorBar];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [backButton setImage:[UIImage imageNamed:@"fanhui_.png"] forState:UIControlStateNormal];
    [backButton setTitle:@"相册" forState:UIControlStateNormal];
    backButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [backButton setTitleColor:[UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(backButton:) forControlEvents:UIControlEventTouchUpInside];
    backButton.frame = CGRectMake(0, 0, 50, 30);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    
    hud = [[MBProgressHUD alloc] initWithView:self.view];
    hud.labelText = @"发布中";
    
    
}

- (void)backButton:(UIButton *)backButton {
    [[LogHelper shared] writeToFilefrom_page:@"iuca" from_section:@"i" from_step:@"r" to_page:@"iuci" to_section:@"i" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%d",0]];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    
}


- (void)viewDidAppear:(BOOL)animated {
    if (_isMissUp) {
        [self createpostImage];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.collectionView reloadData];
}

#pragma mark - 创建右边取消按钮
- (void)addNavBarCancelButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:14.0f];
    [button setTitleColor:[UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1] forState:UIControlStateNormal];
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 6.0, 0, 3.0);
    button.frame = CGRectMake(0, 0, 40, 28.0f);
    [button setTitle:@"取消" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(cancelBtnTouched) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barbutton = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barbutton;
}

#pragma mark 初始化所有的组
- (void) setupAssets{
    if (!self.assets) {
        self.assets = [NSMutableArray array];
    }
    
    __block NSMutableArray *assetsM = [NSMutableArray array];
    __weak typeof(self) weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [[LGPhotoPickerDatas defaultPicker] getGroupPhotosWithGroup:self.assetsGroup finished:^(NSArray *assets) {
            
            [assets enumerateObjectsUsingBlock:^(ALAsset *asset, NSUInteger idx, BOOL *stop) {
                LGPhotoAssets *lgAsset = [[LGPhotoAssets alloc] init];
                lgAsset.asset = asset;
                [assetsM addObject:lgAsset];
            }];
            weakSelf.collectionView.dataArray = assetsM;
            [self.assets setArray:assetsM];
        }];
    });
}

- (void)pickerCollectionViewDidCameraSelect:(LGPhotoPickerCollectionView *)pickerCollectionView{
    
    
    UIImagePickerController *ctrl = [[UIImagePickerController alloc] init];
    ctrl.delegate = self;
    ctrl.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:ctrl animated:YES completion:nil];
    
    
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_PHOTO object:nil];
//    });
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        // 处理
        UIImage *image = info[@"UIImagePickerControllerOriginalImage"];
        
        [self.assets addObject:image];
        [self.selectAssets addObject:image];
        [self.takePhotoImages addObject:image];
        
        NSInteger count = self.selectAssets.count;
        self.makeView.hidden = !count;
        self.makeView.text = [NSString stringWithFormat:@"%ld",(long)count];
        self.sendBtn.enabled = (count > 0);
        self.previewBtn.enabled = (count > 0);
        
        [picker dismissViewControllerAnimated:YES completion:nil];
    }else{
        NSLog(@"请在真机使用!");
    }
}

#pragma mark -初始化底部ToorBar
- (void) setupToorBar{
    UIToolbar *toorBar = [[UIToolbar alloc] init];
    toorBar.translatesAutoresizingMaskIntoConstraints = NO;
    toorBar.barStyle = UIBarStyleBlackTranslucent;
    toorBar.hidden = YES;
    [self.view addSubview:toorBar];
    self.toolBar = toorBar;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(toorBar);
    NSString *widthVfl =  @"H:|-0-[toorBar]-0-|";
    NSString *heightVfl = @"V:[toorBar(44)]-0-|";
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:widthVfl options:0 metrics:0 views:views]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:heightVfl options:0 metrics:0 views:views]];
    
    // 左视图 中间距 右视图
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:self.previewBtn];
    UIBarButtonItem *fiexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.sendBtn];
    
    toorBar.items = @[leftItem,fiexItem,rightItem];
    
}

#pragma mark - setter
-(void)setMaxCount:(NSInteger)maxCount{
    _maxCount = maxCount;
    
    if (!_privateTempMaxCount) {
        _privateTempMaxCount = maxCount;
    }
    
    if (self.selectAssets.count == maxCount){
        maxCount = 0;
    }else if (self.selectPickerAssets.count - self.selectAssets.count > 0) {
        maxCount = _privateTempMaxCount;
    }
    
    self.collectionView.maxCount = maxCount;
}

- (void)setAssetsGroup:(LGPhotoPickerGroup *)assetsGroup{
    if (!assetsGroup.groupName.length) return ;
    
    _assetsGroup = assetsGroup;
    
    
    
//    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0 , SCREEN_WIDTH- 160, 30)];
//    titleLabel.center = CGPointMake(SCREEN_WIDTH/2, 20);
//    titleLabel.backgroundColor = [UIColor clearColor];
//    // titleLabel.font = [UIFont boldSystemFontOfSize:20];
//    titleLabel.text  = assetsGroup.groupName;
//    titleLabel.textAlignment = 1;
//    titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
//    titleLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
//    //    self.navigationController.navigationItem.titleView = titleLabel;
//    [self.navigationController.navigationBar addSubview:titleLabel];
    
    self.title = assetsGroup.groupName;
    
    // 获取Assets
//    [self setupAssets];
}

#pragma mark - LGPhotoPickerCollectionViewDelegate

//cell被点击会调用
- (void) pickerCollectionCellTouchedIndexPath:(NSIndexPath *)indexPath
{
    
    //    UIView * containerView = [transitionContext containerView];
    
    //    NSLog(@"%@ %@",NSStringFromCGRect(snapshotView.frame),NSStringFromCGRect(fromVc.selectedCell.cellImageView.frame));
    
    //3.设置目标控制器的位置，并把透明度设为0，在后面的动画中慢慢显示出来便为1
    //    toVc.view.frame = [transitionContext finalFrameForViewController:toVc];
    //    toVc.view.alpha = 0.0f;
    //4.都添加到 containerVeiw 中，注意顺序不能错了
    //    [self.view addSubview:toVc.view];
    
    //    5, 70, SCREEN_WIDTH - 10, SCREEN_WIDTH - 10)
    
    [[LogHelper shared] writeToFilefrom_page:@"iuc" from_section:@"i" from_step:@"r" to_page:@"iut" to_section:@"i" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%ld",indexPath.row]];
    _isMissUp = NO;
    for (UIView *subView in self.navigationController.navigationBar.subviews) {
        subView.hidden = YES;
    }
    
    
    UIScrollView *postScr = (UIScrollView *)[bottomView viewWithTag:1000];
    UIImageView *postImage = (UIImageView *)[postScr viewWithTag:20000];
    UIImage *image =(UIImage *)[self.assets[indexPath.item] compressionImage];
    CGFloat potation = (image.size.height/image.size.width);
    //        postScr.contentSize = CGSizeMake(0, (SCREEN_WIDTH - 10)/potation);
    //        if (potation * (SCREEN_WIDTH - 10) < (SCREEN_WIDTH - 10)) {
    //            postImage.center = CGPointMake(postScr.width/2, postScr.height/2);
    //        }
    postImage.image = image;
    
    LGPhotoPickerCollectionViewCell *cell = (LGPhotoPickerCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    //    postImage.frame = [self.view convertRect:cell.frame fromView:_collectionView];
    p = [self.view convertRect:cell.frame fromView:_collectionView];
    
    postImage.frame = p;
    postScr.hidden = NO;
    postView.hidden = NO;
    bottomView.hidden = NO;
    ind = indexPath;
    
    
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        bottomView.alpha = 1.0;
        postImage.frame = CGRectMake(0, 0, (SCREEN_WIDTH - 10)/potation,  (SCREEN_WIDTH - 10));
        postImage.center = CGPointMake( (SCREEN_WIDTH - 10)/2,  (SCREEN_WIDTH - 10)/2);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.6 animations:^{
            postView.alpha = 0.7;
            
        } completion:^(BOOL finished) {
            
        }];
        
    }];
    
//    [self setupPhotoBrowserInCasePreview:NO CurrentIndexPath:indexPath];
//    for (UIView *subView in self.navigationController.navigationBar.subviews) {
//        subView.hidden = YES;
//    }
//    
//    ind = indexPath;
////    CGPoint p = [self.view co]
//    p = [self.view convertPoint:CGPointMake(cell.frame.origin.x, cell.frame.origin.y) fromView:_collectionView];
//    CGPoint p2 = CGPointMake(p.x + SCREEN_WIDTH/8, p.y + SCREEN_WIDTH/8);
//    bottomView.center = p2;
//    postView.center = p2;
//    postView.hidden = NO;
//    bottomView.hidden = NO;
//    UIScrollView *postScr = (UIScrollView *)[bottomView viewWithTag:1000];
//    UIImageView *postImage = (UIImageView *)[postScr viewWithTag:20000];
//    UIImage *image =(UIImage *)[self.assets[indexPath.item] compressionImage];
//    CGFloat potation = (image.size.height/image.size.width);
//    postScr.contentSize = CGSizeMake(0, potation * (SCREEN_WIDTH - 10));
//    postImage.frame = CGRectMake(0, 0, SCREEN_WIDTH - 10, potation * (SCREEN_WIDTH - 10));
//    if (potation * (SCREEN_WIDTH - 10) < (SCREEN_WIDTH - 10)) {
//        postImage.center = CGPointMake(postScr.width/2, postScr.height/2);
//    }
//    postImage.image = image;
////    bottomView.transform =  CGAffineTransformScale(bottomView.transform, 0.1, 0.1);
////    postView.transform =  CGAffineTransformScale(postView.transform, 0.1, 0.1);
//    
//
//    [UIView animateWithDuration:0.4 animations:^{
//        
//        bottomView.transform =CGAffineTransformScale(bottomView.transform, 10, 10);
//        postView.transform =CGAffineTransformScale(postView.transform, 10, 10);
//        bottomView.center = CGPointMake(SCREEN_WIDTH/2, (SCREEN_HEIGHT+20)/2);
//        postView.center = CGPointMake(SCREEN_WIDTH/2,  (SCREEN_HEIGHT+20)/2);
//        
//        
//    } completion:^(BOOL finished) {
//        
//    }];
//    
    
    
    
}

- (UIImage *)cutImage:(UIImage*)image
{
    //压缩图片
    CGSize newSize;
    CGImageRef imageRef = nil;
    
    if ((image.size.width / image.size.height) < 1) {
        newSize.width = image.size.width;
        newSize.height = image.size.width;
        
        imageRef = CGImageCreateWithImageInRect([image CGImage], CGRectMake(0, fabs(image.size.height - newSize.height) / 2, newSize.width, newSize.height));
        
    } else {
        newSize.height = image.size.height;
        newSize.width = image.size.height ;
        
        imageRef = CGImageCreateWithImageInRect([image CGImage], CGRectMake(fabs(image.size.width - newSize.width) / 2, 0, newSize.width, newSize.height));
        
    }
    
    return [UIImage imageWithCGImage:imageRef];
}

//cell的右上角选择框被点击会调用
- (void) pickerCollectionViewDidSelected:(LGPhotoPickerCollectionView *) pickerCollectionView deleteAsset:(LGPhotoAssets *)deleteAssets{
    
    if (self.selectPickerAssets.count == 0){
        self.selectAssets = [NSMutableArray arrayWithArray:pickerCollectionView.selectAssets];
        
    } else if (deleteAssets == nil) {
        [self.selectAssets addObject:[pickerCollectionView.selectAssets lastObject]];
    } else if(deleteAssets) { //取消所选的照片
        //根据url删除对象
        for (LGPhotoAssets *selectAsset in self.selectPickerAssets) {
            if ([selectAsset.assetURL isEqual:deleteAssets.assetURL]) {
                [self.selectAssets removeObject:selectAsset];
            }
        }
    }

    [self updateToolbar];
}

- (void)updateToolbar
{
    NSInteger count = self.selectAssets.count;
    self.sendBtn.enabled = (count > 0);
    self.previewBtn.enabled = (count > 0);
    NSString *title = [NSString stringWithFormat:@"确定(%ld)",(long)count];
    [self.sendBtn setTitle:title forState:UIControlStateNormal];
}

#pragma mark - UICollectionViewDataSource
- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.selectAssets.count;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identifier forIndexPath:indexPath];
    
    if (self.selectAssets.count > indexPath.item) {
        UIImageView *imageView = [[cell.contentView subviews] lastObject];
        // 判断真实类型
        if (![imageView isKindOfClass:[UIImageView class]]) {
            imageView = [[UIImageView alloc] initWithFrame:cell.bounds];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            imageView.clipsToBounds = YES;
            [cell.contentView addSubview:imageView];
        }
        imageView.tag = indexPath.item;
        if ([self.selectAssets[indexPath.item] isKindOfClass:[LGPhotoAssets class]]) {
            imageView.image = [self.selectAssets[indexPath.item] thumbImage];
        }else if ([self.selectAssets[indexPath.item] isKindOfClass:[UIImage class]]){
            imageView.image = (UIImage *)self.selectAssets[indexPath.item];
        }
    }
    
    return cell;
}

#pragma mark - LGPhotoPickerBrowserViewControllerDataSource

- (NSInteger)numberOfSectionInPhotosInPickerBrowser:(LGPhotoPickerBrowserViewController *)pickerBrowser{
    return 1;
}

- (NSInteger)photoBrowser:(LGPhotoPickerBrowserViewController *)photoBrowser numberOfItemsInSection:(NSUInteger)section{
    if (self.isPreview) {
        return self.selectAssets.count;
    } else {
        return self.assets.count;
    }
}

- (LGPhotoPickerBrowserPhoto *)photoBrowser:(LGPhotoPickerBrowserViewController *)pickerBrowser photoAtIndexPath:(NSIndexPath *)indexPath
{
    LGPhotoAssets *imageObj = [[LGPhotoAssets alloc] init];
    if (self.isPreview && self.selectAssets.count) {
        imageObj = [self.selectAssets objectAtIndex:indexPath.row];
    } else if (!self.isPreview && self.assets.count){
        imageObj = [self.assets objectAtIndex:indexPath.row];
    }
    // 包装下imageObj 成 LGPhotoPickerBrowserPhoto 传给数据源
    LGPhotoPickerBrowserPhoto *photo = [LGPhotoPickerBrowserPhoto photoAnyImageObjWith:imageObj];
    
    
    LGPhotoPickerCollectionViewCell *cell = (LGPhotoPickerCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
    photo.thumbImage = cell.cellImage;
    
    return photo;
}

#pragma mark - LGPhotoPickerBrowserViewControllerDelegate

- (void)photoBrowserWillExit:(LGPhotoPickerBrowserViewController *)pickerBrowser
{
    self.selectAssets = [NSMutableArray arrayWithArray:pickerBrowser.selectedAssets];
    self.collectionView.lastDataArray = nil;
    self.collectionView.isRecoderSelectPicker = YES;
    self.collectionView.selectAssets = self.selectAssets;
    NSInteger count = self.selectAssets.count;
    self.makeView.hidden = !count;
    self.makeView.text = [NSString stringWithFormat:@"%ld",(long)count];
    self.sendBtn.enabled = (count > 0);
    self.previewBtn.enabled = (count > 0);
    [self updateToolbar];
}

- (void)photoBrowserSendBtnTouched:(LGPhotoPickerBrowserViewController *)pickerBrowser isOriginal:(BOOL)isOriginal
{
    self.isOriginal = isOriginal;
    self.selectAssets = pickerBrowser.selectedAssets;
    [self sendBtnTouched];
}

#pragma mark -<Navigation Actions>
#pragma mark -开启异步通知
- (void) cancelBtnTouched{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) sendBtnTouched {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        [[NSNotificationCenter defaultCenter] postNotificationName:PICKER_TAKE_DONE object:nil userInfo:@{@"selectAssets":self.selectAssets,@"isOriginal":@(self.isOriginal)}];
//        NSLog(@"%@",@(self.isOriginal));
//    });
//    [self dismissViewControllerAnimated:NO completion:nil];
    
    
    
}

- (void)createpostImage {
    postView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT + 20)];
    postView.backgroundColor = [UIColor blackColor];
    postView.alpha = 0.0;
    postView.hidden = YES;
    [self.navigationController.view addSubview:postView];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH , SCREEN_HEIGHT + 20)];
    bottomView.backgroundColor = [UIColor clearColor];
    bottomView.hidden = YES;
    bottomView.alpha = 0.0;
    [self.navigationController.view addSubview:bottomView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignKeyBoard1)];
    [bottomView addGestureRecognizer:tap];
    
    UIButton *postBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [postBackButton setImage:[UIImage imageNamed:@"guanbi"] forState:UIControlStateNormal];
    [postBackButton addTarget:self action:@selector(postBackButton:) forControlEvents:UIControlEventTouchUpInside];
    postBackButton.frame = CGRectMake(SCREEN_WIDTH - 60, 28, 30, 30);
    [bottomView addSubview:postBackButton];
    
    UILabel *postLabel = [[UILabel alloc] initWithFrame:CGRectMake(80, 0 , SCREEN_WIDTH- 160, 30)];
    postLabel.center = CGPointMake(SCREEN_WIDTH/2, 45);
    postLabel.backgroundColor = [UIColor clearColor];
    // titleLabel.font = [UIFont boldSystemFontOfSize:20];
    postLabel.text  =@"增加标签";
    postLabel.textAlignment = 1;
    postLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20];
    postLabel.textColor = [UIColor colorWithRed:241/255.0 green:48/255.0 blue:73/255.0 alpha:1.0];
    //    self.navigationController.navigationItem.titleView = titleLabel;
    [bottomView addSubview:postLabel];
    
    UIScrollView *postImageScr = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 70, SCREEN_WIDTH - 10, SCREEN_WIDTH - 10)];
    if (SCREEN_HEIGHT == 460) {
        postImageScr.frame = CGRectMake(5, 70, SCREEN_WIDTH - 10, SCREEN_WIDTH - 80);
    }
    postImageScr.hidden = YES;
    postImageScr.layer.cornerRadius = 5.0;
    postImageScr.showsVerticalScrollIndicator = NO;
    postImageScr.tag = 1000;
    [bottomView addSubview:postImageScr];
    
    
    UIImageView *postImage = [[UIImageView alloc] initWithFrame:CGRectMake(5, 70, SCREEN_WIDTH - 10, SCREEN_WIDTH - 10)];
    postImage.layer.cornerRadius = 5.0;
    postImage.backgroundColor = [UIColor redColor];
    postImage.tag = 20000;
    postImage.userInteractionEnabled = YES;
    postImage.contentMode = UIViewContentModeScaleAspectFill;
    [postImageScr addSubview:postImage];
    
    UIView *postAddTextView = [[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(postImageScr.frame)+ 5, CGRectGetWidth(postImageScr.frame), 50)];
    postAddTextView.layer.cornerRadius = 5.0;
    postAddTextView.backgroundColor = [UIColor whiteColor];
    [bottomView addSubview:postAddTextView];
                                                                       
                                                                       
    
    
    UITextField *postAddText = [[UITextField alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(postImageScr.frame)+ 10, CGRectGetWidth(postImageScr.frame) - 6, 40)];
    postAddText.tag = 10000;
    postAddText.returnKeyType = UIReturnKeyGo;
    postAddText.delegate = self;
    postAddText.layer.cornerRadius = 5.0;
    postAddText.placeholder = @" 增加标签";
    postAddText.enablesReturnKeyAutomatically = YES;
    _postAddText = postAddText;
    [bottomView addSubview:postAddText];
    [postAddText addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
    
    
    UILabel *postDes = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(postAddText.frame) + 8, CGRectGetWidth(postAddText.frame), 50)];
    postDes.numberOfLines = 2;
    postDes.textColor =  [UIColor colorWithRed:241/255.0 green:48/255.0 blue:73/255.0 alpha:1.0];
    postDes.text = @"标签可以添加多个,每个标签中请用','分隔,例如'灌篮高手,樱木花道'";
    [bottomView addSubview:postDes];
    
    UIButton *postButton = [UIButton buttonWithType:UIButtonTypeCustom];
    postButton.frame = CGRectMake(5, SCREEN_HEIGHT - 45 *__Scale, CGRectGetWidth(postDes.frame), 50);
    [postButton setTitle:@"发布" forState:UIControlStateNormal];
    [postButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    postButton.enabled = NO;
    [postButton setBackgroundImage:[self imageWithColor:[UIColor grayColor]] forState:UIControlStateDisabled];
    [postButton setBackgroundImage:[self imageWithColor:[UIColor colorWithRed:241/255.0 green:48/255.0 blue:73/255.0 alpha:1.0]] forState:UIControlStateNormal];
    postButton.backgroundColor = [UIColor colorWithRed:241/255.0 green:48/255.0 blue:73/255.0 alpha:1.0];
    postButton.layer.cornerRadius = 5.0;
    postButton.layer.masksToBounds = YES;
    postButton.tag = 1;
    [postButton addTarget:self action:@selector(upload) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:postButton];
    
//    bottomView.transform =  CGAffineTransformScale(bottomView.transform, 0.1, 0.1);
//    postView.transform =  CGAffineTransformScale(postView.transform, 0.1, 0.1);
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)postBackButton:(UIButton *)btn {
    for (UIView *subview in self.navigationController.navigationBar.subviews) {
        subview.hidden = NO;
    }
    
    UIScrollView *postScr = (UIScrollView *)[bottomView viewWithTag:1000];
    [UIView animateWithDuration:0.6 animations:^{
        
//        postImage.frame = p;
        postView.alpha = 0.0;
        bottomView.alpha = 0.0;

        
    } completion:^(BOOL finished) {
        bottomView.hidden = YES;
        postView.hidden = YES;
    }];
    
}
- (void)resignKeyBoard1{
    [_postAddText resignFirstResponder];
}
- (void)resignKeyBoard{
    if (isUpBottom) {
        UIScrollView *postScr = (UIScrollView *)[self.navigationController.view viewWithTag:1000];

        UITextField *te = (UITextField *)[bottomView viewWithTag:10000];
        [te resignFirstResponder];
        [UIView animateWithDuration:0.2 animations:^{
            bottomView.frame = CGRectMake(bottomView.frame.origin.x, bottomView.frame.origin.y + 250, bottomView.frame.size.width, bottomView.frame.size.height);
//            postScr.frame = CGRectMake(postScr.frame.origin.x, postScr.frame.origin.y + 250, postScr.frame.size.width, postScr.frame.size.height);
            
        }];
        isUpBottom = NO;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
//    isUpBottom = YES;
//    UIScrollView *postScr = (UIScrollView *)[self.navigationController.view viewWithTag:1000];

    [UIView animateWithDuration:0.2 animations:^{
        bottomView.frame = CGRectMake(bottomView.frame.origin.x, bottomView.frame.origin.y - 250, bottomView.frame.size.width, bottomView.frame.size.height);
//        postScr.frame = CGRectMake(postScr.frame.origin.x, postScr.frame.origin.y - 250, postScr.frame.size.width, postScr.frame.size.height);
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
//    [UIView animateWithDuration:0.2 animations:^{
//        bottomView.frame = CGRectMake(bottomView.frame.origin.x, bottomView.frame.origin.y + 250, bottomView.frame.size.width, bottomView.frame.size.height);
//    }];
//    isUpBottom = NO;
//    [textField resignFirstResponder];

    if (!g_App.userID) {
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以上传图片"];
        [alertView show];
    }else{
      [self upload];
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    isUpBottom = YES;
    [self resignKeyBoard];
}

//- (void)loginView {
//    LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//    [loginView setSkipNext:^{
////        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
////        UserUpLoadPicViewController *useruploadPic = [[UserUpLoadPicViewController alloc]init];
////        [self.navigationController pushViewController:useruploadPic animated:YES];
//        [self upload];
//        
//    }];
//    
//    CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
//    [self presentViewController:nav animated:YES completion:nil];
//}


- (void)textFieldChanged:(UITextField *)text {
    UIButton *b = (UIButton *)[bottomView viewWithTag:1];
    NSString *text1 = text.text;
    if (text1.length > 0) {
        b.enabled = YES;
    }else {
        b.enabled = NO;
    }
}

- (void)upload{
    
    if (!g_App.userID) {
        [[LogHelper shared] writeToFilefrom_page:@"il" from_section:@"i" from_step:@"l" to_page:@"nl" to_section:@"i" to_step:@"r" type:@"upload" id:[NSString stringWithFormat:@"%d",0]];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以上传图片"];
        [alertView show];
        return;
    }
    [[LogHelper shared] writeToFilefrom_page:@"iut" from_section:@"i" from_step:@"r" to_page:@"iu" to_section:@"i" to_step:@"a" type:@"" id:[NSString stringWithFormat:@"%d",0]];
    
    UIImage* image = [self.assets[ind.item] originImage];
    NSData *data;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"1"]) {
        data = UIImageJPEGRepresentation(image,1.0);
    }else{
        data = UIImageJPEGRepresentation(image, 0.1);
    }
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDir;
    if (![fileManager fileExistsAtPath:[NSString stringWithFormat:@"%@/avator", [Tool dataFilePath:nil]] isDirectory:&isDir]) {
        
        [fileManager createDirectoryAtPath:[NSString stringWithFormat:@"%@/avator", [Tool dataFilePath:nil]] withIntermediateDirectories:YES attributes:nil error:nil];
    }
    
    [fileManager createFileAtPath:[Tool dataFilePath:@"/avator/upload.jpg"] contents:data attributes:nil];
    
//    [picker dismissViewControllerAnimated:YES completion:nil];
    [self getUploadTokenForQiniu:[Tool dataFilePath:@"/avator/upload.jpg"]];
    
}


- (void)getUploadTokenForQiniu:(NSString*)imageStr{
    [self.navigationController.view addSubview:hud];
    
    [hud show:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_SYSTEM_GETUPTOKEN,@"r",
                                    [Tool computeMD5HashOfFileInPath:imageStr],@"md5",
                                    [NSString stringWithFormat:@"%lld",[Tool fileSizeAtPath:imageStr]],@"filesize",
                                    @"png",@"filetype",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(getUploadTokenForQiniuFinish:) method:POSTDATA];
}

- (void)getUploadTokenForQiniuFinish:(NSDictionary*)dic {
    //    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UITextField *te = (UITextField *)[bottomView viewWithTag:10000];
    
    if (![[dic objectForKey:@"code"] integerValue]) {
//        if (![[[dic objectForKey:@"results"] objectForKey:@"fileExist"] boolValue]) {
            QNUploadManager *upManager = [[QNUploadManager alloc] init];
            NSData *data = [NSData dataWithContentsOfMappedFile:[Tool dataFilePath:@"/avator/upload.jpg"]];
            
            [upManager putData:data
                           key:[[[dic objectForKey:@"results"] objectForKey:@"saveKey"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                         token:[[[dic objectForKey:@"results"] objectForKey:@"uploadToken"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                      complete: ^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                          NSLog(@"%@", info);
                          NSLog(@"%@", resp);
                          
                          
                          NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                          (NSString *)API_URL_UPLOADNICEPIC,@"r",
                                                          [[dic objectForKey:@"results"] objectForKey:@"urlDownload"],@"images",
                                                          [self changeStr:te.text],@"label",nil];
                          [YK_API_request startLoad:INTERFACE_PREFIXD extraParams:exprame object:self action:@selector(UpLoadFinish:) method:POSTDATA];
                          
                      } option:nil];
//        }
    }else{
            [hud hide:YES];
        
        }
    
}

-(NSString *)changeStr:(NSString *)str{
    NSMutableString *convertedString = [str mutableCopy];
    CFStringTransform((CFMutableStringRef)convertedString, NULL, kCFStringTransformFullwidthHalfwidth, false);
    return convertedString;
}

- (void)UpLoadFinish:(NSDictionary *)dict {
    
//    for (UIView *subView in self.navigationController.navigationBar.subviews) {
//        subView.hidden = YES;
//    }
//    
//    
//    UIScrollView *postScr = (UIScrollView *)[bottomView viewWithTag:1000];
//    UIImageView *postImage = (UIImageView *)[postScr viewWithTag:20000];
//    UIImage *image =(UIImage *)[self.assets[indexPath.item] compressionImage];
//    CGFloat potation = (image.size.height/image.size.width);
//    postScr.contentSize = CGSizeMake(0, potation * (SCREEN_WIDTH - 10));
//    if (potation * (SCREEN_WIDTH - 10) < (SCREEN_WIDTH - 10)) {
//        postImage.center = CGPointMake(postScr.width/2, postScr.height/2);
//    }
//    postImage.image = image;
//    
//    LGPhotoPickerCollectionViewCell *cell = (LGPhotoPickerCollectionViewCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
//    postImage.frame = [self.view convertRect:cell.frame fromView:_collectionView];
//    
//    
//    postView.hidden = NO;
//    bottomView.hidden = NO;
//    
//    
//    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
//        bottomView.alpha = 1.0;
//        postImage.frame = CGRectMake(0, 0, SCREEN_WIDTH - 10, potation * (SCREEN_WIDTH - 10));
//    } completion:^(BOOL finished) {
//        [UIView animateWithDuration:0.6 animations:^{
//            postView.alpha = 0.7;
//            
//        } completion:^(BOOL finished) {
//            
//        }];
//        
//    }];
    

    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"picUpLoadSuccessed" object:@""];
//    [MBProgressHUD hideAllHUDsForView:self.navigationController.view animated:YES];
    [hud hide:YES];
    
    for (UIView *subview in self.navigationController.navigationBar.subviews) {
        subview.hidden = NO;
    }
    
    
        UIScrollView *postScr = (UIScrollView *)[bottomView viewWithTag:1000];
//        UIImageView *postImage = (UIImageView *)[postScr viewWithTag:20000];

    [UIView animateWithDuration:0.4 animations:^{
        
//            postImage.frame = p;
        postView.alpha = 0.0;
        bottomView.alpha = 0.0;

        
    } completion:^(BOOL finished) {
        bottomView.hidden = YES;
        postView.hidden = YES;

        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"TOROOT" object:nil];
        
    }];

}

#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {

        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
//        [loginView setSkipNext:^{
//                isUpBottom = YES;
//            [self resignKeyBoard];
//        }];
        [_postAddText resignFirstResponder];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    
    return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
    
}

- (BOOL)shouldAutorotate

{
    
    return NO;
    
}

- (NSUInteger)supportedInterfaceOrientations

{
    
    return UIInterfaceOrientationMaskPortrait;//只支持这一个方向(正常的方向)
    
}

@end
