//
//  AllTagTableViewController.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/26.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "AllTagTableViewController.h"
#import "PullingRefreshTableView.h"
#import "ThreeTagTableViewCell.h"
#import "TagModel.h"
@interface AllTagTableViewController ()<PullingRefreshTableViewDelegate,UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) PullingRefreshTableView *tagTableView;
@property (nonatomic, retain)NSMutableArray *tagArr;

@end

@implementation AllTagTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self addBackBtn];
    [self initTitleName:@"全部标签"];

    [self createTableView];
    [self loadTagData];
}

-(void)loadTagData{
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_TAG,@"r",
                                    @"1",@"page",
                                    @"999",@"size",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(requireTagFinish:) method:GETDATA];
}

- (void)requireTagFinish:(NSDictionary *)dict {
    _tagArr = [TagModel paraseDictThird:dict];
    [_tagTableView reloadData];
}


-(void)createTableView{
    self.tagTableView = [[PullingRefreshTableView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
    self.tagTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tagTableView.dataSource = self;
    self.tagTableView.delegate = self;
    [self.view addSubview:self.tagTableView];
}


#pragma mark - tableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _tagArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"infoTagCell";
    ThreeTagTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"ThreeTagTableViewCell" owner:self options:nil] lastObject];
    }
    [cell setTagArr:_tagArr[indexPath.row]];
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
