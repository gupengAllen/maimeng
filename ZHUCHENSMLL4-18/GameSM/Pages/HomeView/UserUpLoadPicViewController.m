//
//  UserUpLoadPicViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/20.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "UserUpLoadPicViewController.h"
#import "HZPhotoBrowser.h"
#import "HZPhotoItemModel.h"
#import "CategeryCell.h"
#import "CustomButton.h"
#import "EGORefreshTableHeaderView.h"
#import "LGPhotoPickerViewController.h"
#import "UserPictureWhoPraiseViewController.h"
@interface UserUpLoadPicViewController ()<PSCollectionViewDelegate,PSCollectionViewDataSource, PullPsCollectionViewDelegate,UIScrollViewDelegate,EGORefreshTableHeaderDelegate,LGPhotoPickerViewControllerDelegate>
{
    int         _page;
    UIButton    *_tipBtn;
    UILabel     *_tipLabel;
}
@end

@implementation UserUpLoadPicViewController

-(void)viewWillAppear:(BOOL)animated{
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    

}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"picUpLoadSuccessed" object:nil];
//      [[NSNotificationCenter defaultCenter] removeObserver:self name:@"TOROOT" object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addBackBtn];
    [self addUpLoadBtn];
    [self initTitleName:[NSString stringWithFormat:@"%@",g_App.userName]];
    // Do any additional setup after loading the view.
    self.picArray = [NSMutableArray array];
    _page = 1;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:@"picUpLoadSuccessed" object:nil];
//     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backToRoot) name:@"TOROOT" object:nil];
    [self initView];
    [self prettyImageList];
}
-(void)addUpLoadBtn{
    CustomButton *btn = [CustomButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:@"tianjai"] forState:UIControlStateNormal];
    btn.bounds = CGRectMake(0, 0, 40, 40);
    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, -10);
//    btn.imageEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [self addRightItemWithButton:btn itemTarget:self action:@selector(jumpToUpLoadView:)];
}
-(void)jumpToUpLoadView:(UIButton *)btn{
    [[LogHelper shared] writeToFilefrom_page:@"ipl" from_section:@"i" from_step:@"l" to_page:@"iuc" to_section:@"i" to_step:@"r" type:@"" id:[NSString stringWithFormat:@"%d",0]];
    LGPhotoPickerViewController *pickerVc = [[LGPhotoPickerViewController alloc] initWithShowType:LGShowImageTypeImageBroswer];
    pickerVc.status = PickerViewShowStatusCameraRoll;
    pickerVc.maxCount = 1;   // 最多能选9张图片
    pickerVc.delegate = self;
    //    self.showType = style;
    [pickerVc showPickerVc:self];
}
#pragma mark - 刷新 加载

#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)doneLoadingTableViewData{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_collectionView];
    [self refreshData];
}
//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:1.0];
}

-(void)refreshData
{
    [self loadData:NO];
}

-(void)loadMoreData
{
    [self loadData:YES];
}
-(void)loadData:(BOOL)isOrmore{
    if (isOrmore == YES) {
        _page++;
    } else {
        _page = 1;
        [self.picArray removeAllObjects];
    }
    [self prettyImageList];
}

#pragma mark - 界面初始化
- (PullPsCollectionView*)collectionView {
    if (!_collectionView) {
        _collectionView = [[PullPsCollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight)];
        _collectionView.collectionViewDelegate = self;
        _collectionView.collectionViewDataSource = self;
        _collectionView.delegate=self;
        _collectionView.backgroundColor = [UIColor clearColor];
        _collectionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _collectionView.showsVerticalScrollIndicator = YES;
        
        _collectionView.numColsPortrait = 2;
        _collectionView.numColsLandscape = 2;
        _collectionView.pullArrowImage = [UIImage imageNamed:@"blueArrow.png"];
        _collectionView.pullBackgroundColor = [UIColor clearColor];
        _collectionView.pullTextColor = [UIColor blackColor];
    }
    return _collectionView;
}
- (void)initView {
    
    self.collectionView.frame = CGRectMake(0, 0, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    [self.view addSubview:self.collectionView];

    if (_refreshHeaderView == nil) {
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - ScreenSizeHeight, ScreenSizeWidth, ScreenSizeHeight)];
        view.delegate = self;
        [_collectionView addSubview:view];
        _refreshHeaderView = view;
    }
    _tipBtn = [[UIButton alloc]initWithFrame:CGRectMake(3, 5, ScreenSizeWidth/2.0-5, ScreenSizeWidth/2.0-5)];
    [_tipBtn setImage:[UIImage imageNamed:@"shangchuan"] forState:UIControlStateNormal];
    _tipBtn.backgroundColor = RGBACOLOR(230, 230, 230, 1.0);
    [_tipBtn addTarget:self action:@selector(jumpToUpLoadView:) forControlEvents:UIControlEventTouchUpInside];
    _tipBtn.hidden = YES;
    [self.view addSubview:_tipBtn];
    _tipLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, ScreenSizeWidth/2.0+10, ScreenSizeWidth-40, 100)];
    _tipLabel.backgroundColor = [UIColor clearColor];
    _tipLabel.text = @"上传的美图都会保存在这里，点击加号上传美图吧";
    _tipLabel.textAlignment = 0;
    _tipLabel.hidden = YES;
    _tipLabel.textColor  = RGBACOLOR(148, 148, 148, 1.0);
    _tipLabel.font = [UIFont systemFontOfSize:20];
    _tipLabel.numberOfLines = 0;
    [self.view addSubview:_tipLabel];
}

#pragma mark - 加载数据
- (void)prettyImageList {

    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"prettyImages/getUserUploadImageList",@"r",
                                    [NSString stringWithFormat:@"%d",_page],@"page",
                                    @"10",@"size",nil];//[NSString stringWithFormat:@"%d",page]

    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(prettyImageListFinish:) method:GETDATA];
}

- (void)prettyImageListFinish:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.picArray addObject:item];
        }
        [self.collectionView reloadData];
        
        if (self.picArray.count == 0) {
            _tipBtn.hidden = NO;
            _tipLabel.hidden = NO;
        }else{
            _tipBtn.hidden = YES;
            _tipLabel.hidden = YES;
        }
    }
}

#pragma mark - PSCollectionViewDelegate
- (PSCollectionViewCell *)collectionView:(PSCollectionView *)collectionView viewAtIndex:(NSInteger)index
{
    CategeryCell *cell = (CategeryCell*)[collectionView dequeueReusableView];
    if (!cell) {
        cell = [[CategeryCell alloc] initWithFrame:CGRectMake(0, 0, ScreenSizeWidth/2 - 5, 30)];
    }
    [cell.categoryImage setImageWithURL:[NSURL URLWithString:[[self.picArray objectAtIndex:index] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    cell.praiseNum.text = [NSString stringWithFormat:@"%@",[[self.picArray objectAtIndex:index] objectForKey:@"praiseCount"]];
    [cell.userImage setImageWithURL:[NSURL URLWithString:[[self.picArray objectAtIndex:index] objectForKey:@"userIDInfo"][@"images"]] placeholderImage:[UIImage imageNamed:@"jiazaitu_2"]];
    cell.userName.text = [[self.picArray objectAtIndex:index] objectForKey:@"userIDInfo"][@"name"];
    cell.praCount.text = [[self.picArray objectAtIndex:index] objectForKey:@"praiseCount"];
    cell.commentCount.text = [[self.picArray objectAtIndex:index] objectForKey:@"contentCount"];
    return cell;
}

- (CGFloat)heightForViewAtIndex:(NSInteger)index
{
    if (self.picArray.count) {
        if (![[[self.picArray objectAtIndex:index] objectForKey:@"width"] integerValue]) {
            return ScreenSizeWidth / 2 - 5;
        } else {
            return [[[self.picArray objectAtIndex:index] objectForKey:@"height"] integerValue] * (1.0*(ScreenSizeWidth / 2 - 5) / [[[self.picArray objectAtIndex:index] objectForKey:@"width"] integerValue]);
        }
    } else {
        return ScreenSizeWidth / 2 - 5;
    }
}

- (void)collectionView:(PSCollectionView *)collectionView didSelectView:(PSCollectionViewCell *)view atIndex:(NSInteger)index
{
    
    UserPictureWhoPraiseViewController *userP = [[UserPictureWhoPraiseViewController alloc]init];
    userP.allDataArr = self.picArray;
    userP.initNum = (int)index;
    userP.type = 11;
    [self presentViewController:userP animated:YES completion:nil];
}


- (NSInteger)numberOfViewsInCollectionView:(PSCollectionView *)collectionView
{
    return [self.picArray count];
}

#pragma mark - photorowser
////临时占位图（thumbnail图
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}
////高清原图 （bmiddle图）
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[self.currentPicArray objectAtIndex:index]];
}
//
- (NSString*)photoDrowserWithIndex:(NSInteger)index {
    return [[self.picArray objectAtIndex:index] objectForKey:@"id"];
}

#pragma mark - scrolldelegate
//停止拖动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    CGSize size1 = _collectionView.contentSize;
    CGFloat maximumOffset = size1.height;
    NSLog(@"====%f,%f",_collectionView.contentOffset.y,maximumOffset);
    if([scrollView isKindOfClass:[PullPsCollectionView class]]) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
        if (_collectionView.contentOffset.y>=maximumOffset - ScreenSizeHeight ) {
            [self loadData:YES];
        }
    }
}

- (void)backToRoot{
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
