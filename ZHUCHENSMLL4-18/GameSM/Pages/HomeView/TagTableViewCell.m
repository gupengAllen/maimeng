//
//  TagTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/21.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "TagTableViewCell.h"
#import "UIButton+WebCache.h"
#import "TagModel.h"
#import "UIView+Addtion.h"
#import "InfoTagViewController.h"
@implementation TagTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setTagArr:(NSMutableArray *)tagArr {
    _tagArr = tagArr;
    for (int i = 0; i < tagArr.count; i ++) {
        UIButton *tagButton = [UIButton buttonWithType:UIButtonTypeCustom];
        tagButton.frame = CGRectMake(15 + (50 + (ScreenSizeWidth - 230)/3) * i, 10, 50, 50);
        [tagButton sd_setBackgroundImageWithURL:[NSURL URLWithString:[tagArr[i] images]] forState:UIControlStateNormal];
        [tagButton addTarget:self action:@selector(tagButton:) forControlEvents:UIControlEventTouchUpInside];
        tagButton.tag = i;
        [self.contentView addSubview:tagButton];
        
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(tagButton.frame) - 5, CGRectGetMaxY(tagButton.frame) + 11, 60, 14)];
        tagLabel.text = [tagArr[i] name];
        tagLabel.font = CUSTOMTITLESIZE14;
        tagLabel.textAlignment = 1;
        [self.contentView addSubview:tagLabel];
    }
}

- (void)tagButton:(UIButton *)button {
    [[LogHelper shared] writeToFilefrom_page:@"ml" from_section:@"m" from_step:@"l" to_page:@"mldl" to_section:@"m" to_step:@"l" type:@"" id:@"0" detail:@{@"keyword":[_tagArr[button.tag] name]}];
    InfoTagViewController *infoTag = [[InfoTagViewController alloc] init];
    infoTag.tagName = [_tagArr[button.tag] name];
    [self.viewController.navigationController pushViewController:infoTag animated:YES];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
