//
//  tagCollectionCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/21.
//  Copyright © 2016年 王涛. All rights reserved.
//



#import "tagCollectionCell.h"

@implementation tagCollectionCell
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.tagButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.tagButton.frame = CGRectMake(0, 0,(ScreenSizeWidth - 20)/3 - 10, 35);
        self.tagButton.center = CGPointMake(self.width/2, self.height/2);
        self.tagButton.layer.cornerRadius = 4.0;
        self.tagButton.layer.borderWidth = 1.0;
        self.tagButton.layer.borderColor = [UIColor colorWithRed:220/255.0 green:220/255.0 blue:220/255.0 alpha:1].CGColor;
        self.tagButton.titleLabel.font = CUSTOMTITLESIZE14;
        [self.tagButton setTitleColor:CUSTOMTITLECOLOER3 forState:UIControlStateNormal];
        [self addSubview:self.tagButton];
        self.tagButton.userInteractionEnabled = NO;
//        self.tagButton.backgroundColor = [UIColor cyanColor];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
