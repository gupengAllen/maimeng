//
//  InfoDetailViewController.m
//  GameSM
//
//  Created by 王涛 on 15/7/9.
//  Copyright (c) 2015年 王涛. All rights reserved.
//

#import "InfoDetailViewControllerMax.h"
#import "CommentCell.h"
#import "UIImageView+AFNetworking.h"
#import "LoginViewController.h"
#import "PicDetailViewController.h"
#import "CustomNavigationController.h"
#import "MobClickSocialAnalytics.h"
#import "MobClick.h"

#import "FaceBagModel.h"
#import "FaceBagCell.h"

#import "TFHpple.h"

#import "HZPhotoBrowser.h"
#import "HZPhotoBrowserView.h"
#import "HZPhotoItemModel.h"

#import "CustomTool.h"
#import "upVote.h"

#define COMMENTTITLECOLOR [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1]
#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)
#define TMARGIN 5

@interface InfoDetailViewControllerMax ()<HZPhotoBrowserDelegate,UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate, UITextFieldDelegate, LoginAlertViewDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate, CustomAlertViewDelegate, UMSocialUIDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,UIAlertViewDelegate>
{
    NSMutableArray  *_faceDataArr;
    NSMutableArray  *_faceNameArr;
    UICollectionView *_collectionView;
    BOOL            _isFace;
    UIPageControl   *_pageC;
    UIView          *_FaceBagView;
    UIView          *_bgBlackView;
    UITextView      *_labelFu;
    
    UIView          *_testIv;
    UILabel         *_spotLabel;
    UILabel         *_newCountLabel;
    CGPoint         _beginPoint;
    float           _sum;
    UIScrollView    *_contentScrollView;
    BOOL            _OUT;
    BOOL            _isAppraise;
    BOOL            _isMissTabbar;
    UIView          *_shadowView;
    UIImageView     *_btnImage;
    UIImageView     *_netImageView;
    CGFloat         _keyboardHeight;
    NSString        *_contentStr;
    NSMutableArray  *_imgArr;
    int             _imgIndex;
    
    UILabel         *_nameLabel;
    
    UIView          *_deleteView;
    
    int             _deleteRow;
    int             _onlyOne;
    int             _moreCommentPage;
    NSString        *_deleteId;
    NSString        *_messageNumStr;
    
    NSString        *_documentDiretory;
    NSFileManager   *_fileManager;
    NSMutableArray  *_endOfTheTableView;
    NSInteger       _markLoginType;
    
    NSMutableArray  *_moreOutOfComments;
    int             _lowFirstNum;
    BOOL            _isReplOrComment;
    //资讯评论限制字数
    UIButton        *_showAllBtn;
    BOOL            _showAllContent;
    int             _cellNum;
    
    UIImageView     *_imageView;
    
}

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NSDictionary *dataDic;

@property (nonatomic, assign) int chooseIndex;
@property (nonatomic, assign) BOOL isReply;
@property (nonatomic, assign) BOOL isScroll;
@property (nonatomic, assign) BOOL isScrolling;

@property (nonatomic, assign) BOOL isLoadingOver;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation InfoDetailViewControllerMax

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createTableView];
    // Do any additional setup after loading the view from its nib.
    _endOfTheTableView = [NSMutableArray array];
    [self addBackBtn];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    //初始化
    _documentDiretory = [paths objectAtIndex:0];
    _moreOutOfComments = [NSMutableArray array];
    _isMissTabbar = NO;
    _showAllContent = NO;
    _lowFirstNum = 0;
    _cellNum = -1;
    _fileManager = [[NSFileManager alloc] init];
//    self.scrollView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight-64-44);
    
    //创建分享提示图
//    [self createShareTip];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"manhuago"] isEqualToString:@"1"]) {
        UIView *view = [CustomTool createNavView];
        [self.view addSubview:view];
        
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(60, 30, KScreenWidth-120, 30)];
        _nameLabel.center = CGPointMake(ScreenSizeWidth/2, 42);
        //    _nameLabel.size = CGSizeMake(200, 50);
        
        _nameLabel.textAlignment = 1;
        _nameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:20.0];
        _nameLabel.textColor = [UIColor colorWithRed:236/255.0 green:73/255.0 blue:94/255.0 alpha:1];
        //    _item.titleView = _nameLabel;
        [view addSubview:_nameLabel];
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        backButton.frame = CGRectMake(5, 25, 33, 33);
        [backButton setImage:[UIImage imageNamed:@"fanhui_"] forState:UIControlStateNormal];
        [backButton addTarget:self action:@selector(misBack) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:backButton];
        UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        shareBtn.frame = CGRectMake(KScreenWidth-38, 25, 33, 33);
        [shareBtn setImage:[UIImage imageNamed:@"fenxiang"] forState:UIControlStateNormal];
        [shareBtn addTarget:self action:@selector(rightBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:shareBtn];
        
//        self.scrollView.frame = CGRectMake(0, 64, KScreenWidth, KScreenheight-44-64);
    }
    _onlyOne = 1;
    _moreCommentPage = 1;
    self.isLoadingOver = NO;
    [self addRightItemWithImage:[UIImage imageNamed:@"fenxiang"] itemTarget:self action:@selector(rightBtnPressed)];
    self.dataArray = [[NSMutableArray alloc] init];
    self.inputView.layer.cornerRadius = 2;
    self.inputView.layer.masksToBounds = YES;
    self.inputView.backgroundColor = [UIColor colorWithRed:246/255.0 green:237/255.0 blue:237/255.0 alpha:1];
    
    
    //表情键盘
    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(10,5, 35, 35)];
    [leftBtn addTarget:self action:@selector(faceBag:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"biaoqing.png"] forState:UIControlStateNormal];
    [self.bottomView addSubview:leftBtn];
    _isFace = YES;
    [self initFaceData];
    [self createFaceBag];
    [self createPageControl];
    //加载web数据
    [self loadWebData];
    if (g_App.userInfo.userID) {
        [self addIntergration:@"5"];
    }
    //加载
    //    [self loadMessageData];
    //    [self messageDetail];
    _shadowView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenheight, KScreenWidth, 0)];
    _shadowView.backgroundColor = [UIColor whiteColor];
    _shadowView.alpha = 0;
    [self.view addSubview:_shadowView];
    
    
    
#warning 提示加载失败
    _netImageView = [CustomTool createImageView];
//    [_scrollView addSubview:_netImageView];
    
    
    _spotLabel =[[UILabel alloc]init];
    _spotLabel.text = @"···";
    _spotLabel.textColor = [UIColor colorWithRed:188/255.0 green:161/255.0 blue:164/255.0 alpha:1];
    
    
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 1)];
    lineView.backgroundColor = [UIColor colorWithRed:228/255.0 green:169/255.0 blue:180/255.0 alpha:1];
    [self.view addSubview:lineView];
    self.lineImageView.backgroundColor = [UIColor colorWithRed:203/255.0 green:203/255.0 blue:203/255.0 alpha:1];
    //---- 去掉下方的线||换颜色
    if ([self.navigationController.navigationBar respondsToSelector:@selector( setBackgroundImage:forBarMetrics:)]){
        NSArray *list=self.navigationController.navigationBar.subviews;
        for (id obj in list) {
            if ([obj isKindOfClass:[UIImageView class]]) {
                UIImageView *imageView=(UIImageView *)obj;
                NSArray *list2=imageView.subviews;
                for (id obj2 in list2) {
                    if ([obj2 isKindOfClass:[UIImageView class]]) {
                        UIImageView *imageView2=(UIImageView *)obj2;
                        //                        imageView2.backgroundColor = [UIColor colorWithRed:239/255.0 green:105/255.0 blue:125/255.0 alpha:1];
                        imageView2.hidden = YES;
                    }
                }
            }
        }
    }
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>7.0) {
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }else{
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.separatorInset = UIEdgeInsetsMake(0, 20, 0, 20);
    }
    
    self.sendBtn.backgroundColor = [UIColor colorWithRed:236/255.0 green:93/255.0 blue:94/255.0 alpha:1];
    
    _bgBlackView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenheight)];
    _bgBlackView.backgroundColor = [UIColor blackColor];
    _bgBlackView.alpha = 0;
    [self.view addSubview:_bgBlackView];
    [self createDeleteView];
    //作者属性
    self.authorBgView.backgroundColor = RGBACOLOR(250, 250, 250, 1.0);
    self.authorIntroduce.textColor = RGBACOLOR(160, 160, 160, 1.0);
    self.authorIntroduce.numberOfLines = 0;
    
    //    [self createLowTableView];
    
}
#pragma mark - 创建视图
-(void)createTableView{
    self.tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, ScreenSizeHeight - 64-44)];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
    
}
-(void)createShareTip{
    _imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, KScreenheight-180, KScreenWidth, 80)];
    _imageView.image = [UIImage imageNamed:@"shareTip"];
    [self.view addSubview:_imageView];
    _imageView.alpha = 0;
    [self.view sendSubviewToBack:_imageView];
}


-(void)createLowTableView{
    self.lowTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 2000, KScreenWidth, 200)];
    self.lowTableView.delegate = self;
    self.lowTableView.dataSource = self;
    self.lowTableView.tag = 1000;
    [self.view addSubview:self.lowTableView];
}
-(void)loadWebData{
    if (_onlyOne == 1) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_MESSAGE_DETAIL,@"r",
                                    self.infoID,@"id",
                                    @"0",@"withContent",
                                    @"1",@"withPraise",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadWebDataFinish:) method:GETDATA];
}
-(void)loadMessageData{
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"message/contentList",@"r",
                                    self.infoID,@"id",
                                    @"1",@"page",
                                    [NSString stringWithFormat:@"%d",_moreCommentPage*10],@"size",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadMessageDataFinish:) method:GETDATA];
}
-(void)loadWebDataFinish:(NSDictionary *)dic{
    _imgArr = [NSMutableArray array];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    BOOL refreshWitchVoid = YES;
    if (_onlyOne>1) {
        NSArray *arr= self.dataDic[@"praiseList"];
        if (arr.count) {
            refreshWitchVoid = YES;
        }else{
            refreshWitchVoid = NO;
        }
    }
    if (dic && ![[dic objectForKey:@"code"] integerValue]){
        self.dataDic = [dic objectForKey:@"results"];
        _contentStr = self.dataDic[@"content"];
        NSString *dataString = _contentStr;
        NSData *htmlData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
        TFHpple *xpathParser = [[TFHpple alloc] initWithHTMLData:htmlData];
        NSArray *elements = [xpathParser searchWithXPathQuery:@"//img"];
        for (int i =0; i < elements.count; i ++) {
            TFHppleElement * aElement = [elements objectAtIndex:i];
            NSDictionary *aDic = [aElement attributes];
            NSString *strr = aDic[@"src"];
            [_imgArr addObject:strr];
        }
        if (refreshWitchVoid) {
            [self refreshWebView];
        }else{
            [self refreshView];
        }
    }
}
-(void)loadMessageDataFinish:(NSDictionary *)dic{
    
    if (dic && ![[dic objectForKey:@"code"] integerValue]){
        _messageNumStr = [dic objectForKey:@"extraInfo"][@"countTotal"];
        //        [self.dataArray removeAllObjects];
        for (NSDictionary *item in [dic objectForKey:@"results"]) {
            [self.dataArray addObject:item];
        }
        if (_moreCommentPage*10 < [_messageNumStr intValue]) {
            
            if (_lowFirstNum > 0) {
                if (_moreCommentPage*10 >= _lowFirstNum) {
                    for (int i = 0; i < _moreCommentPage*10-_lowFirstNum +1; i++) {
                        [_moreOutOfComments removeObjectAtIndex:0];
                    }
                }
                
            }
        }else{
            [_moreOutOfComments removeAllObjects];
        }
        
        [self.tableView reloadData];
        [self.lowTableView reloadData];
        [self refreshMessageView];
    }
}
-(void)refreshWebView{
    [self initTitleName:[self.dataDic objectForKey:@"title"]];
    //top的高度
    self.titleName = [self.dataDic objectForKey:@"title"];
    self.titleLabel.text = [self.dataDic objectForKey:@"title"];
    _nameLabel.text = [self.dataDic objectForKey:@"title"];
    self.timeLabel.text = [self.dataDic objectForKey:@"createTimeValue"];
    //作者属性--
    self.authorNameLabel.text = [NSString stringWithFormat:@"%@",self.dataDic[@"userIDInfo"][@"name"]];
    CGSize sexSize = [self.authorNameLabel.text sizeWithFont:[UIFont systemFontOfSize:16.0] constrainedToSize:CGSizeMake(MAXFLOAT, self.authorNameLabel.bounds.size.height) lineBreakMode:0];
    UIImageView *sexImage = [[UIImageView alloc]initWithFrame:CGRectMake(70+sexSize.width+10, 32, 13, 15)];
    if ([[NSString stringWithFormat:@"%@",self.dataDic[@"userIDInfo"][@"sex"]]isEqualToString:@"1"]) {
        sexImage.image = [UIImage imageNamed:@"nan"];
    }else{
        sexImage.image = [UIImage imageNamed:@"nv"];
    }
    [self.authorView addSubview:sexImage];
    
    self.authorNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];
    if (self.dataDic[@"userIDInfo"][@"signature"]){
        self.authorIntroduce.text = [NSString stringWithFormat:@"%@",self.dataDic[@"userIDInfo"][@"signature"]];
        CGSize s = [self.authorIntroduce.text sizeWithFont:self.authorIntroduce.font constrainedToSize:CGSizeMake(MAXFLOAT, 15) lineBreakMode:NSLineBreakByCharWrapping];
        if (s.width > ScreenSizeWidth-70) {
            CGRect frame = self.authorIntroduce.frame;
            frame.size.height += 20;
            frame.origin.y -= 6;
            self.authorIntroduce.frame =frame;
        }
    }else{
        self.authorIntroduce.text = @"嗯哼~";
    }
    [self.whoImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",self.dataDic[@"userIDInfo"][@"images"]]]];
    
    CGSize titleSize = [self.timeLabel.text sizeWithFont:self.timeLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, self.timeLabel.bounds.size.height) lineBreakMode:NSLineBreakByCharWrapping];
    self.timeLabel.frame = CGRectMake(self.timeLabel.frame.origin.x, self.timeLabel.frame.origin.y, titleSize.width + 1, self.timeLabel.frame.size.height);
    self.authorLabel.frame = CGRectMake(self.timeLabel.frame.origin.x + self.timeLabel.frame.size.width +5, self.authorLabel.frame.origin.y, self.authorLabel.frame.size.width, self.authorLabel.frame.size.height);
    
    self.authorLabel.text = [NSString stringWithFormat:@"作者：%@", [self.dataDic objectForKey:@"author"]];
    CGSize size = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.titleLabel.bounds.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    if (size.height > 22) {
        self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y, self.titleLabel.frame.size.width, size.height);
    } else {
        self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y, self.titleLabel.frame.size.width, 22);
    }
    self.authView.frame = CGRectMake(self.authView.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 20, self.authView.frame.size.width, self.authView.frame.size.height);
    self.authTopView.frame = CGRectMake(self.authTopView.frame.origin.x, self.authTopView.frame.origin.y, self.authTopView.frame.size.width, self.authView.frame.origin.y + self.authView.frame.size.height + 2);
    
    //游戏
    if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
        if ([[self.dataDic objectForKey:@"gamesIDInfo"] isKindOfClass:[NSDictionary class]]) {
            [self.gameImageView setImageWithURL:[NSURL URLWithString:[[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"icon"]] placeholderImage:[UIImage imageNamed:@"jiazai_bg.png"]];
            self.gameImageView.hidden = NO;
            self.gameImageView.layer.cornerRadius = 5;
            self.gameImageView.layer.masksToBounds = YES;
            self.gameTitleLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"ch_name"];
            self.gameDevLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"developers"];
            self.gameSizeLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"size"];
            self.gameLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"label"];
        }
    }
    
    [self refreshAppraiseView];
    
    if (!self.isLoadingOver) {
        self.webView.frame = CGRectMake(0, self.authTopView.frame.size.height, self.authTopView.frame.size.width, self.webView.frame.size.height);
        [self.webView loadHTMLString:[self.dataDic objectForKey:@"contentValue"] baseURL:nil];
    } else {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        int height = 0;
        if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
            self.gameView.frame = CGRectMake(self.gameView.frame.origin.x, self.webView.frame.size.height + self.webView.frame.origin.y , self.gameView.frame.size.width, self.gameView.frame.size.height);
            height = self.gameView.bounds.size.height;
            self.gameView.hidden = NO;
        }
        
//        [self.scrollView setContentSize:CGSizeMake(0,height + self.authTopView.frame.size.height + self.webView.frame.size.height + self.authorView.frame.size.height + self.tableView.contentSize.height + self.appraiseView.frame.size.height + 6)];
        
        if ([[self.dataDic objectForKey:@"praiseCount"] integerValue]) {
            self.appraiseView.hidden = NO;
        } else {
            self.appraiseView.hidden = YES;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, height + self.webView.frame.size.height + self.authorView.frame.size.height + self.webView.frame.origin.y + 3, self.tableView.frame.size.width, self.tableView.contentSize.height);
            
//            [self.scrollView setContentSize:CGSizeMake(0, height + self.authTopView.frame.size.height + self.webView.frame.size.height + self.authorView.frame.size.height + self.tableView.contentSize.height)];
        }
//        if (self.scrollView.contentSize.height < self.view.bounds.size.height) {
//            [self.scrollView setContentSize:CGSizeMake(0, self.view.bounds.size.height + 1)];
//        }
    }
    self.webView.hidden = NO;
    [self.view bringSubviewToFront:self.touchBgView];
    [self.view bringSubviewToFront:self.bottomView];
    if (!self.indicatorView) {
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.webView addSubview:self.indicatorView];
        self.indicatorView.center = self.view.center;
    }
    if (_onlyOne == 1) {
        [self loadMessageData];
        _onlyOne ++;
    }
}

-(void)refreshAppraiseView{
    //赞
    NSArray *appraiseArray = [self.dataDic objectForKey:@"praiseList"];
    for (UIImageView *imageV in self.appraiseView.subviews) {
        if ([imageV isKindOfClass:[UIImageView class]] && imageV.tag >= 100) {
            [imageV removeFromSuperview];
        }
    }
    self.appraiseView.frame = CGRectMake(self.appraiseView.frame.origin.x, self.appraiseView.frame.origin.y, self.appraiseView.frame.size.width, 25);
    //    [self.appraiseBtn setTitle:[self.dataDic objectForKey:@"praiseCount"] forState:UIControlStateNormal];
    
    self.appraiseLabel.text = [NSString stringWithFormat:@"TA们也赞过（%@）",[self.dataDic objectForKey:@"praiseCount"]];
    if ([[self.dataDic objectForKey:@"userPraiseExists"] integerValue]) {
        [self.appraiseBtn setImage:[UIImage imageNamed:@"dianzan_3.png"] forState:UIControlStateNormal];
    } else{
        [self.appraiseBtn setImage:[UIImage imageNamed:@"dianzan_wei.png"] forState:UIControlStateNormal];
    }
    int i = 0;
    int num = KScreenWidth/50-1;
    int appNum = (appraiseArray.count > num)?num:(int)appraiseArray.count ;
    for ( ; i < appNum; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + 50 * (i % num), 25 + 50 * (i / num), 40, 40)];
        [imageView setImageWithURL:[NSURL URLWithString:[[[appraiseArray objectAtIndex:i] objectForKey:@"userIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
        //        self.appraiseView.clipsToBounds = YES;
        [self.appraiseView addSubview:imageView];
        imageView.layer.cornerRadius = imageView.bounds.size.height / 2;
        imageView.layer.masksToBounds = YES;
        imageView.tag = 100 + i;
    }
    if (i != 0) {
        self.appraiseView.frame = CGRectMake(self.appraiseView.frame.origin.x, self.appraiseView.frame.origin.y, self.appraiseView.frame.size.width, 25 + 50 );//* (i/num + 1)
        if (appraiseArray.count/num >= 1) {
            _spotLabel.frame = CGRectMake(10 + 50 * num, 20, 100, 50);
            _spotLabel.textAlignment = 0;
            _spotLabel.font = [UIFont systemFontOfSize:50];
            [self.appraiseView addSubview:_spotLabel];
        }
    }
}
-(void)refreshMessageView{
    [self refreshAppraiseView];
    int height = 0;
    if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
        self.gameView.frame = CGRectMake(self.gameView.frame.origin.x, self.webView.frame.size.height + self.webView.frame.origin.y , self.gameView.frame.size.width, self.gameView.frame.size.height);
        height = self.gameView.bounds.size.height;
        self.gameView.hidden = NO;
    }
    
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,self.appraiseView.frame.size.height + self.appraiseView.frame.origin.y + 3, self.tableView.frame.size.width, self.tableView.contentSize.height);
    
    //=========================================================
    self.lowTableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y + self.tableView.frame.size.height , self.tableView.frame.size.width, self.lowTableView.contentSize.height);
    
//    [self.scrollView setContentSize:CGSizeMake(0,height + self.authTopView.frame.size.height + self.webView.frame.size.height + self.authorView.size.height + self.tableView.contentSize.height + self.appraiseView.frame.size.height + 6  + self.lowTableView.contentSize.height)];
    
    if (self.dataArray.count) {
        self.tableView.hidden = NO;
    }
    if (_moreOutOfComments.count) {
        self.lowTableView.hidden = NO;
    }
    if ([[self.dataDic objectForKey:@"praiseCount"] integerValue]) {
        self.appraiseView.hidden = NO;
    } else {
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, height + self.webView.frame.size.height + self.webView.frame.origin.y + 3+ self.authorView.size.height, self.tableView.frame.size.width, self.tableView.contentSize.height);
        
        self.lowTableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y + self.tableView.frame.size.height , self.tableView.frame.size.width, self.lowTableView.contentSize.height);
        
//        [self.scrollView setContentSize:CGSizeMake(0, height + self.authTopView.frame.size.height + self.webView.frame.size.height + self.tableView.contentSize.height + self.authorView.size.height + self.lowTableView.contentSize.height)];
    }
//    if (self.scrollView.contentSize.height < self.view.bounds.size.height) {
//        [self.scrollView setContentSize:CGSizeMake(0, self.view.bounds.size.height + 1)];
//    }
//    if (_isReplOrComment) {
//        [self.scrollView setContentOffset:CGPointMake(0, self.scrollView.contentSize.height-KScreenheight+60+50) animated:YES];
//    }
    
    
}


-(void)createDeleteView{
    _deleteView = [[UIView alloc]initWithFrame:CGRectMake(0, KScreenheight, KScreenWidth, 105)];
    _deleteView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_deleteView];
    
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 0, KScreenWidth-20, 49)];
    deleteBtn.backgroundColor = [UIColor whiteColor];
    deleteBtn.layer.masksToBounds = YES;
    deleteBtn.layer.cornerRadius = 8;
    [deleteBtn setTitle:@"删除评论" forState:UIControlStateNormal];
    [deleteBtn setTitleColor:[UIColor colorWithRed:0/255.0 green:91/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(showAlert) forControlEvents:UIControlEventTouchUpInside];
    [_deleteView addSubview:deleteBtn];
    
    UIButton *cancleBtn = [[UIButton alloc]initWithFrame:CGRectMake(10, 55, KScreenWidth-20, 49)];
    cancleBtn.backgroundColor = [ UIColor whiteColor];
    cancleBtn.layer.masksToBounds = YES;
    cancleBtn.layer.cornerRadius = 8;
    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:[UIColor colorWithRed:0/255.0 green:91/255.0 blue:255/255.0 alpha:1.0] forState:UIControlStateNormal];
    [cancleBtn addTarget:self action:@selector(misDeleteView) forControlEvents:UIControlEventTouchUpInside];
    [_deleteView addSubview:cancleBtn];
}

-(void)showAlert:(UIButton *)sender{
    _deleteRow = sender.tag;
    [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"mc" to_section:@"m" to_step:@"r" type:@"delete" id:_deleteId];
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"您是否确定删除此回复" message:nil delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    //    alertView.alertViewStyle
    alertView.center = CGPointMake(KScreenWidth/2,KScreenheight-100);
    [alertView show];
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (_deleteRow >= 2000) {
        _deleteId = _moreOutOfComments[_deleteRow-2000][@"id"];
    }else{
        _deleteId = _dataArray[_deleteRow][@"id"];
    }
    //     _deleteId = _dataArray[_deleteRow][@"id"];
    if (buttonIndex == 1) {
        [self deleteComments];
    }
}

-(void)deleteComments{
    [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"r" to_page:@"mc" to_section:@"m" to_step:@"a" type:@"delete" id:_deleteId];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_DELETE,@"r",
                                    _deleteId,@"id",nil
                                    ];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(deleteCommentsFinish:) method:POSTDATA];
}
- (void)deleteCommentsFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        [MBProgressHUD showSuccess:@"删除成功" toView:self.view];
        
        if (_deleteRow >= 2000) {
            [_moreOutOfComments removeObjectAtIndex:_deleteRow-2000];
            
        }else{
            [_dataArray removeObjectAtIndex:_deleteRow];
        }
        
        
        if (_moreCommentPage*10 < [_messageNumStr intValue]) {
            for (int i = 0; i < _moreOutOfComments.count; i ++) {
                if ([_moreOutOfComments[i][@"id"] isEqualToString:_deleteId]) {
                    [_moreOutOfComments removeObjectAtIndex:i];
                }
            }
            //            long num = (_moreOutOfComments.count -  _moreOutOfComments.count%10)%10+1;
            //            [_moreOutOfComments removeObjectAtIndex:row];
        }else{
            [_moreOutOfComments removeAllObjects];
        }
        
        NSString *path = [_documentDiretory stringByAppendingPathComponent:@"cartoonMessageDetailList"];
        [_fileManager removeItemAtPath:path error:nil];
        //        [self messageDetail];
        [self loadMessageData];
        [self misDeleteView];
    }
}

-(void)misDeleteView{
    
    [UIView animateWithDuration:0.3 animations:^{
        _deleteView.frame = CGRectMake(0, KScreenheight, KScreenWidth, 105);
        _bgBlackView.alpha = 0;
        self.bottomView.frame = CGRectMake(0, KScreenheight-45-64, KScreenWidth, 45);
    }];
}
-(void)appearDeleteBtn:(UIButton *)sender{
    [self.view bringSubviewToFront:_bgBlackView];
    [self.view bringSubviewToFront:_deleteView];
    
    _deleteRow = sender.tag;
    if (g_App.userID) {
        if (_dataArray[_deleteRow][@"replyUserIDInfo"]) {
            if ([_dataArray[_deleteRow][@"replyUserIDInfo"][@"id"] isEqualToString:g_App.userID]) {
                [UIView animateWithDuration:0.3 animations:^{
                    //                    self.bottomView.alpha = 0.5;
                    _bgBlackView.alpha = 0.4;
                    _deleteView.frame = CGRectMake(0, KScreenheight-189, KScreenWidth, 105);
                }];
                _deleteId = _dataArray[_deleteRow][@"id"];
            }
        }else{
            if ([_dataArray[_deleteRow][@"userID"] isEqualToString:g_App.userID]) {
                [UIView animateWithDuration:0.3 animations:^{
                    _bgBlackView.alpha = 0.4;
                    _deleteView.frame = CGRectMake(0, KScreenheight-189, KScreenWidth, 105);
                }];
                _deleteId = _dataArray[_deleteRow][@"id"];
            }
        }
    }
}

-(void)misBack{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"manhuago"] isEqualToString:@"0"]) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    //    _collectionView.pullDelegate = nil;
    
}

-(void)initFaceData{
    _faceDataArr = [NSMutableArray array];
    _faceNameArr = [NSMutableArray array];
    
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
    NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
    for (int i =1; i < 72; i ++) {
        [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        FaceBagModel *model = [[FaceBagModel alloc]init];
        NSString *imageName = [NSString stringWithFormat:@"%d#.png",i];
        model.imageName = imageName;
        [_faceDataArr addObject:model];
    }
}
-(void)createFaceBag{
    _FaceBagView = [[UIView alloc]init];
    _FaceBagView.frame = CGRectMake(0,0, KScreenWidth, 216);
    _FaceBagView.backgroundColor = [UIColor colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0];
    UIButton *deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-60, 188, 60, 25)];
    [deleteBtn setImage:[UIImage imageNamed:@"shanchu_2.png"] forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(deletaClike) forControlEvents:UIControlEventTouchUpInside];
    [_FaceBagView addSubview:deleteBtn];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    _collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, KScreenWidth, 186) collectionViewLayout:flowLayout];
    _collectionView.delegate = self;
    _collectionView.dataSource = self;
    _collectionView.backgroundColor = [UIColor whiteColor];//colorWithRed:255/255.0 green:247/255.0 blue:245/255.0 alpha:1.0
    _collectionView.pagingEnabled = YES;
    _collectionView.showsHorizontalScrollIndicator = NO;
    _collectionView.showsVerticalScrollIndicator = NO;
    [_FaceBagView addSubview:_collectionView];
    NSString *identifier = @"faceCell";
    [_collectionView registerClass:[FaceBagCell class] forCellWithReuseIdentifier:identifier];
    [_collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"ReusableView"];
    [_inputTextField resignFirstResponder];
}
-(void)createPageControl{
    //分页控件：宽高是系统有默认值的
    _pageC = [[UIPageControl alloc]initWithFrame:CGRectMake(0, 195, 40, 10)];
    _pageC.center = CGPointMake(KScreenWidth/2.0,_FaceBagView.bounds.size.height -15);
    UIView *iv = [[UIView alloc]initWithFrame:CGRectMake(KScreenWidth/2-20, 197.5, 40, 7)];
    iv.backgroundColor = [UIColor clearColor];
    [_FaceBagView addSubview:iv];
    UIView *iv1 = [[UIView alloc]initWithFrame:CGRectMake(0.5, 0, 7, 7)];
    iv1.backgroundColor = [UIColor lightGrayColor];
    iv1.layer.masksToBounds = YES;
    iv1.layer.cornerRadius = 3.5;
    [iv addSubview:iv1];
    UIView *iv2 = [[UIView alloc]initWithFrame:CGRectMake(16.5, 0, 7, 7)];
    iv2.backgroundColor = [UIColor lightGrayColor];
    iv2.layer.masksToBounds = YES;
    iv2.layer.cornerRadius = 3.5;
    [iv addSubview:iv2];
    UIView *iv3 = [[UIView alloc]initWithFrame:CGRectMake(33, 0, 7, 7)];
    iv3.backgroundColor = [UIColor lightGrayColor];
    iv3.layer.masksToBounds = YES;
    iv3.layer.cornerRadius = 3.5;
    [iv addSubview:iv3];
    
    _pageC.numberOfPages =3;
    _pageC.backgroundColor = [UIColor clearColor];
    _pageC.layer.masksToBounds = YES;
    _pageC.layer.cornerRadius = 5;
    _pageC.enabled =NO;
    //设置当前选中点得颜色
    _pageC.currentPageIndicatorTintColor = [UIColor redColor];
    _pageC.currentPage =0;
    [_FaceBagView addSubview:_pageC];
}
-(void)deletaClike{
    BOOL isDeleteAll;
    isDeleteAll = NO;
    if (_inputTextField.text.length) {
        NSString *str = [_inputTextField.text substringFromIndex:_inputTextField.text.length-1];
        if ([str isEqualToString:@"]"]) {
            for (int i = _inputTextField.text.length-1; i>0; i --) {
                NSString *str1 = [_inputTextField.text substringWithRange:NSMakeRange(i-1, 1)];
                if ([str1 isEqualToString:@"["]) {
                    _inputTextField.text = [_inputTextField.text substringToIndex:i-1];
                    isDeleteAll = NO;
                    break;
                }
                isDeleteAll = YES;
            }
            if (isDeleteAll) {
                _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
            }
            
        }else{
            _inputTextField.text = [_inputTextField.text substringToIndex:_inputTextField.text.length - 1];
        }
    }
}
#pragma mark - photorowser
////临时占位图（thumbnail图
- (UIImage *)photoBrowser:(HZPhotoBrowser *)browser placeholderImageForIndex:(NSInteger)index
{
    return nil;
}
////高清原图 （bmiddle图）
- (NSURL *)photoBrowser:(HZPhotoBrowser *)browser highQualityImageURLForIndex:(NSInteger)index
{
    return [NSURL URLWithString:[_imgArr objectAtIndex:index]];
}
//
- (NSString*)photoDrowserWithIndex:(NSInteger)index {
    return [[_imgArr objectAtIndex:index] objectForKey:@"id"];
}
#pragma mark - UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _faceDataArr.count;
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identify = @"faceCell";
    FaceBagCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    cell.model = _faceDataArr[indexPath.row];
    return cell;
}
#pragma mark - UICollectionViewDelegateFlowLayout
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((KScreenWidth-75)/7.0, 180/5.0);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(0, 10, 5, 5);
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
#warning - 表情点击
    NSInteger row = [indexPath row];
    _inputTextField.text = [NSString stringWithFormat:@"%@[%@]",_inputTextField.text,_faceNameArr[row]];
}
-(void)faceBag:(UIButton *)btn{
    if (_isFace) {
        [btn setImage:[UIImage imageNamed:@"jianpan.png"] forState:UIControlStateNormal];
        _inputTextField.inputView = _FaceBagView;
        [_inputTextField becomeFirstResponder];
        [_inputTextField reloadInputViews];
        _isFace = NO;
    }else{
        [btn setImage:[UIImage imageNamed:@"biaoqing.png"] forState:UIControlStateNormal];
        [_inputTextField becomeFirstResponder];
        _inputTextField.inputView = nil;
        [_inputTextField reloadInputViews];
        _isFace = YES;
    }
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)rightBtnPressed {
    [[LogHelper shared] writeToFilefrom_page:@"ms" from_section:@"m" from_step:@"d" to_page:@"ms" to_section:@"m" to_step:@"r" type:@"" id:self.infoID];
    
    
    
    [self.inputTextField resignFirstResponder];
    if (self.dataDic == nil) {
        return;
    }
    [UMSocialData defaultData].extConfig.qzoneData.title = [self.dataDic objectForKey:@"title"];;
    [UMSocialData defaultData].extConfig.qzoneData.url = [self.dataDic objectForKey:@"shareUrl"];
    
    [UMSocialData defaultData].extConfig.qqData.title = [self.dataDic objectForKey:@"title"];;
    [UMSocialData defaultData].extConfig.qqData.url = [self.dataDic objectForKey:@"shareUrl"];
    
    
    [UMSocialData defaultData].extConfig.wechatTimelineData.title = [self.dataDic objectForKey:@"title"];;
    [UMSocialData defaultData].extConfig.wechatTimelineData.url = [self.dataDic objectForKey:@"shareUrl"];
    
    
    [UMSocialData defaultData].extConfig.wechatSessionData.title = [self.dataDic objectForKey:@"title"];;
    [UMSocialData defaultData].extConfig.wechatSessionData.url = [self.dataDic objectForKey:@"shareUrl"];
    
    [UMSocialData defaultData].extConfig.sinaData.shareText = [NSString stringWithFormat:@"%@%@分享自@麦萌",[self.dataDic objectForKey:@"title"],[self.dataDic objectForKey:@"shareUrl"]];
    
    [[UMSocialData defaultData].urlResource setResourceType:UMSocialUrlResourceTypeImage url:[self.dataDic objectForKey:@"images"]];
    
    [UMSocialSnsService presentSnsIconSheetView:self
                                         appKey:UMENGKEY
                                      shareText:[self.dataDic objectForKey:@"shareContent"]
                                     shareImage:[UIImage imageNamed:@"icon.png"]
                                shareToSnsNames:@[UMShareToSina, UMShareToQQ, UMShareToQzone, UMShareToWechatSession,UMShareToWechatTimeline]
                                       delegate:self];
    
    NSDictionary *dict = @{@"shareId" : [self.dataDic objectForKey:@"id"]};
    [MobClick event:@"shareEvent" attributes:dict];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"manhuago"] isEqualToString:@"1"]) {
        self.navigationController.navigationBar.hidden = YES;
    }
    _isMissTabbar = NO;
    _isReplOrComment = NO;
    [(LYTabBarController*)self.tabBarController hiddenBar:YES animated:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self initView:self.data];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"manhuago"] isEqualToString:@"1"]) {
        self.navigationController.navigationBar.hidden = NO;
    }
    if (!_isMissTabbar ) {
        [(LYTabBarController*)self.tabBarController hiddenBar:NO animated:YES];
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    //kbSize即為鍵盤尺寸 (有width, height)
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;//得到鍵盤的高度
    _keyboardHeight = kbSize.height;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, keyboardBounds.size.height, 0);
//        [[self scrollView] setScrollIndicatorInsets:e];
//        [[self scrollView] setContentInset:e];
        
        [UIView animateWithDuration:0.3 animations:^{
            self.bottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.bottomView.frame.size.height - keyboardBounds.size.height, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        }];
        [UIView animateWithDuration:0.01 animations:^{
            _shadowView.frame = CGRectMake(0, KScreenheight - _keyboardHeight - 64, KScreenWidth, 100);
            _shadowView.alpha =1;
        }];
        //        [self performSelector:@selector(lag1) withObject:nil afterDelay:0.3];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}
-(void)lag1{
    //    if (_keyboardHeight == 184) {
    //        _shadowView.frame = CGRectMake(0, KScreenheight-184 - 64 - 42, KScreenWidth, 42);
    //        _shadowView.alpha = 1;
    //    }else{
    _shadowView.frame = CGRectMake(0, KScreenheight - _keyboardHeight - 40, KScreenWidth, 100);
    _shadowView.alpha =1;
    //    }
}

- (void)keyboardWillHide:(NSNotification *)notification
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
#endif
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_3_2
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey];
#else
        NSValue *keyboardBoundsValue = [[notification userInfo] objectForKey:UIKeyboardBoundsUserInfoKey];
#endif
        CGRect keyboardBounds;
        [keyboardBoundsValue getValue:&keyboardBounds];
        UIEdgeInsets e = UIEdgeInsetsMake(0, 0, 0, 0);
//        [[self scrollView] setScrollIndicatorInsets:e];
//        [[self scrollView] setContentInset:e];
        
        [UIView animateWithDuration:0.29 animations:^{
            self.bottomView.frame = CGRectMake(0, self.view.bounds.size.height - self.bottomView.frame.size.height, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
        }];
        [UIView animateWithDuration:0.01 animations:^{
            _shadowView.alpha = 0;
        }];
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_3_2
    }
#endif
}


- (void)initView:(NSDictionary*)dic {
    
    if (self.titleName) {
        [self initTitleName:self.titleName];
    }
    _webView.scrollView.scrollEnabled = NO;
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.backgroundView = nil;
    self.tableView.rowHeight = 80;
    
    self.gameContentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.gameContentView.layer.cornerRadius = 3;
    self.gameContentView.layer.borderWidth = 0.5;
    self.gameImageView.hidden = YES;
}

- (void)refreshView {
    [self initTitleName:[self.dataDic objectForKey:@"title"]];
    self.titleName = [self.dataDic objectForKey:@"title"];
    self.titleLabel.text = [self.dataDic objectForKey:@"title"];
    _nameLabel.text = [self.dataDic objectForKey:@"title"];
    self.timeLabel.text = [self.dataDic objectForKey:@"createTimeValue"];
    
    CGSize titleSize = [self.timeLabel.text sizeWithFont:self.timeLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, self.timeLabel.bounds.size.height) lineBreakMode:NSLineBreakByCharWrapping];
    self.timeLabel.frame = CGRectMake(self.timeLabel.frame.origin.x, self.timeLabel.frame.origin.y, titleSize.width + 1, self.timeLabel.frame.size.height);
    self.authorLabel.frame = CGRectMake(self.timeLabel.frame.origin.x + self.timeLabel.frame.size.width +5, self.authorLabel.frame.origin.y, self.authorLabel.frame.size.width, self.authorLabel.frame.size.height);
    
    self.authorLabel.text = [NSString stringWithFormat:@"作者：%@", [self.dataDic objectForKey:@"author"]];
    CGSize size = [self.titleLabel.text sizeWithFont:self.titleLabel.font constrainedToSize:CGSizeMake(self.titleLabel.bounds.size.width, MAXFLOAT) lineBreakMode:NSLineBreakByCharWrapping];
    if (size.height > 22) {
        self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y, self.titleLabel.frame.size.width, size.height);
    } else {
        self.titleLabel.frame = CGRectMake(self.titleLabel.frame.origin.x, self.titleLabel.frame.origin.y, self.titleLabel.frame.size.width, 22);
    }
    
    self.authView.frame = CGRectMake(self.authView.frame.origin.x, self.titleLabel.frame.origin.y + self.titleLabel.frame.size.height + 20, self.authView.frame.size.width, self.authView.frame.size.height);
    self.authTopView.frame = CGRectMake(self.authTopView.frame.origin.x, self.authTopView.frame.origin.y, self.authTopView.frame.size.width, self.authView.frame.origin.y + self.authView.frame.size.height + 2);
    
    //    self.webView.frame = CGRectMake(0, self.authTopView.frame.size.height, self.authTopView.frame.size.width, self.webView.frame.size.height);
    
    
    
    self.authorView.frame = CGRectMake(self.authorView.frame.origin.x, self.webView.bottom, self.authorView.frame.size.width, self.authorView.size.height);
    
    
    
#warning 赞
    NSArray *appraiseArray = [self.dataDic objectForKey:@"praiseList"];
    for (UIImageView *imageV in self.appraiseView.subviews) {
        if ([imageV isKindOfClass:[UIImageView class]] && imageV.tag >= 100) {
            [imageV removeFromSuperview];
        }
    }
    self.appraiseView.frame = CGRectMake(self.appraiseView.frame.origin.x, self.appraiseView.frame.origin.y, self.appraiseView.frame.size.width, 25);
    //    [self.appraiseBtn setTitle:[self.dataDic objectForKey:@"praiseCount"] forState:UIControlStateNormal];
    self.appraiseLabel.text = [NSString stringWithFormat:@"TA们也赞过（%@）",[self.dataDic objectForKey:@"praiseCount"]];
    if ([[self.dataDic objectForKey:@"userPraiseExists"] integerValue]) {
        [self.appraiseBtn setImage:[UIImage imageNamed:@"dianzan_3.png"] forState:UIControlStateNormal];
    } else
        [self.appraiseBtn setImage:[UIImage imageNamed:@"dianzan_wei.png"] forState:UIControlStateNormal];
    
    if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
        if ([[self.dataDic objectForKey:@"gamesIDInfo"] isKindOfClass:[NSDictionary class]]) {
            [self.gameImageView setImageWithURL:[NSURL URLWithString:[[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"icon"]] placeholderImage:[UIImage imageNamed:@"jiazai_bg.png"]];
            self.gameImageView.hidden = NO;
            self.gameImageView.layer.cornerRadius = 5;
            self.gameImageView.layer.masksToBounds = YES;
            self.gameTitleLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"ch_name"];
            self.gameDevLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"developers"];
            self.gameSizeLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"size"];
            self.gameLabel.text = [[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"label"];
        }
    }
    int i = 0;
    int num = KScreenWidth/50-1;
    int appNum = (appraiseArray.count > num)?num:(int)appraiseArray.count ;
    for ( ; i < appNum; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10 + 50 * (i % num), 25 + 50 * (i / num), 40, 40)];
        [imageView setImageWithURL:[NSURL URLWithString:[[[appraiseArray objectAtIndex:i] objectForKey:@"userIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
        //        self.appraiseView.clipsToBounds = YES;
        [self.appraiseView addSubview:imageView];
        imageView.layer.cornerRadius = imageView.bounds.size.height / 2;
        imageView.layer.masksToBounds = YES;
        imageView.tag = 100 + i;
    }
    if (i != 0) {
        self.appraiseView.frame = CGRectMake(self.appraiseView.frame.origin.x, self.appraiseView.frame.origin.y, self.appraiseView.frame.size.width, 25 + 50 );//* (i/num + 1)
        if (appraiseArray.count/num >= 1) {
            _spotLabel.frame = CGRectMake(10 + 50 * num, 20, 100, 50);
            _spotLabel.textAlignment = 0;
            _spotLabel.font = [UIFont systemFontOfSize:50];
            [self.appraiseView addSubview:_spotLabel];
        }
    }
    
    if (!self.isLoadingOver) {
        self.webView.frame = CGRectMake(0, self.authTopView.frame.size.height, self.authTopView.frame.size.width, self.view.frame.size.height);
        [self.webView loadHTMLString:[self.dataDic objectForKey:@"contentValue"] baseURL:nil];
    } else {
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        int height = 0;
        if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
            self.gameView.frame = CGRectMake(self.gameView.frame.origin.x, self.webView.frame.size.height + self.webView.frame.origin.y , self.gameView.frame.size.width, self.gameView.frame.size.height);
            height = self.gameView.bounds.size.height;
            self.gameView.hidden = NO;
        }
        
        self.appraiseView.frame = CGRectMake(self.appraiseView.frame.origin.x, height + self.webView.frame.size.height + self.webView.frame.origin.y + 3 +  self.authorView.size.height, self.appraiseView.frame.size.width, self.appraiseView.frame.size.height);
        
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,self.appraiseView.frame.size.height + self.appraiseView.frame.origin.y + 3, self.tableView.frame.size.width, self.tableView.contentSize.height);
        
//        [self.scrollView setContentSize:CGSizeMake(0,height + self.authTopView.frame.size.height + self.webView.frame.size.height + self.authorView.size.height + self.tableView.contentSize.height + self.appraiseView.frame.size.height + 6)];
        
        
        if (self.dataArray.count) {
            self.tableView.hidden = NO;
        }
        if ([[self.dataDic objectForKey:@"praiseCount"] integerValue]) {
            self.appraiseView.hidden = NO;
        } else {
            self.appraiseView.hidden = YES;
            self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, height + self.webView.frame.size.height + self.webView.frame.origin.y + 3 + self.authorView.size.height, self.tableView.frame.size.width, self.tableView.contentSize.height);
            
//            [self.scrollView setContentSize:CGSizeMake(0, height + self.authTopView.frame.size.height + self.webView.frame.size.height + self.tableView.contentSize.height + self.authorView.size.height)];
        }
//        if (self.scrollView.contentSize.height < self.view.bounds.size.height) {
//            [self.scrollView setContentSize:CGSizeMake(0, self.view.bounds.size.height + 1)];
//        }
    }
    
    self.webView.hidden = NO;
    
    [self.view bringSubviewToFront:self.touchBgView];
    [self.view bringSubviewToFront:self.bottomView];
    
    if (!self.indicatorView) {
        self.indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [self.webView addSubview:self.indicatorView];
        self.indicatorView.center = self.view.center;
    }
}

-(void)handleSingleTap:(UITapGestureRecognizer *)sender
{
    CGPoint pt = [sender locationInView:self.webView];
    //    NSString *imgURL = [NSString stringWithFormat:@"document.getElementsByTagName('a').attr('href')"];
    
    NSString *imgURL = [NSString stringWithFormat:@"document.elementFromPoint(%f, %f).src", pt.x, pt.y];
    NSString *urlToSave = [self.webView stringByEvaluatingJavaScriptFromString:imgURL];
    if (urlToSave.length > 0) {
        [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"id" to_section:@"i" to_step:@"d" type:@"" id:@"0"];
        if (!self.isScroll) {
            for (int i = 0; i < _imgArr.count; i ++) {
                if ([urlToSave rangeOfString:_imgArr[i]].location != NSNotFound) {
                    _imgIndex = i;
                    NSRange range = [urlToSave rangeOfString:@"?"];
                    if (range.length) {
                        NSString *str = [urlToSave substringToIndex:range.location];
                        [self performSelector:@selector(goToDetailView:) withObject:str afterDelay:0.2];
                    } else {
                        
                        [self performSelector:@selector(goToDetailView:) withObject:urlToSave afterDelay:0.2];
                    }
                }
            }
        }
    }
}

- (void)goToDetailView:(NSString*)objc {
    if (!self.isScroll) {
        HZPhotoBrowser *browserVc = [[HZPhotoBrowser alloc] init];
        //    browserVc.sourceImagesContainerView = self.collectionView; // 原图的父控件
        browserVc.imageCount = _imgArr.count; // 图片总数
        browserVc.currentImageIndex = _imgIndex;
        browserVc.type = 2;
        browserVc.delegate = self;
        _isMissTabbar = YES;
        browserVc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self presentViewController:browserVc animated:YES completion:^{
            
        }];
        
        //        PicDetailViewController *detailView = [[PicDetailViewController alloc] initWithNibName:@"PicDetailViewController" bundle:nil];
        //        detailView.imageData = [NSArray arrayWithObject:objc];
        //        NSLog(@"------%@",detailView.imageData);
        //        detailView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        //        [self presentViewController:detailView animated:YES completion:nil];
    }
}

- (IBAction)appraiseBtnPressed:(id)sender {
    
    UIButton *appraisBtn = (UIButton *)sender;
    _isAppraise = YES;
    //    [self misDeleteView];
    if (!g_App.userID) {
        _markLoginType = 2;
        [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"nl" to_section:@"m" to_step:@"r" type:@"" id:self.infoID];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以点赞哦~"];
        [alertView show];
        return;
    }
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"CURRENTWLAN"] isEqualToString:@"2"]){
        [MBProgressHUD showError:@"点赞失败"  toView:self.view];
    }else{
        
        CGPoint aimPoint = [_bottomView convertPoint:CGPointMake(appraisBtn.origin.x, appraisBtn.origin.y)  toView:self.view];
        //        NSLog(@"point%@",NSStringFromCGPoint(aimPoint));
        
        
        
        if ([[self.dataDic objectForKey:@"userPraiseExists"] integerValue]){
            [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"mp" to_section:@"m" to_step:@"a" type:@"cancel" id:self.infoID];
            [MBProgressHUD showSuccess:@"已取消" toView:self.view];
            [self.appraiseBtn setImage:[UIImage imageNamed:@"dianzan_wei.png"] forState:UIControlStateNormal];
            [self appraiseMsg];
        }else{
            [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"mp" to_section:@"m" to_step:@"a" type:@"insert" id:self.infoID];
            
            [MBProgressHUD showSuccess:@"点赞成功" toView:self.view];
            [self.appraiseBtn setImage:[UIImage imageNamed:@"dianzan_3.png"] forState:UIControlStateNormal];
            [self appraiseMsg];
        }
    }
}

- (IBAction)gameDownLoadBtnPressed:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[self.dataDic objectForKey:@"gamesIDInfo"] objectForKey:@"download_ios"]]];
}

- (IBAction)sendBtnPressed:(id)sender {
    self.sendBtn.hidden = YES;
    self.appraiseBtn.hidden = NO;
    self.touchBgView.hidden = YES;
    
    if (!self.inputTextField.text.length) {
        self.inputTextField.placeholder = @"我来说两句";
        self.isReply = NO;
        //        self.touchBgView.hidden = YES;
        [self.inputTextField resignFirstResponder];
        return;
    }
    if (self.isReply) {
        [self replyMessage:self.inputTextField.text];
    } else
        [self commentMessage:self.inputTextField.text];
    
    [self.inputTextField resignFirstResponder];
    self.inputTextField.text = @"";
}

#pragma mark - UMSocialUIDelegate
-(void)didFinishGetUMSocialDataInViewController:(UMSocialResponseEntity *)response
{
    [[LogHelper shared] writeToFilefrom_page:@"ms" from_section:@"m" from_step:@"r" to_page:@"ms" to_section:@"m" to_step:@"a" type:[[response.data allKeys] objectAtIndex:0] id:@"0"];
    //根据`responseCode`得到发送结果,如果分享成功
    if(response.responseCode == UMSResponseCodeSuccess)
    {
        //得到分享到的微博平台名
        NSLog(@"share to sns name is %@",[[response.data allKeys] objectAtIndex:0]);
        
        //        [[upVote shared] startAnimitionTop:@"分享成功" bottom:@"+9999" point:CGPointMake(KScreenWidth/2, KScreenheight/2) fatherView:self.view];
        
        
        
        [self addIntergration:@"4"];
    }
}

#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 1000) {
        return _moreOutOfComments.count;
    }else{
        return self.dataArray.count;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView.tag == 1000) {
        return 0.01;
    }else{
        if (self.dataArray.count == 0) {
            return 0.01;
        }else{
            return 25;
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (tableView.tag == 1000) {
        return 0.01;
    }else{
        if (self.dataArray.count < 10) {
            return 0.01;
        }else{
            if (_moreCommentPage*10 >= [_messageNumStr intValue]) {
                return 0.01;
            }else{
                if (_moreCommentPage*10 + 1 == _lowFirstNum) {
                    return 0.01;
                }
                return 50;
            }
        }
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView.tag == 1000) {
        return nil;
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 5, self.tableView.bounds.size.width, 20)];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (view.bounds.size.height - 12)/2, 14, 12)];
        imageView.image = [UIImage imageNamed:@"biaoqianOld.png"];
        [view addSubview:imageView];
        view.backgroundColor = [UIColor whiteColor];
        _newCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, (view.bounds.size.height - 14)/2, self.tableView.bounds.size.width - 40, 14)];
        _newCountLabel.text = [NSString stringWithFormat:@"最新评论（%@）",_messageNumStr];//[self.dataDic objectForKey:@"commentCount"]
        [view addSubview:_newCountLabel];
        _newCountLabel.font = [UIFont systemFontOfSize:12];
        UIImageView *lineImageView = [[UIImageView alloc]init];
        if ([[[UIDevice currentDevice]systemVersion]doubleValue]>7.0) {
            lineImageView.frame = CGRectMake(10, 24, KScreenWidth-20, 2);
        }else{
            lineImageView.frame = CGRectMake(0, 24, KScreenWidth, 2);
        }
        
        lineImageView.backgroundColor = [UIColor whiteColor];
        [view addSubview:lineImageView];
        return view;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (tableView.tag == 1000) {
        return nil;
    }else{
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 50)];
        UIView *line = [[UIView alloc]initWithFrame:CGRectMake(20, 0, self.tableView.bounds.size.width-40, 1)];
        line.backgroundColor = RGBACOLOR(230, 230, 230, 1.0);
        //    [view addSubview:line];
        UIButton *moreBtn = [[UIButton alloc]initWithFrame:CGRectMake(5, 0, self.tableView.bounds.size.width-10, 45)];
        moreBtn.layer.masksToBounds = YES;
        moreBtn.layer.cornerRadius = 10;
        moreBtn.layer.borderWidth = 1;
        moreBtn.layer.borderColor = RGBACOLOR(230, 230, 230, 1.0).CGColor;    moreBtn.backgroundColor = [UIColor whiteColor];//RGBACOLOR(242,242, 242, 1.0);
        [moreBtn setTitle:@"更多评论" forState:UIControlStateNormal];
        [moreBtn setTitleColor:RGBACOLOR(200, 200, 200, 1.0) forState:UIControlStateNormal];
        //    [moreBtn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
        moreBtn.titleLabel.textAlignment = 1;
        [moreBtn addTarget:self action:@selector(moreCommentData) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:moreBtn];
        if (_moreCommentPage*10 >= [_messageNumStr intValue]) {
            return nil;
            //            moreBtn.hidden = YES;
        }else{
            moreBtn.hidden = NO;
        }
        if (_moreCommentPage*10 + 1 == _lowFirstNum) {
            return nil;
            //             moreBtn.hidden = YES;
        }
        return view;
    }
    
}

-(void)moreCommentData{
    _moreCommentPage ++;
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"message/contentList",@"r",
                                    self.infoID,@"id",
                                    [NSString stringWithFormat:@"%d",_moreCommentPage],@"page",
                                    @"10",@"size",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadMessageDataFinish:) method:GETDATA];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 1000) {
        
        CommentCell *cell = [[CommentCell alloc]init];
        cell.backgroundColor = [UIColor blackColor];
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentCell" owner:self options:nil] lastObject];
        //    if (!cell) {
        cell = [CommentCell commentCellOwner:self];
        cell.userImageView.layer.cornerRadius = 18;
        cell.userImageView.layer.masksToBounds = YES;
        //    }
        
        if([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]){
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        
        if (![_moreOutOfComments count]) {
            return cell;
        }
        
        //        cell.floorNum.text = [NSString stringWithFormat:@"%ld楼",(long)[indexPath row]+1];
        int row = (int)[indexPath row];
        int num =  [_messageNumStr intValue];
        cell.floorNum.text = [NSString stringWithFormat:@"%ld楼",num-(_moreOutOfComments.count - row)+1];
        if ([indexPath row] == 0) {
            _lowFirstNum = [cell.floorNum.text intValue];
        }
        cell.floorNum.textColor = RGBACOLOR(154, 205, 208, 1.0);
        
        int valueType = (int)[[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"valueType"] integerValue];
        if (valueType == 1) {
            cell.userNameLabel.text = [[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"];
            cell.userNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];//colorWithRed:192/255.0 green:126/255.0 blue:126/255.0 alpha:1.0
            [cell.userImageView setImageWithURL:[NSURL URLWithString:[[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
            cell.userTimeLabel.text = [[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"];
        } else {
            cell.userNameLabel.text = [[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"name"];
            cell.userNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];
            [cell.userImageView setImageWithURL:[NSURL URLWithString:[[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"回复：%@ %@",[[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"],[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"]]];
            
            if ([[[[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"] length] > 0) {
                [str addAttribute:NSForegroundColorAttributeName value:COMMENTTITLECOLOR range:NSMakeRange(3, [[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"] length])];
            }
            [cell.userTimeLabel setAttributedText:str];
        }
#warning -
        // UITextView-富文本
        NSString *cententString = [[_moreOutOfComments objectAtIndex:indexPath.row] objectForKey:@"content"];
        
        //去掉换行符
        //        cententString = [cententString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        //        cententString = [cententString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        _labelFu = [[UITextView alloc]initWithFrame:CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-50-30, 20)];
        _labelFu.font = [UIFont boldSystemFontOfSize:16];
        _labelFu.userInteractionEnabled = NO;
        _labelFu.bounces = NO;
        _labelFu.editable = NO;
        [cell.contentView addSubview:_labelFu];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 8;// 字体的行间距
        NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:16],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        //表情
        _faceNameArr = [NSMutableArray array];
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
        NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
        for (int i =1; i < 72; i ++) {
            [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        }
#warning 正则表达式 "\\[[^\\]]+\\]"--"\\#[^\\#]+\\#"
        NSString *regexString = @"\\[[^\\]]+\\]";
        NSMutableAttributedString *attri = [_labelFu.attributedText mutableCopy];//_model.content
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
        NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
        if (arrr.count>0) {
            for (int i =0 ; i < arrr.count; i ++) {
                NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
                NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
                NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
                [attri appendAttributedString:attrStr1];
                NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
                for (int j = 0; j < _faceNameArr.count; j ++) {
                    if ([faceStr isEqualToString:_faceNameArr[j]]) {
                        NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                        attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                        attch.bounds = CGRectMake(0, -2, 30, 30);
                        NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                        [attri appendAttributedString:string1];
                        break;
                    }
                }
                cententString = [cententString substringFromIndex:result.range.location+result.range.length];
            }
            NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
            [attri appendAttributedString:attrStr2];
            [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
            _labelFu.attributedText = [attri copy];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attri boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
            
            _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-50-30, rect.size.height+15);
        }else{
            NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
            NSRange allRange = [cententString rangeOfString:cententString];
            [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
            [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
            
            _labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
            int height = (int)rect.size.height/19;
            _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-40-30, height*27+7);
        }
        cell.height = TMARGIN +32+_labelFu.frame.size.height+6;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        cell.replyBtn.frame = CGRectMake(cell.frame.size.width - 60, 15, 60, 20);//cell.frame.size.height/2-20
        [cell.replyBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 35)];
        cell.replyNameLabel.frame = CGRectMake(cell.frame.size.width - 30, 13.5, 30, 20);
        cell.replyNameLabel.textColor = RGBACOLOR(203, 203, 203, 1.0);
        [cell.contentView addSubview:cell.replyNameLabel];
        if ([cell.userNameLabel.text isEqualToString:g_App.userName]) {
            [cell.replyBtn setImage:[UIImage imageNamed:@"shanchu_3"] forState:UIControlStateNormal];
            cell.replyNameLabel.frame = CGRectMake(cell.frame.size.width - 30, 15, 30, 20);
            cell.replyNameLabel.text = @"删除";
            [cell.replyBtn addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
            cell.replyBtn.tag = [indexPath row] + 2000;
        }else{
            [cell.replyBtn setImage:[UIImage imageNamed:@"huifu"] forState:UIControlStateNormal];
            cell.replyNameLabel.text = @"回复";
            
            [cell.replyBtn addTarget:self action:@selector(replyBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            cell.replyBtn.tag = indexPath.row + 100;
        }
        return cell;
    }else{
        //        static NSString *cellIdentifier = @"commentCell";
        //        CommentCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        //        if (!cell) {
        CommentCell *cell = [[CommentCell alloc]init];
        //        }
        cell.backgroundColor = [UIColor blackColor];
        cell = [[[NSBundle mainBundle] loadNibNamed:@"CommentCell" owner:self options:nil] lastObject];
        cell = [CommentCell commentCellOwner:self];
        cell.userImageView.layer.cornerRadius = 18;
        cell.userImageView.layer.masksToBounds = YES;
        
        if([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]){
            [cell setPreservesSuperviewLayoutMargins:NO];
        }
        if (![self.dataArray count]) {
            return cell;
        }
        //楼层
        if (_endOfTheTableView.count >0) {
            int row = (int)[indexPath row];
            if (row > _dataArray.count-_endOfTheTableView.count-1) {
                int num =  [_messageNumStr intValue];
                cell.floorNum.text = [NSString stringWithFormat:@"%ld楼",num-(_dataArray.count - row)+1];
            }else{
                cell.floorNum.text = [NSString stringWithFormat:@"%ld楼",(long)[indexPath row]+1];
            }
        }else{
            cell.floorNum.text = [NSString stringWithFormat:@"%ld楼",(long)[indexPath row]+1];
        }
        cell.floorNum.textColor = RGBACOLOR(154, 205, 208, 1.0);
        
        int valueType = (int)[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"valueType"] integerValue];
        if (valueType == 1) {
            cell.userNameLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"];
            cell.userNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];//colorWithRed:192/255.0 green:126/255.0 blue:126/255.0 alpha:1.0
            [cell.userImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
            cell.userTimeLabel.text = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"];
        } else {
            cell.userNameLabel.text = [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"name"];
            cell.userNameLabel.textColor = [UIColor colorWithRed:238/255.0 green:126/255.0 blue:138/255.0 alpha:1.0];
            [cell.userImageView setImageWithURL:[NSURL URLWithString:[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
            NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"回复：%@ %@",[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"],[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"createTimeValue"]]];
            
            if ([[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"] length] > 0) {
                [str addAttribute:NSForegroundColorAttributeName value:COMMENTTITLECOLOR range:NSMakeRange(3, [[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"] length])];
            }
            [cell.userTimeLabel setAttributedText:str];
        }
        
        // UITextView-富文本
        NSString *cententString = [[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"content"];
        _showAllBtn.hidden = YES;
        
        
        if (cententString.length>100) {
            _showAllBtn.hidden = NO;
            if (_cellNum == (int)[indexPath row]) {
                if (!_showAllContent) {
                    cententString = [cententString substringToIndex:100];
                    cententString = [cententString stringByAppendingString:@"......"];
                }
                NSLog(@"我们想= 啊");
            }else{
                cententString = [cententString substringToIndex:100];
                cententString = [cententString stringByAppendingString:@"......"];
            }
            
        }
        //去掉换行符
        //        cententString = [cententString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
        //        cententString = [cententString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
        
        _labelFu = [[UITextView alloc]initWithFrame:CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-50-30, 20)];
        _labelFu.font = [UIFont boldSystemFontOfSize:16];
        _labelFu.userInteractionEnabled = NO;
        _labelFu.bounces = NO;
        _labelFu.editable = NO;
        [cell.contentView addSubview:_labelFu];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.lineSpacing = 8;// 字体的行间距
        NSDictionary *attributes = @{
                                     NSFontAttributeName:[UIFont systemFontOfSize:16],
                                     NSParagraphStyleAttributeName:paragraphStyle
                                     };
        //表情
        _faceNameArr = [NSMutableArray array];
        NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"FaceBag" ofType:@"plist"];
        NSMutableDictionary *data = [[NSMutableDictionary alloc]initWithContentsOfFile:plistPath];
        for (int i =1; i < 72; i ++) {
            [_faceNameArr addObject:data[[NSString stringWithFormat:@"%d",i]]];
        }
#warning 正则表达式 "\\[[^\\]]+\\]"--"\\#[^\\#]+\\#"
        NSString *regexString = @"\\[[^\\]]+\\]";
        NSMutableAttributedString *attri = [_labelFu.attributedText mutableCopy];//_model.content
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexString options:0 error:nil];
        NSArray *arrr = [regex matchesInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
        if (arrr.count>0) {
            for (int i =0 ; i < arrr.count; i ++) {
                NSTextCheckingResult *result = [regex firstMatchInString:cententString options:0 range:NSMakeRange(0, cententString.length)];
                NSString *faceString = [cententString substringToIndex:result.range.location];//表情之前
                NSAttributedString *attrStr1 = [[NSAttributedString alloc]initWithString:faceString];
                [attri appendAttributedString:attrStr1];
                NSString *faceStr = [cententString substringWithRange:NSMakeRange(result.range.location+1, result.range.length-2)];//表情
                for (int j = 0; j < _faceNameArr.count; j ++) {
                    if ([faceStr isEqualToString:_faceNameArr[j]]) {
                        NSTextAttachment *attch = [[NSTextAttachment alloc]init];
                        attch.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i#.png",j+1]];
                        attch.bounds = CGRectMake(0, -2, 30, 30);
                        NSAttributedString *string1 = [NSAttributedString attributedStringWithAttachment:attch];
                        [attri appendAttributedString:string1];
                        break;
                    }
                }
                cententString = [cententString substringFromIndex:result.range.location+result.range.length];
            }
            NSAttributedString *attrStr2 = [[NSAttributedString alloc] initWithString:cententString];
            [attri appendAttributedString:attrStr2];
            [attri addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:NSMakeRange(0, attri.length)];
            _labelFu.attributedText = [attri copy];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attri boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
            
            _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-50-30, rect.size.height+15);
        }else{
            NSMutableAttributedString *attrStr3 = [[NSMutableAttributedString alloc] initWithString:cententString];
            NSRange allRange = [cententString rangeOfString:cententString];
            [attrStr3 addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16.0] range:allRange];
            [attrStr3 addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:allRange];
            NSStringDrawingOptions options = NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
            CGRect rect = [attrStr3 boundingRectWithSize:CGSizeMake(KScreenWidth - 2*TMARGIN - 50-30, MAXFLOAT) options:options context:nil];
            
            _labelFu.attributedText = [[NSAttributedString alloc]initWithString:cententString attributes:attributes];
            int height = (int)rect.size.height/19;
            _labelFu.frame = CGRectMake(2*TMARGIN + 40, TMARGIN + 32, KScreenWidth - 2*TMARGIN-40-30, height*27+7);
        }
        cell.height = TMARGIN +32+_labelFu.frame.size.height+6;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //创建显示所有内容的按钮
        _showAllBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth-100, cell.height - 40, 100, 40)];
        _showAllBtn.backgroundColor = [UIColor clearColor];
        [_showAllBtn setTitleColor:RGBACOLOR(203, 203, 203, 1.0) forState:UIControlStateNormal];
        _showAllBtn.titleLabel.font = [UIFont systemFontOfSize:11];
        [_showAllBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 40, 0, 0)];
        _showAllBtn.tag = (int)[indexPath row];
        if (_cellNum == (int)[indexPath row]) {
            if (!_showAllContent) {
                [_showAllBtn setTitle:@"展开全部" forState:UIControlStateNormal];
                _showAllBtn.selected = YES;
            }else{
                _showAllBtn.selected = NO;
                [_showAllBtn setTitle:@"收起全部" forState:UIControlStateNormal];
            }
        }else{
            [_showAllBtn setTitle:@"展开全部" forState:UIControlStateNormal];
            _showAllBtn.selected = YES;
        }
        [_showAllBtn addTarget:self action:@selector(showAllContentComment:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:_showAllBtn];
        
        
        cell.replyBtn.frame = CGRectMake(cell.frame.size.width - 60, 15, 60, 20);//cell.frame.size.height/2-20
        [cell.replyBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 35)];
        cell.replyNameLabel.frame = CGRectMake(cell.frame.size.width - 30, 13.5, 30, 20);
        cell.replyNameLabel.textColor = RGBACOLOR(203, 203, 203, 1.0);
        [cell.contentView addSubview:cell.replyNameLabel];
        if ([cell.userNameLabel.text isEqualToString:g_App.userName]) {
            [cell.replyBtn setImage:[UIImage imageNamed:@"shanchu_3"] forState:UIControlStateNormal];
            cell.replyNameLabel.frame = CGRectMake(cell.frame.size.width - 30, 15, 30, 20);
            cell.replyNameLabel.text = @"删除";
            [cell.replyBtn addTarget:self action:@selector(showAlert:) forControlEvents:UIControlEventTouchUpInside];
            cell.replyBtn.tag = [indexPath row];
        }else{
            [cell.replyBtn setImage:[UIImage imageNamed:@"huifu"] forState:UIControlStateNormal];
            cell.replyNameLabel.text = @"回复";
            
            [cell.replyBtn addTarget:self action:@selector(replyBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            cell.replyBtn.tag = indexPath.row + 100;
        }
        return cell;
    }
    
}
-(void)showAllContentComment:(UIButton *)btn{
    _cellNum = btn.tag;
    
    if (btn.selected) {
        _showAllContent = YES;
    }else{
        _showAllContent = NO;
    }
    //    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:btn.tag inSection:0];
    //    [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationLeft];
    //    CGRect frame = _tableView.frame;
    //    frame.size.height += 100;
    //    _tableView.frame = frame;
    
    [self.tableView reloadData];
    
    [self refreshMessageView];
}

- (CGSize)sizeOfStr:(NSString *)str andFont:(UIFont *)font andMaxSize:(CGSize)size andLineBreakMode:(NSLineBreakMode)mode
{
    NSLog(@"版本号:%f",[[[UIDevice currentDevice]systemVersion]doubleValue]);
    CGSize s;
    if ([[[UIDevice currentDevice]systemVersion]doubleValue]>=7.0) {
        NSLog(@"ios7以后版本");
        // NSDictionary *dic=@{NSFontAttributeName:font};
        NSMutableDictionary  *mdic=[NSMutableDictionary dictionary];
        [mdic setObject:[UIColor redColor] forKey:NSForegroundColorAttributeName];
        [mdic setObject:font forKey:NSFontAttributeName];
        s = [str boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine
                           attributes:mdic context:nil].size;//| NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
    }
    else
    {
        NSLog(@"ios7之前版本");
        s=[str sizeWithFont:font constrainedToSize:size lineBreakMode:mode];
    }
    return s;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView.tag == 1000) {
        UITableViewCell *cell = (UITableViewCell*)[self tableView:self.lowTableView cellForRowAtIndexPath:indexPath];
        return cell.bounds.size.height;
    }else{
        UITableViewCell *cell = (UITableViewCell*)[self tableView:self.tableView cellForRowAtIndexPath:indexPath];
        return cell.bounds.size.height;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"mc" to_section:@"m" to_step:@"r" type:@"reply" id:@"0"];
    self.chooseIndex = (int)indexPath.row;
    if (g_App.userID) {
        
        int valueType = (int)[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"valueType"] integerValue];
        if (valueType == 1) {
            if ([[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"id"] isEqualToString:g_App.userID]) {
                self.inputTextField.placeholder = @"我来说两句";
                self.isReply = NO;
            } else {
                self.inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"userIDInfo"] objectForKey:@"name"]];
                self.isReply = YES;
            }
            //            self.inputTextField.placeholder = @"我来说两句";
            //            self.isReply = NO;
        } else {
            if ([[[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"id"] isEqualToString:g_App.userID]) {
                
                self.inputTextField.placeholder = @"我来说两句";
                self.isReply = NO;
            } else {
                self.inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", [[[self.dataArray objectAtIndex:indexPath.row] objectForKey:@"replyUserIDInfo"] objectForKey:@"name"]];
                self.isReply = YES;
            }
        }
        
        [self.inputTextField becomeFirstResponder];
        return;
    } else {
        _markLoginType = 1;
        [[LogHelper shared] writeToFilefrom_page:@"mc" from_section:@"m" from_step:@"r" to_page:@"nl" to_section:@"m" to_step:@"m" type:@"r" id:@""];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        return;
    }
}

- (void)replyBtnPressed:(UIButton*)btn {
    self.chooseIndex = (int)btn.tag - 100;
    if (g_App.userID) {
        int valueType = (int)[[[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"valueType"] integerValue];
        if (valueType == 1) {
            if ([[[[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"userIDInfo"] objectForKey:@"id"] isEqualToString:g_App.userID]) {
                
                self.inputTextField.placeholder = @"真的不来几句吗骚年 ಠ౪ಠ";
                self.isReply = NO;
            } else {
                self.inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", [[[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"userIDInfo"] objectForKey:@"name"]];
                self.isReply = YES;
            }
            //            self.inputTextField.placeholder = @"我来说两句";
            //            self.isReply = NO;
        } else {
            if ([[[[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"replyUserIDInfo"] objectForKey:@"id"] isEqualToString:g_App.userID]) {
                
                self.inputTextField.placeholder = @"真的不来几句吗骚年 ಠ౪ಠ";
                self.isReply = NO;
            } else {
                self.inputTextField.placeholder = [NSString stringWithFormat:@"回复%@", [[[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"replyUserIDInfo"] objectForKey:@"name"]];
                self.isReply = YES;
            }
        }
        
        [self.inputTextField becomeFirstResponder];
        return;
    } else {
        [[LogHelper shared] writeToFilefrom_page:@"mc" from_section:@"m" from_step:@"r" to_page:@"nl" to_section:@"m" to_step:@"m" type:@"r" id:@"0"];
        
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        return;
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    //    self.inputTextField.text = nil;
    //    self.sendBtn.hidden = YES;
    self.appraiseBtn.hidden = NO;
    self.touchBgView.hidden = YES;
    //    [self.inputTextField resignFirstResponder];
    self.isScroll = YES;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 计算当前页数
    int currentPage = _collectionView.contentOffset.x / (KScreenWidth*3/4);
    _pageC.currentPage = currentPage;
//    if (scrollView == _scrollView) {
//        self.isScroll = YES;
//        self.isScrolling = YES;
//    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    if (scrollView == _scrollView) {
//        
//        self.isScrolling = NO;
//        [self performSelector:@selector(refreshScrollStatus) withObject:nil afterDelay:0.5];
//    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
//    if (scrollView == _scrollView) {
//        
//        self.isScrolling = NO;
//        [self performSelector:@selector(refreshScrollStatus) withObject:nil afterDelay:0.5];
//    }
}

- (void)refreshScrollStatus {
    if (!self.isScrolling) {
        self.isScroll = NO;
    }
}

- (void)touchBgViewTouchInView {
    //    self.inputTextField.text = nil;
    if ([self.inputTextField.text isEqualToString:@""]) {
        self.inputTextField.placeholder = @"我来说两句";
        self.isReply = NO;
    }
    self.sendBtn.hidden = YES;
    self.appraiseBtn.hidden = NO;
    self.touchBgView.hidden = YES;
    [self.inputTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [[LogHelper shared] writeToFilefrom_page:@"md" from_section:@"m" from_step:@"d" to_page:@"mc" to_section:@"m" to_step:@"r" type:@"comment" id:@"0"];
    
    [self misDeleteView];
    if (!g_App.userID) {
        [[LogHelper shared] writeToFilefrom_page:@"mc" from_section:@"m" from_step:@"r" to_page:@"nl" to_section:@"m" to_step:@"r" type:@"comment" id:@""];
        LoginAlertView *alertView = [[LoginAlertView alloc] initWithDelegate:self title:@"才可以评论哦~"];
        [alertView show];
        self.touchBgView.hidden = YES;
        [textField resignFirstResponder];
        return;
    }
    self.sendBtn.hidden = NO;
    self.appraiseBtn.hidden = YES;
    self.touchBgView.hidden = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    self.sendBtn.hidden = YES;
    self.appraiseBtn.hidden = NO;
    self.touchBgView.hidden = YES;
    if (!textField.text.length) {
        textField.text = @"1";
        [textField resignFirstResponder];
        return YES;
    }
    if (self.isReply) {
        [self replyMessage:textField.text];
    } else
        [self commentMessage:textField.text];
    textField.text = @"";
    [textField resignFirstResponder];
    //    self.touchBgView.hidden = YES;
    return YES;
}


#pragma mark - LoginAlertViewDelegate
- (void)loginAlertViewPressedWithIndex:(int)index {
    if (index) {
        
        NSArray *resArr = [self stringWithProtocol];
        [[LogHelper shared] writeToFilefrom_page:resArr[0] from_section:@"m" from_step:@"r" to_page:resArr[1] to_section:@"m" to_step:@"r" type:resArr[2] id:@"0"];
        LoginViewController *loginView = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
        CustomNavigationController *nav = [[CustomNavigationController alloc] initWithRootViewController:loginView];
        [self presentViewController:nav animated:YES completion:nil];
    }
}

- (NSArray *)stringWithProtocol{
    NSArray *returnStr = @[@"",@"",@""];
    switch (_markLoginType) {
        case 0:
            return returnStr = @[@"pl",@"mc",@"comment"];
            break;
        case 1:
            return returnStr = @[@"pl",@"mc",@"reply"];
            break;
        case 2:
            return returnStr = @[@"nl",@"mp",@"insert"];
            break;
        default:
            return returnStr;
            break;
    }
    
}


#pragma mark - UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *picName = [[request URL] absoluteString];
    if ([picName isEqualToString:@"about:blank"]) {
        NSLog(@"picName is %@",picName);
        if ([picName hasPrefix:@"pic:"]) {
            
            return NO;
        }else {
            return YES;
        }
        
    }else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:picName]];
        return NO;
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"webViewDidStartLoad");
    [self.indicatorView startAnimating];
    CGRect frame = CGRectMake(webView.frame.origin.x, webView.frame.origin.y, KScreenWidth, webView.frame.size.height);
    NSString *fitHeight = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    frame.size.height = [fitHeight floatValue];
    webView.frame = frame;
    
    //    NSLog(@"%f",[fitHeight floatValue]);
    int height = 0;
    if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
        self.gameView.frame = CGRectMake(self.gameView.frame.origin.x, webView.frame.size.height + webView.frame.origin.y , self.gameView.frame.size.width, self.gameView.frame.size.height);
        height = self.gameView.bounds.size.height;
        self.gameView.hidden = NO;
    }
    
    self.authTopView.frame = CGRectMake(0, 0, ScreenSizeWidth, 100);
    self.webView.frame = CGRectMake(0, 110, ScreenSizeWidth, 100);
    self.authorView.frame = CGRectMake(0, 220, ScreenSizeWidth, 100);
    self.appraiseView.frame = CGRectMake(0, 330, ScreenSizeWidth, 100);
    
//    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,self.appraiseView.frame.size.height + self.appraiseView.frame.origin.y + 3, self.tableView.frame.size.width, self.tableView.contentSize.height);
    
//    [self.scrollView setContentSize:CGSizeMake(0,height + self.authTopView.frame.size.height + webView.frame.size.height + self.tableView.contentSize.height + self.appraiseView.frame.size.height + 6)];
    
    
    if (self.dataArray.count) {
        self.tableView.hidden = NO;
    }
    if ([[self.dataDic objectForKey:@"praiseCount"] integerValue]) {
        self.appraiseView.hidden = NO;
    } else {
//        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, height + webView.frame.size.height + webView.frame.origin.y + 3, self.tableView.frame.size.width, self.tableView.contentSize.height);
        
//        [self.scrollView setContentSize:CGSizeMake(0, height + self.authTopView.frame.size.height + webView.frame.size.height + self.tableView.contentSize.height)];
    }
//    if (self.scrollView.contentSize.height < self.view.bounds.size.height) {
//        [self.scrollView setContentSize:CGSizeMake(0, self.view.bounds.size.height + 1)];
//    }
    
    if ([[self.dataDic objectForKey:@"contentValue"] length]) {
        self.webView.hidden = NO;
    }
    
    [self performSelector:@selector(hideView) withObject:nil afterDelay:0.3];
}

- (void)hideView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"webViewDidFinishLoad");
    self.isLoadingOver = YES;
    [self.indicatorView stopAnimating];
    
    CGRect frame = webView.frame;
    NSString *fitHeight = [webView stringByEvaluatingJavaScriptFromString:@"document.body.scrollHeight"];
    frame.size.height = [fitHeight floatValue];
    webView.frame = frame;
    
    int height = 0;
    if ([[self.dataDic objectForKey:@"gamesID"] integerValue]) {
        self.gameView.frame = CGRectMake(self.gameView.frame.origin.x, webView.frame.size.height + webView.frame.origin.y + 80, self.gameView.frame.size.width, self.gameView.frame.size.height);
        height = self.gameView.bounds.size.height;
        self.gameView.hidden = NO;
    }
    
    self.authTopView.frame = CGRectMake(0, 0, ScreenSizeWidth, self.authTopView.frame.size.height);
    self.webView.frame = CGRectMake(0, self.authTopView.bottom, ScreenSizeWidth, self.webView.frame.size.height);
    self.authorView.frame = CGRectMake(0, self.webView.bottom, ScreenSizeWidth, 110);
    self.appraiseView.frame = CGRectMake(0, self.authorView.bottom, ScreenSizeWidth, self.appraiseView.frame.size.height);
    
    self.tableTopView.frame = CGRectMake(0, 0, ScreenSizeWidth, self.authTopView.frame.size.height + self.webView.frame.size.height +110 +self.appraiseView.frame.size.height);
    
//    self.authorView.frame = CGRectMake(self.authorView.frame.origin.x, self.webView.bottom, self.authorView.frame.size.width, self.authorView.size.height);
    
//    self.appraiseView.frame = CGRectMake(self.appraiseView.frame.origin.x, height + webView.frame.size.height + webView.frame.origin.y + 3 + self.authorView.frame.size.height, self.appraiseView.frame.size.width, self.appraiseView.frame.size.height);
//    CGRect fffff =self.appraiseView.frame;
//    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,self.appraiseView.frame.size.height + self.appraiseView.frame.origin.y + 3, self.tableView.frame.size.width, self.tableView.contentSize.height);
    
//    [self.scrollView setContentSize:CGSizeMake(0,height + self.authTopView.frame.size.height + webView.frame.size.height + self.authorView.size.height + self.tableView.contentSize.height + self.appraiseView.frame.size.height + 6)];
    
    if (self.dataArray.count) {
        self.tableView.hidden = NO;
    }
    if ([[self.dataDic objectForKey:@"praiseCount"] integerValue]) {
        self.appraiseView.hidden = NO;
    } else {
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, height + webView.frame.size.height + webView.frame.origin.y + 3 + self.authorView.size.height, self.tableView.frame.size.width, self.tableView.contentSize.height);
        
//        [self.scrollView setContentSize:CGSizeMake(0, height + self.authTopView.frame.size.height + webView.frame.size.height + self.authorView.size.height + self.tableView.contentSize.height)];
    }
//    if (self.scrollView.contentSize.height < self.view.bounds.size.height) {
//        [self.scrollView setContentSize:CGSizeMake(0, self.view.bounds.size.height + 1)];
//    }
    
    if ([[self.dataDic objectForKey:@"contentValue"] length]) {
        self.webView.hidden = NO;
    }
    
    UITapGestureRecognizer* singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    
    [self.webView addGestureRecognizer:singleTap];
    singleTap.delegate = self;
    singleTap.cancelsTouchesInView = NO;
    
    self.touchBgView.hidden = YES;
    _imageView.alpha = 1.0;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self.indicatorView stopAnimating];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - CustomAlertViewDelegate
- (void)customAlertViewBtnWasSeletecd:(CustomAlertView *)alertView withIndex:(NSInteger)index {
    [self goBackToPreview:nil];
}

#pragma mark - 加载数据
- (void)messageDetail {
    [_webView stopLoading];
    _imgArr = [NSMutableArray array];
    if (!_isAppraise) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_MESSAGE_DETAIL,@"r",
                                    self.infoID,@"id",
                                    @"1",@"withContent",
                                    @"1",@"withPraise",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(messageDetailFinish:) method:GETDATA];
    _isAppraise = NO;
}

- (void)messageDetailFinish:(NSDictionary*)dic {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        _netImageView.alpha = 0;
        self.dataDic = [dic objectForKey:@"results"];
        
        //        NSFileManager *fileManager = [[NSFileManager alloc]init];
        //        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        //        NSString *documentDiretory = [paths objectAtIndex:0];
        //        NSString *preFolder = [NSString stringWithFormat:@"%@/111Folder",documentDiretory];
        //        NSString *urlStr = @" http://api.playsm.com/index.php?r=message/rtfContent&id=435";
        //        NSString * path=[preFolder stringByAppendingString:[NSString stringWithFormat:@"/%lu.html",(unsigned long)[urlStr hash]]];
        //
        //        if (![[NSFileManager defaultManager] fileExistsAtPath:preFolder]) {
        //            [fileManager createDirectoryAtPath:preFolder withIntermediateDirectories:YES attributes:nil error:nil];
        //            [fileManager createFileAtPath:path contents:nil attributes:nil];
        //            NSArray *arr = [[NSArray alloc] initWithObjects:nil, nil];
        //            [arr writeToFile:path atomically:YES];
        //        }
        //
        //        NSString * htmlResponseStr=[NSString stringWithContentsOfURL:[NSURL URLWithString:@" http://api.playsm.com/index.php?r=message/rtfContent&id=435"] encoding:NSUTF8StringEncoding error:Nil];
        //        [htmlResponseStr writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        _contentStr = self.dataDic[@"content"];
        [self.dataArray removeAllObjects];
        for (NSDictionary *item in [[dic objectForKey:@"results"] objectForKey:@"commentList"] ) {
            [self.dataArray addObject:item];
        }
        [self.tableView reloadData];
        [self refreshView];
    } else {
        if ([[dic objectForKey:@"code"] integerValue] == 51) {
            CustomAlertView * alertView=[[CustomAlertView alloc]initWithTitle:@"脾气差的提示君" message:[dic objectForKey:@"error"] delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil];
            [alertView show];
        }else{
            _netImageView.frame = CGRectMake(0, 0, KScreenWidth, KScreenheight - 64 - 40);
            _netImageView.alpha = 0;
            
            UIButton *refreshBtn = [CustomTool createBtn];
            refreshBtn.frame = CGRectMake(KScreenWidth/2 - 50, KScreenheight - 64 - 40 - 80, 100, 30);
            [_netImageView addSubview:refreshBtn];
            [refreshBtn addTarget:self action:@selector(messageDetail) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    NSString *dataString = _contentStr;
    NSData *htmlData = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    TFHpple *xpathParser = [[TFHpple alloc] initWithHTMLData:htmlData];
    NSArray *elements = [xpathParser searchWithXPathQuery:@"//img"];
    for (int i =0; i < elements.count; i ++) {
        TFHppleElement * aElement = [elements objectAtIndex:i];
        NSDictionary *aDic = [aElement attributes];
        NSString *strr = aDic[@"src"];
        [_imgArr addObject:strr];
    }
}

- (void)commentMessage:(NSString*)msg {
    
    
    
    [[LogHelper shared] writeToFilefrom_page:@"mc" from_section:@"m" from_step:@"r" to_page:@"mc" to_section:@"m" to_step:@"a" type:@"comment" id:self.infoID];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_COMMENT_MESSAGE,@"r",
                                    self.infoID,@"valueID",
                                    msg,@"content",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(commentMessageFinish:) method:POSTDATA];
}

- (void)commentMessageFinish:(NSDictionary*)dic {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        
        if (_moreCommentPage*10 < [_messageNumStr intValue]) {
            //            [_endOfTheTableView addObject:[dic objectForKey:@"results"]];
            [_moreOutOfComments addObject:[dic objectForKey:@"results"]];
        }
        
        NSString *path = [_documentDiretory stringByAppendingPathComponent:@"cartoonMessageDetailList"];
        [_fileManager removeItemAtPath:path error:nil];
        _isReplOrComment = YES;
        [self loadMessageData];
        [self addIntergration:@"2"];
        
    }
}

- (void)replyMessage:(NSString*)msg {
    self.isReply = NO;
    self.inputTextField.placeholder = @"真的不来几句吗骚年 ಠ౪ಠ";
    [[LogHelper shared] writeToFilefrom_page:@"mc" from_section:@"m" from_step:@"r" to_page:@"mc" to_section:@"m" to_step:@"a" type:@"reply" id:[[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"id"]];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_CONTENT_REPLY_MESSAGE,@"r",
                                    [[self.dataArray objectAtIndex:self.chooseIndex] objectForKey:@"id"],@"contentID",
                                    msg,@"content",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(replyMessageFinish:) method:POSTDATA];
}

- (void)replyMessageFinish:(NSDictionary*)dic {
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        if (_moreCommentPage*10 < [_messageNumStr intValue]) {
            //            [_endOfTheTableView addObject:[dic objectForKey:@"results"]];
            [_moreOutOfComments addObject:[dic objectForKey:@"results"]];
        }
        NSString *path = [_documentDiretory stringByAppendingPathComponent:@"cartoonMessageDetailList"];
        [_fileManager removeItemAtPath:path error:nil];
        _isReplOrComment = YES;
        [self loadMessageData];
        [self addIntergration:@"2"];
    }
}

- (void)appraiseMsg {
    [[LogHelper shared] writeToFilefrom_page:@"nl" from_section:@"m" from_step:@"r" to_page:@"mp" to_section:@"m" to_step:@"a" type:@"insert" id:self.infoID];
    
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_MESSAGE_PRAISE,@"r",
                                    self.infoID,@"id",nil];
    
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(appraiseMsgFinish:) method:POSTDATA];
}

- (void)appraiseMsgFinish:(NSDictionary*)dic {
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        NSString *path = [_documentDiretory stringByAppendingPathComponent:@"cartoonMessageDetailList"];
        [_fileManager removeItemAtPath:path error:nil];
        //        [self messageDetail];
        [self loadWebData];
        if (![dic[@"results"] isEqualToString:@"取消赞"]) {
            [self addIntergration:@"3"];
        }
        
    }else{
        
    }
}

- (void)addIntergration:(NSString *)type{
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:@{@"r":@"userScore/add",@"type":type} object:self action:@selector(addInterCount:) method:POSTDATA];
}

- (void)addInterCount:(NSDictionary *)d {
    if ([[NSString stringWithFormat:@"%@",d[@"status"]] isEqualToString:@"0"]) {
        //        [[upVote shared] startAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",d[@"score"]] point:CGPointMake(KScreenWidth - 15,KScreenheight - 15) fatherView:self.view];
        if ([[NSString stringWithFormat:@"%@",d[@"type"]] isEqualToString:@"3"]) {
            [[upVote shared] startAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",d[@"score"]]  point:CGPointMake(KScreenWidth - 40,KScreenheight - 100) fatherView:self.view color:[UIColor colorWithRed:255/255.0 green:80/255.0 blue:0/255.0 alpha:1.0]];
        }else if([[NSString stringWithFormat:@"%@",d[@"type"]] isEqualToString:@"2"]){
            [[upVote shared] startAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",d[@"score"]]  point:CGPointMake(KScreenWidth/2,KScreenheight - 100) fatherView:self.view color:[UIColor colorWithRed:255/255.0 green:80/255.0 blue:0/255.0 alpha:1.0]];
        }else if ([[NSString stringWithFormat:@"%@",d[@"type"]] isEqualToString:@"4"]){
            [[upVote shared] startMiddleAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",d[@"score"]]   point:CGPointMake(KScreenWidth/2, 100) fatherView:self.view];
        }else if ([[NSString stringWithFormat:@"%@",d[@"type"]] isEqualToString:@"5"]){
            [[upVote shared] startMiddleAnimitionTop:@"积分" bottom:[NSString stringWithFormat:@"+%@",d[@"score"]]   point:CGPointMake(KScreenWidth/2, 60) fatherView:self.view];
            
        }
        
        
    }
    
}

@end
