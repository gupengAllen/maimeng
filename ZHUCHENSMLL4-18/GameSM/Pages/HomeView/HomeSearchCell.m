//
//  HomeSearchCell.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/6.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "HomeSearchCell.h"
//#import "Tool.h"

#define KScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define KScreenheight ([UIScreen mainScreen].bounds.size.height)

@implementation HomeSearchCell

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}
-(void)createView{
    self.clockImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 10, 10)];
    self.clockImageView.image = [UIImage imageNamed:@"lishi"];
    [self.contentView addSubview:self.clockImageView];
    
    self.searchTitle = [[UILabel alloc]initWithFrame:CGRectMake(50, 2, 200, 25)];
    self.searchTitle.textColor = [UIColor colorWithRed:40/255.0 green:40/255.0 blue:40/255.0 alpha:1.0];
    self.searchTitle.font = [UIFont systemFontOfSize:14.0];
    self.searchTitle.text = _dataStr;
    [self.contentView addSubview:self.searchTitle];
    
    self.deleteBtn = [[UIButton alloc]initWithFrame:CGRectMake(KScreenWidth - 50, 5, 50, 20)];
    [self.deleteBtn setTitle:@"x" forState:UIControlStateNormal];
    [self.deleteBtn setTitleColor:[UIColor colorWithRed:201/255.0 green:201/255.0 blue:201/255.0 alpha:1.0] forState:UIControlStateNormal];
    //    [self.deleteBtn setImage:[UIImage imageNamed:@"biyan_"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.deleteBtn];}
-(void)setDataStr:(NSString *)dataStr{
    _dataStr = dataStr;
    [self createView];
}

//自定义分割线
- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, rect); //上分割线，
//    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
//    CGContextStrokeRect(context, CGRectMake(20, -1, rect.size.width - 40, 1));
    //下分割线
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1].CGColor);
    CGContextStrokeRect(context, CGRectMake(20, rect.size.height, rect.size.width - 40, 1));
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
