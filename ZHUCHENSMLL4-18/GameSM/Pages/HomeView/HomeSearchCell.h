//
//  HomeSearchCell.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/1/6.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeSearchCell : UITableViewCell

@property (nonatomic, strong)UILabel *searchTitle;
@property (nonatomic, strong)UIImageView *clockImageView;
@property (nonatomic, strong)UIButton *deleteBtn;

@property (nonatomic, strong)NSString *dataStr;

@end
