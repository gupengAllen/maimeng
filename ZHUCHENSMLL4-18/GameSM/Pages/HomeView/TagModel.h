//
//  TagModel.h
//  GameSM
//
//  Created by 顾鹏 on 16/4/21.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TagModel : NSObject
@property (nonatomic, copy)NSString *id;
@property (nonatomic, copy)NSString *name;
@property (nonatomic, copy)NSString *images;
@property (nonatomic, copy)NSString *priority;
@property (nonatomic, copy)NSString *status;
@property (nonatomic, copy)NSString *createTime;
@property (nonatomic, copy)NSString *modifyTime;
@property (nonatomic, copy)NSString *userID;
+ (NSMutableArray *)paraseDict:(NSDictionary *)dict ;
+ (NSMutableArray *)paraseAllDict:(NSDictionary *)dict;
+ (NSMutableArray *)paraseDictThird:(NSDictionary *)dict;
@end
