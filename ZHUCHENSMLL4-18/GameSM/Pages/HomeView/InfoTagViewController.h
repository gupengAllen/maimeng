//
//  InfoTagViewController.h
//  GameSM
//
//  Created by 祝嘉蔓 on 16/4/22.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "BaseViewController.h"
#import "TopShowView.h"
#import "PullingRefreshTableView.h"

@interface InfoTagViewController : BaseViewController

@property (strong,nonatomic) UIView *topView;
@property (strong,nonatomic) PullingRefreshTableView *tagTableView;
@property (strong,nonatomic) UICollectionView *tagCollectionView;

@property (nonatomic, strong) TopShowView *topShowView;
@property (nonatomic, assign) BOOL isLoading;
@property (nonatomic, assign) BOOL isMore;
@property (nonatomic, strong) NSString *infoCount;
//
@property (strong,nonatomic) NSMutableArray *dataArray;
@property (nonatomic, copy)NSString *tagName;

@end
