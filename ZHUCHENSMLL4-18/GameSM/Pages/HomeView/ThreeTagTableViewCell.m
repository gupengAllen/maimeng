//
//  ThreeTagTableViewCell.m
//  GameSM
//
//  Created by 顾鹏 on 16/4/26.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "ThreeTagTableViewCell.h"
#import "TagModel.h"
#import "UIView+Addtion.h"
#import "InfoTagViewController.h"
@implementation ThreeTagTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setTagArr:(NSMutableArray<TagModel *> *)tagArr {
    _tagArr = tagArr;
    _firstTag.layer.cornerRadius = 4.0;
    _firstTag.layer.borderColor = CUSTOMBORDER.CGColor;
    _firstTag.layer.borderWidth = 1.0;
    
    _secondTag.layer.cornerRadius = 4.0;
    _secondTag.layer.borderColor = CUSTOMBORDER.CGColor;
    _secondTag.layer.borderWidth = 1.0;
    
    _thirdTag.layer.cornerRadius = 4.0;
    _thirdTag.layer.borderColor = CUSTOMBORDER.CGColor;
    _thirdTag.layer.borderWidth = 1.0;
    
    
    switch (tagArr.count) {
        case 1:
            _secondTag.hidden = YES;
            _thirdTag.hidden = YES;
            [_firstTag setTitle:[tagArr[0] name] forState:UIControlStateNormal];
            break;
        case 2:
            _thirdTag.hidden = YES;
            [_firstTag setTitle:[tagArr[0] name] forState:UIControlStateNormal];
            [_secondTag setTitle:[tagArr[1] name] forState:UIControlStateNormal];
            break;
        case 3:
            [_firstTag setTitle:[tagArr[0] name] forState:UIControlStateNormal];
            [_secondTag setTitle:[tagArr[1] name] forState:UIControlStateNormal];
            [_thirdTag setTitle:[tagArr[2] name] forState:UIControlStateNormal];

        default:
            break;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)firstPress:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"mll" from_section:@"m" from_step:@"l" to_page:@"mldl" to_section:@"m" to_step:@"l" type:@"" id:@"0" detail:@{@"keyword":[_tagArr[0] name]}];
    InfoTagViewController *infoTag = [[InfoTagViewController alloc] init];
    infoTag.tagName = [_tagArr[0] name];
    [self.viewController.navigationController pushViewController:infoTag animated:YES];

}

- (IBAction)secondPress:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"mll" from_section:@"m" from_step:@"l" to_page:@"mldl" to_section:@"m" to_step:@"l" type:@"" id:@"0" detail:@{@"keyword":[_tagArr[1] name]}];
    InfoTagViewController *infoTag = [[InfoTagViewController alloc] init];
    infoTag.tagName = [_tagArr[1] name];
    [self.viewController.navigationController pushViewController:infoTag animated:YES];

}

- (IBAction)thirdPress:(id)sender {
    [[LogHelper shared] writeToFilefrom_page:@"mll" from_section:@"m" from_step:@"l" to_page:@"mldl" to_section:@"m" to_step:@"l" type:@"" id:@"0" detail:@{@"keyword":[_tagArr[2] name]}];
    InfoTagViewController *infoTag = [[InfoTagViewController alloc] init];
    infoTag.tagName = [_tagArr[2] name];
    [self.viewController.navigationController pushViewController:infoTag animated:YES];

}
@end
