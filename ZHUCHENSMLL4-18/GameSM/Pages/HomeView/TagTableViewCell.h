//
//  TagTableViewCell.h
//  GameSM
//
//  Created by 顾鹏 on 16/4/21.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TagModel;
@interface TagTableViewCell : UITableViewCell
@property (nonatomic, retain)NSMutableArray <TagModel *>*tagArr;
@end
