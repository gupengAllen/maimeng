//
//  InfoTagViewController.m
//  GameSM
//
//  Created by 祝嘉蔓 on 16/4/22.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "InfoTagViewController.h"
#import "PullingRefreshTableView.h"
#import "InfoCell.h"
#import "InfoDetailViewController.h"

@interface InfoTagViewController ()<UITableViewDataSource,UITableViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
{
    //2.8
    BOOL            _showAllTag;
    NSString        *_identiCell;
    NSString        *_lastID;
    int             _page;
    int             _size;
    int             _tagPage;
    NSInteger             _markTag;
    LYTabBarController *_lyTabbar;
    NSMutableArray  *_tagsArr;
    UIButton        *showBtn;
    UILabel *labelOne1;
    UIView *bottomView;
    NSMutableArray *_tagType;
    
    
}
@end

@implementation InfoTagViewController
-(void)viewWillAppear:(BOOL)animated{
    [_lyTabbar hiddenBar:YES animated:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTitleName:_tagName];
    [self initData];
    [self addBackBtn];
    _tagType = [NSMutableArray array];
    [self createTopView];
    [self createCollectionView];
    [self createTableView];
    // Do any additional setup after loading the view from its nib.
    [self loadTags];
    [self loadTagData];
}

-(void)initData{
    _showAllTag = NO;
    _page = 1;
    _tagPage = 1;
    _size = 80;
//    _tagName = @"萌宠";
    _lyTabbar = (LYTabBarController *)self.tabBarController;
    self.dataArray = [NSMutableArray array];
    _tagsArr = [NSMutableArray array];
}

#pragma mark - UI
-(void)createTopView{
    self.topView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenSizeWidth, 101)];
    self.topView.backgroundColor = CUSTOMBGCOLOR;
    [self.view addSubview:self.topView];
    
    UIImageView *lineOne = [[UIImageView alloc]initWithFrame:CGRectMake(15, 12, 2, 12)];
    lineOne.backgroundColor = CUSTOMREDCOLOR;
    lineOne.layer.masksToBounds = YES;
    lineOne.layer.cornerRadius = 1;
    [self.topView addSubview:lineOne];
    UILabel *labelOne = [CustomTool createLabelWithFrame:CGRectMake(lineOne.right+10, 10, 80, 18) andType:14 andColor:CUSTOMREDCOLOR];
    labelOne.text = @"全部标签";
    [self.topView addSubview:labelOne];
    UILabel *labelTwo = [CustomTool createLabelWithFrame:CGRectMake(ScreenSizeWidth-50-40, 8.5, 40, 18) andType:12 andColor:CUSTOMTITLECOLOER2];
    labelTwo.textAlignment = 2;
    labelTwo.text = @"展开";
    [self.topView addSubview:labelTwo];
    
    showBtn = [[UIButton alloc]initWithFrame:CGRectMake(ScreenSizeWidth-50, 0, 35, 35)];
    [showBtn setImage:[UIImage imageNamed:@"xialaDetail"] forState:UIControlStateNormal];
    [showBtn addTarget:self action:@selector(showAllTags:) forControlEvents:UIControlEventTouchUpInside];
    [self.topView addSubview:showBtn];
    
//--
    bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.topView.frame), ScreenSizeWidth, ScreenSizeHeight - CGRectGetMaxY(self.topView.frame))];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    UIImageView *lineOne1 = [[UIImageView alloc]initWithFrame:CGRectMake(15, 12, 2, 12)];
    lineOne1.backgroundColor = CUSTOMREDCOLOR;
    lineOne1.layer.masksToBounds = YES;
    lineOne1.layer.cornerRadius = 1;
    [bottomView addSubview:lineOne1];
    labelOne1 = [CustomTool createLabelWithFrame:CGRectMake(lineOne.right+10, 10, 80, 18) andType:14 andColor:CUSTOMREDCOLOR];
    labelOne1.text = _tagName;
    [bottomView addSubview:labelOne1];
    
    
}

-(void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    flowLayout.minimumInteritemSpacing = 5;
    flowLayout.minimumLineSpacing = 10;
    
        _tagCollectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 40, ScreenSizeWidth, 40) collectionViewLayout:flowLayout];
        _tagCollectionView.delegate = self;
        _tagCollectionView.dataSource = self;
        _tagCollectionView.backgroundColor = CUSTOMBGCOLOR;
        _identiCell = @"tagCollectionCell";
        [_tagCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:_identiCell];
    [self.view addSubview:_tagCollectionView];
}

-(void)createTableView{
    self.tagTableView = [[PullingRefreshTableView alloc]initWithFrame:CGRectMake(0, 40, ScreenSizeWidth, ScreenSizeHeight-64-101-40)];
    self.tagTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tagTableView.dataSource = self;
    self.tagTableView.delegate = self;
    [bottomView addSubview:self.tagTableView];
//     [self.view bringSubviewToFront:_tagCollectionView];
}

#pragma mark - 点击事件
-(void)showAllTags:(UIButton *)btn{
    
    
    if(btn.selected){
        _showAllTag = NO;
        [UIView animateWithDuration:0.2 animations:^{
            btn.transform = CGAffineTransformMakeRotation(0);
            _tagCollectionView.frame = CGRectMake(0, 40, ScreenSizeWidth, 40);
            bottomView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), ScreenSizeWidth, bottomView.frame.size.width);
            
        }];
        [_tagCollectionView reloadData];
    }else{
        _showAllTag = YES;
        [UIView animateWithDuration:0.2 animations:^{
            btn.transform = CGAffineTransformMakeRotation(-(M_PI-0.000001));
            if (_tagsArr.count/4 * 40 > ScreenSizeHeight - 104) {
                _tagCollectionView.frame = CGRectMake(0, 40, ScreenSizeWidth, ScreenSizeHeight - 104);
            }else{
                _tagCollectionView.scrollEnabled = NO;
                _tagCollectionView.frame = CGRectMake(0, 40, ScreenSizeWidth, _tagsArr.count/4 * 40);
                
            }
            bottomView.frame = CGRectMake(0, CGRectGetMaxY(_tagCollectionView.frame)+2, ScreenSizeWidth, bottomView.frame.size.width);
        }];
        [_tagCollectionView reloadData];
    }
    btn.selected = !btn.selected;
}

#pragma mark - collectionDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_tagsArr.count>4) {
        if (_showAllTag) {
            return _tagsArr.count;
        }else{
            return 4;
        }
    }else
        return _tagsArr.count;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:_identiCell forIndexPath:indexPath];
    UILabel *label = [CustomTool createLabelWithFrame:CGRectMake(0, 0, 80*_Scale6, 30) andType:12 andColor:CUSTOMTITLECOLOER2];
    label.text = _tagsArr[indexPath.row][@"name"];
    label.backgroundColor = [UIColor whiteColor];
    label.layer.masksToBounds = YES;
    label.layer.cornerRadius = 6;
    label.textAlignment = 1;
    if ([_tagType[indexPath.row] isEqualToString:@"1"]) {
        cell.layer.borderWidth = 1.0;
        cell.layer.borderColor = CUSTOMREDCOLOR.CGColor;
        cell.layer.cornerRadius = 2.0;
        label.textColor = CUSTOMREDCOLOR;
        
    }else{
        cell.layer.borderWidth = 0;
        label.textColor = CUSTOMTITLECOLOER2;

    }
    [cell.contentView addSubview:label];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[LogHelper shared] writeToFilefrom_page:@"mldl" from_section:@"m" from_step:@"l" to_page:@"mldl" to_section:@"m" to_step:@"l" type:@"direct" id:@"0" detail:@{@"keyword":_tagName}];
    
    
    UICollectionViewCell *markTagCell = [collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:_markTag inSection:0]];
    markTagCell.layer.borderWidth = 0;
    [_tagType replaceObjectAtIndex:_markTag withObject:@"0"];
    _markTag = indexPath.row;
    UICollectionViewCell *cell =  [collectionView cellForItemAtIndexPath:indexPath];
    cell.layer.borderWidth = 1.0;
    cell.layer.borderColor = CUSTOMREDCOLOR.CGColor;
    cell.layer.cornerRadius = 2.0;
    if (_showAllTag) {
        [self performSelector:@selector(showAllTags:) withObject:showBtn afterDelay:0];
    }else{
    }
    [_tagType replaceObjectAtIndex:indexPath.row withObject:@"1"];
    _tagName = _tagsArr[indexPath.row][@"name"];
    [self changeTitleView:_tagName];
    labelOne1.text = _tagName;
    [self loadData:NO];
}
//
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(80*_Scale6, 30);
}
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(8, 15, 15, 15);
}

#pragma mark - tableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _dataArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"infoCell";
        InfoCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (!cell) {
            cell = [InfoCell infoCellOwner:self];
        }
        cell = [[[NSBundle mainBundle] loadNibNamed:@"InfoCell" owner:self options:nil] lastObject];
    
    if (![self.dataArray count]) {
        return cell;
    }
    cell.contentBg.layer.cornerRadius = 2;
    
    [cell.infoImage setImageWithURL:[NSURL URLWithString:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"images"]] placeholderImage:[UIImage imageNamed:@"jaizai_bg.png"]];
    cell.infoImage.layer.borderWidth = 0.5;
    cell.infoImage.layer.borderColor = [UIColor colorWithRed:150/255.0 green:150/255.0 blue:150/255.0 alpha:1].CGColor;
    cell.infoContentLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"title"];
    cell.authLabel.text = [NSString stringWithFormat:@"作者：%@",[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"author"]];
    cell.authLabel.textColor = CUSTOMTITLECOLOER2;
    cell.timeLabel.text = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"createTimeValue"];
    cell.timeLabel.textColor = CUSTOMTITLECOLOER2;
    [cell resetSize];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [[LogHelper shared] writeToFilefrom_page:@"mldl" from_section:@"m" from_step:@"l" to_page:@"md" to_section:@"m" to_step:@"d" type:@"" id:[[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"]];
    InfoDetailViewController *detailView =[[InfoDetailViewController alloc]initWithNibName:@"InfoDetailViewController" bundle:nil];
    
    detailView.infoID = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"id"];
    detailView.titleName = [[self.dataArray objectAtIndex:indexPath.section] objectForKey:@"title"];
    
    detailView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailView animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}
#pragma mark - ScrollViewDelegate
//always
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSLog(@"---%f",_tagCollectionView.contentOffset.y);
    if ([scrollView isKindOfClass:[UITableView class]]) {
        CGPoint contentOffsetPoint = self.tagTableView.contentOffset;
        CGRect frame = self.tagTableView.frame;
        if (contentOffsetPoint.y >= self.tagTableView.contentSize.height - frame.size.height - 20|| self.tagTableView.contentSize.height < frame.size.height)
        {
            [self.tagTableView tableViewDidEndDragging:scrollView];
        }
        [self.tagTableView tableViewDidScroll:scrollView];
    }
}

//停止拖动
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    if ([scrollView isKindOfClass:[UITableView class]]) {
        CGPoint contentOffsetPoint = self.tagTableView.contentOffset;
        CGRect frame = self.tagTableView.frame;
        if (contentOffsetPoint.y < -50)
        {
            [self refreshData];
            [self.tagTableView tableViewDidEndDragging:scrollView];
        }else if (contentOffsetPoint.y >= self.tagTableView.contentSize.height - frame.size.height - 25|| self.tagTableView.contentSize.height < frame.size.height)
        {
                    [self.tagTableView tableViewDidEndDragging:scrollView];
            [self performSelector:@selector(loadMoreData) withObject:nil afterDelay:0.0f];
        }
    }else if ([scrollView isKindOfClass:[UICollectionView class]]){
        CGPoint contentOffsetPoint = self.tagCollectionView.contentOffset;
        if (contentOffsetPoint.y > self.tagCollectionView.contentSize.height - self.tagCollectionView.frame.size.height) {
            [self loadMoreTag];
        }

    }
}
//减速停止
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
}


#pragma mark - 加载数据
-(void)loadTags{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSMutableDictionary *exprame = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    (NSString *)API_URL_MESSAGETAGS_LIST,@"r",
                                    [NSString stringWithFormat:@"%d",_size],@"size",
                                    [NSString stringWithFormat:@"%d",_tagPage],@"page",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:exprame object:self action:@selector(loadTagsFinish:) method:GETDATA];
}

- (void)loadMoreTag {
    _tagPage ++;
    [self loadTags];
}

-(void)loadTagsFinish:(NSDictionary *)dict{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    if (dict && ![[dict objectForKey:@"code"] integerValue]){
//        [_tagsArr removeAllObjects];
        for (NSDictionary *dic in dict[@"results"]) {
            [_tagsArr addObject:dic];
            [_tagType addObject:@"0"];
        }
        
        
        [_tagCollectionView reloadData];
    }
}


-(void)loadTagData{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSMutableDictionary *searchKeyword = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                          (NSString *)API_URL_MESSAGE_LIST,@"r",
                                          [NSString stringWithFormat:@"%d",_page],@"page",
                                          @"10",@"size",
                                          @"id",@"order",
                                          _tagName,@"search_title",nil];
    [YK_API_request startLoad:INTERFACE_PREFIX extraParams:searchKeyword object:self action:@selector(loadTagDataFinish:) method:GETDATA];
}

-(void)loadTagDataFinish:(NSDictionary *)dic{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];

    [self.hud hide:YES];
    self.isLoading = NO;
    int itemCount = 0;
    if (dic && ![[dic objectForKey:@"code"] integerValue]) {
        for (NSDictionary *item in [dic objectForKey:@"results"] ) {
            [self.dataArray addObject:item];
        }

        itemCount = (int)[[dic objectForKey:@"results"] count];
        _lastID = [NSString stringWithFormat:@"%@",self.dataArray[0][@"id"]];
        [self.tagTableView reloadData];
        
        if (![self.dataArray count]) {
            self.contentStatusLabel.hidden = NO;
        } else {
            self.contentStatusLabel.hidden = YES;
        }
        if (!self.isMore) {
            if (self.infoCount) {
                if ([[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue] <= [self.infoCount intValue]){
                    self.topShowView.showInfo = @" 没有更新的内容了啦~";
                    [self.topShowView show];
                }else{
                    int i = [[[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"] intValue]-[self.infoCount intValue];
                    self.topShowView.showInfo = [NSString stringWithFormat:@"又来了%d篇",i];
                    [self.topShowView show];
                }
            }
            self.infoCount = [[dic objectForKey:@"extraInfo"] objectForKey:@"countTotal"];
        } else {
            if (itemCount == 0) {
                [self bottomShow];
            }
        }
    }
    [self.tagTableView tableViewDidFinishedLoading];
}

-(void)refreshData{
    [self loadData:NO];
}
-(void)loadMoreData{
    [self loadData:YES];
}
//上拉加载更多---资讯
-(void)loadData:(BOOL)isOrmore{
    
    if (self.isLoading) {
        return;
    }
    if (isOrmore == YES) {
        _page++;
        self.isMore = YES;
    } else {
        _page = 1;
        self.isMore = NO;
        [self.dataArray removeAllObjects];
    }
        self.isLoading = YES;
        [self loadTagData];

}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
