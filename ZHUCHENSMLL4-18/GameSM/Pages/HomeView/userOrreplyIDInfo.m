//
//  userOrreplyIDInfo.m
//  GameSM
//
//  Created by 顾鹏 on 16/3/1.
//  Copyright © 2016年 王涛. All rights reserved.
//

#import "userOrreplyIDInfo.h"

@implementation userOrreplyIDInfo
+ (userOrreplyIDInfo *)paraseUserOrReplyIDInfo:(NSDictionary *)userDict{
    userOrreplyIDInfo *userIdInfo = [[userOrreplyIDInfo alloc] init];
    userIdInfo.id = userDict[@"id"];
    userIdInfo.name = userDict[@"name"];
    userIdInfo.images = userDict[@"images"];
    return userIdInfo;
}
@end
