//
//  GuideViewController.h
//  GameSM
//
//  Created by 王涛 on 15/7/30.
//  Copyright (c) 2015年 王涛. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface GuideViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end
